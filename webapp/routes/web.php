<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Artisan;

Route::pattern('id', '[1-9][0-9]*');
Route::pattern('id2', '[1-9][0-9]*');

Route::get('/', '\App\Http\Controllers\PublicController@getIndex');
Route::get('/contact', '\App\Http\Controllers\PublicController@getContactView');

Route::get('/products', '\App\Http\Controllers\ProductController@getProducts');
Route::get('/product/{id}', '\App\Http\Controllers\ProductController@getProduct');

Route::get('/news', 'PublicNewsController@getAllPosts');
Route::get('/news/{id}', 'PublicNewsController@getNewsItem');
Route::get('/post/{id}', 'PublicNewsController@getNewsItem');
Route::get('/page/{id}', 'PageController@page');

Route::get('/cart', '\App\Http\Controllers\CartController@getCartView');
Route::post('/cart/add', '\App\Http\Controllers\CartController@add');
Route::post('/cart/updateItem', '\App\Http\Controllers\CartController@updateItem');
Route::get('/cart/remove/{key}', '\App\Http\Controllers\CartController@remove');
Route::get('/cart/getCartContent', '\App\Http\Controllers\CartController@getCartContent');

Route::get('/checkout', '\App\Http\Controllers\CartController@getCheckoutPage');
Route::get('/checkoutNoAuth', '\App\Http\Controllers\CartController@checkoutNoAuth');
Route::get('/afterCheckoutPage/{id}', '\App\Http\Controllers\CartController@afterCheckoutPage');
Route::post('/pay', '\App\Http\Controllers\CartController@getPayPage');
Route::post('/finishOrder', '\App\Http\Controllers\CartController@finishOrder');
Route::post('/callback', '\App\Http\Controllers\CartuController@callback');
Route::get('/callback', '\App\Http\Controllers\CartuController@callback');

Route::post('/login', 'Auth\UserLoginController@login')->name('user.login.submit');
Route::get('/logout', 'Auth\UserLoginController@logout');
Route::get('/user/profile', 'Auth\UserLoginController@showProfilePage');
Route::get('/user/password', 'Auth\UserLoginController@showPasswordPage');
Route::get('/user/address', 'Auth\UserLoginController@showAddressPage');
Route::get('/user/purchaseHistory', 'Auth\UserLoginController@showPurchaseHistoryPage');

Route::get('/register', 'Auth\RegisterController@showRegistrationForm');
//Route::post('/register', 'Auth\RegisterController@register');
Route::post('/register', ['middleware' => 'verifyCsrf', 'uses' => 'Auth\RegisterController@register']);
Route::get('/userSetParent/{id}/{id2}', 'Auth\RegisterController@userSetParent');

Route::get('/price-calculator', 'PublicController@getPriceCalculatorView');

Route::prefix('admin')->group(function () {
    Route::get('/', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
});

Route::group(['prefix' => 'admin', 'middleware' => 'auth:admin'], function () {
    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
    Route::get('/dashboard', 'AdminController@index')->name('admin.dashboard');

    Route::get('/getUsers', '\App\Http\Controllers\Admin\AdminPermissionController@getUsers');
    Route::get('/getUsersWithRoles', '\App\Http\Controllers\Admin\AdminPermissionController@getUsersWithRoles');
    Route::get('/getUserRoles/{id}', '\App\Http\Controllers\Admin\AdminPermissionController@getUserRoles');
    Route::get('/getUserPermissions/{id}', '\App\Http\Controllers\Admin\AdminPermissionController@getUserPermissions');
    Route::post('/attachRole', '\App\Http\Controllers\Admin\AdminPermissionController@attachRoleToUser');

    Route::get('/getRoles', '\App\Http\Controllers\Admin\PermissionController@getRoles');
    Route::get('/getPermissions', '\App\Http\Controllers\Admin\PermissionController@getPermissions');
    Route::post('/addPermission', '\App\Http\Controllers\Admin\PermissionController@addPermission');
    Route::post('/addRole', '\App\Http\Controllers\Admin\PermissionController@addRole');
});

Route::group(['prefix' => 'api', 'middleware' => 'auth:admin'], function () {
    Route::post('/admin/getProducts', '\App\Http\Controllers\Admin\ProductController@getProducts');
    Route::get('/admin/getProductInfo/{id}', '\App\Http\Controllers\Admin\ProductController@getProductInfo');
    Route::get('/admin/getProductInfoTypes', '\App\Http\Controllers\Admin\ProductController@getProductInfoTypes');
    Route::post('/admin/addProduct', '\App\Http\Controllers\Admin\ProductController@addProduct');
    Route::post('/admin/addProductInfoTypes', '\App\Http\Controllers\Admin\ProductController@addProductInfoTypes');
    Route::post('/admin/addProductInfo', '\App\Http\Controllers\Admin\ProductController@addProductInfo');
    Route::get('/admin/removeProductInfo/{id}', '\App\Http\Controllers\Admin\ProductController@removeProductInfo');
    Route::get('/admin/removeProductInfoType/{id}', '\App\Http\Controllers\Admin\ProductController@removeProductInfoType');
    Route::get('/admin/getproductFullInfo/{id}', '\App\Http\Controllers\Admin\ProductController@getproductFullInfo');
    Route::get('/admin/removeProduct/{id}', '\App\Http\Controllers\Admin\ProductController@removeProduct');
    Route::get('/admin/removeProductAllFile/{id}', '\App\Http\Controllers\Admin\ProductController@removeProductAllFile');
    Route::post('/admin/addProductFile', '\App\Http\Controllers\Admin\ProductController@addProductFile');
    Route::get('/admin/getProductFiles/{id}', '\App\Http\Controllers\Admin\ProductController@getProductFiles');
    Route::get('/admin/removeProductFile/{id}', '\App\Http\Controllers\Admin\ProductController@removeProductFile');
    Route::post('/admin/productInfo/changeOrder/{id}/{direction}', array('uses' => '\App\Http\Controllers\Admin\ProductController@changeProductInfoOrder'));


    // CATEGORIES
    Route::get('/admin/getCategories', '\App\Http\Controllers\Admin\CategoryController@getCategories');
    Route::get('/admin/getCategoriesFlat', '\App\Http\Controllers\Admin\CategoryController@getCategoriesFlat');
    Route::get('/admin/getCategoriesTree', '\App\Http\Controllers\Admin\CategoryController@getCategoriesTree');
    Route::post('/admin/addCategory', '\App\Http\Controllers\Admin\CategoryController@addCategory');
    Route::get('/admin/removeCategory/{id}', '\App\Http\Controllers\Admin\CategoryController@removeCategory');
    Route::post('/admin/categories/changeOrder/{id}/{direction}', array('uses' => '\App\Http\Controllers\Admin\CategoryController@changeOrder'));

    // BRANCHES
    Route::get('/admin/getBranches', '\App\Http\Controllers\Admin\BranchController@getBranches');
    Route::post('/admin/addBranche', '\App\Http\Controllers\Admin\BranchController@addBranche');

    // LANGUAGES
    Route::get('/admin/getLanguage', '\App\Http\Controllers\Admin\LanguageController@getLanguage');
    Route::post('/admin/getAllLanguage', '\App\Http\Controllers\Admin\LanguageController@getAllLanguage');
    Route::post('/admin/saveLang', '\App\Http\Controllers\Admin\LanguageController@saveLang');

    // MENU
    Route::get('/admin/menu', array('uses' => '\App\Http\Controllers\Admin\MenusController@getMenus'));
    Route::post('/admin/menus/add', array('uses' => '\App\Http\Controllers\Admin\MenusController@addMenu'));
    Route::post('/admin/menus/destroy/{id}', array('uses' => '\App\Http\Controllers\Admin\MenusController@destroy'));
    Route::post('/admin/menus/changeOrder/{id}/{direction}', array('uses' => '\App\Http\Controllers\Admin\MenusController@changeOrder'));

    // PARAMS
    Route::post('/admin/params/getParams', array('uses' => '\App\Http\Controllers\Admin\ParamsController@getParams'));
    Route::post('/admin/params/updateParam', array('uses' => '\App\Http\Controllers\Admin\ParamsController@updateParam'));

    // WEB PAGES
    Route::get('/admin/pages', array('uses' => 'Admin\PageController@getPages'));
    Route::get('/admin/pages/destroy/{id}', 'Admin\PageController@destroy');
    Route::post('/admin/pages/add', array('uses' => 'Admin\PageController@add'));

    // WEB NEWS
    Route::get('/admin/news', array('uses' => 'Admin\NewsController@getNews'));
    Route::get('/admin/news/destroy/{id}', 'Admin\NewsController@destroy');
    Route::get('/admin/news/gallery/{articleId}/{articleCategoryid}', 'Admin\FolderController@getArticleImages');
    Route::get('/admin/news/gallery/remove/{fileManager}/{articleId}/{articleCategoryid}', 'Admin\FolderController@destroyArticleImages');
    Route::post('/admin/news/add', array('uses' => 'Admin\NewsController@add'));

    // WEB POSTS
    Route::get('/admin/posts', array('uses' => 'Admin\PostController@getPosts'));
    Route::post('/admin/post/add', array('uses' => 'Admin\PostController@addPost'));
    Route::get('/admin/post/destroy/{id}', 'Admin\PostController@destroy');

    // WEB CATEGORIES
    Route::get('/admin/web/categories', array('uses' => 'Admin\WebCategoryController@getCategories'));
    Route::get('/admin/web/category/remove/{id}', array('uses' => 'Admin\WebCategoryController@destroyCategory'));
    Route::post('/admin/web/category/add', array('uses' => 'Admin\WebCategoryController@addCategory'));

    // PUBLIC USERS
    Route::post('/admin/getPublicUsers', 'Admin\PublicUserController@getPublicUsers');
    Route::get('/admin/getPublicUsersTree', 'Admin\PublicUserController@getPublicUsersTree');
    Route::get('/admin/removePublicUsers/{id}', 'Admin\PublicUserController@removePublicUsers');

    // SLIDESHOW
    Route::get('/admin/slideshow/getAll', 'Admin\SlideShowController@geSlideShows');
    Route::get('/admin/slideshow/remove/{id}', 'Admin\SlideShowController@removeSlide');
    Route::post('/admin/slideshow/add', 'Admin\SlideShowController@addSlide');

    // Links
    Route::get('/admin/links/getLinks', 'Admin\LinksController@getLinks');
    Route::get('/admin/links/remove/{id}', 'Admin\LinksController@removeLink');
    Route::post('/admin/links/add', 'Admin\LinksController@addLink');

    // Cities
    Route::get('/admin/cities/getCities', 'Admin\CitiesController@getCities');
    Route::get('/admin/cities/remove/{id}', 'Admin\CitiesController@removeCity');
    Route::post('/admin/cities/add', 'Admin\CitiesController@addCity');

    // Sells
    Route::post('/admin/sells/getSells', 'Admin\SellController@getSells');
    Route::get('/admin/sells/remove/{id}', 'Admin\SellController@removeSell');

    // Installments
    Route::post('/admin/installments/getTbcInstallments', 'Admin\SellController@getTbcInstallments');
    Route::get('/admin/installments/confirmTbcInstallment/{id}', 'Admin\SellController@confirmTbcInstallment');
    Route::get('/admin/installments/cancelTbcInstallment/{id}', 'Admin\SellController@cancelTbcInstallment');
    Route::get('/admin/installments/statusTbcInstallment/{id}', 'Admin\SellController@statusTbcInstallment');
});

//Clear Cache facade value:
Route::get('/clear-cache', function () {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('view:clear');
    return '<h1>All Cache Cleared</h1>';
});
