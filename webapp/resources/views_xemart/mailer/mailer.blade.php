<?php
if (isset($mailer)) {
    $text = '';
    if (isset($mailer['fullName'])) $text = "სახელი გვარდი (ან ორგანიზაციის დასახელება): <b>" . $mailer['fullName'] . "</b><br>";
    if (isset($mailer['phone'])) $text .= "პირადი ნომერი (ან საიდენტიფიკაციო კოდი): <b>" . $mailer['phone'] . "</b><br>";
    if (isset($mailer['phone'])) $text .= "ტელეფონი: <b>" . $mailer['phone'] . "</b><br><br>";

    if (array_key_exists('cardContent', $mailer) && $mailer['cardContent']) {
        $text .= '<table width="100%" border="1" cellspacing="0" cellpadding="8"><thead><tr>'
            . '<th>დასახელება</th><th>რაოდენობა</th><th>ერთ. ფასი</th><th>ჯამი</th>'
            . '<tr></thead><tbody>';

        foreach ($mailer['cardContent'] as $key => $value) {
            $text .= "<tr><td><b>" . $value->name . "</b></td>"
                . "<td>" . $value->qty . "</td>"
                . "<td>" . $value->price . "</td>"
                . "<td>" . $value->subtotal . "</td>"
                . "</tr>";
        }

        $text .= "<tr><td align='right' colspan='3'>სულ:</td>"
            . "<td>" . $mailer['cardSubtotal'] . "</td>"
            . "</tr>";

        $text .= '</tbody></table><br>';
    }
    echo $text;
}
?>
