@extends('web')
@section('content')
    <!-- Breadcrumb Area -->
    <section class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-box text-center">
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item"><a href="{{url("/")}}">მთავარი</a></li>
                            <li class="list-inline-item"><span>||</span> კალათა</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Breadcrumb Area -->

    <!-- Shopping Cart -->
    <section class="shopping-cart">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="cart-table table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th class="t-pro">პროდუქტი</th>
                                <th class="t-price">ფასი</th>
                                <th class="t-qty">რაოდენობა</th>
                                <th class="t-total">ჯამი</th>
                                <th class="t-rem"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(Cart::content() as $key => $value)
                                <tr>
                                    <td class="t-pro d-flex">
                                        <div class="t-img">
                                            <a href="{{url('/product/'.$value->id)}}">
                                                <img src="{{$value->options->img}}" height="60" alt="">
                                            </a>
                                        </div>
                                        <div class="t-content">
                                            <p class="t-heading">
                                                <a href="{{url('/product/'.$value->id)}}">
                                                    {{$value->name}}
                                                </a>
                                            </p>
                                            <!--                                        <ul class="list-unstyled col-sz">
                                                                                        <li><p>Color : <span>Red</span></p></li>
                                                                                        <li><p>Size : <span>M</span></p></li>
                                                                                    </ul>-->
                                        </div>
                                    </td>
                                    <td class="t-price">{{$value->price}}</td>
                                    <td class="t-qty">
                                        <div class="qty-box">
                                            <div class="quantity buttons_added">
                                                <input type="button" value="-" class="minus">
                                                <input type="number" step="1" min="1" max="100000"
                                                       value="{{$value->qty}}"
                                                       class="qty text"
                                                       size="6"
                                                       onchange="updateItemToShoppingCart('{{$key}}',this.value, true);"
                                                       readonly>
                                                <input type="button" value="+" class="plus">
                                            </div>
                                        </div>
                                    </td>
                                    <td class="t-total">{{$value->subtotal}}</td>
                                    <td class="t-rem">
                                        <a href="javascript:void(0)"
                                           onclick="removeItemFromShoppingCart('{{$key}}', true); return false">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-4">
                    <!--                    <div class="shipping">
                                            <h6>Calculate Shipping and Tax</h6>
                                            <p>Enter your destination to get shipping estimate</p>
                                            <form action="#">
                                                <div class="country-box">
                                                    <select class="country">
                                                        <option>Select Country</option>
                                                        <option>United State</option>
                                                        <option>United Kingdom</option>
                                                        <option>Germany</option>
                                                        <option>Australia</option>
                                                    </select>
                                                </div>
                                                <div class="state-box">
                                                    <select class="state">
                                                        <option>State/Province</option>
                                                        <option>State 1</option>
                                                        <option>State 2</option>
                                                        <option>State 3</option>
                                                        <option>State 4</option>
                                                    </select>
                                                </div>
                                                <div class="post-box">
                                                    <input type="text" name="zip" value="" placeholder="Zip/Postal Code">
                                                    <button type="button">Get Estimate</button>
                                                </div>
                                            </form>
                                        </div>-->
                </div>
                <div class="col-md-4">
                    <!--<div class="coupon">
                        <h6>Discount Coupon</h6>
                        <p>Enter your coupon code if you have one</p>
                        <form action="#">
                            <input type="text" name="zip" value="" placeholder="Your Coupon">
                            <button type="button">Apply Code</button>
                        </form>
                    </div>-->
                </div>
                <div class="col-md-4">
                    <div class="crt-sumry">
                        <h5>კალათა</h5>
                        <ul class="list-unstyled">
                            <li>ფასი <span>{{Cart::subtotal()}}</span></li>
                            <li>მიწოდება <span>0.00</span></li>
                            <li>ჯამი <span>{{Cart::subtotal()}}</span></li>
                        </ul>
                        <div class="cart-btns text-right">
                            {{--                            <button type="button" class="up-cart">Update Cart</button>--}}
                            @if(Cart::count() > 0)
                                <a href="{{url("/checkoutNoAuth")}}">
                                    <button type="button" class="chq-out">ყიდვა</button>
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Shopping Cart -->
@stop
