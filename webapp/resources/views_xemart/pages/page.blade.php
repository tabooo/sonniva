@extends('web')
@section('content')
    <!-- Breadcrumb Area -->
    <section class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-box text-center">
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item"><a href="{{url("/")}}">მთავარი</a></li>
                            <li class="list-inline-item"><span>||</span> {{$page['title_ge']}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Breadcrumb Area -->

    <!-- Terms & Condition -->
    <section class="term-condition">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="term-box">
                        {!!$page['description_ge']!!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Terms & Condition -->
@stop
