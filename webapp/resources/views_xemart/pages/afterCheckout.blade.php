@extends('web')
@section('content')
    <!-- Breadcrumb Area -->
    <section class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-box text-center">
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item"><a href="{{url("/")}}">მთავარი</a></li>
                            <li class="list-inline-item"><span>||</span> შეკვეთა</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Breadcrumb Area -->

    <!-- Checkout -->
    <section class="checkout">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <b>მადლობა. თქვენი შეკვეთა მიღებულია. შეკვეთის ნომერი: {{$sell['sell_id']}}</b>
                </div>
                <!-- End Checkout Form -->
            </div>
        </div>
        <!-- End Main Content -->
    </section>
@stop
