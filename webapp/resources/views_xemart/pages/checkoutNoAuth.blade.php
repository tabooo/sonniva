@extends('web')
@section('content')
    <div id="overlay" onclick="off()">
        <div class="w-100 d-flex justify-content-center align-items-center">
            <div class="spinner"></div>
        </div>
    </div>

    <!-- Breadcrumb Area -->
    <section class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-box text-center">
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item"><a href="{{url("/")}}">მთავარი</a></li>
                            <li class="list-inline-item"><span>||</span> შეკვეთა</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Breadcrumb Area -->

    <!-- Checkout -->
    <section class="checkout">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <form action="#" id="checkoutForm">
                        <h5>შეავსეთ სავალდებულო ველები</h5>
                        <div class="row">
                            <div class="col-md-6">
                                <label>სახელი*</label>
                                <input type="text" name="first_name" value="" placeholder="">
                            </div>
                            <div class="col-md-6">
                                <label>გვარი*</label>
                                <input type="text" name="last_name" value="" placeholder="">
                            </div>
                            <div class="col-md-6">
                                <label>ელ. ფოსტა*</label>
                                <input type="text" name="email" value="" placeholder="">
                            </div>
                            <div class="col-md-6">
                                <label>ტელეფონი*</label>
                                <input type="text" name="phone" value="" placeholder="">
                            </div>
                            <div class="col-md-12">
                                <label>კომპანია</label>
                                <input type="text" name="company" value="" placeholder="დასახელება, კოდი (არასავალდებულო)">
                            </div>
                            <div class="col-md-12">
                                <label>მისამართი*</label>
                                <input type="text" name="address" value="" placeholder="">
                            </div>
                            <div class="col-md-6 contry">
                                <label>ქვეყანა*</label>
                                <select class="country" name="country">
                                    <option value="1" selected>საქართველო</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>ქალაქი*</label>
                                <input type="text" name="city" value="" placeholder="">
                            </div>
                            <!--                            <div class="col-md-12">
                                                            <ul class="list-unstyled">
                                                                <li><input type="checkbox" id="samsung" name="name">
                                                                    <label for="samsung">
                                                                        Create An Account?
                                                                    </label></li>
                                                                <li><input type="checkbox" id="apple" name="name"><label for="apple">Ship To Same
                                                                        Address?</label></li>
                                                            </ul>
                                                        </div>-->
                            <div class="col-md-12">
                                <label>შენიშვნა</label>
                                <textarea name="note"
                                          placeholder="დაგვიტოვეთ კომენტარი შეკვეთასთან დაკავშირებით (არასავალდებულო)"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="order-review">
                                <h5>შეკვეთა</h5>
                                <div class="review-box">
                                    <ul class="list-unstyled">
                                        <li>პროდუქტი <span>ჯამი</span></li>
                                        @foreach(Cart::content() as $key => $value)
                                            <li class="d-flex justify-content-between">
                                                <div class="pro">
                                                    <img src="{{$value->options->img}}" alt="">
                                                    <p>{{$value->name}}</p>
                                                    <span>{{$value->qty}} X {{$value->price}}</span>
                                                </div>
                                                <div class="prc">
                                                    <p>{{$value->subtotal}}</p>
                                                </div>
                                            </li>
                                        @endforeach
                                        <li>ჯამი <span>{{Cart::subtotal()}}</span></li>
                                        <li>მიწოდება <span>00.00</span></li>
                                        <li>მთლიანი ჯამი <span>{{Cart::subtotal()}}</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="pay-meth">
                                <h5>გადახდის მეთოდი</h5>
                                <div class="pay-box">
                                    <ul class="list-unstyled">
                                        <li>
                                            <input type="radio" id="pay1" name="payment_type" value="1" checked>
                                            <label for="pay1">
                                                <span><i class="fa fa-circle"></i></span>
                                                ნაღდი ანგარიშწორებით
                                            </label>
                                            <p>გადახდა ხდება ადგილზე კურიერთან.</p>
                                        </li>
                                        {{--                                        <li>--}}
                                        {{--                                            <input type="radio" id="pay2" name="payment" value="pay2">--}}
                                        {{--                                            <label for="pay2">--}}
                                        {{--                                                <span><i class="fa fa-circle"></i></span>--}}
                                        {{--                                                საბანკო გადარიცხვა--}}
                                        {{--                                            </label>--}}
                                        {{--                                        </li>--}}
                                        {{--                                        <li>--}}
                                        {{--                                            <input type="radio" id="pay3" name="payment" value="pay3">--}}
                                        {{--                                            <label for="pay3">--}}
                                        {{--                                                <span><i class="fa fa-circle"></i></span>--}}
                                        {{--                                                ბარათით გადახდა--}}
                                        {{--                                            </label>--}}
                                        {{--                                        </li>--}}
                                        <li>
                                            <input type="radio" id="pay4" name="payment_type" value="2">
                                            <label for="pay4">
                                                <span><i class="fa fa-circle"></i></span>
                                                განვადება (TBC)
                                            </label>
                                        </li>
                                        <li>
                                            <input type="radio" id="pay5" name="payment_type" value="3">
                                            <label for="pay5">
                                                <span><i class="fa fa-circle"></i></span>
                                                განვადება (Crystal)
                                            </label>
                                        </li>
                                        <li>
                                            <input type="radio" id="pay6" name="payment_type" value="4">
                                            <label for="pay6">
                                                <span><i class="fa fa-circle"></i></span>
                                                განვადება (Credo)
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <button type="button" name="button" class="ord-btn" onclick="finishOrder()">
                                შეკვეთის გაფორმება
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Checkout -->
@stop
