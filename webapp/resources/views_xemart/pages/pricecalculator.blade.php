@extends('web')
@section('content')
    <!-- Breadcrumb Area -->
    <section class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-box text-center">
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item"><a href="{{url("/")}}">მთავარი</a></li>
                            <li class="list-inline-item"><span>||</span> ფასის კალკულატორი</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Breadcrumb Area -->

    <!-- Register -->
    <section class="register">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form action="#" id="calculateform">
                        <div style="color: red; font-weight: bold; text-align: center; margin-bottom: 15px;">
                            გთხოვთ გაითვალისწინოთ რომ პროგრამის მიერ დაგენერირებული ფასი შესაძლოა შეიცვალოს სამზარეულოს
                            ტექნიკური მახასიათებლებიდან გამომდინარე
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>კედლის ზომა (სმ)* </label>
                                <small class="form-text text-muted">მიუთითეთ კედლის ან კუთხის შემთხვევაში
                                    ჯამური სიგანე.</small>
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="size"
                                       placeholder="მაგ: 350">
                            </div>

                            <div class="col-md-6">
                                <label>სამზარეულოს მასალა* </label>
                            </div>
                            <div class="col-md-6">
                                <div class="form-check form-check-inline">
                                    <input type="radio" name="material" value="1" class="form-check-input">
                                    <label for="male">ლამინატი</label><br>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input type="radio" name="material" value="2" class="form-check-input">
                                    <label for="male">აკრილი</label><br>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <label>კუთხის სამზარეულო </label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" name="iscorner" value="450"
                                       placeholder="მონიშნეთ თუ კუთხის სამზარეულო გსურთ">
                            </div>

                            <div class="col-md-6">
                                <label>რბილი მექანიზმით </label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" name="rbilimeqanizmi" value="1">
                            </div>

                            <div class="col-md-6">
                                <label>ზედაპირის პლინტუსით </label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" name="zedapirisplintusi" value="1">
                            </div>

                            <div class="col-md-12"
                                 style="margin-top: 15px; margin-bottom: 15px; font-weight: lighter; font-style: italic">
                                <label>ჩაშენების შემთხვევაში მონიშნეთ შესაბამისი ტექნიკა </label>
                            </div>

                            <div class="col-md-6">
                                <label>ნიჟარის ჩაშენება </label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" name="nijara" value="30">
                            </div>

                            <div class="col-md-6">
                                <label>გაზქუქრის ჩაშენება </label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" name="gazqura" value="30">
                            </div>

                            <div class="col-md-6">
                                <label>ღუმელის ჩაშენება </label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" name="gumeli" value="30">
                            </div>

                            <div class="col-md-6">
                                <label>სარეცხი მანქანის ჩაშენება </label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" name="sarecximanqana" value="100">
                            </div>

                            <div class="col-md-6">
                                <label>ჭურჭლის საწური </label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" name="churchlissawuri" value="50">
                            </div>

                            <div class="col-md-6 text-left">
                                <button type="button" name="button" onclick="calculateprice();">გამოთვლა</button>
                            </div>
                            <div class="col-md-6" id="calculatedprice" style="font-size: 20px; font-weight: bold">
                            </div>
                        </div>

                        <div style="color: red; font-weight: bold; text-align: center; margin-bottom: 15px; margin-top: 15px;">
                            ფასში გათვალისწინებულია ადგილზე მოტანა და მონტაჟი თბილისის მასშტაბით
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- End Register -->

    <script>
        function calculateprice() {
            $("#calculatedprice").html("");
            var formData = {
                size: $('input[name=size]').val(),
                iscorner: $('input[name=iscorner]:checked').val(),
                material: $('input[name=material]:checked').val(),
                nijara: $('input[name=nijara]:checked').val(),
                gazqura: $('input[name=gazqura]:checked').val(),
                gumeli: $('input[name=gumeli]:checked').val(),
                sarecximanqana: $('input[name=sarecximanqana]:checked').val(),
                churchlissawuri: $('input[name=churchlissawuri]:checked').val(),
                rbilimeqanizmi: $('input[name=rbilimeqanizmi]:checked').val(),
                zedapirisplintusi: $('input[name=zedapirisplintusi]:checked').val(),
            };
            if (!formData.size || !formData.material) {
                alert('მიუთიეთ აუცილებელი ველები');
                return;
            }

            if (formData.iscorner && formData.size < 175) {
                alert('ზომა 175 სმ–ზე მეტი უნდა იყოს');
                return;
            } else if (!formData.iscorner && formData.size < 40) {
                alert('ზომა 40 სმ–ზე მეტი უნდა იყოს');
                return;
            }


            let price = 0;

            if (formData.iscorner) {
                price += Number(formData.iscorner);
                formData.size = Number(formData.size) - 172;
            }

            if (formData.material == 1) {
                price += formData.size * 3.65;
            } else if (formData.material == 2) {
                price += formData.size * 4.25;
            }

            if (formData.zedapirisplintusi == 1) {
                price += formData.size * 0.2;
            }

            if (formData.nijara) {
                price += Number(formData.nijara);
            }

            if (formData.gazqura) {
                price += Number(formData.gazqura);
            }

            if (formData.gumeli) {
                price += Number(formData.gumeli);
            }

            if (formData.sarecximanqana) {
                price += Number(formData.sarecximanqana);
            }

            if (formData.churchlissawuri) {
                price += Number(formData.churchlissawuri);
            }

            if (formData.rbilimeqanizmi == 1) {
                price *= 1.05;
            }

            $("#calculatedprice").html(price.toFixed(2) + " ლარი");
        }
    </script>
@stop
