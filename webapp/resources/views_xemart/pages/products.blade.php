@extends('web')
@section('content')
    <!-- Breadcrumb Area -->
    <section class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-box text-center">
                        <ul class="list-unstyled list-inline">
                            @foreach(array_reverse($parentCategories) as $parentCategory)
                                <li class="list-inline-item">
                                    <a href="#">
                                        @if($loop->index > 0)
                                            ||
                                        @endif
                                        {{$parentCategory['category_name']}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Breadcrumb Area -->

    <!-- Category Area -->
    <section class="category">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="category-box">
                        <div class="sec-title">
                            <h6>კატეგორიები</h6>
                        </div>
                        <!-- accordion -->
                        <div id="accordion">
                            @foreach($categories[0]['childrenObjects'] as $category)
                                <div class="card">
                                    <div class="card-header">
                                        <a href="{{url('/products?categoryId='.$category['category_id'])}}"
                                           data-toggle="collapse" data-target="#collapse{{ $loop->index }}">
                                            <span>{{$category['category_name']}}</span>
                                            <i class="fa fa-angle-down"></i>
                                        </a>
                                    </div>
                                    @if(count($category['childrenObjects'])>0)
                                        <div id="collapse{{ $loop->index }}" class="collapse">
                                            <div class="card-body">
                                                <ul class="list-unstyled">
                                                    @foreach($category['childrenObjects'] as $subCat)
                                                        <li>
                                                            <a href="{{url('/products?categoryId='.$subCat['category_id'])}}">
                                                                <i class="fa fa-angle-right"></i> {{$subCat['category_name']}}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="product-box">
                        <!--                        <div class="cat-box d-flProductStorageex justify-content-between">
                                                    &lt;!&ndash; Nav tabs &ndash;&gt;
                                                    <div class="view">
                                                        <ul class="nav nav-tabs" role="tablist">
                                                            <li class="nav-item">
                                                                <a class="nav-link active" data-toggle="tab" href="#grid"><i
                                                                            class="fa fa-th-large"></i></a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" data-toggle="tab" href="#list"><i class="fa fa-th-list"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="sortby">
                                                        <span>Sort By</span>
                                                        <select class="sort-box">
                                                            <option>Position</option>
                                                            <option>Name</option>
                                                            <option>Price</option>
                                                            <option>Rating</option>
                                                        </select>
                                                    </div>
                                                    <div class="show-item">
                                                        <span>Show</span>
                                                        <select class="show-box">
                                                            <option>12</option>
                                                            <option>24</option>
                                                            <option>36</option>
                                                        </select>
                                                    </div>
                                                    <div class="page">
                                                        <p>Page 1 of 20</p>
                                                    </div>
                                                </div>-->
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="grid" role="tabpanel">
                                <div class="row">
                                    @foreach($products as $product)
                                        <?php
                                        $prodImg = asset('/assets/images/demo/noimage.jpg');
                                        if (count($product['productFile']) > 0) {
                                            $prodImg = asset('/files/product_files/' . $product['product_id'] . '/' . $product['productFile'][0]['file_name']);
                                        }
                                        ?>
                                        <div class="col-lg-4 col-md-6">
                                            <div class="tab-item">
                                                <div class="tab-img">
                                                    <a href="{{asset("/product/".$product['product_id'])}}">
                                                        <img class="main-img img-fluid" src="{{$prodImg}}" alt="">
                                                        <img class="sec-img img-fluid" src="{{$prodImg}}" alt="">
                                                    </a>
                                                </div>
                                                <div class="tab-heading">
                                                    <p>
                                                        <a href="{{asset("/product/".$product['product_id'])}}">{{$product['name']}}</a>
                                                    </p>
                                                </div>
                                                <div class="img-content d-flex justify-content-between">
                                                    <div>
                                                        <ul class="list-unstyled list-inline price">
                                                            <li class="list-inline-item">{{$product['price']}} ლარი</li>
                                                            <li class="list-inline-item"></li>
                                                        </ul>
                                                    </div>
                                                    <div>
                                                        <a href="#" data-toggle="tooltip" data-placement="top"
                                                           onclick="addItemToShoppingCart({{$product['product_id']}},'{{$prodImg}}'); return false;"
                                                           title="კალათაში დამატება"
                                                           title="Add to Cart">
                                                            <img src="{{url("/themes/xemart")}}/images/it-cart.png"
                                                                 alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="pagination-box text-center">
                            <ul class="list-unstyled list-inline">
                            {!!$products->links()!!}
                            <!--                                <li class="list-inline-item"><a href="#">1</a></li>
                                <li class="list-inline-item"><a href="#">2</a></li>
                                <li class="active list-inline-item"><a href="#">3</a></li>
                                <li class="list-inline-item"><a href="#">4</a></li>
                                <li class="list-inline-item"><a href="#">...</a></li>
                                <li class="list-inline-item"><a href="#">12</a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-angle-right"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-angle-double-right"></i></a>
                                </li>-->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Category Area -->
@stop
