@extends('web')
@section('content')
    <!-- Breadcrumb Area -->
    <section class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-box text-center">
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item"><a href="{{url("/")}}">მთავარი</a></li>
                            <li class="list-inline-item"><span>||</span> კონტაქტი</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Breadcrumb Area -->

    <!-- Contact -->
    <section class="contact-area">
        <!--        <div id="map"></div>-->
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8422.395418764567!2d44.7743903425146!3d41.72555812000069!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x404472de351b439d%3A0x4d91ec009ae12593!2z4YOa4YOQ4YOR4YOd4YOg4YOQ4YOi4YOd4YOg4YOY4YOQICLhg5vhg6Dhg6nhg5Thg5Xhg5Thg5rhg5giIC0g4YOa4YOY4YOb4YOR4YOQ4YOu4YOY4YOhIOGDk-GDmOGDkOGDkuGDnOGDneGDoeGDouGDmOGDmeGDmOGDoSDhg5Thg5Xhg6Dhg53hg57hg6Phg5rhg5gg4YOv4YOS4YOj4YOk4YOY!5e0!3m2!1sen!2sge!4v1608749460509!5m2!1sen!2sge"
                width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
                tabindex="0"></iframe>
        <div class="container" style="margin-top: 20px">
            <div class="row">
                <div class="col-md-4">
                    <h5>შპს იო-მახა; <br>ს/კ: 417 875 419</h5>
                    <br><br>
                    <h5>თიბისი ბანკი <br>ა/ა GE35TB7689636080100001</h5>
                </div>
                <div class="col-md-8" style="align-self: flex-end;">
                    <h4>საქართველოს ბანკი <br>ა/ა GE02BG0000000622833700</h4>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-4 col-md-5">
                    <div class="contact-box-tp">
                        <h4>საკონტაქტო ინფორმაცია</h4>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="contact-box d-flex">
                                <div class="contact-icon">
                                    <i class="fa fa-map-marker"></i>
                                </div>
                                <div class="contact-content">
                                    <h6>მისამართი</h6>
                                    <p>{{$params['address_ge']}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="contact-box d-flex">
                                <div class="contact-icon">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <div class="contact-content">
                                    <h6>ელ. ფოსტა</h6>
                                    <p>{{$params['email']}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="contact-box d-flex">
                                <div class="contact-icon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <div class="contact-content">
                                    <h6>ტელეფონი</h6>
                                    <p>{{$params['mobile_main']}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="social-link">
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8 col-md-7">
                    <div class="contact-form">
                        <h4>მოგვწერეთ</h4>
                        <form action="#">
                            <div class="row">
                                <div class="col-md-6">
                                    <p><input type="text" id="name" name="name" placeholder="სახელი გვარი"
                                              required="">
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <p><input type="text" id="email" name="email" placeholder="ელ. ფოსტა"
                                              required="">
                                    </p>
                                </div>
                                <div class="col-md-12">
                                    <p><input type="text" id="subject" name="subject" placeholder="სათაური"></p>
                                </div>
                                <div class="col-md-12">
                                    <p><textarea name="message" id="message" placeholder="ტექსტი"
                                                 required=""></textarea></p>
                                </div>
                                <div class="col-md-12">
                                    <button type="submit">გაგზავნა</button>
                                </div>
                            </div>
                            <div id="form-messages"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Contact -->
@stop
