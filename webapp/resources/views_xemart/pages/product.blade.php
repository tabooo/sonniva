@extends('web')
@section('content')
    <style>
        .zoom {
            transition: transform .2s; /* Animation */
            z-index: 999 !important;
            position: relative;
        }

        .zoom:hover {
            transform: scale(1.5);
        }
    </style>

    <!-- Breadcrumb Area -->
    <section class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-box text-center">
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item"><a href="{{url("/")}}">მთავარი</a></li>
                            <li class="list-inline-item"><span>||</span> <a href="{{url("/products")}}">პროდუქტები</a>
                            </li>
                            <li class="list-inline-item"><span>||</span> {{$product['name']}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Breadcrumb Area -->

    <!-- Single Product Area -->
    <section class="sg-product">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="sg-img">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <?php $prodImg = asset('/assets/images/demo/noimage.jpg'); ?>
                                    @if(count($product['productFile'])>0)
                                        <?php $prodImg = asset('/files/product_files/' . $product['product_id'] . '/' . $product['productFile'][0]['file_name']);?>
                                        @foreach($product['productFile'] as $productImg)
                                            <div class="tab-pane fade show <?php if ($loop->index == 0) echo "active";?>"
                                                 id="sg{{ $loop->index }}" role="tabpanel">
                                                <img src="{{asset('/files/product_files/'.$product['product_id'].'/'.$productImg['file_name'])}}"
                                                     alt="" class="img-fluid zoom">
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="tab-pane fade show active" id="sg1" role="tabpanel">
                                            <img src="{{$prodImg}}" alt="" class="img-fluid">
                                        </div>
                                    @endif

                                </div>
                                <div class="nav d-flex justify-content-start">
                                    <?php $prodImg = asset('/assets/images/demo/noimage.jpg'); ?>
                                    @if(count($product['productFile'])>0)
                                        <?php $prodImg = asset('/files/product_files/' . $product['product_id'] . '/' . $product['productFile'][0]['file_name']);?>
                                        @foreach($product['productFile'] as $productImg)
                                            <a class="nav-item nav-link <?php if ($loop->index == 0) echo "active";?>"
                                               data-toggle="tab"
                                               href="#sg{{ $loop->index }}">
                                                <img src="{{asset('/files/product_files/'.$product['product_id'].'/'.$productImg['file_name'])}}"
                                                     alt="">
                                            </a>
                                        @endforeach
                                    @else
                                        <a class="nav-item nav-link active" data-toggle="tab" href="#sg1">
                                            <img src="{{$prodImg}}" alt="">
                                        </a>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="sg-content">
                            <!--                            <div class="pro-tag">
                                                            <ul class="list-unstyled list-inline">
                                                                <li class="list-inline-item"><a href="#">Home Appliance ,</a></li>
                                                                <li class="list-inline-item"><a href="#">Smart Led Tv</a></li>
                                                            </ul>
                                                        </div>-->
                            <div class="pro-name">
                                <p>{{$product['name']}}</p>
                            </div>
                            <div class="pro-price">
                                <ul class="list-unstyled list-inline">
                                    <li class="list-inline-item">{{$product['price']}}</li>
                                    <li class="list-inline-item"></li>
                                </ul>
                            </div>
                            <div class="colo-siz">
                                <div class="qty-box">
                                    <ul class="list-unstyled list-inline">
                                        <li class="list-inline-item">რაოდენობა :</li>
                                        <li class="list-inline-item quantity buttons_added">
                                            <input type="button" value="-" class="minus">
                                            <input type="number" step="1" min="1" max="10" value="1"
                                                   class="qty text" size="4" readonly>
                                            <input type="button" value="+" class="plus">
                                        </li>
                                    </ul>
                                </div>
                                <div class="pro-btns">
                                    <a href="javascript:void(0)" class="cart"
                                       onclick="addItemToShoppingCart({{$product['product_id']}},'{{$prodImg}}'); return false;"
                                    >კალათაში დამატება</a>
                                </div>
                                <div style="color: red; margin-top: 10px;">
                                    ყურადღება!
                                    <br> პროდუქციის შეძენამდე გთხოვთ გადაამოწმოთ მარაგშია თუ არა თქვენთვის სასურველი
                                    ნივთი...
                                    <br> ისარგებლეთ ონლაინ განვადებით და მიიღეთ სასურველი პროდუქცია ყველაზე მარტივად და
                                    სწრაფად მხოლოდ ჩვენთან
                                </div>
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-md-3 col-sm-6" style="margin-top: 10px">
                                        <img src="{{url("/assets/images")}}/TBC.png">
                                    </div>
                                    <div class="col-md-3 col-sm-6" style="margin-top: 10px">
                                        <img src="{{url("/assets/images")}}/CRYSTAL.png">
                                    </div>
                                    <div class="col-md-3 col-sm-6" style="margin-top: 10px">
                                        <img src="{{url("/assets/images")}}/CREDO.png">
                                    </div>
                                    <div class="col-md-3 col-sm-6" style="margin-top: 10px">
                                        <img src="{{url("/assets/images")}}/BOG.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="sg-tab">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#pro-det">
                                        აღწერა
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="pro-det" role="tabpanel">
                                    <p>
                                        {!! $product['description'] !!}
                                    </p>

                                    <br>
                                    <table class="table table-bordered">
                                        <tbody>
                                        @if(count($product['productInfo'])>0)
                                            @foreach($product['productInfo'] as $productInfo)
                                                <tr>
                                                    <td>{{$productInfo['product_info_type_name']}}</td>
                                                    <td>{{$productInfo['value']}}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!--            <div class="col-md-3">
                <div class="category-box">
                    <div class="sec-title">
                        <h6>კატეგორიები</h6>
                    </div>
                    &lt;!&ndash; accordion &ndash;&gt;
                    <div id="accordion">
                        @foreach($categories[0]['childrenObjects'] as $category)
            <div class="card">
                <div class="card-header">
                    <a href="#" data-toggle="collapse" data-target="#collapse{{ $loop->index }}">
                                        <span>{{$category['category_name']}}</span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                </div>
                                <div id="collapse{{ $loop->index }}" class="collapse">
                                    <div class="card-body">
                                        <ul class="list-unstyled">
                                            @if(count($category['childrenObjects'])>0)
                @foreach($category['childrenObjects'] as $subCat)
                    <li>
                        <a href="{{url('/products?categoryId='.$subCat['category_id'])}}">
                                                            <i class="fa fa-angle-right"></i> {{$subCat['category_name']}}
                            </a>
                        </li>
@endforeach
            @endif
                    </ul>
                </div>
            </div>
        </div>
@endforeach
                </div>
            </div>
        </div>-->
        </div>
    </section>
    <!-- End Single Product Area -->
@stop
