@extends('web')
@section('content')
    <!-- Slider Area 2 -->
    <section class="slider-area2">
        <div class="slider-wrapper owl-carousel">
            @foreach($slideshows as $slideshow)
                <div class="slider-item slider{{ $loop->index }}">
                    <div class="slider-table">
                        <div class="slider-tablecell">
                            <div class="container" style="max-width: 100%;">
                                <div class="row">
                                    <div class="col-md-12 col-sm-0">
                                        <div class="img1 wow fadeInRight effect" data-wow-duration="2s"
                                             data-wow-delay="0s">
                                            <img src="{{$slideshow['img_path']}}" alt=""
                                                 style="display: block!important;"
                                                 class="img-fluid">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="slider-box">
                                        <!--<div class="wow fadeInUp effect" data-wow-duration="1.2s"
                                                 data-wow-delay="0.5s">
                                                <h4>{{$slideshow['title_ge']}}</h4>
                                            </div>
                                            <div class="wow fadeInUp effect" data-wow-duration="1.2s"
                                    data-wow-delay="0.6s">
                                                <h1>Huawei Honor 8x | 8x Max</h1>
                                            </div>
                                            <div class="wow fadeInUp effect" data-wow-duration="1.2s"
                                                 data-wow-delay="0.7s">
                                                <p>The Smart Power In Your Hand</p>
                                            </div>
                                            <div class="wow fadeInUp effect" data-wow-duration="1.2s"
                                                 data-wow-delay="0.8s">
                                                <a href="#">Learn More</a>
                                            </div>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
    <!-- End Slider Area 2 -->

    <!-- Service Area -->
    <section class="service-area">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="service-box d-flex">
                        <div class="sr-img" style="height: 70px;">
                            <img src="{{url("/themes/xemart")}}/images/service-1.png" alt="">
                        </div>
                        <div class="">
                            <h6>უფასო მიწოდება</h6>
                            <p>თბილისის მასშტაბით ტრანსპორტირება უფასოა კომპლექტაციის შეძენის შემთხვევაში</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="service-box d-flex">
                        <div class="sr-img" style="height: 70px;">
                            <img src="{{url("/themes/xemart")}}/images/service-2.png" alt="">
                        </div>
                        <div class="">
                            <h6>თანხის დაბრუნება</h6>
                            <p>გარანტირებულად დაიბრუნეთ თანხის 100% თუ ჩვენი მომსახურებით უკმაყოფილო დარჩებით</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="service-box d-flex">
                        <div class="sr-img" style="height: 70px;">
                            <img src="{{url("/themes/xemart")}}/images/service-3.png" alt="">
                        </div>
                        <div class="">
                            <h6>უსაფრთხო გადახდის სისტემა</h6>
                            <p>ჩვენი ვებ-გვერდი დაცულია, გადახდა საიმედო და უსაფრთხო &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Service Area -->

    <!-- Feature Product Area -->
    <section class="feat-pro2">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12 padding-fix-l20">
                            <div class="ftr-product">
                                <div class="tab-box d-flex justify-content-end">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a href="{{url("/products?limit=300")}}">ყველას ნახვა</a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane fade show active" id="all" role="tabpanel">
                                        <div class="tab-slider owl-carousel">
                                            @foreach($newProducts as $product)
                                                <?php
                                                $prodImg = asset('/assets/images/demo/noimage.jpg');
                                                if (count($product['productFile']) > 0) {
                                                    $prodImg = asset('/files/product_files/' . $product['product_id'] . '/' . $product['productFile'][0]['file_name']);
                                                }
                                                ?>
                                                <div class="tab-item">
                                                    <div class="tab-img">
                                                        <a href="{{asset("/product/".$product['product_id'])}}">
                                                            <img class="main-img img-fluid"
                                                                 src="{{count($product['productFile'])>0?url('/files/product_files/'.$product['product_id'].'/'.$product['productFile'][0]['file_name']):url('/assets/images/demo/noimage.jpg')}}"
                                                                 alt="">
                                                            <img class="sec-img img-fluid"
                                                                 src="{{count($product['productFile'])>0?url('/files/product_files/'.$product['product_id'].'/'.$product['productFile'][0]['file_name']):url('/assets/images/demo/noimage.jpg')}}"
                                                                 alt="">
                                                        </a>
                                                    <!-- <div class="layer-box">
                                                            <a href="#" class="it-comp" data-toggle="tooltip"
                                                               data-placement="left" title="Compare"><img
                                                                        src="{{url("/themes/xemart")}}/images/it-comp.png"
                                                                        alt=""></a>
                                                            <a href="#" class="it-fav" data-toggle="tooltip"
                                                               data-placement="left" title="Favourite"><img
                                                                        src="{{url("/themes/xemart")}}/images/it-fav.png"
                                                                        alt=""></a>
                                                        </div>-->
                                                    </div>
                                                    <div class="tab-heading">
                                                        <p>
                                                            <a href="{{asset("/product/".$product['product_id'])}}">{{$product['name']}}</a>
                                                        </p>
                                                    </div>
                                                    <div class="img-content d-flex justify-content-between">
                                                        <div>
                                                            <ul class="list-unstyled list-inline price">
                                                                <li class="list-inline-item">{{$product['price']}}
                                                                    ლარი
                                                                </li>
                                                                <li></li>
                                                                {{-- <li class="list-inline-item">$150.00</li>--}}
                                                            </ul>
                                                        </div>
                                                        <div>
                                                            <a href="javascript:void(0)" data-toggle="tooltip"
                                                               data-placement="top"
                                                               onclick="addItemToShoppingCart({{$product['product_id']}},'{{$prodImg}}'); return false;"
                                                               title="კალათაში დამატება"><img
                                                                        src="{{url("/themes/xemart")}}/images/it-cart.png"
                                                                        alt=""></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Feature Product Area -->

    <!-- Best Offer -->
    <section class="best-ofr">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="bst-slider">
                        <div class="sec-title">
                            <h6>ყველაზე გაყიდვადი</h6>
                        </div>
                        <div class="bst-body owl-carousel">
                            <div class="bst-items">
                                @foreach($bestSellroducts as $product)
                                    <?php
                                    $prodImg = asset('/assets/images/demo/noimage.jpg');
                                    if (count($product['productFile']) > 0) {
                                        $prodImg = asset('/files/product_files/' . $product['product_id'] . '/' . $product['productFile'][0]['file_name']);
                                    }
                                    ?>
                                    <div class="bst-box d-flex">
                                        <div class="bst-img">
                                            <a href="{{asset("/product/".$product['product_id'])}}">
                                                <img src="{{count($product['productFile'])>0?url('/files/product_files/'.$product['product_id'].'/'.$product['productFile'][0]['file_name']):url('/assets/images/demo/noimage.jpg')}}"
                                                     width="100" alt="" class="img-fluid" style="width: 100px;">
                                            </a>
                                        </div>
                                        <div class="bst-content">
                                            <p>
                                                <a href="{{asset("/product/".$product['product_id'])}}">{{$product['name']}}</a>
                                            </p>
                                            <ul class="list-unstyled list-inline price">
                                                <li class="list-inline-item">{{$product['price']}} ლარი</li>
                                                <li class="list-inline-item"></li>
                                            </ul>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        <!--                            <div class="bst-items">
                                <div class="bst-box d-flex">
                                    <div class="bst-img">
                                        <a href="#"><img src="{{url("/themes/xemart")}}/images/sbar-5.png" alt=""
                                                         class="img-fluid"></a>
                                    </div>
                                    <div class="bst-content">
                                        <p><a href="#">Items Title Name Here</a></p>
                                        <ul class="list-unstyled list-inline fav">
                                            <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                            <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                            <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                            <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                            <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                        </ul>
                                        <ul class="list-unstyled list-inline price">
                                            <li class="list-inline-item">$120.00</li>
                                            <li class="list-inline-item">$150.00</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="bst-slider">
                        <div class="sec-title">
                            <h6>ფასდაკლებები</h6>
                        </div>
                        <div class="bst-body owl-carousel">
                            <div class="bst-items">
                                @foreach($saleProducts as $product)
                                    <?php
                                    $prodImg = asset('/assets/images/demo/noimage.jpg');
                                    if (count($product['productFile']) > 0) {
                                        $prodImg = asset('/files/product_files/' . $product['product_id'] . '/' . $product['productFile'][0]['file_name']);
                                    }
                                    ?>
                                    <div class="bst-box d-flex">
                                        <div class="bst-img">
                                            <a href="{{asset("/product/".$product['product_id'])}}">
                                                <img src="{{count($product['productFile'])>0?url('/files/product_files/'.$product['product_id'].'/'.$product['productFile'][0]['file_name']):url('/assets/images/demo/noimage.jpg')}}"
                                                     width="100" alt="" class="img-fluid" style="width: 100px;">
                                            </a>
                                        </div>
                                        <div class="bst-content">
                                            <p>
                                                <a href="{{asset("/product/".$product['product_id'])}}">{{$product['name']}}</a>
                                            </p>
                                            <ul class="list-unstyled list-inline price">
                                                <li class="list-inline-item" style="color: red">{{$product['price']}}
                                                    ლარი
                                                </li>
                                                <li class="list-inline-item">{{$product['price']*1.2}} ლარი</li>
                                            </ul>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        <!--                            <div class="bst-items">
                                <div class="bst-box d-flex">
                                    <div class="bst-img">
                                        <a href="#"><img src="{{url("/themes/xemart")}}/images/sbar-13.png" alt=""
                                                         class="img-fluid"></a>
                                    </div>
                                    <div class="bst-content">
                                        <p><a href="#">Items Title Name Here</a></p>
                                        <ul class="list-unstyled list-inline fav">
                                            <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                            <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                            <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                            <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                            <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                        </ul>
                                        <ul class="list-unstyled list-inline price">
                                            <li class="list-inline-item">$120.00</li>
                                            <li class="list-inline-item">$150.00</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="bst-slider">
                        <div class="sec-title">
                            <h6>პოპულარული</h6>
                        </div>
                        <div class="bst-body owl-carousel">
                            <div class="bst-items">
                                @foreach($popularSaleProducts as $product)
                                    <?php
                                    $prodImg = asset('/assets/images/demo/noimage.jpg');
                                    if (count($product['productFile']) > 0) {
                                        $prodImg = asset('/files/product_files/' . $product['product_id'] . '/' . $product['productFile'][0]['file_name']);
                                    }
                                    ?>
                                    <div class="bst-box d-flex">
                                        <div class="bst-img">
                                            <a href="{{asset("/product/".$product['product_id'])}}">
                                                <img src="{{count($product['productFile'])>0?url('/files/product_files/'.$product['product_id'].'/'.$product['productFile'][0]['file_name']):url('/assets/images/demo/noimage.jpg')}}"
                                                     width="100" alt="" class="img-fluid" style="width: 100px;">
                                            </a>
                                        </div>
                                        <div class="bst-content">
                                            <p>
                                                <a href="{{asset("/product/".$product['product_id'])}}">{{$product['name']}}</a>
                                            </p>
                                            <ul class="list-unstyled list-inline price">
                                                <li class="list-inline-item">{{$product['price']}} ლარი</li>
                                                <li class="list-inline-item"></li>
                                            </ul>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        <!--                            <div class="bst-items">
                                <div class="bst-box d-flex">
                                    <div class="bst-img">
                                        <a href="#"><img src="{{url("/themes/xemart")}}/images/sbar-3.png" alt=""
                                                         class="img-fluid"></a>
                                    </div>
                                    <div class="bst-content">
                                        <p><a href="#">Items Title Name Here</a></p>
                                        <ul class="list-unstyled list-inline fav">
                                            <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                            <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                            <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                            <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                            <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                        </ul>
                                        <ul class="list-unstyled list-inline price">
                                            <li class="list-inline-item">$120.00</li>
                                            <li class="list-inline-item">$150.00</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Best Offer -->
@stop
