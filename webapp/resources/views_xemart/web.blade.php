<?php

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Auth;

if (Auth::check()) {
    Cart::restore(Auth::user()->email);
    Cart::store(Auth::user()->email);
}
?>
        <!doctype html>
<html class="no-js" lang="zxx">

<!-- Mirrored from www.thetahmid.com/themes/xemart-v1.0/02-home-two.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 15 Dec 2020 19:43:52 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>IOMAX</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{url("/themes/xemart")}}/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="{{url("/themes/xemart")}}/images/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
{{--    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet">--}}

<!-- Bootstrap -->
    <link rel="stylesheet" href="{{url("/themes/xemart")}}/css/assets/bootstrap.min.css">

    <!-- Fontawesome Icon -->
    <link rel="stylesheet" href="{{url("/themes/xemart")}}/css/assets/font-awesome.min.css">

    <!-- Animate Css -->
    <link rel="stylesheet" href="{{url("/themes/xemart")}}/css/assets/animate.css">

    <!-- Owl Slider -->
    <link rel="stylesheet" href="{{url("/themes/xemart")}}/css/assets/owl.carousel.min.css">

    <!-- Custom Style -->
    <link rel="stylesheet" href="{{url("/themes/xemart")}}/css/assets/normalize.css">
    <link rel="stylesheet" href="{{url("/themes/xemart")}}/css/style.css">
    <link rel="stylesheet" href="{{url("/themes/xemart")}}/css/assets/responsive.css">

    <link rel="stylesheet" href="{{url("/themes/xemart")}}/css/assets/toast.min.css">

    <?php
    if(isset($product)){?>
    <?php $prodImg = asset('/assets/images/demo/noimage.jpg');
    if (count($product['productFile']) > 0) {
        $prodImg = asset('/files/product_files/' . $product['product_id'] . '/' . $product['productFile'][0]['file_name']);
    }
    ?>
    <meta property="og:image" content="{{$prodImg}}"/>
    <meta property="og:title" content="<?php echo $product['name']; ?>"/>
    <meta property="og:url" content="<?php echo Request::url(); ?>"/>
    <meta property="og:type" content="article"/>
    <?php
    }else{
    ?>
    <meta property="og:url" content="https://iomax.ge"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="IOMAX"/>
    <meta property="og:description" content="&nbsp;"/>
    <meta property="og:image"
          content="https://iomax.ge/themes/xemart/images/logo_iomax.png"/>
    <?php
    }
    ?>
    <meta property="og:site_name" content="iomax.ge"/>
</head>
<body>

<!-- Preloader -->
<div class="preloader">
    <div class="load-list">
        <div class="load"></div>
        <div class="load load2"></div>
    </div>
</div>
<!-- End Preloader -->

@include('components.header')
@yield('content')

<!-- Brand area 2 -->
<section class="brand2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="tp-bnd owl-carousel">
                    <div class="bnd-items">
                        <a href="#">
                            <img src="{{url("/assets/images/banners")}}/domino.png" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="bnd-items">
                        <a href="#">
                            <img src="{{url("/assets/images/banners")}}/gorgia.png" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="bnd-items">
                        <a href="#">
                            <img src="{{url("/assets/images/banners")}}/saba.jpg" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="bnd-items">
                        <a href="#">
                            <img src="{{url("/assets/images/banners")}}/domino.png" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="bnd-items">
                        <a href="#">
                            <img src="{{url("/assets/images/banners")}}/gorgia.png" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="bnd-items">
                        <a href="#">
                            <img src="{{url("/assets/images/banners")}}/saba.jpg" alt="" class="img-fluid">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Brand area 2 -->

<!-- Footer Area -->
<section class="footer-top">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="f-contact">
                    <h5>საკონტაქტო ინფორმაცია</h5>
                    <div class="f-add">
                        <i class="fa fa-map-marker"></i>
                        <span>მისამართი :</span>
                        <p>{{$params['address_ge']}}</p>
                    </div>
                    <div class="f-email">
                        <i class="fa fa-envelope"></i>
                        <span>ელ. ფოსტა :</span>
                        <p>{{$params['email']}}</p>
                    </div>
                    <div class="f-phn">
                        <i class="fa fa-phone"></i>
                        <span>ტელეფონი :</span>
                        <p><a href="tel:{{$params['mobile_main']}}" style="color: white">{{$params['mobile_main']}}</a>
                        </p>
                    </div>
                    <div class="f-social">
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li class="list-inline-item"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li class="list-inline-item"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li class="list-inline-item"><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="f-cat">
                    <h5>კატეგორიები</h5>
                    <ul class="list-unstyled">
                        <li><a href="#"><i class="fa fa-angle-right"></i>სამზარეულო</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i>ავეჯი</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="f-link">
                    <h5>ლინკები</h5>
                    <ul class="list-unstyled">
                        <li><a href="{{url("/cart")}}"><i class="fa fa-angle-right"></i>კალათა</a></li>
{{--                        <li><a href="#"><i class="fa fa-angle-right"></i>ავტორიზაცია</a></li>--}}
                        <li><a href="{{url("/contact")}}"><i class="fa fa-angle-right"></i>ჩვენი მისამართი</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="f-sup">
                    <h5>დახმარება</h5>
                    <ul class="list-unstyled">
                        <li><a href="#"><i class="fa fa-angle-right"></i>კონტაქტი</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i>წესები და პირობები</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i>კონფიდენციალურობა</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i>დაბრუნების პოლიტიკა</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i>ხ.დ.კ.</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i>ჩვენს შესახებ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="footer-btm">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <p>Copyright &copy; 2020 | Designed With <i class="fa fa-heart"></i> by
                    <a href="http://pixl.ge" target="_blank">www.pixl.ge</a>
                </p>
            </div>
            <div class="col-md-6 text-right">
                <img src="{{url("/themes/xemart")}}/images/payment.png" alt="" class="img-fluid">
            </div>
        </div>
    </div>
    <div class="back-to-top text-center">
        <img src="{{url("/themes/xemart")}}/images/backtotop.png" alt="" class="img-fluid">
    </div>
</section>
<!-- End Footer Area -->



<input type="hidden" value="{{url('/')}}" id="url" name="url">
<!-- =========================================
JavaScript Files
========================================== -->

<!-- jQuery JS -->
<script src="{{url("/themes/xemart")}}/js/assets/vendor/jquery-1.12.4.min.js"></script>

<!-- Bootstrap -->
<script src="{{url("/themes/xemart")}}/js/assets/popper.min.js"></script>
<script src="{{url("/themes/xemart")}}/js/assets/bootstrap.min.js"></script>

<!-- Owl Slider -->
<script src="{{url("/themes/xemart")}}/js/assets/owl.carousel.min.js"></script>

<!-- Wow Animation -->
<script src="{{url("/themes/xemart")}}/js/assets/wow.min.js"></script>

<!-- Price Filter -->
<script src="{{url("/themes/xemart")}}/js/assets/price-filter.js"></script>

<!-- Mean Menu -->
<script src="{{url("/themes/xemart")}}/js/assets/jquery.meanmenu.min.js"></script>

<!-- Custom JS -->
<script src="{{url("/themes/xemart")}}/js/plugins.js"></script>

<script src="{{url("/themes/xemart")}}/js/assets/toast.min.js"></script>

<script src="{{url("/themes/xemart")}}/js/custom.js"></script>
<script src="{{url("/themes/xemart")}}/js/cart.js"></script>

<script>
    $.toastDefaults = {
        position: 'bottom-right', /** top-left/top-right/top-center/bottom-left/bottom-right/bottom-center - Where the toast will show up **/
        dismissible: true, /** true/false - If you want to show the button to dismiss the toast manually **/
        stackable: true, /** true/false - If you want the toasts to be stackable **/
        pauseDelayOnHover: true, /** true/false - If you want to pause the delay of toast when hovering over the toast **/
        style: {
            toast: '', /** Classes you want to apply separated my a space to each created toast element (.toast) **/
            info: '', /** Classes you want to apply separated my a space to modify the "info" type style  **/
            success: '', /** Classes you want to apply separated my a space to modify the "success" type style  **/
            warning: '', /** Classes you want to apply separated my a space to modify the "warning" type style  **/
            error: '', /** Classes you want to apply separated my a space to modify the "error" type style  **/
        }
    };
</script>
</body>

<!-- Mirrored from www.thetahmid.com/themes/xemart-v1.0/02-home-two.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 15 Dec 2020 19:43:56 GMT -->
</html>
