<!-- Top Bar 2 -->
<section class="top-bar2">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="top-left d-flex">
                    <div class="lang-box">
                            <span><img src="{{url("/themes/xemart")}}/images/fl-geo.jpg" alt="">ქართული<i
                                        class="fa fa-angle-down"></i></span>
                        <ul class="list-unstyled">
                            <li><img src="{{url("/themes/xemart")}}/images/fl-geo.jpg" alt="">ქართული</li>
                            <li><img src="{{url("/themes/xemart")}}/images/fl-eng.png" alt="">English</li>
                        </ul>
                    </div>
                    <!--                    <div class="mny-box">
                                            <span>ლარი<i class="fa fa-angle-down"></i></span>
                                            <ul class="list-unstyled">
                                                <li>ლარი</li>
                                                <li>დოლარი</li>
                                            </ul>
                                        </div>-->
                    <div class="call-us">
                        <p>
                            <a href="tel:{{$params['mobile_main']}}">
                                <img src="{{url("/themes/xemart")}}/images/phn.png" alt="">
                                {{$params['mobile_main']}}
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="top-right text-right">
                    <ul class="list-unstyled list-inline">
                        <li class="list-inline-item"><a href="#"><img
                                        src="{{url("/themes/xemart")}}/images/user.png" alt="">ჩემი ანგარიში</a></li>

                        {{--<li class="list-inline-item"><a href="#"><img
                                    src="{{url("/themes/xemart")}}/images/wishlist.png" alt="">Wishlist</a></li>--}}
                        {{--<li class="list-inline-item"><a href="#"><img
                                    src="{{url("/themes/xemart")}}/images/checkout.png" alt="">Checkout</a></li>--}}
                        <li class="list-inline-item"><a href="#"><img
                                        src="{{url("/themes/xemart")}}/images/login.png" alt="">შესვლა</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Top Bar 2 -->

<!-- Logo Area 2 -->
<section class="logo-area2">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="logo">
                    <a href="{{url("/")}}">
                        <img src="{{url("/themes/xemart")}}/images/logo_iomax.png" width="180" style="margin-top: 0px"
                             alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-6 col-md-7 padding-fix">
                <form action="#" class="search-bar d-flex">
                    <input type="text" name="search-bar" placeholder="ძებნა">
                    <div class="search-cat">
                        <select class="form-control scat-id">
                            <option>ყველა კატეგორია</option>
                            @foreach($categories[0]['childrenObjects'] as $category)
                                <option value="{{$category['category_id']}}">{{$category['category_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <div class="col-lg-3 col-md-2">
                <div class="carts-area d-flex">
                    <div class="wsh-box ml-auto">
                        {{--<a href="#" data-toggle="tooltip" data-placement="top" title="Wishlist">
                            <img src="{{url("/themes/xemart")}}/images/heart.png" alt="favorite">
                            <span>0</span>
                        </a>--}}
                    </div>
                    <div class="cart-box ml-4">
                        <a href="#" data-toggle="tooltip" data-placement="top"
                           title="Shopping Cart"
                           class="cart-btn">
                            <img src="{{url("/themes/xemart")}}/images/cart.png" alt="cart">
                            <span id="cartTotalItems">{{Cart::count()}}</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Logo Area 2 -->

<!-- Cart Body -->
<div class="cart-body">
    <div class="close-btn">
        <button class="close-cart"><img src="{{url("/themes/xemart")}}/images/close.png" alt="">დახურვა
        </button>
    </div>
    <div class="crt-bd-box">
        <div class="cart-heading text-center">
            <h5>კალათა</h5>
        </div>
        <div class="cart-content" id="cartContent">
            @foreach(Cart::content() as $key => $value)
                <div class="content-item d-flex justify-content-between">
                    <div class="cart-img">
                        <a href="{{url('/product/'.$value->id)}}">
                            <img src="{{$value->options->img}}" height="50" alt="">
                        </a>
                    </div>
                    <div class="cart-disc">
                        <p><a href="{{url('/product/'.$value->id)}}">{{$value->name}}</a></p>
                        <span>{{$value->qty}} x {{$value->price}}</span>
                    </div>
                    <div class="delete-btn">
                        <a href="javascript:void(0)"
                           onclick="removeItemFromShoppingCart('{{$key}}')">
                            <i class="fa fa-trash-o"></i>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="cart-btm">
            <p class="text-right">ჯამი: <span id="cartSubTotal">{{Cart::subtotal()}}</span></p>
            <a href="{{url("/cart")}}">ყიდვა</a>
        </div>
    </div>
</div>
<div class="cart-overlay"></div>
<!-- End Cart Body -->

<!-- Sticky Menu -->
<section class="sticky-menu">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-3">
                <div class="sticky-logo">
                    <a href="{{url("/")}}">
                        <img src="{{url("/themes/xemart")}}/images/logo_iomax.png" style="height: 80px; margin-top: 0px"
                             alt="" class="img-fluid">
                    </a>
                </div>
            </div>
            <div class="col-lg-7 col-md-7">
                <div class="main-menu">
                    <ul class="list-unstyled list-inline">
                        @foreach($menus as $menu)
                            <li class="list-inline-item"><a
                                        href="{{url($menu['link'])}}">{{$menu['name_ge']}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-2">
                <div class="carts-area d-flex">
                    <div class="src-box">
                        <form action="#">
                            <input type="text" name="search" placeholder="ძებნა">
                            <button type="button" name="button"><i class="fa fa-search"></i>
                            </button>
                        </form>
                    </div>
                <!--                    <div class="wsh-box ml-auto">
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Wishlist">
                            <img src="{{url("/themes/xemart")}}/images/heart.png"
                                 alt="favorite">
                            <span>0</span>
                        </a>
                    </div>-->
                    <div class="cart-box ml-4">
                        <a href="#" data-toggle="tooltip" data-placement="top"
                           title="Shopping Cart"
                           class="cart-btn">
                            <img src="{{url("/themes/xemart")}}/images/cart.png" alt="cart">
                            <span id="cartTotalItems2">{{Cart::count()}}</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Sticky Menu -->

<!-- Menu Area 2 -->
<section class="menu-area2">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-0">
                <div class="sidemenu">
                    <p>კატეგორიები <i class="fa fa-bars"></i></p>
                    <ul class="list-unstyled gt-menu">
                        @foreach($categories[0]['childrenObjects'] as $category)
                            <li>
                                <a href="{{url('/products?categoryId='.$category['category_id'])}}">{{$category['category_name']}}</a>
                                @if(count($category['childrenObjects'])>0)
                                    <div class="mega-menu">
                                        <div class="row">
                                            @foreach($category['childrenObjects'] as $subCat)
                                                @if($loop->index==0 || $loop->index==12)
                                                    <div class="col-md-6">
                                                        @endif
                                                        <a href="{{url('/products?categoryId='.$subCat['category_id'])}}">
                                                            - {{$subCat['category_name']}}</a>
                                                        @if($loop->index==11 || $loop->index==(count($category['childrenObjects'])-1))
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-lg-9 col-md-12">
                <div class="main-menu">
                    <ul class="list-unstyled list-inline">
                        @foreach($menus as $menu)
                            <li class="list-inline-item"><a
                                        href="{{url($menu['link'])}}">{{$menu['name_ge']}}</a>
                            </li>
                        @endforeach

                        <li class="list-inline-item cup-btn">
                            <a href="https://iomax.ge/page/31">
                                ონლაინ განვადება
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Menu Area 2 -->

<!-- Mobile Menu -->
<section class="mobile-menu-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mobile-menu">
                    <nav id="dropdown">
                        <a href="{{url("/")}}">
                            <img src="{{url("/themes/xemart")}}/images/logo_iomax.png" alt="" style="margin-top: 0px;">
                        </a>
                        {{--                        <a href="#"><span>Sign In</span></a>--}}
                        <ul class="list-unstyled">
                            @foreach($menus as $menu)
                                <li>
                                    <a href="{{url($menu['link'])}}">{{$menu['name_ge']}}</a>
                                </li>
                            @endforeach
                            <li>
                                <a href="{{url("/cart")}}">
                                    <b>
                                        <span class="fa fa-shopping-cart" style="margin-top: 0px;"></span>
                                        კალათა
                                    </b>
                                </a>
                            </li>
                            @foreach($categories[0]['childrenObjects'] as $category)
                                <li>
                                    <a href="{{url('/products?categoryId='.$category['category_id'])}}">- {{$category['category_name']}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Mobile Menu -->
