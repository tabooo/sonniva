@extends('web')
@section('content')
    <!-- Full Slider -->
    <div class="container-fluid">
        <div class="row">
            <div class="home-slider">
                @foreach($slideshows as $slideshow)
                    <div class="item">
                        <a href="#">
                            <img src="{{$slideshow['img_path']}}"
                                 alt="Slider"></a>
                    </div>
                @endforeach

                <div class="item">
                    <a href="#"><img src="{{$slideshow['img_path']}}"
                                     alt="Slider"></a>
                </div>
                <div class="item">
                    <a href="#"><img src="{{$slideshow['img_path']}}"
                                     alt="Slider"></a>
                </div>
            </div>
        </div>
    </div>
    <!-- End Full Slider -->
    <!--Custom content-->
    <!-- Main Content -->
    <div class="container m-t-2">
        <div class="row">

            <!-- New Arrivals & Best Selling -->
            <div class="col-md-3 m-b-1">
                <div class="title"><span><a href="#l">ახალი მიღებული <i
                                    class="fa fa-chevron-circle-right"></i></a></span>
                </div>
                <div class="widget-slider owl-controls-top-offset m-b-2">
                    @foreach($newProducts as $product)
                        <?php $prodImg = asset('/assets/images/demo/noimage.jpg'); ?>
                        @if(count($product['productFile'])>0)
                            <?php $prodImg = asset('/files/product_files/' . $product['product_id'] . '/' . $product['productFile'][0]['file_name']);?>
                        @endif
                        <div class="box-product-outer">
                            <div class="box-product">
                                <div class="img-wrapper">
                                    <a href="{{asset("/product/".$product['product_id'])}}">
                                        <img alt="Product"
                                             src="{{count($product['productFile'])>0?asset('/files/product_files/'.$product['product_id'].'/'.$product['productFile'][0]['file_name']):asset('/assets/images/demo/noimage.jpg')}}">
                                    </a>

                                    <div class="tags tags-left">
                                <span class="label-tags"><a href="#"><span
                                                class="label label-success arrowed-right">New</span></a></span>
                                    </div>
                                    <div class="option">
                                        <a href="#" data-toggle="tooltip" title="Add to Cart"
                                           onclick="addItemToShoppingCart({{$product['product_id']}},'{{$prodImg}}'); return false;"><i
                                                    class="fa fa-shopping-cart"></i></a>
                                        {{--<a href="#" data-toggle="tooltip" title="Add to Compare"><i
                                                    class="fa fa-align-left"></i></a>
                                        <a href="#" data-toggle="tooltip" title="Add to Wishlist" class="wishlist"><i
                                                    class="fa fa-heart"></i></a>--}}
                                    </div>
                                </div>
                                <h6><a href="{{asset("/product/".$product['product_id'])}}l">{{$product['name']}}</a>
                                </h6>

                                <div class="price">
                                    <div>{{$product['price']}} GEL</div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="title"><span><a href="#">ყველაზე გაყიდვადი <i
                                    class="fa fa-chevron-circle-right"></i></a></span>
                </div>
                <div class="widget-slider owl-controls-top-offset">
                    @foreach($bestSellroducts as $product)
                        <?php $prodImg = asset('/assets/images/demo/noimage.jpg'); ?>
                        @if(count($product['productFile'])>0)
                            <?php $prodImg = asset('/files/product_files/' . $product['product_id'] . '/' . $product['productFile'][0]['file_name']);?>
                        @endif
                        <div class="box-product-outer">
                            <div class="box-product">
                                <div class="img-wrapper">
                                    <a href="{{asset("/product/".$product['product_id'])}}">
                                        <img alt="Product"
                                             src="{{count($product['productFile'])>0?asset('/files/product_files/'.$product['product_id'].'/'.$product['productFile'][0]['file_name']):asset('/assets/images/demo/noimage.jpg')}}">
                                    </a>

                                    <div class="tags tags-left">
                                    <span class="label-tags">
                                        <span class="label label-primary arrowed-right">Popular</span></span>
                                        <span class="label-tags">
                                            <span class="label label-default arrowed-right">Top Week</span>
                                        </span>
                                    </div>
                                    <div class="option">
                                        <a href="#" data-toggle="tooltip" title="Add to Cart"
                                           onclick="addItemToShoppingCart({{$product['product_id']}},'{{$prodImg}}'); return false;"><i
                                                    class="fa fa-shopping-cart"></i></a>
                                        {{--<a href="#" data-toggle="tooltip" title="Add to Compare"><i
                                                    class="fa fa-align-left"></i></a>
                                        <a href="#" data-toggle="tooltip" title="Add to Wishlist" class="wishlist"><i
                                                    class="fa fa-heart"></i></a>--}}
                                    </div>
                                </div>
                                <h6><a href="{{asset("/product/".$product['product_id'])}}">{{$product['name']}}</a>
                                </h6>

                                <div class="price">
                                    <div>{{$product['price']}} GEL</div>
                                </div>
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half-o"></i>
                                    <a href="#">(0 შეფასება)</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <!-- End New Arrivals & Best Selling -->

            <div class="clearfix visible-sm visible-xs"></div>

            <div class="col-md-9">

                <div class="title"><span>ფასდაკლებული</span></div>
                <?php $it = 1; ?>
                @foreach($saleProducts as $product)
                    <?php $prodImg = asset('/assets/images/demo/noimage.jpg'); ?>
                    @if(count($product['productFile'])>0)
                        <?php $prodImg = asset('/files/product_files/' . $product['product_id'] . '/' . $product['productFile'][0]['file_name']);?>
                    @endif
                    <div class="col-sm-4 col-lg-3 <?php
                    if ($it == 1) {
                        echo "box-product-outer";
                    } else if ($it == 2 || $it == 3) {
                        echo "hidden-xs box-product-outer";
                    } else {
                        echo "visible-lg box-product-outer";
                    }
                    $it++;
                    ?>">
                        <div class="box-product">
                            <div class="img-wrapper" style="height: 273px">
                                <a href="{{asset("/product/".$product['product_id'])}}" style="height: 100%">
                                    <img style="height: 100%" alt="Product"
                                         src="{{count($product['productFile'])>0?asset('/files/product_files/'.$product['product_id'].'/'.$product['productFile'][0]['file_name']):asset('/assets/images/demo/noimage.jpg')}}">
                                </a>

                                <div class="tags tags-left">
                                <span class="label-tags">
                                    <span class="label label-success arrowed-right">საწყობში</span>
                                </span>
                                </div>
                                <div class="option">
                                    <a href="#" data-toggle="tooltip" title="Add to Cart"
                                       onclick="addItemToShoppingCart({{$product['product_id']}},'{{$prodImg}}'); return false;"><i
                                                class="fa fa-shopping-cart"></i></a>
                                    {{--<a href="#" data-toggle="tooltip" title="Add to Compare"><i
                                                class="fa fa-align-left"></i></a>
                                    <a href="#" data-toggle="tooltip" title="Add to Wishlist" class="wishlist"><i
                                                class="fa fa-heart"></i></a>--}}
                                </div>
                            </div>
                            <h6><a href="{{asset("/product/".$product['product_id'])}}">{!! $product['name'] !!}</a>
                            </h6>

                            <div class="price">
                                <div>{!! $product['price'] !!} GEL</div>
                            </div>
                            <div class="rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-half-o"></i>
                                <a href="#">(0 შეფასება)</a>
                            </div>
                        </div>
                    </div>
                @endforeach

                <div class="clearfix visible-sm visible-xs"></div>

                <div class="title m-t-2"><span>პოპულარული</span></div>
                <div class="product-slider owl-controls-top-offset">
                    @foreach($popularSaleProducts as $product)
                        <?php $prodImg = asset('/assets/images/demo/noimage.jpg'); ?>
                        @if(count($product['productFile'])>0)
                            <?php $prodImg = asset('/files/product_files/' . $product['product_id'] . '/' . $product['productFile'][0]['file_name']);?>
                        @endif
                        <div class="box-product-outer">
                            <div class="box-product">
                                <div class="img-wrapper">
                                    <a href="{{asset("/product/".$product['product_id'])}}">
                                        <img alt="Product"
                                             src="{{count($product['productFile'])>0?asset('/files/product_files/'.$product['product_id'].'/'.$product['productFile'][0]['file_name']):asset('/assets/images/demo/noimage.jpg')}}">
                                    </a>

                                    <div class="tags">
                                        <span class="label-tags">
                                            <span class="label label-default arrowed">პოპულარული</span>
                                        </span>
                                    </div>
                                    <div class="tags tags-left">
                                        <span class="label-tags">
                                            <span class="label label-success arrowed-right">საწყობში</span>
                                        </span>
                                    </div>
                                    <div class="option">
                                        <a href="#" data-toggle="tooltip" title="Add to Cart"
                                           onclick="addItemToShoppingCart({{$product['product_id']}},'{{$prodImg}}'); return false;"><i
                                                    class="fa fa-shopping-cart"></i></a>
                                        {{--<a href="#" data-toggle="tooltip" title="Add to Compare"><i
                                                    class="fa fa-align-left"></i></a>
                                        <a href="#" data-toggle="tooltip" title="Add to Wishlist" class="wishlist"><i
                                                    class="fa fa-heart"></i></a>--}}
                                    </div>
                                </div>
                                <h6><a href="{{asset("/product/".$product['product_id'])}}">{!! $product['name'] !!}</a>
                                </h6>

                                <div class="price">
                                    <div>{!! $product['price'] !!} GEL</div>
                                </div>
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half-o"></i>
                                    <i class="fa fa-star-o"></i>
                                    <a href="#">(0 შეფასება)</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>

        </div>

        <!-- Brand & Clients -->
        <div class="row">
            <div class="col-xs-12 m-t-1">
                <div class="title text-center"><span>Brand & Clients</span></div>
                <div class="brand-slider owl-controls-top-offset">
                    <div class="brand">
                        <a href="#"><img src="{{asset('/assets/images/bosch.png')}}" alt=""></a>
                    </div>
                    <div class="brand">
                        <a href="#"><img src="{{asset('/assets/images/crown.jpg')}}" alt=""></a>
                    </div>
                    <div class="brand">
                        <a href="#"><img src="{{asset('/assets/images/linus.png')}}" alt=""></a>
                    </div>
                    <div class="brand">
                        <a href="#"><img src="{{asset('/assets/images/samet.png')}}" alt=""></a>
                    </div>
                    <div class="brand">
                        <a href="#"><img src="{{asset('/assets/images/tolsen.png')}}" alt=""></a>
                    </div>
                    <div class="brand">
                        <a href="#"><img src="{{asset('/assets/images/bosch.png')}}" alt=""></a>
                    </div>
                    <div class="brand">
                        <a href="#"><img src="{{asset('/assets/images/crown.jpg')}}" alt=""></a>
                    </div>
                    <div class="brand">
                        <a href="#"><img src="{{asset('/assets/images/linus.png')}}" alt=""></a>
                    </div>
                    <div class="brand">
                        <a href="#"><img src="{{asset('/assets/images/samet.png')}}" alt=""></a>
                    </div>
                    <div class="brand">
                        <a href="#"><img src="{{asset('/assets/images/tolsen.png')}}" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Brand & Clients -->

    </div>
    <!-- End Main Content -->
@stop