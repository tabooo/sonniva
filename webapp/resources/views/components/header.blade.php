<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{asset('/favicon.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('/favicon.ico')}}" type="image/x-icon">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>IOMAX</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap -->
    <link href="{{asset('/assets/bootstrap/css/bootstrap.css')}}" rel="stylesheet">
    <!-- Plugins -->
    <link href="{{asset('/assets/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('/assets/css/bootstrap-select.css')}}" rel="stylesheet">
    <link href="{{asset('/assets/css/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('/assets/css/owl.theme.default.css')}}" rel="stylesheet">
    <link href="{{asset('/assets/css/jquery.bootstrap-touchspin.css')}}" rel="stylesheet" id="theme">
    <link href="{{asset('/assets/css/style.teal.flat.css')}}" rel="stylesheet" id="theme">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!-- Top Header -->
<div class="top-header">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul class="list-inline pull-left">
                    <li>
                        <select class="selectpicker" data-width="115px" data-style="btn-default">
                            <option value="GEO"
                                    data-content="<img alt='French' src='{{asset('/assets/images/geo.jpg')}}'> ქართული">
                                ქართული
                            </option>
                            <option value="ENG"
                                    data-content="<img alt='English' src='{{asset('/assets/images/en.jpg')}}'> English">
                                English
                            </option>
                            <option value="RUS"
                                    data-content="<img alt='French' src='{{asset('/assets/images/ru.jpg')}}'> Русский">
                                Русский
                            </option>
                        </select>
                    </li>
                    {{--<li>
                        <select class="selectpicker" data-width="70px" data-style="btn-default">
                            <option value="USD"> GEL</option>
                            <option value="USD">$ USD</option>
                            <option value="EUR">€ EUR</option>
                        </select>
                    </li>--}}
                    <li class="hidden-xs"><a href="#"><i class="fa fa-phone"></i> {{$params['mobile_main']}}</a></li>
                    <li class="hidden-xs"><a href="mailto:{{$params['email']}}"><i
                                    class="fa fa-envelope"></i> {{$params['email']}}</a></li>
                </ul>
                <ul class="list-inline pull-right">
                    <li>
                        <div class="dropdown">
                            @if(!Auth::check())
                                <button class="btn dropdown-toggle" type="button" id="dropdownLogin"
                                        data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="true">
                                    შესვლა <span class="caret"></span>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-login"
                                     aria-labelledby="dropdownLogin" data-dropdown-in="zoomIn"
                                     data-dropdown-out="fadeOut">
                                    {{--<form>--}}
                                    <div class="form-group">
                                        <label for="username">მომხმარებელი</label>
                                        <input type="text" class="form-control" id="username"
                                               placeholder="მომხმარებელი">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">პაროლი</label>
                                        <input type="password" class="form-control" id="password"
                                               placeholder="პაროლი">
                                    </div>
                                    {{--<div class="checkbox">--}}
                                    {{--<label>--}}
                                    {{--<input type="checkbox"><span> დამიმახსოვრე</span>--}}
                                    {{--</label>--}}
                                    {{--</div>--}}
                                    <button type="button" class="btn btn-default btn-sm" onclick="loginUser()">
                                        <i class="fa fa-long-arrow-right"></i> შესვლა
                                    </button>
                                    <a class="btn btn-default btn-sm pull-right" href="{{url("/register")}}"
                                       role="button">რეგისტრაცია</a>
                                    {{--</form>--}}
                                </div>
                            @else
                                <button class="btn dropdown-toggle" type="button" id="dropdownLogin"
                                        data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="true">
                                    {{Auth::user()->email}} <span class="caret"></span>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-login"
                                     aria-labelledby="dropdownLogin" data-dropdown-in="zoomIn"
                                     data-dropdown-out="fadeOut">
                                    <ul>
                                        <li><a href="{{asset('/user/profile')}}">პროფილი</a></li>
                                        <li><a href="#" onclick="logoutUser(); return false;">გასვლა</a></li>
                                    </ul>
                                </div>
                            @endif
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Top Header -->

<!-- Middle Header -->
<div class="middle-header">
    <div class="container">
        <div class="row">
            <div class="col-md-3 logo">
                <a href="{{asset("/")}}">
                    <img alt="Logo" src="{{asset('/assets/images/logo-teal.png')}}"
                         class="img-responsive"/>
                </a>
            </div>
            <div class="col-sm-8 col-md-6 search-box m-t-2">
                <form action="{{url('/products')}}" , method="get">
                    <div class="input-group">
                        <input type="text" name="name" class="form-control" aria-label="Search here..."
                               placeholder="საძიები სიტყვა...">

                        <div class="input-group-btn">
                            <select name="categoryId" class="selectpicker hidden-xs" data-width="150px">
                                <option value="0">–ყველა კატეგორია–</option>
                                @foreach($categories[0]['childrenObjects'] as $category)
                                    <option value="{{$category['category_id']}}">{{$category['category_name']}}</option>
                                @endforeach
                            </select>
                            <button type="submit" class="btn btn-default btn-search"><i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-sm-4 col-md-3 cart-btn hidden-xs m-t-2">
                <button type="button" class="btn btn-default dropdown-toggle" id="dropdown-cart" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="true">
                    <i class="fa fa-shopping-cart"></i> კალათა : <span id="cartTotalItems">{{Cart::count()}}</span>
                    პროდუქტი <i
                            class="fa fa-caret-down"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-cart">
                    <div id="cartContent">
                        @foreach(Cart::content() as $key => $value)
                            <div class="media">
                                <div class="media-left">
                                    <a href="#">
                                        <img class="media-object img-thumbnail" src="{{$value->options->img}}"
                                             width="50" alt="product">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <a href="#" class="media-heading">{{$value->name}}</a>

                                    <div>x{{$value->qty}} {{$value->subtotal}}</div>
                                </div>
                                <div class="media-right">
                                    <a href="#" data-toggle="tooltip" title="Remove"
                                       onclick="removeItemFromShoppingCart('{{$key}}')">
                                        <i class="fa fa-remove"></i>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="subtotal-cart">სულ თანხა: <span id="cartSubTotal">{{Cart::subtotal()}}</span></div>
                    <div class="text-center">
                        <div class="btn-group" role="group" aria-label="View Cart and Checkout Button">
                            <a href="{{url('/cart')}}">
                                <button class="btn btn-default btn-sm" type="button">
                                    <i class="fa fa-shopping-cart"></i> კალათის ნახვა
                                </button>
                            </a>
                            {{--<button class="btn btn-default btn-sm" type="button">--}}
                            {{--<i class="fa fa-check"></i> შეკვეთა--}}
                            {{--</button>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Middle Header -->

<!-- Navigation Bar -->
<nav class="navbar navbar-default shadow-navbar" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="btn btn-default btn-cart-xs visible-xs pull-right">
                <i class="fa fa-shopping-cart"></i> Cart : 4 items
            </a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li><a href="{{asset("/")}}">მთავარი</a></li>
                <li class="dropdown">
                    <a href="{{asset("/products")}}" class="dropdown-toggle"
                       role="button" aria-haspopup="true"
                       aria-expanded="false">პროდუქცია <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        @foreach($categories[0]['childrenObjects'] as $category)
                            @if(count($category['childrenObjects'])>0)
                                <li class="dropdown dropdown-submenu">
                                    <a href="{{url('/products?categoryId='.$category['category_id'])}}">{{$category['category_name']}}</a>
                                    <ul class="dropdown-menu">
                                        @foreach($category['childrenObjects'] as $subCat)
                                            @if(count($subCat['childrenObjects'])>0)
                                                <li class="dropdown dropdown-submenu">
                                                    <a href="{{url('/products?categoryId='.$subCat['category_id'])}}">{{$subCat['category_name']}}</a>
                                                    <ul class="dropdown-menu">
                                                        @foreach($subCat['childrenObjects'] as $subSubCat)
                                                            <li>
                                                                <a href="{{url('/products?categoryId='.$subSubCat['category_id'])}}">{{$subSubCat['category_name']}}</a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </li>
                                            @else
                                                <li>
                                                    <a href="{{url('/products?categoryId='.$subCat['category_id'])}}">{{$subCat['category_name']}}</a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </li>
                            @else
                                <li>
                                    <a href="{{url('/products?categoryId='.$category['category_id'])}}">{{$category['category_name']}}</a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </li>
                @foreach($menus as $menu)
                    @if(count($menu['children'])>0)
                        <li class="dropdown">
                            <a href="{{asset($menu['link'])}}" class="dropdown-toggle"
                               role="button" aria-haspopup="true"
                               aria-expanded="false">{{$menu['name_ge']}}</a>

                            <ul class="dropdown-menu">
                                @foreach($menu['children'] as $subMenu)
                                    @if(count($subMenu['children'])>0)
                                        <li class="dropdown dropdown-submenu">
                                            <a href="{{asset($subMenu['link'])}}">{{$subMenu['name_ge']}}</a>
                                            <ul class="dropdown-menu">
                                                @foreach($subMenu['children'] as $subSubMenu)
                                                    <li>
                                                        <a href="{{asset($subSubMenu['link'])}}">{{$subSubMenu['name_ge']}}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @else
                                        <li><a href="{{asset($subMenu['link'])}}">{{$subMenu['name_ge']}}</a></li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                    @else
                        <li><a href="{{asset($menu['link'])}}">{{$menu['name_ge']}}</a></li>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
</nav>
<!-- End Navigation Bar -->
