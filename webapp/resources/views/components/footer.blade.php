<!-- Footer -->
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="title-footer"><span>კონტაქტი</span></div>
                <ul class="footer-icon">
                    <li><span><i class="fa fa-map-marker"></i></span> {{$params['address_ge']}}</li>
                    <li><span><i class="fa fa-phone"></i></span> {{$params['mobile_main']}}</li>
                    <li><span><i class="fa fa-envelope"></i></span> <a
                                href="mailto:{{$params['email']}}">{{$params['email']}}</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="title-footer"><span>სოციალური ქსელები</span></div>
                <p>გამოიწერეთ ჩვენი ოფიციალური სოციალური გვერდები</p>
                <ul class="follow-us">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                </ul>
            </div>
            <div class="clearfix visible-sm-block"></div>
            <div class="col-md-3 col-sm-6">
                <div class="title-footer"><span>გადახდის მეთოდები</span></div>
                <p>ანგარიშსწორების მეთოდები: ვიზა, მასტერქარდი, ფეიფალი, ჩასარიცხი აპარატები, საბანკი გადარიცხვა</p>
                <img src="{{asset('/assets/images/payment-1.png')}}" alt="Payment-1">
                <img src="{{asset('/assets/images/payment-2.png')}}" alt="Payment-2">
                <img src="{{asset('/assets/images/payment-3.png')}}" alt="Payment-3">
                <img src="{{asset('/assets/images/payment-4.png')}}" alt="Payment-4">
                <img src="{{asset('/assets/images/payment-5.png')}}" alt="Payment-5">
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="title-footer"><span>სიახლეების გამოწერა</span></div>
                <p>მიუთითეთ თქვენი ელ. ფოსტა და თქვენ მიიღებთ მნიშვნელოვან სიახლეებს და პროდუქციას თქვენ ელ ფოსტაზე</p>

                <div class="input-group">
                    <input class="form-control" type="text" placeholder="ელ. ფოსტა">
              <span class="input-group-btn">
                <button class="btn btn-default subscribe-button" type="button">გამოწერა</button>
              </span>
                </div>
            </div>
        </div>
    </div>
    <div class="text-center copyright">
        Copyright &copy; 2017 <a href="http://pixl.ge">PIXL.GE</a>
    </div>
</div>
<!-- End Footer -->

<a href="#top" class="back-top text-center" onclick="$('body,html').animate({scrollTop:0},500); return false">
    <i class="fa fa-angle-double-up"></i>
</a>
<input type="hidden" value="{{url('/')}}" id="url" name="url">

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{asset('/assets/js/jquery.js')}}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{asset('/assets/bootstrap/js/bootstrap.js')}}"></script>
<!-- Plugins -->
<script src="{{asset('/assets/js/bootstrap-select.js')}}"></script>
<script src="{{asset('/assets/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('/assets/js/jquery.ez-plus.js')}}"></script>
<script src="{{asset('/assets/js/jquery.bootstrap-touchspin.js')}}"></script>
<script src="{{asset('/assets/js/jquery.raty-fa.js')}}"></script>
<script src="{{asset('/assets/js/mimity.js')}}"></script>
<script src="{{asset('/assets/js/mimity.detail.js')}}"></script>

<script src="{{asset('/js/cart.js')}}"></script>
<script src="{{asset('/js/loginUser.js')}}"></script>
<script src="{{asset('/js/product.js')}}"></script>
</body>
</html>