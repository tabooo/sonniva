<!DOCTYPE html>
<head>
    <meta charset="utf-8"/>
    <title>ავტორიზაცია</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>

    <link href="{{asset("css/login/bootstrap.min.css")}}" rel="stylesheet">
    <link href="{{asset("css/login/components.min.css")}}" rel="stylesheet">
    <link href="{{asset("css/login/login.min.css")}}" rel="stylesheet">

</head>
<body class="login">

<div class="user-login-5">
    <div class="row bs-reset">
        <div class="col-md-6 bs-reset">
            <div class="login-bg"
                 style="background-image:url(dashboard/resources/images/bg1.jpg)"></div>
        </div>
        <div class="col-md-6 login-container bs-reset">
            <div class="login-content">
                <h1>ავტორიზაცია</h1>
                <form action="javascript:;" class="login-form" method="post">
                    <div class="alert alert-danger display-hide">
                        <span> შეავსეთ ელ-ფოსტის და პაროლის ველები.</span>
                    </div>

                    <div class="alert alert-danger-login display-hide">
                        <span> ელ-ფოსტა ან პაროლი არასწორია.</span>
                    </div>

                    <div class="row">
                        <div class="col-xs-6">
                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="email"
                                   autocomplete="off" placeholder="ელ-ფოსტა" name="username" required autofocus/></div>
                        <div class="col-xs-6">
                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="password"
                                   autocomplete="off" placeholder="პაროლი" name="password" required/></div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                    <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-8 text-right">
                            <button class="btn green" type="submit">შესვლა</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="{{asset("js/localStorage.js")}}" type="text/javascript"></script>
<script src="{{asset("js/login/jquery.min.js")}}" type="text/javascript"></script>
<script src="{{asset("js/login/jquery.validate.min.js")}}" type="text/javascript"></script>
<script src="{{asset("js/login/login.js")}}" type="text/javascript"></script>

</body>
</html>
