@extends('web')
@section('content')
    <!-- Breadcrumbs -->
    <div class="breadcrumb-container">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{asset("/")}}">მთავარი</a></li>
                <li><a href="{{asset("/user/profile")}}">ჩემი პროფილი</a></li>
                <li class="active">შეკვეთების ისტორია</li>
            </ol>
        </div>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Main Content -->
    <div class="container m-t-3">
        <div class="row">

            <!-- Account Sidebar -->
            <div class="col-sm-4 col-md-3 m-b-3">
                <div class="account-picture">
                    <img src="{{asset('/assets/images/demo/user.png')}}" alt="" class="img-circle img-responsive">
                </div>
                <h4 class="text-center m-b-3">{{$user['name']}} {{$user['last_name']}}</h4>
                <ul class="nav nav-pills nav-stacked">
                    <li role="presentation"><a href="{{asset('/user/profile')}}">ჩემი პროფილი</a></li>
                    <li role="presentation"><a href="{{asset('/user/address')}}">ჩემი მისამართი</a>
                    </li>
                    <li role="presentation" class="active"><a href="{{asset('/user/purchaseHistory')}}">შეკვეთების
                            ისტორია</a></li>
                    <li role="presentation"><a href="{{asset('/user/password')}}">პაროლის შეცვლა</a></li>
                </ul>
            </div>
            <!-- End Account Sidebar -->

            <!-- My Profile Content -->
            <div class="col-sm-8 col-md-9">
                <div class="title m-b-2"><span>შეკვეთების ისტორია</span></div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <td>შეკვეთის ნომერი</td>
                                    <td>პროდუქტი</td>
                                    <td>თარიღი</td>
                                    <td class="text-right">ჯამი</td>
                                    <td class="text-center">სტატუსი</td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><a href="#"><span class="badge">12345</span></a></td>
                                    <td>2 items</td>
                                    <td>2016-12-19</td>
                                    <td class="text-right">GEL 74.00</td>
                                    <td class="text-center"><span class="label label-warning">Processing</span></td>
                                </tr>
                                <tr>
                                    <td><a href="#"><span class="badge">65678</span></a></td>
                                    <td>3 items</td>
                                    <td>2016-12-10</td>
                                    <td class="text-right">GEL 100.00</td>
                                    <td class="text-center"><span class="label label-danger">Canceled</span></td>
                                </tr>
                                <tr>
                                    <td><a href="#"><span class="badge">13579</span></a></td>
                                    <td>1 item</td>
                                    <td>2016-12-01</td>
                                    <td class="text-right">GEL 20.00</td>
                                    <td class="text-center"><span class="label label-success">Finished</span></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End My Profile Content -->

        </div>
    </div>
    <!-- End Main Content -->
@stop