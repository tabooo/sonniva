@extends('web')
@section('content')
    <!-- Breadcrumbs -->
    <div class="breadcrumb-container">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{asset("/")}}">მთავარი</a></li>
                <li><a href="{{asset("/user/profile")}}">ჩემი პროფილი</a></li>
                <li class="active">პაროლის შეცვლა</li>
            </ol>
        </div>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Main Content -->
    <div class="container m-t-3">
        <div class="row">

            <!-- Account Sidebar -->
            <div class="col-sm-4 col-md-3 m-b-3">
                <div class="account-picture">
                    <img src="{{asset('/assets/images/demo/user.png')}}" alt="" class="img-circle img-responsive">
                </div>
                <h4 class="text-center m-b-3">{{$user['name']}} {{$user['last_name']}}</h4>
                <ul class="nav nav-pills nav-stacked">
                    <li role="presentation"><a href="{{asset('/user/profile')}}">ჩემი პროფილი</a></li>
                    <li role="presentation"><a href="{{asset('/user/address')}}">ჩემი მისამართი</a></li>
                    <li role="presentation"><a href="{{asset('/user/purchaseHistory')}}">შეკვეთების ისტორია</a></li>
                    <li role="presentation" class="active"><a href="{{asset('/user/password')}}">პაროლის შეცვლა</a>
                    </li>
                </ul>
            </div>
            <!-- End Account Sidebar -->

            <!-- My Profile Content -->
            <div class="col-sm-8 col-md-9">
                <div class="title m-b-2"><span>პაროლის შეცვლა</span></div>
                <div class="row">
                    <div class="col-xs-12">
                        <form>
                            <div class="form-group">
                                <label for="oldInputPasswd">ძველი პაროლი</label>
                                <input type="password" class="form-control" id="oldInputPasswd"
                                       placeholder="ძველი პაროლი">
                            </div>
                            <div class="form-group">
                                <label for="newInputPasswd">ახალი პაროლი</label>
                                <input type="password" class="form-control" id="newInputPasswd"
                                       placeholder="ახალი პაროლი">
                            </div>
                            <div class="form-group">
                                <label for="retypeInputPasswd">გაიმეორეთ ახალი პაროლი</label>
                                <input type="password" class="form-control" id="retypeInputPasswd"
                                       placeholder="გაიმეორეთ ახალი პაროლი">
                            </div>
                            <button type="submit" class="btn btn-default btn-theme"><i class="fa fa-check"></i>
                                დამახსოვრება
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- End My Profile Content -->

        </div>
    </div>
    <!-- End Main Content -->
@stop