@extends('web')
@section('content')
    <!-- Breadcrumbs -->
    <div class="breadcrumb-container">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{asset("/")}}">მთავარი</a></li>
                <li><a href="{{asset("/user/profile")}}">ჩემი პროფილი</a></li>
                <li class="active">მისამართი</li>
            </ol>
        </div>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Main Content -->
    <div class="container m-t-3">
        <div class="row">

            <!-- Account Sidebar -->
            <div class="col-sm-4 col-md-3 m-b-3">
                <div class="account-picture">
                    <img src="{{asset('/assets/images/demo/user.png')}}" alt="" class="img-circle img-responsive">
                </div>
                <h4 class="text-center m-b-3">{{$user['name']}} {{$user['last_name']}}</h4>
                <ul class="nav nav-pills nav-stacked">
                    <li role="presentation"><a href="{{asset('/user/profile')}}">ჩემი პროფილი</a></li>
                    <li role="presentation" class="active"><a href="{{asset('/user/address')}}">ჩემი მისამართი</a>
                    </li>
                    <li role="presentation"><a href="{{asset('/user/purchaseHistory')}}">შეკვეთების ისტორია</a></li>
                    <li role="presentation"><a href="{{asset('/user/password')}}">პაროლის შეცვლა</a></li>
                </ul>
            </div>
            <!-- End Account Sidebar -->

            <!-- My Profile Content -->
            <div class="col-sm-8 col-md-9">
                <div class="title m-b-2"><span>ჩემი მისამართი</span></div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <td>მისამართი</td>
                                    <td>ფასი (GEL)</td>
                                    <td>მიტანის დრო (დღე)</td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($addresses as $address)
                                    <tr>
                                        <td>საქართველო, {{$address['city']['name_ge']}}, {{$address['address']}}</td>
                                        <td>{{$address['city']['price']}}</td>
                                        <td>{{$address['city']['delivery_time']}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{--<ul class="list-group list-group-nav">
                            <li class="list-group-item">
                                <strong>მისამართი</strong>
                                <p>მ. კოსტავას 21</p>
                            </li>
                            <li class="list-group-item">
                                <strong>ქვეყანა</strong>
                                <p>საქართველო</p>
                            </li>
                            <li class="list-group-item">
                                <strong>რეგიონი</strong>
                                <p>ქვემო ქართლი</p>
                            </li>
                            <li class="list-group-item">
                                <strong>ქალაქი</strong>
                                <p>რუსთავი</p>
                            </li>
                            <li class="list-group-item">
                                <strong>საფოსტო ინდექსი</strong>
                                <p>3700</p>
                            </li>
                        </ul>
                        <a href="#" class="btn btn-theme pull-right"><i class="fa fa-pencil"></i> რედაქტირება</a>--}}
                    </div>
                </div>
            </div>
            <!-- End My Profile Content -->

        </div>
    </div>
    <!-- End Main Content -->
@stop