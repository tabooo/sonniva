@extends('web')
@section('content')
    <!-- Breadcrumbs -->
    <div class="breadcrumb-container">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{asset("/")}}">მთავარი</a></li>
                <li class="active">ჩემი პროფილი</li>
            </ol>
        </div>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Main Content -->
    <div class="container m-t-3">
        <div class="row">

            <!-- Account Sidebar -->
            <div class="col-sm-4 col-md-3 m-b-3">
                <div class="account-picture">
                    <img src="{{asset('/assets/images/demo/user.png')}}" alt="" class="img-circle img-responsive">
                </div>
                <h4 class="text-center m-b-3">{{$user['name']}} {{$user['last_name']}}</h4>
                <ul class="nav nav-pills nav-stacked">
                    <li role="presentation" class="active"><a href="{{asset('/user/profile')}}">ჩემი პროფილი</a></li>
                    <li role="presentation"><a href="{{asset('/user/address')}}">ჩემი მისამართი</a></li>
                    <li role="presentation"><a href="{{asset('/user/purchaseHistory')}}">შეკვეთების ისტორია</a></li>
                    <li role="presentation"><a href="{{asset('/user/password')}}">პაროლის შეცვლა</a></li>
                </ul>
            </div>
            <!-- End Account Sidebar -->

            <!-- My Profile Content -->
            <div class="col-sm-8 col-md-9">
                <div class="title m-b-2"><span>ჩემი პროფილი</span></div>
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="list-group list-group-nav">
                            <li class="list-group-item">
                                <strong>რეფერალ კოდი</strong>
                                @foreach($userTrees as $userTree)
                                    <p>{{$userTree['user_tree_id']}}
                                        (დონე: {{count(explode("/",$userTree['parent_path']))-1}})</p>
                                @endforeach
                            </li>
                            <li class="list-group-item">
                                <strong>სახელი</strong>
                                <p>{{$user['name']}}</p>
                            </li>
                            <li class="list-group-item">
                                <strong>გვარი</strong>
                                <p>{{$user['last_name']}}</p>
                            </li>
                            <li class="list-group-item">
                                <strong>დაბადების თარიღი</strong>
                                <p>{{$user['birth_date']}}</p>
                            </li>
                            <li class="list-group-item">
                                <strong>ელ. ფოსტა</strong>
                                <p>{{$user['email']}}</p>
                            </li>
                            <li class="list-group-item">
                                <strong>ტელ. ნომერი</strong>
                                <p>{{$user['mobile_no']}}</p>
                            </li>
                        </ul>
                        <a href="#" class="btn btn-theme pull-right"><i class="fa fa-pencil"></i> რედაქტირება</a>
                    </div>
                </div>
            </div>
            <!-- End My Profile Content -->

        </div>
    </div>
    <!-- End Main Content -->
@stop