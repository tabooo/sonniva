<?php

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Auth;

if (Auth::check()) {
    Cart::restore(Auth::user()->email);
    Cart::store(Auth::user()->email);
}
?>
@include('components.header')
@yield('content')
@include('components.footer')