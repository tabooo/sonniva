@extends('web')
@section('content')
    <!-- Breadcrumbs -->
    <div class="breadcrumb-container">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{asset("/")}}">მთავარი</a></li>
                <li><a href="{{asset("/products")}}">პროდუქტები</a></li>
                <li class="active">{{$product['name']}}</li>
            </ol>
        </div>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Main Content -->
    <div class="container m-t-3">
        <div class="row">

            <!-- Image List -->
            <div class="col-sm-4">
                <?php $prodImg = asset('/assets/images/demo/noimage.jpg'); ?>
                @if(count($product['productFile'])>0)
                    <?php $prodImg = asset('/files/product_files/' . $product['product_id'] . '/' . $product['productFile'][0]['file_name']);?>
                    <div class="image-detail" style="height:400px; overflow: hidden; border: 1px solid #ddd;">
                        <img src="{{$prodImg}}"
                             style="border: 0">
                    </div>
                    <div class="products-slider-detail m-b-2">
                        @foreach($product['productFile'] as $productImg)
                            <a href="#">
                                <img src="{{asset('/files/product_files/'.$product['product_id'].'/'.$productImg['file_name'])}}"
                                     class="img-thumbnail">
                            </a>
                        @endforeach
                    </div>
                @else
                    <div class="image-detail">
                        <img src="{{$prodImg}}">
                    </div>
                @endif
                <div class="title"><span>გაზიარება</span></div>
                <div class="share-button m-b-3">
                    <button type="button" class="btn btn-primary"><i class="fa fa-facebook"></i></button>
                    <button type="button" class="btn btn-info"><i class="fa fa-twitter"></i></button>
{{--                    <button type="button" class="btn btn-danger"><i class="fa fa-google-plus"></i></button>--}}
{{--                    <button type="button" class="btn btn-primary"><i class="fa fa-linkedin"></i></button>--}}
                    <button type="button" class="btn btn-warning"><i class="fa fa-envelope"></i></button>
                </div>
            </div>
            <!-- End Image List -->

            <div class="col-sm-8">
                <div class="title-detail">{{$product['name']}}</div>
                <table class="table table-detail">
                    <tbody>
                    <tr>
                        <td>ფასი</td>
                        <td>
                            <div class="price">
                                <div>{{$product['price']}}
                                    GEL{{--<span class="label label-default arrowed">-10%</span>--}}</div>
                                {{--<span class="price-old">${{$product['price']*1.1}}</span>--}}
                            </div>
                        </td>
                    </tr>
                    {{--<tr>
                        <td>საწყობში</td>
                        <td><span class="label label-success arrowed">{{($product['amount'])}}</span></td>
                    </tr>--}}
                    <tr>
                        <td>რაოდენობა</td>
                        <td>
                            <div class="input-qty">
                                <input type="text" value="1" class="form-control text-center" id="addQtyValue"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <button class="btn btn-theme m-b-1" type="button"
                                    onclick="addItemToShoppingCart({{$product['product_id']}},'{{$prodImg}}'); return false;">
                                <i class="fa fa-shopping-cart"></i> კალათაში დამატება
                            </button>
                            {{--<button class="btn btn-theme m-b-1" type="button">
                                <i class="fa fa-align-left"></i> Add to Compare
                            </button>--}}
                            {{--<button class="btn btn-theme m-b-1" type="button">
                                <i class="fa fa-heart"></i> Add to Wishlist
                            </button>--}}
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-md-8">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#desc" aria-controls="desc" role="tab" data-toggle="tab">აღწერა</a>
                    </li>
                    <li role="presentation">
                        <a href="#detail" aria-controls="detail" role="tab" data-toggle="tab">დეტალური ინფორმაცია</a>
                    </li>
                    {{--<li role="presentation">
                        <a href="#review" aria-controls="review" role="tab" data-toggle="tab">შეფასებები (2)</a>
                    </li>--}}
                </ul>
                <!-- End Nav tabs -->

                <!-- Tab panes -->
                <div class="tab-content tab-content-detail">

                    <!-- Description Tab Content -->
                    <div role="tabpanel" class="tab-pane active" id="desc">
                        <div class="well">
                            <p>
                                {{$product['description']}}
                            </p>
                        </div>
                    </div>
                    <!-- End Description Tab Content -->

                    <!-- Detail Tab Content -->
                    <div role="tabpanel" class="tab-pane" id="detail">
                        <div class="well">
                            <table class="table table-bordered">
                                <tbody>
                                @if(count($product['productInfo'])>0)
                                    @foreach($product['productInfo'] as $productInfo)
                                        <tr>
                                            <td>{{$productInfo['product_info_type_name']}}</td>
                                            <td>{{$productInfo['value']}}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- End Detail Tab Content -->

                    <!-- Review Tab Content -->
                {{--<div role="tabpanel" class="tab-pane" id="review">
                    <div class="well">
                        <div class="media">
                            <div class="media-left">
                                <a href="#">
                                    <img class="media-object img-thumbnail" alt="64x64"
                                         src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI2NCIgaGVpZ2h0PSI2NCI+PHJlY3Qgd2lkdGg9IjY0IiBoZWlnaHQ9IjY0IiBmaWxsPSIjZWVlIi8+PHRleHQgdGV4dC1hbmNob3I9Im1pZGRsZSIgeD0iMzIiIHk9IjMyIiBzdHlsZT0iZmlsbDojYWFhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjEycHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+NjR4NjQ8L3RleHQ+PC9zdmc+">
                                </a>

                                <div class="product-rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half-o"></i>
                                </div>
                            </div>
                            <div class="media-body">
                                <h5 class="media-heading"><strong>John Thor</strong></h5>
                                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante
                                sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra
                                turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue
                                felis
                                in faucibus.
                            </div>
                        </div>
                        <hr/>
                        <h4 class="m-b-2">დაამატე განხილვა</h4>

                        <form role="form">
                            <div class="form-group">
                                <label for="Name">სახელი</label>
                                <input type="text" id="Name" class="form-control" placeholder="სახელი">
                            </div>
                            <div class="form-group">
                                <label for="Email">ელ. ფოსტა</label>
                                <input type="text" id="Email" class="form-control" placeholder="ელ. ფოსტა">
                            </div>
                            <div class="form-group">
                                <label>შეფასება</label>

                                <div class="clearfix"></div>
                                <div class="input-rating"></div>
                            </div>
                            <div class="form-group">
                                <label for="Review">ტექსტი</label>
                                <textarea id="Review" class="form-control" rows="5"
                                          placeholder="ტექსტი"></textarea>
                            </div>
                            <button type="submit" class="btn btn-theme">შეფასების გაგზავნა</button>
                        </form>
                    </div>
                </div>--}}
                <!-- End Review Tab Content -->

                </div>
                <!-- End Tab panes -->

            </div>
        </div>

        <!-- Related Products -->
        <div class="row m-t-3">
            <div class="col-xs-12">
                <div class="title"><span>მსგავსი პროდუქტები</span></div>
                <div class="related-product-slider owl-controls-top-offset">
                    @foreach($likeProducts as $product)
                        <?php $prodImg = asset('/assets/images/demo/noimage.jpg'); ?>
                        @if(count($product['productFile'])>0)
                            <?php $prodImg = asset('/files/product_files/' . $product['product_id'] . '/' . $product['productFile'][0]['file_name']);?>
                        @endif
                        <div class="box-product-outer">
                            <div class="box-product">
                                <div class="img-wrapper">
                                    <a href="{{asset("/product/".$product['product_id'])}}">
                                        <img alt="Product" src="{{$prodImg}}">
                                    </a>

                                    <div class="tags">
                                <span class="label-tags">
                                    <span class="label label-default arrowed">Featured</span>
                                </span>
                                    </div>
                                    {{--<div class="tags tags-left">
                                <span class="label-tags">
                                    <span class="label label-danger arrowed-right">Sale</span>
                                </span>
                                    </div>--}}
                                    <div class="option">
                                        <a href="#" data-toggle="tooltip" title="კალათაში დამატება"
                                           onclick="addItemToShoppingCart({{$product['product_id']}},'{{$prodImg}}'); return false;">
                                            <i class="fa fa-shopping-cart"></i>
                                        </a>
                                        {{--<a href="#" data-toggle="tooltip" title="Add to Compare"><i
                                                    class="fa fa-align-left"></i></a>
                                        <a href="#" data-toggle="tooltip" title="Add to Wishlist" class="wishlist"><i
                                                    class="fa fa-heart"></i></a>--}}
                                    </div>
                                </div>
                                <h6><a href="{{asset("/product/".$product['product_id'])}}">{{$product['name']}}</a>
                                </h6>

                                <div class="price">
                                    <div>${{$product['price']}}
                                        {{--<span class="label-tags"><span class="label label-default">-10%</span></span>--}}
                                    </div>
                                    {{--<span class="price-old">${{$product['price']*1.1}}</span>--}}
                                </div>
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half-o"></i>
                                    <a href="#">(5 reviews)</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <!-- End Related Products -->
    </div>
@stop