@extends('web')
@section('content')
    <!-- Breadcrumbs -->
    <div class="breadcrumb-container">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{asset('/')}}">მთავარი</a></li>
                <li class="active">გადახდა</li>
            </ol>
        </div>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Main Content -->
    <div class="container m-t-3">
        <div class="row">

            <!-- Checkout Form -->
            <div class="col-md-12">
                <div class="title"><span>მისამართი</span></div>
                <form action="{{asset('/pay')}}" method="POST" id="addressForm"></form>
                <div class="row">
                    <div class="form-group col-sm-6">
                        <label for="countryInput">ქვეყანა (*)</label>
                        <select class="form-control selectpicker" id="countryInput" name="country_id"
                                data-live-search="true" form="addressForm">
                            <option value=""> --- აირჩიეთ ქვეყანა ---</option>
                            <option value="1" selected>საქართველო</option>
                        </select>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="cityInput">ქალაქი (*)</label>
                        <select class="form-control selectpicker" id="cityInput" name="city_id" data-live-search="true"
                                form="addressForm">
                            <option value=""> --- აირჩიეთ ქალაქი ---</option>
                            @foreach($cities as $city)
                                <option value="{{$city["city_id"]}}" <?php if ($primaryAddress && $city["city_id"] == $primaryAddress['city_id']) echo "selected"; ?>>{{$city["name_ge"]}}
                                    (მიტანა {{$city['price']}} ლარი)
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="addressInput">მისამართი (*)</label>
                        <textarea class="form-control" rows="3" id="addressInput" name="address" form="addressForm">
                            {{$primaryAddress['address']}}
                        </textarea>
                    </div>
                </div>
                <nav aria-label="Checkout Next Navigation">
                    <ul class="pager">
                        <li class="previous">
                            <a href="{{asset('/cart')}}">
                                <span aria-hidden="true">&larr;</span>კალათაში დაბრუნება
                            </a>
                        </li>
                        <li class="next">
                            <button type="submit" form="addressForm">გაგრძელება</button>
                            {{--<a href="{{asset('/pay')}}">გაგრძელება<span aria-hidden="true">&rarr;</span>--}}
                            </a>
                        </li>
                    </ul>
                </nav>

            </div>
            <!-- End Checkout Form -->
        </div>
    </div>
    <!-- End Main Content -->
@stop