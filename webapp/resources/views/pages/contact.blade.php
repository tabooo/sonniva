@extends('web')
@section('content')
    <div class="breadcrumb-container">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{asset("/")}}">მთავარი</a></li>
                <li class="active">კონტაქტი</li>
            </ol>
        </div>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Main Content -->
    <div class="container m-t-3">
        <div class="row">

            <!-- Contact Form -->
            <div class="col-sm-8">
                <div class="title"><span>მოგვწერეთ</span></div>
                <form>
                    <div class="form-group">
                        <label for="nameInput">სახელი (*)</label>
                        <input type="text" class="form-control" id="nameInput" placeholder="სახელი">
                    </div>
                    <div class="form-group">
                        <label for="emailInput">ელ. ფოსტა (*)</label>
                        <input type="email" class="form-control" id="emailInput" placeholder="ელ. ფოსტა">
                    </div>
                    <div class="form-group">
                        <label for="subjectInput">სათაური</label>
                        <input type="text" class="form-control" id="subjectInput" placeholder="სათაური">
                    </div>
                    <div class="form-group">
                        <label for="notesInput">ტექსტი (*)</label>
                        <textarea class="form-control" rows="3" id="notesInput"></textarea>
                    </div>
                    <button type="button" class="btn btn-theme pull-right">გაგზავნა <i
                                class="fa fa-arrow-circle-right"></i>
                    </button>
                </form>
            </div>
            <!-- End Contact Form -->

            <div class="clearfix visible-xs"></div>

            <!-- Contact Info -->
            <div class="col-sm-4">
                <div class="title"><span>საკონტაქტო ინფორმაცია</span></div>
                <ul class="list-group list-group-nav">
                    <li class="list-group-item">&raquo; {{$params['address_ge']}}</li>
                    <li class="list-group-item">&raquo; {{$params['mobile_main']}}</li>
                    <li class="list-group-item">&raquo; {{$params['email']}}</li>
                </ul>

                <!-- Location Map -->
                <div class="title"><span>Our Location</span></div>
                <div class="embed-responsive embed-responsive-4by3">
                    <iframe class="embed-responsive-item"
                            src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d11919.403557038086!2d44.836341852978535!3d41.68056364734279!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ska!2sge!4v1495116254707"></iframe>
                </div>
                <!-- End Location Map -->

            </div>
            <!-- End Contact Info -->

        </div>
    </div>
    </div>
@stop