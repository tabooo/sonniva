@extends('web')
@section('content')
        <!-- Breadcrumbs -->
<div class="breadcrumb-container">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{asset("/")}}">მთავარი</a></li>
            <li class="{{asset($page['link'])}}">{{$page['title_ge']}}</li>
        </ol>
    </div>
</div>
<!-- End Breadcrumbs -->

<!-- Main Content -->
<div class="container m-t-3">
    <div class="row">

        <!-- Content -->
        <div class="col-sm-12 blog-detail-content">
            <div class="title"><span>{{$page['title_ge']}}</span></div>
            <small>
                <span><i class="fa fa-clock-o"></i>{{date('Y-m-d H:i', strtotime($page['created_at']))}}</span>
                {{--<span><i class="fa fa-user"></i> <a href="#">Admin</a></span>--}}
                {{--<span><i class="fa fa-tag"></i> <a href="#">Tags</a></span>--}}
            </small>
            {{--<img src="" alt="Blog Detail" class="img-thumbnail">--}}

            {!!$page['description_ge']!!}

            <div class="title m-t-3"><span>Share to</span></div>
            <div class="share-button">
                <button type="button" class="btn btn-primary"><i class="fa fa-facebook"></i></button>
                <button type="button" class="btn btn-info"><i class="fa fa-twitter"></i></button>
                <button type="button" class="btn btn-danger"><i class="fa fa-google-plus"></i></button>
                <button type="button" class="btn btn-primary"><i class="fa fa-linkedin"></i></button>
                <button type="button" class="btn btn-warning"><i class="fa fa-envelope"></i></button>
            </div>
        </div>
        <!-- End Content -->

        <!-- Blog Sidebar -->
        {{--<div class="col-sm-4">--}}
            {{--<div class="title"><span>Categories</span></div>--}}
            {{--<ul class="list-group list-group-nav">--}}
                {{--<li class="list-group-item">&raquo; <a href="blog.html">News</a></li>--}}
                {{--<li class="list-group-item">&raquo; <a href="blog.html">Events</a></li>--}}
                {{--<li class="list-group-item">&raquo; <a href="blog.html">Promotions</a></li>--}}
            {{--</ul>--}}
            {{--<div class="title"><span>Archives</span></div>--}}
            {{--<ul class="list-group list-group-nav">--}}
                {{--<li class="list-group-item">&raquo; <a href="blog.html">January 2016</a></li>--}}
                {{--<li class="list-group-item">&raquo; <a href="blog.html">February 2016</a></li>--}}
                {{--<li class="list-group-item">&raquo; <a href="blog.html">March 2016</a></li>--}}
                {{--<li class="list-group-item">&raquo; <a href="blog.html">April 2016</a></li>--}}
                {{--<li class="list-group-item">&raquo; <a href="blog.html">May 2016</a></li>--}}
            {{--</ul>--}}
        {{--</div>--}}
        <!-- End Blog Sidebar -->

    </div>
</div>
<!-- End Main Content -->
@stop