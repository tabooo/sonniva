@extends('web')
@section('content')
    <!-- Breadcrumbs -->
    <div class="breadcrumb-container">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{asset('/')}}">მთავარი</a></li>
                <li class="active">შეკვეთა</li>
            </ol>
        </div>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Main Content -->
    <div class="container m-t-3">
        <div class="row">

            <!-- Checkout Form -->
            <div class="col-md-12">
                <b>მადლობა. თქვენი შეკვეთა მიღებულია</b>
            </div>
            <!-- End Checkout Form -->
        </div>
    </div>
    <!-- End Main Content -->
@stop