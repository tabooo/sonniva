@extends('web')
@section('content')
    <!-- Breadcrumbs -->
    <div class="breadcrumb-container">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{url('/')}}">მთავარი</a></li>
                <li class="active">კალათა</li>
            </ol>
        </div>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Main Content -->
    <div class="container m-t-3">
        <div class="row">

            <!-- Shopping Cart List -->
            <div class="col-md-9">
                <div class="title"><span>ჩემი კალათა</span></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-cart">
                        <thead>
                        <tr>
                            <th>სურათი</th>
                            <th>დასახელება</th>
                            <th>რაოდენობა</th>
                            <th>ერთ. ფასი</th>
                            <th>ჯამი</th>
                            <th>ქმედება</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(Cart::content() as $key => $value)
                            <tr>
                                <td class="img-cart">
                                    <a href="{{url('/product/'.$value->id)}}">
                                        <img alt="Product" src="{{$value->options->img}}" class="img-thumbnail">
                                    </a>
                                </td>
                                <td>
                                    <p><a href="{{url('/product/'.$value->id)}}" class="d-block">{{$value->name}}</a>
                                    </p>
                                </td>
                                <td>
                                    <input type="text" value="{{$value->qty}}" class="form-control text-center"
                                           style="width:80px;"
                                           onchange="updateItemToShoppingCart('{{$key}}',this.value, true);"/>
                                </td>
                                <td class="unit">{{$value->price}}</td>
                                <td class="sub">{{$value->subtotal}}</td>
                                <td class="action">
                                    {{--<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Update"><i--}}
                                    {{--class="fa fa-refresh"></i></a>&nbsp;--}}
                                    <a href="#" onclick="removeItemFromShoppingCart('{{$key}}', true); return false"
                                       class="text-danger" data-toggle="tooltip" data-placement="top"
                                       data-original-title="Remove"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="4" class="text-right">სულ თანხა:</td>
                            <td colspan="2"><b>{{Cart::subtotal()}}</b></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <nav aria-label="Shopping Cart Next Navigation">
                    <ul class="pager">
                        <li class="previous">
                            <a href="{{asset('/products')}}">
                                <span aria-hidden="true">&larr;</span> მაღაზიაში დაბრუნება
                            </a>
                        </li>
                        <li class="next">
                            <a href="{{asset('/checkoutNoAuth')}}"> {{--<a href="{{asset('/checkout')}}">--}}
                                გაგრძელება <span aria-hidden="true">&rarr;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <!-- End Shopping Cart List -->

            <!-- New Arrivals -->
            <div class="col-md-3 hidden-sm hidden-xs">
                <div class="title"><span><a href="products.html">შესაძლოა გინდოდეთ <i
                                    class="fa fa-chevron-circle-right"></i></a></span>
                </div>
                <div class="widget-slider owl-controls-top-offset">
                    @foreach($likeProducts as $product)
                        <?php $prodImg = asset('/assets/images/demo/noimage.jpg'); ?>
                        @if(count($product['productFile'])>0)
                            <?php $prodImg = asset('/files/product_files/' . $product['product_id'] . '/' . $product['productFile'][0]['file_name']);?>
                        @endif
                        <div class="box-product-outer">
                            <div class="box-product">
                                <div class="img-wrapper">
                                    <a href="{{asset("/product/".$product['product_id'])}}">
                                        <img alt="Product" src="{{$prodImg}}">
                                    </a>

                                    <div class="option">
                                        <a href="#" data-toggle="tooltip" title="Add to Cart"
                                           onclick="addItemToShoppingCart({{$product['product_id']}},'{{$prodImg}}'); return false;"><i
                                                    class="fa fa-shopping-cart"></i></a>
                                        <a href="#" data-toggle="tooltip" title="Add to Compare"><i
                                                    class="fa fa-align-left"></i></a>
                                        <a href="#" data-toggle="tooltip" title="Add to Wishlist" class="wishlist"><i
                                                    class="fa fa-heart"></i></a>
                                    </div>
                                </div>
                                <h6><a href="{{asset("/product/".$product['product_id'])}}">{{$product['name']}}</a>
                                </h6>

                                <div class="price">
                                    <div>$15.00</div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <!-- End New Arrivals -->

        </div>
    </div>
@stop