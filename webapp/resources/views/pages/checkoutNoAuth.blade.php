@extends('web')
@section('content')
    <!-- Breadcrumbs -->
    <div class="breadcrumb-container">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{asset('/')}}">მთავარი</a></li>
                <li class="active">შეკვეთა</li>
            </ol>
        </div>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Main Content -->
    <div class="container m-t-3">
        <div class="row">

            <!-- Checkout Form -->
            <div class="col-md-12">
                <div class="title"><span>მისამართი</span></div>
                <form action="{{asset('/finishOrder')}}" method="POST" id="addressForm"></form>
                <div class="row">
                    <div class="form-group col-sm-12">
                        <label for="fullNameInput">სახელი გვარი (ან ორგანიზაციის დასახელება)</label>
                        <input type="text" class="form-control" id="fullNameInput" name="fullName" form="addressForm">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="personalNoInput">პირადი ნომერი (ან საიდდენტიფიკაციო კოდი)</label>
                        <input type="text" class="form-control" id="personalNoInput" name="personalNo"
                               form="addressForm">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="phoneInput">ტელეფონი</label>
                        <input type="text" class="form-control" id="phoneInput" name="phone" form="addressForm">
                    </div>
                </div>
                <nav aria-label="Checkout Next Navigation">
                    <ul class="pager">
                        <li class="previous">
                            <a href="{{asset('/cart')}}">
                                <span aria-hidden="true">&larr;</span>კალათაში დაბრუნება
                            </a>
                        </li>
                        <li class="next">
                            <button type="submit" form="addressForm">შეკვეთის გაგზავნა</button>
                        </li>
                    </ul>
                </nav>

            </div>
            <!-- End Checkout Form -->
        </div>
    </div>
    <!-- End Main Content -->
@stop
