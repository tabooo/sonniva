@extends('web')
@section('content')
    <!-- Breadcrumbs -->
    <div class="breadcrumb-container">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{asset('/')}}">მთავარი</a></li>
                <li class="active">გადახდა</li>
            </ol>
        </div>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Main Content -->
    <div class="container m-t-3">
        <div class="row">

            <!-- Checkout Form -->
            <div class="col-md-12">
                <div class="title"><span>მისამართი</span></div>
                <div class="row">
                    <div class="form-group col-sm-6">
                        <label for="countryInput">ქვეყანა (*)</label>
                        <input type="text" class="form-control" value="საქართველო" readonly>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="cityInput">ქალაქი (*)</label>
                        <input type="text" class="form-control"
                               value="{{$city['name_ge']." (მიტანა ".$city['price']." GEL)"}}" readonly>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="addressInput">მისამართი (*)</label>
                        <textarea class="form-control" rows="3"
                                  id="addressInput" readonly>{{session('address')}}</textarea>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-cart">
                        <thead>
                        <tr>
                            <th>სურათი</th>
                            <th>დასახელება</th>
                            <th>რაოდენობა</th>
                            <th>ერთ. ფასი</th>
                            <th>ჯამი</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(Cart::content() as $key => $value)
                            <tr>
                                <td class="img-cart">
                                    <a href="{{url('/product/'.$value->id)}}">
                                        <img alt="Product" src="{{$value->options->img}}" class="img-thumbnail">
                                    </a>
                                </td>
                                <td>
                                    <p><a href="{{url('/product/'.$value->id)}}"
                                          class="d-block">{{$value->name}}</a></p>
                                    {{--<small>Size : M</small>--}}
                                </td>
                                <td class="text-center">{{$value->qty}}</td>
                                <td class="unit">GEL {{$value->price}}</td>
                                <td class="sub">GEL {{$value->subtotal}}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="4" class="text-right">სულ თანხა:</td>
                            <td><b>GEL {{Cart::subtotal()+$city['price']}}</b></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <nav aria-label="Checkout Next Navigation">
                    <ul class="pager">
                        <li class="previous">
                            <a href="{{asset('/cart')}}">
                                <span aria-hidden="true">&larr;</span>კალათაში დაბრუნება
                            </a>
                        </li>
                        <li class="next">


                            <form action="https://e-commerce.cartubank.ge/servlet/Process3DSServlet/3dsproxy_init.jsp"
                                  method="GET">
                                <input name="PurchaseDesc" type="hidden" value="<?php echo base64_encode(openssl_encrypt(Auth::user()->email,"AES-256-CBC", "tabo", 0, substr(hash('sha256', "tabo"), 0, 16)));//uniqid(); ?>"/>
                                <input name="PurchaseAmt" type="hidden"
                                       value="{{Cart::subtotal(2,'.','')+$city['price']}}"/>
                                <input name="CountryCode" type="hidden" value="268"/>
                                <input name="CurrencyCode" type="hidden" value="981"/>
                                <input name="MerchantName" type="hidden" value="Terakom!gdc.pixl.ge"/>
                                <input name="MerchantURL" type="hidden"
                                       value="{{url('/checkout')}}"/>
                                <input name="MerchantCity" type="hidden" value="Tbilisi"/>
                                <input name="MerchantID" type="hidden" value="000000008001290-00000001"/>
                                <input name="xDDDSProxy.Language" type="hidden" value="01"/>
                                <input type="submit" value="გადახდა"/>
                            </form>
                            {{-- <a href="#" onclick="checkout(); return false;">
                                 გადახდა <span aria-hidden="true">&rarr;</span>
                             </a>--}}
                        </li>
                    </ul>
                </nav>

            </div>
            <!-- End Checkout Form -->
        </div>
    </div>
    <!-- End Main Content -->
@stop