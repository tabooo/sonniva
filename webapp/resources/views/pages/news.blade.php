@extends('web')
@section('content')
        <!-- Breadcrumbs -->
<div class="breadcrumb-container">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{asset("/")}}">მთავარი</a></li>
            <li class="active">სიახლეები</li>
        </ol>
    </div>
</div>
<!-- End Breadcrumbs -->

<!-- Main Content -->
<div class="container m-t-3">
    <div class="row">

        <!-- Content -->
        <div class="col-sm-8">
            <div class="title m-b-2"><span>ბოლო სიახლეები</span></div>
            <div class="row">
                @foreach($news as $new)
                    <div class="col-md-6">
                        <div class="thumbnail blog-list">
                            <div style="height:260px; overflow: hidden">
                                <a href="{{asset($new['link'])}}">
                                    <img src="{{asset($new['img_path'])}}" style="width:100%">
                                </a>
                            </div>

                            <div class="caption">
                                <h4 style="height:50px; overflow: hidden">{{$new['title_ge']}}</h4>
                                <small>
                                    <span><i class="fa fa-clock-o"></i> {{date('Y-m-d H:i', strtotime($new['published_at']))}}</span>
                                    <span><i class="fa fa-user"></i> <a href="#">Admin</a></span>
                                    <span><i class="fa fa-tag"></i> <a href="#">Tags</a></span>
                                </small>
                                <p style="height:50px; overflow: hidden">
                                    {!!$new['description_ge_short']!!}
                                </p>

                                <div class="text-right">
                                    <a href="{{asset($new['link'])}}" class="btn btn-theme btn-sm">
                                        <i class="fa fa-long-arrow-right"></i> გაგრძელება
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="col-xs-12 text-center">
                    <nav aria-label="Page navigation">
                        {!!$news->links()!!}
                    </nav>
                </div>
            </div>
        </div>
        <!-- End Content -->

        <!-- Blog Sidebar -->
        <div class="col-sm-4">
            <div class="title"><span>კატეგორიები</span></div>
            <ul class="list-group list-group-nav">
                @foreach($web_categories as $cat)
                    <li class="list-group-item">&raquo;
                        <a href="{{asset('/news?category='.$cat['category_id'])}}">{{$cat['name_ge']}}</a>
                    </li>
                @endforeach
            </ul>
            <div class="title"><span>პოპულარული სიახლეები</span></div>
            <ul class="list-group list-group-nav">
                @foreach($popularNews as $new)
                    <li class="list-group-item">&raquo; <a
                                href="{{asset('/news/'.$new['post_id'])}}">{{$new['title_ge']}}</a></li>
                @endforeach
            </ul>
        </div>
        <!-- End Blog Sidebar -->

    </div>
</div>
@stop