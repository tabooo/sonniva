@extends('web')
@section('content')
    <div class="breadcrumb-container">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{asset("/")}}">მთავარი</a></li>
                <li class="active">რეგისტრაცია</li>
            </ol>
        </div>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Main Content -->
    <div class="container m-t-3">
        <div class="row">

            <!-- Register Form -->
            <div class="col-sm-8 login-register-form m-b-3">
                <div class="title"><span>რეგისტრაცია</span></div>
                <div class="row">
                    <form>
                        <div class="form-group col-sm-6">
                            <label for="nameInput">სახელი *</label>
                            <input type="text" class="form-control" name="name" placeholder="სახელი">
                            {{ csrf_field() }}
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="nameInput">გვარი *</label>
                            <input type="text" class="form-control" name="last_name" placeholder="გვარი">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="emailInput">ელ. ფოსტა *</label>
                            <input type="email" class="form-control" name="email" placeholder="ელ. ფოსტა">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="emailInput">დაბადების თარიღი</label>
                            <input type="text" class="form-control" name="birth_date" placeholder="1990-01-01">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="passwordInput">პაროლი *</label>
                            <input type="password" class="form-control" name="password" placeholder="პაროლი">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="passwordConfirmInput">გაიმეორეთ პაროლი *</label>
                            <input type="password" class="form-control" name="password2"
                                   placeholder="გაიმეორეთ პაროლი">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="passwordConfirmInput">ქალაქი</label>
                            <select name="city_id" class="form-control">
                                @foreach($cities as $city)
                                    <option value="{{$city['city_id']}}">{{$city['name_ge']}}</option>
                                @endforeach
                            </select>

                            <br/>
                            <label for="passwordConfirmInput">რეფერალი</label>
                            <input type="text" class="form-control" name="user_tree_id"
                                   placeholder="რეფერალი">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="addressInput">მისამართი</label>
                            <textarea class="form-control" rows="3" id="address"
                                      placeholder="ქუჩა, კორპუსის ნომერი, ბინის ნომერი"></textarea>
                        </div>
                        <div class="col-xs-12">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="confirm_rules"><span> ვეთანხმები <a href="#"><u>საიტის წესებს და
                                                პირობებს.</u></a></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <button type="button" class="btn btn-theme" onclick="registerUser(this.form)"><i
                                        class="fa fa-long-arrow-right"></i>
                                რეგისტრაცია
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- End Register Form -->

            <!-- Login Form -->
            <div class="col-sm-4">
                <div class="title"><span>უკვე დარეგისტრირებული ხარ ?</span></div>
                <form>
                    <div class="form-group">
                        <label for="emailInputLogin">ელ. ფოსტა</label>
                        <input type="email" class="form-control" name="username"
                               placeholder="ელ. ფოსტა">
                    </div>
                    <div class="form-group">
                        <label for="passwordInputLogin">პაროლი</label>
                        <input type="password" class="form-control" name="password"
                               placeholder="პაროლი">
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox"><span> დამახსოვრება</span>
                        </label>
                    </div>
                    <button type="button" class="btn btn-theme" onclick="loginUser(this.form)"><i
                                class="fa fa-long-arrow-right"></i>
                        შესვლა
                    </button>
                </form>
            </div>
            <!-- End Login Form -->

        </div>
    </div>
@stop