@extends('web')
@section('content')
        <!-- Breadcrumbs -->
<div class="breadcrumb-container">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{asset("/")}}">მთავარი</a></li>
            <li><a href="{{asset("/news")}}">სიახლეები</a></li>
            <li class="{{asset($news['link'])}}">{{$news['title_ge']}}</li>
        </ol>
    </div>
</div>
<!-- End Breadcrumbs -->

<!-- Main Content -->
<div class="container m-t-3">
    <div class="row">

        <!-- Content -->
        <div class="col-sm-8 blog-detail-content">
            <div class="title"><span>{{$news['title_ge']}}</span></div>
            <small>
                <span><i class="fa fa-clock-o"></i>{{date('Y-m-d H:i', strtotime($news['created_at']))}}</span>
                {{--<span><i class="fa fa-user"></i> <a href="#">Admin</a></span>--}}
                <span><i class="fa fa-tag"></i> <a href="#">Tags</a></span>
            </small>
            <img src="{{$news['img_path']}}" alt="Blog Detail" class="img-thumbnail">

            {!!$news['description_ge']!!}

            <div class="title m-t-3"><span>Share to</span></div>
            <div class="share-button">
                <button type="button" class="btn btn-primary"><i class="fa fa-facebook"></i></button>
                <button type="button" class="btn btn-info"><i class="fa fa-twitter"></i></button>
                <button type="button" class="btn btn-danger"><i class="fa fa-google-plus"></i></button>
                <button type="button" class="btn btn-primary"><i class="fa fa-linkedin"></i></button>
                <button type="button" class="btn btn-warning"><i class="fa fa-envelope"></i></button>
            </div>
        </div>
        <!-- End Content -->

        <!-- Blog Sidebar -->
        <div class="col-sm-4">
            <div class="title"><span>კატეგორიები</span></div>
            <ul class="list-group list-group-nav">
                @foreach($web_categories as $cat)
                    <li class="list-group-item">&raquo;
                        <a href="{{asset('/news?category='.$cat['category_id'])}}">{{$cat['name_ge']}}</a>
                    </li>
                @endforeach
            </ul>
            <div class="title"><span>მსგავსი სიახლეები</span></div>
            <ul class="list-group list-group-nav">
                @foreach($sameCategories as $new)
                    <li class="list-group-item">&raquo; <a
                                href="{{asset('/news/'.$new['post_id'])}}">{{$new['title_ge']}}</a></li>
                @endforeach
            </ul>
        </div>
        <!-- End Blog Sidebar -->

    </div>
</div>
<!-- End Main Content -->
@stop