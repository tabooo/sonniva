@extends('web')
@section('content')
    <!-- Breadcrumbs -->
    <div class="breadcrumb-container">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{asset("/")}}">მთავარი</a></li>
                <li class="active">პროდუქტები</li>
            </ol>
        </div>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Main Content -->
    <div class="container m-t-3">
        <div class="row">

            <!-- Filter Sidebar -->
            <div class="col-sm-3">
                {{--<div class="filter-sidebar">
                    <div class="title"><span>Enabled Filters</span></div>
                    <ul>
                        <li>Categories: T-Shirts <a href="#" class="remove-filter" data-toggle="tooltip" title="Remove"><i
                                        class="fa fa-remove"></i></a></li>
                        <li>Availability: In Stock <a href="#" class="remove-filter" data-toggle="tooltip"
                                                      title="Remove"><i
                                        class="fa fa-remove"></i></a></li>
                        <li>Brand: Brand Name 1 <a href="#" class="remove-filter" data-toggle="tooltip"
                                                   title="Remove"><i
                                        class="fa fa-remove"></i></a></li>
                    </ul>
                </div>--}}
                {{--<div class="filter-sidebar">
                    <div class="title"><span>Categories</span></div>
                    <div class="checkbox"><label><input type="checkbox"
                                                        checked="checked"><span> T-Shirts (10)</span></label></div>
                    <div class="checkbox"><label><input type="checkbox"><span> Polo T-Shirts (11)</span></label></div>
                </div>--}}
                {{--<div class="filter-sidebar">
                    <div class="title"><span>Options</span></div>
                    <ul>
                        <li>
                            <select class="selectpicker" data-width="100%">
                                <option value="0">All Options</option>
                                <option value="1">Option 1</option>
                                <option value="2">Option 2</option>
                                <option value="3">Option 3</option>
                            </select>
                        </li>
                        <li>
                            <select class="selectpicker" data-width="100%">
                                <option value="0">All Options</option>
                                <option value="1">Option 1</option>
                                <option value="2">Option 2</option>
                                <option value="3">Option 3</option>
                            </select>
                        </li>
                    </ul>
                </div>--}}
                {{--<div class="filter-sidebar">
                    <div class="title"><span>Options 2</span></div>
                    <div class="radio"><label><input type="radio" name="options2"
                                                     checked="checked"><span>All Options 2</span></label></div>
                    <div class="radio"><label><input type="radio" name="options2"><span>Options 2.1</span></label></div>
                    <div class="radio"><label><input type="radio" name="options2"><span>Options 2.2</span></label></div>
                    <div class="radio"><label><input type="radio" name="options2"><span>Options 2.3</span></label></div>
                </div>--}}
                <div class="filter-sidebar">
                    <div class="title"><span>საწყობში</span></div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" checked="checked"><span>არის საწყობში</span>
                        </label>
                    </div>
                </div>
                <div class="filter-sidebar">
                    <div class="title"><span>ბრენდი</span></div>
                    <div class="checkbox"><label>
                            <input type="checkbox" checked="checked"><span> ბრენდი 1 (11)</span>
                        </label>
                    </div>
                    <div class="checkbox"><label>
                            <input type="checkbox" checked="checked"><span> ბრენდი 2 (11)</span>
                        </label>
                    </div>
                </div>
                <div class="filter-sidebar">
                    <div class="title"><span>ფასი</span></div>
                    <div id="range-value">დიაპაზონი: <span id="min-price"></span> - <span id="max-price"></span></div>
                    <input type="hidden" name="min-price" value="">
                    <input type="hidden" name="max-price" value="">

                    <div class="price-range">
                        <div id="price"></div>
                    </div>
                </div>
                {{--<div class="filter-sidebar">
                    <div class="title"><span>Size</span></div>
                    <label class="checkbox-inline"><input type="checkbox"><span> M (12)</span></label>
                    <label class="checkbox-inline"><input type="checkbox"><span> L (13)</span></label>
                    <label class="checkbox-inline"><input type="checkbox"><span> XL (14)</span></label>
                </div>--}}
            </div>
            <!-- End Filter Sidebar -->

            <!-- Product List -->
            <div class="col-sm-9">
                <div class="title"><span>პროდუქცია</span></div>

                <!-- Product Sorting Bar -->
                <div class="product-sorting-bar">
                    <div>დალაგება
                        <select id="sortby" class="selectpicker" data-width="180px" onchange="getProducts()">
                            <option value="new" <?php if (app('request')->input('sort') == 'new') echo 'selected';?>>
                                ახალი დამატებული
                            </option>
                            <option value="lowToHigh" <?php if (app('request')->input('sort') == 'lowToHigh') echo 'selected';?>>
                                ფასი დაბალი &raquo; მაღლისკენ
                            </option>
                            <option value="highToLow" <?php if (app('request')->input('sort') == 'highToLow') echo 'selected';?>>
                                ფასი მაღალი &raquo; დაბლისკენ
                            </option>
                        </select>
                    </div>
                    <div>მაჩვენე
                        <select id="limitPerPage" class="selectpicker" data-width="60px" onchange="getProducts()">
                            <option value="8" <?php if (app('request')->input('limit') == '8') echo 'selected';?>>8</option>
                            <option value="12" <?php if (app('request')->input('limit') == '12') echo 'selected';?>>12</option>
                            <option value="16" <?php if (app('request')->input('limit') == '16') echo 'selected';?>>16</option>
                        </select> გვერდზე
                    </div>
                </div>
                <!-- End Product Sorting Bar -->

                <?php $it = 0; ?>
                @foreach($products as $product)
                    <?php $prodImg = asset('/assets/images/demo/noimage.jpg'); ?>
                    @if(count($product['productFile'])>0)
                        <?php $prodImg = asset('/files/product_files/' . $product['product_id'] . '/' . $product['productFile'][0]['file_name']);?>
                    @endif
                    <?php if ($it > 0 && ($it) % 4 == 0) echo '<div class="clearfix"></div>'; ?>
                    <div class="col-sm-4 col-md-3 <?php
                    $it++;
                    if ($it % 4 == 0) {
                        echo " hidden-sm ";
                    }
                    ?>box-product-outer">
                        <div class="box-product">
                            <div class="img-wrapper">
                                <a href="{{asset("/product/".$product['product_id'])}}">
                                    <img alt="Product"
                                         src="{{$prodImg}}">
                                </a>

                                {{--<div class="tags">
                                <span class="label-tags">
                                    <span class="label label-default arrowed">Featured</span>
                                </span>
                                </div>--}}
                                <div class="tags tags-left">
                                <span class="label-tags">
                                    <span class="label label-success arrowed-right">საწყობში</span>
                                </span>
                                </div>
                                <div class="option">
                                    <a href="#" data-toggle="tooltip" title="კალათაში დამატება"
                                       onclick="addItemToShoppingCart({{$product['product_id']}},'{{$prodImg}}'); return false;">
                                        <i class="fa fa-shopping-cart"></i></a>
                                    {{--<a href="#" data-toggle="tooltip" title="Add to Compare"><i
                                                class="fa fa-align-left"></i></a>
                                    <a href="#" data-toggle="tooltip" title="Add to Wishlist" class="wishlist"><i
                                                class="fa fa-heart"></i></a>--}}
                                </div>
                            </div>
                            <h6><a href="{{asset("/product/".$product['product_id'])}}">{{$product['name']}}</a></h6>

                            <div class="price">
                                <div>{{$product['price']}} GEL
                                    {{--<span class="label-tags">
                                        <span class="label label-default">-10%</span>
                                    </span>--}}
                                </div>
                                {{--<span class="price-old">${{$product['price']*1.1}}</span>--}}
                            </div>
                            <div class="rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-half-o"></i>
                                <a href="#">(0 შეფასება)</a>
                            </div>
                        </div>
                    </div>
                @endforeach

                <div class="col-xs-12 text-center">
                    <nav aria-label="Page navigation">
                        {!!$products->links()!!}
                        {{--<ul class="pagination">--}}
                        {{--<li class="disabled"><span>&laquo;</span></li>--}}
                        {{--<li class="disabled"><span>&lsaquo;</span></li>--}}
                        {{--<li class="active"><span>1</span></li>--}}
                        {{--<li><a href="#">2</a></li>--}}
                        {{--<li><a href="#">3</a></li>--}}
                        {{--<li><a href="#">&rsaquo;</a></li>--}}
                        {{--<li><a href="#">&raquo;</a></li>--}}
                        {{--</ul>--}}
                    </nav>
                </div>
            </div>
            <!-- End Product List -->

        </div>
    </div>
    <!-- End Main Content -->
@stop