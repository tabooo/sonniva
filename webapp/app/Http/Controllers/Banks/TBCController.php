<?php

namespace App\Http\Controllers\Banks;

use Illuminate\Routing\Controller;

class TBCController extends Controller
{
    static $host = 'https://api.tbcbank.ge';
    static $username = '0Pmju1TSeQ554ABAnhDZ45cRe1MZiRbY';
    static $password = 'ufjHqrANuRUJ2Dc2';
    static $merchantKey = '417875419-06220c98-a8d4-4eb1-9514-01004e70d7ef';
    static $campaignId = '529';

    public function __construct()
    {
        $this->response = new \App\Library\ResponseDescription();
    }

    static function getTbcOauthToken()
    {
        $url = TBCController::$host . '/oauth/token';
        $body = 'grant_type=client_credentials&scope=online_installments';
        $headers = [
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization: Basic ' . base64_encode(TBCController::$username . ":" . TBCController::$password)
        ];

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response, true);
    }

    static function createTbcInstallmentApplication($data)
    {
        $authResponse = TBCController::getTbcOauthToken();
        $url = TBCController::$host . '/v1/online-installments/applications';
        $headers = array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $authResponse['access_token'],
        );

        $responseHeaders = [];

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // this function is called by curl for each header received
        curl_setopt($ch, CURLOPT_HEADERFUNCTION,
            function ($curl, $header) use (&$responseHeaders) {
                $len = strlen($header);
                $header = explode(':', $header, 2);
                if (count($header) < 2) // ignore invalid headers
                    return $len;

                $responseHeaders[strtolower(trim($header[0]))][] = trim($header[1]);

                return $len;
            }
        );
        $response = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $response = json_decode($response, true);

        if ($httpcode == 201) {
            return array("sessionId" => $response['sessionId'], 'location' => $responseHeaders['location'][0]);
        }
        return $response;
    }

    static function confirmSession($sessionId)
    {
        $authResponse = TBCController::getTbcOauthToken();
        $url = TBCController::$host . "/v1/online-installments/applications/" . $sessionId . "/confirm";
        $headers = array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $authResponse['access_token'],
        );

        $body = ['merchantKey' => TBCController::$merchantKey];
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return ["body" => $response, "status" => $httpcode];
    }

    static function cancelSession($sessionId)
    {
        $authResponse = TBCController::getTbcOauthToken();
        $url = TBCController::$host . "/v1/online-installments/applications/$sessionId/cancel";
        $headers = array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $authResponse['access_token'],
        );

        $body = ['merchantKey' => TBCController::$merchantKey];
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return ["body" => $response, "status" => $httpcode];
    }

    static function statusSession($sessionId)
    {
        $authResponse = TBCController::getTbcOauthToken();
        $url = TBCController::$host . "/v1/online-installments/applications/$sessionId/status";
        $headers = array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $authResponse['access_token'],
        );

        $body = ['merchantKey' => TBCController::$merchantKey];
        $ch = curl_init($url);
        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
//        curl_setopt($ch, CURLOPT_POs, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return ["body" => $response, "status" => $httpcode];
    }
}
