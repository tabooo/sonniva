<?php

namespace App\Http\Controllers\Banks;

use Illuminate\Routing\Controller;

class CrystalController extends Controller
{
    static $host = 'https://crystalone.ge/online_installment';
    static $secretHash = '8QMUOM8Gs5wKfooOcgOdN3q6AM142PS873qkGtcRUGxyVT3ByuFeDtTAKLkw5uvT';
    static $vendorId = '6362';

    static function createOnlineInstallment($cart)
    {
        $hash = md5(json_encode($cart) . CrystalController::$secretHash);
        $url = CrystalController::$host . '/' . CrystalController::$vendorId . '?cart=' . urlencode(json_encode($cart)) . '&cart_hash=' . $hash;

        return $url;
    }

}
