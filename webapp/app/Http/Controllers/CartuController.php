<?php

namespace App\Http\Controllers;

use App\Model\Payment;
use App\Model\Sell;
use App\Model\SellItem;
use App\Model\ShoppingCart;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class CartuController extends Controller
{
    function callback(Request $request)
    {
        $req = $request->all();

        $fp = fopen(asset("/banks/cartu/CartuBankKEY.pem"), "r");
        $cert = fread($fp, 8192);
        fclose($fp);

        /*
         ბანკიდან callback url ზე შემოსული მოთხოვნების log.txt ფაილში ჩაწერა
         გაანთავისუფლეთ კოდი კომენტარებისგან თუ გსურთ გააქტიურება
        */

        $h = fopen(public_path('banks/cartu/log.txt'), 'a');
        fwrite($h, 'LARAVEL: ' . print_r($req, true) . "\n");
        fclose($h);

        $user = Auth::guard()->user();

        /*  xml პარამეტრების გადატანა იდენტიფიკატორებში  */

        $xml = xml_parser_create('UTF-8');
        xml_parse_into_struct($xml, $_POST['ConfirmRequest'], $vals);
        xml_parser_free($xml);

        foreach ($vals as $data) {
            if ($data['tag'] == 'STATUS')
                $Status = $data['value'];
            if ($data['tag'] == 'PAYMENTID')
                $PaymentId = $data['value'];
            if ($data['tag'] == 'PAYMENTDATE')
                $PaymentDate = $data['value'];
            if ($data['tag'] == 'TRANSACTIONID')
                $TransactionId = $data['value'];
            if ($data['tag'] == 'AMOUNT')
                $Amount = $data['value'];
            if ($data['tag'] == 'REASON')
                $Reason = $data['value'];
            if ($data['tag'] == 'CARDTYPE')
                $CardType = $data['value'];

        }

        if ($Status == 'C') {
            return 1;
        }

        if ($Status == 'Y') {

            Payment::create([
                'transaction_id' => $TransactionId,
                'payment_id' => $PaymentId,
                'payment_date' => $PaymentDate,
                'amount' => $Amount,
                'card_type' => $CardType,
                'status' => $Status,
                'reason' => $Reason,
            ]);

            $sell = Sell::create([
                'transaction_id' => $TransactionId,
                'user_id' => $user['id'],
                'whole_price' => $Amount,
                'bonus' => $Amount * 0.01,
                'city_id' => session('city_id'),
                'address' => session('address'),
                'state' => 1,
            ]);

            $decrypted = openssl_decrypt(base64_decode($TransactionId), 'aes-256-cbc', "tabo", 0, substr(hash('sha256', "tabo"), 0, 16));

            $h = fopen(public_path('banks/cartu/log.txt'), 'a');
            fwrite($h, 'LARAVEL: ' . $decrypted . "\n");
            fclose($h);

            $wholePrice = 0;
            $content = unserialize(ShoppingCart::where('identifier', $decrypted)->first()['content']);
            foreach ($content as $key => $value) {
                $wholePrice += $value->subtotal;

                SellItem::create([
                    'sell_id' => $sell['sell_id'],
                    'product_id' => $value->id,
                    'product_price' => $value->price,
                    'amount' => $value->qty,
                    'whole_price' => $value->subtotal,
                    'state' => 1,
                ]);
            }

            ShoppingCart::where('identifier', $decrypted)->delete();
            $wholePrice += session('shipping_price');

            /*if ($wholePrice != $Amount) {
                return 0;
            }*/

            //Cart::destroy();

            return 1;
        }
        return 0;
    }
}
