<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Library\Helpers;
use App\Library\ResponseDescription;
use App\Model\City;
use App\Model\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CitiesController extends Controller
{

    private $response = NULL;

    public function __construct()
    {
        $this->response = new ResponseDescription();
    }

    public function getCities()
    {
        $cities = City::where('state', 1)->get();

        $resp = json_encode($cities);

        return $resp;
    }

    public function addCity(Request $request)
    {
        $validator = Validator::make($data = $request->all(), City::$rules);
        if (!$validator->fails()) {
            $data['name_ge'] = Helpers::strip_html_php($data['name_ge']);
            $data['name_en'] = Helpers::strip_html_php($data['name_en']);
            $data['name_ru'] = Helpers::strip_html_php($data['name_ru']);
            $data['price'] = Helpers::strip_html_php($data['price']);
            if (array_key_exists('city_id', $data) && $data['city_id'] != null) {
                $this->updateCity($data);
            } else {
                $city = City::create($data);

                if ($city->city_id) {
                    $this->response->success = true;
                    $this->response->message = "successful";
                    $this->response->data = $city;
                }
            }
        }
        return $this->response->toJson();
    }

    public function updateCity($data)
    {
        $city = City::find($data['city_id']);

        if ($city && $city->update($data)) {
            $this->response->success = true;
            $this->response->message = "successful";
            $this->response->data = [$city];
        }
        return $this->response->toJson();
    }

    public function removeCity($city_id)
    {
        $city = City::find($city_id);

        if ($city) {
            $city->state = 0;
            $city->update();
            $this->response->success = true;
            $this->response->message = "successful";
            $this->response->data = [$city];
        }

        return $this->response->toJson();
    }
}