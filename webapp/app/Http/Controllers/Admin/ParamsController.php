<?php

/**
 * Created by PhpStorm.
 * User: ika
 * Date: 10/24/15
 * Time: 6:30 PM
 */
namespace App\Http\Controllers\Admin;

use App\Model\Params;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class ParamsController extends Controller
{

    private $response = NULL;

    public function __construct()
    {
        $this->response = new \App\Library\ResponseDescription();
    }

    public function getParams()
    {
        $pages = Params::get();

        $resp = json_encode($pages);

        return $resp;
    }

    public function updateParam(Request $request)
    {
        $validator = Validator::make($data = $request->all(), Params::$rules);
        $data['param_id'] = \App\Library\Helpers::strip_html_php($data['param_id']);
        $data['value'] = \App\Library\Helpers::strip_html_php($data['value']);

        $album =Params::find($data['param_id']);

        if ($album->update($data)) {
            $this->response->setSuccess();
            $this->response->data = [$data];
        }

        return $this->response->toJson();
    }
}