<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Image;
use App\Model\ProductFile;

class UploadController extends Controller
{
    private $extList = [];
    private $thumb = null;
    private $destinationPath = null;

    public function __construct()
    {
        $this->middleware('auth:admin');

        $this->thumb = 'thumb';
        $this->destinationPath = public_path() . '/files/product_files/';
        $this->extList = ['jpg', 'jpeg', 'png', 'bmp', 'gif', 'svg'];
    }

    public function getFiles($productID)
    {
        $folder = ProductFile::where('product_id', $productID)
            ->where('state_id', 1)->get();

        return $folder->toJson();
    }

    public function upload($path = null, Request $request)
    {
        $uploadedCount = 0;

        $rules = [
            'image' => 'required|file|
                mimes:jpg,jpeg,png,bmp,gif,svg|
                mimetypes:image/jpeg,image/png,image/bmp,image/gif,image/svg+xml'
        ];

        if ($request->hasFile('image')) {
            if ($this->makeDirectory($path)) {
                $files = $request->file('image');
                $filesCount = count($files);

                if ($filesCount > 10) {
                    return false;
                }

                foreach ($files as $file) {
                    $validator = Validator::make(['image' => $file], $rules);

                    if ($validator->passes()) {
                        $extension = strtolower($file->getClientOriginalExtension());

                        $uniqName = $this->uniqName();
                        $fileName = $uniqName . "." . $extension;
                        $originalFileName = $uniqName . '_original' . "." . $extension;

                        if (in_array($extension, $this->extList)) {
                            if ($file->move($this->destinationPath . $path, $fileName)) {
                                $this->optimizeQuality($this->destinationPath . $path . '/' . $fileName);

                                $this->copyFileToSaveOriginal($this->destinationPath . $path . '/' . $fileName,
                                    $this->destinationPath . $path . '/' . $originalFileName);

                                $this->resizeImageForThumb($this->destinationPath . $path, $fileName);
//                                $this->modifyImageForWeb($this->destinationPath . $path . '/' . $fileName);

                                $obj = ProductFile::create([
                                    'product_id' => $path,
                                    'file_name' => $fileName,
                                    'original_file_name' => $fileName . '_original',
                                    'is_cover' => 0
                                ]);

                                if ($obj && $obj->id) {
                                    $uploadedCount++;
                                }
                            }
                        }
                    }

                    if ($uploadedCount == $filesCount) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public function setCoverImage($productFileId)
    {
        ProductFile::find($productFileId)
            ->where('state_id', 1)
            ->update(['is_cover' => '1']);
    }

    public function removeDirectory($path)
    {
        if (empty($path)) {
            return false;
        }

        $dir = $this->destinationPath . $path;

        if (File::deleteDirectory($dir)) {
            ProductFile::where('product_id', $path)
                ->where('state_id', 1)
                ->update(['state_id' => '0']);

            return true;
        }

        return false;
    }

    public function removeFile($fileID)
    {
        if (empty($fileID)) {
            return false;
        }

        $file = ProductFile::find($fileID);

        if ($file) {
            $path = $this->destinationPath . $file->product_id . '/' . $file->file_name;
            $pathOriginal = $this->destinationPath . $file->product_id . '/' . $file->original_file_name;
            $thumbPath = $this->destinationPath . $file->product_id . '/' . $this->thumb . '/' . $file->file_name;

            File::delete($path);
            File::delete($pathOriginal);
            File::delete($thumbPath);

            $file->state_id = 0;
            $file->save();

            if (!File::exists($file)
                && !File::exists($thumbPath)
                && !File::exists($pathOriginal)
            ) {

                return true;
            }
        }

        return false;
    }

    private function makeDirectory($path)
    {
        if (empty($path)) {
            return false;
        }

        $dir = $this->destinationPath . $path;
        $thumbDir = $this->destinationPath . $path . '/' . $this->thumb;

        if (File::exists($dir)) {
            return true;
        }

        if (File::makeDirectory($dir, 0775)
            && File::makeDirectory($thumbDir, 0775)
        ) {

            return true;
        }

        return false;
    }

    private function uniqName()
    {
        return md5(microtime() . uniqid(rand(), TRUE));
    }

    private function optimizeQuality($path)
    {
        $img = Image::make($path);
        $img->save($path, 80);
    }

    private function cropImageForThumb($path)
    {
        $size = 100;
        $img = Image::make($path);

        $centreX = round($img->width() / 2);
        $centreY = round($img->height() / 2);
        $x = $centreX - $size / 2;
        $y = $centreY - $size / 2;

        if ($img->width() >= $size && $img->height() >= $size) {
            $img->crop($size, $size, $x, $y);
            $img->save($path);
        }
    }

    private function resizeImageForThumb($path, $fileName)
    {
        $size = 120;
        $thumb = $path . '/' . $this->thumb . '/' . $fileName;

        $img = Image::make($path . '/' . $fileName);

        $img->resize($size, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        $img->save($thumb);

        $this->cropImageForThumb($thumb);
    }

    public function modifyImageForWeb($path)
    {
        $sizeW = 435;
        $sizeH = 600;

        $img = Image::make(public_path() . '/assets/images/white_bg.jpg');
        $imgResize = Image::make($path);

        if ($imgResize->width() >= $imgResize->height()) {
            $imgResize->resize($sizeW, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $imgResize->save();
        } else if ($imgResize->height() > $imgResize->width()) {
            $imgResize->resize(null, $sizeH, function ($constraint) {
                $constraint->aspectRatio();
            });

            $imgResize->save();
        }

        $img->insert($path, 'center');
        $img->save($path);
    }

    private function copyFileToSaveOriginal($file, $dest)
    {
        if (File::copy($file, $dest)) {
            return true;
        }

        return false;
    }
}
