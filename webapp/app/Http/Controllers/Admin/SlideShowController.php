<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Library\Helpers;
use App\Library\PostTypes;
use App\Model\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SlideShowController extends Controller
{

    private $response = NULL;

    public function __construct()
    {
        $this->response = new \App\Library\ResponseDescription();
    }

    public function geSlideShows()
    {
        $slideshow = Post::where('type', PostTypes::slideshow)->where('state', 1)->get();

        $resp = json_encode($slideshow);

        return $resp;
    }

    public function addSlide(Request $request)
    {
        $validator = Validator::make($data = $request->all(), Post::$rules);
        if (!$validator->fails()) {
            $data['title_ge'] = Helpers::strip_html_php($data['title_ge']);
            $data['title_en'] = Helpers::strip_html_php($data['title_en']);
            $data['title_ru'] = Helpers::strip_html_php($data['title_ru']);
            $data['type'] = PostTypes::slideshow;
            if (array_key_exists('post_id', $data) && $data['post_id'] != null) {
                $this->updateSlide($data);
            } else {
                $slide = Post::create($data);

                if ($slide->post_id) {
                    $this->response->success = true;
                    $this->response->message = "successful";
                    $this->response->data = $slide;
                }
            }
        }
        return $this->response->toJson();
    }

    public function updateSlide($data)
    {
        $slide = Post::find($data['post_id']);

        if ($slide && $slide->update($data)) {
            $this->response->success = true;
            $this->response->message = "successful";
            $this->response->data = [$slide];
        }
        return $this->response->toJson();
    }

    public function removeSlide($post_id)
    {
        $post = Post::find($post_id);

        if ($post) {
            $post->state = 0;
            $post->update();
            $this->response->success = true;
            $this->response->message = "successful";
            $this->response->data = [$post];
        }

        return $this->response->toJson();
    }
}