<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

//DB::enableQueryLog();
//dd(DB::getQueryLog());

class NewsController extends Controller
{

    private $response = NULL;

    public function __construct()
    {
        $this->response = new \App\Library\ResponseDescription();
    }


    public static function getNews(Request $request)
    {
        $rules = [
            'category' => 'integer|min:0',
            'tag' => 'integer|min:0',
            'text' => 'string'
        ];

        $validator = Validator::make($args = $request->all(), $rules);

        if (!$validator->fails()) {
            $query = \App\Model\Post::where('type', 1);
            $query->with('tags');
            $query->with('categories');
            if (array_key_exists('category', $args) && !empty($args['category'])) {
                $args['category'] = \App\Library\Helpers::strip_html_php($args['category']);
                $query->whereHas('categories', function ($query) use ($args) {
                    $query->where('web_news_categories.category_id', $args['category']);
                });
            }

            if (array_key_exists('tag', $args) && !empty($args['tag'])) {
                $args['tag'] = \App\Library\Helpers::strip_html_php($args['tag']);
                $query->whereHas('tags', function ($query) use ($args) {
                    $query->where('web_news_tags.tag_id', $args['tag']);
                });
            }

            if (array_key_exists('text', $args) && !empty($args['text'])) {
                $args['text'] = \App\Library\Helpers::strip_html_php($args['text']);

                $query->orWhere(function ($localQuery) use ($args) {
                    $localQuery->orWhere('description_ge', 'LIKE', '%' . $args['text'] . '%')
                        ->orWhere('description_en', 'LIKE', '%' . $args['text'] . '%');
                });
            }

            $news = $query->where('disabled', 0)->where('state', 1)
                ->orderBy('published_at', 'DESC')->paginate(30);
            $news = self::modifyNewsData($news);

            return $news->toJson();
        }

        return [];
    }

    public function getTags()
    {
        $category = \App\Model\Tags::where('disabled', 0)->where('state', 1)->get();

        return $category->toJson();
    }

    public function addTag(Request $request)
    {
        $validator = Validator::make($data = $request->all(), \App\Model\Tags::$rules);

        if (!$validator->fails()) {
            $data['name_ge'] = \App\Library\Helpers::strip_html_php($data['name_ge']);
            $data['name_en'] = \App\Library\Helpers::strip_html_php($data['name_en']);
            $data['name_ru'] = \App\Library\Helpers::strip_html_php($data['name_ru']);
            $data['disabled'] = \App\Library\Helpers::strip_html_php($data['disabled']);

            if ($request->has('tag_id')) {
                $this->updateTag($data);
            } else {
                $tag = \App\Model\Tags::create($data);

                if ($tag->tag_id) {
                    $data['link'] = '/news?tag=' . $tag->tag_id;
                    $data['tag_id'] = $tag->tag_id;
                    $tag = \App\Model\Tags::find($tag->tag_id);
                    $tag->update($data);

                    $this->response->success = true;
                    $this->response->message = "successful";
                    $this->response->data = $tag;
                }
            }
        }

        return $this->response->toJson();
    }

    public function add(Request $request)
    {
        $validator = Validator::make($data = $request->all(), \App\Model\Post::$rules);

        if (!$validator->fails()) {
            $data['img_path'] = \App\Library\Helpers::strip_html_php($data['img_path']);

            $data['title_ge'] = \App\Library\Helpers::strip_html_php($data['title_ge']);
            $data['title_en'] = \App\Library\Helpers::strip_html_php($data['title_en']);
            $data['title_ru'] = \App\Library\Helpers::strip_html_php($data['title_ru']);

            $data['keyword_ge'] = \App\Library\Helpers::strip_html_php($data['keyword_ge']);
            $data['keyword_en'] = \App\Library\Helpers::strip_html_php($data['keyword_en']);
            $data['keyword_ru'] = \App\Library\Helpers::strip_html_php($data['keyword_ru']);

            $data['google_description_ge'] = \App\Library\Helpers::strip_html_php($data['google_description_ge']);
            $data['google_description_en'] = \App\Library\Helpers::strip_html_php($data['google_description_en']);
            $data['google_description_ru'] = \App\Library\Helpers::strip_html_php($data['google_description_ru']);

            $data['description_ge'] = \App\Library\Helpers::html_escape($data['description_ge']);
            $data['description_en'] = \App\Library\Helpers::html_escape($data['description_en']);
            $data['description_ru'] = \App\Library\Helpers::html_escape($data['description_ru']);

            $data['description_ge_short'] = \App\Library\Helpers::html_escape($data['description_ge_short']);
            $data['description_en_short'] = \App\Library\Helpers::html_escape($data['description_en_short']);
            $data['description_ru_short'] = \App\Library\Helpers::html_escape($data['description_ru_short']);

            $data['type'] = 1;

            $data['published_at'] = date('Y-m-d H:i:s', $data['published_at'] / 1000);
            if ($request->has('post_id')) {
                $this->update($data);
            } else {
                $news = \App\Model\Post::create($data);

                if ($news->post_id) {
                    $data['link'] = '/news/' . $news->post_id;
                    $data['post_id'] = $news->post_id;
                    $news = \App\Model\Post::find($news->post_id);
                    $news->update($data);
                    try {
                        $this->addArticleImages($data, $news->post_id);
                    } catch (Exception $e) {
                        $this->response->setMessage('image error');

                        return $this->response->toJson();
                    }
                    if ($this->syncTags($news, $data) && $this->syncCategories($news, $data)) {
                        $this->response->success = true;
                        $this->response->message = "successful";
                        $this->response->data = [$news];
                    }
                }
            }
        }

        return $this->response->toJson();
    }

    public function syncTags($news, $data)
    {
        if (is_array($data['tags'])
            && $news->tags()->sync($data['tags'])
        ) {

            return true;
        } else {
            return true;
        }

        return false;
    }

    public function syncCategories($news, $data)
    {
        if (is_array($data['categories'])
            && $news->categories()->sync($data['categories'])
        ) {

            return true;
        } else {
            return true;
        }

        return false;
    }

    public function updateTag($data)
    {
        $tag = \App\Model\Tags::find($data['tag_id']);

        if ($tag && $tag->update($data)) {
            $this->response->success = true;
            $this->response->message = "successful";
            $this->response->data = [$tag];
        }
    }

    public function update($data)
    {
        $news = \App\Model\Post::find($data['post_id']);

        if ($news && $news->update($data)) {
            try {
                $this->addArticleImages($data);
            } catch (Exception $e) {
                $this->response->setMessage('image error');

                return $this->response->toJson();
            }
            if ($this->syncTags($news, $data) && $this->syncCategories($news, $data)) {
                $this->response->success = true;
                $this->response->message = "successful";
                $this->response->data = [$news];
            }
        }
    }

    public function destroy($post_id)
    {
        $news = \App\Model\Post::find($post_id);

        if ($news) {
            $news->state = 0;

            if ($news->save()) {
                $this->response->success = true;
                $this->response->message = "successful";
                $this->response->data = [$news];
            }
        }

        return $this->response->toJson();
    }

    private static function modifyNewsData($news)
    {
        foreach ($news as $key => $value) {
            $value['description_ge'] = \App\Library\Helpers::htmlSpecDecode($value['description_ge']);
            $value['description_en'] = \App\Library\Helpers::htmlSpecDecode($value['description_en']);
            $value['description_ru'] = \App\Library\Helpers::htmlSpecDecode($value['description_ru']);
        }

        return $news;
    }

    private function addArticleImages($data, $post_id = NULL)
    {
        $insertList = [];

        if ($post_id != NULL) {
            $data['post_id'] = $post_id;
        }

        if (array_key_exists('gallery', $data) && is_array($data['gallery']) && count($data['gallery'])) {
            $gallery = $data['gallery'];

            foreach ($gallery as $value) {
                array_push($insertList, [
                    'article_id' => $data['post_id'],
                    'article_category_id' => 1,
                    'file_manager_id' => $value['file_manager_id']
                ]);
            }

            \DB::table('web_article_images')->insert($insertList);
            $dirName = $data['post_id'] . '_1';

            try {
                FolderController::resizeArticleImages($dirName, $gallery);
            } catch (Exception $e) {
                throw $e;
            }
        }
    }
}