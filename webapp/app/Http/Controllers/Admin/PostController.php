<?php
/**
 * Created by PhpStorm.
 * User: irakl
 * Date: 12/4/2016
 * Time: 5:00 AM
 */

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use \App\Model\Post;
use \App\Model\PostMeta;
use \App\Model\WebCategory;
use \App\Library\Helpers;
use \App\Library\ResponseDescription;

class PostController extends Controller
{
    private $response = NULL;

    public function __construct()
    {
        $this->response = new ResponseDescription();
    }

    public function getPosts(Request $request)
    {
        $rules = [
            /*'category' => 'integer|min:1',
            'text' => 'string'*/
        ];

        $validator = Validator::make($args = $request->all(), $rules);

        if (!$validator->fails()) {

            $query = Post::select()
                ->with('meta')
                ->where('type', 1)
                ->where('disabled', 0)
                ->where('state', 1);

            $query->with(['categories' => function ($q) {
                $q->where(WebCategory::$tbl . '.disabled', 0)
                    ->where(WebCategory::$tbl . '.state', 1);
            }]);

            if (array_key_exists('category', $args) && !empty($args['category'])) {
                $query->whereHas('categories', function ($q) use ($args) {
                    $q->where(WebCategory::$tbl . '.category_id', $args['category'])
                        ->where(WebCategory::$tbl . '.disabled', 0)
                        ->where(WebCategory::$tbl . '.state', 1);
                });
            }

            if (array_key_exists('text', $args) && !empty($args['text'])) {
                $args['text'] = Helpers::strip_html_php($args['text']);

                $query->where(function ($q) use ($args) {
                    $q->orWhere('title_ge', 'LIKE', '%' . $args['text'] . '%')
                        ->orWhere('title_en', 'LIKE', '%' . $args['text'] . '%')
                        ->orWhere('title_ru', 'LIKE', '%' . $args['text'] . '%')
                        ->orWhere('description_ge', 'LIKE', '%' . $args['text'] . '%')
                        ->orWhere('description_en', 'LIKE', '%' . $args['text'] . '%')
                        ->orWhere('description_ru', 'LIKE', '%' . $args['text'] . '%');
                });
            }

            $posts = $query->orderBy('published_at', 'DESC')
                ->paginate(30);

            $posts = $this->modifyData($posts);

            return $posts->toJson();
        }
    }

    public function addPost(Request $request)
    {
        $validator = Validator::make($data = $request->all(), Post::$rules);

        if (!$validator->fails()) {
            $data = $this->rmHtmlFromData($data);

            if ($request->has('post_id')) {
                $this->updatePost($data);
            } else {
                $post = Post::create($data);

                if ($post->post_id) {
                    $data['link'] = '/post/' . $post->post_id;
                    $data['post_id'] = $post->post_id;
                    $post->update($data);

                    if ($this->addPostMeta($data) && $this->syncCategories($post, $data)) {
                        $this->response->setSuccess();
                        $this->response->data = [$post];
                    }
                }
            }
        }

        return $this->response->toJson();
    }

    public function updatePost($data)
    {
        $post = Post::find($data['post_id']);
        unset($data['link']);

        if ($post && $post->update($data)) {

            if ($this->addPostMeta($data) && $this->syncCategories($post, $data)) {
                $this->response->setSuccess();
                $this->response->data = [$post];
            }
        }
    }

    public function destroy($post_id)
    {
        $post = Post::find($post_id);

        if ($post) {
            $post->state = 0;

            if ($post->save()) {

                PostMeta::where('post_id', $post_id)->delete();

                $this->response->setSuccess();
                $this->response->data = [$post];
            }
        }

        return $this->response->toJson();
    }

    private function addPostMeta($data)
    {
        $meta = [];

        if (array_key_exists('meta_id', $data)
            && array_key_exists('showInGallery', $data)
            && !empty($data['meta_id'])
        ) {
            $postMeta = PostMeta::find($data['meta_id']);
            $meta['meta_value'] = $data['showInGallery'];

            if ($postMeta && $postMeta->update($meta)) {
                return true;
            } else {
                return false;
            }

        } else if (array_key_exists('showInGallery', $data) && $data['showInGallery'] == 1) {
            $meta['post_id'] = $data['post_id'];
            $meta['meta_key'] = 'showInGallery';
            $meta['meta_value'] = $data['showInGallery'];

            if (PostMeta::create($meta)) {
                return true;
            } else {
                return false;
            }
        }

        return true;
    }

    private function syncCategories($post, $data)
    {
        if (is_array($data['categories'])
            && $post->categories()->sync($data['categories'])
        ) {

            return true;
        } else {
            return true;
        }

        return false;
    }

    private function modifyData($news)
    {
        foreach ($news as $key => $value) {
            $value['description_ge'] = Helpers::htmlSpecDecode($value['description_ge']);
            $value['description_en'] = Helpers::htmlSpecDecode($value['description_en']);
            $value['description_ru'] = Helpers::htmlSpecDecode($value['description_ru']);
        }

        return $news;
    }

    private function rmHtmlFromData($data)
    {
        $data['img_path'] = Helpers::strip_html_php($data['img_path']);

        $data['title_ge'] = Helpers::strip_html_php($data['title_ge']);
        $data['title_en'] = Helpers::strip_html_php($data['title_en']);
        $data['title_ru'] = Helpers::strip_html_php($data['title_ru']);

        $data['keyword_ge'] = Helpers::strip_html_php($data['keyword_ge']);
        $data['keyword_en'] = Helpers::strip_html_php($data['keyword_en']);
        $data['keyword_ru'] = Helpers::strip_html_php($data['keyword_ru']);

        $data['google_description_ge'] = Helpers::strip_html_php($data['google_description_ge']);
        $data['google_description_en'] = Helpers::strip_html_php($data['google_description_en']);
        $data['google_description_ru'] = Helpers::strip_html_php($data['google_description_ru']);

        $data['description_ge'] = Helpers::html_escape($data['description_ge']);
        $data['description_en'] = Helpers::html_escape($data['description_en']);
        $data['description_ru'] = Helpers::html_escape($data['description_ru']);

        $data['description_ge_short'] = Helpers::html_escape($data['description_ge_short']);
        $data['description_en_short'] = Helpers::html_escape($data['description_en_short']);
        $data['description_ru_short'] = Helpers::html_escape($data['description_ru_short']);

        $data['type'] = 1;
        $data['published_at'] = date('Y-m-d H:i:s', $data['published_at'] / 1000);

        return $data;
    }
}