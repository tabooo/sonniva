<?php

/**
 * Created by PhpStorm.
 * User: ika
 * Date: 10/24/15
 * Time: 6:30 PM
 */
namespace App\Http\Controllers\Admin;

use App\Model\WebCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class WebCategoryController extends Controller
{

    private $response = NULL;

    public function __construct()
    {
        $this->response = new \App\Library\ResponseDescription();
    }

    public function getCategories()
    {
        return WebCategory::treeFresh();
    }

    public function addCategory(Request $request)
    {
        $validator = Validator::make($data = $request->all(), WebCategory::$rules);

        if (!$validator->fails()) {
            $data['name_ge'] = \App\Library\Helpers::strip_html_php($data['name_ge']);
            $data['name_en'] = \App\Library\Helpers::strip_html_php($data['name_en']);
            $data['name_ru'] = \App\Library\Helpers::strip_html_php($data['name_ru']);
            $data['disabled'] = \App\Library\Helpers::strip_html_php($data['disabled']);
            $data['link'] = \App\Library\Helpers::strip_html_php($data['link']);
            $data['img_path'] = \App\Library\Helpers::strip_html_php($data['img_path']);
            $data['color'] = \App\Library\Helpers::strip_html_php($data['color']);

            if ($request->has('category_id')) {
                $this->updateCategory($data);
            } else {
                $category = WebCategory::create($data);

                if ($category->category_id) {
                    //$data['link'] = '/news?category=' . $category->category_id;
                    $data['category_id'] = $category->category_id;
                    $category = WebCategory::find($category->category_id);
                    $category->update($data);

                    $this->response->success = true;
                    $this->response->message = "successful";
                    $this->response->data = $category;
                }
            }
        }

        return $this->response->toJson();
    }

    public function updateCategory($data)
    {
        $category = WebCategory::find($data['category_id']);

        if ($category && $category->update($data)) {
            $this->response->success = true;
            $this->response->message = "successful";
            $this->response->data = [$category];
        }
    }

    public function destroyCategory($category_id)
    {
        $categorys = WebCategory::where('category_id', $category_id)->orWhere('parent_id', $category_id)->get();

        if ($categorys) {
            for ($i = 0; $i < count($categorys); $i++) {
                $categorys[$i]->state = 0;
                $categorys[$i]->update();
            }
            $this->response->success = true;
            $this->response->message = "successful";
            $this->response->data = [$categorys];
        }

        return $this->response->toJson();
    }
}