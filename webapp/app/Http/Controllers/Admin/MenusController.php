<?php

/**
 * Created by PhpStorm.
 * User: ika
 * Date: 10/24/15
 * Time: 6:30 PM
 */

namespace App\Http\Controllers\Admin;

use App\Model\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class MenusController extends Controller
{

    private $response = NULL;

    public function __construct()
    {
        $this->response = new \App\Library\ResponseDescription();
    }

    public function getMenus()
    {
        return Menu::treeFresh();
    }

    public function addMenu(Request $request)
    {
        $validator = Validator::make($data = $request->all(), Menu::$rules);

        if (!$validator->fails()) {
            $data['name_ge'] = \App\Library\Helpers::strip_html_php($data['name_ge']);
            $data['name_en'] = \App\Library\Helpers::strip_html_php($data['name_en']);
            $data['name_ru'] = \App\Library\Helpers::strip_html_php($data['name_ru']);
            $data['disabled'] = \App\Library\Helpers::strip_html_php($data['disabled']);

            if ($request->has('menu_id')) {
                $this->updateMenu($data);
            } else {
                $maxOrder = Menu::where('parent_id', $data['parent_id'])->orderBy('order', 'DESC')->first();
                $data['order'] = $maxOrder ? $maxOrder->order + 1 : 1;
                $menu = Menu::create($data);

                if ($menu->menu_id) {
                    $this->response->success = true;
                    $this->response->message = "successful";
                    $this->response->data = $menu;
                }
            }
        }

        return $this->response->toJson();
    }

    public function updateMenu($data)
    {
        $menu = Menu::find($data['menu_id']);

        if ($menu && $menu->update($data)) {
            $this->response->success = true;
            $this->response->message = "successful";
            $this->response->data = [$menu];
        }
    }

    public function destroy($menu_id)
    {
        $menus = Menu::where('menu_id', $menu_id)->orWhere('parent_id', $menu_id)->get();

        if ($menus) {
            for ($i = 0; $i < count($menus); $i++) {
                if ($menus[$i]->editable == 1) {
                    $menus[$i]->state = 0;
                    $menus[$i]->update();
                }
            }
//            if ($menus->update()) {
            $this->response->success = true;
            $this->response->message = "successful";
            $this->response->data = [$menus];
//            }
        }

        return $this->response->toJson();
    }

    public function changeOrder($menu_id, $direction)
    {
        $menu = Menu::find($menu_id);
        $friends = Menu::where('parent_id', $menu->parent_id)->where('state', 1)->orderBy('order', 'ASC')->get();

        if ($friends) {
            $ind = 0;
            for ($i = 0; $i < count($friends); $i++) {
                if ($direction == "up") {
                    if ($friends[$i]->order < $menu->order) {
                        $ind = $i;
                    }
                } else {
                    if ($friends[$i]->order > $menu->order) {
                        $ind = $i;
                        break;
                    }
                }
            }
            $o = $menu->order;
            $menu->order = $friends[$ind]->order;
            $friends[$ind]->order = $o;
            $menu->update();
            $friends[$ind]->update();
            $this->response->success = true;
            $this->response->message = "successful";
            $this->response->data = [$menu];
        }

        return $this->response->toJson();
    }
}
