<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\UserTree;
use App\User;
use Illuminate\Http\Request;

class PublicUserController extends Controller
{

    private $response = NULL;

    public function __construct()
    {
        $this->response = new \App\Library\ResponseDescription();
    }

    public function getPublicUsers(Request $request)
    {
        $query = User::select()
            ->where('state_id', 1);

        if ($request['name']) {
            $query->where('name', 'LIKE', '%' . $request['name'] . '%');
        }

        if ($request['last_name']) {
            $query->where('last_name', 'LIKE', '%' . $request['name'] . '%');
        }

        return $query->orderBy('name', 'ASC')->get();
    }

    public function getPublicUsersTree(Request $request)
    {
        $query = UserTree::select();

        if ($request['parent_id'] == null) {
            $query->whereNull('parent_id');
        } else {
            $query->where('parent_id', $request['parent_id']);
        }


        return $query->with('user')->get();
    }

    public function removePublicUsers($id)
    {

        //TODO
        return;
        $user = User::find($id);

        if ($user) {
            $user->is_active = 0;
            $user->update();
            $this->response->success = true;
            $this->response->message = "successful";
            $this->response->data = [$user];
        }

        return $this->response->toJson();
    }
}