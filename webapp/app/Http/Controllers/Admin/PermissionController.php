<?php
/**
 * Created by PhpStorm.
 * User: Irakli Gogiashvili
 * Date: 5/3/2017
 * Time: 5:15 PM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Library\ResponseDescription;
use App\Model\Permission;
use App\Model\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PermissionController extends Controller
{
    protected $response = null;

    public function __construct()
    {
        $this->middleware('auth:admin');

        $this->response = new ResponseDescription();
    }

    public function getRoles()
    {
        $user = Role::orderBy('updated_at', 'DESC')->get();

        return $user->toJson();
    }

    public function getPermissions()
    {
        $user = Permission::orderBy('updated_at', 'DESC')->get();

        return $user->toJson();
    }

    public function addRole(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, Role::$rules);

        if (!$validator->fails()) {
            $role = Role::create($data);

            if ($role) {
                if (array_key_exists('permissions', $data)
                    && is_array($data['permissions'])
                    && count($data['permissions']) > 0
                ) {
                    $list = [];
                    $permissions = $data['permissions'];

                    foreach ($permissions as $key => $value) {
                        $permission = Permission::find($value);

                        if ($permission) {
                            array_push($list, $permission);
                        }
                    }

                    if (count($list) > 0) {
                        $role->attachPermissions($list);
                    }
                }

                $this->response->setSuccess();
                $this->response->setData($role);
            }
        }

        return $this->response->toJson();
    }

    public function addPermission(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, Permission::$rules);

        if (!$validator->fails()) {
            $permission = Permission::create($data);

            if ($permission) {
                $this->response->setSuccess();
                $this->response->setData($permission);
            }
        }

        return $this->response->toJson();
    }
}