<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Library\ResponseDescription;
use Illuminate\Http\Request;
use App\Admin;
use App\Model\Role;
use Illuminate\Support\Facades\Validator;

class AdminPermissionController extends Controller
{
    protected $response = null;

    public function __construct()
    {
        $this->middleware('auth:admin');

        $this->response = new ResponseDescription();
    }

    public function getUsers()
    {
        $user = Admin::where('state_id', 1)
            ->orderBy('updated_at', 'DESC')
            ->paginate(50);

        return $user->toJson();
    }

    public function getUsersWithRoles()
    {
        $user = Admin::with('roles.perms')
            ->where('state_id', 1)
            ->paginate(50);

        return $user->toJson();
    }

    public function getUserRoles($userId)
    {
        $user = Admin::with('roles.perms')
            ->where('id', $userId)
            ->where('state_id', 1)
            ->get();

        return $user->toArray();
    }

    public function getUserPermissions($userId)
    {
        $permissions = [];
        $list = $this->getUserRoles($userId);

        foreach ($list as $key => $value) {
            foreach ($value["roles"] as $k => $role) {
                if (array_key_exists('perms', $role) && is_array($role["perms"]) && count($role["perms"]) > 0) {
                    $permissions = array_merge($permissions, $role["perms"]);
                }
            }
        }

        return $permissions;
    }

    public function attachRoleToUser(Request $request)
    {
        $rules = [
            'userId' => 'required|integer|min:0',
            'roleId' => 'required|integer|min:0',
            'checked' => 'required|integer|min:1|max:2'
        ];

        $data = $request->all();
        $validator = Validator::make($data, $rules);

        if (!$validator->fails()) {
            $user = Admin::find($data['userId']);
            $role = Role::find($data['roleId']);

            if ($user && $role) {
                if ($data['checked'] == 1) {

                    $user->attachRole($role);
                    $this->response->setSuccess();
                } else if ($data['checked'] == 2) {

                    $user->detachRole($role);
                    $this->response->setSuccess();
                }
            }
        }

        return $this->response->toJson();
    }
}