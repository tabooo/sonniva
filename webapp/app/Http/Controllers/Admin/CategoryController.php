<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Library\ResponseDescription;
use App\Model\Category;
use App\Model\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class CategoryController extends Controller
{
    private $response = NULL;

    public function __construct()
    {
        $this->response = new \App\Library\ResponseDescription();
    }

    public function getCategoriesTree()
    {
        $parentCategories = Category::whereNull('parent_id')->where('state_id', 1)
            ->orderBy('order', 'ASC')->get();
        $this->getChildrens($parentCategories);
        return $parentCategories->toJson();
    }

    public function getCategoriesFlat()
    {
        $parentCategories = Category::where('state_id', 1)->get();
        return $parentCategories->toJson();
    }

    public function getChildrens($parents)
    {
        foreach ($parents as $parent) {
            $childrens = Category::where('parent_id', $parent['category_id'])->where('state_id', 1)
                ->orderBy('order', 'ASC')->get();
            $parent['childrenObjects'] = $childrens;
            $this->getChildrens($childrens);
        }
    }

    public function getCategories(Request $request)
    {
        $categoryId = $request['categoryId'];

        if ($categoryId) {
            return Category::where('parent_id', $request['categoryId'])->get();
        }

        return Category::whereNull('parent_id')->get()->toJson();
    }


    public function getAllCategories()
    {
        return Category::where('state_id', 1)->get();
    }

    public function getParents($parentId)
    {

        $parent = null;

        if ($parentId) {
            $parent = Category::where('category_id', $parentId)->where("state_id", 1)->first();
        }

        if (!$parent) {
            return [];
        }

        $childCategories = [];
        array_push($childCategories, $parent['category_id']);
        array_merge($childCategories, $this->getParents($parent['parent_id']));

        return $childCategories;
    }


    public function setParents()
    {
        $allCategories = $this->getAllCategories();

        foreach ($allCategories as $category) {

            $parents = $this->getParents($category['parent_id']);

            if (!$parents && count($parents) < 1) {
                $category['parents'] = null;
            } else {

                $category['parents'] = implode(", ", $parents);;
            }
            $category->save();
        }
    }


    public function setCategoryChindrens()
    {
        $allCategories = $this->getAllCategories();

        foreach ($allCategories as $category) {

            $childrensIds = $this->getChildCategories($category['category_id']);

            if (!$childrensIds || count($childrensIds) < 1) {

                $category['childrens'] = null;
            } else {

                $k = '';
                $numItems = count($childrensIds);
                $i = 0;
                foreach ($childrensIds as $id) {
                    $i++;
                    $k .= $id['category_id'];
                    if ($i < $numItems) {
                        $k .= ',';
                    }
                }
                $category['childrens'] = $k;
            }
            $category->save();
        }
    }

    public function getChildCategories($parent)
    {

        $categories = Category::select('category_id')->where("parent_id", $parent)->where("state_id", 1)->get();

        if (!$categories || count($categories) < 1) {
            return [];
        }

        $childCategories = [];

        foreach ($categories as $child) {

            $c = $this->getChildCategories($child['category_id']);
            array_merge((array)$childCategories, (array)$c);
        }

        array_merge((array)$categories, (array)$childCategories);

        return $categories;
    }


    public function addCategory(Request $request)
    {

        $validator = Validator::make($data = $request->all(), [
            //'CATEGORY_ID' => 'integer',
            'category_name' => 'required',
        ]);

        $response = new ResponseDescription();

        if (!$validator->fails()) {

            if ($request->has('category_id')) {
                $categoryId = $data['category_id'];
                $category = Category::find($categoryId);
                $category['category_name'] = $data['category_name'];
                $category->update();
                $response->setData($category);
            } else {
                $maxOrder = Category::where('parent_id', $data['parent_id'])->orderBy('order', 'DESC')->first();
                $data['order'] = $maxOrder ? $maxOrder->order + 1 : 1;

                $obj = new Category($data);
                $obj['state_id'] = 1;
                $obj->save();
                $this->setParents();
                $response->setData($obj);
            }
            $response->setSuccess();
        }

        return $response->toJson();
    }

    public function removeCategory($categoryId)
    {
        $response = new ResponseDescription();
        $productCount = Product::where("category_id", $categoryId)->where("state_id", 1)->count();

        if ($productCount > 0) {
            $response->setMessage("ეს კატეგორია დამატებულია პროდუცტებში");
            return $response->toJson();
        }
        $category = Category::find($categoryId);

        if ($category) {
            $category['state_id'] = 2;
            // $category['CANCEL_DATE'] = date('Y-m-d H:i:s');
            $category->update();
            $this->setParents();
            $this->setCategoryChindrens();
            $response->setSuccess();
            $response->setData($category);
        }

        return $response;
    }

    public function changeOrder($category_id, $direction)
    {
        $category = Category::find($category_id);
        $friends = Category::where('parent_id', $category->parent_id)->where('state_id', 1)->orderBy('order', 'ASC')->get();

        if ($friends && count($friends) > 0) {
            $ind = -1;
            for ($i = 0; $i < count($friends); $i++) {
                if ($direction == "up") {
                    if ($friends[$i]->order < $category->order) {
                        $ind = $i;
                    }
                } else {
                    if ($friends[$i]->order > $category->order) {
                        $ind = $i;
                        break;
                    }
                }
            }
            if ($ind > -1) {
                $o = $category->order;
                $category->order = $friends[$ind]->order;
                $friends[$ind]->order = $o;
                $category->update();
                $friends[$ind]->update();
            }
            $this->response->success = true;
            $this->response->message = "successful";
            $this->response->data = [$category];
        }

        return $this->response->toJson();
    }
}
