<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Library\Helpers;
use App\Model\Language;
use Symfony\Component\HttpFoundation\Request;
use \App\Library\ResponseDescription;


class LanguageController extends Controller
{
    private $response = NULL;

    public function __construct()
    {
        $this->response = new ResponseDescription();
    }

    public function getLanguage(Request $request)
    {
        $langs = Language::get();
        $arr = [];
        foreach ($langs as $lang) {
            if (array_key_exists('lang', $request->all())) {
                if ($request['lang'] == 'ka' || $request['lang'] == "") {
                    $arr[$lang['key']] = $lang['text_ka'];
                } else if ($request['lang'] == 'en') {
                    $arr[$lang['key']] = $lang['text_en'];
                } else if ($request['lang'] == 'ru') {
                    $arr[$lang['key']] = $lang['text_ru'];
                } else if ($request['lang'] == 'tr') {
                    $arr[$lang['key']] = $lang['text_tr'];
                }
            }
        }
        return $arr;
    }

    public function getAllLanguage(Request $request)
    {
        $args = $request->all();
        $query = Language::select();
        if (array_key_exists('search', $args)) {
            $query->where(function ($localQuery) use ($args) {
                $search = Helpers::strip_html_php($args['search']);
                $localQuery->orWhere('text_ka', 'LIKE', '%' . $search . '%')
                    ->orWhere('text_en', 'LIKE', '%' . $search . '%')
                    ->orWhere('text_ru', 'LIKE', '%' . $search . '%')
                    ->orWhere('text_tr', 'LIKE', '%' . $search . '%');
            });
        }
        return $query->orderBy('key', 'DESC')->get()->toJson();
    }

    public function saveLang(Request $request)
    {
        $data = $request->all();
        $language = Language::find($data['lang_id']);
        if ($language && $language->update($data)) {

            $this->response->setSuccess();
            $this->response->data = [$language];
        }
        return $this->response->toJson();
    }
}
