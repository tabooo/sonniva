<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Model\Branch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Library\ResponseDescription;


class BranchController extends Controller
{

    public function getBranches()
    {
        return Branch::where('branch_state_id', 1)->get()->toJson();
    }

    public function addBranche(Request $request)
    {

        $response = new ResponseDescription();

        $validator = Validator::make($data = $request->all(), [
            'branch_name' => 'required|max:80'
        ]);

        if (!$validator->fails()) {
            $obj = new Branch($data);
            $obj['branch_state_id'] = 1;
            $obj->save();

            $response->setSuccess();
            $response->setData($obj);
        }

        return $response->toJson();
    }
}
