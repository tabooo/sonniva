<?php

namespace App\Http\Controllers\Admin;


use App\Model\Menu;
use Auth;
use App\Http\Controllers\Controller;
use App\Library\Helpers;
use App\Library\ResponseDescription;
use App\Model\Category;
use App\Model\Product;
use App\Model\ProductFile;
use App\Model\ProductInfo;
use App\Model\ProductInfoTypes;
use App\Model\Recieving;
use App\Model\RecievingProduct;
use App\Model\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class ProductController extends Controller
{
    private $response = NULL;

    private $currentUser;

    public function __construct()
    {
        $this->response = new \App\Library\ResponseDescription();
        $this->currentUser = Auth::guard('admin')->user();
    }

    public function getProducts(Request $request)
    {
        $products = [];

        $resp = json_encode(((object)['data' => [], 'total' => 0]));

        $rules = [
//            'product_id' => 'integer',
//            'category_id' => 'integer'
        ];

        $validator = Validator::make($args = $request->all(), $rules);

        if (!$validator->fails()) {

            $query = Product::select()->where('state_id', 1);

            if (array_key_exists('name', $args) && !empty($args['name'])) {
                $args['name'] = Helpers::strip_html_php($args['name']);
                $query->where('name', 'LIKE', '%' . $args['name'] . '%');
            }

            if (array_key_exists('barcode', $args) && !empty($args['barcode'])) {
                $args['barcode'] = Helpers::strip_html_php($args['barcode']);
                $query->where('barcode', $args['barcode']);
            }

            if (array_key_exists('product_id', $args) && !empty($args['product_id'])) {
                $args['product_id'] = Helpers::strip_html_php($args['product_id']);
                $query->where('product_id', $args['product_id']);
            }


            if (array_key_exists('category_id', $args) && !empty($args['category_id'])) {

                $categoryId = $args['category_id'];
                $category = Category::find($categoryId);
                $ids = $category['childrens'];

                if (!$ids) {
                    $ids = $categoryId;
                } else {
                    $ids .= "," . $categoryId;
                }

                $categoryIds = explode(',', $ids);

                $query->whereIn('category_id', $categoryIds);
            }

            $limit = 50;
            if (array_key_exists('limit', $args) && !empty($args['limit'])) {
                $limit = $args['limit'];
            }

            $total = $query->count();
            $products = $query->orderBy('name', 'DESC')->skip($args['start'])->take($limit)->get();
            $products = self::modifyProductsData($products);
            $resp = json_encode(((object)['data' => $products, 'total' => $total]));
        }

        return $resp;
    }

    public function getProductInfo($productId)
    {
        return ProductInfo::where('state_id', 1)->where('product_id', $productId)
            ->orderBy('order', 'ASC')->get()->toJson();
    }

    public function getProductInfoTypes(Request $request)
    {

        $query = ProductInfoTypes::select();

        if (array_key_exists('name', $request) && !empty($request['name'])) {

            $request['name'] = Helpers::strip_html_php($request['name']);
            $query->where('product_info_type_name', 'LIKE', $request['name'] . '%');
        }
        return $query->orderBy('product_info_type_name', 'DESC')->get()->toJson();
    }

    public function addProduct(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'category_id' => 'integer|required',
            'name' => 'required|max:80',
            'price' => 'required|numeric',
            'condition_id' => 'required|numeric',
            'unit_id' => 'required|numeric',
            'unit_name' => 'required'
        ]);

        $response = new ResponseDescription();

        if (!$validator->fails()) {

            if ($request->has('product_id')) {

                $product = Product::find($request->get('product_id'));
                if ($product) {
                    $product['category_id'] = $request->get('category_id');
                    $product['name'] = $request->get('name');
                    $product['price'] = $request->get('price');
                    $product['condition_id'] = $request->get('condition_id');
                    $product['unit_id'] = $request->get('unit_id');
                    $product['unit_name'] = $request->get('unit_name');
                    $product['description'] = \App\Library\Helpers::html_escape($request->get('description'));
                    $product['barcode'] = $request->get('barcode');
                    $product['minamount'] = $request->get('minamount');
                    $product->update();
                    $response->setData($product);
                }
            } else {
                $obj = new Product($request->all());
                $obj['state_id'] = 1;
                $obj->save();

                if ($obj['productFiles']) {
                    $productFile = new UploadController();
                    $productFile->upload($obj['product_id'], $request);
                }
                $response->setData($obj);
            }
            $response->setSuccess();
        } else {
            $response->setMessage('შეავსეთ ყველა აუცილებელი ველი.');
        }

        return $response->toJson();
    }


    public function addProductInfoTypes(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_info_type_name' => 'required|max:80'
        ]);

        $response = new ResponseDescription();

        if (!$validator->fails()) {

            $name = $request->get('product_info_type_name');

            $productInfoTypes = ProductInfoTypes::where('product_info_type_name', $name)->get();
            if ($productInfoTypes && count($productInfoTypes) > 0) {

                $response->setMessage("ასეთი ჩანაწერი უკვე არსებობს");
                return $response->toJson();
            }

            $obj = new ProductInfoTypes($request->all());
            $obj->save();
            $response->setSuccess();
            $response->setData($obj);
        }

        return $response->toJson();
    }

    public function addProductInfo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required|numeric',
            'product_info_type_id' => 'required|numeric',
            'product_info_type_name' => 'required',
            'value' => 'required'
        ]);

        $response = new ResponseDescription();

        if (!$validator->fails()) {
            $maxOrder = ProductInfo::where('product_id', $request['product_id'])->orderBy('order', 'DESC')->first();
            $obj = new ProductInfo($request->all());
            $obj['order'] = $maxOrder ? $maxOrder->order + 1 : 1;
            $obj['state_id'] = 1;
            $obj->save();
            $response->setSuccess();
            $response->setData($obj);
        }
        return $response->toJson();
    }

    public function removeProductInfo($productInfoId)
    {

        $response = new ResponseDescription();
        $productInfo = ProductInfo::find($productInfoId);
        if ($productInfo) {
            $productInfo['state_id'] = 2;
            $productInfo->update();
            $response->setSuccess();
        }

        return $response->toJson();
    }

    public function removeProductInfoType($productInfoTypeId)
    {
        $response = new ResponseDescription();
        $count = ProductInfoTypes::destroy($productInfoTypeId);

        if ($count > 0) {
            $response->setSuccess();
        }

        return $response->toJson();
    }

    public function getproductFullInfo($productId)
    {
        $product = Product::where('product_id', $productId)->with('productInfo')->first();
        $product['description'] = \App\Library\Helpers::htmlSpecDecode($product['description']);

        return $product->toJson();
    }

    public function removeProduct($productid)
    {
        $response = new ResponseDescription();
        $product = Product::find($productid);
        if ($product) {
            $product['state_id'] = 2;
            $product->update();
            $obj = new UploadController();
            $obj->removeDirectory($productid);
            $response->setSuccess();
        }
        return $response->toJson();
    }

    public function addProductFile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required|numeric',
        ]);

        $response = new ResponseDescription();

        if (!$validator->fails()) {
            $obj = new UploadController();
            if ($obj->upload($request['product_id'], $request)) {
                $response->setSuccess();
                $response->setData($obj);
            }
        }
        return $response->toJson();
    }

    public function removeProductAllFile($productid)
    {
        $response = new ResponseDescription();
        $obj = new UploadController();
        if ($obj->removeDirectory($productid)) {
            $response->setSuccess();
        }

        return $response->toJson();
    }

    public function removeProductFile($productFileId)
    {
        $response = new ResponseDescription();
        $obj = new UploadController();

        if ($obj->removeFile($productFileId)) {
            $response->setSuccess();
        }
        return $response->toJson();
    }

    public function getProductFiles($productId)
    {
        return ProductFile::where('state_id', 1)->where('product_id', $productId)->get()->toJson();
    }

    public function ctateNewStorage($recievingProduct)
    {
        $storage = new Storage();
        $storage['product_id'] = $recievingProduct['product_id'];
        $storage['amount'] = $recievingProduct['amount'];

        $validDate = $recievingProduct['valid_date'];

        if ($validDate) {
            $storage['valid_date'] = Helpers::convertFromTimestampMilliseconds($validDate);
        }

        return $storage;
    }


    public function updateStorages($recievingProducts, $branchId)
    {

        $wholePrice = 0;

        foreach ($recievingProducts as $recievingProduct) {

            $validDate = $recievingProduct['valid_date'];

            $amount = $recievingProduct['amount'];
            $wholePrice += $amount * $recievingProduct['one_price'];

            $query = Storage::where('branch_id', $branchId)->where('product_id', $recievingProduct['product_id']);

            if ($validDate) {
                $query->where('valid_date', Helpers::convertFromTimestampMilliseconds($validDate));
            } else {
                $query->whereNull('valid_date');
            }

            $storage = $query->first();
            if ($storage) {
                $storage['amount'] = $storage['amount'] + $amount;
                $storage->update();
            } else {
                $storage = $this->ctateNewStorage($recievingProduct);
                $storage['branch_id'] = $branchId;
                $storage->save();
            }
        }

        return $wholePrice;
    }

    public function saveRecieving($request, $wholePrice)
    {

        $recievingObject = new Recieving();
        $recievingObject['state_id'] = 1;
        $recievingObject['recieving_whole_price'] = $wholePrice;
        $recievingObject['payed_money'] = $request['payed_money'];
        $recievingObject['recieving_invoic_number'] = $request['recieving_invoice_number'];
        $recievingObject['branch_id'] = $request['branch_id'];
        $recievingId = $recievingObject->save();

        return $recievingId;
    }

    public function saveRecievingproducts($recievingProducts, $recievingId, $branchId)
    {
        foreach ($recievingProducts as $recievingProduct) {

            $recievingProductObject = new RecievingProduct($recievingProduct);
            $recievingProductObject['recieving_id'] = $recievingId;
            $recievingProductObject['state_id'] = 1;
            $recievingProductObject['branch_id'] = $branchId;

            if ($recievingProduct['valid_date']) {
                $recievingProductObject['valid_date'] = date('Y-m-d H:i:s', $recievingProduct['valid_date'] / 1000);
            }

            $recievingProductObject['whole_price'] = $recievingProduct['amount'] * $recievingProduct['one_price'];
            $recievingProductObject->save();
        }
    }

    public function receivProduct(Request $request)
    {

        $validator = Validator::make($request->all(), [
            //'product_id' => 'required|numeric'
            'branch_id' => 'required|numeric'
            //'amount' => 'required',
        ]);

        $response = new ResponseDescription();

        $recievingProducts = $request['recieving_products'];
        $branchId = $request['branch_id'];

        $wholePrice = $this->updateStorages($recievingProducts, $branchId);
        $recievingId = $this->saveRecieving($request, $wholePrice);
        $this->saveRecievingproducts($recievingProducts, $recievingId, $branchId);

        $response->setSuccess();

        return $response->toJson();
    }

    public function changeProductInfoOrder($product_info_id, $direction)
    {
        $productInfo = ProductInfo::find($product_info_id);
        $friends = ProductInfo::where('product_id', $productInfo->product_id)->where('state_id', 1)->orderBy('order', 'ASC')->get();

        if ($friends && count($friends) > 0) {
            $ind = -1;
            for ($i = 0; $i < count($friends); $i++) {
                if ($direction == "up") {
                    if ($friends[$i]->order < $productInfo->order) {
                        $ind = $i;
                    }
                } else {
                    if ($friends[$i]->order > $productInfo->order) {
                        $ind = $i;
                        break;
                    }
                }
            }
            if ($ind > -1) {
                $o = $productInfo->order;
                $productInfo->order = $friends[$ind]->order;
                $friends[$ind]->order = $o;
                $productInfo->update();
                $friends[$ind]->update();
            }
            $this->response->success = true;
            $this->response->message = "successful";
            $this->response->data = [$productInfo];
        }

        return $this->response->toJson();
    }

    private static function modifyProductsData($data)
    {
        foreach ($data as $key => $value) {
            $value['description'] = \App\Library\Helpers::htmlSpecDecode($value['description']);
        }

        return $data;
    }
}
