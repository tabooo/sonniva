<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Library\Helpers;
use App\Library\PostTypes;
use App\Model\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LinksController extends Controller
{

    private $response = NULL;

    public function __construct()
    {
        $this->response = new \App\Library\ResponseDescription();
    }

    public function getLinks()
    {
        $links = Post::where('type', PostTypes::links)->where('state', 1)->get();

        $resp = json_encode($links);

        return $resp;
    }

    public function addLink(Request $request)
    {
        $validator = Validator::make($data = $request->all(), Post::$rules);
        if (!$validator->fails()) {
            $data['title_ge'] = Helpers::strip_html_php($data['title_ge']);
            $data['title_en'] = Helpers::strip_html_php($data['title_en']);
            $data['title_ru'] = Helpers::strip_html_php($data['title_ru']);
            $data['link'] = Helpers::strip_html_php($data['link']);
            $data['type'] = PostTypes::links;
            if (array_key_exists('post_id', $data) && $data['post_id'] != null) {
                $this->updateLink($data);
            } else {
                $link = Post::create($data);

                if ($link->post_id) {
                    $this->response->success = true;
                    $this->response->message = "successful";
                    $this->response->data = $link;
                }
            }
        }
        return $this->response->toJson();
    }

    public function updateLink($data)
    {
        $link = Post::find($data['post_id']);

        if ($link && $link->update($data)) {
            $this->response->success = true;
            $this->response->message = "successful";
            $this->response->data = [$link];
        }
        return $this->response->toJson();
    }

    public function removeLink($post_id)
    {
        $post = Post::find($post_id);

        if ($post) {
            $post->state = 0;
            $post->update();
            $this->response->success = true;
            $this->response->message = "successful";
            $this->response->data = [$post];
        }

        return $this->response->toJson();
    }
}