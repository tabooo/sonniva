<?php

/**
 * Created by PhpStorm.
 * User: ika
 * Date: 10/24/15
 * Time: 6:30 PM
 */
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class PageController extends Controller
{

    private $response = NULL;

    public function __construct()
    {
        $this->response = new \App\Library\ResponseDescription();
    }


    public function getPages(Request $request)
    {
        $data = $request->all();

        $query = \App\Model\Post::with('menu')->where('type', 2)->where('disabled', 0)->where('state', 1);

        if ($data['searchName']) {
            $query->where('title_ge', 'LIKE', '%' . $data['searchName'] . '%');
        }
        if ($data['post_id']) {
            $query->where('post_id', $data['post_id']);
        }
        $total = $query->count();
        $pages = $query->skip($data['start'])->take($data['limit'])->get(); //->paginate($data['limit']); ->orderBy('updated_at', 'DESC')
        $pages = self::modifypagesData($pages);

        $resp = json_encode(((object)['data' => $pages, 'total' => $total]));

        return $resp;
    }

    public function add(Request $request)
    {
        $validator = Validator::make($data = $request->all(), \App\Model\Post::$rules);

        $post_id = null;
        if (!$validator->fails()) {
            $data['img_path'] = \App\Library\Helpers::strip_html_php($data['img_path']);

            $data['keyword_ge'] = \App\Library\Helpers::strip_html_php($data['keyword_ge']);
            $data['keyword_en'] = \App\Library\Helpers::strip_html_php($data['keyword_en']);
            $data['keyword_ru'] = \App\Library\Helpers::strip_html_php($data['keyword_ru']);

            $data['google_description_ge'] = \App\Library\Helpers::strip_html_php($data['google_description_ge']);
            $data['google_description_en'] = \App\Library\Helpers::strip_html_php($data['google_description_en']);
            $data['google_description_ru'] = \App\Library\Helpers::strip_html_php($data['google_description_ru']);

            $data['description_ge'] = \App\Library\Helpers::html_escape($data['description_ge']);
            $data['description_en'] = \App\Library\Helpers::html_escape($data['description_en']);
            $data['description_ru'] = \App\Library\Helpers::html_escape($data['description_ru']);

            $data['type'] = 2;

            if ($request->has('post_id')) {
                $data['link'] = '/page/' . $data['post_id'];
                $this->update($data);
            } else {
                $page = \App\Model\Post::create($data);
                $data['link'] = '/page/' . $page->post_id;
                $data['post_id'] = $page->post_id;
                $page = \App\Model\Post::find($page->post_id);
                $page->update($data);

                try {
                    $this->addArticleImages($data, $page->post_id);
                } catch (Exception $e) {
                    $this->response->setMessage('image error');

                    return $this->response->toJson();
                }

                if ($page->post_id) {
                    $this->response->success = true;
                    $this->response->message = "successful";
                    $this->response->data = [$page];
                }
            }
        }

        return $this->response->toJson();
    }

    public function update($data)
    {
        $page = \App\Model\Post::find($data['post_id']);

        if ($page && $page->update($data)) {

            try {
                $this->addArticleImages($data);
            } catch (Exception $e) {
                $this->response->setMessage('image error');

                return $this->response->toJson();
            }

            $this->response->success = true;
            $this->response->message = "successful";
            $this->response->data = [$page];
        }

    }

    public function destroy($post_id)
    {
        $page = \App\Model\Post::find($post_id);

        if ($page) {
            $page->state = 0;

            if ($page->save()) {
                $this->response->success = true;
                $this->response->message = "successful";
                $this->response->data = [$page];
            }
        }

        return $this->response->toJson();
    }

    private static function modifypagesData($data)
    {
        foreach ($data as $key => $value) {
            $value['description_ge'] = \App\Library\Helpers::htmlSpecDecode($value['description_ge']);
            $value['description_en'] = \App\Library\Helpers::htmlSpecDecode($value['description_en']);
            $value['description_ru'] = \App\Library\Helpers::htmlSpecDecode($value['description_ru']);
            $value['description_ge_short'] = \App\Library\Helpers::htmlSpecDecode($value['description_ge_short']);
            $value['description_en_short'] = \App\Library\Helpers::htmlSpecDecode($value['description_en_short']);
            $value['description_ru_short'] = \App\Library\Helpers::htmlSpecDecode($value['description_ru_short']);
        }

        return $data;
    }

    private function addArticleImages($data, $post_id = NULL, $article_category_id = 2)
    {
//        $insertList = [];
//
//        if ($post_id != NULL) {
//            $data['post_id'] = $post_id;
//        }
//
//        if (array_key_exists('gallery', $data) && is_array($data['gallery'])) {
//            $gallery = $data['gallery'];
//
//            foreach ($gallery as $value) {
//                array_push($insertList, [
//                    'article_id' => $data['post_id'],
//                    'article_category_id' => $article_category_id,
//                    'file_manager_id' => $value['file_manager_id']
//                ]);
//            }
//
//            \DB::table('web_article_images')->insert($insertList);
//            $dirName = $data['post_id'] . '_' . $article_category_id;
//
//            try {
//                FolderController::resizeArticleImages($dirName, $gallery);
//            } catch (Exception $e) {
//                throw $e;
//            }
//        }
    }
}