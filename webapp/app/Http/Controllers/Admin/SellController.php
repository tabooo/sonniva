<?php

/**
 * Created by PhpStorm.
 * User: ika
 * Date: 10/24/15
 * Time: 6:30 PM
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Banks\CrystalController;
use App\Model\InstallmentsTBC;
use App\Model\Sell;
use Illuminate\Http\Request;

class SellController extends Controller
{

    private $response = NULL;

    public function __construct()
    {
        $this->response = new \App\Library\ResponseDescription();
    }


    public function getSells(Request $request)
    {
        $data = $request->all();

        $query = Sell::where('state', 1);

        if ($data['first_name']) {
            $query->where('first_name', 'LIKE', '%' . $data['first_name'] . '%');
        }
        if ($data['payment_type']) {
            $query->where('payment_type', $data['payment_type']);
        }
        $total = $query->count();
        $sells = $query->skip($data['start'])->take($data['limit'])->get(); //->paginate($data['limit']); ->orderBy('updated_at', 'DESC')

        $resp = json_encode(((object)['data' => $sells, 'total' => $total]));

        return $resp;
    }

    public function removeSell($sell_id)
    {
        $sell = Sell::find($sell_id);

        if ($sell) {
            $sell->state = 0;

            if ($sell->save()) {
                $this->response->success = true;
                $this->response->message = "successful";
                $this->response->data = [$sell];
            }
        }

        return $this->response->toJson();
    }

    public function getTbcInstallments(Request $request)
    {
        $data = $request->all();

        $query = InstallmentsTBC::where('state', 1);

        if ($data['status'] && $data['status'] != "") {
            $query->where('status', $data['status']);
        }
        if ($data['session_id'] && $data['session_id'] != "") {
            $query->where('session_id', $data['session_id']);
        }
        $total = $query->count();
        $sells = $query->skip($data['start'])->take($data['limit'])->with('sell')->orderBy('created_at', 'DESC')->get(); //->paginate($data['limit']); ->orderBy('updated_at', 'DESC')

        $resp = json_encode(((object)['data' => $sells, 'total' => $total]));

        return $resp;
    }


    public function confirmTbcInstallment($installment_id)
    {
        $installmentTbc = InstallmentsTBC::find($installment_id);

        if ($installmentTbc) {
            $confirm = CrystalController::confirmSession($installmentTbc->session_id);
            if ($confirm['status'] == 200) {
                $installmentTbc->status = 2;
                if ($installmentTbc->save()) {
                    $this->response->success = true;
                    $this->response->message = "successful";
                    $this->response->data = [];
                }
            } else {
                $this->response->data = $confirm;
            }
        }

        return $this->response->toJson();
    }


    public function cancelTbcInstallment($installment_id)
    {
        $installmentTbc = InstallmentsTBC::find($installment_id);

        if ($installmentTbc) {
            $confirm = CrystalController::cancelSession($installmentTbc->session_id);
            if ($confirm['status'] == 200) {
                $installmentTbc->status = 0;
                if ($installmentTbc->save()) {
                    $this->response->success = true;
                    $this->response->message = "successful";
                    $this->response->data = [];
                }
            } else {
                $this->response->data = $confirm;
            }
        }

        return $this->response->toJson();
    }


    public function statusTbcInstallment($installment_id)
    {
        $installmentTbc = InstallmentsTBC::find($installment_id);

        if ($installmentTbc) {
            $confirm = CrystalController::statusSession($installmentTbc->session_id);
            if ($confirm['status'] == 200) {
                $this->response->success = true;
                $this->response->message = "successful";
                $this->response->data = $confirm;
            } else {
                $this->response->data = $confirm;
            }
        }

        return $this->response->toJson();
    }
}
