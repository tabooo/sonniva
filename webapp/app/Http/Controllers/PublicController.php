<?php

namespace App\Http\Controllers;

use App\Library\PostTypes;
use App\Model\Category;
use App\Model\Params;
use App\Model\Post;
use App\Model\Product;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class PublicController extends Controller
{
    function getIndex()
    {
        $saleProducts = Product::where('state_id', 1)->with('productFile')->orderBy('name', 'DESC')->take(4)->get();
        $popularSaleProducts = Product::where('state_id', 1)->with('productFile')->orderBy('created_at', 'DESC')->take(4)->get();
        $newProducts = Product::where('state_id', 1)->with('productFile')->orderBy('created_at', 'DESC')->take(8)->get();
        $bestSellroducts = Product::where('state_id', 1)->with('productFile')->orderBy('name', 'DESC')->take(4)->get();
        $slideshows = Post::where('type', PostTypes::slideshow)->where('state', 1)->get();
        $links = Post::where('type', PostTypes::links)->where('state', 1)->get();
        return view('pages.index', ['saleProducts' => $saleProducts, 'popularSaleProducts' => $popularSaleProducts,
            'newProducts' => $newProducts, 'bestSellroducts' => $bestSellroducts, 'slideshows' => $slideshows, 'links' => $links]);
    }

    public static function getParams()
    {
        $params = Params::get();
        $ob = [];
        foreach ($params as $param) {
            $ob[$param['name']] = $param['value'];
        }

        return $ob;
    }

    public static function getCategoriesTree()
    {
        $parentCategories = Category::whereNull('parent_id')->where('state_id', 1)
            ->orderBy('order', 'ASC')->get();
        PublicController::getChildrens($parentCategories);
        return $parentCategories;
    }

    public static function getChildrens($parents)
    {
        foreach ($parents as $parent) {
            $childrens = Category::where('parent_id', $parent['category_id'])->where('state_id', 1)
                ->orderBy('order', 'ASC')->get();
            $parent['childrenObjects'] = $childrens;
            PublicController::getChildrens($childrens);
        }
    }

    function getContactView()
    {
        return view('pages.contact');
    }

    function getPriceCalculatorView()
    {
        return view('pages.pricecalculator');
    }

    function api(Request $request)
    {
        $requestData = $request->all();
        if (array_key_exists('status_id', $requestData)
            && array_key_exists('order_id', $requestData)
            && array_key_exists('hash', $requestData)) {

        }
    }
}
