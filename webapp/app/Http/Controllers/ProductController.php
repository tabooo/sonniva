<?php

namespace App\Http\Controllers;

use App\Library\Helpers;
use App\Model\Category;
use App\Model\Product;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    function getProduct($product_id)
    {
        $product = Product::where('state_id', 1)->where('product_id', $product_id)
            ->with('productFile')->with('productInfo')
            ->first();
        $product = self::modifyProductData($product);
        $likeProducts = Product::where('state_id', 1)->where('category_id', $product['category_id'])->take(8)->get();
        return view('pages.product', ['product' => $product, 'likeProducts' => $likeProducts]);
    }

    function getProducts(Request $request)
    {
        $rules = [
            'product_id' => 'integer',
            'category_id' => 'integer'
        ];

        $validator = Validator::make($args = $request->all(), $rules);

        if (!$validator->fails()) {
            $parentCategories = [];
            $query = Product::select()->where('state_id', 1);

            if (array_key_exists('name', $args) && !empty($args['name'])) {
                $args['name'] = Helpers::strip_html_php($args['name']);
                $query->where('name', 'LIKE', '%' . $args['name'] . '%');
            }

            if (array_key_exists('categoryId', $args) && !empty($args['categoryId'])) {

                $categoryId = $args['categoryId'];
                $category = Category::find($categoryId);
                $parentCategories = $this->getParentsTree($category);
                $ids = $category['childrens'];

                if (!$ids) {
                    $ids = $categoryId;
                } else {
                    $ids .= "," . $categoryId;
                }

                $categoryIds = explode(',', $ids);

                $query->whereIn('category_id', $categoryIds);
            }

            if (array_key_exists('sort', $args) && !empty($args['sort'])) {
                $args['sort'] = Helpers::strip_html_php($args['sort']);
                if ($args['sort'] == 'new') {
                    $query->orderBy('created_at', 'DESC');
                } else if ($args['sort'] == 'lowToHigh') {
                    $query->orderBy('price', 'ASC');
                } else if ($args['sort'] == 'highToLow') {
                    $query->orderBy('price', 'DESC');
                }
            }

            $limit = 9;

            if (array_key_exists('limit', $args) && !empty($args['limit'])) {
                $args['limit'] = Helpers::strip_html_php($args['limit']);
                $limit = $args['limit'];
            }

            $products = $query->with('productFile')->paginate($limit);

            return view('pages.products', ['products' => $products->appends(Input::except('page')),
                'parentCategories' => $parentCategories]);
        }
        return view('pages.products', ['products' => [], 'parentCategories' => []]);
    }

    function getParentsTree($category)
    {
        $tmpParents = [];
        if ($category['parent_id'] == null) {
            return null;
        }
        $parent = Category::find($category['parent_id']);
        array_push($tmpParents, $parent);
        $tmp = $this->getParentsTree($parent);
        if ($tmp != null) {
            $tmpParents = array_merge($tmpParents, $tmp);
        }

        return $tmpParents;

    }

    private static function modifyProductData($data)
    {
        $data['description'] = \App\Library\Helpers::htmlSpecDecode($data['description']);

        return $data;
    }

}
