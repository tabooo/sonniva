<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use \App\Model\Post;
use \App\Model\WebCategory;

class PublicNewsController extends Controller
{
    public static function getAllPosts(Request $request)
    {
        $rules = [
            'category' => 'integer|min:1'
        ];

        $validator = Validator::make($args = $request->all(), $rules);

        if (!$validator->fails()) {
            $query = Post::select()
                ->with('meta')
                ->where('type', 1)
                ->where('disabled', 0)
                ->where('state', 1);

            $query->with(['categories' => function ($q) {
                $q->where(WebCategory::$tbl . '.disabled', 0)
                    ->where(WebCategory::$tbl . '.state', 1);
            }]);

            if (array_key_exists('category', $args) && !empty($args['category'])) {
                $query->whereHas('categories', function ($q) use ($args) {
                    $q->where(WebCategory::$tbl . '.category_id', $args['category'])
                        ->where(WebCategory::$tbl . '.disabled', 0)
                        ->where(WebCategory::$tbl . '.state', 1);
                });
            }

            $posts = $query->orderBy('published_at', 'DESC')
                ->paginate(4);

            $categories = self::getCategories();
            $tags = [];//self::getTags();
            $popularNews = self::getMostViewed(3, (array_key_exists('category', $args) && !empty($args['category'])) ? $args['category'] : null);

            return view('pages.news', [
                'news' => self::modifyNewsData($posts),
                'web_categories' => $categories,
                'tags' => $tags,
                'popularNews' => self::modifyNewsData($popularNews)
            ]);
        }

        return view('pages.news', ['news' => [], 'categories' => []]);
    }

    public static function getNewsItem($id)
    {
        $news = \App\Model\Post::with('categories')
            ->where('disabled', 0)->where('state', 1)->where('post_id', $id)
            ->first();

        if ($news) {

            $args = [];
            foreach ($news->categories as $k => $v) {
                array_push($args, $v->category_id);
            }

            $cats = \DB::table('web_post_categories')
                ->whereIn('category_id', $args)
                ->select('post_id')
                ->distinct()->get();

            $newsArgs = [];
            foreach ($cats as $k => $v) {
                //if ($news->post_id != $v->post_id)
                array_push($newsArgs, $v->post_id);
            }

            $sameCategories = \App\Model\Post::whereIn('post_id', $newsArgs)
                ->where('disabled', 0)
                ->where('state', 1)
                ->where('type', 1)
                ->orderBy('published_at', 'DESC')->get();

            $categories = self::getCategories();
            $news = self::modifyNewsData([$news]);
            $sameCategories = self::modifyNewsData($sameCategories);

            return view('pages.newsItem', ['news' => $news[0], 'web_categories' => $categories, 'sameCategories' => $sameCategories]);
        }

        return response(404);
    }

    public static function modifyNewsData($news)
    {
        foreach ($news as $key => $value) {
            $value['description_ge'] = \App\Library\Helpers::htmlSpecDecode($value['description_ge']);
            $value['description_en'] = \App\Library\Helpers::htmlSpecDecode($value['description_en']);
            $value['description_ru'] = \App\Library\Helpers::htmlSpecDecode($value['description_ru']);
        }

        return $news;
    }

    public static function getCategories()
    {
        $category = WebCategory::tree();

        return $category;
    }

    public static function getLastPosts($category = null, $count = 4)
    {
        $query = Post::select()
            ->where('type', 1)
            ->where('disabled', 0)
            ->where('state', 1)
            ->with('meta');

        $query->with(['categories' => function ($q) {
            $q->where(WebCategory::$tbl . '.disabled', 0)
                ->where(WebCategory::$tbl . '.state', 1);
        }]);

        if (!empty((int)$category)) {
            $query->whereHas('categories', function ($q) use ($category) {
                $q->where(WebCategory::$tbl . '.category_id', $category)
                    ->where(WebCategory::$tbl . '.disabled', 0)
                    ->where(WebCategory::$tbl . '.state', 1);
            });
        }

        $posts = $query->orderBy('published_at', 'DESC')->take($count)->get();

        return self::modifyNewsData($posts);
    }

    public static function getMostViewed($count = 5, $category = null)
    {
        $query = Post::select()
            ->where('type', 1)
            ->where('disabled', 0)
            ->where('state', 1)
            ->with('meta');

        if ($category != null && !empty((int)$category)) {
            $query->whereHas('categories', function ($q) use ($category) {
                $q->where(WebCategory::$tbl . '.category_id', $category)
                    ->where(WebCategory::$tbl . '.disabled', 0)
                    ->where(WebCategory::$tbl . '.state', 1);
            });
        }

        $news = $query->where('disabled', 0)->where('state', 1)->
        orderBy('views', 'DESC')->take($count)->get();

        return $news;
    }
}