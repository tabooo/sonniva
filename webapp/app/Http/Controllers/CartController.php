<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Banks\CredoController;
use App\Http\Controllers\Banks\CrystalController;
use App\Http\Controllers\Banks\TBCController;
use App\Library\Helpers;
use App\Library\ResponseDescription;
use App\Model\City;
use App\Model\InstallmentsTBC;
use App\Model\Product;
use App\Model\Sell;
use App\Model\SellItem;
use App\Model\ShoppingCart;
use App\Model\UserAddresses;
use Carbon\Carbon;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    private $response = NULL;

    public function __construct()
    {
        $this->response = new \App\Library\ResponseDescription();
    }

    function add(Request $request)
    {
        $response = new ResponseDescription();

        $product = Product::find($request['id']);

        $item = Cart::add($request['id'], $product['name'], $request['qty'], $product['price'], $request['options']);
        if (Auth::check()) {
            ShoppingCart::where('identifier', Auth::user()->email)->delete();
            Cart::store(Auth::user()->email);
        }

        $response->setSuccess();
        $response->setData($item);
        return $response->toJson();
    }

    function updateItem(Request $request)
    {
        $response = new ResponseDescription();

        $item = Cart::update($request['rowId'], $request['qty']);

        if (Auth::check()) {
            ShoppingCart::where('identifier', Auth::user()->email)->delete();
            Cart::store(Auth::user()->email);
        }

        $response->setSuccess();
        $response->setData($item);
        return $response->toJson();
    }

    function remove($rowId)
    {
        $response = new ResponseDescription();

        $item = Cart::remove($rowId);

        if (Auth::check()) {
            ShoppingCart::where('identifier', Auth::user()->email)->delete();
            Cart::store(Auth::user()->email);
        }

        $response->setSuccess();
        $response->setData($item);
        return $response->toJson();
    }

    function getCartContent()
    {
        $resp = ['cart' => Cart::content(), 'count' => Cart::Count(), 'subtotal' => Cart::subtotal()];
        return $resp;
    }

    function getCartView()
    {
        $likeProducts = Product::where('state_id', 1)->take(4)->get();
        return view('pages.cart', ['likeProducts' => $likeProducts]);
    }

    function getCheckoutPage()
    {
        if (!Auth::check()) {
            header("Location: " . asset("/register"));
            die();
        }
        $user = Auth::guard()->user();
        $user->birth_date = Carbon::parse($user->from_date)->format('m/d/Y');

        $addresses = UserAddresses::where('user_id', $user['id'])->where('state', 1)->get();
        $primaryAddress = UserAddresses::where('user_id', $user['id'])->where('state', 1)->where('primary', 1)->first();

        $cities = City::where('state', 1)->get();

        return view('pages.checkout', ['user' => $user, 'cities' => $cities, 'addresses' => $addresses, 'primaryAddress' => $primaryAddress]);
    }

    function checkoutNoAuth()
    {
        return view('pages.checkoutNoAuth');
    }

    function getPayPage(Request $request)
    {
        if (!Auth::check()) {
            header("Location: " . asset("/register"));
            die();
        }

        $city = City::find($request['city_id']);

        session(["city_id" => $request['city_id']]);
        session(["country_id" => $request['country_id']]);
        session(["address" => $request['address']]);
        session(["shipping_price" => $city['price']]);

        return view('pages.pay', ['city' => $city]);
    }

    function finishOrder(Request $request)
    {
        if (Cart::count() < 1) {
            return $this->response->toJson();
        }
        $rules = [
            /*'name' => 'required',
            'email' => 'required|email|max:255',
            'message' => 'required',*/
            'fullName' => 'required',
            'phone' => 'required',
        ];

        $validator = Validator::make($args = $request->all(), $rules);

        try {
            /*$args['cardContent'] = Cart::content();
            $args['cardCount'] = Cart::count();
            $args['cardSubtotal'] = Cart::subtotal();
            Mail::send('mailer.mailer', ['mailer' => $args], function ($message) use ($args, $tbcInstallmentResponse) {
                $message->to('tabagari89@gmail.com')->subject('შეკვეთა: ' . $tbcInstallmentResponse['sessionId'])
                    ->from('info@iomax.ge', 'შეკვეთა საიტიდან');
            });

            if (count(Mail::failures())) {
                return 'false';
            } else {

            }*/

            $products = array();
            $productsForCrystal = array();
            $productsForCredo = array();
            $credoHashString = '';

            foreach (Cart::content() as $key => $value) {
                array_push($products, array(
                    'name' => $value->name,
                    'price' => $value->subtotal,
                    'quantity' => $value->qty
                ));

                array_push($productsForCrystal, array(
                    'title' => $value->name,
                    'price' => $value->price * 1.06,
                    'price_shop' => $value->price * 1.06,
                    'quantity' => $value->qty,
                    "vendor_product_id" => $value->id,
                ));

                array_push($productsForCredo, array(
                    'title' => $value->name,
                    'price' => $value->price * 100 * 1.06,
                    'amount' => $value->qty,
                    "id" => $value->id,
                    "type" => 0
                ));
                $credoHashString .= $value->id . $value->name . $value->qty . ($value->price * 100) . '0';
            }

            $data = [
                "merchantKey" => TBCController::$merchantKey,
                "priceTotal" => Cart::subtotal(),
                "campaignId" => TBCController::$campaignId,
                "invoiceId" => strval(time()),
                "products" => $products
            ];

            $dataForCrystal = [
                'products' => $productsForCrystal,
                'order_id' => $data['invoiceId'],
                'vendor_id' => CrystalController::$vendorId,
            ];

            $dataForCredo = [
                'products' => $productsForCredo,
                'orderCode' => $data['invoiceId'],
                'merchantId' => CredoController::$merchandId,
                'check' => md5($credoHashString),
            ];

            $tbcInstallmentResponse = null;
            if ($args['payment_type'] == 2) {
                $tbcInstallmentResponse = TBCController::createTbcInstallmentApplication($data);
                if (!array_key_exists('location', $tbcInstallmentResponse)) {
                    $this->response->data = $tbcInstallmentResponse;
                    return $this->response->toJson();
                }
            }

            $sell = [
                'transaction_id' => $tbcInstallmentResponse ? $tbcInstallmentResponse['sessionId'] : null,
                'whole_price' => Cart::subtotal(),
                'state' => 1,
                'first_name' => Helpers::strip_html_php($args['first_name']),
                'last_name' => Helpers::strip_html_php($args['last_name']),
                'email' => Helpers::strip_html_php($args['email']),
                'phone' => Helpers::strip_html_php($args['phone']),
                'company' => Helpers::strip_html_php($args['company']),
                'address' => Helpers::strip_html_php($args['address']),
                'country' => Helpers::strip_html_php($args['country']),
                'city' => Helpers::strip_html_php($args['city']),
                'note' => Helpers::strip_html_php($args['note']),
                'invoice_id' => $data['invoiceId'],
                'payment_type' => Helpers::strip_html_php($args['payment_type']),];
            $sell = Sell::create($sell);

            foreach (Cart::content() as $key => $value) {
                $sellItem = [
                    'sell_id' => $sell->sell_id,
                    'product_id' => $value->id,
                    'product_price' => $value->price,
                    'amount' => $value->qty,
                    'whole_price' => $value->subtotal,
                    'state' => 1
                ];
                SellItem::create($sellItem);
            }

            Cart::destroy();

            if ($args['payment_type'] == 2) {
                $installmentsTbc = [
                    'session_id' => $tbcInstallmentResponse['sessionId'],
                    'sell_id' => $sell->sell_id,
                    'state' => 1,
                    'status' => 1
                ];
                $installmentsTbc = InstallmentsTBC::create($installmentsTbc);
                $this->response->success = true;
                $this->response->message = "successful";
                $this->response->data = [$tbcInstallmentResponse['location']];
                return $this->response->toJson();
            } else if ($args['payment_type'] == 3) {
                $crystalInstallmentResponse = CrystalController::createOnlineInstallment($dataForCrystal);
                $this->response->success = true;
                $this->response->message = "successful";
                $this->response->data = [$crystalInstallmentResponse];
                return $this->response->toJson();
            } else if ($args['payment_type'] == 4) {
                $credoInstallmentResponse = CredoController::createOnlineInstallment($dataForCredo);
                $this->response->success = true;
                $this->response->message = "successful";
                $this->response->data = [$credoInstallmentResponse, json_encode($dataForCredo)];
                return $this->response->toJson();
            }


            $this->response->success = true;
            $this->response->message = "successful";
            $this->response->data = [];
            return $this->response->toJson();
        } catch (Exception $e) {
            return $this->response->toJson();
        }
    }


    function afterCheckoutPage(Request $sellId)
    {
        $sell = Sell::find($sellId);
        return view('pages.afterCheckout', ['sell' => $sell]);
    }
}
