<?php

/**
 * Created by PhpStorm.
 * User: ika
 * Date: 10/24/15
 * Time: 6:30 PM
 */
namespace App\Http\Controllers;

use App\Model\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class MenusController extends Controller
{

    private $response = NULL;

    public function __construct()
    {
        $this->response = new \App\Library\ResponseDescription();
    }

    public function getMenuTree()
    {
        $menu = Menu::tree();
        //$this->getMenuItems($menu);
        return $menu;
    }

    public function getMenuItems($data)
    {
        for ($i = 0; $i < count($data); $i++) {
            if ($data[$i]->editable == 1) {
                $data[$i]->link = "page/detail/" . $data[$i]->link;
            }
            if (count($data[$i]->children) > 0) {
                $this->getMenuItems($data[$i]->children);
            }
        }
    }

    public static function getSubMenu($search)
    {
        $menu = Menu::where('disabled', 0)->where('state', 1)->where('link', $search)->first();
        $subMenu = [];
        if ($menu) {
            $subMenu = Menu::where('disabled', 0)->where('state', 1)->where('parent_id', $menu->menu_id)->get();
        }
        return $subMenu;
    }

    public static function getUpMenuIds($menu)
    {
        $ret = "";
        if ($menu) {
            $ret .= $menu->menu_id;
            if ($menu->parent_id) {
                $upMenu = Menu::where('disabled', 0)->where('state', 1)->where('menu_id', $menu->parent_id)->first();
                $ret .= ", " . self::getUpMenuIds($upMenu);
            }
        }
        return $ret;
    }

    public static function getMenuByLink($search)
    {
        return Menu::where('disabled', 0)->where('state', 1)->where('link', $search)->first();
    }

    public static function getMenusByIds($search)
    {
        return Menu::where('disabled', 0)->where('state', 1)->whereIn('menu_id', $search)->get();
    }

    public static function getAllSubMenu($menus, $menu_id = null, $link = null)
    {
        function getSubMenu($menus, $menu_id = null, $link = null)
        {
            $rmenu = null;
            foreach ($menus as $menu) {
                if ($menu_id && $menu_id == $menu['menu_id']) {
                    $rmenu = $menu;
                    break;
                } else if ($link && $link == $menu['link']) {
                    $rmenu = $menu;
                    //print_r($menu['menu_id']);
                    break;
                }
                if (!$rmenu) {
                    $rmenu = getSubMenu($menu['children'], $menu_id, $link);
                }
            }
            return $rmenu;
        }

        $menu = getSubMenu($menus, $menu_id, $link);
        return $menu;
    }
}