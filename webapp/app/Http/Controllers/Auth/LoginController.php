<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Library\ResponseDescription;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $response = null;

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['logout', 'authenticate', 'doLogin']]);

        $this->response = new ResponseDescription();
    }

    public function doLogin()
    {
        if (Auth::check()) {
            return redirect()->route('dashboard');
        }

        return view('dashboard.login');
    }

    public function authenticated(Request $request, $user)
    {
        $this->response->data = $user;
        $this->response->setSuccess();
    }

    public function authenticate(Request $request)
    {
        $login = $this->login($request);

        if (!empty($login) && is_array($login->original)) {
            if (array_key_exists($this->username(), $login->original)) {
                $this->response->data = $login->original[$this->username()];
            }
        }

        return $this->response->toJson();
    }
}
