<?php

namespace App\Http\Controllers\Auth;

use App\Model\UserAddresses;
use App\Model\UserTree;
use App\User;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Library\ResponseDescription;

class UserLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        //return view(''); // TODO
    }

    public function login(Request $request)
    {
        $response = new ResponseDescription();

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:3'
        ]);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $response->setSuccess();
            $response->setData(Auth::user());
        }

        return $response->toJson();
    }

    public function logout()
    {
        Auth::logout();

        $response = new ResponseDescription();
        $response->setSuccess();
        return $response->toJson();
    }

    public function showProfilePage()
    {
        if (!Auth::check()) {
            header("Location: " . asset("/register"));
            die();
        }
        $user = Auth::guard()->user();
        $user->birth_date = Carbon::parse($user->from_date)->format('m/d/Y');

        $userTrees=UserTree::where('user_id',$user['id'])->get();

        return view('user/profile')->with(['user' => $user, 'userTrees' => $userTrees]);
    }

    public function showPasswordPage()
    {
        if (!Auth::check()) {
            header("Location: " . asset("/register"));
            die();
        }
        $user = Auth::guard()->user();
        $user->birth_date = Carbon::parse($user->from_date)->format('m/d/Y');

        return view('user/password')->with('user', $user);
    }

    public function showAddressPage()
    {
        if (!Auth::check()) {
            header("Location: " . asset("/register"));
            die();
        }
        $user = Auth::guard()->user();
        $user->birth_date = Carbon::parse($user->from_date)->format('m/d/Y');

        $addresses = UserAddresses::where('state', 1)->where('user_id', Auth::guard()->user()->id)->with('city')->get();

        return view('user/address')->with(['user' => $user, 'addresses' => $addresses]);
    }

    public function showPurchaseHistoryPage()
    {
        if (!Auth::check()) {
            header("Location: " . asset("/register"));
            die();
        }
        $user = Auth::guard()->user();
        $user->birth_date = Carbon::parse($user->from_date)->format('m/d/Y');

        return view('user/purchaseHistory')->with('user', $user);
    }
}