<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Library\ResponseDescription;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\Http\Controllers\Admin\AdminPermissionController;

class AdminLoginController extends Controller
{
    use ThrottlesLogins;

    protected $response = null;

    public function __construct()
    {
        $this->middleware('guest:admin', ['except' => ['logout']]);

        $this->response = new ResponseDescription();
    }

    public function showLoginForm()
    {
        return view('dashboard.login');
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('/');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendFailedLoginResponse(['block' => 'has too many login attempts']);
        }

        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) {
            return $this->sendLoginResponse($request);
        }

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse();
    }

    private function username()
    {
        return 'email';
    }

    private function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|email',
            'password' => 'required|string|min:6',
        ]);
    }

    private function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        $user = Auth::guard('admin')->user();
        $request->session()->put('admin', [
            'id' => $user->id,
            'branch_id' => $user->branch_id,
        ]);

        $permissions = [];
        $security = new AdminPermissionController();
        $permissions = $security->getUserPermissions($user->id);
        $user["permissions"] = $permissions;

        $this->response->setSuccess();
        $this->response->setData($user);

        return $this->response->toJson();
    }

    private function sendFailedLoginResponse($errors = [])
    {
        $errors[$this->username()] = trans('auth.failed');

        $this->response->success = false;
        $this->response->setData($errors);

        return $this->response->toJson();
    }
}