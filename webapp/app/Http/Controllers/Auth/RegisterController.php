<?php

namespace App\Http\Controllers\Auth;

use App\Model\City;
use App\Model\UserAddresses;
use App\Model\UserTree;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Library\ResponseDescription;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        if (Auth()->check()) {
            header("Location: " . asset('/'));
            die();
        }
        $cities = City::where('state', 1)->get();
        return view('pages.register', ['cities' => $cities]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
//            'name' => 'required|max:255',
//            'last_name' => 'required|max:255',
//            'email' => 'required|email|max:255|unique:users',
//            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'last_name' => $data['last_name'],
            'address' => $data['address'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'bonus' => 0,
            'state_id' => 1
        ]);
    }

    public function register(Request $request)
    {
        $response = new ResponseDescription();
        $validator = $this->validator($request->all());

        if (!$validator->fails()) {
            $useData = $request->all();
            if ($useData['password'] != $useData['password2']) {
                $response->setSuccess(false);
                $response->setMessage("პაროლები არ ემთხვევა ერთმანეთს");
                return $response->toJson();
            }
            $user = $this->create($useData);
            UserAddresses::create([
                'user_id' => $user['id'],
                'city_id' => $useData['city_id'],
                'address' => $useData['address'],
                'state' => 1
            ]);

            if ($user && $user->id) {

                if ($this->userSetParent($useData['userTreeId'], $user->id)) {
                    $response->setSuccess();
                    $response->setData($user);
                } else {
                    $response->setSuccess(false);
                    $response->setMessage("დაფიქსირდა შეცდომა");
                    return $response->toJson();
                }
            }
        } else {
            $response->setSuccess(false);
            $response->setMessage("შეავსეთ ყველა აუცილებელი ველი");
            return $response->toJson();
        }

        return $response->toJson();
    }


    protected function userSetParent($userTreeId, $userId)
    {

        if ($userTreeId) {

            $parent = UserTree::where('user_tree_id', $userTreeId)->where('childrens', '<', 2)->first();

            if (!$parent) {
                return false;
            }

            $userTree = new UserTree();
            $userTree['user_id'] = $userId;
            $userTree['parent_id'] = $userTreeId;
            $userTree['childrens'] = 0;
            $userTree['user_parent_path'] = $parent['user_parent_path'] . "/" . $userId;
            $userTree->save();

            $parentPath = $parent['parent_path'] . "/" . $userTree->user_tree_id;
            $userTree['parent_path'] = $parentPath;
            $userTree->save();


            $parent['childrens'] += 1;
            $parent->save();

            return true;

        } else {


            $parent = UserTree::where('childrens', '<', 2)->orderBy("user_tree_id", 'ASC')->first();

            if ($parent) {

                $userTree = new UserTree();
                $userTree['user_id'] = $userId;
                $userTree['parent_id'] = $parent['user_tree_id'];
                $userTree['childrens'] = 0;
                $userTree['user_parent_path'] = $parent['user_parent_path'] . "/" . $userId;
                $userTree->save();

                $parentPath = $parent['parent_path'] . "/" . $userTree->user_tree_id;
                $userTree['parent_path'] = $parentPath;
                $userTree->save();

                $parent['childrens'] += 1;
                $parent->save();

                return true;
            } else {
                return false;
            }
        }
    }
}
