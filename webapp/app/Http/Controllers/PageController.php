<?php

/**
 * Created by PhpStorm.
 * User: ika
 * Date: 10/24/15
 * Time: 6:30 PM
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    private $response = NULL;

    public function __construct()
    {
        $this->response = new \App\Library\ResponseDescription();
    }

    public function getPage($id)
    {
        $page = \App\Model\Post::where('disabled', 0)->where('state', 1)->where('post_id', $id)
            ->first();

        return view('page.newsItem')->with('newsItem', $page);
    }

    public static function page($pageId)
    {
        $page = \App\Model\Post::where('disabled', 0)->where('state', 1)->find($pageId);

        if ($page) {
            $query = \App\Model\Post::select();
            $query->where('disabled', 0)->where('state', 1);

            $subMenu = MenusController::getSubMenu($page->link);
            $upMenu = MenusController::getMenusByIds(explode(",", MenusController::getUpMenuIds(MenusController::getMenuByLink($page['link']))));

            $page = self::modifypageData($page);
            //$galleryImages = Admin\FolderController::getArticleImages($pageId, 2);

            return view('pages.page')->with(['page' => $page, 'subMenu' => $subMenu, 'upMenu' => $upMenu/*, 'galleryImages' => $galleryImages*/]);
        }

        return response(404);
    }

    private static function modifypagesData($data)
    {
        foreach ($data as $key => $value) {
            $value['description_ge'] = \App\Library\Helpers::htmlSpecDecode($value['description_ge']);
            $value['description_en'] = \App\Library\Helpers::htmlSpecDecode($value['description_en']);
            $value['description_ru'] = \App\Library\Helpers::htmlSpecDecode($value['description_ru']);
        }

        return $data;
    }

    private static function modifypageData($data)
    {
        $data['description_ge'] = \App\Library\Helpers::htmlSpecDecode($data['description_ge']);
        $data['description_en'] = \App\Library\Helpers::htmlSpecDecode($data['description_en']);
        $data['description_ru'] = \App\Library\Helpers::htmlSpecDecode($data['description_ru']);

        return $data;
    }
}