<?php

namespace App\Library;


abstract class PostTypes
{
    const news = 1;
    const page = 2;
    const news_category = 3;
    const news_tags = 4;
    const links = 5;
    const slideshow = 6;
}