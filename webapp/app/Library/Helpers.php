<?php
/**
 * Created by PhpStorm.
 * User: ika
 * Date: 10/24/15
 * Time: 9:09 PM
 */

namespace App\Library;

use Carbon\Carbon;
use DateTimeZone;


class Helpers
{
    public static function html_escape($var)
    {
        if (is_array($var)) {
            return array_map('html_escape', $var);
        } else {
            return htmlspecialchars(trim($var), ENT_QUOTES, 'UTF-8');
        }
    }

    public static function strip_html_php($var)
    {
        if (is_array($var)) {
            return array_map('strip_html_php', $var);
        } else {
            return strip_tags(trim($var));
        }
    }

    public static function htmlSpecDecode($var)
    {
        return htmlspecialchars_decode($var);
    }

    public static function htmlDecodeAndStrip($var)
    {
        $var = self::htmlSpecDecode($var);

        return self::strip_html_php($var);
    }

    public static function decodeData($arr, $fields)
    {
        foreach ($arr as $item) {
            foreach ($fields as $field) {
                $item[$field] = Helpers::htmlSpecDecode($item[$field]);
            }
        }

        return $arr;
    }

    public static function convertFromTimestampMilliseconds($timestampInMilliseconds)
    {
        return Carbon::createFromTimestamp(date($timestampInMilliseconds / 1000), new DateTimeZone(config('app.timezone')));
    }
}