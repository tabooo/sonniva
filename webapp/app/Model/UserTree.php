<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserTree extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_tree';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'user_tree_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'parent_id', 'parent_path', 'user_parent_path', 'childrens'];

    public static $rules = [
        'user_id' => 'required',
        'parent_id' => 'required'
    ];

    public function children()
    {

        return $this->hasMany('App\Model\UserTree', 'user_id', 'parent_id');

    }

    public function user()
    {

        return $this->hasOne('App\User', 'id', 'user_id');

    }
}
