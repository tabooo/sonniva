<?php namespace App\Model;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'display_name', 'description',
    ];

    public static $rules = [
        'name' => 'required|unique:roles|max:255',
        'display_name' => 'required|max:255',
        'description' => 'required|max:255'
    ];
}