<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'product_id';

    public static $tbl = 'products';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'supplier_code', 'barcode', 'name', 'description', 'category_id', 'minamount', 'price',
        'condition_id', 'state_id', 'self_price', 'last_price', 'unit_name', 'unit_id', 'insert_date'];

    public static $rules = [
        'name' => 'required',
        'category_id' => 'required|integer',
        'insert_date' => 'required',
        'condition_id' => 'integer',
        'state_id' => 'integer',
        'unit_id' => 'integer',
    ];

    public function productInfo()
    {
        return $this->hasMany('App\Model\ProductInfo', 'product_id', 'product_id')->where('state_id', 1)
            ->orderBy('order', 'ASC');
    }

    public function productFile()
    {
        return $this->hasMany('App\Model\ProductFile', 'product_id', 'product_id')->where('state_id', 1);
    }
}
