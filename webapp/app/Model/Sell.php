<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Sell extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sell';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'sell_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['transaction_id', 'user_id', 'whole_price', 'city_id', 'address', 'shipping_price', 'payment_type',
        'state', 'first_name', 'last_name', 'email', 'phone', 'company', 'address', 'country', 'city', 'note', 'invoice_id',];

    public static $rules = [
    ];

    public function sellItems()
    {
        return $this->hasMany('App\Model\SellItem', 'sell_id', 'sell_id')->where('state_id', 1);
    }
}
