<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'branches';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'branch_id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['branch_name', 'branch_state_id'];

    public static $rules = [
        'branch_name' => 'required',
        'branch_state_id' => 'required',
        'branch_id' => 'integer'
    ];
}
