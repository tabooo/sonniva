<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ShoppingCart extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shoppingcart';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'identifier';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

}
