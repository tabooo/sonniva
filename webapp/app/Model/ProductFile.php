<?php
/**
 * Created by PhpStorm.
 * User: Irakli Gogiashvili
 * Date: 5/15/2017
 * Time: 12:32 PM
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductFile extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_files';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['product_id', 'file_name', 'original_file_name', 'is_cover'];

    public static $rules = [
        'product_id' => 'required|integer|min:1',
        'file_name' => 'required|max:255',
        'original_file_name' => 'required|max:255',
        'is_cover' => 'max:1'
    ];
}