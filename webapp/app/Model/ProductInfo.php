<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductInfo extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_info';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'product_info_id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 'product_info_type_id', 'product_info_type_name', 'value', 'state_id', 'order'];

    public static $rules = [
        'product_id' => 'required' | 'integer',
        'product_info_type_id' => 'required|integer',
        'product_info_type_name' => 'required',
        'value' => 'required',
        'state_id' => 'integer',
    ];
}
