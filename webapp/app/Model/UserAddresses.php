<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserAddresses extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_addresses';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'address_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['address', 'city_id', 'user_id', 'primary', 'state'];

    public static $rules = [
    ];

    public function city()
    {
        return $this->belongsTo('App\Model\City', 'city_id', 'city_id');
    }
}
