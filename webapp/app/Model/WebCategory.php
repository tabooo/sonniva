<?php
/**
 * Created by PhpStorm.
 * User: ika
 * Date: 10/24/15
 * Time: 8:16 PM
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WebCategory extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'web_categories';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'category_id';

    public static $tbl = 'web_categories';
    public static $pKey = 'category_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name_ge', 'name_en', 'name_ru', 'parent_id', 'link', 'type', 'img_path', 'color'];

    public static $rules = [
        'name_ge' => 'required|max:255',
        'name_en' => 'required|max:255'
    ];

    public function parent()
    {

        return $this->hasOne('App\Model\WebCategory', 'category_id', 'parent_id');

    }

    public function children()
    {

        return $this->hasMany('App\Model\WebCategory', 'parent_id', 'category_id')->where('state', 1);

    }

    public static function tree()
    {
        function getCatItems($data)
        {
            for ($i = 0; $i < count($data); $i++) {
                if (count($data[$i]->children) > 0) {
                    getCatItems($data[$i]->children);
                }
            }
        }

        $cat = static::with(implode('.', array_fill(0, 10, 'children')))->where('parent_id', '=', NULL)->where('state', 1)->get();
        getCatItems($cat);
        return $cat;

    }

    public static function treeFresh()
    {
        return static::with(implode('.', array_fill(0, 10, 'children')))->where('parent_id', '=', NULL)->where('state', 1)->get();
    }

    static function  removeDeletedCats($data)
    {
        for ($i = 0; $i < count($data); $i++) {
            if ($data[$i]->state == 0) {
                unset($data[$i]);
            }
            if (count($data[$i]->children) > 0) {
                self::removeDeletedCats($data[$i]->children);
            }
        }
    }
}