<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SellItem extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sell_items';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'sell_item_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['sell_id', 'product_id', 'product_price', 'amount', 'whole_price', 'state'];

    public static $rules = [
    ];
}
