<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'langs';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'lang_id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['key', 'text_ka', 'text_en', 'text_ru', 'text_tr'];

    public static $rules = [
        'key' => 'required',
        'text_ka' => 'required'
    ];
}
