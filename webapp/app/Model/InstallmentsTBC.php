<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InstallmentsTBC extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'installments_tbc';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['sell_id', 'state', 'session_id', 'status'];

    public static $rules = [
        'session_id' => 'required',
        'sell_id' => 'required',
    ];

    public function sell()
    {
        return $this->hasOne('App\Model\Sell', 'sell_id', 'sell_id');
    }
}
