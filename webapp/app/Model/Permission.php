<?php namespace App\Model;

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'display_name', 'description',
    ];

    public static $rules = [
        'name' => 'required|unique:permissions|max:255',
        'display_name' => 'required|max:255',
        'description' => 'required|max:255'
    ];
}