<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'category';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'category_id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_name', 'parent_id', 'childrens', 'parents', 'category_id',
        'state_id', 'order'];

    public static $rules = [
        'category_name' => 'required',
        'parent_id' => 'integer',
        'category_id' => 'integer',
        'state_id' => 'integer'
    ];
    /**
     * @var mixed
     */
    public $parent;

    public function children()
    {
        return $this->hasMany('App\Model\Category', 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Model\Category', 'parent_id');
    }
}
