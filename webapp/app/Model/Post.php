<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'web_posts';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'post_id';

    public static $tbl = 'web_posts';
    public static $pKey = 'post_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title_ge', 'title_en', 'title_ru',
        'keyword_ge', 'keyword_en', 'keyword_ru',
        'google_description_ge', 'google_description_en', 'google_description_ru',
        'description_ge_short', 'description_en_short', 'description_ru_short',
        'description_ge', 'description_en', 'description_ru',
        'img_path', 'published_at', 'link_name', 'type', 'link', 'order'];

    public static $rules = [
        'showInGallery' => 'integer|min:0',
    ];

    public function meta()
    {
        return $this->hasMany('App\Model\PostMeta',  'post_id', 'post_id');
    }

    public function menu()
    {
        return $this->belongsTo('App\Model\Menu', 'post_id', 'link');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Model\WebCategory', 'web_post_categories', 'post_id', 'category_id');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Model\WebCategory', 'web_post_categories', 'post_id', 'category_id');
    }
}
