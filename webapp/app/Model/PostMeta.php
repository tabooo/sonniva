<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PostMeta extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'web_postmeta';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'meta_id';

    public static $tbl = 'web_postmeta';
    public static $pKey = 'meta_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['post_id', 'meta_key', 'meta_value'];

    public static $rules = [];
}
