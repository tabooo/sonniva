<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Storage;

class ProductInfoTypes extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_info_types';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'product_info_type_id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['product_info_type_name'];

}
