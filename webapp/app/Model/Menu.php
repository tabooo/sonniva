<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'web_menu';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'menu_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name_ge', 'name_en', 'name_ru', 'disabled', 'link', 'parent_id', 'order', 'url'];

    public static $rules = [
        'name_ge' => 'required',
        'name_en' => 'required',
        'name_ru' => 'required',
        'order' => 'required'
    ];

    public function parent()
    {

        return $this->hasOne('App\Model\Menu', 'menu_id', 'parent_id');

    }

    public function children()
    {

        return $this->hasMany('App\Model\Menu', 'parent_id', 'menu_id')->where('state', 1)->orderBy('order', 'ASC');

    }

    public static function tree()
    {
        function getMenuItems($data)
        {
            for ($i = 0; $i < count($data); $i++) {
                if (count($data[$i]->children) > 0) {
                    getMenuItems($data[$i]->children);
                }
            }
        }

        $menu = static::with(implode('.', array_fill(0, 10, 'children')))->where('parent_id', '=', NULL)->where('state', 1)->where('disabled', 0)->orderBy('order', 'ASC')->get();
        getMenuItems($menu);
        return $menu;

    }

    public static function treeFresh()
    {
        return static::with(implode('.', array_fill(0, 10, 'children')))->where('parent_id', '=', NULL)->where('state', 1)->orderBy('order', 'ASC')->get();
    }

    static function  removeDeletedMenus($data)
    {
        for ($i = 0; $i < count($data); $i++) {
            if ($data[$i]->state == 0) {
                //array_splice($data, $i, 1);
                unset($data[$i]);
            }
            if (count($data[$i]->children) > 0) {
                self::removeDeletedMenus($data[$i]->children);
            }
        }
    }
}
