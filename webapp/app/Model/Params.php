<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Params extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'web_params';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'param_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['param_id', 'name', 'value'];

    public static $rules = [
    ];

}
