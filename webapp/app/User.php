<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'last_name', 'address', 'birth_date',
        'personal_no', 'mobile_no', 'email', 'password',
        'bonus', 'city_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /*public static function parentsCount($parent_id)
    {
        $count = 1;
        if ($parent_id == null) {
            return 0;
        }

        $parent = User::where('id', $parent_id)->get()[0];
        User::parentsCount($parent['parent_id']);
        $count++;
        return $count;
    }*/
}
