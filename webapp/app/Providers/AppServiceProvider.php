<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $menus = \App\Model\Menu::tree();
        $params = \App\Http\Controllers\PublicController::getParams();
        $categories = \App\Http\Controllers\PublicController::getCategoriesTree();

        view()->composer('*', function ($view) use ($params, $menus, $categories) {
            $view->with('params', $params)->with('menus', $menus)->with('categories', $categories);
        });

        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
