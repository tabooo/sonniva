-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 10.28.1.117    Database: onlinemarket
-- ------------------------------------------------------
-- Server version	5.5.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `branches`
--

DROP TABLE IF EXISTS `branches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `branches` (
  `BRANCH_ID` int(11) NOT NULL AUTO_INCREMENT,
  `BRANCH_NAME` varchar(200) CHARACTER SET utf8 NOT NULL,
  `BRANCH_STATE_ID` int(2) NOT NULL DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`BRANCH_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `branches`
--

LOCK TABLES `branches` WRITE;
/*!40000 ALTER TABLE `branches` DISABLE KEYS */;
INSERT INTO `branches` VALUES (1,'რუსთავი',1,NULL,NULL),(2,'თბილისი',1,NULL,NULL),(3,'გორი',1,NULL,NULL),(4,'xinkali',1,'2017-05-03 13:39:36','2017-05-03 09:39:36');
/*!40000 ALTER TABLE `branches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `CATEGORY_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CATEGORY_NAME` varchar(100) CHARACTER SET utf8 NOT NULL,
  `PARENT_ID` int(11) DEFAULT NULL,
  `CHILDRENS` text,
  `PARENTS` text,
  `cancel_date` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `STATE_ID` int(1) NOT NULL,
  PRIMARY KEY (`CATEGORY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'– ყველა –',NULL,'74,79,80,81,82',NULL,NULL,'0000-00-00 00:00:00','2017-05-08 11:17:19',1),(74,'ნახევარ-ფაბრიკატეbii',1,'76','1',NULL,'0000-00-00 00:00:00','2017-05-02 14:49:51',1),(76,'ხინკალი',74,NULL,'74',NULL,'0000-00-00 00:00:00','2017-05-02 12:08:58',1),(79,'ქათმები',1,'83,85','1',NULL,'0000-00-00 00:00:00','2017-05-08 11:17:19',1),(80,'ზეთები',1,NULL,'1',NULL,'0000-00-00 00:00:00','2017-05-02 12:08:58',1),(81,'თევზეული',1,NULL,'1',NULL,'0000-00-00 00:00:00','2017-05-02 12:08:58',1),(82,'xinkali',1,NULL,'1','2017-05-02 15:24:57','2017-05-02 09:15:13','2017-05-02 15:24:57',1),(83,'კვერცხები',79,NULL,'79',NULL,'2017-05-08 07:08:44','2017-05-08 11:16:04',1),(84,'asdadsa',79,NULL,'79','2017-05-08 11:17:19','2017-05-08 07:11:44','2017-05-08 11:17:19',2),(85,'წიწილები',79,NULL,'79',NULL,'2017-05-08 07:15:29','2017-05-08 11:15:29',1);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `langs`
--

DROP TABLE IF EXISTS `langs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `langs` (
  `LANG_ID` int(11) NOT NULL AUTO_INCREMENT,
  `KEY` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `TEXT_KA` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TEXT_EN` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TEXT_RU` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TEXT_TR` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`LANG_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=288 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `langs`
--

LOCK TABLES `langs` WRITE;
/*!40000 ALTER TABLE `langs` DISABLE KEYS */;
INSERT INTO `langs` VALUES (1,'ACTION','მოქმედება','Action','Действие','Action','0000-00-00 00:00:00',NULL),(2,'ACTIVE','აქტიური','Active','Активный','Active','0000-00-00 00:00:00',NULL),(3,'ADD','დამატება','Add','Добавить','Add','0000-00-00 00:00:00',NULL),(4,'ADDRESS','მისამართი','Address','Адрес','Address','0000-00-00 00:00:00',NULL),(5,'CANCEL','გაუქმება','Cancel','Отменить','Cancel','0000-00-00 00:00:00',NULL),(6,'CHANGE_PASSWORD','პაროლის შეცვლა','Change Password','Сменить пароль','Change Password','0000-00-00 00:00:00',NULL),(7,'CLEAR','გასუფთავება','Clear','Очистить','Clear','0000-00-00 00:00:00',NULL),(8,'CLOSE','დახურვა','Close','Закрыть','Close','0000-00-00 00:00:00',NULL),(9,'DATE','თარიღი','Date','Дата','Date','0000-00-00 00:00:00',NULL),(10,'DELETE','წაშლა','Delete','Удалить','Delete','0000-00-00 00:00:00',NULL),(11,'EDIT','რედაქტირება','Edit','Редактировать','Edit','0000-00-00 00:00:00',NULL),(12,'ERROR','შეცდომა','Error','Ошибка','Error','0000-00-00 00:00:00',NULL),(13,'EXIT','გასვლა','Logout','Выход','Logout','0000-00-00 00:00:00',NULL),(14,'EXPORT','ექსპორტი','Export','Экспорт','Export','0000-00-00 00:00:00',NULL),(15,'FINALIZE','დასრულება','Finalize','Завершить','Finalize','0000-00-00 00:00:00',NULL),(16,'FIRST_NAME','სახელი','Firstname','Имя','Firstname','0000-00-00 00:00:00',NULL),(17,'FROM_WHO','ვისგან','From','Из','From','0000-00-00 00:00:00',NULL),(18,'HISTORY','ისტორია','History','История','History','0000-00-00 00:00:00',NULL),(19,'IDENTITY_CODE','საიდენტიფიკაციო კოდი','Identity code','Идентификационный код','Identity code','0000-00-00 00:00:00',NULL),(20,'INACTIVE','არააქტიური','არააქტიური','Неактивный','არააქტიური','0000-00-00 00:00:00',NULL),(21,'INFO','ინფო','Info','Инфо','Info','0000-00-00 00:00:00',NULL),(22,'IP_ADDRESS','IP მისამართი','IP Address','IP адрес','IP Address','0000-00-00 00:00:00',NULL),(23,'JULY','ივლისი','July','Июль','July','0000-00-00 00:00:00',NULL),(24,'JUNE','ივნისი','June','Июнь','June','0000-00-00 00:00:00',NULL),(25,'LANGUAGE','ენა','Language','Язык','Language','0000-00-00 00:00:00',NULL),(26,'LAST_NAME','გვარი','Lastname','Фамилия','Lastname','0000-00-00 00:00:00',NULL),(27,'LOGIN','შესვლა','Login','Войти','Login','0000-00-00 00:00:00',NULL),(28,'MAIN_INFO','ძირითადი ინფორმაცია','Basic Information','Основная информация','Basic Information','0000-00-00 00:00:00',NULL),(29,'MARCH','მარტი','March','Март','March','0000-00-00 00:00:00',NULL),(30,'MAY','მაისი','May','Май','May','0000-00-00 00:00:00',NULL),(31,'NEW_PASSWORD','ახალი პაროლი','New password','Новый пароль','New password','0000-00-00 00:00:00',NULL),(32,'NO_RECORDS_FOUND','ვერ მოიძებნა ჩანაწერები','No records found','Записей не найдено','No records found','0000-00-00 00:00:00',NULL),(33,'NOTE','შენიშვნა','Note','Note','Note','0000-00-00 00:00:00',NULL),(34,'NOVEMBER','ნოემბერი','November','Ноябрь','November','0000-00-00 00:00:00',NULL),(35,'OCTOBER','ოქტომბერი','October','Октябрь','October','0000-00-00 00:00:00',NULL),(36,'OLD_PASSWORD','ძველი პაროლი','Old password','Старый пароль','Old password','0000-00-00 00:00:00',NULL),(37,'OPTIONS','პარამეტრები','Settings','Опции','Settings','0000-00-00 00:00:00',NULL),(38,'ORGANISATION','ორგანიზაცია','Organisation','Организация','Organisation','0000-00-00 00:00:00',NULL),(39,'OVERDUE','ვადაგასული','Overdue','Истекший','Overdue','0000-00-00 00:00:00',NULL),(40,'OWN','საკუთარი','Own','Собственный','Own','0000-00-00 00:00:00',NULL),(41,'PAGE','გვერდი','Page','Страница','Page','0000-00-00 00:00:00',NULL),(42,'PAGES','გვერდები','Pages','Страницы','Pages','0000-00-00 00:00:00',NULL),(43,'PASSWORD','პაროლი','Password','Пароль','Password','0000-00-00 00:00:00',NULL),(44,'PERSONAL','პირადი','Personal','Личное','Personal','0000-00-00 00:00:00',NULL),(45,'PLEASE_WAIT','გთხოვთ, დაელოდოთ','Please Wait...','Пожалуйста, подождите','Please Wait...','0000-00-00 00:00:00',NULL),(46,'PRINT','ბეჭდვა','Print','Печать','Print','0000-00-00 00:00:00',NULL),(47,'PRINT_PREVIEW','საბეჭდად გადახედვა','Print Preview','Предварительный просмотр','Print Preview','0000-00-00 00:00:00',NULL),(48,'REFRESH','განახლება','Refresh','Обновить','Refresh','0000-00-00 00:00:00',NULL),(49,'REPEAT_NEW_PASSWORD','გაიმეორეთ ახალი პაროლი','Repeat new password','Повторите новый пароль','Repeat new password','0000-00-00 00:00:00',NULL),(50,'RESULT','შედეგი','Result','Результат','Result','0000-00-00 00:00:00',NULL),(51,'SAVE','შენახვა','Save','Сохранить','Save','0000-00-00 00:00:00',NULL),(52,'SEARCH','ძებნა','Search','Поиск','Search','0000-00-00 00:00:00',NULL),(53,'SELECT','არჩევა','Select','Выбирать','Select','0000-00-00 00:00:00',NULL),(54,'SELECT_A_FILE','აირჩიეთ ფაილი','Select a file','Выберите файл','Select a file','0000-00-00 00:00:00',NULL),(55,'SEND','გადაგზავნა','Send','Переслать','Send','0000-00-00 00:00:00',NULL),(56,'SEPTEMBER','სექტემბერი','September','Сентябрь','September','0000-00-00 00:00:00',NULL),(57,'STATES','სტატუსები','Statuses','Статусы','Statuses','0000-00-00 00:00:00',NULL),(58,'STATE','სტატუსი','State','Статус','State','0000-00-00 00:00:00',NULL),(59,'TIME','დრო','Time','Время','Time','0000-00-00 00:00:00',NULL),(60,'TITLE','სათაური','Title','название','Title','0000-00-00 00:00:00',NULL),(61,'TEXT','ტექსტი','Text','Текст','Text','0000-00-00 00:00:00',NULL),(62,'TO_ARCHIVE','დაარქივება','Archive','Архивировать','Archive','0000-00-00 00:00:00',NULL),(63,'TO_WHO','ვის','To','Кому','To','0000-00-00 00:00:00',NULL),(64,'TOTAL','სულ','Total','Всего','Total','0000-00-00 00:00:00',NULL),(65,'TYPE','ტიპი','Type','Вид','Type','0000-00-00 00:00:00',NULL),(66,'USERNAME','მომხმარებელი','User','Пользователь','User','0000-00-00 00:00:00',NULL),(67,'VERSION','ვერსია','Version','Версия','Version','0000-00-00 00:00:00',NULL),(68,'VIEW_ALL','ყველას ნახვა','View All','View All','View All','0000-00-00 00:00:00',NULL),(69,'PARCEL_NO','ამანათი','Parcel #','посылка','Parcel #','0000-00-00 00:00:00',NULL),(70,'MARK_AS_REGISTERED','დადასტურება','დადასტურება','подтвердить','დადასტურება','0000-00-00 00:00:00',NULL),(71,'FILENAMES','ფაილები','filenames','файлы','filenames','0000-00-00 00:00:00',NULL),(72,'ADD_INGREDIENT','ინგრედიენტის დამატება','Add ingredient','добавить ингридиент','Add ingredient','0000-00-00 00:00:00',NULL),(73,'ADMINISTRATOR','ადმინისტრატორი','Administrator','администратор','Administrator','0000-00-00 00:00:00',NULL),(74,'AMOUNT','რაოდენობა','Amount','количество','Amount','0000-00-00 00:00:00',NULL),(75,'ASK_DELETE','გსურთ წაშლა?','Do you want to delete?','Удалить?','Do you want to delete?','0000-00-00 00:00:00',NULL),(76,'CASHIER','მოლარე','Cashier','кассир','Cashier','0000-00-00 00:00:00',NULL),(77,'CATEGORIES','კატეგორიები','Categories','категории','Categories','0000-00-00 00:00:00',NULL),(78,'CATEGORY','კატეგორია','Category','категория','Category','0000-00-00 00:00:00',NULL),(79,'CHOOSE_SECTION','მიუთითეთ სექცია','Choose section','укажите секцию','Choose section','0000-00-00 00:00:00',NULL),(80,'DEFAULT_COLOR','თავდაპირველი ფერი','Default color','начальный цвет','Default color','0000-00-00 00:00:00',NULL),(81,'DESCRIPTION','აღწერა','Description','описание','Description','0000-00-00 00:00:00',NULL),(82,'FOODS','კერძები','Foods','блюда','Foods','0000-00-00 00:00:00',NULL),(83,'IMAGE','სურათი','Image','изображение','Image','0000-00-00 00:00:00',NULL),(84,'INGREDIENTS','ინგრედიენტები','Ingredients','ингридиенты','Ingredients','0000-00-00 00:00:00',NULL),(85,'MIN_AMOUNT','კრიტ. რაოდ.','კრიტ. რაოდ.','критическое количество','კრიტ. რაოდ.','0000-00-00 00:00:00',NULL),(86,'NAME1','დასახელება','Name','Наименование','Name','0000-00-00 00:00:00',NULL),(87,'NAME2','სახელი','Name','Имя','Name','0000-00-00 00:00:00',NULL),(88,'NAVIGATION','ნავიგაცია','Navigation','навигация','Navigation','0000-00-00 00:00:00',NULL),(89,'NEW_SELL','ახალი გაყიდვა','New sell','новая продажа','New sell','0000-00-00 00:00:00',NULL),(90,'NOTICE','შეტყობინება','Notice','сообщение','Notice','0000-00-00 00:00:00',NULL),(91,'PLACE','ადგილი','Place','место','Place','0000-00-00 00:00:00',NULL),(92,'PRICE','ფასი','Price','цена','Price','0000-00-00 00:00:00',NULL),(93,'RECIEVINGS','მიღებები','Recievings','закупки','Recievings','0000-00-00 00:00:00',NULL),(94,'REPORTS','რეპორტები','Reports','отчеты','Reports','0000-00-00 00:00:00',NULL),(95,'SELLS','გაყიდვები','Sells','продажи','Sells','0000-00-00 00:00:00',NULL),(96,'TABLES','მაგიდები','Tables','столы','Tables','0000-00-00 00:00:00',NULL),(97,'SECTIONS','სექციები','Sections','секции','Sections','0000-00-00 00:00:00',NULL),(98,'PLACES','ადგილები','Places','места',NULL,'0000-00-00 00:00:00',NULL),(99,'NEW_CATEGORY','ახალი კატეგორია','New category','новая категория','New category','0000-00-00 00:00:00',NULL),(100,'ADD_CATEGORY','კატეგორიის დამატება','Add category','добавить категорию','Add category','0000-00-00 00:00:00',NULL),(101,'EDIT_CATEGORY','კატეგორიის რედაქტირება','Edit category','редактирование категории','Edit category','0000-00-00 00:00:00',NULL),(102,'ONE_PRICE','ერთ. ფასი','One price','цена единицы','One price','0000-00-00 00:00:00',NULL),(103,'WHOLEPRICE','ჯამი','Whole price','сумма','Whole price','0000-00-00 00:00:00',NULL),(104,'INVOICE_NO','ინვოისის №','Invoice №','№ Инвойса','Invoice №','0000-00-00 00:00:00',NULL),(105,'PROVIDER','მომწოდებელი','Provider','поставщик','Provider','0000-00-00 00:00:00',NULL),(106,'MANUFACTURER','მწარმოებელი','Manufacturer','изготовитель','Manufacturer','0000-00-00 00:00:00',NULL),(107,'YES','დიახ','Yes','Да','Yes','0000-00-00 00:00:00','2017-05-02 13:21:05'),(108,'NO','არა','No','Нет','No','0000-00-00 00:00:00',NULL),(109,'NEW_INGREDIENT','ახალი ინგრედიენტი','New ingredient','новый ингридиент','New ingredient','0000-00-00 00:00:00',NULL),(110,'RECIEVE_INGREDIENT','ინგრედიენტის მიღება','Recieve ingredient','прием ингридиента','Recieve ingredient','0000-00-00 00:00:00',NULL),(111,'CHOOSE_TABLE','მიუთითეთ მაგიდა','Choose table','укажите стол','Choose table','0000-00-00 00:00:00',NULL),(112,'CASH','ნაღდი','Cash','наличные','Cash','0000-00-00 00:00:00',NULL),(113,'TRANSFER','გადარიცხვა','Transfer','безналичные','Transfer','0000-00-00 00:00:00',NULL),(114,'NEW_PLACE','ახალი ადგილი','New place','новое место','New place','0000-00-00 00:00:00',NULL),(115,'ADD_PLACE','ადგილის დამატება','Add place','добавить место','Add place','0000-00-00 00:00:00',NULL),(116,'ADD_INGREDIENT_ON_PRODUCT','პროდუქტზე ინგრედიენტის დამატება','Add ingredient on product','добавить ингридиент в продукт','Add ingredient on product','0000-00-00 00:00:00',NULL),(117,'INGREDIENT','ინგრედიენტი','Ingredient','ингридиент','Ingredient','0000-00-00 00:00:00',NULL),(118,'NEW_PRODUCT','ახალი პროდუქტი','new Product','Новый товар','new Product','0000-00-00 00:00:00',NULL),(119,'ADD_PRODUCT','კერძის დამატება','Add product','добавить блюдо','Add product','0000-00-00 00:00:00',NULL),(120,'ADD_SECTION','სექციის დამატება','Add section','добавить секцию','Add section','0000-00-00 00:00:00',NULL),(121,'SECTION','სექცია','Section','секциа','Section','0000-00-00 00:00:00',NULL),(122,'FILL_ALL_FIELDS','გთხოვთ მიუთითოთ ყველა აუცილებელი ველი','Fill all required fields','заполните все обязательные поля','Fill all required fields','0000-00-00 00:00:00',NULL),(123,'ADD_TABLE','მაგიდის დამატება','Add table','добавить стол','Add table','0000-00-00 00:00:00',NULL),(124,'NEW_TABLE','ახალი მაგიდა','New table','новый стол','New table','0000-00-00 00:00:00',NULL),(125,'TABLE','მაგიდა','Table','стол','Table','0000-00-00 00:00:00',NULL),(126,'CANCEL_ORDER','შეკვეთის გაუქმება','Cancel order','отмена заказа','Cancel order','0000-00-00 00:00:00',NULL),(127,'USERS','მომხმარებლები','Users','пользователи','Users','0000-00-00 00:00:00',NULL),(128,'ADD_USER','მომხმარებლის დამატება','User added','добавить пользователя','User added','0000-00-00 00:00:00',NULL),(129,'RANK','რანკი','Rank','ранг','Rank','0000-00-00 00:00:00',NULL),(130,'PERSONAL_NO','პირადი ნომერი','Personal No','личный номер','Personal No','0000-00-00 00:00:00',NULL),(131,'PHONE','ტელეფონი','Phone','телефон','Phone','0000-00-00 00:00:00',NULL),(132,'EMAIL','ელ. ფოსტა','eMail','эл.почта','eMail','0000-00-00 00:00:00',NULL),(133,'PRINTER','პრინტერი','Printer','Принтер','Printer','0000-00-00 00:00:00',NULL),(134,'WAITER','ოფიციანტი','Waiter','официант','Waiter','0000-00-00 00:00:00',NULL),(135,'SELLITEM_REPORTS','კერძების რეპორტები','კერძების რეპორტები','Отчеты по блюдам','კერძების რეპორტები','0000-00-00 00:00:00',NULL),(136,'ORDER_NUMBER','შეკვ. №','Order №','Заказ. №','Order №','0000-00-00 00:00:00',NULL),(137,'SEARCH_ORDER','შეკვეთის ძებნა','Search Order','Поиск заказа','Search Order','0000-00-00 00:00:00',NULL),(138,'SELF_PRICE','თვითღირებულება','Self Price','Себестоимость','Self Price','0000-00-00 00:00:00',NULL),(139,'CALL','გამოძახება','CALL','Вызов','CALL','0000-00-00 00:00:00',NULL),(140,'MESSAGE','შეტყობინება','Message','Сообщение','Message','0000-00-00 00:00:00',NULL),(141,'CONFIRM','დასტური','Confirm','Подтвердить','Confirm','0000-00-00 00:00:00',NULL),(142,'MENUS','მენიუები','Menus','Меню','Menus','0000-00-00 00:00:00',NULL),(143,'ADD_MENU','მენიუს დამატება','Add Menu','Добавить меню','Add Menu','0000-00-00 00:00:00',NULL),(144,'PERCENT','პროცენტი','Percent','Процент','Percent','0000-00-00 00:00:00',NULL),(145,'TAKE_MONEY','მოწ. თანხა','Money','Поданная сумма','Money','0000-00-00 00:00:00',NULL),(146,'RETURN_CHANGE','ხურდა','Change','Сдача','Change','0000-00-00 00:00:00',NULL),(147,'MESSAGES','შეტყობინებები','Messages','Сообщения','Messages','0000-00-00 00:00:00',NULL),(148,'INGREDIENTS_REPORTS','ინგრედიენტების რეპორტები','Ingredients Reports','Отчеты по ингридиентам','Ingredients Reports','0000-00-00 00:00:00',NULL),(149,'OPERATIONS','ოპერაციები','Operations','Операции','Operations','0000-00-00 00:00:00',NULL),(150,'RECIEVING_HISTORY','მიღებების ისტორია','Recieving History','История закупок','Recieving History','0000-00-00 00:00:00',NULL),(151,'SELL_HISTORY','გაყიდვების ისტორია','Sell History','История продаж','Sell History','0000-00-00 00:00:00',NULL),(152,'GUEST_COUNT','სტუმრების რაოდენობა','Guest Count','Кол-во гостей','Guest Count','0000-00-00 00:00:00',NULL),(153,'VIEW_ORDER','შეკვეთის ნახვა','View Order','Смотреть заказ','View Order','0000-00-00 00:00:00',NULL),(154,'ADD_IMAGE','სურათის დამატება','Add Image','Добавить картинку','Add Image','0000-00-00 00:00:00',NULL),(155,'SEND_MSG','მიწერე მიმტანს','Send Message','Послать сообщение','Send Message','0000-00-00 00:00:00',NULL),(156,'ADDITIONAL_INFO','დამატებითი ინფო','Additional Info','Доп. Инфо','Additional Info','0000-00-00 00:00:00',NULL),(157,'ADD_ADDITIONAL_INFO','დამატებითი ინფოს დამატება','Add Additional Info','Добавить доп. инфо','Add Additional Info','0000-00-00 00:00:00',NULL),(158,'ADD_NEW_ADDETIONAL_INFO_TYPE','ახალი დამატებითი ინფოს დამატება','Add new additional info type','Добавить новое доп. инфо','Add new additional info type','0000-00-00 00:00:00',NULL),(159,'CHOOSE_FILE','აირჩიეთ ფაილი','Choose file','Выбрать файл','Choose file','0000-00-00 00:00:00',NULL),(160,'RESOURCES','რესურსები','Resources','Ресурсы','Resources','0000-00-00 00:00:00',NULL),(161,'FILE_SIZE_ERROR_30','ფაილის ზომა არ უნდა აღემატებოდეს 30 kb-ს','Max allowed file size is 30 kb','Размер файла не должен превышать 30 Кб','Max allowed file size is 30 kb','0000-00-00 00:00:00',NULL),(162,'FILE_SIZE_ERROR_500','ფაილის ზომა არ უნდა აღემატებოდეს 500 kb-ს','Max allowed file size is 500 kb','Размер файла не должен превышать 500 Кб','Max allowed file size is 500 kb','0000-00-00 00:00:00',NULL),(163,'CHOOSE_FILE_ERROR','გთხოვთ მიუთითოთ ფაილი','Choose file','Укажите файл','Choose file','0000-00-00 00:00:00',NULL),(164,'RECIEVE_PRODUCT','პროდუქტის მიღება','Recieve product','Принять товар','Recieve product','0000-00-00 00:00:00',NULL),(165,'SPLIT_AMOUNT','დაშლილი რაოდენობა','Split Amount','Штучное кол-во','Split Amount','0000-00-00 00:00:00',NULL),(166,'BARCODE','შტრიხკოდი','Barcode','Штрихкод','Barcode','0000-00-00 00:00:00',NULL),(167,'IN_STORAGE','საწყობში','In Storage','В складе','In Storage','0000-00-00 00:00:00',NULL),(168,'RECIEVE_BY_INVOICE','ინვოისით მიღება','Recieve by Invoice','Прием по инвойсу','Recieve by Invoice','0000-00-00 00:00:00',NULL),(169,'WEIGHT','წონა','Weight','Вес','Weight','0000-00-00 00:00:00',NULL),(170,'WARRANTY','გარანტია','Warranty','Гарантия','Warranty','0000-00-00 00:00:00',NULL),(171,'CONDITION','მდგომარეობა','Condition','Состояние','Condition','0000-00-00 00:00:00',NULL),(172,'NEW','ახალი','New','Новый','New','0000-00-00 00:00:00',NULL),(173,'USED','მეორადი','Used','Вторичный','Used','0000-00-00 00:00:00',NULL),(174,'PRICE_WHOLESALE','ფასი საბითუმო','Price Wholesale','Оптовая цена','Price Wholesale','0000-00-00 00:00:00',NULL),(175,'PRICE_VIP','ფასი VIP','Price VIP','Цена VIP','Price VIP','0000-00-00 00:00:00',NULL),(176,'SPLIT','დაშლა','Split','Поштучно','Split','0000-00-00 00:00:00',NULL),(177,'FILL_SPLIT_AMOUNT','მიუთითეთ დაშლის რაოდენობა','Fill Split Amount','Укажите поштучное кол-во','Fill Split Amount','0000-00-00 00:00:00',NULL),(178,'INVOICE_NUMBER','ინვოისის ნომერი','Invoice Number','Номер инвойса','Invoice Number','0000-00-00 00:00:00',NULL),(179,'FILL_ALL_IMPORTANT_FIELDS','შეავსეთ ყველა სავალდებულო ველი','Fill all important fields','Заполните все обязательные поля','Fill all important fields','0000-00-00 00:00:00',NULL),(180,'SEARCH_PRODUCT','პროდუქტის ძებნა','Search Product','Поиск товара','Search Product','0000-00-00 00:00:00',NULL),(181,'BRANCH','ფილიალი','Branch','Филиал','Branch','0000-00-00 00:00:00',NULL),(182,'WHOLE','მთლიანი','Whole','Целый','Whole','0000-00-00 00:00:00',NULL),(183,'PRODUCTS','პროდუქტები','Products','Товары','Products','0000-00-00 00:00:00',NULL),(184,'CLIENTS','კლიენტები','Clients','Клиенты','Clients','0000-00-00 00:00:00',NULL),(185,'PAYMENTS','გადახდები','Payments','Платежи','Payments','0000-00-00 00:00:00',NULL),(186,'ADD_PAYMENT','გადახდა','New Payment','Оплатить','New Payment','0000-00-00 00:00:00',NULL),(187,'PAYMENT_TYPE','გადახდის ტიპი','Payment Type','Тип оплаты','Payment Type','0000-00-00 00:00:00',NULL),(188,'MONEY','თანხა','Money','Сумма','Money','0000-00-00 00:00:00',NULL),(189,'CHANGE','შეცვლა','Change','Изменить','Change','0000-00-00 00:00:00',NULL),(190,'SELLER','გამყიდველი','Seller','Продавец','Seller','0000-00-00 00:00:00',NULL),(191,'SPLIT_PRICE','დაშლის ფასი','Split Price','Поштучнай цена','Split Price','0000-00-00 00:00:00',NULL),(192,'DATABASE_NULL','ბაზის განულება','Delete Database','Обнулить базу','Delete Database','0000-00-00 00:00:00',NULL),(193,'DATABASE_NULLED_OK','ბაზა განულდა წარმატებით','Database was nulled succesfuly','База обнулена успешно','Database was nulled succesfuly','0000-00-00 00:00:00',NULL),(194,'REMOVED','გაუქმებული','Removed','Отенено','Removed','0000-00-00 00:00:00',NULL),(195,'CHANGE_LANGUAGE','ენის შეცვლა','Change Language','Изменить язык','Change Language','0000-00-00 00:00:00',NULL),(196,'CHANGE_THEME','თემის შეცვლა','Change Theme','Изменить тему','Change Theme','0000-00-00 00:00:00',NULL),(197,'BONUS','ბონუსი','Bonus','Бонус','Bonus','0000-00-00 00:00:00',NULL),(198,'NEW_CLIENT','ახალი კლიენტი','New Client','Новый клиент','New Client','0000-00-00 00:00:00',NULL),(199,'CLIENT','კლიენტი','Client','Клиент','Client','0000-00-00 00:00:00',NULL),(200,'CHOOSE_CLIENT','კლიენტის არჩევა','Choose Client','Выбрать клиента','Choose Client','0000-00-00 00:00:00',NULL),(201,'AMOUNT_IN_STORAGE','რაოდ. საწყობში','Amount in Storage','Кол-во на складе','Amount in Storage','0000-00-00 00:00:00',NULL),(202,'NEED_RECEIPT','საჭიროებს რეცეპტს','Need Receipt','Нужен рецепт','Need Receipt','0000-00-00 00:00:00',NULL),(203,'EARNING','მოგება','Earning','Прибыль','Earning','0000-00-00 00:00:00',NULL),(204,'SELL_PRICE','გასაყიდი ფასი','Sell Price','Продажная цена','Sell Price','0000-00-00 00:00:00',NULL),(205,'VAT','დ.ღ.გ.','VAT','Н.Д.С.','VAT','0000-00-00 00:00:00',NULL),(206,'INCLUDE_VAT','დ.ღ.გ.–ის ჩათვლით','Include VAT','С учетом НДС','Include VAT','0000-00-00 00:00:00',NULL),(207,'WITHOUT_VAT','დ.ღ.გ.–ის გარეშე','Without VAT','Без НДС','Without VAT','0000-00-00 00:00:00',NULL),(208,'CONFIRM_PRODUCT_SELL_PRICE_CHANGE','პროდუქტის გასაყიდი ფასი შეიცვალა. გინდათ შეიცვალოს პროდუქტის გასაყიდი ფასი?','Product sell price was changed. Do you want to change product sell price with new price?','Продажная ценв товара изменилась.','Product sell price was changed. Do you want to change product sell price with new price?','0000-00-00 00:00:00',NULL),(209,'CALCULATE_PERCENT','პროცენტის გამოთვლა','Calculate Percent','Подсчет процентов','Calculate Percent','0000-00-00 00:00:00',NULL),(210,'FROM','დან','From','От','From','0000-00-00 00:00:00',NULL),(211,'BEFORE','მდე','Before','До','Before','0000-00-00 00:00:00',NULL),(212,'PRODUCT','პროდუქტი','Product','Продукт','Product','0000-00-00 00:00:00',NULL),(213,'SOLD_PRODUCTS','გაყიდული პროდუქტები','Sold Products','Продано Продукты','Sold Products','0000-00-00 00:00:00',NULL),(214,'THIS_PRODUCT_IS_NOT_IN_STORAGE','მითითებული პროდუქტი არ არის საწყობში','This product is not in storage','Этот продукт не в памяти','This product is not in storage','0000-00-00 00:00:00',NULL),(215,'CHOOSE_PRODUCT','აირჩიეთ პროდუქტი','Choose Product','Выберите продукт','Choose Product','0000-00-00 00:00:00',NULL),(216,'SELECTED_NUMBER_IS_GREATER_THAN_THE_NUMBER_OF_STOCK','მითითებული რაოდენობა მეტია საწყობში არსებულ რაოდენობაზე','Set the number is greater than the number of stock','Установите число больше, чем число акций','Set the number is greater than the number of stock','0000-00-00 00:00:00',NULL),(217,'SPECIFY_THE_NUMBER_OF_SALE','მიუთითეთ გასაყიდი რაოდენობა','Specify the number of sale','Укажите количество продажи','Specify the number of sale','0000-00-00 00:00:00',NULL),(218,'ADD_SUBCATEGORY','ქვეკატეგორიის დამატება','Add Subcategory','Добавить подкатегорию','Add Subcategory','0000-00-00 00:00:00',NULL),(219,'PARENT','მშობელი','Parent','Родитель','Parent','0000-00-00 00:00:00',NULL),(220,'DISCARD_INGREDIENT','ინგრედიენტის ჩამოწერა','Discard Ingredient','ინგრედიენტის ჩამოწერა','Discard Ingredient','0000-00-00 00:00:00',NULL),(221,'DISCARDED_INGREDIENTS','ჩამოწერილი ინრედიენტები','Discarded Ingredients','ჩამოწერილი ინრედიენტები','Discarded Ingredients','0000-00-00 00:00:00',NULL),(222,'DISCARD_AMOUNT','ჩამოსაწერი რაოდენობა','Discard Amount','ჩამოსაწერი რაოდენობა','Discard Amount','0000-00-00 00:00:00',NULL),(223,'DATABASE','ბაზა','Database','ბაზა','Database','0000-00-00 00:00:00',NULL),(224,'DOWNLOAD_DATABASE_FOR_WEB','ბაზის ჩამოტვირთვა საიტისთვის','Download Database for WEB','ბაზის ჩამოტვირთვა საიტისთვის','Download Database for WEB','0000-00-00 00:00:00',NULL),(225,'REMAINS','ნაშთები','Remains','ნაშთები','Remains','0000-00-00 00:00:00',NULL),(226,'PRE_CHECK','წინასწარი ჩეკი','Pre Check','წინასწარი ჩეკი','Pre Check','0000-00-00 00:00:00',NULL),(227,'SALE_PERCENT','ფასდაკ. %','Sale %','ფასდაკლება %','Sale %','0000-00-00 00:00:00',NULL),(228,'CODE','კოდი','Code','Code','Code','0000-00-00 00:00:00',NULL),(229,'INCORRECT_VALUES','არასწორი მონაცემები!','Incorrect values','Incorrect values','Incorrect values','0000-00-00 00:00:00',NULL),(230,'DAY_TRADED_VOLUME','ღის ნავაჭრი','Day trading volume','Day trading volume','Day trading volume','0000-00-00 00:00:00',NULL),(231,'MAIN_PAGE','მთავარი გვერდი','Main Page','Main Page','Main Page','0000-00-00 00:00:00',NULL),(232,'COMMENT','კომენტარი','Comment','Comment','Comment','0000-00-00 00:00:00',NULL),(233,'ORDERS','შეკვეთები','Orders','Orders','Orders','0000-00-00 00:00:00',NULL),(234,'SELLING_PRICE_HAS_NOT_SET','გასაყიდი ფასი არ აქვს მითითებული','Selling price has not set','Selling price has not set','Selling price has not set','0000-00-00 00:00:00',NULL),(235,'NEW_SECTION','ახალი სექცია','New Saction','New Saction','New Saction','0000-00-00 00:00:00',NULL),(236,'PORTION','პორცია','Portion','Portion','Portion','0000-00-00 00:00:00',NULL),(237,'ADD_TYPE','ტიპის დამატება','Add Type','Add Type','Add Type','0000-00-00 00:00:00',NULL),(238,'AMOUNT_SHORT','რაოდ.','Am.','Am.','Am.','0000-00-00 00:00:00',NULL),(239,'FOR_SERVICE','მომსახურების','For Service','For Service','For Service','0000-00-00 00:00:00',NULL),(240,'GEL','ლ','GEL','GEL','GEL','0000-00-00 00:00:00',NULL),(241,'SALE','ფასდაკლება','Sale','Sale','Sale','0000-00-00 00:00:00',NULL),(242,'PLACE_OF_MAKING','გატანის ადგილი','Place Of Making','Place Of Making','Place Of Making','0000-00-00 00:00:00',NULL),(243,'SUM','ჯამი','Sum','Sum','Sum','0000-00-00 00:00:00',NULL),(244,'LANGUAGES','ენები','Languages','Languages','Languages','0000-00-00 00:00:00',NULL),(245,'NEW_PHRASE','ახალი ფრაზა','New Phrase','New Phrase','New Phrase','0000-00-00 00:00:00',NULL),(246,'TURKISH','თურქულად','Turkish','Turkish','Turkish','0000-00-00 00:00:00',NULL),(247,'ENGLISH','ინგლისურად','English','English','English','0000-00-00 00:00:00',NULL),(248,'RUSSIAN','რუსულად','Russian','Russian','Russian','0000-00-00 00:00:00',NULL),(249,'GEORGIAN','ქართულად','Georgian','Georgian','Georgian','0000-00-00 00:00:00',NULL),(250,'WAYBILLS','ზედნადებები','Waybills','Waybills','Waybills','0000-00-00 00:00:00',NULL),(251,'WAYBILL_NO','ზედნადებების №','Waybill No','Waybill No','Waybill No','0000-00-00 00:00:00',NULL),(252,'SELLER_NAME','გამყიდველი','Seller Name','Seller Name','Seller Name','0000-00-00 00:00:00',NULL),(253,'ACTIVATE_DATE','აქტივაციის თარიღი','Activate Date','Activate Date','Activate Date','0000-00-00 00:00:00',NULL),(254,'CREATE_DATE','შექმნის თარიღი','Create Date','Create Date','Create Date','0000-00-00 00:00:00',NULL),(255,'CLOSE_DATE','დასრულების თარიღი','Close Date','Close Date','Close Date','0000-00-00 00:00:00',NULL),(256,'DRIVER_NAME','მძღოლი','Driver Name','Driver Name','Driver Name','0000-00-00 00:00:00',NULL),(257,'CAR','ავტო','Car','Car','Car','0000-00-00 00:00:00',NULL),(258,'TRANS_PRICE','ტრანს. თანხა','Trans. Price','Trans. Price','Trans. Price','0000-00-00 00:00:00',NULL),(259,'DELIVERY_DATE','მიწოდების თარიღი','Delivery Date','Delivery Date','Delivery Date','0000-00-00 00:00:00',NULL),(260,'VIEW','ნახვა','View','View','View','0000-00-00 00:00:00',NULL),(261,'CONFIRM_RECEIVE','მიღების დადასტურება','Confirm Receive','Confirm Receive','Confirm Receive','0000-00-00 00:00:00',NULL),(262,'BAR_CODE','შტრიხკოდი','Barcode','Barcode','Barcode','0000-00-00 00:00:00',NULL),(263,'ADD_BARCODE','შტრიხკოდის დამატება','Add Barcode','Add Barcode','Add Barcode','0000-00-00 00:00:00',NULL),(264,'RECEIVE','მიღება','Receive','Receive','Receive','0000-00-00 00:00:00',NULL),(265,'CAR_NO','მანქანის №','Car No','Car No','Car No','0000-00-00 00:00:00',NULL),(266,'GRANT_RIGHTS','უფლებების მინიჭება','Grant Rights','Grant Rights','Grant Rights','0000-00-00 00:00:00',NULL),(267,'SUPPLIERS','მომწოდებლები','Suppliers','Suppliers','Suppliers','0000-00-00 00:00:00',NULL),(268,'TRADED','ნავაჭრი','Traded','Traded','Traded','0000-00-00 00:00:00',NULL),(269,'RECEIPT','რეცეპტი','Receipt','Receipt','Receipt','0000-00-00 00:00:00',NULL),(270,'STORAGE','საწყობი','Storage','Storage','Storage','0000-00-00 00:00:00',NULL),(271,'DISCARDS','ჩამოწერები','Discards','Discards','Discards','0000-00-00 00:00:00',NULL),(272,'PAYED_MONEY','გადახდილი თანხა','Payed Money','Payed Money','Payed Money','0000-00-00 00:00:00',NULL),(273,'DEADLINE','ვადა','Deadline','Deadline','Deadline','0000-00-00 00:00:00',NULL),(275,'UPLOAD_TO_RS','აიტვირთოს rs.ge-ზე','Upload to RS','Upload to RS','Upload to RS','0000-00-00 00:00:00',NULL),(276,'CARD','ბარათი','Card','Card','Card','0000-00-00 00:00:00',NULL),(277,'DEBT','ვალი','Debt','Debt','Debt','0000-00-00 00:00:00',NULL),(278,'TIN','საიდენტიფიკაციო ნომერი','TIN','TIN','TIN','0000-00-00 00:00:00',NULL),(279,'DRIVERS','მძღოლები','Drivers','Drivers','Drivers','0000-00-00 00:00:00',NULL),(280,'DRIVER','მძღოლი','Driver','Driver','Driver','0000-00-00 00:00:00',NULL),(281,'CHOOSE_DRIVER','აირჩიეთ მძღოლი','Select Driver','Select Driver','Select Driver','0000-00-00 00:00:00',NULL),(282,'NEW_DRIVER','ახალი მძღოლი','New Driver','New Driver','New Driver','0000-00-00 00:00:00',NULL),(283,'CARS','მანქანები','Cars','Cars','Cars','0000-00-00 00:00:00',NULL),(284,'STORAGES','საწყობები','Storages','Storages','Storages','0000-00-00 00:00:00',NULL),(285,'ADD_STORAGE','საწყობის დამატება','Add Storage','Add Storage','Add Storage','0000-00-00 00:00:00',NULL),(286,'MOVE_PRODUCTS','პროდუქტების გადატანა','Move Products','Move Products','Move Products','0000-00-00 00:00:00',NULL),(287,'TRUCK','????????','Truck','Truck','Truck','0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `langs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `langs_copy`
--

DROP TABLE IF EXISTS `langs_copy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `langs_copy` (
  `LANG_ID` int(11) NOT NULL AUTO_INCREMENT,
  `KEY` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `TEXT_KA` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TEXT_EN` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TEXT_RU` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TEXT_TR` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`LANG_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=288 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `langs_copy`
--

LOCK TABLES `langs_copy` WRITE;
/*!40000 ALTER TABLE `langs_copy` DISABLE KEYS */;
INSERT INTO `langs_copy` VALUES (1,'ACTION','მოქმედება','Action','Действие','Action'),(2,'ACTIVE','აქტიური','Active','Активный','Active'),(3,'ADD','დამატება','Add','Добавить','Add'),(4,'ADDRESS','მისამართი','Address','Адрес','Address'),(5,'CANCEL','გაუქმება','Cancel','Отменить','Cancel'),(6,'CHANGE_PASSWORD','პაროლის შეცვლა','Change Password','Сменить пароль','Change Password'),(7,'CLEAR','გასუფთავება','Clear','Очистить','Clear'),(8,'CLOSE','დახურვა','Close','Закрыть','Close'),(9,'DATE','თარიღი','Date','Дата','Date'),(10,'DELETE','წაშლა','Delete','Удалить','Delete'),(11,'EDIT','რედაქტირება','Edit','Редактировать','Edit'),(12,'ERROR','შეცდომა','Error','Ошибка','Error'),(13,'EXIT','გასვლა','Logout','Выход','Logout'),(14,'EXPORT','ექსპორტი','Export','Экспорт','Export'),(15,'FINALIZE','დასრულება','Finalize','Завершить','Finalize'),(16,'FIRST_NAME','სახელი','Firstname','Имя','Firstname'),(17,'FROM_WHO','ვისგან','From','Из','From'),(18,'HISTORY','ისტორია','History','История','History'),(19,'IDENTITY_CODE','საიდენტიფიკაციო კოდი','Identity code','Идентификационный код','Identity code'),(20,'INACTIVE','არააქტიური','არააქტიური','Неактивный','არააქტიური'),(21,'INFO','ინფო','Info','Инфо','Info'),(22,'IP_ADDRESS','IP მისამართი','IP Address','IP адрес','IP Address'),(23,'JULY','ივლისი','July','Июль','July'),(24,'JUNE','ივნისი','June','Июнь','June'),(25,'LANGUAGE','ენა','Language','Язык','Language'),(26,'LAST_NAME','გვარი','Lastname','Фамилия','Lastname'),(27,'LOGIN','შესვლა','Login','Войти','Login'),(28,'MAIN_INFO','ძირითადი ინფორმაცია','Basic Information','Основная информация','Basic Information'),(29,'MARCH','მარტი','March','Март','March'),(30,'MAY','მაისი','May','Май','May'),(31,'NEW_PASSWORD','ახალი პაროლი','New password','Новый пароль','New password'),(32,'NO_RECORDS_FOUND','ვერ მოიძებნა ჩანაწერები','No records found','Записей не найдено','No records found'),(33,'NOTE','შენიშვნა','Note','Note','Note'),(34,'NOVEMBER','ნოემბერი','November','Ноябрь','November'),(35,'OCTOBER','ოქტომბერი','October','Октябрь','October'),(36,'OLD_PASSWORD','ძველი პაროლი','Old password','Старый пароль','Old password'),(37,'OPTIONS','პარამეტრები','Settings','Опции','Settings'),(38,'ORGANISATION','ორგანიზაცია','Organisation','Организация','Organisation'),(39,'OVERDUE','ვადაგასული','Overdue','Истекший','Overdue'),(40,'OWN','საკუთარი','Own','Собственный','Own'),(41,'PAGE','გვერდი','Page','Страница','Page'),(42,'PAGES','გვერდები','Pages','Страницы','Pages'),(43,'PASSWORD','პაროლი','Password','Пароль','Password'),(44,'PERSONAL','პირადი','Personal','Личное','Personal'),(45,'PLEASE_WAIT','გთხოვთ, დაელოდოთ','Please Wait...','Пожалуйста, подождите','Please Wait...'),(46,'PRINT','ბეჭდვა','Print','Печать','Print'),(47,'PRINT_PREVIEW','საბეჭდად გადახედვა','Print Preview','Предварительный просмотр','Print Preview'),(48,'REFRESH','განახლება','Refresh','Обновить','Refresh'),(49,'REPEAT_NEW_PASSWORD','გაიმეორეთ ახალი პაროლი','Repeat new password','Повторите новый пароль','Repeat new password'),(50,'RESULT','შედეგი','Result','Результат','Result'),(51,'SAVE','შენახვა','Save','Сохранить','Save'),(52,'SEARCH','ძებნა','Search','Поиск','Search'),(53,'SELECT','არჩევა','Select','Выбирать','Select'),(54,'SELECT_A_FILE','აირჩიეთ ფაილი','Select a file','Выберите файл','Select a file'),(55,'SEND','გადაგზავნა','Send','Переслать','Send'),(56,'SEPTEMBER','სექტემბერი','September','Сентябрь','September'),(57,'STATES','სტატუსები','Statuses','Статусы','Statuses'),(58,'STATE','სტატუსი','State','Статус','State'),(59,'TIME','დრო','Time','Время','Time'),(60,'TITLE','სათაური','Title','название','Title'),(61,'TEXT','ტექსტი','Text','Текст','Text'),(62,'TO_ARCHIVE','დაარქივება','Archive','Архивировать','Archive'),(63,'TO_WHO','ვის','To','Кому','To'),(64,'TOTAL','სულ','Total','Всего','Total'),(65,'TYPE','ტიპი','Type','Вид','Type'),(66,'USERNAME','მომხმარებელი','User','Пользователь','User'),(67,'VERSION','ვერსია','Version','Версия','Version'),(68,'VIEW_ALL','ყველას ნახვა','View All','View All','View All'),(69,'PARCEL_NO','ამანათი','Parcel #','посылка','Parcel #'),(70,'MARK_AS_REGISTERED','დადასტურება','დადასტურება','подтвердить','დადასტურება'),(71,'FILENAMES','ფაილები','filenames','файлы','filenames'),(72,'ADD_INGREDIENT','ინგრედიენტის დამატება','Add ingredient','добавить ингридиент','Add ingredient'),(73,'ADMINISTRATOR','ადმინისტრატორი','Administrator','администратор','Administrator'),(74,'AMOUNT','რაოდენობა','Amount','количество','Amount'),(75,'ASK_DELETE','გსურთ წაშლა?','Do you want to delete?','Удалить?','Do you want to delete?'),(76,'CASHIER','მოლარე','Cashier','кассир','Cashier'),(77,'CATEGORIES','კატეგორიები','Categories','категории','Categories'),(78,'CATEGORY','კატეგორია','Category','категория','Category'),(79,'CHOOSE_SECTION','მიუთითეთ სექცია','Choose section','укажите секцию','Choose section'),(80,'DEFAULT_COLOR','თავდაპირველი ფერი','Default color','начальный цвет','Default color'),(81,'DESCRIPTION','აღწერა','Description','описание','Description'),(82,'FOODS','კერძები','Foods','блюда','Foods'),(83,'IMAGE','სურათი','Image','изображение','Image'),(84,'INGREDIENTS','ინგრედიენტები','Ingredients','ингридиенты','Ingredients'),(85,'MIN_AMOUNT','კრიტ. რაოდ.','კრიტ. რაოდ.','критическое количество','კრიტ. რაოდ.'),(86,'NAME1','დასახელება','Name','Наименование','Name'),(87,'NAME2','სახელი','Name','Имя','Name'),(88,'NAVIGATION','ნავიგაცია','Navigation','навигация','Navigation'),(89,'NEW_SELL','ახალი გაყიდვა','New sell','новая продажа','New sell'),(90,'NOTICE','შეტყობინება','Notice','сообщение','Notice'),(91,'PLACE','ადგილი','Place','место','Place'),(92,'PRICE','ფასი','Price','цена','Price'),(93,'RECIEVINGS','მიღებები','Recievings','закупки','Recievings'),(94,'REPORTS','რეპორტები','Reports','отчеты','Reports'),(95,'SELLS','გაყიდვები','Sells','продажи','Sells'),(96,'TABLES','მაგიდები','Tables','столы','Tables'),(97,'SECTIONS','სექციები','Sections','секции','Sections'),(98,'PLACES','ადგილები','Places','места',NULL),(99,'NEW_CATEGORY','ახალი კატეგორია','New category','новая категория','New category'),(100,'ADD_CATEGORY','კატეგორიის დამატება','Add category','добавить категорию','Add category'),(101,'EDIT_CATEGORY','კატეგორიის რედაქტირება','Edit category','редактирование категории','Edit category'),(102,'ONE_PRICE','ერთ. ფასი','One price','цена единицы','One price'),(103,'WHOLEPRICE','ჯამი','Whole price','сумма','Whole price'),(104,'INVOICE_NO','ინვოისის №','Invoice №','№ Инвойса','Invoice №'),(105,'PROVIDER','მომწოდებელი','Provider','поставщик','Provider'),(106,'MANUFACTURER','მწარმოებელი','Manufacturer','изготовитель','Manufacturer'),(107,'YES','დიახ','Yes','Да','Yes'),(108,'NO','არა','No','Нет','No'),(109,'NEW_INGREDIENT','ახალი ინგრედიენტი','New ingredient','новый ингридиент','New ingredient'),(110,'RECIEVE_INGREDIENT','ინგრედიენტის მიღება','Recieve ingredient','прием ингридиента','Recieve ingredient'),(111,'CHOOSE_TABLE','მიუთითეთ მაგიდა','Choose table','укажите стол','Choose table'),(112,'CASH','ნაღდი','Cash','наличные','Cash'),(113,'TRANSFER','გადარიცხვა','Transfer','безналичные','Transfer'),(114,'NEW_PLACE','ახალი ადგილი','New place','новое место','New place'),(115,'ADD_PLACE','ადგილის დამატება','Add place','добавить место','Add place'),(116,'ADD_INGREDIENT_ON_PRODUCT','პროდუქტზე ინგრედიენტის დამატება','Add ingredient on product','добавить ингридиент в продукт','Add ingredient on product'),(117,'INGREDIENT','ინგრედიენტი','Ingredient','ингридиент','Ingredient'),(118,'NEW_PRODUCT','ახალი პროდუქტი','new Product','Новый товар','new Product'),(119,'ADD_PRODUCT','კერძის დამატება','Add product','добавить блюдо','Add product'),(120,'ADD_SECTION','სექციის დამატება','Add section','добавить секцию','Add section'),(121,'SECTION','სექცია','Section','секциа','Section'),(122,'FILL_ALL_FIELDS','გთხოვთ მიუთითოთ ყველა აუცილებელი ველი','Fill all required fields','заполните все обязательные поля','Fill all required fields'),(123,'ADD_TABLE','მაგიდის დამატება','Add table','добавить стол','Add table'),(124,'NEW_TABLE','ახალი მაგიდა','New table','новый стол','New table'),(125,'TABLE','მაგიდა','Table','стол','Table'),(126,'CANCEL_ORDER','შეკვეთის გაუქმება','Cancel order','отмена заказа','Cancel order'),(127,'USERS','მომხმარებლები','Users','пользователи','Users'),(128,'ADD_USER','მომხმარებლის დამატება','User added','добавить пользователя','User added'),(129,'RANK','რანკი','Rank','ранг','Rank'),(130,'PERSONAL_NO','პირადი ნომერი','Personal No','личный номер','Personal No'),(131,'PHONE','ტელეფონი','Phone','телефон','Phone'),(132,'EMAIL','ელ. ფოსტა','eMail','эл.почта','eMail'),(133,'PRINTER','პრინტერი','Printer','Принтер','Printer'),(134,'WAITER','ოფიციანტი','Waiter','официант','Waiter'),(135,'SELLITEM_REPORTS','კერძების რეპორტები','კერძების რეპორტები','Отчеты по блюдам','კერძების რეპორტები'),(136,'ORDER_NUMBER','შეკვ. №','Order №','Заказ. №','Order №'),(137,'SEARCH_ORDER','შეკვეთის ძებნა','Search Order','Поиск заказа','Search Order'),(138,'SELF_PRICE','თვითღირებულება','Self Price','Себестоимость','Self Price'),(139,'CALL','გამოძახება','CALL','Вызов','CALL'),(140,'MESSAGE','შეტყობინება','Message','Сообщение','Message'),(141,'CONFIRM','დასტური','Confirm','Подтвердить','Confirm'),(142,'MENUS','მენიუები','Menus','Меню','Menus'),(143,'ADD_MENU','მენიუს დამატება','Add Menu','Добавить меню','Add Menu'),(144,'PERCENT','პროცენტი','Percent','Процент','Percent'),(145,'TAKE_MONEY','მოწ. თანხა','Money','Поданная сумма','Money'),(146,'RETURN_CHANGE','ხურდა','Change','Сдача','Change'),(147,'MESSAGES','შეტყობინებები','Messages','Сообщения','Messages'),(148,'INGREDIENTS_REPORTS','ინგრედიენტების რეპორტები','Ingredients Reports','Отчеты по ингридиентам','Ingredients Reports'),(149,'OPERATIONS','ოპერაციები','Operations','Операции','Operations'),(150,'RECIEVING_HISTORY','მიღებების ისტორია','Recieving History','История закупок','Recieving History'),(151,'SELL_HISTORY','გაყიდვების ისტორია','Sell History','История продаж','Sell History'),(152,'GUEST_COUNT','სტუმრების რაოდენობა','Guest Count','Кол-во гостей','Guest Count'),(153,'VIEW_ORDER','შეკვეთის ნახვა','View Order','Смотреть заказ','View Order'),(154,'ADD_IMAGE','სურათის დამატება','Add Image','Добавить картинку','Add Image'),(155,'SEND_MSG','მიწერე მიმტანს','Send Message','Послать сообщение','Send Message'),(156,'ADDITIONAL_INFO','დამატებითი ინფო','Additional Info','Доп. Инфо','Additional Info'),(157,'ADD_ADDITIONAL_INFO','დამატებითი ინფოს დამატება','Add Additional Info','Добавить доп. инфо','Add Additional Info'),(158,'ADD_NEW_ADDETIONAL_INFO_TYPE','ახალი დამატებითი ინფოს დამატება','Add new additional info type','Добавить новое доп. инфо','Add new additional info type'),(159,'CHOOSE_FILE','აირჩიეთ ფაილი','Choose file','Выбрать файл','Choose file'),(160,'RESOURCES','რესურსები','Resources','Ресурсы','Resources'),(161,'FILE_SIZE_ERROR_30','ფაილის ზომა არ უნდა აღემატებოდეს 30 kb-ს','Max allowed file size is 30 kb','Размер файла не должен превышать 30 Кб','Max allowed file size is 30 kb'),(162,'FILE_SIZE_ERROR_500','ფაილის ზომა არ უნდა აღემატებოდეს 500 kb-ს','Max allowed file size is 500 kb','Размер файла не должен превышать 500 Кб','Max allowed file size is 500 kb'),(163,'CHOOSE_FILE_ERROR','გთხოვთ მიუთითოთ ფაილი','Choose file','Укажите файл','Choose file'),(164,'RECIEVE_PRODUCT','პროდუქტის მიღება','Recieve product','Принять товар','Recieve product'),(165,'SPLIT_AMOUNT','დაშლილი რაოდენობა','Split Amount','Штучное кол-во','Split Amount'),(166,'BARCODE','შტრიხკოდი','Barcode','Штрихкод','Barcode'),(167,'IN_STORAGE','საწყობში','In Storage','В складе','In Storage'),(168,'RECIEVE_BY_INVOICE','ინვოისით მიღება','Recieve by Invoice','Прием по инвойсу','Recieve by Invoice'),(169,'WEIGHT','წონა','Weight','Вес','Weight'),(170,'WARRANTY','გარანტია','Warranty','Гарантия','Warranty'),(171,'CONDITION','მდგომარეობა','Condition','Состояние','Condition'),(172,'NEW','ახალი','New','Новый','New'),(173,'USED','მეორადი','Used','Вторичный','Used'),(174,'PRICE_WHOLESALE','ფასი საბითუმო','Price Wholesale','Оптовая цена','Price Wholesale'),(175,'PRICE_VIP','ფასი VIP','Price VIP','Цена VIP','Price VIP'),(176,'SPLIT','დაშლა','Split','Поштучно','Split'),(177,'FILL_SPLIT_AMOUNT','მიუთითეთ დაშლის რაოდენობა','Fill Split Amount','Укажите поштучное кол-во','Fill Split Amount'),(178,'INVOICE_NUMBER','ინვოისის ნომერი','Invoice Number','Номер инвойса','Invoice Number'),(179,'FILL_ALL_IMPORTANT_FIELDS','შეავსეთ ყველა სავალდებულო ველი','Fill all important fields','Заполните все обязательные поля','Fill all important fields'),(180,'SEARCH_PRODUCT','პროდუქტის ძებნა','Search Product','Поиск товара','Search Product'),(181,'BRANCH','ფილიალი','Branch','Филиал','Branch'),(182,'WHOLE','მთლიანი','Whole','Целый','Whole'),(183,'PRODUCTS','პროდუქტები','Products','Товары','Products'),(184,'CLIENTS','კლიენტები','Clients','Клиенты','Clients'),(185,'PAYMENTS','გადახდები','Payments','Платежи','Payments'),(186,'ADD_PAYMENT','გადახდა','New Payment','Оплатить','New Payment'),(187,'PAYMENT_TYPE','გადახდის ტიპი','Payment Type','Тип оплаты','Payment Type'),(188,'MONEY','თანხა','Money','Сумма','Money'),(189,'CHANGE','შეცვლა','Change','Изменить','Change'),(190,'SELLER','გამყიდველი','Seller','Продавец','Seller'),(191,'SPLIT_PRICE','დაშლის ფასი','Split Price','Поштучнай цена','Split Price'),(192,'DATABASE_NULL','ბაზის განულება','Delete Database','Обнулить базу','Delete Database'),(193,'DATABASE_NULLED_OK','ბაზა განულდა წარმატებით','Database was nulled succesfuly','База обнулена успешно','Database was nulled succesfuly'),(194,'REMOVED','გაუქმებული','Removed','Отенено','Removed'),(195,'CHANGE_LANGUAGE','ენის შეცვლა','Change Language','Изменить язык','Change Language'),(196,'CHANGE_THEME','თემის შეცვლა','Change Theme','Изменить тему','Change Theme'),(197,'BONUS','ბონუსი','Bonus','Бонус','Bonus'),(198,'NEW_CLIENT','ახალი კლიენტი','New Client','Новый клиент','New Client'),(199,'CLIENT','კლიენტი','Client','Клиент','Client'),(200,'CHOOSE_CLIENT','კლიენტის არჩევა','Choose Client','Выбрать клиента','Choose Client'),(201,'AMOUNT_IN_STORAGE','რაოდ. საწყობში','Amount in Storage','Кол-во на складе','Amount in Storage'),(202,'NEED_RECEIPT','საჭიროებს რეცეპტს','Need Receipt','Нужен рецепт','Need Receipt'),(203,'EARNING','მოგება','Earning','Прибыль','Earning'),(204,'SELL_PRICE','გასაყიდი ფასი','Sell Price','Продажная цена','Sell Price'),(205,'VAT','დ.ღ.გ.','VAT','Н.Д.С.','VAT'),(206,'INCLUDE_VAT','დ.ღ.გ.–ის ჩათვლით','Include VAT','С учетом НДС','Include VAT'),(207,'WITHOUT_VAT','დ.ღ.გ.–ის გარეშე','Without VAT','Без НДС','Without VAT'),(208,'CONFIRM_PRODUCT_SELL_PRICE_CHANGE','პროდუქტის გასაყიდი ფასი შეიცვალა. გინდათ შეიცვალოს პროდუქტის გასაყიდი ფასი?','Product sell price was changed. Do you want to change product sell price with new price?','Продажная ценв товара изменилась.','Product sell price was changed. Do you want to change product sell price with new price?'),(209,'CALCULATE_PERCENT','პროცენტის გამოთვლა','Calculate Percent','Подсчет процентов','Calculate Percent'),(210,'FROM','დან','From','От','From'),(211,'BEFORE','მდე','Before','До','Before'),(212,'PRODUCT','პროდუქტი','Product','Продукт','Product'),(213,'SOLD_PRODUCTS','გაყიდული პროდუქტები','Sold Products','Продано Продукты','Sold Products'),(214,'THIS_PRODUCT_IS_NOT_IN_STORAGE','მითითებული პროდუქტი არ არის საწყობში','This product is not in storage','Этот продукт не в памяти','This product is not in storage'),(215,'CHOOSE_PRODUCT','აირჩიეთ პროდუქტი','Choose Product','Выберите продукт','Choose Product'),(216,'SELECTED_NUMBER_IS_GREATER_THAN_THE_NUMBER_OF_STOCK','მითითებული რაოდენობა მეტია საწყობში არსებულ რაოდენობაზე','Set the number is greater than the number of stock','Установите число больше, чем число акций','Set the number is greater than the number of stock'),(217,'SPECIFY_THE_NUMBER_OF_SALE','მიუთითეთ გასაყიდი რაოდენობა','Specify the number of sale','Укажите количество продажи','Specify the number of sale'),(218,'ADD_SUBCATEGORY','ქვეკატეგორიის დამატება','Add Subcategory','Добавить подкатегорию','Add Subcategory'),(219,'PARENT','მშობელი','Parent','Родитель','Parent'),(220,'DISCARD_INGREDIENT','ინგრედიენტის ჩამოწერა','Discard Ingredient','ინგრედიენტის ჩამოწერა','Discard Ingredient'),(221,'DISCARDED_INGREDIENTS','ჩამოწერილი ინრედიენტები','Discarded Ingredients','ჩამოწერილი ინრედიენტები','Discarded Ingredients'),(222,'DISCARD_AMOUNT','ჩამოსაწერი რაოდენობა','Discard Amount','ჩამოსაწერი რაოდენობა','Discard Amount'),(223,'DATABASE','ბაზა','Database','ბაზა','Database'),(224,'DOWNLOAD_DATABASE_FOR_WEB','ბაზის ჩამოტვირთვა საიტისთვის','Download Database for WEB','ბაზის ჩამოტვირთვა საიტისთვის','Download Database for WEB'),(225,'REMAINS','ნაშთები','Remains','ნაშთები','Remains'),(226,'PRE_CHECK','წინასწარი ჩეკი','Pre Check','წინასწარი ჩეკი','Pre Check'),(227,'SALE_PERCENT','ფასდაკ. %','Sale %','ფასდაკლება %','Sale %'),(228,'CODE','კოდი','Code','Code','Code'),(229,'INCORRECT_VALUES','არასწორი მონაცემები!','Incorrect values','Incorrect values','Incorrect values'),(230,'DAY_TRADED_VOLUME','ღის ნავაჭრი','Day trading volume','Day trading volume','Day trading volume'),(231,'MAIN_PAGE','მთავარი გვერდი','Main Page','Main Page','Main Page'),(232,'COMMENT','კომენტარი','Comment','Comment','Comment'),(233,'ORDERS','შეკვეთები','Orders','Orders','Orders'),(234,'SELLING_PRICE_HAS_NOT_SET','გასაყიდი ფასი არ აქვს მითითებული','Selling price has not set','Selling price has not set','Selling price has not set'),(235,'NEW_SECTION','ახალი სექცია','New Saction','New Saction','New Saction'),(236,'PORTION','პორცია','Portion','Portion','Portion'),(237,'ADD_TYPE','ტიპის დამატება','Add Type','Add Type','Add Type'),(238,'AMOUNT_SHORT','რაოდ.','Am.','Am.','Am.'),(239,'FOR_SERVICE','მომსახურების','For Service','For Service','For Service'),(240,'GEL','ლ','GEL','GEL','GEL'),(241,'SALE','ფასდაკლება','Sale','Sale','Sale'),(242,'PLACE_OF_MAKING','გატანის ადგილი','Place Of Making','Place Of Making','Place Of Making'),(243,'SUM','ჯამი','Sum','Sum','Sum'),(244,'LANGUAGES','ენები','Languages','Languages','Languages'),(245,'NEW_PHRASE','ახალი ფრაზა','New Phrase','New Phrase','New Phrase'),(246,'TURKISH','თურქულად','Turkish','Turkish','Turkish'),(247,'ENGLISH','ინგლისურად','English','English','English'),(248,'RUSSIAN','რუსულად','Russian','Russian','Russian'),(249,'GEORGIAN','ქართულად','Georgian','Georgian','Georgian'),(250,'WAYBILLS','ზედნადებები','Waybills','Waybills','Waybills'),(251,'WAYBILL_NO','ზედნადებების №','Waybill No','Waybill No','Waybill No'),(252,'SELLER_NAME','გამყიდველი','Seller Name','Seller Name','Seller Name'),(253,'ACTIVATE_DATE','აქტივაციის თარიღი','Activate Date','Activate Date','Activate Date'),(254,'CREATE_DATE','შექმნის თარიღი','Create Date','Create Date','Create Date'),(255,'CLOSE_DATE','დასრულების თარიღი','Close Date','Close Date','Close Date'),(256,'DRIVER_NAME','მძღოლი','Driver Name','Driver Name','Driver Name'),(257,'CAR','ავტო','Car','Car','Car'),(258,'TRANS_PRICE','ტრანს. თანხა','Trans. Price','Trans. Price','Trans. Price'),(259,'DELIVERY_DATE','მიწოდების თარიღი','Delivery Date','Delivery Date','Delivery Date'),(260,'VIEW','ნახვა','View','View','View'),(261,'CONFIRM_RECEIVE','მიღების დადასტურება','Confirm Receive','Confirm Receive','Confirm Receive'),(262,'BAR_CODE','შტრიხკოდი','Barcode','Barcode','Barcode'),(263,'ADD_BARCODE','შტრიხკოდის დამატება','Add Barcode','Add Barcode','Add Barcode'),(264,'RECEIVE','მიღება','Receive','Receive','Receive'),(265,'CAR_NO','მანქანის №','Car No','Car No','Car No'),(266,'GRANT_RIGHTS','უფლებების მინიჭება','Grant Rights','Grant Rights','Grant Rights'),(267,'SUPPLIERS','მომწოდებლები','Suppliers','Suppliers','Suppliers'),(268,'TRADED','ნავაჭრი','Traded','Traded','Traded'),(269,'RECEIPT','რეცეპტი','Receipt','Receipt','Receipt'),(270,'STORAGE','საწყობი','Storage','Storage','Storage'),(271,'DISCARDS','ჩამოწერები','Discards','Discards','Discards'),(272,'PAYED_MONEY','გადახდილი თანხა','Payed Money','Payed Money','Payed Money'),(273,'DEADLINE','ვადა','Deadline','Deadline','Deadline'),(275,'UPLOAD_TO_RS','აიტვირთოს rs.ge-ზე','Upload to RS','Upload to RS','Upload to RS'),(276,'CARD','ბარათი','Card','Card','Card'),(277,'DEBT','ვალი','Debt','Debt','Debt'),(278,'TIN','საიდენტიფიკაციო ნომერი','TIN','TIN','TIN'),(279,'DRIVERS','მძღოლები','Drivers','Drivers','Drivers'),(280,'DRIVER','მძღოლი','Driver','Driver','Driver'),(281,'CHOOSE_DRIVER','აირჩიეთ მძღოლი','Select Driver','Select Driver','Select Driver'),(282,'NEW_DRIVER','ახალი მძღოლი','New Driver','New Driver','New Driver'),(283,'CARS','მანქანები','Cars','Cars','Cars'),(284,'STORAGES','საწყობები','Storages','Storages','Storages'),(285,'ADD_STORAGE','საწყობის დამატება','Add Storage','Add Storage','Add Storage'),(286,'MOVE_PRODUCTS','პროდუქტების გადატანა','Move Products','Move Products','Move Products'),(287,'TRUCK','????????','Truck','Truck','Truck');
/*!40000 ALTER TABLE `langs_copy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2017_05_02_140156_entrust_setup_tables',2),(4,'2017_05_05_144232_entrust_setup_tables',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1),(2,1),(1,2),(1,5),(2,5),(10,5),(7,6),(8,6),(9,6),(3,7),(5,7),(3,10),(7,14),(7,16),(6,17),(7,18),(6,19);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `display_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'super create-post','Create Posts','create new blog posts','2017-05-05 10:46:44','2017-05-05 10:46:44'),(2,'super edit-user','Edit Users','edit existing users','2017-05-05 10:46:44','2017-05-05 10:46:44'),(3,'Flight 10',NULL,NULL,'2017-05-08 04:19:29','2017-05-08 04:19:29'),(5,'test1234','test1234','test1234','2017-05-08 04:21:51','2017-05-08 04:21:51'),(6,'ggggggggggg','ggggggggggg','ggggggggggg','2017-05-08 04:23:38','2017-05-08 04:23:38'),(7,'hhhhhhhhhttttttt','hhhhhhhhhttttttt','hhhhhhhhhttttttt','2017-05-08 04:24:29','2017-05-08 04:24:29'),(8,'nnmmmm','nnmmmm','nnmmmm','2017-05-08 04:25:18','2017-05-08 04:25:18'),(9,'ffffff','fffffffffffffffffffffffff','ffffffffffffffffffffffffffff','2017-05-08 04:27:15','2017-05-08 04:27:15'),(10,'new test','new test','new test','2017-05-08 04:34:18','2017-05-08 04:34:18');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_info`
--

DROP TABLE IF EXISTS `product_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_info` (
  `PRODUCT_INFO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ID` int(11) NOT NULL,
  `PRODUCT_INFO_TYPE_ID` int(11) NOT NULL,
  `PRODUCT_INFO_TYPE_NAME` varchar(255) CHARACTER SET utf8 NOT NULL,
  `VALUE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `STATE_ID` int(11) NOT NULL DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`PRODUCT_INFO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_info`
--

LOCK TABLES `product_info` WRITE;
/*!40000 ALTER TABLE `product_info` DISABLE KEYS */;
INSERT INTO `product_info` VALUES (1,1,20,'ტრანსმისია','ავტომატიკა',2,'2017-05-03 15:21:34','2017-05-03 11:12:38'),(2,1,20,'ტრანსმისია','meqanika',1,'2017-05-11 12:03:04','2017-05-11 08:03:04'),(3,1,21,'ტრანსმისიაssssss','model',2,'2017-05-11 12:06:30','2017-05-11 08:06:17'),(4,1,23,'xxx','asd',1,'2017-05-11 12:13:12','2017-05-11 08:13:12');
/*!40000 ALTER TABLE `product_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_info_types`
--

DROP TABLE IF EXISTS `product_info_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_info_types` (
  `PRODUCT_INFO_TYPE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_INFO_TYPE_NAME` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`PRODUCT_INFO_TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_info_types`
--

LOCK TABLES `product_info_types` WRITE;
/*!40000 ALTER TABLE `product_info_types` DISABLE KEYS */;
INSERT INTO `product_info_types` VALUES (1,'ბრენდი',NULL,NULL),(2,'მოდელი',NULL,NULL),(3,'ეკრანის ზომა',NULL,NULL),(4,'ეკრანის ტიპი',NULL,NULL),(5,'ეკრანის რეზოლუცია',NULL,NULL),(6,'CPU',NULL,NULL),(7,'RAM',NULL,NULL),(8,'HDD',NULL,NULL),(9,'LAN',NULL,NULL),(10,'ODD',NULL,NULL),(11,'Display',NULL,NULL),(12,'Battery',NULL,NULL),(13,'OS',NULL,NULL),(14,'Weight',NULL,NULL),(15,'SupplierCode',NULL,NULL),(16,'PartNumber',NULL,NULL),(17,'ნოუთის ინფო',NULL,NULL),(18,'მონაცემები',NULL,NULL),(20,'ტრანსმისია','2017-05-03 11:06:49','2017-05-03 15:06:49'),(21,'ტრანსმისიაssssss','2017-05-03 11:39:19','2017-05-03 15:39:19'),(22,'ragaca','2017-05-11 08:12:26','2017-05-11 12:12:26'),(23,'xxx','2017-05-11 08:12:57','2017-05-11 12:12:57');
/*!40000 ALTER TABLE `product_info_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `PRODUCT_ID` int(11) NOT NULL AUTO_INCREMENT,
  `SUPPLIER_CODE` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `BARCODE` varchar(60) CHARACTER SET utf8 DEFAULT '',
  `NAME` text CHARACTER SET utf8 NOT NULL,
  `DESCRIPTION` text CHARACTER SET utf8,
  `CATEGORY_ID` int(11) NOT NULL,
  `MINAMOUNT` decimal(10,2) DEFAULT NULL,
  `PRICE` decimal(10,2) DEFAULT '0.00',
  `CONDITION_ID` int(11) DEFAULT NULL,
  `STATE_ID` int(2) NOT NULL DEFAULT '1',
  `SELF_PRICE` decimal(15,2) DEFAULT '0.00',
  `LAST_PRICE` decimal(10,2) DEFAULT NULL,
  `UNIT_NAME` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `UNIT_ID` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`PRODUCT_ID`),
  KEY `SUPPLIER_CODE_INDX` (`SUPPLIER_CODE`) USING BTREE,
  KEY `PRODUCT_ID_INX` (`PRODUCT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,NULL,'25','ზეთი \"მზიური\"','asd',80,NULL,2.00,1,1,2.00,2.00,'ცალი',1,'2017-05-11 11:07:38',NULL),(2,NULL,'','ზალატოი','',80,NULL,3.00,1,1,3.00,3.00,'',NULL,NULL,NULL),(3,NULL,'','ქარვა','',80,NULL,1.00,1,1,1.00,1.00,'',NULL,NULL,NULL),(4,NULL,'','ბრავიტა','',80,NULL,3.00,1,2,3.00,3.00,'',NULL,'2017-05-11 10:39:39',NULL),(5,NULL,'','სკუმბრია','',82,NULL,12.00,1,1,12.00,12.00,'',NULL,NULL,NULL),(6,NULL,'','კალმახი','',81,NULL,10.00,1,1,10.00,10.00,'',NULL,NULL,NULL),(7,NULL,'','ლოქო','',81,NULL,3.00,1,1,3.00,3.00,'',NULL,NULL,NULL),(8,NULL,'','ბროილერის ქათამი','',79,NULL,4.00,1,1,4.00,4.00,'',NULL,NULL,NULL),(9,NULL,'','დედალი','',79,NULL,4.00,1,1,4.00,4.00,'',NULL,NULL,NULL),(10,NULL,'','წიწილა','',79,NULL,4.00,1,1,4.00,4.00,'',NULL,NULL,NULL),(12,NULL,'','ვიბრატორიdsdsdsdsdsds',NULL,1,NULL,120.00,1,1,0.00,NULL,'ცალი',1,'2017-05-03 14:46:41','2017-05-03 10:31:10'),(14,NULL,'','ვიბრატორიwsdfssssssssssssssssssssssss',NULL,1,NULL,120.00,1,1,0.00,NULL,'ცალი',1,'2017-05-03 14:46:57','2017-05-03 10:46:57'),(15,NULL,'sadsasa','sad','sadsasad',81,1.00,2.00,1,2,NULL,NULL,'გრამი',3,'2017-05-11 11:17:23','2017-05-11 07:17:07');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (1,1),(2,2),(2,5),(1,6),(2,6),(2,16),(1,17),(2,17);
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `display_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'super owner','Project Owner','User is the owner of a given project','2017-05-05 10:46:44','2017-05-05 10:46:44'),(2,'super admin','User Administrator','User is allowed to manage and edit other users','2017-05-05 10:46:44','2017-05-05 10:46:44'),(3,'didi yle','didi yle','didi yle','2017-05-10 07:28:00','2017-05-10 07:28:00'),(4,'das','dasd','asdasd','2017-05-10 07:31:22','2017-05-10 07:31:22'),(5,'patara yle','patara yle','patara yle','2017-05-10 07:32:09','2017-05-10 07:32:09'),(6,'satesto yle','satesto yle','satesto yle','2017-05-10 07:35:26','2017-05-10 07:35:26'),(7,'Mmmmmmmmmmmm','Mmmmmmmmmmmm','Mmmmmmmmmmmm','2017-05-10 07:36:43','2017-05-10 07:36:43'),(8,'gdf','gdf','gdfgdfg','2017-05-10 07:37:12','2017-05-10 07:37:12'),(9,'bbbb','bbbbbbbbbb','bbbbbbbbbbbbbbbbbb','2017-05-10 07:38:06','2017-05-10 07:38:06'),(10,'prena','prena','prena','2017-05-10 07:38:34','2017-05-10 07:38:34'),(11,'nnnnn','nnnnnnnnn','nnnnnnnnnnnnnnnnnn','2017-05-10 07:39:39','2017-05-10 07:39:39'),(12,'gdfg','dfgdf','gdfgdfg','2017-05-10 07:40:13','2017-05-10 07:40:13'),(13,'ffffffffffffffff','ffffffffff','fffffffffffff','2017-05-10 07:40:25','2017-05-10 07:40:25'),(14,'nnnnnn','nnnnnnnnnnnn','nnnnnnnnnnnnnn','2017-05-10 07:52:32','2017-05-10 07:52:32'),(15,'qqqqqq','qqqqqqqqqqqqqqq','qqqqqqqqqqqqqqq','2017-05-10 07:53:15','2017-05-10 07:53:15'),(16,'selections[0].data','selections[0].data','selections[0].data','2017-05-10 07:53:57','2017-05-10 07:53:57'),(17,'data','data','data','2017-05-10 07:54:17','2017-05-10 07:54:17'),(18,'kima','kima','kima','2017-05-10 07:56:33','2017-05-10 07:56:33'),(19,'JUJU','JUJU','JUJU','2017-05-10 08:21:03','2017-05-10 08:21:03');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `storage`
--

DROP TABLE IF EXISTS `storage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storage` (
  `STORAGE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `BRANCH_ID` int(11) NOT NULL,
  `PRODUCT_ID` int(11) NOT NULL,
  `AMOUNT` decimal(10,4) DEFAULT NULL,
  `PLACE` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `VALID_DATE` date DEFAULT NULL,
  `amounta` decimal(10,4) NOT NULL,
  PRIMARY KEY (`STORAGE_ID`,`PRODUCT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `storage`
--

LOCK TABLES `storage` WRITE;
/*!40000 ALTER TABLE `storage` DISABLE KEYS */;
INSERT INTO `storage` VALUES (1,1,1,100.0000,'rus',NULL,0.0000),(2,2,2,50.0000,'adsa',NULL,0.0000);
/*!40000 ALTER TABLE `storage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state_id` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin','admin@gmail.com','$2y$10$b9Rs1OvVD/BThlNHmSGaauHgeTzWZsvj9IQJe.GH8hKudziWP0IRG','lySZSZtN5LGugVJ549qph3UmYFh22h3KSUKVT6Lr5KR2yDbcdTtV6b6qIoT5','2017-05-04 07:45:54','2017-05-04 07:46:15',1),(2,'Admin1','admin1@gmail.com','$2y$10$b9Rs1OvVD/BThlNHmSGaauHgeTzWZsvj9IQJe.GH8hKudziWP0IRG','','2017-05-04 07:58:39','0000-00-00 00:00:00',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-11 17:53:05
