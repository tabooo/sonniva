/*
Navicat MySQL Data Transfer

Source Server         : localhost_mysql
Source Server Version : 50527
Source Host           : 127.0.0.1:3306
Source Database       : onlinemarket

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2017-05-19 17:24:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for branches
-- ----------------------------
DROP TABLE IF EXISTS `branches`;
CREATE TABLE `branches` (
  `BRANCH_ID` int(11) NOT NULL AUTO_INCREMENT,
  `BRANCH_NAME` varchar(200) CHARACTER SET utf8 NOT NULL,
  `BRANCH_STATE_ID` int(2) NOT NULL DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`BRANCH_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of branches
-- ----------------------------
INSERT INTO `branches` VALUES ('1', 'რუსთავი', '1', null, null);
INSERT INTO `branches` VALUES ('2', 'თბილისი', '1', null, null);
INSERT INTO `branches` VALUES ('3', 'გორი', '1', null, null);
INSERT INTO `branches` VALUES ('4', 'xinkali', '1', '2017-05-03 13:39:36', '2017-05-03 13:39:36');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `CATEGORY_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CATEGORY_NAME` varchar(100) CHARACTER SET utf8 NOT NULL,
  `PARENT_ID` int(11) DEFAULT NULL,
  `CHILDRENS` text,
  `PARENTS` text,
  `cancel_date` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `STATE_ID` int(1) NOT NULL,
  PRIMARY KEY (`CATEGORY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', '– ყველა –', null, '74,79,80,81,82', null, null, '0000-00-00 00:00:00', '2017-05-08 11:17:19', '1');
INSERT INTO `category` VALUES ('74', 'ტელევიზორები', '1', '76', '1', null, '0000-00-00 00:00:00', '2017-05-18 09:40:43', '1');
INSERT INTO `category` VALUES ('76', 'LED ტელევიზორები', '74', null, '74', null, '0000-00-00 00:00:00', '2017-05-17 14:08:45', '1');
INSERT INTO `category` VALUES ('79', 'ციფრული ტექნიკა', '1', '83,85', '1', null, '0000-00-00 00:00:00', '2017-05-17 14:06:31', '1');
INSERT INTO `category` VALUES ('80', 'ტელეფონი/ტაბლეტი', '1', null, '1', null, '0000-00-00 00:00:00', '2017-05-17 14:07:37', '1');
INSERT INTO `category` VALUES ('81', 'კომპიუტერული ტექნიკა', '1', null, '1', null, '0000-00-00 00:00:00', '2017-05-17 14:07:49', '1');
INSERT INTO `category` VALUES ('82', 'საყოფაცხოვრებო ტექნიკა', '1', null, '1', '2017-05-02 15:24:57', '2017-05-02 13:15:13', '2017-05-17 14:08:01', '1');
INSERT INTO `category` VALUES ('83', 'ფოტო კამერა', '79', null, '79', null, '2017-05-08 11:08:44', '2017-05-17 14:09:17', '1');
INSERT INTO `category` VALUES ('84', 'asdadsa', '79', null, '79', '2017-05-08 11:17:19', '2017-05-08 11:11:44', '2017-05-08 11:17:19', '2');
INSERT INTO `category` VALUES ('85', 'ვიდე კამერა', '79', null, '79', null, '2017-05-08 11:15:29', '2017-05-17 14:09:32', '1');
INSERT INTO `category` VALUES ('86', 'წვრილი ტექნიკა', '1', null, '1', null, '2017-05-17 14:08:13', '2017-05-17 14:08:13', '1');
INSERT INTO `category` VALUES ('87', 'ჩასაშენებელი ტექნიკა', '1', null, '1', null, '2017-05-17 14:08:26', '2017-05-17 14:08:26', '1');
INSERT INTO `category` VALUES ('88', '3D ტელევიზორები', '74', null, '74', null, '2017-05-17 14:08:55', '2017-05-17 14:08:55', '1');
INSERT INTO `category` VALUES ('89', 'მობილური ტელეფონი', '80', null, '80', null, '2017-05-17 14:10:02', '2017-05-17 14:10:03', '1');
INSERT INTO `category` VALUES ('90', 'პლანშეტი', '80', null, '80', null, '2017-05-17 14:11:06', '2017-05-17 14:11:06', '1');
INSERT INTO `category` VALUES ('91', 'ჭკვიანი საათი', '80', null, '80', null, '2017-05-17 14:11:17', '2017-05-17 14:11:17', '1');
INSERT INTO `category` VALUES ('92', 'მაცივარი', '82', null, '82', null, '2017-05-17 14:11:32', '2017-05-17 14:11:32', '1');
INSERT INTO `category` VALUES ('93', 'სარეცხი მანქანა', '82', null, '82', null, '2017-05-17 14:11:43', '2017-05-17 14:11:43', '1');
INSERT INTO `category` VALUES ('94', 'საშრობი', '82', null, '82', null, '2017-05-17 14:11:51', '2017-05-17 14:11:52', '1');
INSERT INTO `category` VALUES ('95', 'ჭურჭლის სარეცხი მანქანა', '82', null, '82', null, '2017-05-17 14:12:00', '2017-05-17 14:12:01', '1');
INSERT INTO `category` VALUES ('96', 'ქურა', '82', null, '82', null, '2017-05-17 14:12:15', '2017-05-17 14:12:15', '1');
INSERT INTO `category` VALUES ('97', 'გამწოვი', '82', null, '82', null, '2017-05-17 14:12:21', '2017-05-17 14:12:21', '1');
INSERT INTO `category` VALUES ('98', 'ჩაიდანი', '86', null, '86', null, '2017-05-18 11:37:52', '2017-05-18 11:37:52', '1');

-- ----------------------------
-- Table structure for langs
-- ----------------------------
DROP TABLE IF EXISTS `langs`;
CREATE TABLE `langs` (
  `LANG_ID` int(11) NOT NULL AUTO_INCREMENT,
  `KEY` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `TEXT_KA` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TEXT_EN` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TEXT_RU` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TEXT_TR` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`LANG_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=288 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of langs
-- ----------------------------
INSERT INTO `langs` VALUES ('1', 'ACTION', 'მოქმედება', 'Action', 'Действие', 'Action', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('2', 'ACTIVE', 'აქტიური', 'Active', 'Активный', 'Active', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('3', 'ADD', 'დამატება', 'Add', 'Добавить', 'Add', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('4', 'ADDRESS', 'მისამართი', 'Address', 'Адрес', 'Address', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('5', 'CANCEL', 'გაუქმება', 'Cancel', 'Отменить', 'Cancel', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('6', 'CHANGE_PASSWORD', 'პაროლის შეცვლა', 'Change Password', 'Сменить пароль', 'Change Password', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('7', 'CLEAR', 'გასუფთავება', 'Clear', 'Очистить', 'Clear', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('8', 'CLOSE', 'დახურვა', 'Close', 'Закрыть', 'Close', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('9', 'DATE', 'თარიღი', 'Date', 'Дата', 'Date', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('10', 'DELETE', 'წაშლა', 'Delete', 'Удалить', 'Delete', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('11', 'EDIT', 'რედაქტირება', 'Edit', 'Редактировать', 'Edit', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('12', 'ERROR', 'შეცდომა', 'Error', 'Ошибка', 'Error', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('13', 'EXIT', 'გასვლა', 'Logout', 'Выход', 'Logout', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('14', 'EXPORT', 'ექსპორტი', 'Export', 'Экспорт', 'Export', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('15', 'FINALIZE', 'დასრულება', 'Finalize', 'Завершить', 'Finalize', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('16', 'FIRST_NAME', 'სახელი', 'Firstname', 'Имя', 'Firstname', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('17', 'FROM_WHO', 'ვისგან', 'From', 'Из', 'From', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('18', 'HISTORY', 'ისტორია', 'History', 'История', 'History', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('19', 'IDENTITY_CODE', 'საიდენტიფიკაციო კოდი', 'Identity code', 'Идентификационный код', 'Identity code', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('20', 'INACTIVE', 'არააქტიური', 'არააქტიური', 'Неактивный', 'არააქტიური', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('21', 'INFO', 'ინფო', 'Info', 'Инфо', 'Info', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('22', 'IP_ADDRESS', 'IP მისამართი', 'IP Address', 'IP адрес', 'IP Address', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('23', 'JULY', 'ივლისი', 'July', 'Июль', 'July', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('24', 'JUNE', 'ივნისი', 'June', 'Июнь', 'June', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('25', 'LANGUAGE', 'ენა', 'Language', 'Язык', 'Language', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('26', 'LAST_NAME', 'გვარი', 'Lastname', 'Фамилия', 'Lastname', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('27', 'LOGIN', 'შესვლა', 'Login', 'Войти', 'Login', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('28', 'MAIN_INFO', 'ძირითადი ინფორმაცია', 'Basic Information', 'Основная информация', 'Basic Information', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('29', 'MARCH', 'მარტი', 'March', 'Март', 'March', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('30', 'MAY', 'მაისი', 'May', 'Май', 'May', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('31', 'NEW_PASSWORD', 'ახალი პაროლი', 'New password', 'Новый пароль', 'New password', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('32', 'NO_RECORDS_FOUND', 'ვერ მოიძებნა ჩანაწერები', 'No records found', 'Записей не найдено', 'No records found', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('33', 'NOTE', 'შენიშვნა', 'Note', 'Note', 'Note', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('34', 'NOVEMBER', 'ნოემბერი', 'November', 'Ноябрь', 'November', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('35', 'OCTOBER', 'ოქტომბერი', 'October', 'Октябрь', 'October', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('36', 'OLD_PASSWORD', 'ძველი პაროლი', 'Old password', 'Старый пароль', 'Old password', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('37', 'OPTIONS', 'პარამეტრები', 'Settings', 'Опции', 'Settings', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('38', 'ORGANISATION', 'ორგანიზაცია', 'Organisation', 'Организация', 'Organisation', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('39', 'OVERDUE', 'ვადაგასული', 'Overdue', 'Истекший', 'Overdue', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('40', 'OWN', 'საკუთარი', 'Own', 'Собственный', 'Own', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('41', 'PAGE', 'გვერდი', 'Page', 'Страница', 'Page', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('42', 'PAGES', 'გვერდები', 'Pages', 'Страницы', 'Pages', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('43', 'PASSWORD', 'პაროლი', 'Password', 'Пароль', 'Password', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('44', 'PERSONAL', 'პირადი', 'Personal', 'Личное', 'Personal', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('45', 'PLEASE_WAIT', 'გთხოვთ, დაელოდოთ', 'Please Wait...', 'Пожалуйста, подождите', 'Please Wait...', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('46', 'PRINT', 'ბეჭდვა', 'Print', 'Печать', 'Print', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('47', 'PRINT_PREVIEW', 'საბეჭდად გადახედვა', 'Print Preview', 'Предварительный просмотр', 'Print Preview', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('48', 'REFRESH', 'განახლება', 'Refresh', 'Обновить', 'Refresh', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('49', 'REPEAT_NEW_PASSWORD', 'გაიმეორეთ ახალი პაროლი', 'Repeat new password', 'Повторите новый пароль', 'Repeat new password', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('50', 'RESULT', 'შედეგი', 'Result', 'Результат', 'Result', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('51', 'SAVE', 'შენახვა', 'Save', 'Сохранить', 'Save', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('52', 'SEARCH', 'ძებნა', 'Search', 'Поиск', 'Search', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('53', 'SELECT', 'არჩევა', 'Select', 'Выбирать', 'Select', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('54', 'SELECT_A_FILE', 'აირჩიეთ ფაილი', 'Select a file', 'Выберите файл', 'Select a file', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('55', 'SEND', 'გადაგზავნა', 'Send', 'Переслать', 'Send', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('56', 'SEPTEMBER', 'სექტემბერი', 'September', 'Сентябрь', 'September', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('57', 'STATES', 'სტატუსები', 'Statuses', 'Статусы', 'Statuses', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('58', 'STATE', 'სტატუსი', 'State', 'Статус', 'State', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('59', 'TIME', 'დრო', 'Time', 'Время', 'Time', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('60', 'TITLE', 'სათაური', 'Title', 'название', 'Title', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('61', 'TEXT', 'ტექსტი', 'Text', 'Текст', 'Text', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('62', 'TO_ARCHIVE', 'დაარქივება', 'Archive', 'Архивировать', 'Archive', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('63', 'TO_WHO', 'ვის', 'To', 'Кому', 'To', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('64', 'TOTAL', 'სულ', 'Total', 'Всего', 'Total', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('65', 'TYPE', 'ტიპი', 'Type', 'Вид', 'Type', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('66', 'USERNAME', 'მომხმარებელი', 'User', 'Пользователь', 'User', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('67', 'VERSION', 'ვერსია', 'Version', 'Версия', 'Version', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('68', 'VIEW_ALL', 'ყველას ნახვა', 'View All', 'View All', 'View All', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('69', 'PARCEL_NO', 'ამანათი', 'Parcel #', 'посылка', 'Parcel #', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('70', 'MARK_AS_REGISTERED', 'დადასტურება', 'დადასტურება', 'подтвердить', 'დადასტურება', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('71', 'FILENAMES', 'ფაილები', 'filenames', 'файлы', 'filenames', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('72', 'ADD_INGREDIENT', 'ინგრედიენტის დამატება', 'Add ingredient', 'добавить ингридиент', 'Add ingredient', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('73', 'ADMINISTRATOR', 'ადმინისტრატორი', 'Administrator', 'администратор', 'Administrator', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('74', 'AMOUNT', 'რაოდენობა', 'Amount', 'количество', 'Amount', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('75', 'ASK_DELETE', 'გსურთ წაშლა?', 'Do you want to delete?', 'Удалить?', 'Do you want to delete?', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('76', 'CASHIER', 'მოლარე', 'Cashier', 'кассир', 'Cashier', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('77', 'CATEGORIES', 'კატეგორიები', 'Categories', 'категории', 'Categories', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('78', 'CATEGORY', 'კატეგორია', 'Category', 'категория', 'Category', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('79', 'CHOOSE_SECTION', 'მიუთითეთ სექცია', 'Choose section', 'укажите секцию', 'Choose section', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('80', 'DEFAULT_COLOR', 'თავდაპირველი ფერი', 'Default color', 'начальный цвет', 'Default color', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('81', 'DESCRIPTION', 'აღწერა', 'Description', 'описание', 'Description', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('82', 'FOODS', 'კერძები', 'Foods', 'блюда', 'Foods', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('83', 'IMAGE', 'სურათი', 'Image', 'изображение', 'Image', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('84', 'INGREDIENTS', 'ინგრედიენტები', 'Ingredients', 'ингридиенты', 'Ingredients', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('85', 'MIN_AMOUNT', 'კრიტ. რაოდ.', 'კრიტ. რაოდ.', 'критическое количество', 'კრიტ. რაოდ.', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('86', 'NAME1', 'დასახელება', 'Name', 'Наименование', 'Name', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('87', 'NAME2', 'სახელი', 'Name', 'Имя', 'Name', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('88', 'NAVIGATION', 'ნავიგაცია', 'Navigation', 'навигация', 'Navigation', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('89', 'NEW_SELL', 'ახალი გაყიდვა', 'New sell', 'новая продажа', 'New sell', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('90', 'NOTICE', 'შეტყობინება', 'Notice', 'сообщение', 'Notice', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('91', 'PLACE', 'ადგილი', 'Place', 'место', 'Place', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('92', 'PRICE', 'ფასი', 'Price', 'цена', 'Price', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('93', 'RECIEVINGS', 'მიღებები', 'Recievings', 'закупки', 'Recievings', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('94', 'REPORTS', 'რეპორტები', 'Reports', 'отчеты', 'Reports', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('95', 'SELLS', 'გაყიდვები', 'Sells', 'продажи', 'Sells', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('96', 'TABLES', 'მაგიდები', 'Tables', 'столы', 'Tables', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('97', 'SECTIONS', 'სექციები', 'Sections', 'секции', 'Sections', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('98', 'PLACES', 'ადგილები', 'Places', 'места', null, '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('99', 'NEW_CATEGORY', 'ახალი კატეგორია', 'New category', 'новая категория', 'New category', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('100', 'ADD_CATEGORY', 'კატეგორიის დამატება', 'Add category', 'добавить категорию', 'Add category', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('101', 'EDIT_CATEGORY', 'კატეგორიის რედაქტირება', 'Edit category', 'редактирование категории', 'Edit category', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('102', 'ONE_PRICE', 'ერთ. ფასი', 'One price', 'цена единицы', 'One price', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('103', 'WHOLEPRICE', 'ჯამი', 'Whole price', 'сумма', 'Whole price', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('104', 'INVOICE_NO', 'ინვოისის №', 'Invoice №', '№ Инвойса', 'Invoice №', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('105', 'PROVIDER', 'მომწოდებელი', 'Provider', 'поставщик', 'Provider', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('106', 'MANUFACTURER', 'მწარმოებელი', 'Manufacturer', 'изготовитель', 'Manufacturer', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('107', 'YES', 'დიახ', 'Yes', 'Да', 'Yes', '0000-00-00 00:00:00', '2017-05-02 13:21:05');
INSERT INTO `langs` VALUES ('108', 'NO', 'არა', 'No', 'Нет', 'No', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('109', 'NEW_INGREDIENT', 'ახალი ინგრედიენტი', 'New ingredient', 'новый ингридиент', 'New ingredient', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('110', 'RECIEVE_INGREDIENT', 'ინგრედიენტის მიღება', 'Recieve ingredient', 'прием ингридиента', 'Recieve ingredient', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('111', 'CHOOSE_TABLE', 'მიუთითეთ მაგიდა', 'Choose table', 'укажите стол', 'Choose table', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('112', 'CASH', 'ნაღდი', 'Cash', 'наличные', 'Cash', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('113', 'TRANSFER', 'გადარიცხვა', 'Transfer', 'безналичные', 'Transfer', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('114', 'NEW_PLACE', 'ახალი ადგილი', 'New place', 'новое место', 'New place', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('115', 'ADD_PLACE', 'ადგილის დამატება', 'Add place', 'добавить место', 'Add place', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('116', 'ADD_INGREDIENT_ON_PRODUCT', 'პროდუქტზე ინგრედიენტის დამატება', 'Add ingredient on product', 'добавить ингридиент в продукт', 'Add ingredient on product', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('117', 'INGREDIENT', 'ინგრედიენტი', 'Ingredient', 'ингридиент', 'Ingredient', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('118', 'NEW_PRODUCT', 'ახალი პროდუქტი', 'new Product', 'Новый товар', 'new Product', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('119', 'ADD_PRODUCT', 'კერძის დამატება', 'Add product', 'добавить блюдо', 'Add product', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('120', 'ADD_SECTION', 'სექციის დამატება', 'Add section', 'добавить секцию', 'Add section', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('121', 'SECTION', 'სექცია', 'Section', 'секциа', 'Section', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('122', 'FILL_ALL_FIELDS', 'გთხოვთ მიუთითოთ ყველა აუცილებელი ველი', 'Fill all required fields', 'заполните все обязательные поля', 'Fill all required fields', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('123', 'ADD_TABLE', 'მაგიდის დამატება', 'Add table', 'добавить стол', 'Add table', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('124', 'NEW_TABLE', 'ახალი მაგიდა', 'New table', 'новый стол', 'New table', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('125', 'TABLE', 'მაგიდა', 'Table', 'стол', 'Table', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('126', 'CANCEL_ORDER', 'შეკვეთის გაუქმება', 'Cancel order', 'отмена заказа', 'Cancel order', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('127', 'USERS', 'მომხმარებლები', 'Users', 'пользователи', 'Users', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('128', 'ADD_USER', 'მომხმარებლის დამატება', 'User added', 'добавить пользователя', 'User added', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('129', 'RANK', 'რანკი', 'Rank', 'ранг', 'Rank', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('130', 'PERSONAL_NO', 'პირადი ნომერი', 'Personal No', 'личный номер', 'Personal No', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('131', 'PHONE', 'ტელეფონი', 'Phone', 'телефон', 'Phone', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('132', 'EMAIL', 'ელ. ფოსტა', 'eMail', 'эл.почта', 'eMail', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('133', 'PRINTER', 'პრინტერი', 'Printer', 'Принтер', 'Printer', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('134', 'WAITER', 'ოფიციანტი', 'Waiter', 'официант', 'Waiter', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('135', 'SELLITEM_REPORTS', 'კერძების რეპორტები', 'კერძების რეპორტები', 'Отчеты по блюдам', 'კერძების რეპორტები', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('136', 'ORDER_NUMBER', 'შეკვ. №', 'Order №', 'Заказ. №', 'Order №', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('137', 'SEARCH_ORDER', 'შეკვეთის ძებნა', 'Search Order', 'Поиск заказа', 'Search Order', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('138', 'SELF_PRICE', 'თვითღირებულება', 'Self Price', 'Себестоимость', 'Self Price', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('139', 'CALL', 'გამოძახება', 'CALL', 'Вызов', 'CALL', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('140', 'MESSAGE', 'შეტყობინება', 'Message', 'Сообщение', 'Message', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('141', 'CONFIRM', 'დასტური', 'Confirm', 'Подтвердить', 'Confirm', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('142', 'MENUS', 'მენიუები', 'Menus', 'Меню', 'Menus', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('143', 'ADD_MENU', 'მენიუს დამატება', 'Add Menu', 'Добавить меню', 'Add Menu', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('144', 'PERCENT', 'პროცენტი', 'Percent', 'Процент', 'Percent', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('145', 'TAKE_MONEY', 'მოწ. თანხა', 'Money', 'Поданная сумма', 'Money', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('146', 'RETURN_CHANGE', 'ხურდა', 'Change', 'Сдача', 'Change', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('147', 'MESSAGES', 'შეტყობინებები', 'Messages', 'Сообщения', 'Messages', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('148', 'INGREDIENTS_REPORTS', 'ინგრედიენტების რეპორტები', 'Ingredients Reports', 'Отчеты по ингридиентам', 'Ingredients Reports', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('149', 'OPERATIONS', 'ოპერაციები', 'Operations', 'Операции', 'Operations', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('150', 'RECIEVING_HISTORY', 'მიღებების ისტორია', 'Recieving History', 'История закупок', 'Recieving History', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('151', 'SELL_HISTORY', 'გაყიდვების ისტორია', 'Sell History', 'История продаж', 'Sell History', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('152', 'GUEST_COUNT', 'სტუმრების რაოდენობა', 'Guest Count', 'Кол-во гостей', 'Guest Count', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('153', 'VIEW_ORDER', 'შეკვეთის ნახვა', 'View Order', 'Смотреть заказ', 'View Order', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('154', 'ADD_IMAGE', 'სურათის დამატება', 'Add Image', 'Добавить картинку', 'Add Image', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('155', 'SEND_MSG', 'მიწერე მიმტანს', 'Send Message', 'Послать сообщение', 'Send Message', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('156', 'ADDITIONAL_INFO', 'დამატებითი ინფო', 'Additional Info', 'Доп. Инфо', 'Additional Info', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('157', 'ADD_ADDITIONAL_INFO', 'დამატებითი ინფოს დამატება', 'Add Additional Info', 'Добавить доп. инфо', 'Add Additional Info', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('158', 'ADD_NEW_ADDETIONAL_INFO_TYPE', 'ახალი დამატებითი ინფოს დამატება', 'Add new additional info type', 'Добавить новое доп. инфо', 'Add new additional info type', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('159', 'CHOOSE_FILE', 'აირჩიეთ ფაილი', 'Choose file', 'Выбрать файл', 'Choose file', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('160', 'RESOURCES', 'რესურსები', 'Resources', 'Ресурсы', 'Resources', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('161', 'FILE_SIZE_ERROR_30', 'ფაილის ზომა არ უნდა აღემატებოდეს 30 kb-ს', 'Max allowed file size is 30 kb', 'Размер файла не должен превышать 30 Кб', 'Max allowed file size is 30 kb', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('162', 'FILE_SIZE_ERROR_500', 'ფაილის ზომა არ უნდა აღემატებოდეს 500 kb-ს', 'Max allowed file size is 500 kb', 'Размер файла не должен превышать 500 Кб', 'Max allowed file size is 500 kb', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('163', 'CHOOSE_FILE_ERROR', 'გთხოვთ მიუთითოთ ფაილი', 'Choose file', 'Укажите файл', 'Choose file', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('164', 'RECIEVE_PRODUCT', 'პროდუქტის მიღება', 'Recieve product', 'Принять товар', 'Recieve product', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('165', 'SPLIT_AMOUNT', 'დაშლილი რაოდენობა', 'Split Amount', 'Штучное кол-во', 'Split Amount', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('166', 'BARCODE', 'შტრიხკოდი', 'Barcode', 'Штрихкод', 'Barcode', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('167', 'IN_STORAGE', 'საწყობში', 'In Storage', 'В складе', 'In Storage', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('168', 'RECIEVE_BY_INVOICE', 'ინვოისით მიღება', 'Recieve by Invoice', 'Прием по инвойсу', 'Recieve by Invoice', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('169', 'WEIGHT', 'წონა', 'Weight', 'Вес', 'Weight', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('170', 'WARRANTY', 'გარანტია', 'Warranty', 'Гарантия', 'Warranty', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('171', 'CONDITION', 'მდგომარეობა', 'Condition', 'Состояние', 'Condition', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('172', 'NEW', 'ახალი', 'New', 'Новый', 'New', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('173', 'USED', 'მეორადი', 'Used', 'Вторичный', 'Used', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('174', 'PRICE_WHOLESALE', 'ფასი საბითუმო', 'Price Wholesale', 'Оптовая цена', 'Price Wholesale', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('175', 'PRICE_VIP', 'ფასი VIP', 'Price VIP', 'Цена VIP', 'Price VIP', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('176', 'SPLIT', 'დაშლა', 'Split', 'Поштучно', 'Split', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('177', 'FILL_SPLIT_AMOUNT', 'მიუთითეთ დაშლის რაოდენობა', 'Fill Split Amount', 'Укажите поштучное кол-во', 'Fill Split Amount', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('178', 'INVOICE_NUMBER', 'ინვოისის ნომერი', 'Invoice Number', 'Номер инвойса', 'Invoice Number', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('179', 'FILL_ALL_IMPORTANT_FIELDS', 'შეავსეთ ყველა სავალდებულო ველი', 'Fill all important fields', 'Заполните все обязательные поля', 'Fill all important fields', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('180', 'SEARCH_PRODUCT', 'პროდუქტის ძებნა', 'Search Product', 'Поиск товара', 'Search Product', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('181', 'BRANCH', 'ფილიალი', 'Branch', 'Филиал', 'Branch', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('182', 'WHOLE', 'მთლიანი', 'Whole', 'Целый', 'Whole', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('183', 'PRODUCTS', 'პროდუქტები', 'Products', 'Товары', 'Products', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('184', 'CLIENTS', 'კლიენტები', 'Clients', 'Клиенты', 'Clients', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('185', 'PAYMENTS', 'გადახდები', 'Payments', 'Платежи', 'Payments', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('186', 'ADD_PAYMENT', 'გადახდა', 'New Payment', 'Оплатить', 'New Payment', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('187', 'PAYMENT_TYPE', 'გადახდის ტიპი', 'Payment Type', 'Тип оплаты', 'Payment Type', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('188', 'MONEY', 'თანხა', 'Money', 'Сумма', 'Money', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('189', 'CHANGE', 'შეცვლა', 'Change', 'Изменить', 'Change', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('190', 'SELLER', 'გამყიდველი', 'Seller', 'Продавец', 'Seller', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('191', 'SPLIT_PRICE', 'დაშლის ფასი', 'Split Price', 'Поштучнай цена', 'Split Price', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('192', 'DATABASE_NULL', 'ბაზის განულება', 'Delete Database', 'Обнулить базу', 'Delete Database', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('193', 'DATABASE_NULLED_OK', 'ბაზა განულდა წარმატებით', 'Database was nulled succesfuly', 'База обнулена успешно', 'Database was nulled succesfuly', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('194', 'REMOVED', 'გაუქმებული', 'Removed', 'Отенено', 'Removed', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('195', 'CHANGE_LANGUAGE', 'ენის შეცვლა', 'Change Language', 'Изменить язык', 'Change Language', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('196', 'CHANGE_THEME', 'თემის შეცვლა', 'Change Theme', 'Изменить тему', 'Change Theme', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('197', 'BONUS', 'ბონუსი', 'Bonus', 'Бонус', 'Bonus', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('198', 'NEW_CLIENT', 'ახალი კლიენტი', 'New Client', 'Новый клиент', 'New Client', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('199', 'CLIENT', 'კლიენტი', 'Client', 'Клиент', 'Client', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('200', 'CHOOSE_CLIENT', 'კლიენტის არჩევა', 'Choose Client', 'Выбрать клиента', 'Choose Client', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('201', 'AMOUNT_IN_STORAGE', 'რაოდ. საწყობში', 'Amount in Storage', 'Кол-во на складе', 'Amount in Storage', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('202', 'NEED_RECEIPT', 'საჭიროებს რეცეპტს', 'Need Receipt', 'Нужен рецепт', 'Need Receipt', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('203', 'EARNING', 'მოგება', 'Earning', 'Прибыль', 'Earning', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('204', 'SELL_PRICE', 'გასაყიდი ფასი', 'Sell Price', 'Продажная цена', 'Sell Price', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('205', 'VAT', 'დ.ღ.გ.', 'VAT', 'Н.Д.С.', 'VAT', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('206', 'INCLUDE_VAT', 'დ.ღ.გ.–ის ჩათვლით', 'Include VAT', 'С учетом НДС', 'Include VAT', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('207', 'WITHOUT_VAT', 'დ.ღ.გ.–ის გარეშე', 'Without VAT', 'Без НДС', 'Without VAT', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('208', 'CONFIRM_PRODUCT_SELL_PRICE_CHANGE', 'პროდუქტის გასაყიდი ფასი შეიცვალა. გინდათ შეიცვალოს პროდუქტის გასაყიდი ფასი?', 'Product sell price was changed. Do you want to change product sell price with new price?', 'Продажная ценв товара изменилась.', 'Product sell price was changed. Do you want to change product sell price with new price?', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('209', 'CALCULATE_PERCENT', 'პროცენტის გამოთვლა', 'Calculate Percent', 'Подсчет процентов', 'Calculate Percent', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('210', 'FROM', 'დან', 'From', 'От', 'From', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('211', 'BEFORE', 'მდე', 'Before', 'До', 'Before', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('212', 'PRODUCT', 'პროდუქტი', 'Product', 'Продукт', 'Product', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('213', 'SOLD_PRODUCTS', 'გაყიდული პროდუქტები', 'Sold Products', 'Продано Продукты', 'Sold Products', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('214', 'THIS_PRODUCT_IS_NOT_IN_STORAGE', 'მითითებული პროდუქტი არ არის საწყობში', 'This product is not in storage', 'Этот продукт не в памяти', 'This product is not in storage', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('215', 'CHOOSE_PRODUCT', 'აირჩიეთ პროდუქტი', 'Choose Product', 'Выберите продукт', 'Choose Product', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('216', 'SELECTED_NUMBER_IS_GREATER_THAN_THE_NUMBER_OF_STOCK', 'მითითებული რაოდენობა მეტია საწყობში არსებულ რაოდენობაზე', 'Set the number is greater than the number of stock', 'Установите число больше, чем число акций', 'Set the number is greater than the number of stock', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('217', 'SPECIFY_THE_NUMBER_OF_SALE', 'მიუთითეთ გასაყიდი რაოდენობა', 'Specify the number of sale', 'Укажите количество продажи', 'Specify the number of sale', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('218', 'ADD_SUBCATEGORY', 'ქვეკატეგორიის დამატება', 'Add Subcategory', 'Добавить подкатегорию', 'Add Subcategory', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('219', 'PARENT', 'მშობელი', 'Parent', 'Родитель', 'Parent', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('220', 'DISCARD_INGREDIENT', 'ინგრედიენტის ჩამოწერა', 'Discard Ingredient', 'ინგრედიენტის ჩამოწერა', 'Discard Ingredient', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('221', 'DISCARDED_INGREDIENTS', 'ჩამოწერილი ინრედიენტები', 'Discarded Ingredients', 'ჩამოწერილი ინრედიენტები', 'Discarded Ingredients', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('222', 'DISCARD_AMOUNT', 'ჩამოსაწერი რაოდენობა', 'Discard Amount', 'ჩამოსაწერი რაოდენობა', 'Discard Amount', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('223', 'DATABASE', 'ბაზა', 'Database', 'ბაზა', 'Database', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('224', 'DOWNLOAD_DATABASE_FOR_WEB', 'ბაზის ჩამოტვირთვა საიტისთვის', 'Download Database for WEB', 'ბაზის ჩამოტვირთვა საიტისთვის', 'Download Database for WEB', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('225', 'REMAINS', 'ნაშთები', 'Remains', 'ნაშთები', 'Remains', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('226', 'PRE_CHECK', 'წინასწარი ჩეკი', 'Pre Check', 'წინასწარი ჩეკი', 'Pre Check', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('227', 'SALE_PERCENT', 'ფასდაკ. %', 'Sale %', 'ფასდაკლება %', 'Sale %', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('228', 'CODE', 'კოდი', 'Code', 'Code', 'Code', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('229', 'INCORRECT_VALUES', 'არასწორი მონაცემები!', 'Incorrect values', 'Incorrect values', 'Incorrect values', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('230', 'DAY_TRADED_VOLUME', 'ღის ნავაჭრი', 'Day trading volume', 'Day trading volume', 'Day trading volume', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('231', 'MAIN_PAGE', 'მთავარი გვერდი', 'Main Page', 'Main Page', 'Main Page', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('232', 'COMMENT', 'კომენტარი', 'Comment', 'Comment', 'Comment', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('233', 'ORDERS', 'შეკვეთები', 'Orders', 'Orders', 'Orders', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('234', 'SELLING_PRICE_HAS_NOT_SET', 'გასაყიდი ფასი არ აქვს მითითებული', 'Selling price has not set', 'Selling price has not set', 'Selling price has not set', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('235', 'NEW_SECTION', 'ახალი სექცია', 'New Saction', 'New Saction', 'New Saction', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('236', 'PORTION', 'პორცია', 'Portion', 'Portion', 'Portion', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('237', 'ADD_TYPE', 'ტიპის დამატება', 'Add Type', 'Add Type', 'Add Type', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('238', 'AMOUNT_SHORT', 'რაოდ.', 'Am.', 'Am.', 'Am.', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('239', 'FOR_SERVICE', 'მომსახურების', 'For Service', 'For Service', 'For Service', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('240', 'GEL', 'ლ', 'GEL', 'GEL', 'GEL', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('241', 'SALE', 'ფასდაკლება', 'Sale', 'Sale', 'Sale', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('242', 'PLACE_OF_MAKING', 'გატანის ადგილი', 'Place Of Making', 'Place Of Making', 'Place Of Making', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('243', 'SUM', 'ჯამი', 'Sum', 'Sum', 'Sum', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('244', 'LANGUAGES', 'ენები', 'Languages', 'Languages', 'Languages', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('245', 'NEW_PHRASE', 'ახალი ფრაზა', 'New Phrase', 'New Phrase', 'New Phrase', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('246', 'TURKISH', 'თურქულად', 'Turkish', 'Turkish', 'Turkish', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('247', 'ENGLISH', 'ინგლისურად', 'English', 'English', 'English', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('248', 'RUSSIAN', 'რუსულად', 'Russian', 'Russian', 'Russian', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('249', 'GEORGIAN', 'ქართულად', 'Georgian', 'Georgian', 'Georgian', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('250', 'WAYBILLS', 'ზედნადებები', 'Waybills', 'Waybills', 'Waybills', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('251', 'WAYBILL_NO', 'ზედნადებების №', 'Waybill No', 'Waybill No', 'Waybill No', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('252', 'SELLER_NAME', 'გამყიდველი', 'Seller Name', 'Seller Name', 'Seller Name', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('253', 'ACTIVATE_DATE', 'აქტივაციის თარიღი', 'Activate Date', 'Activate Date', 'Activate Date', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('254', 'CREATE_DATE', 'შექმნის თარიღი', 'Create Date', 'Create Date', 'Create Date', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('255', 'CLOSE_DATE', 'დასრულების თარიღი', 'Close Date', 'Close Date', 'Close Date', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('256', 'DRIVER_NAME', 'მძღოლი', 'Driver Name', 'Driver Name', 'Driver Name', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('257', 'CAR', 'ავტო', 'Car', 'Car', 'Car', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('258', 'TRANS_PRICE', 'ტრანს. თანხა', 'Trans. Price', 'Trans. Price', 'Trans. Price', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('259', 'DELIVERY_DATE', 'მიწოდების თარიღი', 'Delivery Date', 'Delivery Date', 'Delivery Date', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('260', 'VIEW', 'ნახვა', 'View', 'View', 'View', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('261', 'CONFIRM_RECEIVE', 'მიღების დადასტურება', 'Confirm Receive', 'Confirm Receive', 'Confirm Receive', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('262', 'BAR_CODE', 'შტრიხკოდი', 'Barcode', 'Barcode', 'Barcode', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('263', 'ADD_BARCODE', 'შტრიხკოდის დამატება', 'Add Barcode', 'Add Barcode', 'Add Barcode', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('264', 'RECEIVE', 'მიღება', 'Receive', 'Receive', 'Receive', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('265', 'CAR_NO', 'მანქანის №', 'Car No', 'Car No', 'Car No', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('266', 'GRANT_RIGHTS', 'უფლებების მინიჭება', 'Grant Rights', 'Grant Rights', 'Grant Rights', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('267', 'SUPPLIERS', 'მომწოდებლები', 'Suppliers', 'Suppliers', 'Suppliers', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('268', 'TRADED', 'ნავაჭრი', 'Traded', 'Traded', 'Traded', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('269', 'RECEIPT', 'რეცეპტი', 'Receipt', 'Receipt', 'Receipt', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('270', 'STORAGE', 'საწყობი', 'Storage', 'Storage', 'Storage', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('271', 'DISCARDS', 'ჩამოწერები', 'Discards', 'Discards', 'Discards', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('272', 'PAYED_MONEY', 'გადახდილი თანხა', 'Payed Money', 'Payed Money', 'Payed Money', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('273', 'DEADLINE', 'ვადა', 'Deadline', 'Deadline', 'Deadline', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('275', 'UPLOAD_TO_RS', 'აიტვირთოს rs.ge-ზე', 'Upload to RS', 'Upload to RS', 'Upload to RS', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('276', 'CARD', 'ბარათი', 'Card', 'Card', 'Card', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('277', 'DEBT', 'ვალი', 'Debt', 'Debt', 'Debt', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('278', 'TIN', 'საიდენტიფიკაციო ნომერი', 'TIN', 'TIN', 'TIN', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('279', 'DRIVERS', 'მძღოლები', 'Drivers', 'Drivers', 'Drivers', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('280', 'DRIVER', 'მძღოლი', 'Driver', 'Driver', 'Driver', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('281', 'CHOOSE_DRIVER', 'აირჩიეთ მძღოლი', 'Select Driver', 'Select Driver', 'Select Driver', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('282', 'NEW_DRIVER', 'ახალი მძღოლი', 'New Driver', 'New Driver', 'New Driver', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('283', 'CARS', 'მანქანები', 'Cars', 'Cars', 'Cars', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('284', 'STORAGES', 'საწყობები', 'Storages', 'Storages', 'Storages', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('285', 'ADD_STORAGE', 'საწყობის დამატება', 'Add Storage', 'Add Storage', 'Add Storage', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('286', 'MOVE_PRODUCTS', 'პროდუქტების გადატანა', 'Move Products', 'Move Products', 'Move Products', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('287', 'TRUCK', '????????', 'Truck', 'Truck', 'Truck', '0000-00-00 00:00:00', null);

-- ----------------------------
-- Table structure for langs_copy
-- ----------------------------
DROP TABLE IF EXISTS `langs_copy`;
CREATE TABLE `langs_copy` (
  `LANG_ID` int(11) NOT NULL AUTO_INCREMENT,
  `KEY` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `TEXT_KA` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TEXT_EN` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TEXT_RU` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TEXT_TR` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`LANG_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=288 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of langs_copy
-- ----------------------------
INSERT INTO `langs_copy` VALUES ('1', 'ACTION', 'მოქმედება', 'Action', 'Действие', 'Action');
INSERT INTO `langs_copy` VALUES ('2', 'ACTIVE', 'აქტიური', 'Active', 'Активный', 'Active');
INSERT INTO `langs_copy` VALUES ('3', 'ADD', 'დამატება', 'Add', 'Добавить', 'Add');
INSERT INTO `langs_copy` VALUES ('4', 'ADDRESS', 'მისამართი', 'Address', 'Адрес', 'Address');
INSERT INTO `langs_copy` VALUES ('5', 'CANCEL', 'გაუქმება', 'Cancel', 'Отменить', 'Cancel');
INSERT INTO `langs_copy` VALUES ('6', 'CHANGE_PASSWORD', 'პაროლის შეცვლა', 'Change Password', 'Сменить пароль', 'Change Password');
INSERT INTO `langs_copy` VALUES ('7', 'CLEAR', 'გასუფთავება', 'Clear', 'Очистить', 'Clear');
INSERT INTO `langs_copy` VALUES ('8', 'CLOSE', 'დახურვა', 'Close', 'Закрыть', 'Close');
INSERT INTO `langs_copy` VALUES ('9', 'DATE', 'თარიღი', 'Date', 'Дата', 'Date');
INSERT INTO `langs_copy` VALUES ('10', 'DELETE', 'წაშლა', 'Delete', 'Удалить', 'Delete');
INSERT INTO `langs_copy` VALUES ('11', 'EDIT', 'რედაქტირება', 'Edit', 'Редактировать', 'Edit');
INSERT INTO `langs_copy` VALUES ('12', 'ERROR', 'შეცდომა', 'Error', 'Ошибка', 'Error');
INSERT INTO `langs_copy` VALUES ('13', 'EXIT', 'გასვლა', 'Logout', 'Выход', 'Logout');
INSERT INTO `langs_copy` VALUES ('14', 'EXPORT', 'ექსპორტი', 'Export', 'Экспорт', 'Export');
INSERT INTO `langs_copy` VALUES ('15', 'FINALIZE', 'დასრულება', 'Finalize', 'Завершить', 'Finalize');
INSERT INTO `langs_copy` VALUES ('16', 'FIRST_NAME', 'სახელი', 'Firstname', 'Имя', 'Firstname');
INSERT INTO `langs_copy` VALUES ('17', 'FROM_WHO', 'ვისგან', 'From', 'Из', 'From');
INSERT INTO `langs_copy` VALUES ('18', 'HISTORY', 'ისტორია', 'History', 'История', 'History');
INSERT INTO `langs_copy` VALUES ('19', 'IDENTITY_CODE', 'საიდენტიფიკაციო კოდი', 'Identity code', 'Идентификационный код', 'Identity code');
INSERT INTO `langs_copy` VALUES ('20', 'INACTIVE', 'არააქტიური', 'არააქტიური', 'Неактивный', 'არააქტიური');
INSERT INTO `langs_copy` VALUES ('21', 'INFO', 'ინფო', 'Info', 'Инфо', 'Info');
INSERT INTO `langs_copy` VALUES ('22', 'IP_ADDRESS', 'IP მისამართი', 'IP Address', 'IP адрес', 'IP Address');
INSERT INTO `langs_copy` VALUES ('23', 'JULY', 'ივლისი', 'July', 'Июль', 'July');
INSERT INTO `langs_copy` VALUES ('24', 'JUNE', 'ივნისი', 'June', 'Июнь', 'June');
INSERT INTO `langs_copy` VALUES ('25', 'LANGUAGE', 'ენა', 'Language', 'Язык', 'Language');
INSERT INTO `langs_copy` VALUES ('26', 'LAST_NAME', 'გვარი', 'Lastname', 'Фамилия', 'Lastname');
INSERT INTO `langs_copy` VALUES ('27', 'LOGIN', 'შესვლა', 'Login', 'Войти', 'Login');
INSERT INTO `langs_copy` VALUES ('28', 'MAIN_INFO', 'ძირითადი ინფორმაცია', 'Basic Information', 'Основная информация', 'Basic Information');
INSERT INTO `langs_copy` VALUES ('29', 'MARCH', 'მარტი', 'March', 'Март', 'March');
INSERT INTO `langs_copy` VALUES ('30', 'MAY', 'მაისი', 'May', 'Май', 'May');
INSERT INTO `langs_copy` VALUES ('31', 'NEW_PASSWORD', 'ახალი პაროლი', 'New password', 'Новый пароль', 'New password');
INSERT INTO `langs_copy` VALUES ('32', 'NO_RECORDS_FOUND', 'ვერ მოიძებნა ჩანაწერები', 'No records found', 'Записей не найдено', 'No records found');
INSERT INTO `langs_copy` VALUES ('33', 'NOTE', 'შენიშვნა', 'Note', 'Note', 'Note');
INSERT INTO `langs_copy` VALUES ('34', 'NOVEMBER', 'ნოემბერი', 'November', 'Ноябрь', 'November');
INSERT INTO `langs_copy` VALUES ('35', 'OCTOBER', 'ოქტომბერი', 'October', 'Октябрь', 'October');
INSERT INTO `langs_copy` VALUES ('36', 'OLD_PASSWORD', 'ძველი პაროლი', 'Old password', 'Старый пароль', 'Old password');
INSERT INTO `langs_copy` VALUES ('37', 'OPTIONS', 'პარამეტრები', 'Settings', 'Опции', 'Settings');
INSERT INTO `langs_copy` VALUES ('38', 'ORGANISATION', 'ორგანიზაცია', 'Organisation', 'Организация', 'Organisation');
INSERT INTO `langs_copy` VALUES ('39', 'OVERDUE', 'ვადაგასული', 'Overdue', 'Истекший', 'Overdue');
INSERT INTO `langs_copy` VALUES ('40', 'OWN', 'საკუთარი', 'Own', 'Собственный', 'Own');
INSERT INTO `langs_copy` VALUES ('41', 'PAGE', 'გვერდი', 'Page', 'Страница', 'Page');
INSERT INTO `langs_copy` VALUES ('42', 'PAGES', 'გვერდები', 'Pages', 'Страницы', 'Pages');
INSERT INTO `langs_copy` VALUES ('43', 'PASSWORD', 'პაროლი', 'Password', 'Пароль', 'Password');
INSERT INTO `langs_copy` VALUES ('44', 'PERSONAL', 'პირადი', 'Personal', 'Личное', 'Personal');
INSERT INTO `langs_copy` VALUES ('45', 'PLEASE_WAIT', 'გთხოვთ, დაელოდოთ', 'Please Wait...', 'Пожалуйста, подождите', 'Please Wait...');
INSERT INTO `langs_copy` VALUES ('46', 'PRINT', 'ბეჭდვა', 'Print', 'Печать', 'Print');
INSERT INTO `langs_copy` VALUES ('47', 'PRINT_PREVIEW', 'საბეჭდად გადახედვა', 'Print Preview', 'Предварительный просмотр', 'Print Preview');
INSERT INTO `langs_copy` VALUES ('48', 'REFRESH', 'განახლება', 'Refresh', 'Обновить', 'Refresh');
INSERT INTO `langs_copy` VALUES ('49', 'REPEAT_NEW_PASSWORD', 'გაიმეორეთ ახალი პაროლი', 'Repeat new password', 'Повторите новый пароль', 'Repeat new password');
INSERT INTO `langs_copy` VALUES ('50', 'RESULT', 'შედეგი', 'Result', 'Результат', 'Result');
INSERT INTO `langs_copy` VALUES ('51', 'SAVE', 'შენახვა', 'Save', 'Сохранить', 'Save');
INSERT INTO `langs_copy` VALUES ('52', 'SEARCH', 'ძებნა', 'Search', 'Поиск', 'Search');
INSERT INTO `langs_copy` VALUES ('53', 'SELECT', 'არჩევა', 'Select', 'Выбирать', 'Select');
INSERT INTO `langs_copy` VALUES ('54', 'SELECT_A_FILE', 'აირჩიეთ ფაილი', 'Select a file', 'Выберите файл', 'Select a file');
INSERT INTO `langs_copy` VALUES ('55', 'SEND', 'გადაგზავნა', 'Send', 'Переслать', 'Send');
INSERT INTO `langs_copy` VALUES ('56', 'SEPTEMBER', 'სექტემბერი', 'September', 'Сентябрь', 'September');
INSERT INTO `langs_copy` VALUES ('57', 'STATES', 'სტატუსები', 'Statuses', 'Статусы', 'Statuses');
INSERT INTO `langs_copy` VALUES ('58', 'STATE', 'სტატუსი', 'State', 'Статус', 'State');
INSERT INTO `langs_copy` VALUES ('59', 'TIME', 'დრო', 'Time', 'Время', 'Time');
INSERT INTO `langs_copy` VALUES ('60', 'TITLE', 'სათაური', 'Title', 'название', 'Title');
INSERT INTO `langs_copy` VALUES ('61', 'TEXT', 'ტექსტი', 'Text', 'Текст', 'Text');
INSERT INTO `langs_copy` VALUES ('62', 'TO_ARCHIVE', 'დაარქივება', 'Archive', 'Архивировать', 'Archive');
INSERT INTO `langs_copy` VALUES ('63', 'TO_WHO', 'ვის', 'To', 'Кому', 'To');
INSERT INTO `langs_copy` VALUES ('64', 'TOTAL', 'სულ', 'Total', 'Всего', 'Total');
INSERT INTO `langs_copy` VALUES ('65', 'TYPE', 'ტიპი', 'Type', 'Вид', 'Type');
INSERT INTO `langs_copy` VALUES ('66', 'USERNAME', 'მომხმარებელი', 'User', 'Пользователь', 'User');
INSERT INTO `langs_copy` VALUES ('67', 'VERSION', 'ვერსია', 'Version', 'Версия', 'Version');
INSERT INTO `langs_copy` VALUES ('68', 'VIEW_ALL', 'ყველას ნახვა', 'View All', 'View All', 'View All');
INSERT INTO `langs_copy` VALUES ('69', 'PARCEL_NO', 'ამანათი', 'Parcel #', 'посылка', 'Parcel #');
INSERT INTO `langs_copy` VALUES ('70', 'MARK_AS_REGISTERED', 'დადასტურება', 'დადასტურება', 'подтвердить', 'დადასტურება');
INSERT INTO `langs_copy` VALUES ('71', 'FILENAMES', 'ფაილები', 'filenames', 'файлы', 'filenames');
INSERT INTO `langs_copy` VALUES ('72', 'ADD_INGREDIENT', 'ინგრედიენტის დამატება', 'Add ingredient', 'добавить ингридиент', 'Add ingredient');
INSERT INTO `langs_copy` VALUES ('73', 'ADMINISTRATOR', 'ადმინისტრატორი', 'Administrator', 'администратор', 'Administrator');
INSERT INTO `langs_copy` VALUES ('74', 'AMOUNT', 'რაოდენობა', 'Amount', 'количество', 'Amount');
INSERT INTO `langs_copy` VALUES ('75', 'ASK_DELETE', 'გსურთ წაშლა?', 'Do you want to delete?', 'Удалить?', 'Do you want to delete?');
INSERT INTO `langs_copy` VALUES ('76', 'CASHIER', 'მოლარე', 'Cashier', 'кассир', 'Cashier');
INSERT INTO `langs_copy` VALUES ('77', 'CATEGORIES', 'კატეგორიები', 'Categories', 'категории', 'Categories');
INSERT INTO `langs_copy` VALUES ('78', 'CATEGORY', 'კატეგორია', 'Category', 'категория', 'Category');
INSERT INTO `langs_copy` VALUES ('79', 'CHOOSE_SECTION', 'მიუთითეთ სექცია', 'Choose section', 'укажите секцию', 'Choose section');
INSERT INTO `langs_copy` VALUES ('80', 'DEFAULT_COLOR', 'თავდაპირველი ფერი', 'Default color', 'начальный цвет', 'Default color');
INSERT INTO `langs_copy` VALUES ('81', 'DESCRIPTION', 'აღწერა', 'Description', 'описание', 'Description');
INSERT INTO `langs_copy` VALUES ('82', 'FOODS', 'კერძები', 'Foods', 'блюда', 'Foods');
INSERT INTO `langs_copy` VALUES ('83', 'IMAGE', 'სურათი', 'Image', 'изображение', 'Image');
INSERT INTO `langs_copy` VALUES ('84', 'INGREDIENTS', 'ინგრედიენტები', 'Ingredients', 'ингридиенты', 'Ingredients');
INSERT INTO `langs_copy` VALUES ('85', 'MIN_AMOUNT', 'კრიტ. რაოდ.', 'კრიტ. რაოდ.', 'критическое количество', 'კრიტ. რაოდ.');
INSERT INTO `langs_copy` VALUES ('86', 'NAME1', 'დასახელება', 'Name', 'Наименование', 'Name');
INSERT INTO `langs_copy` VALUES ('87', 'NAME2', 'სახელი', 'Name', 'Имя', 'Name');
INSERT INTO `langs_copy` VALUES ('88', 'NAVIGATION', 'ნავიგაცია', 'Navigation', 'навигация', 'Navigation');
INSERT INTO `langs_copy` VALUES ('89', 'NEW_SELL', 'ახალი გაყიდვა', 'New sell', 'новая продажа', 'New sell');
INSERT INTO `langs_copy` VALUES ('90', 'NOTICE', 'შეტყობინება', 'Notice', 'сообщение', 'Notice');
INSERT INTO `langs_copy` VALUES ('91', 'PLACE', 'ადგილი', 'Place', 'место', 'Place');
INSERT INTO `langs_copy` VALUES ('92', 'PRICE', 'ფასი', 'Price', 'цена', 'Price');
INSERT INTO `langs_copy` VALUES ('93', 'RECIEVINGS', 'მიღებები', 'Recievings', 'закупки', 'Recievings');
INSERT INTO `langs_copy` VALUES ('94', 'REPORTS', 'რეპორტები', 'Reports', 'отчеты', 'Reports');
INSERT INTO `langs_copy` VALUES ('95', 'SELLS', 'გაყიდვები', 'Sells', 'продажи', 'Sells');
INSERT INTO `langs_copy` VALUES ('96', 'TABLES', 'მაგიდები', 'Tables', 'столы', 'Tables');
INSERT INTO `langs_copy` VALUES ('97', 'SECTIONS', 'სექციები', 'Sections', 'секции', 'Sections');
INSERT INTO `langs_copy` VALUES ('98', 'PLACES', 'ადგილები', 'Places', 'места', null);
INSERT INTO `langs_copy` VALUES ('99', 'NEW_CATEGORY', 'ახალი კატეგორია', 'New category', 'новая категория', 'New category');
INSERT INTO `langs_copy` VALUES ('100', 'ADD_CATEGORY', 'კატეგორიის დამატება', 'Add category', 'добавить категорию', 'Add category');
INSERT INTO `langs_copy` VALUES ('101', 'EDIT_CATEGORY', 'კატეგორიის რედაქტირება', 'Edit category', 'редактирование категории', 'Edit category');
INSERT INTO `langs_copy` VALUES ('102', 'ONE_PRICE', 'ერთ. ფასი', 'One price', 'цена единицы', 'One price');
INSERT INTO `langs_copy` VALUES ('103', 'WHOLEPRICE', 'ჯამი', 'Whole price', 'сумма', 'Whole price');
INSERT INTO `langs_copy` VALUES ('104', 'INVOICE_NO', 'ინვოისის №', 'Invoice №', '№ Инвойса', 'Invoice №');
INSERT INTO `langs_copy` VALUES ('105', 'PROVIDER', 'მომწოდებელი', 'Provider', 'поставщик', 'Provider');
INSERT INTO `langs_copy` VALUES ('106', 'MANUFACTURER', 'მწარმოებელი', 'Manufacturer', 'изготовитель', 'Manufacturer');
INSERT INTO `langs_copy` VALUES ('107', 'YES', 'დიახ', 'Yes', 'Да', 'Yes');
INSERT INTO `langs_copy` VALUES ('108', 'NO', 'არა', 'No', 'Нет', 'No');
INSERT INTO `langs_copy` VALUES ('109', 'NEW_INGREDIENT', 'ახალი ინგრედიენტი', 'New ingredient', 'новый ингридиент', 'New ingredient');
INSERT INTO `langs_copy` VALUES ('110', 'RECIEVE_INGREDIENT', 'ინგრედიენტის მიღება', 'Recieve ingredient', 'прием ингридиента', 'Recieve ingredient');
INSERT INTO `langs_copy` VALUES ('111', 'CHOOSE_TABLE', 'მიუთითეთ მაგიდა', 'Choose table', 'укажите стол', 'Choose table');
INSERT INTO `langs_copy` VALUES ('112', 'CASH', 'ნაღდი', 'Cash', 'наличные', 'Cash');
INSERT INTO `langs_copy` VALUES ('113', 'TRANSFER', 'გადარიცხვა', 'Transfer', 'безналичные', 'Transfer');
INSERT INTO `langs_copy` VALUES ('114', 'NEW_PLACE', 'ახალი ადგილი', 'New place', 'новое место', 'New place');
INSERT INTO `langs_copy` VALUES ('115', 'ADD_PLACE', 'ადგილის დამატება', 'Add place', 'добавить место', 'Add place');
INSERT INTO `langs_copy` VALUES ('116', 'ADD_INGREDIENT_ON_PRODUCT', 'პროდუქტზე ინგრედიენტის დამატება', 'Add ingredient on product', 'добавить ингридиент в продукт', 'Add ingredient on product');
INSERT INTO `langs_copy` VALUES ('117', 'INGREDIENT', 'ინგრედიენტი', 'Ingredient', 'ингридиент', 'Ingredient');
INSERT INTO `langs_copy` VALUES ('118', 'NEW_PRODUCT', 'ახალი პროდუქტი', 'new Product', 'Новый товар', 'new Product');
INSERT INTO `langs_copy` VALUES ('119', 'ADD_PRODUCT', 'კერძის დამატება', 'Add product', 'добавить блюдо', 'Add product');
INSERT INTO `langs_copy` VALUES ('120', 'ADD_SECTION', 'სექციის დამატება', 'Add section', 'добавить секцию', 'Add section');
INSERT INTO `langs_copy` VALUES ('121', 'SECTION', 'სექცია', 'Section', 'секциа', 'Section');
INSERT INTO `langs_copy` VALUES ('122', 'FILL_ALL_FIELDS', 'გთხოვთ მიუთითოთ ყველა აუცილებელი ველი', 'Fill all required fields', 'заполните все обязательные поля', 'Fill all required fields');
INSERT INTO `langs_copy` VALUES ('123', 'ADD_TABLE', 'მაგიდის დამატება', 'Add table', 'добавить стол', 'Add table');
INSERT INTO `langs_copy` VALUES ('124', 'NEW_TABLE', 'ახალი მაგიდა', 'New table', 'новый стол', 'New table');
INSERT INTO `langs_copy` VALUES ('125', 'TABLE', 'მაგიდა', 'Table', 'стол', 'Table');
INSERT INTO `langs_copy` VALUES ('126', 'CANCEL_ORDER', 'შეკვეთის გაუქმება', 'Cancel order', 'отмена заказа', 'Cancel order');
INSERT INTO `langs_copy` VALUES ('127', 'USERS', 'მომხმარებლები', 'Users', 'пользователи', 'Users');
INSERT INTO `langs_copy` VALUES ('128', 'ADD_USER', 'მომხმარებლის დამატება', 'User added', 'добавить пользователя', 'User added');
INSERT INTO `langs_copy` VALUES ('129', 'RANK', 'რანკი', 'Rank', 'ранг', 'Rank');
INSERT INTO `langs_copy` VALUES ('130', 'PERSONAL_NO', 'პირადი ნომერი', 'Personal No', 'личный номер', 'Personal No');
INSERT INTO `langs_copy` VALUES ('131', 'PHONE', 'ტელეფონი', 'Phone', 'телефон', 'Phone');
INSERT INTO `langs_copy` VALUES ('132', 'EMAIL', 'ელ. ფოსტა', 'eMail', 'эл.почта', 'eMail');
INSERT INTO `langs_copy` VALUES ('133', 'PRINTER', 'პრინტერი', 'Printer', 'Принтер', 'Printer');
INSERT INTO `langs_copy` VALUES ('134', 'WAITER', 'ოფიციანტი', 'Waiter', 'официант', 'Waiter');
INSERT INTO `langs_copy` VALUES ('135', 'SELLITEM_REPORTS', 'კერძების რეპორტები', 'კერძების რეპორტები', 'Отчеты по блюдам', 'კერძების რეპორტები');
INSERT INTO `langs_copy` VALUES ('136', 'ORDER_NUMBER', 'შეკვ. №', 'Order №', 'Заказ. №', 'Order №');
INSERT INTO `langs_copy` VALUES ('137', 'SEARCH_ORDER', 'შეკვეთის ძებნა', 'Search Order', 'Поиск заказа', 'Search Order');
INSERT INTO `langs_copy` VALUES ('138', 'SELF_PRICE', 'თვითღირებულება', 'Self Price', 'Себестоимость', 'Self Price');
INSERT INTO `langs_copy` VALUES ('139', 'CALL', 'გამოძახება', 'CALL', 'Вызов', 'CALL');
INSERT INTO `langs_copy` VALUES ('140', 'MESSAGE', 'შეტყობინება', 'Message', 'Сообщение', 'Message');
INSERT INTO `langs_copy` VALUES ('141', 'CONFIRM', 'დასტური', 'Confirm', 'Подтвердить', 'Confirm');
INSERT INTO `langs_copy` VALUES ('142', 'MENUS', 'მენიუები', 'Menus', 'Меню', 'Menus');
INSERT INTO `langs_copy` VALUES ('143', 'ADD_MENU', 'მენიუს დამატება', 'Add Menu', 'Добавить меню', 'Add Menu');
INSERT INTO `langs_copy` VALUES ('144', 'PERCENT', 'პროცენტი', 'Percent', 'Процент', 'Percent');
INSERT INTO `langs_copy` VALUES ('145', 'TAKE_MONEY', 'მოწ. თანხა', 'Money', 'Поданная сумма', 'Money');
INSERT INTO `langs_copy` VALUES ('146', 'RETURN_CHANGE', 'ხურდა', 'Change', 'Сдача', 'Change');
INSERT INTO `langs_copy` VALUES ('147', 'MESSAGES', 'შეტყობინებები', 'Messages', 'Сообщения', 'Messages');
INSERT INTO `langs_copy` VALUES ('148', 'INGREDIENTS_REPORTS', 'ინგრედიენტების რეპორტები', 'Ingredients Reports', 'Отчеты по ингридиентам', 'Ingredients Reports');
INSERT INTO `langs_copy` VALUES ('149', 'OPERATIONS', 'ოპერაციები', 'Operations', 'Операции', 'Operations');
INSERT INTO `langs_copy` VALUES ('150', 'RECIEVING_HISTORY', 'მიღებების ისტორია', 'Recieving History', 'История закупок', 'Recieving History');
INSERT INTO `langs_copy` VALUES ('151', 'SELL_HISTORY', 'გაყიდვების ისტორია', 'Sell History', 'История продаж', 'Sell History');
INSERT INTO `langs_copy` VALUES ('152', 'GUEST_COUNT', 'სტუმრების რაოდენობა', 'Guest Count', 'Кол-во гостей', 'Guest Count');
INSERT INTO `langs_copy` VALUES ('153', 'VIEW_ORDER', 'შეკვეთის ნახვა', 'View Order', 'Смотреть заказ', 'View Order');
INSERT INTO `langs_copy` VALUES ('154', 'ADD_IMAGE', 'სურათის დამატება', 'Add Image', 'Добавить картинку', 'Add Image');
INSERT INTO `langs_copy` VALUES ('155', 'SEND_MSG', 'მიწერე მიმტანს', 'Send Message', 'Послать сообщение', 'Send Message');
INSERT INTO `langs_copy` VALUES ('156', 'ADDITIONAL_INFO', 'დამატებითი ინფო', 'Additional Info', 'Доп. Инфо', 'Additional Info');
INSERT INTO `langs_copy` VALUES ('157', 'ADD_ADDITIONAL_INFO', 'დამატებითი ინფოს დამატება', 'Add Additional Info', 'Добавить доп. инфо', 'Add Additional Info');
INSERT INTO `langs_copy` VALUES ('158', 'ADD_NEW_ADDETIONAL_INFO_TYPE', 'ახალი დამატებითი ინფოს დამატება', 'Add new additional info type', 'Добавить новое доп. инфо', 'Add new additional info type');
INSERT INTO `langs_copy` VALUES ('159', 'CHOOSE_FILE', 'აირჩიეთ ფაილი', 'Choose file', 'Выбрать файл', 'Choose file');
INSERT INTO `langs_copy` VALUES ('160', 'RESOURCES', 'რესურსები', 'Resources', 'Ресурсы', 'Resources');
INSERT INTO `langs_copy` VALUES ('161', 'FILE_SIZE_ERROR_30', 'ფაილის ზომა არ უნდა აღემატებოდეს 30 kb-ს', 'Max allowed file size is 30 kb', 'Размер файла не должен превышать 30 Кб', 'Max allowed file size is 30 kb');
INSERT INTO `langs_copy` VALUES ('162', 'FILE_SIZE_ERROR_500', 'ფაილის ზომა არ უნდა აღემატებოდეს 500 kb-ს', 'Max allowed file size is 500 kb', 'Размер файла не должен превышать 500 Кб', 'Max allowed file size is 500 kb');
INSERT INTO `langs_copy` VALUES ('163', 'CHOOSE_FILE_ERROR', 'გთხოვთ მიუთითოთ ფაილი', 'Choose file', 'Укажите файл', 'Choose file');
INSERT INTO `langs_copy` VALUES ('164', 'RECIEVE_PRODUCT', 'პროდუქტის მიღება', 'Recieve product', 'Принять товар', 'Recieve product');
INSERT INTO `langs_copy` VALUES ('165', 'SPLIT_AMOUNT', 'დაშლილი რაოდენობა', 'Split Amount', 'Штучное кол-во', 'Split Amount');
INSERT INTO `langs_copy` VALUES ('166', 'BARCODE', 'შტრიხკოდი', 'Barcode', 'Штрихкод', 'Barcode');
INSERT INTO `langs_copy` VALUES ('167', 'IN_STORAGE', 'საწყობში', 'In Storage', 'В складе', 'In Storage');
INSERT INTO `langs_copy` VALUES ('168', 'RECIEVE_BY_INVOICE', 'ინვოისით მიღება', 'Recieve by Invoice', 'Прием по инвойсу', 'Recieve by Invoice');
INSERT INTO `langs_copy` VALUES ('169', 'WEIGHT', 'წონა', 'Weight', 'Вес', 'Weight');
INSERT INTO `langs_copy` VALUES ('170', 'WARRANTY', 'გარანტია', 'Warranty', 'Гарантия', 'Warranty');
INSERT INTO `langs_copy` VALUES ('171', 'CONDITION', 'მდგომარეობა', 'Condition', 'Состояние', 'Condition');
INSERT INTO `langs_copy` VALUES ('172', 'NEW', 'ახალი', 'New', 'Новый', 'New');
INSERT INTO `langs_copy` VALUES ('173', 'USED', 'მეორადი', 'Used', 'Вторичный', 'Used');
INSERT INTO `langs_copy` VALUES ('174', 'PRICE_WHOLESALE', 'ფასი საბითუმო', 'Price Wholesale', 'Оптовая цена', 'Price Wholesale');
INSERT INTO `langs_copy` VALUES ('175', 'PRICE_VIP', 'ფასი VIP', 'Price VIP', 'Цена VIP', 'Price VIP');
INSERT INTO `langs_copy` VALUES ('176', 'SPLIT', 'დაშლა', 'Split', 'Поштучно', 'Split');
INSERT INTO `langs_copy` VALUES ('177', 'FILL_SPLIT_AMOUNT', 'მიუთითეთ დაშლის რაოდენობა', 'Fill Split Amount', 'Укажите поштучное кол-во', 'Fill Split Amount');
INSERT INTO `langs_copy` VALUES ('178', 'INVOICE_NUMBER', 'ინვოისის ნომერი', 'Invoice Number', 'Номер инвойса', 'Invoice Number');
INSERT INTO `langs_copy` VALUES ('179', 'FILL_ALL_IMPORTANT_FIELDS', 'შეავსეთ ყველა სავალდებულო ველი', 'Fill all important fields', 'Заполните все обязательные поля', 'Fill all important fields');
INSERT INTO `langs_copy` VALUES ('180', 'SEARCH_PRODUCT', 'პროდუქტის ძებნა', 'Search Product', 'Поиск товара', 'Search Product');
INSERT INTO `langs_copy` VALUES ('181', 'BRANCH', 'ფილიალი', 'Branch', 'Филиал', 'Branch');
INSERT INTO `langs_copy` VALUES ('182', 'WHOLE', 'მთლიანი', 'Whole', 'Целый', 'Whole');
INSERT INTO `langs_copy` VALUES ('183', 'PRODUCTS', 'პროდუქტები', 'Products', 'Товары', 'Products');
INSERT INTO `langs_copy` VALUES ('184', 'CLIENTS', 'კლიენტები', 'Clients', 'Клиенты', 'Clients');
INSERT INTO `langs_copy` VALUES ('185', 'PAYMENTS', 'გადახდები', 'Payments', 'Платежи', 'Payments');
INSERT INTO `langs_copy` VALUES ('186', 'ADD_PAYMENT', 'გადახდა', 'New Payment', 'Оплатить', 'New Payment');
INSERT INTO `langs_copy` VALUES ('187', 'PAYMENT_TYPE', 'გადახდის ტიპი', 'Payment Type', 'Тип оплаты', 'Payment Type');
INSERT INTO `langs_copy` VALUES ('188', 'MONEY', 'თანხა', 'Money', 'Сумма', 'Money');
INSERT INTO `langs_copy` VALUES ('189', 'CHANGE', 'შეცვლა', 'Change', 'Изменить', 'Change');
INSERT INTO `langs_copy` VALUES ('190', 'SELLER', 'გამყიდველი', 'Seller', 'Продавец', 'Seller');
INSERT INTO `langs_copy` VALUES ('191', 'SPLIT_PRICE', 'დაშლის ფასი', 'Split Price', 'Поштучнай цена', 'Split Price');
INSERT INTO `langs_copy` VALUES ('192', 'DATABASE_NULL', 'ბაზის განულება', 'Delete Database', 'Обнулить базу', 'Delete Database');
INSERT INTO `langs_copy` VALUES ('193', 'DATABASE_NULLED_OK', 'ბაზა განულდა წარმატებით', 'Database was nulled succesfuly', 'База обнулена успешно', 'Database was nulled succesfuly');
INSERT INTO `langs_copy` VALUES ('194', 'REMOVED', 'გაუქმებული', 'Removed', 'Отенено', 'Removed');
INSERT INTO `langs_copy` VALUES ('195', 'CHANGE_LANGUAGE', 'ენის შეცვლა', 'Change Language', 'Изменить язык', 'Change Language');
INSERT INTO `langs_copy` VALUES ('196', 'CHANGE_THEME', 'თემის შეცვლა', 'Change Theme', 'Изменить тему', 'Change Theme');
INSERT INTO `langs_copy` VALUES ('197', 'BONUS', 'ბონუსი', 'Bonus', 'Бонус', 'Bonus');
INSERT INTO `langs_copy` VALUES ('198', 'NEW_CLIENT', 'ახალი კლიენტი', 'New Client', 'Новый клиент', 'New Client');
INSERT INTO `langs_copy` VALUES ('199', 'CLIENT', 'კლიენტი', 'Client', 'Клиент', 'Client');
INSERT INTO `langs_copy` VALUES ('200', 'CHOOSE_CLIENT', 'კლიენტის არჩევა', 'Choose Client', 'Выбрать клиента', 'Choose Client');
INSERT INTO `langs_copy` VALUES ('201', 'AMOUNT_IN_STORAGE', 'რაოდ. საწყობში', 'Amount in Storage', 'Кол-во на складе', 'Amount in Storage');
INSERT INTO `langs_copy` VALUES ('202', 'NEED_RECEIPT', 'საჭიროებს რეცეპტს', 'Need Receipt', 'Нужен рецепт', 'Need Receipt');
INSERT INTO `langs_copy` VALUES ('203', 'EARNING', 'მოგება', 'Earning', 'Прибыль', 'Earning');
INSERT INTO `langs_copy` VALUES ('204', 'SELL_PRICE', 'გასაყიდი ფასი', 'Sell Price', 'Продажная цена', 'Sell Price');
INSERT INTO `langs_copy` VALUES ('205', 'VAT', 'დ.ღ.გ.', 'VAT', 'Н.Д.С.', 'VAT');
INSERT INTO `langs_copy` VALUES ('206', 'INCLUDE_VAT', 'დ.ღ.გ.–ის ჩათვლით', 'Include VAT', 'С учетом НДС', 'Include VAT');
INSERT INTO `langs_copy` VALUES ('207', 'WITHOUT_VAT', 'დ.ღ.გ.–ის გარეშე', 'Without VAT', 'Без НДС', 'Without VAT');
INSERT INTO `langs_copy` VALUES ('208', 'CONFIRM_PRODUCT_SELL_PRICE_CHANGE', 'პროდუქტის გასაყიდი ფასი შეიცვალა. გინდათ შეიცვალოს პროდუქტის გასაყიდი ფასი?', 'Product sell price was changed. Do you want to change product sell price with new price?', 'Продажная ценв товара изменилась.', 'Product sell price was changed. Do you want to change product sell price with new price?');
INSERT INTO `langs_copy` VALUES ('209', 'CALCULATE_PERCENT', 'პროცენტის გამოთვლა', 'Calculate Percent', 'Подсчет процентов', 'Calculate Percent');
INSERT INTO `langs_copy` VALUES ('210', 'FROM', 'დან', 'From', 'От', 'From');
INSERT INTO `langs_copy` VALUES ('211', 'BEFORE', 'მდე', 'Before', 'До', 'Before');
INSERT INTO `langs_copy` VALUES ('212', 'PRODUCT', 'პროდუქტი', 'Product', 'Продукт', 'Product');
INSERT INTO `langs_copy` VALUES ('213', 'SOLD_PRODUCTS', 'გაყიდული პროდუქტები', 'Sold Products', 'Продано Продукты', 'Sold Products');
INSERT INTO `langs_copy` VALUES ('214', 'THIS_PRODUCT_IS_NOT_IN_STORAGE', 'მითითებული პროდუქტი არ არის საწყობში', 'This product is not in storage', 'Этот продукт не в памяти', 'This product is not in storage');
INSERT INTO `langs_copy` VALUES ('215', 'CHOOSE_PRODUCT', 'აირჩიეთ პროდუქტი', 'Choose Product', 'Выберите продукт', 'Choose Product');
INSERT INTO `langs_copy` VALUES ('216', 'SELECTED_NUMBER_IS_GREATER_THAN_THE_NUMBER_OF_STOCK', 'მითითებული რაოდენობა მეტია საწყობში არსებულ რაოდენობაზე', 'Set the number is greater than the number of stock', 'Установите число больше, чем число акций', 'Set the number is greater than the number of stock');
INSERT INTO `langs_copy` VALUES ('217', 'SPECIFY_THE_NUMBER_OF_SALE', 'მიუთითეთ გასაყიდი რაოდენობა', 'Specify the number of sale', 'Укажите количество продажи', 'Specify the number of sale');
INSERT INTO `langs_copy` VALUES ('218', 'ADD_SUBCATEGORY', 'ქვეკატეგორიის დამატება', 'Add Subcategory', 'Добавить подкатегорию', 'Add Subcategory');
INSERT INTO `langs_copy` VALUES ('219', 'PARENT', 'მშობელი', 'Parent', 'Родитель', 'Parent');
INSERT INTO `langs_copy` VALUES ('220', 'DISCARD_INGREDIENT', 'ინგრედიენტის ჩამოწერა', 'Discard Ingredient', 'ინგრედიენტის ჩამოწერა', 'Discard Ingredient');
INSERT INTO `langs_copy` VALUES ('221', 'DISCARDED_INGREDIENTS', 'ჩამოწერილი ინრედიენტები', 'Discarded Ingredients', 'ჩამოწერილი ინრედიენტები', 'Discarded Ingredients');
INSERT INTO `langs_copy` VALUES ('222', 'DISCARD_AMOUNT', 'ჩამოსაწერი რაოდენობა', 'Discard Amount', 'ჩამოსაწერი რაოდენობა', 'Discard Amount');
INSERT INTO `langs_copy` VALUES ('223', 'DATABASE', 'ბაზა', 'Database', 'ბაზა', 'Database');
INSERT INTO `langs_copy` VALUES ('224', 'DOWNLOAD_DATABASE_FOR_WEB', 'ბაზის ჩამოტვირთვა საიტისთვის', 'Download Database for WEB', 'ბაზის ჩამოტვირთვა საიტისთვის', 'Download Database for WEB');
INSERT INTO `langs_copy` VALUES ('225', 'REMAINS', 'ნაშთები', 'Remains', 'ნაშთები', 'Remains');
INSERT INTO `langs_copy` VALUES ('226', 'PRE_CHECK', 'წინასწარი ჩეკი', 'Pre Check', 'წინასწარი ჩეკი', 'Pre Check');
INSERT INTO `langs_copy` VALUES ('227', 'SALE_PERCENT', 'ფასდაკ. %', 'Sale %', 'ფასდაკლება %', 'Sale %');
INSERT INTO `langs_copy` VALUES ('228', 'CODE', 'კოდი', 'Code', 'Code', 'Code');
INSERT INTO `langs_copy` VALUES ('229', 'INCORRECT_VALUES', 'არასწორი მონაცემები!', 'Incorrect values', 'Incorrect values', 'Incorrect values');
INSERT INTO `langs_copy` VALUES ('230', 'DAY_TRADED_VOLUME', 'ღის ნავაჭრი', 'Day trading volume', 'Day trading volume', 'Day trading volume');
INSERT INTO `langs_copy` VALUES ('231', 'MAIN_PAGE', 'მთავარი გვერდი', 'Main Page', 'Main Page', 'Main Page');
INSERT INTO `langs_copy` VALUES ('232', 'COMMENT', 'კომენტარი', 'Comment', 'Comment', 'Comment');
INSERT INTO `langs_copy` VALUES ('233', 'ORDERS', 'შეკვეთები', 'Orders', 'Orders', 'Orders');
INSERT INTO `langs_copy` VALUES ('234', 'SELLING_PRICE_HAS_NOT_SET', 'გასაყიდი ფასი არ აქვს მითითებული', 'Selling price has not set', 'Selling price has not set', 'Selling price has not set');
INSERT INTO `langs_copy` VALUES ('235', 'NEW_SECTION', 'ახალი სექცია', 'New Saction', 'New Saction', 'New Saction');
INSERT INTO `langs_copy` VALUES ('236', 'PORTION', 'პორცია', 'Portion', 'Portion', 'Portion');
INSERT INTO `langs_copy` VALUES ('237', 'ADD_TYPE', 'ტიპის დამატება', 'Add Type', 'Add Type', 'Add Type');
INSERT INTO `langs_copy` VALUES ('238', 'AMOUNT_SHORT', 'რაოდ.', 'Am.', 'Am.', 'Am.');
INSERT INTO `langs_copy` VALUES ('239', 'FOR_SERVICE', 'მომსახურების', 'For Service', 'For Service', 'For Service');
INSERT INTO `langs_copy` VALUES ('240', 'GEL', 'ლ', 'GEL', 'GEL', 'GEL');
INSERT INTO `langs_copy` VALUES ('241', 'SALE', 'ფასდაკლება', 'Sale', 'Sale', 'Sale');
INSERT INTO `langs_copy` VALUES ('242', 'PLACE_OF_MAKING', 'გატანის ადგილი', 'Place Of Making', 'Place Of Making', 'Place Of Making');
INSERT INTO `langs_copy` VALUES ('243', 'SUM', 'ჯამი', 'Sum', 'Sum', 'Sum');
INSERT INTO `langs_copy` VALUES ('244', 'LANGUAGES', 'ენები', 'Languages', 'Languages', 'Languages');
INSERT INTO `langs_copy` VALUES ('245', 'NEW_PHRASE', 'ახალი ფრაზა', 'New Phrase', 'New Phrase', 'New Phrase');
INSERT INTO `langs_copy` VALUES ('246', 'TURKISH', 'თურქულად', 'Turkish', 'Turkish', 'Turkish');
INSERT INTO `langs_copy` VALUES ('247', 'ENGLISH', 'ინგლისურად', 'English', 'English', 'English');
INSERT INTO `langs_copy` VALUES ('248', 'RUSSIAN', 'რუსულად', 'Russian', 'Russian', 'Russian');
INSERT INTO `langs_copy` VALUES ('249', 'GEORGIAN', 'ქართულად', 'Georgian', 'Georgian', 'Georgian');
INSERT INTO `langs_copy` VALUES ('250', 'WAYBILLS', 'ზედნადებები', 'Waybills', 'Waybills', 'Waybills');
INSERT INTO `langs_copy` VALUES ('251', 'WAYBILL_NO', 'ზედნადებების №', 'Waybill No', 'Waybill No', 'Waybill No');
INSERT INTO `langs_copy` VALUES ('252', 'SELLER_NAME', 'გამყიდველი', 'Seller Name', 'Seller Name', 'Seller Name');
INSERT INTO `langs_copy` VALUES ('253', 'ACTIVATE_DATE', 'აქტივაციის თარიღი', 'Activate Date', 'Activate Date', 'Activate Date');
INSERT INTO `langs_copy` VALUES ('254', 'CREATE_DATE', 'შექმნის თარიღი', 'Create Date', 'Create Date', 'Create Date');
INSERT INTO `langs_copy` VALUES ('255', 'CLOSE_DATE', 'დასრულების თარიღი', 'Close Date', 'Close Date', 'Close Date');
INSERT INTO `langs_copy` VALUES ('256', 'DRIVER_NAME', 'მძღოლი', 'Driver Name', 'Driver Name', 'Driver Name');
INSERT INTO `langs_copy` VALUES ('257', 'CAR', 'ავტო', 'Car', 'Car', 'Car');
INSERT INTO `langs_copy` VALUES ('258', 'TRANS_PRICE', 'ტრანს. თანხა', 'Trans. Price', 'Trans. Price', 'Trans. Price');
INSERT INTO `langs_copy` VALUES ('259', 'DELIVERY_DATE', 'მიწოდების თარიღი', 'Delivery Date', 'Delivery Date', 'Delivery Date');
INSERT INTO `langs_copy` VALUES ('260', 'VIEW', 'ნახვა', 'View', 'View', 'View');
INSERT INTO `langs_copy` VALUES ('261', 'CONFIRM_RECEIVE', 'მიღების დადასტურება', 'Confirm Receive', 'Confirm Receive', 'Confirm Receive');
INSERT INTO `langs_copy` VALUES ('262', 'BAR_CODE', 'შტრიხკოდი', 'Barcode', 'Barcode', 'Barcode');
INSERT INTO `langs_copy` VALUES ('263', 'ADD_BARCODE', 'შტრიხკოდის დამატება', 'Add Barcode', 'Add Barcode', 'Add Barcode');
INSERT INTO `langs_copy` VALUES ('264', 'RECEIVE', 'მიღება', 'Receive', 'Receive', 'Receive');
INSERT INTO `langs_copy` VALUES ('265', 'CAR_NO', 'მანქანის №', 'Car No', 'Car No', 'Car No');
INSERT INTO `langs_copy` VALUES ('266', 'GRANT_RIGHTS', 'უფლებების მინიჭება', 'Grant Rights', 'Grant Rights', 'Grant Rights');
INSERT INTO `langs_copy` VALUES ('267', 'SUPPLIERS', 'მომწოდებლები', 'Suppliers', 'Suppliers', 'Suppliers');
INSERT INTO `langs_copy` VALUES ('268', 'TRADED', 'ნავაჭრი', 'Traded', 'Traded', 'Traded');
INSERT INTO `langs_copy` VALUES ('269', 'RECEIPT', 'რეცეპტი', 'Receipt', 'Receipt', 'Receipt');
INSERT INTO `langs_copy` VALUES ('270', 'STORAGE', 'საწყობი', 'Storage', 'Storage', 'Storage');
INSERT INTO `langs_copy` VALUES ('271', 'DISCARDS', 'ჩამოწერები', 'Discards', 'Discards', 'Discards');
INSERT INTO `langs_copy` VALUES ('272', 'PAYED_MONEY', 'გადახდილი თანხა', 'Payed Money', 'Payed Money', 'Payed Money');
INSERT INTO `langs_copy` VALUES ('273', 'DEADLINE', 'ვადა', 'Deadline', 'Deadline', 'Deadline');
INSERT INTO `langs_copy` VALUES ('275', 'UPLOAD_TO_RS', 'აიტვირთოს rs.ge-ზე', 'Upload to RS', 'Upload to RS', 'Upload to RS');
INSERT INTO `langs_copy` VALUES ('276', 'CARD', 'ბარათი', 'Card', 'Card', 'Card');
INSERT INTO `langs_copy` VALUES ('277', 'DEBT', 'ვალი', 'Debt', 'Debt', 'Debt');
INSERT INTO `langs_copy` VALUES ('278', 'TIN', 'საიდენტიფიკაციო ნომერი', 'TIN', 'TIN', 'TIN');
INSERT INTO `langs_copy` VALUES ('279', 'DRIVERS', 'მძღოლები', 'Drivers', 'Drivers', 'Drivers');
INSERT INTO `langs_copy` VALUES ('280', 'DRIVER', 'მძღოლი', 'Driver', 'Driver', 'Driver');
INSERT INTO `langs_copy` VALUES ('281', 'CHOOSE_DRIVER', 'აირჩიეთ მძღოლი', 'Select Driver', 'Select Driver', 'Select Driver');
INSERT INTO `langs_copy` VALUES ('282', 'NEW_DRIVER', 'ახალი მძღოლი', 'New Driver', 'New Driver', 'New Driver');
INSERT INTO `langs_copy` VALUES ('283', 'CARS', 'მანქანები', 'Cars', 'Cars', 'Cars');
INSERT INTO `langs_copy` VALUES ('284', 'STORAGES', 'საწყობები', 'Storages', 'Storages', 'Storages');
INSERT INTO `langs_copy` VALUES ('285', 'ADD_STORAGE', 'საწყობის დამატება', 'Add Storage', 'Add Storage', 'Add Storage');
INSERT INTO `langs_copy` VALUES ('286', 'MOVE_PRODUCTS', 'პროდუქტების გადატანა', 'Move Products', 'Move Products', 'Move Products');
INSERT INTO `langs_copy` VALUES ('287', 'TRUCK', '????????', 'Truck', 'Truck', 'Truck');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('3', '2017_05_02_140156_entrust_setup_tables', '2');
INSERT INTO `migrations` VALUES ('4', '2017_05_05_144232_entrust_setup_tables', '3');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `display_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES ('1', 'super create-post', 'Create Posts', 'create new blog posts', '2017-05-05 14:46:44', '2017-05-05 14:46:44');
INSERT INTO `permissions` VALUES ('2', 'super edit-user', 'Edit Users', 'edit existing users', '2017-05-05 14:46:44', '2017-05-05 14:46:44');
INSERT INTO `permissions` VALUES ('3', 'Flight 10', null, null, '2017-05-08 08:19:29', '2017-05-08 08:19:29');
INSERT INTO `permissions` VALUES ('5', 'test1234', 'test1234', 'test1234', '2017-05-08 08:21:51', '2017-05-08 08:21:51');
INSERT INTO `permissions` VALUES ('6', 'ggggggggggg', 'ggggggggggg', 'ggggggggggg', '2017-05-08 08:23:38', '2017-05-08 08:23:38');
INSERT INTO `permissions` VALUES ('7', 'hhhhhhhhhttttttt', 'hhhhhhhhhttttttt', 'hhhhhhhhhttttttt', '2017-05-08 08:24:29', '2017-05-08 08:24:29');
INSERT INTO `permissions` VALUES ('8', 'nnmmmm', 'nnmmmm', 'nnmmmm', '2017-05-08 08:25:18', '2017-05-08 08:25:18');
INSERT INTO `permissions` VALUES ('9', 'ffffff', 'fffffffffffffffffffffffff', 'ffffffffffffffffffffffffffff', '2017-05-08 08:27:15', '2017-05-08 08:27:15');
INSERT INTO `permissions` VALUES ('10', 'new test', 'new test', 'new test', '2017-05-08 08:34:18', '2017-05-08 08:34:18');

-- ----------------------------
-- Table structure for permission_role
-- ----------------------------
DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of permission_role
-- ----------------------------
INSERT INTO `permission_role` VALUES ('1', '1');
INSERT INTO `permission_role` VALUES ('2', '1');
INSERT INTO `permission_role` VALUES ('1', '2');
INSERT INTO `permission_role` VALUES ('1', '5');
INSERT INTO `permission_role` VALUES ('2', '5');
INSERT INTO `permission_role` VALUES ('10', '5');
INSERT INTO `permission_role` VALUES ('7', '6');
INSERT INTO `permission_role` VALUES ('8', '6');
INSERT INTO `permission_role` VALUES ('9', '6');
INSERT INTO `permission_role` VALUES ('3', '7');
INSERT INTO `permission_role` VALUES ('5', '7');
INSERT INTO `permission_role` VALUES ('3', '10');
INSERT INTO `permission_role` VALUES ('7', '14');
INSERT INTO `permission_role` VALUES ('7', '16');
INSERT INTO `permission_role` VALUES ('6', '17');
INSERT INTO `permission_role` VALUES ('7', '18');
INSERT INTO `permission_role` VALUES ('6', '19');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `PRODUCT_ID` int(11) NOT NULL AUTO_INCREMENT,
  `SUPPLIER_CODE` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `BARCODE` varchar(60) CHARACTER SET utf8 DEFAULT '',
  `NAME` text CHARACTER SET utf8 NOT NULL,
  `DESCRIPTION` text CHARACTER SET utf8,
  `CATEGORY_ID` int(11) NOT NULL,
  `MINAMOUNT` decimal(10,2) DEFAULT NULL,
  `PRICE` decimal(10,2) DEFAULT '0.00',
  `CONDITION_ID` int(11) DEFAULT NULL,
  `STATE_ID` int(2) NOT NULL DEFAULT '1',
  `SELF_PRICE` decimal(15,2) DEFAULT '0.00',
  `LAST_PRICE` decimal(10,2) DEFAULT NULL,
  `UNIT_NAME` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `UNIT_ID` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`PRODUCT_ID`),
  KEY `SUPPLIER_CODE_INDX` (`SUPPLIER_CODE`) USING BTREE,
  KEY `PRODUCT_ID_INX` (`PRODUCT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('1', null, '25', 'ზეთი \"მზიური\"', 'asd', '80', null, '2.00', '1', '2', '2.00', '2.00', 'ცალი', '1', '2017-05-17 14:03:29', null);
INSERT INTO `products` VALUES ('2', null, '', 'ზალატოი', '', '80', null, '3.00', '1', '2', '3.00', '3.00', '', null, '2017-05-17 14:03:30', null);
INSERT INTO `products` VALUES ('3', null, '', 'ქარვა', '', '80', null, '1.00', '1', '2', '1.00', '1.00', '', null, '2017-05-17 14:03:22', null);
INSERT INTO `products` VALUES ('4', null, '', 'ბრავიტა', '', '80', null, '3.00', '1', '2', '3.00', '3.00', '', null, '2017-05-11 10:39:39', null);
INSERT INTO `products` VALUES ('5', null, '', 'სკუმბრია', '', '82', null, '12.00', '1', '2', '12.00', '12.00', '', null, '2017-05-17 14:03:24', null);
INSERT INTO `products` VALUES ('6', null, '', 'კალმახი', '', '81', null, '10.00', '1', '2', '10.00', '10.00', '', null, '2017-05-17 14:03:28', null);
INSERT INTO `products` VALUES ('7', null, '', 'ლოქო', '', '81', null, '3.00', '1', '2', '3.00', '3.00', '', null, '2017-05-17 14:03:26', null);
INSERT INTO `products` VALUES ('8', null, '', 'ბროილერის ქათამი', '', '79', null, '4.00', '1', '2', '4.00', '4.00', '', null, '2017-05-17 14:03:37', null);
INSERT INTO `products` VALUES ('9', null, '', 'დედალი', '', '79', null, '4.00', '1', '2', '4.00', '4.00', '', null, '2017-05-17 14:03:36', null);
INSERT INTO `products` VALUES ('10', null, null, 'BOSCH KDN46VW25U', 'ორკამერიანი მაცივარი/საყინულე\nNo frost მშრალი გაყინვა \nმართვა: ელექტრონული (სენსორები)', '92', null, '1915.00', '1', '1', '4.00', '4.00', 'ცალი', '1', '2017-05-17 14:13:06', null);
INSERT INTO `products` VALUES ('12', null, '', 'ვიბრატორიdsdsdsdsdsds', null, '1', null, '120.00', '1', '2', '0.00', null, 'ცალი', '1', '2017-05-17 14:03:34', '2017-05-03 14:31:10');
INSERT INTO `products` VALUES ('14', null, '', 'ვიბრატორიwsdfssssssssssssssssssssssss', null, '1', null, '120.00', '1', '2', '0.00', null, 'ცალი', '1', '2017-05-17 14:03:32', '2017-05-03 14:46:57');
INSERT INTO `products` VALUES ('15', null, 'sadsasa', 'sad', 'sadsasad', '81', '1.00', '2.00', '1', '2', null, null, 'გრამი', '3', '2017-05-11 11:17:23', '2017-05-11 11:17:07');
INSERT INTO `products` VALUES ('16', null, null, 'HITACHI 65HZ6W69', 'LED ტელევიზორი \nზომა: 65 ინჩი (165 სმ)\nრეზოლუცია: 3840 x 2160', '76', null, '3475.00', '1', '1', null, null, 'ცალი', '1', '2017-05-17 14:14:38', '2017-05-17 14:09:15');
INSERT INTO `products` VALUES ('17', null, null, 'SAMSUNG RB37K63412C/WT/O', 'ორკამერიანი მაცივარი/საყინულე\nმუშაობის პრინციპი: NoFrost მშრალი გაყინვა \nმართვა: ელექტრონული', '92', null, '2688.00', '1', '1', null, null, 'ცალი', '1', '2017-05-17 14:19:22', '2017-05-17 14:19:22');
INSERT INTO `products` VALUES ('18', null, null, 'SAMSUNG RB37K63412A/WT/O', 'ორკამერიანი მაცივარი/საყინულე\nმუშაობის პრინციპი: NoFrost მშრალი გაყინვა \nმართვა: ელექტრონული', '1', null, '2730.00', '1', '1', null, null, 'ცალი', '1', '2017-05-17 14:20:06', '2017-05-17 14:20:06');
INSERT INTO `products` VALUES ('19', null, null, 'GORENJE K17FE', 'ჩაიდანი\nტევადობა: 1.7 ლ\nსიმძლავრე: 2200 ვატი', '98', null, '115.00', '1', '1', null, null, 'ცალი', '1', '2017-05-18 11:38:33', '2017-05-18 11:38:33');
INSERT INTO `products` VALUES ('20', null, null, 'test', 'test', '1', null, '43.00', '1', '2', null, null, 'ცალი', '1', '2017-05-19 07:44:53', '2017-05-19 07:17:00');
INSERT INTO `products` VALUES ('21', null, 'kima', 'kima', 'kima', '74', '1.00', '2.00', '2', '1', null, null, 'ცალი', '1', '2017-05-19 07:14:22', '2017-05-19 07:14:22');

-- ----------------------------
-- Table structure for product_files
-- ----------------------------
DROP TABLE IF EXISTS `product_files`;
CREATE TABLE `product_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `original_file_name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `is_cover` tinyint(1) NOT NULL DEFAULT '0',
  `state_id` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of product_files
-- ----------------------------
INSERT INTO `product_files` VALUES ('2', '17', '557fd1ba7f55a2b6b26e984858a1d298.jpg', '557fd1ba7f55a2b6b26e984858a1d298.jpg_original', '0', '1', '2017-05-19 12:39:48', '2017-05-19 12:39:48');
INSERT INTO `product_files` VALUES ('3', '17', 'b49ad8bdcc52a1daa4d259042637139b.jpeg', 'b49ad8bdcc52a1daa4d259042637139b.jpeg_original', '0', '1', '2017-05-19 12:42:07', '2017-05-19 12:42:07');
INSERT INTO `product_files` VALUES ('4', '17', '3570d54396ddae645882d06447300bd0.jpg', '3570d54396ddae645882d06447300bd0.jpg_original', '0', '1', '2017-05-19 12:43:37', '2017-05-19 12:43:37');

-- ----------------------------
-- Table structure for product_info
-- ----------------------------
DROP TABLE IF EXISTS `product_info`;
CREATE TABLE `product_info` (
  `PRODUCT_INFO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ID` int(11) NOT NULL,
  `PRODUCT_INFO_TYPE_ID` int(11) NOT NULL,
  `PRODUCT_INFO_TYPE_NAME` varchar(255) CHARACTER SET utf8 NOT NULL,
  `VALUE` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `STATE_ID` int(11) NOT NULL DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`PRODUCT_INFO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product_info
-- ----------------------------
INSERT INTO `product_info` VALUES ('1', '1', '20', 'ტრანსმისია', 'ავტომატიკა', '2', '2017-05-03 15:21:34', '2017-05-03 15:12:38');
INSERT INTO `product_info` VALUES ('2', '1', '20', 'ტრანსმისია', 'meqanika', '1', '2017-05-11 12:03:04', '2017-05-11 12:03:04');
INSERT INTO `product_info` VALUES ('3', '1', '21', 'ტრანსმისიაssssss', 'model', '2', '2017-05-11 12:06:30', '2017-05-11 12:06:17');
INSERT INTO `product_info` VALUES ('4', '1', '23', 'xxx', 'asd', '1', '2017-05-11 12:13:12', '2017-05-11 12:13:12');

-- ----------------------------
-- Table structure for product_info_types
-- ----------------------------
DROP TABLE IF EXISTS `product_info_types`;
CREATE TABLE `product_info_types` (
  `PRODUCT_INFO_TYPE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_INFO_TYPE_NAME` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`PRODUCT_INFO_TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product_info_types
-- ----------------------------
INSERT INTO `product_info_types` VALUES ('1', 'ბრენდი', null, null);
INSERT INTO `product_info_types` VALUES ('2', 'მოდელი', null, null);
INSERT INTO `product_info_types` VALUES ('3', 'ეკრანის ზომა', null, null);
INSERT INTO `product_info_types` VALUES ('4', 'ეკრანის ტიპი', null, null);
INSERT INTO `product_info_types` VALUES ('5', 'ეკრანის რეზოლუცია', null, null);
INSERT INTO `product_info_types` VALUES ('6', 'CPU', null, null);
INSERT INTO `product_info_types` VALUES ('7', 'RAM', null, null);
INSERT INTO `product_info_types` VALUES ('8', 'HDD', null, null);
INSERT INTO `product_info_types` VALUES ('9', 'LAN', null, null);
INSERT INTO `product_info_types` VALUES ('10', 'ODD', null, null);
INSERT INTO `product_info_types` VALUES ('11', 'Display', null, null);
INSERT INTO `product_info_types` VALUES ('12', 'Battery', null, null);
INSERT INTO `product_info_types` VALUES ('13', 'OS', null, null);
INSERT INTO `product_info_types` VALUES ('14', 'Weight', null, null);
INSERT INTO `product_info_types` VALUES ('15', 'SupplierCode', null, null);
INSERT INTO `product_info_types` VALUES ('16', 'PartNumber', null, null);
INSERT INTO `product_info_types` VALUES ('17', 'ნოუთის ინფო', null, null);
INSERT INTO `product_info_types` VALUES ('18', 'მონაცემები', null, null);
INSERT INTO `product_info_types` VALUES ('20', 'ტრანსმისია', '2017-05-03 15:06:49', '2017-05-03 15:06:49');
INSERT INTO `product_info_types` VALUES ('21', 'ტრანსმისიაssssss', '2017-05-03 15:39:19', '2017-05-03 15:39:19');
INSERT INTO `product_info_types` VALUES ('22', 'ragaca', '2017-05-11 12:12:26', '2017-05-11 12:12:26');
INSERT INTO `product_info_types` VALUES ('23', 'xxx', '2017-05-11 12:12:57', '2017-05-11 12:12:57');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `display_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'super owner', 'Project Owner', 'User is the owner of a given project', '2017-05-05 14:46:44', '2017-05-05 14:46:44');
INSERT INTO `roles` VALUES ('2', 'super admin', 'User Administrator', 'User is allowed to manage and edit other users', '2017-05-05 14:46:44', '2017-05-05 14:46:44');
INSERT INTO `roles` VALUES ('3', 'didi yle', 'didi yle', 'didi yle', '2017-05-10 11:28:00', '2017-05-10 11:28:00');
INSERT INTO `roles` VALUES ('4', 'das', 'dasd', 'asdasd', '2017-05-10 11:31:22', '2017-05-10 11:31:22');
INSERT INTO `roles` VALUES ('5', 'patara yle', 'patara yle', 'patara yle', '2017-05-10 11:32:09', '2017-05-10 11:32:09');
INSERT INTO `roles` VALUES ('6', 'satesto yle', 'satesto yle', 'satesto yle', '2017-05-10 11:35:26', '2017-05-10 11:35:26');
INSERT INTO `roles` VALUES ('7', 'Mmmmmmmmmmmm', 'Mmmmmmmmmmmm', 'Mmmmmmmmmmmm', '2017-05-10 11:36:43', '2017-05-10 11:36:43');
INSERT INTO `roles` VALUES ('8', 'gdf', 'gdf', 'gdfgdfg', '2017-05-10 11:37:12', '2017-05-10 11:37:12');
INSERT INTO `roles` VALUES ('9', 'bbbb', 'bbbbbbbbbb', 'bbbbbbbbbbbbbbbbbb', '2017-05-10 11:38:06', '2017-05-10 11:38:06');
INSERT INTO `roles` VALUES ('10', 'prena', 'prena', 'prena', '2017-05-10 11:38:34', '2017-05-10 11:38:34');
INSERT INTO `roles` VALUES ('11', 'nnnnn', 'nnnnnnnnn', 'nnnnnnnnnnnnnnnnnn', '2017-05-10 11:39:39', '2017-05-10 11:39:39');
INSERT INTO `roles` VALUES ('12', 'gdfg', 'dfgdf', 'gdfgdfg', '2017-05-10 11:40:13', '2017-05-10 11:40:13');
INSERT INTO `roles` VALUES ('13', 'ffffffffffffffff', 'ffffffffff', 'fffffffffffff', '2017-05-10 11:40:25', '2017-05-10 11:40:25');
INSERT INTO `roles` VALUES ('14', 'nnnnnn', 'nnnnnnnnnnnn', 'nnnnnnnnnnnnnn', '2017-05-10 11:52:32', '2017-05-10 11:52:32');
INSERT INTO `roles` VALUES ('15', 'qqqqqq', 'qqqqqqqqqqqqqqq', 'qqqqqqqqqqqqqqq', '2017-05-10 11:53:15', '2017-05-10 11:53:15');
INSERT INTO `roles` VALUES ('16', 'selections[0].data', 'selections[0].data', 'selections[0].data', '2017-05-10 11:53:57', '2017-05-10 11:53:57');
INSERT INTO `roles` VALUES ('17', 'data', 'data', 'data', '2017-05-10 11:54:17', '2017-05-10 11:54:17');
INSERT INTO `roles` VALUES ('18', 'kima', 'kima', 'kima', '2017-05-10 11:56:33', '2017-05-10 11:56:33');
INSERT INTO `roles` VALUES ('19', 'JUJU', 'JUJU', 'JUJU', '2017-05-10 12:21:03', '2017-05-10 12:21:03');

-- ----------------------------
-- Table structure for role_user
-- ----------------------------
DROP TABLE IF EXISTS `role_user`;
CREATE TABLE `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of role_user
-- ----------------------------
INSERT INTO `role_user` VALUES ('1', '1');
INSERT INTO `role_user` VALUES ('2', '2');
INSERT INTO `role_user` VALUES ('2', '5');
INSERT INTO `role_user` VALUES ('1', '6');
INSERT INTO `role_user` VALUES ('2', '6');
INSERT INTO `role_user` VALUES ('2', '16');
INSERT INTO `role_user` VALUES ('1', '17');
INSERT INTO `role_user` VALUES ('2', '17');

-- ----------------------------
-- Table structure for storage
-- ----------------------------
DROP TABLE IF EXISTS `storage`;
CREATE TABLE `storage` (
  `STORAGE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `BRANCH_ID` int(11) NOT NULL,
  `PRODUCT_ID` int(11) NOT NULL,
  `AMOUNT` decimal(10,4) DEFAULT NULL,
  `PLACE` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `VALID_DATE` date DEFAULT NULL,
  `amounta` decimal(10,4) NOT NULL,
  PRIMARY KEY (`STORAGE_ID`,`PRODUCT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of storage
-- ----------------------------
INSERT INTO `storage` VALUES ('1', '1', '1', '100.0000', 'rus', null, '0.0000');
INSERT INTO `storage` VALUES ('2', '2', '2', '50.0000', 'adsa', null, '0.0000');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state_id` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Admin', 'admin@gmail.com', '$2y$10$b9Rs1OvVD/BThlNHmSGaauHgeTzWZsvj9IQJe.GH8hKudziWP0IRG', 'lySZSZtN5LGugVJ549qph3UmYFh22h3KSUKVT6Lr5KR2yDbcdTtV6b6qIoT5', '2017-05-04 11:45:54', '2017-05-04 11:46:15', '1');
INSERT INTO `users` VALUES ('2', 'Admin1', 'admin1@gmail.com', '$2y$10$b9Rs1OvVD/BThlNHmSGaauHgeTzWZsvj9IQJe.GH8hKudziWP0IRG', '', '2017-05-04 11:58:39', '0000-00-00 00:00:00', '1');

-- ----------------------------
-- Table structure for web_menu
-- ----------------------------
DROP TABLE IF EXISTS `web_menu`;
CREATE TABLE `web_menu` (
  `menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name_ge` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_ru` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  `editable` tinyint(1) NOT NULL DEFAULT '1',
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `order` int(11) DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`menu_id`),
  KEY `menu_state_index` (`state`),
  KEY `menu_disabled_index` (`disabled`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of web_menu
-- ----------------------------
INSERT INTO `web_menu` VALUES ('72', 'მთავარი', 'მთავარი', 'მთავარი', '/', null, '0', '0', '1', null, '2017-05-18 09:38:18', '2017-05-18 09:39:22', '1', null);
INSERT INTO `web_menu` VALUES ('73', 'ჩვენს შესახებ', 'ჩვენს შესახებ', 'ჩვენს შესახებ', '/', null, '1', '0', '1', null, '2017-05-18 09:38:35', '2017-05-18 09:38:35', '2', null);
INSERT INTO `web_menu` VALUES ('74', 'კონტაქტი', 'კონტაქტი', 'კონტაქტი', '/contact', null, '1', '0', '1', null, '2017-05-18 09:38:51', '2017-05-18 09:38:51', '3', null);

-- ----------------------------
-- Table structure for web_params
-- ----------------------------
DROP TABLE IF EXISTS `web_params`;
CREATE TABLE `web_params` (
  `param_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `value` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`param_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of web_params
-- ----------------------------
INSERT INTO `web_params` VALUES ('1', 'address_ge', 'მისამართი: თბილისი თამარაშვილის შესახვევი 4 (გ. სვანიძის 8)', '2016-11-13 23:07:33', '0000-00-00 00:00:00');
INSERT INTO `web_params` VALUES ('2', 'address_en', 'Adress: Tamarashvili str. 4 (G. Svanidze 8), Tbilisi ', '2016-11-13 23:07:33', '0000-00-00 00:00:00');
INSERT INTO `web_params` VALUES ('3', 'address_ru', 'Adress: Tamarashvili str. 4 (G. Svanidze 8), Tbilisi ', '2016-11-13 23:07:33', '0000-00-00 00:00:00');
INSERT INTO `web_params` VALUES ('4', 'mobile_main', '+(995)322 29 34 92 ', '2016-11-13 23:08:41', '0000-00-00 00:00:00');
INSERT INTO `web_params` VALUES ('5', 'mobile', '+(995)322 29 34 92 ', '2016-11-13 23:08:41', '0000-00-00 00:00:00');
INSERT INTO `web_params` VALUES ('6', 'workTime', '09:00 - 18:01', '2016-11-13 23:09:30', '2017-05-17 13:09:25');
INSERT INTO `web_params` VALUES ('7', 'email', 'info@gdc.ge', '2016-11-14 11:42:25', '2017-05-18 11:36:15');
