<?php

$fp = fopen("CartuBankKEY.pem", "r");
$cert = fread($fp, 8192);
fclose($fp);

/*
 ბანკიდან callback url ზე შემოსული მოთხოვნების log.txt ფაილში ჩაწერა 
 გაანთავისუფლეთ კოდი კომენტარებისგან თუ გსურთ გააქტიურება 
*/

/*$h = fopen('log.txt', 'a');
fwrite($h, 'ConfirmRequest: ' . print_r($_REQUEST, true) . "\n");
fclose($h);*/

///////////////////////////////////////////////////////////////
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, "http://gdc.pixl.ge/callback");
curl_setopt($ch, CURLOPT_PROXY, '');
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $_REQUEST);

// in real life you should use something like:
// curl_setopt($ch, CURLOPT_POSTFIELDS,
//          http_build_query(array('postvar1' => 'value1')));

// receive server response ...
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

$server_output = curl_exec($ch);

$h = fopen('log.txt', 'a');
fwrite($h, 'CALLBACK: ' . $server_output . "\n");
fclose($h);

curl_close($ch);

if ($server_output != 1) {
    ResponseToBank($TransactionId, $PaymentId, 'DECLINED');
}

// further processing ....
//echo $server_output;
//die();

///////////////////////////////////////////////////////////////

/*  გადამოწმება ხელმოწერაზე  დასარწმუნებლად, რომ მოთხოვნა ნამდვილად ქართუ ბანკის ინიცირებულია  */
if (
!openssl_verify(
    'ConfirmRequest=' . $_REQUEST['ConfirmRequest'],
    base64_decode($_REQUEST['signature']),
    openssl_get_publickey($cert)
)
)

    die("signature error");


/*  xml პარამეტრების გადატანა იდენტიფიკატორებში  */

$xml = xml_parser_create('UTF-8');
xml_parse_into_struct($xml, $_POST['ConfirmRequest'], $vals);
xml_parser_free($xml);

foreach ($vals as $data) {
    if ($data['tag'] == 'STATUS')
        $Status = $data['value'];
    if ($data['tag'] == 'PAYMENTID')
        $PaymentId = $data['value'];
    if ($data['tag'] == 'PAYMENTDATE')
        $PaymentDate = $data['value'];
    if ($data['tag'] == 'TRANSACTIONID')
        $TransactionId = $data['value'];
    if ($data['tag'] == 'AMOUNT')
        $Amount = $data['value'];
    if ($data['tag'] == 'REASON')
        $Reason = $data['value'];
    if ($data['tag'] == 'CARDTYPE')
        $CardType = $data['value'];

}


if ($Status == 'C') {
    if (check_order($TransactionId, $Amount))
        ResponseToBank($TransactionId, $PaymentId, 'ACCEPTED');
    else
        ResponseToBank($TransactionId, $PaymentId, 'DECLINED');
}


if ($Status == 'Y') {

    if (success_order($TransactionId))
        ResponseToBank($TransactionId, $PaymentId, 'ACCEPTED');
    else
        ResponseToBank($TransactionId, $PaymentId, 'DECLINED');

}


ResponseToBank($TransactionId, $PaymentId, 'ACCEPTED');


function ResponseToBank($TransactionId, $PaymentId, $Status)
{
    $xmlstr = "<ConfirmResponse>
                                            <TransactionId>$TransactionId</TransactionId>
                                            <PaymentId>$PaymentId</PaymentId>
                                            <Status>$Status</Status>
                                      </ConfirmResponse>";

    header('Content-type: text/xml');
    die($xmlstr);
}


function check_order($orderid, $amount)
{
    //  ტრანზაქციის არსებობის შესამოწმებელი კოდი

    return true;
}


function success_order($orderid)
{

    //  ტრანზაქციის დამოწმების კოდი

    return true;

}


?>