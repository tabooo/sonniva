function addItemToShoppingCart(productId, prodImg) {
    var request = {
        id: productId,
        //name: 'test',
        qty: $("#addQtyValue").val() ? $("#addQtyValue").val() : 1,
        //price: 30,
        options: {
            img: prodImg
        }

    };

    $.post($("#url").val() + '/cart/add', request, function (result) {
        $.toast({
            type: 'info',
            title: 'შეტყობინება',
            // subtitle: '11 mins ago',
            content: 'პროდუქტი წარმატებით დაემატა კალათაში',
            delay: 3000,
            /*img: {
                src: 'https://via.placeholder.com/20',
                class: 'rounded-0', /!**  Classes you want to apply separated my a space to modify the image **!/
                alt: 'Image'
            }*/
        });
        getCartContent();
    })
}

function updateItemToShoppingCart(rowId, qty, reload) {
    var request = {
        rowId: rowId,
        qty: qty
    };

    $.post($("#url").val() + '/cart/updateItem', request, function (result) {
        if (reload) {
            location.reload();
        } else {
            getCartContent();
        }
    })
}

function removeItemFromShoppingCart(rowId, reload) {
    $.get($("#url").val() + '/cart/remove/' + rowId, function (result) {
        if (reload) {
            location.reload();
        } else {
            getCartContent();
        }
    })
}

function getCartContent() {
    $.get($("#url").val() + '/cart/getCartContent', function (result) {
        $("#cartTotalItems").html(result.count);
        $("#cartTotalItems2").html(result.count);
        $("#cartSubTotal").html(result.subtotal);

        var body = '';
        for (var key in result.cart) {
            var item = result.cart[key];
            body += '<div class="content-item d-flex justify-content-between">' +
                '<div class="cart-img">' +
                ' <a href="#">' +
                '<img src="' + item.options.img + '" height="50" alt="">' +
                '</a></div>' +
                '<div class="cart-disc">' +
                '<p><a href="#">' + item.name + '</a></p>' +
                '<span>' + item.qty + ' x ' + item.price + '</span>' +
                '</div>' +
                '<div class="delete-btn">' +
                '<a href="javascript:void(0)" onclick="removeItemFromShoppingCart(\'' + key + '\')">' +
                '<i class="fa fa-trash-o"></i>' +
                '</a>' +
                '</div>' +
                '</div>';
        }
        $("#cartContent").html(body);
    })
}

function finishOrder() {
    var formData = {
        first_name: $('input[name=first_name]').val(),
        last_name: $('input[name=last_name]').val(),
        email: $('input[name=email]').val(),
        phone: $('input[name=phone]').val(),
        company: $('input[name=company]').val(),
        address: $('input[name=address]').val(),
        city: $('input[name=city]').val(),
        note: $('textarea[name=note]').val(),
        country: $('select[name=country] option').filter(':selected').val(),
        payment_type: $('input[type=radio][name=payment_type]:checked').val(),
    };

    if (!formData['first_name'] || formData['first_name'] == ''
        || !formData['last_name'] || formData['last_name'] == ''
        || !formData['email'] || formData['email'] == ''
        || !formData['phone'] || formData['phone'] == ''
        || !formData['address'] || formData['address'] == ''
        || !formData['city'] || formData['city'] == '') {
        alert('შეავსეთ სავალდებულო ველები');
        return;
    }

    loadingOn();
    $.post($("#url").val() + '/finishOrder', formData).done(function (result) {
        loadingOff();
        var response = JSON.parse(result);
        if (response.success) {
            if (formData.payment_type == 4) {
                const form = document.createElement('form');
                form.method = "POST";
                form.action = response.data[0];

                const hiddenField = document.createElement('input');
                hiddenField.type = 'hidden';
                hiddenField.name = 'credoinstallment';
                hiddenField.value = response.data[1];

                form.appendChild(hiddenField);

                document.body.appendChild(form);
                form.submit();
            } else {
                if (response.data[0]) {
                    window.open(response.data[0], "_self");
                } else {
                    window.open($("#url").val(), "_self");
                }
            }
        } else {
            alert('დაფიქსირდა შეცდომა')
        }
    })
}
