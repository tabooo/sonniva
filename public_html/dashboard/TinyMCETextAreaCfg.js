if (typeof console.log == "object" && Function.prototype.bind && console) {
    ["log", "info", "warn", "error", "assert", "dir", "clear", "profile", "profileEnd"]
        .forEach(function (method) {
            console[method] = this.call(console[method], console);
        }, Function.prototype.bind);
}

/*function show_dirties(form) {
 var dirties = "";

 form.getFields().each(function (item, index, length) {
 if (item.isDirty()) {
 dirties += item.name + '(' + item.getXType() + '): ' + +' - current[' + escape(item.getValue()) + '] old[' + escape(item.originalValue) + ']\n';
 }
 });

 if (dirties == "") dirties = "No dirty fields";

 alert(dirties);
 }*/

var defaultFontsFormats =
    'AcadMtavr=bpg_nino_mtavruli_normal;' +
    'Andale Mono=andale mono,times;' +
    'Arial=arial,helvetica,sans-serif;' +
    'Arial Black=arial black,avant garde;' +
    'Book Antiqua=book antiqua,palatino;' +
    'Comic Sans MS=comic sans ms,sans-serif;' +
    'Courier New=courier new,courier;' +
    'Georgia=georgia,palatino;' +
    'Helvetica=helvetica;' +
    'Impact=impact,chicago;' +
    'Symbol=symbol;' +
    'Tahoma=tahoma,arial,helvetica,sans-serif;' +
    'Terminal=terminal,monaco;' +
    'Times New Roman=times new roman,times;' +
    'Trebuchet MS=trebuchet ms,geneva;' +
    'Verdana=verdana,geneva;' +
    'Webdings=webdings;' +
    'Wingdings=wingdings,zapf dingbats';

var tinyCfg1 = {
    theme: 'modern',
    plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern'
    ],

    //imagetools

    toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
    toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | inserttime preview | forecolor backcolor",
    toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft",

    content_css: [
        '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css'
        //'//www.tinymce.com/css/codepen.min.css'
    ],

    style_formats_merge: true,

    style_formats: [
        {title: 'Image Baseline', selector: 'img', styles: {'vertical-align': 'baseline'}},
        {title: 'Image Top', selector: 'img', styles: {'vertical-align': 'top'}},
        {title: 'Image Middle', selector: 'img', styles: {'vertical-align': 'middle'}},
        {title: 'Image Bottom', selector: 'img', styles: {'vertical-align': 'bottom'}},
        {title: 'Image Text Top', selector: 'img', styles: {'vertical-align': 'text-top'}},
        {title: 'Image Text Bottom', selector: 'img', styles: {'vertical-align': 'text-bottom'}},
        {title: 'Image Size', selector: 'img', styles: {'margin': '3px', 'width': '220px'}},
        {title: 'Image Left', selector: 'img', styles: {'float': 'left', 'margin': '0 10px 0 10px'}},
        {title: 'Image Right', selector: 'img', styles: {'float': 'right', 'margin': '0 10px 0 10px'}},
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}}
    ],

    menubar: true,
    image_advtab: true,
    relative_urls: true,
    toolbar_items_size: 'small',
    font_formats: defaultFontsFormats,
    file_browser_callback: myFileBrowser,
    document_base_url: 'public_html/file_manager/'
};

var tinyCfg2 = tinyCfg1;

function myFileBrowser(field_name, url, type, win) {
    var winList = win.top.tinymce.activeEditor.windowManager.windows;
    var winId = win.top.tinymce.activeEditor.windowManager.windows[winList.length - 1]._id;
    var winRef = document.getElementById(winId);

    document.getElementById("mce-modal-block").style.visibility = "hidden";
    winRef.style.display = "none";

    var extWindow = Ext.create('Dashboard.view.chooser.FileManagerWindow', {
        listeners: {
            onCancel: closeWin,
            selected: function (selected) {
                closeWin();
                selected = selected[0];

                var url = Dashboard.util.Url.PUBLIC + selected.data.img_path; // TODO
                win.document.getElementById(field_name).value = url;
            }
        }
    });

    return false;

    function closeWin() {
        Ext.destroy(extWindow);
        document.getElementById("mce-modal-block").style.visibility = "visible";
        winRef.style.display = "block";
    }
}

/*function modifyElement(html) {
    var content = html;
    var parser = new DOMParser();
    var htmlDoc = parser.parseFromString(content, "text/html");

    var nodesToWrap = htmlDoc.getElementsByTagName("img");
    for (var index = 0; index < nodesToWrap.length; index++) {
        var node = nodesToWrap[index];

        if (node.parentNode.tagName != 'A') {
            var wrapper = document.createElement("a");
            wrapper.rel = "prettyPhoto[pp_gal]";
            wrapper.setAttribute('href', node.attributes.src.value);

            var div = document.createElement("div");
            div.className = "";

            node.parentNode.insertBefore(wrapper, node);
            node.parentNode.removeChild(node);
            wrapper.appendChild(node);

            wrapper.parentNode.insertBefore(div, wrapper);
            wrapper.parentNode.removeChild(wrapper);
            div.appendChild(wrapper);
        }
    }

    return htmlDoc.activeElement.innerHTML;
}*/

function modifyElement(html) {
    var content = html;
    var parser = new DOMParser();
    var htmlDoc = parser.parseFromString(content, "text/html");

    var nodesToWrap = htmlDoc.getElementsByTagName("img");
    for (var index = 0; index < nodesToWrap.length; index++) {
        var node = nodesToWrap[index];

        if (node.parentNode.tagName != 'A') {
            var wrapper = document.createElement("a");
            wrapper.rel = "prettyPhoto[pp_gal]";
            wrapper.setAttribute('href', node.attributes.src.value);

            node.parentNode.insertBefore(wrapper, node);
            node.parentNode.removeChild(node);
            wrapper.appendChild(node);
        }
    }

    return htmlDoc.activeElement.innerHTML;
}
