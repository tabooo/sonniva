#!/bin/bash
for i in *.svg
do
    name=`basename $i .svg`
    convert -background transparent $name.svg $name.png
done
