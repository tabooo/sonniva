function paddy(n, p, c) {
	var pad_char = typeof c !== 'undefined' ? c : '0';
	var pad = new Array(1 + p).join(pad_char);
	return (pad + n).slice(-pad.length);
}

function arrayFromObject(obj) {
	var arr = [];
	for ( var i in obj) {
		arr.push(obj[i]);
	}
	return arr;
}

function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	var expires = "expires=" + d.toUTCString();
	document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ')
			c = c.substring(1);
		if (c.indexOf(name) == 0)
			return c.substring(name.length, c.length);
	}
	return "";
}

function checkCookie() {
	var user = getCookie("username");
	if (user != "") {
		alert("Welcome again " + user);
	} else {
		user = prompt("Please enter your name:", "");
		if (user != "" && user != null) {
			setCookie("username", user, 365);
		}
	}
}

function download(text, name, type) {
	var a = document.createElement("a");
	var file = new Blob([ text ], {
		type : type
	});
	a.href = URL.createObjectURL(file);
	a.download = name;
	a.click();
}

function timeToDate4(time) {
	var d = new Date(time);
	return d.getFullYear() + "-" + paddy((d.getMonth() + 1), 2) + "-" + paddy(d.getDate(), 2);
}