/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */

function loadLocale() {
    Dashboard.statical.Config.setSessionData(Ext.decode(localStorage.getItem('sessionData'), true));
}

loadLocale();

Ext.define('Dashboard.Application', {
    extend: 'Ext.app.Application',
    name: 'Dashboard',
    mainView: "Dashboard.view.main.Main",

    stores: [
        "NavigationTree"
    ],

    requires: [
        'Dashboard.util.Util',
        'Dashboard.statical.Config',
        'Dashboard.store.JsonAjaxProxy'
    ],

    init: function () {
        Ext.Ajax.on('requestexception', function (conn, response, options, eOpts) {
            if (response.status == 401) {
                location.href = window.location.origin + '/admin';
            }
        });

        var that = this;
        Ext.Ajax.request({
            method: 'get',
            url: '../api/admin/getLanguage?lang=' + (getCookie("lang") != "" ? getCookie("lang") : 'ka'),
            withCredentials: true,
            async: false,
            success: function (result) {
                that.Language = JSON.parse(result.responseText);
                that.requires = ['Dashboard.*'];
            }
        });
    },

    launch: function () {
        Ext.tip.QuickTipManager.init();
    },

    onAppUpdate: function () {

    },

    api: Ext.create('Dashboard.component.RestAPI'),
    Language: {}
});
