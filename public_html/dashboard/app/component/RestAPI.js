Ext.define('Dashboard.component.RestAPI', {
	constructor : function(config) {

		function post(method) {

			method = disableCache(method);

			var params, loadCallback, errorCallback;

			if (typeof arguments[1] == 'function') {
				loadCallback = arguments[1]
				errorCallback = arguments[2]
			} else {
				params = arguments[1]
				loadCallback = arguments[2]
				errorCallback = arguments[3]
			}

			var reloadArgs = arguments

			Ext.Ajax.request({
				method : 'post',
				url : base + method,
				withCredentials : true,
				params : params,
				callback : function(a, success, response) {

					setTimeout(function() {
						var responseObject = parseJson(response.responseText)
						if (response.status == 200) {
							loadCallback(responseObject, response)
						} else {
							if (errorListener) {
								errorListener(responseObject, function() {
									post.apply(that, reloadArgs)
								})
							}
							if (errorCallback) {
								errorCallback(responseObject)
							} else {
								Ext.Msg.alert(Dashboard.app.Language.ERROR, responseObject.description ? responseObject.description : responseObject);
							}
						}
					}, 0);

				}
			})

		}

		function get(method) {

			method = disableCache(method);

			var params, loadCallback, errorCallback;

			if (typeof arguments[1] == 'function') {
				loadCallback = arguments[1]
				errorCallback = arguments[2]
			} else {
				params = arguments[1]
				loadCallback = arguments[2]
				errorCallback = arguments[3]
			}

			var reloadArgs = arguments

			Ext.Ajax.request({
				method : 'get',
				url : base + method,
				withCredentials : true,
				params : params,
				callback : function(a, success, response) {

					setTimeout(function() {
						var responseObject = parseJson(response.responseText)
						if (response.status == 200) {
							loadCallback(responseObject, response)
						} else {
							if (errorListener) {
								errorListener(responseObject, function() {
									post.apply(that, reloadArgs)
								})
							}
							if (errorCallback) {
								errorCallback(responseObject)
							}
						}
					}, 0);

				}
			})

		}

		function parseJson(text) {
			try {
				return Ext.decode(text)
			} catch (e) {
				console.log(e);
			}
		}

		function disableCache(method) {
			if (method.indexOf("?") != -1) {
				method = method + '&time=' + new Date().getTime();
			} else {
				method = method + '?time=' + new Date().getTime();
			}

			return method;
		}

		function postObject(method) {

			method = disableCache(method);

			var params, loadCallback
			if (typeof arguments[1] == 'function') {
				loadCallback = arguments[1]
				errorCallback = arguments[2]
			} else {
				params = arguments[1]
				loadCallback = arguments[2]
				errorCallback = arguments[3]
			}

			var reloadArgs = arguments

			Ext.Ajax.request({
				url : base + method,
				method : 'post',
				withCredentials : true,
				jsonData : params,
				callback : function(a, success, response) {
					setTimeout(function() {
						var responseObject = parseJson(response.responseText)
						if (response.status == 200) {
							loadCallback(responseObject, response)
						} else {
							if (errorListener) {
								errorListener(responseObject, function() {
									postObject.apply(that, reloadArgs)
								})
							}
							if (errorCallback) {
								errorCallback(responseObject ? responseObject : response)
							} else {
								Ext.Msg.alert(Dashboard.app.Language.ERROR, responseObject ? (responseObject.description ? responseObject.description
										: responseObject) : response.statusText);
							}
						}
					}, 0)
				}
			})

		}

		var base = ''
		if (config && config.base) {
			base = config.base
		}

		var errorListener

		var that = this
		that.get = get
		that.post = post
		that.postObject = postObject
		that.onError = function(listener) {
			errorListener = listener
		}

		that.callParent(arguments);

		that.getBase = function() {
			return base;
		}
	}
});
