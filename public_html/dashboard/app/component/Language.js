Ext.define('Dashboard.component.Language', {
    Language: {},
    constructor: function (config) {
        var that = this;
        Ext.create('Dashboard.component.RestAPI').get('../api/admin/getLanguage', function (result) {
            ///console.log(result)
            that.Language = result;
        });
        that.callParent(arguments);
    }
});
