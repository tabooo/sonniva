Ext.define("Dashboard.view.user_tree.UserTree", {
    extend: 'Ext.container.Container',
    //anchor: "100% -1",
    margin: 10,
    title: 'მომხმარებლების სქემა',
    closable: true,
    layout: {
        type: "vbox",
        pack: "center",
        align: "center"
    },
    constructor: function (config) {
        var that = this;

        var selecteWindow = config.selectWindow;

        var libs = 1;
        var libsLoaded = false;

        var userTreeStore = Ext.create('Dashboard.store.UserTree');

        var tree = Ext.create('Ext.tree.Panel', {
            store: userTreeStore,
            //rootVisible: false,
            width: '100%',
            flex: 1,
            autoScroll: true,
            scrollable: true,
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: [{
                    xtype: 'button',
                    text: 'განახლება',
                    iconCls: 'fa fa-refresh fa-lg',
                    handler: function () {
                        search();
                    }
                }]
            }],
            columns: [{
                xtype: 'treecolumn',
                text: 'მომხმარებელი',
                dataIndex: 'user',
                textAlign: 'left',
                flex: 1,
                renderer: function (val) {
                    return val.name + " " + val.last_name;
                }
            }, {
                text: 'რეფერალ კოდი',
                dataIndex: 'user_tree_id',
                textAlign: 'left',
                flex: 1
            }],
            listeners: {
                rowdblclick: function (me, record, tr, rowIndex, e, eOpts) {
                    if (record.data.menu_id) {
                        var window = Ext.create('Dashboard.view.menus.MenuItem', {
                            menuItem: record.data.menu,
                            listeners: {
                                added: function (item) {
                                    reLoadLibs();
                                    window.close();
                                }
                            }
                        }).show();
                    }
                    return false;
                },
                itemexpand: function (node) {
                    expandNode(node)
                }
            },
            buttons: selecteWindow ? [{
                text: 'არჩევა',
                iconCls: "x-fa fa-plus-circle",
                handler: function () {
                    var selected = tree.getSelectionModel().getSelection();
                    if (selected[0].data.menu.editable == 0) {
                        Ext.Msg.alert(Dashboard.app.Language.NOTICE, 'ამ მენიუს რედაქტირების უფლება არ გაქვთ');
                        return;
                    }
                    that.fireEvent('select', selected[0].data);
                }
            }] : null
        });

        tree.on('beforeitemcontextmenu', function (th, record, item, index, evt, eOpts) {
            var item;
            var items = [];
            var rows = tree.getSelectionModel().getSelection();

            if (rows.length == 1) {
                var menu = tree.getSelectionModel().getSelection()[0];

                item = Ext.create('Ext.menu.Item', {
                    text: Dashboard.app.Language.EDIT,
                    iconCls: "x-fa fa-edit",
                    listeners: {
                        click: function (itm) {
                            var window = Ext.create('Dashboard.pages.menus.MenuItem', {
                                menuItem: menu.data.menu,
                                listeners: {
                                    added: function (item) {
                                        reLoadLibs();
                                        window.close();
                                    }
                                }
                            }).show();
                        }
                    }
                });

                items.push(item);
            }

            var contextMenu = Ext.create('Ext.menu.Menu', {
                width: 222,
                //title : 'ოპერაციები',
                items: items
            });

            evt.stopEvent();
            contextMenu.showAt(evt.getXY());
        });

        that.items = [tree];

        that.callParent(arguments);

        function expandNode(node) {
            if (node.data.childrens < 1) {
                return;
            }
            Dashboard.app.api.get('../api/admin/getPublicUsersTree?parent_id=' + node.data.user_tree_id, function (result) {
                node.removeAll();
                result = categoriesForTreeCombo(result);
                Ext.each(result, function (item) {
                    node.appendChild(item)
                });
            });
        }

        function categoriesForTreeCombo(data) {
            var items = [];
            for (var i = 0; i < data.length; i++) {
                var item = {
                    text: data[i].name + " " + data[i].last_name,
                    user_tree_id: data[i].user_tree_id,
                    user_id: data[i].user_id,
                    parent_id: data[i].parent_id,
                    user: data[i].user,
                    link: data[i].link,
                    expanded: false,
                    menu: data[i]
                    //leaf: 'true'
                };
                /*if (data[i].children.length > 0) {
                    item.children = categoriesForTreeCombo(data[i].children)
                }*/
                items.push(item);
            }
            return items;
        }

        function reloadLib() {
            libs--;
            if (libs == 0) {
                libsLoaded = true;
                search();
            }
        }

        function reLoadLibs() {
            if (libsLoaded) {
                search();
                return;
            }
            reloadLib();
        }

        function search() {
            Dashboard.app.api.get('../api/admin/getPublicUsersTree', function (result) {
                result = categoriesForTreeCombo(result);
                result = {
                    text: '.',
                    children: result,
                    expanded: true
                };
                userTreeStore.setRootNode(result);
            });
        }

        reLoadLibs();
    }
});