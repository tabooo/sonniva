Ext.define("Dashboard.view.pages.PageItem", {
    extend: 'Dashboard.view.shell.FitWindow',
    title: 'გვერდის დამატება',
    layout: 'fit',
    border: false,
    modal: true,
    maximizable: true,
    constructor: function (config) {
        var that = this,
            record = (config.record) ? config.record.data : null;

        var libs = 1;
        var labelWidth = 80;

        var tinyPanelGe = Ext.widget("tinymce_textarea", {
            xtype: 'tinymce_textarea',
            minHeight: 350,
            flex: 1,
            fieldStyle: 'font-family: Courier New; font-size: 12px;',
            style: {border: '0'},
            tinyMCEConfig: tinyCfg1,
            name: 'description_ge',
            emptyText: 'ქართულად'
        });

        var tinyPanelEn = Ext.widget("tinymce_textarea", {
            xtype: 'tinymce_textarea',
            minHeight: 350,
            flex: 1,
            fieldStyle: 'font-family: Courier New; font-size: 12px;',
            style: {border: '0'},
            tinyMCEConfig: tinyCfg2,
            name: 'description_en',
            emptyText: 'English Text'
        });

        var tinyPanelRu = Ext.widget("tinymce_textarea", {
            xtype: 'tinymce_textarea',
            minHeight: 350,
            flex: 1,
            fieldStyle: 'font-family: Courier New; font-size: 12px;',
            style: {border: '0'},
            tinyMCEConfig: tinyCfg1,
            name: 'description_ru',
            emptyText: 'Руский'
        });

        var disabledStore = Ext.create('Dashboard.store.Disabled');

        var disabledCombo = Ext.create('Ext.form.field.ComboBox', {
            ctype: 'combobox',
            store: disabledStore,
            queryMode: 'local',
            labelWidth: labelWidth,
            labelAlign: 'right',
            fieldLabel: 'დამალული',
            name: 'disabled',
            displayField: 'disabled_name',
            valueField: 'disabled_id',
            allowBlank: false,
            editable: false
        });

        disabledCombo.setValue(0);

        /*var imageView = Ext.create('Dashboard.view.pages.component.ImageView', {
         record: record
         });*/

        var keywordsGE = getKeywordTextField('keyword_ge');
        var keywordsEN = getKeywordTextField('keyword_en');
        var keywordsRU = getKeywordTextField('keyword_ru');

        var googleDescriptionGE = getGoogleDescTextField('google_description_ge');
        var googleDescriptionEN = getGoogleDescTextField('google_description_en');
        var googleDescriptionRU = getGoogleDescTextField('google_description_ru');

        var form = Ext.create("Ext.form.Panel", {
            border: false,
            scrollable: true,
            flex: 1,
            padding: '10',
            layout: {
                type: 'vbox'
            },
            defaults: {
                width: '100%',
                labelWidth: labelWidth,
                labelAlign: 'right'
            },
            defaultType: 'textfield',

            items: [{
                fieldLabel: 'ლინკი',
                name: 'link',
                readOnly: true
            }, disabledCombo, {
                xtype: 'tabpanel',
                flex: 1,
                items: [
                    {
                        layout: 'vbox',
                        title: 'ქართული',
                        defaults: {
                            width: '100%',
                            labelWidth: labelWidth,
                            labelAlign: 'right',
                            xtype: 'textfield'
                        },
                        items: [{
                            fieldLabel: 'დასახელება',
                            name: 'title_ge'
                        }, keywordsGE, googleDescriptionGE, tinyPanelGe]
                    }, {
                        layout: 'vbox',
                        title: 'ინგლისური',
                        defaults: {
                            width: '100%',
                            labelWidth: labelWidth,
                            labelAlign: 'right',
                            xtype: 'textfield'
                        },
                        items: [{
                            width: '100%',
                            fieldLabel: 'დასახელება',
                            name: 'title_en'
                        }, keywordsEN, googleDescriptionEN, tinyPanelEn]
                    }, {
                        layout: 'vbox',
                        title: 'რუსული',
                        defaults: {
                            width: '100%',
                            labelWidth: labelWidth,
                            labelAlign: 'right',
                            xtype: 'textfield'
                        },
                        items: [{
                            width: '100%',
                            fieldLabel: 'დასახელება',
                            name: 'title_ru'
                        }, keywordsRU, googleDescriptionRU, tinyPanelRu]
                    }/*, {
                     xtype: 'panel',
                     title: 'Gallery',
                     margin: '5px 0 0 0',
                     scrollable: true,
                     dockedItems: [{
                     xtype: 'toolbar',
                     dock: 'top',
                     items: [{
                     xtype: 'button',
                     text: 'Add Gallery Imgage',
                     iconCls: 'fa fa-camera-retro fa-lg',
                     listeners: {
                     click: function () {
                     var extWindow = Ext.create('Dashboard.view.chooser.FileManagerWindow', {
                     multiSelect: true,
                     listeners: {
                     onCancel: function () {
                     extWindow.close();
                     },
                     selected: function (selected) {
                     imageView.addFiles(selected);
                     extWindow.close();
                     }
                     }
                     });
                     }
                     }
                     }]
                     }],
                     layout: {
                     type: 'vbox',
                     align: 'stretch'
                     },
                     items: [imageView]
                     }*/
                ]
            }
            ]
        });

        that.items = [form];

        that.buttons = [{
            text: 'შენახვა',
            handler: function () {
                var request = config.pageItem ? config.pageItem : {};
                var formData = form.getForm().getFieldValues();

                request.title_ge = formData.title_ge;
                request.title_en = formData.title_en;
                request.title_ru = formData.title_ru;

                request.keyword_ge = formData.keyword_ge;
                request.keyword_en = formData.keyword_en;
                request.keyword_ru = formData.keyword_ru;

                request.google_description_ge = formData.google_description_ge;
                request.google_description_en = formData.google_description_en;
                request.google_description_ru = formData.google_description_ru;

                request.description_ge = modifyElement(formData.description_ge);
                request.description_en = modifyElement(formData.description_en);
                request.description_ru = modifyElement(formData.description_ru);
                request.disabled = formData.disabled;
                request.img_path = "";


                //var imageViewStore = imageView.getStore().getRange();
                var fileManagerList = [];

                /*Ext.Array.each(imageViewStore, function (rec) {

                 if (rec.data.added) {
                 fileManagerList.push({
                 file_manager_id: rec.data.file_manager_id,
                 fileName: rec.data.record.fileName,
                 folder_id: rec.data.record.folder_id,
                 path: rec.data.record.img_path
                 });
                 }
                 });*/

                request.gallery = fileManagerList;

                Dashboard.app.api.postObject('../api/admin/pages/add', Ext.JSON.encode(request), function (result) {
                    if (result.success) {
                        that.setTitle('page/' + result.data[0].post_id + " - " + result.data[0].title_ge);
                        that.fireEvent("added", result.data[0]);
                    } else {
                        Ext.Msg.alert(Dashboard.app.Language.NOTICE, result.message)
                    }
                });

            }
        }];

        that.callParent(arguments);

        /*imageView.on('removeImage', function (rec) {
         imageView.removeImage(rec);
         });*/

        function getKeywordTextField(name) {
            return Ext.create('Ext.form.field.Text', {
                name: name,
                fieldLabel: 'Keywords'
            });
        }

        function getGoogleDescTextField(name) {
            return Ext.create('Ext.form.field.Text', {
                name: name,
                fieldLabel: 'Google Description'
            });
        }

        function loadData() {
            if (config.pageItem) {
                form.getForm().setValues(config.pageItem);
            }
        }

        function reloadLib() {
            libs--;
            if (libs == 0) {
                loadData();
            }
        }

        function reLoadLibs() {
            reloadLib();
        }

        /*function loadNewsGalleryImages() {
         if (record) {
         imageView.loadNewsGalleryImages(record.post_id, 2);
         }
         }*/

        //loadNewsGalleryImages();
        reLoadLibs();
    }
});