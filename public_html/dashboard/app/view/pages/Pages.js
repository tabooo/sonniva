Ext.define("Dashboard.view.pages.Pages", {
    extend: 'Ext.container.Container',
    //anchor: "100% -1",
    margin: 10,
    title: 'გვერდები',
    closable: true,
    layout: {
        type: "vbox",
        pack: "center"
    },
    constructor: function (config) {
        var that = this;

        var libs = 1;
        var libsLoaded = false;

        var categories = [];

        var limit = 50, start = 0;
        var lastSearchData = {
            start: start,
            limit: limit
        };

        var searchIdField = Ext.create('Ext.form.field.Text', {
            name: 'pageId',
            emptyText: 'ID',
            width: 80,
            enableKeyEvents: true,
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == e.ENTER) {
                        var form = field.up('form').getForm();
                        search();
                    }
                }
            }
        });

        var searchNameField = Ext.create('Ext.form.field.Text', {
            name: 'searchName',
            emptyText: 'ძებნა სიტყვით',
            width: 200,
            enableKeyEvents: true,
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == e.ENTER) {
                        var form = field.up('form').getForm();
                        search();
                    }
                }
            }
        });

        var searchBtn = Ext.create('Ext.button.Button', {
            iconCls: 'x-fa fa-search',
            text: 'ძებნა',
            handler: function () {
                search();
            }
        });

        var addBtn = Ext.create('Ext.button.Button', {
            iconCls: 'x-fa fa-plus-circle',
            text: 'გვერდის დამატება',
            handler: function () {
                var window = Ext.create('Dashboard.view.pages.PageItem', {
                    categories: categories,
                    listeners: {
                        added: function (item) {
                            reLoadLibs();
                            window.close();
                        }
                    }
                }).show();
            }
        });

        var form = Ext.create('Ext.form.Panel', {
            border: false,
            align: 'left',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [searchIdField, searchNameField, searchBtn]
        });

        var store = Ext.create('Dashboard.store.Posts', {
            pageSize: limit,
            autoLoad: false,
            proxy: {
                type: 'ajax',
                actionMethods: {
                    read: 'GET'
                },
                headers: {
                    'Content-Type': 'application/json'
                },
                jsonData: true,
                url: '../api/admin/pages',
                limitParam: false,
                startParam: false,
                pageParam: false,
                extraParams: lastSearchData,
                paramsAsJson: true,
                reader: {
                    type: 'json',
                    totalProperty: 'total',
                    rootProperty: 'data'
                }
            }
        });

        var pagingToolbar = Ext.create('Ext.toolbar.Paging', {
            dock: 'bottom',
            displayInfo: true,
            store: store,
            listeners: {
                beforechange: function (a, pageNumber) {
                    lastSearchData.start = (pageNumber - 1) * limit;
                }
            }
        });

        var grid = Ext.create('Ext.grid.Panel', {
            store: store,
            width: '100%',
            flex: 1,
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: [{
                    xtype: 'button',
                    text: 'განახლება',
                    iconCls: 'fa fa-refresh fa-lg',
                    handler: function () {
                        search();
                    }
                }, addBtn]
            }, pagingToolbar],
            columns: [{
                header: "ლინკი",
                dataIndex: 'link',
                width: 90
            }, {
                header: "დასახელება ქართ.",
                dataIndex: 'title_ge',
                flex: 1
            }, {
                header: "მენიუ",
                dataIndex: 'menu',
                width: 150,
                renderer: function (a) {
                    if (a)
                        return a.name_ge;
                }
            }, {
                //header : myjs.lang.ACTION,
                width: 50,
                xtype: 'actioncolumn',
                editor: {
                    xtype: 'button'
                },
                items: [{
                    iconCls: 'x-fa fa-trash',
                    handler: function (grid, rowIndex, colIndex) {

                        var selectedRecord = store.getAt(rowIndex);
                        if (selectedRecord.data.page_type_id == 1) {
                            return;
                        }

                        var msgBox = Ext.create('Ext.window.MessageBox', {
                            buttonText: {
                                yes: Dashboard.app.Language.YES,
                                no: Dashboard.app.Language.NO
                            }
                        });

                        msgBox.confirm(Dashboard.app.Language.MARK_AS_REGISTERED, '<b>' + Dashboard.app.Language.ASK_DELETE + '</b>', function (answer) {
                            if (answer == 'yes') {
                                Dashboard.app.api.get('../api/admin/pages/destroy/' + selectedRecord.data.post_id, function (result) {
                                    if (result.success) {
                                        store.remove(selectedRecord);
                                    } else {
                                        Ext.Msg.alert(Dashboard.app.Language.NOTICE, result.message)
                                    }
                                });
                            }
                        });
                    }
                }]
            }],
            listeners: {
                itemdblclick: function (data, record, item, index, e, eOpts) {

                    var pageItemPanel = Ext.create('Dashboard.view.pages.PageItem', {
                        pageItem: record.data,
                        record: record,
                        title: "page/" + record.data.post_id + " - " + record.data.title_ge,
                        categories: categories,
                        listeners: {
                            added: function (item) {
                                this.close();
                                search();
                            }
                        }
                    }).show();

                }
            }
        });

        that.items = [form, grid];

        that.callParent(arguments);

        function reloadLib() {
            libs--;
            if (libs == 0) {
                search();
                libsLoaded = true;
            }
        }

        function reLoadLibs() {
            if (libsLoaded) {
                search();
                return;
            }

            reloadLib();
        }

        function search() {
            var formData = form.getForm().getFieldValues();
            lastSearchData = {
                searchName: formData.searchName,
                post_id: formData.post_id,
                //dateFrom: startDateField.getValue() == null ? null : startDateField.getValue().getTime(),
                //dateTo: endDateField.getValue() == null ? null : endDateField.getValue().getTime(),
                start: start,
                limit: limit
            };
            store.proxy.extraParams = lastSearchData;
            store.loadPage(1);
        }

        reLoadLibs();
    }
});