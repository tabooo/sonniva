Ext.define('Dashboard.view.category.Category', {
    extend: 'Ext.window.Window',
    title: Dashboard.app.Language.EDIT_CATEGORY,
    autoShow: true,
    modal: true,
    width: 400,
    padding: 5,
    border: false,
    constructor: function (config) {
        var that = this;

        var category = config.category;

        var parentCat = config.parentCat;

        var parentNameField = Ext.create('Ext.form.field.Text', {
            labelWidth: 100,
            width: '100%',
            xtype: 'textfield',
            name: 'parent_name',
            fieldLabel: Dashboard.app.Language.PARENT,
            readOnly: true
        });

        var nameField = Ext.create('Ext.form.field.Text', {
            labelWidth: 100,
            width: '100%',
            xtype: 'textfield',
            name: 'category_name',
            fieldLabel: Dashboard.app.Language.NAME1,
            allowBlank: false
        });

        var addButton = Ext.create('Ext.button.Button', {
            iconCls: 'x-fa fa-save',
            text: Dashboard.app.Language.SAVE,
            handler: function () {

                var request = {
                    category_id: category ? category.category_id : null,
                    category_name: nameField.getValue(),
                    parent_id: parentCat ? parentCat.category_id : null,
                };

                Dashboard.app.api.postObject('../api/admin/addCategory', request, function (result) {
                    that.fireEvent('added');
                });
            }
        });

        that.items = [parentNameField, nameField];

        that.buttons = [addButton];

        that.callParent(arguments);

        if (category) {
            nameField.setValue(category.category_name);
        }

        if (parentCat) {
            parentNameField.setValue(parentCat.category_name);
        }

    }
});
