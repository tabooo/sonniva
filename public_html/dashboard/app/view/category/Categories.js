Ext.define("Dashboard.view.category.Categories", {
    extend: 'Ext.container.Container',
    //anchor: "100% -1",
    margin: 10,
    title: 'მენიუ',
    closable: true,
    layout: {
        type: "vbox",
        pack: "center",
        align: "center"
    },
    constructor: function (config) {
        var that = this;

        var selectWindow = config.selectWindow;

        var libs = 1;
        var libsLoaded = false;

        var addCategoryButton = Ext.create('Ext.Button', {
            text: Dashboard.app.Language.NEW_CATEGORY,
            iconCls: 'x-fa fa-plus-circle',
            hidden: selectWindow,
            handler: function () {
                var selected = tree.getSelectionModel().getSelection();
                if (!selected[0]) {
                    return;
                }
                editCategory(null, selected[0].data.category);
            }
        });

        var categoryTreeStore = Ext.create('Dashboard.store.CategoriesTree');

        var tree = Ext.create('Ext.tree.Panel', {
            store: categoryTreeStore,
            rootVisible: false,
            width: '100%',
            flex: 1,
            autoScroll: true,
            scrollable: true,
            viewConfig: {
                plugins: {
                    ptype: 'gridviewdragdrop',
                    dragText: 'Drag and drop to reorganize'
                }
            },
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: [addCategoryButton, {
                    xtype: 'button',
                    text: 'განახლება',
                    iconCls: 'fa fa-refresh fa-lg',
                    handler: function () {
                        search();
                    }
                }]
            }],
            columns: [{
                xtype: 'treecolumn',
                text: 'დასახელება',
                dataIndex: 'category_name',
                textAlign: 'left',
                flex: 1
            }, {
                header: '',
                width: 25,
                xtype: 'actioncolumn',
                editor: {
                    xtype: 'button'
                },
                items: [{
                    iconCls: 'x-fa fa-arrow-up',
                    isDisabled: function (grid, rowIndex, colIndex, items, rec) {
                        //if (rec.data.creatorProfileId !== curUser.user.employeeProfileId) {
                        //    return true;
                        //}
                        return false;
                    },
                    handler: function (grid, rowIndex, colIndex) {
                        var selectedRecord = categoryTreeStore.getAt(rowIndex);
                        Dashboard.app.api.post('../api/admin/categories/changeOrder/' + selectedRecord.data.category_id + '/up', function (result) {
                            reLoadLibs();
                        });
                    }
                }]
            }, {
                header: '',
                width: 25,
                xtype: 'actioncolumn',
                editor: {
                    xtype: 'button'
                },
                items: [{
                    iconCls: 'x-fa fa-arrow-down',
                    isDisabled: function (grid, rowIndex, colIndex, items, rec) {
                        //if (rec.data.creatorProfileId !== curUser.user.employeeProfileId) {
                        //    return true;
                        //}
                        return false;
                    },
                    handler: function (grid, rowIndex, colIndex) {
                        var selectedRecord = categoryTreeStore.getAt(rowIndex);
                        Dashboard.app.api.post('../api/admin/categories/changeOrder/' + selectedRecord.data.category_id + '/down', function (result) {
                            reLoadLibs();
                        });
                    }
                }]
            }],
            listeners: {
                rowdblclick: function (me, record, tr, rowIndex, e, eOpts) {
                    if (selectWindow) {
                        that.fireEvent('select', record.data);
                    } else {
                        editCategory(record.data, null);
                    }
                },
                drop: function(node, data, overModel, dropPosition, eOpts){
                    console.log(node);
                    console.log(data);
                    console.log(overModel);
                    console.log(dropPosition);
                }
            },
            buttons: selectWindow ? [{
                text: 'არჩევა',
                iconCls: "x-fa fa-check-circle",
                handler: function () {
                    var selected = tree.getSelectionModel().getSelection();
                    that.fireEvent('select', selected[0].data);
                }
            }] : null
        });

        tree.on('beforeitemcontextmenu', function (th, record, item, index, evt, eOpts) {
            var itm;
            var items = [];
            var rows = tree.getSelectionModel().getSelection();

            if (rows.length == 1) {
                var category = tree.getSelectionModel().getSelection()[0];

                itm = Ext.create('Ext.menu.Item', {
                    text: Dashboard.app.Language.EDIT,
                    iconCls: "x-fa fa-edit",
                    listeners: {
                        click: function (itm) {
                            editCategory(category.data, null);
                        }
                    }
                });

                items.push(itm);

                itm = Ext.create('Ext.menu.Item', {
                    text: 'ქვეკატეგორიის დამატება',
                    iconCls: "x-fa fa-plus-circle",
                    listeners: {
                        click: function (itm) {
                            editCategory(null, category.data.category);
                        }
                    }
                });

                items.push(itm);

                itm = Ext.create('Ext.menu.Item', {
                    text: Dashboard.app.Language.DELETE,
                    iconCls: "x-fa fa-trash",
                    listeners: {
                        click: function (itm) {
                            deleteCategory(category.data.category_id);
                        }
                    }
                });

                items.push(itm);
            }

            var contextCategory = Ext.create('Ext.menu.Menu', {
                width: 222,
                items: items
            });

            evt.stopEvent();
            contextCategory.showAt(evt.getXY());
        });

        that.items = [tree];

        that.callParent(arguments);

        function categoriesForTreeCombo(data, parent) {
            var items = [];
            for (var i = 0; i < data.length; i++) {
                var item = {
                    text: data[i].category_name,
                    category_id: data[i].category_id,
                    category_name: data[i].category_name,
                    parent_id: data[i].parent_id,
                    expanded: true,
                    category: data[i],
                    parent: parent,
                    leaf: true
                };
                if (data[i].childrenObjects.length > 0) {
                    item.leaf = false;
                    item.children = categoriesForTreeCombo(data[i].childrenObjects, data[i])
                }
                items.push(item);
            }
            return items;
        }

        function editCategory(category, parent) {
            var window = Ext.create('Dashboard.view.category.Category', {
                category: category,
                parentCat: parent ? parent : category.parent,
                listeners: {
                    added: function (item) {
                        reLoadLibs();
                        window.close();
                    }
                }
            }).show();
        }

        function deleteCategory($categoryId) {
            var msgBox = Ext.create('Ext.window.MessageBox', {
                buttonText: {
                    yes: Dashboard.app.Language.YES,
                    no: Dashboard.app.Language.NO
                }
            });

            msgBox.confirm(Dashboard.app.Language.MARK_AS_REGISTERED, '<b>' + Dashboard.app.Language.ASK_DELETE + '</b>', function (answer) {
                if (answer == 'yes') {
                    Dashboard.app.api.get('../api/admin/removeCategory/' + $categoryId, function (result) {
                        search();
                    });
                }
            });
        }

        function reloadLib() {
            libs--;
            if (libs == 0) {
                libsLoaded = true;
                search();
            }
        }

        function reLoadLibs() {
            if (libsLoaded) {
                search();
                return;
            }
            reloadLib();
        }

        function search() {
            Dashboard.app.api.get('../api/admin/getCategoriesTree', function (result) {
                categoryTreeStore.setRootNode({
                    text: '.',
                    children: categoriesForTreeCombo(result),
                    expanded: true
                });
            });
        }

        reLoadLibs();
    }
});
