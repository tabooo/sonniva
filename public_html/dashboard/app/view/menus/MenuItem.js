Ext.define("Dashboard.view.menus.MenuItem", {
    extend: 'Ext.window.Window',
    layout: 'fit',
    //padding: 5,
    border: false,
    modal: true,
    constructor: function (config) {
        var that = this;
        var libs = 1;

        var labelWidth = 80;

        var disabledStore = Ext.create('Dashboard.store.Disabled');

        var disabledCombo = Ext.create('Ext.form.field.ComboBox', {
            ctype: 'combobox',
            store: disabledStore,
            queryMode: 'local',
            //width : 250,
            labelWidth: labelWidth,
            labelAlign: 'right',
            fieldLabel: 'დამალული',
            name: 'disabled',
            displayField: 'disabled_name',
            valueField: 'disabled_id',
            allowBlank: false,
            editable: false
        });
        disabledCombo.setValue(0);

        var form = Ext.create("Ext.form.Panel", {
            border: false,
            scrollable: true,
            //flex: 1,
            bodyCls: 'x-border-layout-ct',
            cls: 'x-border-layout-ct',
            padding: 10,

            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            defaults: {
                anchor: '100%',
                labelWidth: labelWidth,
                labelAlign: 'right'
            },
            defaultType: 'textfield',

            items: [
                {
                    fieldLabel: 'დასახელება ქართ.',
                    name: 'name_ge'
                }, {
                    fieldLabel: 'დასახელება ინგლ.',
                    name: 'name_en'
                }, {
                    fieldLabel: 'დასახელება რუს.',
                    name: 'name_ru'
                }, {
                    fieldLabel: 'ლინკი',
                    name: 'link'
                }, {
                    fieldLabel: 'URL',
                    name: 'url'
                }, disabledCombo, {
                    xtype: 'hidden',
                    name: 'order'
                }
            ]
        });

        that.items = [form];

        that.buttons = [{
            text: 'შენახვა',
            handler: function () {
                var request = {};
                request.parent_id = config.parent_id ? config.parent_id : null;
                request.order = 1;
                if (config.menuItem) {
                    request = config.menuItem;
                }
                var formData = form.getForm().getFieldValues();

                request.name_ge = formData.name_ge;
                request.name_en = formData.name_en;
                request.name_ru = formData.name_ru;
                request.disabled = formData.disabled;
                request.link = formData.link;
                request.url = formData.url;

                Dashboard.app.api.postObject('../api/admin/menus/add', request, function (result) {
                    if (result.success) {
                        that.fireEvent("added", result.data);
                    } else {
                        Ext.Msg.alert(Dashboard.app.Language.NOTICE, result.message)
                    }
                });
            }
        }];

        that.callParent(arguments);

        if (config.menuItem) {
            reLoadLibs();
        }

        function loadData() {
            form.getForm().setValues(config.menuItem);
        }

        function reloadLib() {
            libs--;
            if (libs == 0) {
                loadData();
            }
        }

        function reLoadLibs() {
            reloadLib();
        }
    }
});