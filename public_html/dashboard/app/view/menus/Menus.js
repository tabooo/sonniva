Ext.define("Dashboard.view.menus.Menus", {
    extend: 'Ext.container.Container',
    //anchor: "100% -1",
    margin: 10,
    title: 'მენიუ',
    closable: true,
    layout: {
        type: "vbox",
        pack: "center",
        align: "center"
    },
    constructor: function (config) {
        var that = this;

        var selecteWindow = config.selectWindow;

        var libs = 1;
        var libsLoaded = false;

        var addMenuButton = Ext.create('Ext.Button', {
            text: 'ახალი მენიუ',
            iconCls: "x-fa fa-plus-circle",
            handler: function () {

                var selected = tree.getSelectionModel().getSelection()

                var window = Ext.create('Dashboard.view.menus.MenuItem', {
                    parent_id: selected[0].data.menu_id,
                    listeners: {
                        added: function (item) {
                            reLoadLibs();
                            window.close();
                        }
                    }
                }).show();
            }
        });

        var menuTreeStore = Ext.create('Dashboard.store.MenuTree');

        var tree = Ext.create('Ext.tree.Panel', {
            store: menuTreeStore,
            //rootVisible: false,
            width: '100%',
            flex: 1,
            autoScroll: true,
            scrollable: true,
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: [{
                    xtype: 'button',
                    text: 'განახლება',
                    iconCls: 'fa fa-refresh fa-lg',
                    handler: function () {
                        search();
                    }
                }]
            }],
            columns: [{
                xtype: 'treecolumn',
                text: 'დასახელება',
                dataIndex: 'name_ge',
                textAlign: 'left',
                flex: 1
            }, {
                text: 'ლინკი',
                dataIndex: 'link',
                width: 150
            }, {
                header: '',
                width: 25,
                xtype: 'actioncolumn',
                editor: {
                    xtype: 'button'
                },
                items: [{
                    iconCls: 'x-fa fa-arrow-up',
                    isDisabled: function (grid, rowIndex, colIndex, items, rec) {
                        //if (rec.data.creatorProfileId !== curUser.user.employeeProfileId) {
                        //    return true;
                        //}
                        return false;
                    },
                    handler: function (grid, rowIndex, colIndex) {
                        var selectedRecord = menuTreeStore.getAt(rowIndex);
                        Dashboard.app.api.post('../api/admin/menus/changeOrder/' + selectedRecord.data.menu_id + '/up', function (result) {
                            reLoadLibs();
                        });
                    }
                }]
            }, {
                header: '',
                width: 25,
                xtype: 'actioncolumn',
                editor: {
                    xtype: 'button'
                },
                items: [{
                    iconCls: 'x-fa fa-arrow-down',
                    isDisabled: function (grid, rowIndex, colIndex, items, rec) {
                        //if (rec.data.creatorProfileId !== curUser.user.employeeProfileId) {
                        //    return true;
                        //}
                        return false;
                    },
                    handler: function (grid, rowIndex, colIndex) {
                        var selectedRecord = menuTreeStore.getAt(rowIndex);
                        Dashboard.app.api.post('../api/admin/menus/changeOrder/' + selectedRecord.data.menu_id + '/down', function (result) {
                            reLoadLibs();
                        });
                    }
                }]
            }],
            listeners: {
                rowdblclick: function (me, record, tr, rowIndex, e, eOpts) {
                    if (record.data.menu_id) {
                        var window = Ext.create('Dashboard.view.menus.MenuItem', {
                            menuItem: record.data.menu,
                            listeners: {
                                added: function (item) {
                                    reLoadLibs();
                                    window.close();
                                }
                            }
                        }).show();
                    }
                    return false;
                }
            },
            buttons: selecteWindow ? [{
                text: 'არჩევა',
                iconCls: "x-fa fa-plus-circle",
                handler: function () {
                    var selected = tree.getSelectionModel().getSelection();
                    if (selected[0].data.menu.editable == 0) {
                        Ext.Msg.alert(Dashboard.app.Language.NOTICE, 'ამ მენიუს რედაქტირების უფლება არ გაქვთ');
                        return;
                    }
                    that.fireEvent('select', selected[0].data);
                }
            }] : null
        });

        tree.on('beforeitemcontextmenu', function (th, record, item, index, evt, eOpts) {
            var item;
            var items = [];
            var rows = tree.getSelectionModel().getSelection();

            if (rows.length == 1) {
                var menu = tree.getSelectionModel().getSelection()[0];

                item = Ext.create('Ext.menu.Item', {
                    text: Dashboard.app.Language.EDIT,
                    iconCls: "x-fa fa-edit",
                    listeners: {
                        click: function (itm) {
                            var window = Ext.create('Dashboard.view.menus.MenuItem', {
                                menuItem: menu.data.menu,
                                listeners: {
                                    added: function (item) {
                                        reLoadLibs();
                                        window.close();
                                    }
                                }
                            }).show();
                        }
                    }
                });

                items.push(item);

                item = Ext.create('Ext.menu.Item', {
                    text: 'ქვემენიუს დამატება',
                    iconCls: "x-fa fa-plus-circle",
                    listeners: {
                        click: function (itm) {
                            editMenu(menu.data.menu ? menu.data.menu.menu_id : null, true);
                        }
                    }
                });

                items.push(item);

                item = Ext.create('Ext.menu.Item', {
                    text: Dashboard.app.Language.DELETE,
                    iconCls: "x-fa fa-trash",
                    listeners: {
                        click: function (itm) {
                            deleteMenu(menu.data.menu_id);
                        }
                    }
                });

                items.push(item);
            }

            var contextMenu = Ext.create('Ext.menu.Menu', {
                width: 222,
                //title : 'ოპერაციები',
                items: items
            });

            evt.stopEvent();
            contextMenu.showAt(evt.getXY());
        });

        that.items = [tree];

        that.callParent(arguments);

        function categoriesForTreeCombo(data) {
            var items = [];
            for (var i = 0; i < data.length; i++) {
                var item = {
                    text: data[i].name_ge,
                    menu_id: data[i].menu_id,
                    name_ge: data[i].name_ge,
                    parent_id: data[i].parent_id,
                    link: data[i].link,
                    expanded: true,
                    menu: data[i],
                    leaf: data[i].children.length > 0 ? 'false' : 'true'
                };
                if (data[i].children.length > 0) {
                    item.children = categoriesForTreeCombo(data[i].children)
                }
                items.push(item);
            }
            return items;
        }

        function editMenu(menu, isNew) {
            var window = Ext.create('Dashboard.view.menus.MenuItem', {
                menuItem: isNew ? null : menu.data.menu,
                parent_id: isNew ? menu : null,
                listeners: {
                    added: function (item) {
                        reLoadLibs();
                        window.close();
                    }
                }
            }).show();
        }

        function deleteMenu(menu_id) {
            var msgBox = Ext.create('Ext.window.MessageBox', {
                buttonText: {
                    yes: Dashboard.app.Language.YES,
                    no: Dashboard.app.Language.NO
                }
            });

            msgBox.confirm(Dashboard.app.Language.MARK_AS_REGISTERED, '<b>' + Dashboard.app.Language.ASK_DELETE + '</b>', function (answer) {
                if (answer == 'yes') {
                    Dashboard.app.api.post('../api/admin/menus/destroy/' + menu_id, function (result) {
                        if (result.success) {
                            search();
                        } else {
                            Ext.Msg.alert(Dashboard.app.Dashboard.app.Language.NOTICE, result.message)
                        }
                    });
                }
            });
        }

        function reloadLib() {
            libs--;
            if (libs == 0) {
                libsLoaded = true;
                search();
            }
        }

        function reLoadLibs() {
            if (libsLoaded) {
                search();
                return;
            }
            reloadLib();
        }

        function search() {
            Dashboard.app.api.get('../api/admin/menu', function (result) {
                result = categoriesForTreeCombo(result);
                result = {
                    text: '.',
                    children: result,
                    expanded: true
                };
                menuTreeStore.setRootNode(result);
            });
        }

        reLoadLibs();
    }
});
