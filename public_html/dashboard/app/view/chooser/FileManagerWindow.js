Ext.define('Dashboard.view.chooser.FileManagerWindow', {
    extend: 'Dashboard.view.shell.FitWindow',
    title: 'File Manager',
    layout: 'fit',
    modal: true,
    autoShow: true,

    height: 600,
    width: 900,

    initComponent: function () {
        var me = this;

        me.items = [{
            xtype: 'container',
            flex: 1,
            id: 'elfinder'
        }];

        me.callParent(arguments);

        me.on('afterrender', function () {
            setTimeout(function () {
                $('#elfinder').elfinder({
                    height: me.height - 45,
                    width: me.width - 5,
                    lang: 'en',
                    url: Dashboard.util.Url.CONNECTOR,
                    getFileCallback: function (file) {
                        me.fireEvent('selected', file);
                    }
                });
            }, 0);
        });


        me.on('close', function () {
            me.fireEvent('onCancel');
        });
    }
});