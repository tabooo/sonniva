Ext.define('Dashboard.view.sells.Sells', {
    extend: 'Ext.panel.Panel',
    border: false,
    flex: 1,
    title: 'გაყიდვები',
    closable: true,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    constructor: function (config) {
        var that = this;

        var libs = 1;
        var libsLoaded = false;

        var limit = 100, start = 0;
        var lastSearchData = {
            start: start,
            limit: limit
        };

        var paymentTypesStore = Ext.create('Dashboard.store.PaymentTypes');

        var form = Ext.create('Ext.form.Panel', {
            border: false,
            buttonAlign: 'center',
            padding: 5,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            defaults: {
                defaults: {
                    xtype: 'textfield',
                    labelWidth: 110,
                    labelAlign: 'left',
                    margin: '5px 5px',
                    enableKeyEvents: true,
                    listeners: {
                        specialkey: function (f, e) {
                            if (e.getKey() == e.ENTER) {
                                search();
                            }
                        }
                    },
                    defaults: {
                        xtype: 'textfield',
                        labelWidth: 110,
                        labelAlign: 'left'
                    }
                }
            },
            items: [{
                xtype: 'container',
                layout: 'vbox',
                items: [{
                    xtype: 'combo',
                    store: paymentTypesStore,
                    queryMode: 'local',
                    fieldLabel: 'გადახდის ტიპი',
                    name: 'payment_type',
                    displayField: 'name',
                    valueField: 'id',
                    typeAhead: true,
                    selectOnFocus: true,
                    forceSelection: true,
                    editable: true
                }]
            }, {
                xtype: 'container',
                layout: 'vbox',
                items: [{
                    fieldLabel: 'სახელი',
                    name: 'first_name'
                },]
            },],
            buttons: [{
                xtype: 'button',
                text: 'ძებნა',//Dashboard.app.Language.SEARCH,
                iconCls: 'x-fa fa-search',
                margin: '0 3px 0 3px',
                handler: function () {
                    search();
                }
            }]
        });

        var infoBtn = Ext.create('Ext.button.Button', {
            iconCls: 'x-fa fa-info-circle',
            handler: function () {
                Ext.toast({
                    html: "<span style='background-color: #f2dede; color: #090;'> ვადა </span><br>"
                        + "<span style='background-color: #fcf8e3; color: #090;'> კრიტიკული რაოდენობა </span>",
                    title: 'ინფო',//Dashboard.app.Language.INFO,
                    width: 200,
                    align: 'br',
                    closable: true
                });
            }
        });

        var viewSellBtn = Ext.create('Ext.button.Button', {
            text: 'დეტალური ნახვა',
            iconCls: 'x-fa fa-eye',
            margin: '0 3px 0 3px',
            handler: function () {
            }
        });

        var store = Ext.create('Dashboard.store.Sells', {
            pageSize: limit,
            autoLoad: false,
            proxy: {
                type: 'ajax',
                actionMethods: {
                    read: 'POST'
                },
                headers: {
                    'Content-Type': 'application/json'
                },
                jsonData: true,
                url: '../api/admin/sells/getSells',
                limitParam: false,
                startParam: false,
                pageParam: false,
                extraParams: lastSearchData,
                paramsAsJson: true,
                reader: {
                    type: 'json',
                    totalProperty: 'total',
                    rootProperty: 'data'
                }
            }
        });

        store.on("load", function (st, records, successful, eOpts) {
            pagingToolbar.add(infoBtn);
        });

        var pagingToolbar = Ext.create('Ext.toolbar.Paging', {
            dock: 'bottom',
            displayInfo: true,
            store: store,
            listeners: {
                beforechange: function (a, pageNumber) {
                    lastSearchData.start = (pageNumber - 1) * limit;
                }
            }
        });

        var grid = Ext.create('Ext.grid.Panel', {
            store: store,
            flex: 1,
            viewConfig: {
                stripeRows: false,
                getRowClass: function (record) {
                    var color = "";
                    if (record.get('state') == 1) {
                        // color = "warning";
                    }
                    return color;
                }
            },
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: [viewSellBtn]
            }, pagingToolbar],
            columns: [{
                header: 'ID',
                dataIndex: 'sell_id',
                width: 60
            }, {
                header: 'სახელი',
                dataIndex: 'first_name',
                minWidth: 200,
                flex: 1
            }, {
                header: 'გვარი',
                dataIndex: 'last_name',
                width: 120
            }, {
                header: 'ელ. ფოსტა',
                dataIndex: 'email',
                width: 120
            }, {
                header: 'ტელეფონი',
                dataIndex: 'phone',
                width: 120
            }, {
                header: 'გადახდის ტიპი',
                width: 120,
                dataIndex: 'payment_type',
                renderer: function (v, b, r) {
                    var cn = '';
                    paymentTypesStore.data.items.forEach(function (item) {
                        if (r.data.payment_type == item.data.id) {
                            cn = item.data.name;
                        }
                    });
                    return cn;
                }
            }, {
                header: 'ფასი',
                dataIndex: 'whole_price',
                width: 70
            }, {
                header: 'ქალაქი',
                width: 90,
                dataIndex: 'city'
            }, {
                header: 'მისამართი',
                width: 90,
                dataIndex: 'address'
            }, {
                header: '',
                width: 50,
                xtype: 'actioncolumn',
                editor: {
                    xtype: 'button'
                },
                items: [{
                    iconCls: 'x-fa fa-trash',
                    handler: function (grid, rowIndex, colIndex) {
                        var selectedRecord = store.getAt(rowIndex);
                        removeSell(selectedRecord);
                    }
                }]
            }],
            listeners: {
                itemdblclick: function (data, record, item, index, e, eOpts) {
                    // openProductWindow(record)
                }
            }
        });

        that.items = [form, grid];

        that.callParent(arguments);

        function removeSell(sell) {
            var msgBox = Ext.create('Ext.window.MessageBox', {
                buttonText: {
                    yes: Dashboard.app.Language.YES,
                    no: Dashboard.app.Language.NO
                }
            });

            msgBox.confirm(Dashboard.app.Language.MARK_AS_REGISTERED, '<b>' + Dashboard.app.Language.MAIN_INFO + '</b>', function (answer) {
                if (answer == 'yes') {

                    Dashboard.app.api.get('../api/admin/sells/remove/' + sell.data.sell_id, function (result) {
                        if (result.success) {
                            store.remove(sell);
                        } else {
                            Ext.Msg.alert(Dashboard.app.Language.NOTICE, result.message)
                        }
                    });
                }
            });
        }

        function search() {
            var formData = form.getForm().getFieldValues();
            lastSearchData = {
                first_name: formData.first_name,
                payment_type: formData.payment_type,
                start: start,
                limit: limit
            };
            store.proxy.extraParams = lastSearchData;
            store.loadPage(1);
        }

        function loadGrid() {
            search();
        }

        function reloadLib() {
            libs--;
            if (libs <= 0) {
                loadGrid();
                libsLoaded = true;
            }
        }

        function reloadLibs() {
            if (libsLoaded) {
                loadGrid();
                return;
            }
            reloadLib();
        }

        that.on('afterrender', function () {
            reloadLibs();
        });

    }
})
;
