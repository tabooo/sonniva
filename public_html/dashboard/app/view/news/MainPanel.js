Ext.define('Dashboard.view.news.MainPanel', {
    extend: 'Ext.panel.Panel',
    layout: 'fit',

    constructor: function (config) {
        var that = this;
        var categoriesStore = Ext.create('Dashboard.store.Categories');
        var tagsStore = Ext.create('Dashboard.store.Categories');

        var categories = Ext.create('Ext.form.ComboBox', {
            fieldLabel: 'კატეგორია',
            labelAlign: 'right',
            store: categoriesStore,
            name: 'category',
            displayField: 'name_ge',
            valueField: 'category_id',
            queryMode: 'local',
            forceSelection: true
        });

        /*var tags = Ext.create('Ext.form.ComboBox', {
            fieldLabel: 'ტეგი',
            labelAlign: 'right',
            store: tagsStore,
            name: 'tag',
            displayField: 'name_ge',
            valueField: 'tag_id',
            queryMode: 'local',
            forceSelection: true
        });*/

        var form = Ext.create('Ext.form.Panel', {
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            buttonAlign: 'center',
            bodyPadding: 10,
            items: [categories, /*tags*/, {
                xtype: 'textfield',
                labelAlign: 'right',
                name: 'text',
                flex: 1,
                fieldLabel: 'ტექსტი'
            }],

            buttons: [{
                text: 'ძებნა',
                iconCls:'fa fa-search',
                handler: search
            }, {
                text: 'გასუფთავება',
                handler: function () {
                    form.getForm().reset();
                }
            }]
        });

        var grid = Ext.create('Dashboard.view.news.NewsGrid', {
            flex: 1
        });

        var panel = Ext.create('Ext.Panel', {
            width: 500,
            height: 400,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },

            items: [form, grid]
        });

        that.items = [panel];
        that.callParent(arguments);

        grid.on('loadCategory', function (cat) {
            categoriesStore.loadData(cat);
        });

        /*grid.on('loadTag', function (tag) {
            tagsStore.loadData(tag);
        });*/

        function search() {
            var fieldValues = form.getForm().getFieldValues();
            grid.getStore().getProxy().extraParams = fieldValues;
            grid.getStore().loadPage(1);
        }
    }
});