Ext.define('Dashboard.view.news.Window', {
    extend: 'Dashboard.view.shell.FitWindow',
    layout: 'fit',
    modal: true,
    border: false,
    maximizable: true,

    constructor: function (config) {
        var that = this;

        var form = Ext.create('Dashboard.view.news.NewsFrom', {
            record: config.record,
            categories: config.categories,
            listeners: {
                addedNews: function () {
                    that.fireEvent('addedNews');
                },
                onClose: function () {
                    that.fireEvent('onClose');
                }
            }
        });

        that.items = [form];
        that.callParent(arguments);
    }
});