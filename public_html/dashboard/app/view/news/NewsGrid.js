Ext.define('Dashboard.view.news.NewsGrid', {
    extend: 'Ext.grid.Panel',
    layout: 'fit',

    constructor: function (config) {
        var that = this,
            loadCount = 1, pageSize = 30;

        var categories = [],
            addBtnId = Ext.id(),
            editBtnId = Ext.id();

        var store = Ext.create('Dashboard.store.Posts', {
            autoLoad: false,
            pageSize: pageSize,

            proxy: {
                type: 'ajax',
                url: '../api/admin/posts',

                reader: {
                    type: 'json',
                    rootProperty: 'data'
                }
            },
            listeners: {
                load: function () {
                    afterLoad();
                }
            }
        });

        that.dockedItems = [{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                xtype: 'button',
                disabled: true,
                id: addBtnId,
                text: 'სიახლის დამატება',
                iconCls: 'fa fa-plus fa-lg',
                handler: function () {
                    showNewsWin();
                }
            }, {
                xtype: 'button',
                text: 'რედაქტირება',
                id: editBtnId,
                disabled: true,
                iconCls: 'fa fa-pencil-square-o fa-lg',
                listeners: {
                    click: function () {
                        var selected = that.getSelectionModel().getSelection();
                        if (selected.length) {
                            showNewsWin(selected[0]);
                        }
                    }
                }
            }]
        }, {
            dock: 'bottom',
            xtype: 'pagingtoolbar',
            store: store,
            displayInfo: true,
            displayMsg: 'Displaying films {0} - {1} of {2}',
            emptyMsg: "No films to display"
        }];

        that.store = store;

        that.columns = [{
            header: "#",
            dataIndex: 'post_id',
            width: 50
        }, {
            header: "დასახელება ქართ.",
            dataIndex: 'title_ge',
            flex: 1
        }, {
            header: "კატეგორია",
            dataIndex: 'category_id',
            width: 200
        }, {
            align: 'center',
            header: "გალერეა",
            dataIndex: 'showInGallery',
            width: 100,
            renderer: function (value) {
                if (value == 1) {
                    return '<img src="resources/images/check.png" width="25px">';
                }

                return '';
            }
        }, {
            width: 50,
            xtype: 'actioncolumn',
            editor: {
                xtype: 'button'
            },
            items: [{
                icon: 'resources/icons/16x16/delete2.png',
                handler: function (grid, rowIndex, colIndex) {
                    var msgBox = Ext.create('Ext.window.MessageBox', {
                        buttonText: {
                            yes: Dashboard.app.Language.YES,
                            no: Dashboard.app.Language.NO
                        }
                    });

                    msgBox.confirm(Dashboard.app.Language.MARK_AS_REGISTERED, '<b>' + Dashboard.app.Language.ASK_DELETE + '</b>', function (answer) {
                        if (answer == 'yes') {
                            var selectedRecord = store.getAt(rowIndex);
                            Ext.Ajax.request({
                                url: '../api/admin/post/destroy/' + selectedRecord.data.post_id,
                                method: "GET",
                                success: function (resultContainer) {
                                    var resp = JSON.parse(resultContainer.responseText);
                                    if (resp.success) {
                                        store.remove(selectedRecord);
                                    } else {
                                        Ext.Msg.alert(Dashboard.app.Language.NOTICE, resp.description)
                                    }

                                },
                                failure: function (data) {
                                    Ext.Msg.alert(Dashboard.app.Language.NOTICE, data.status + ': ' + data.statusText)
                                }
                            });
                        }
                    });
                }
            }]
        }];

        that.listeners = {
            itemdblclick: function (data, record, item, index, e, eOpts) {
                showNewsWin(record);
            },
            selectionchange: function () {
                Ext.getCmp(editBtnId).setDisabled(false);
            }
        };

        that.callParent(arguments);

        that.on('afterrender', function () {
            that.getStore().setPageSize(pageSize);
            setTimeout(loadData, 0);
        });

        function getCategories() {
            Dashboard.app.api.get('../api/admin/web/categories', function (result) {
                categories = result;

                that.fireEvent('loadCategory', categories);
                libsLoaded();
            });
        }

        function loadData() {
            that.getView().mask("Please wait...");
            getCategories();
        }

        function libsLoaded() {
            loadCount--;

            if (loadCount == 0) {
                loadNews();
            }
        }

        function loadNews() {
            that.getStore().load();
        }

        function afterLoad() {
            var records = store.data.items;

            for (var i = 0; i < records.length; i++) {
                var rec = records[i], text = '';

                for (var j = 0; j < rec.data.categories.length; j++) {
                    text += rec.data.categories[j].name_ge;

                    if (j + 1 < rec.data.categories.length)text += ', ';
                }

                for (var j = 0; j < rec.data.meta.length; j++) {
                    if (rec.data.meta[j].meta_key == 'showInGallery') {
                        rec.set('showInGallery', rec.data.meta[j].meta_value);
                        rec.set('meta_id', rec.data.meta[j].meta_id);

                        break;
                    }
                }

                rec.set('category_id', text);
                rec.commit();
            }

            Ext.getCmp(addBtnId).setDisabled(false);
            if (that.getView()) that.getView().unmask();
        }

        function showNewsWin(record) {
            var title = (record) ? record.data.title_ge : 'სიახლის დამატება';

            var win = Ext.create('Dashboard.view.news.Window', {
                title: title,
                record: record,
                categories: categories,
                listeners: {
                    addedNews: function () {
                        loadNews();

                        Ext.toast({
                            html: 'Data Saved',
                            title: 'Success',
                            width: 200,
                            align: 't'
                        });

                        win.close();
                    },
                    onClose: function () {
                        win.close();
                    }
                }
            });

            win.show();
        }
    }
});