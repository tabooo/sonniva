Ext.define('Dashboard.view.news.NewsFrom', {
    extend: 'Ext.form.Panel',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    fieldDefaults: {
        labelWidth: 150,
        labelAlign: 'right',
        msgTarget: 'side'
    },

    bodyPadding: 10,

    constructor: function (config) {
        var that = this,
            record = (config.record) ? config.record.data : null,
            categoryList = config.categories;

        var showInGalleryId = Ext.id();

        var categoriesForm = Ext.create('Dashboard.view.web_category.SelectCategoryForm', {
            flex: 1,
            border: 1,
            categories: categoryList,
            autoScroll: true,
            height: 140
        });

        var publishDateField = Ext.create('Ext.form.field.Date', {
            format: 'Y-m-d',
            flex: 1,
            name: 'published_at',
            fieldLabel: 'გამოქვეყნების თარიღი',
            value: new Date()
        });

        var publishTimeField = Ext.create('Ext.form.field.Time', {
            width: 100,
            margin: '0 0 0 5px',
            format: 'H:i',
            name: 'published_time',
            increment: 60,
            value: new Date()
        });

        var imgField = Ext.create('Ext.form.field.Text', {
            flex: 1,
            labelAlign: 'right',
            fieldLabel: 'სურათი',
            name: 'img_path',
            readOnly: true
        });

        var selectImgBtn = Ext.create('Ext.button.Button', {
            margin: '0 0 0 5px',
            iconCls: 'x-fa fa-picture-o',
            handler: showFileManagerWindow
        });

        var titleGE = getTextField('title_ge');
        var titleEN = getTextField('title_en');
        var titleRU = getTextField('title_ru');

        var keywordsGE = getKeywordTextField('keyword_ge');
        var keywordsEN = getKeywordTextField('keyword_en');
        var keywordsRU = getKeywordTextField('keyword_ru');

        var googleDescriptionGE = getGoogleDescTextField('google_description_ge');
        var googleDescriptionEN = getGoogleDescTextField('google_description_en');
        var googleDescriptionRU = getGoogleDescTextField('google_description_ru');

        var tinyPanelGE = getTinymceTextarea('description_ge', 2);
        var tinyPanelEN = getTinymceTextarea('description_en', 2);
        var tinyPanelRU = getTinymceTextarea('description_ru', 2);

        var tabPanel = Ext.create('Ext.tab.Panel', {
            flex: 1,
            scrollable: true,
            items: [{
                xtype: 'container',
                margin: '5px 0 0 0',
                title: 'ქართული',
                minHeight: 900,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [titleGE, keywordsGE, googleDescriptionGE, tinyPanelGE]
            }, {
                xtype: 'container',
                margin: '5px 0 0 0',
                title: 'ინგლისური',
                minHeight: 900,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [titleEN, keywordsEN, googleDescriptionEN, tinyPanelEN, {
                    xtype: 'hiddenfield',
                    name: 'post_id'
                }, {
                    xtype: 'hiddenfield',
                    name: 'meta_id'
                }]
            }, {
                xtype: 'container',
                margin: '5px 0 0 0',
                title: 'რუსული',
                minHeight: 900,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [titleRU, keywordsRU, googleDescriptionRU, tinyPanelRU]
            }]
        });

        that.items = [{
            xtype: 'container',
            layout: 'hbox',
            height: 150,
            items: [categoriesForm, {
                xtype: 'container',
                layout: 'vbox',
                flex: 1,
                items: [{
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    flex: 1,
                    width: '100%',
                    items: [publishDateField, publishTimeField]
                }, {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    flex: 1,
                    width: '100%',
                    items: [imgField, selectImgBtn]
                }, {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    flex: 1,
                    width: '100%',
                    items: [{
                        xtype: 'checkboxfield',
                        name: 'showInGallery',
                        id: showInGalleryId,
                        fieldLabel: 'გალერეა (785x428)'
                    }]
                }]
            }]
        }, tabPanel];

        that.dockedItems = [{
            xtype: 'toolbar',
            dock: 'bottom',
            ui: 'footer',
            layout: {
                pack: 'end'
            },
            items: [{
                minWidth: 80,
                text: 'Save',
                listeners: {
                    click: onButtonClickSubmit
                }
            }, {
                minWidth: 80,
                text: 'Cancel',
                listeners: {
                    click: onButtonClickCancel
                }
            }]
        }];

        that.callParent(arguments);

        that.on('afterrender', function () {
            setTimeout(setFormValues, 0);
        });

        function showFileManagerWindow() {
            var window = Ext.create('Dashboard.view.chooser.FileManagerWindow', {
                listeners: {
                    selected: function (selected) {
                        window.close();
                        var url = selected.url;
                        imgField.setValue(url);
                    }
                }
            });

            window.show();
        }

        function getTextField(name) {
            return Ext.create('Ext.form.field.Text', {
                name: name,
                fieldLabel: 'სათაური'
            });
        }

        function getKeywordTextField(name) {
            return Ext.create('Ext.form.field.Text', {
                name: name,
                fieldLabel: 'Keywords'
            });
        }

        function getGoogleDescTextField(name) {
            return Ext.create('Ext.form.field.Text', {
                name: name,
                fieldLabel: 'Google Description'
            });
        }

        function getTinymceTextarea(name, flex) {
            return Ext.widget("tinymce_textarea", {
                margin: '0 15px 0 0',
                flex: flex,
                name: name,
                style: {border: '0'},
                tinyMCEConfig: tinyCfg1,
                fieldStyle: 'font-family: Courier New; font-size: 12px;'
            });
        }

        function setFormValues() {
            if (record) {
                var categoriesIds = [];

                record.categories.forEach(function (item) {
                    categoriesIds.push(item.category_id)
                });
                categoriesForm.selectItems(categoriesIds);

                that.getForm().setValues(record);
                that.getForm().setValues({
                    'published_time': new Date(record.published_at),
                    'published_at': new Date(record.published_at)
                });
            }
        }

        function onButtonClickSubmit() {
            if (that.isValid() && categoriesForm.getSelectedItems() &&
                categoriesForm.getSelectedItems().length) {

                var values = that.getForm().getFieldValues();

                if (Ext.getCmp(showInGalleryId).checked) {
                    values.showInGallery = 1;
                } else {
                    values.showInGallery = 0;
                }

                if (values.showInGallery && Ext.isEmpty(values.img_path)) {
                    Ext.Msg.alert(Dashboard.app.LanguageNOTICE, "აირჩიე სურათი");
                    return false;
                }

                var categories = [];
                categoriesForm.getSelectedItems().forEach(function (item) {
                    categories.push(item.data.category_id)
                });
                values.categories = categories;

                var published_at = new Date(publishDateField.getRawValue() + "T" + publishTimeField.getRawValue() + ":00");
                values.published_at = published_at.getTime();

                values.description_ge = modifyElement(values.description_ge);
                values.description_en = modifyElement(values.description_en);
                values.description_ru = modifyElement(values.description_ru);
                values.description_ge_short = "";
                values.description_en_short = "";
                values.description_ru_short = "";
                values.tags = [];

                var fileManagerList = [];
                values.gallery = fileManagerList;

                Dashboard.app.api.postObject('../api/admin/post/add', Ext.JSON.encode(values), function (result) {
                    if (result.success) {
                        record = result.data[0];
                        that.getForm().setValues({
                            post_id: record.post_id
                        });
                        that.fireEvent('addedNews');
                    } else {
                        Ext.Msg.alert(Dashboard.app.Language.NOTICE, result.message)
                    }
                });

            } else {
                Ext.Msg.alert(Dashboard.app.LanguageNOTICE, "კატეგორია და სათაური(GE) სავალდებულო ველებია")
            }
        }

        function onButtonClickCancel() {
            that.fireEvent('onClose');
        }
    }
});