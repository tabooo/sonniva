Ext.define('Dashboard.view.cities.City', {
    extend: 'Ext.window.Window',
    layout: 'fit',
    modal: true,
    border: false,
    maximizable: true,
    constructor: function (config) {
        var that = this;

        var record = config.record;

        var titleGE = Ext.create('Ext.form.field.Text', {
            name: 'name_ge',
            fieldLabel: 'დასახელება ქართ.',
            allowBlank: false
        });
        var titleEN = Ext.create('Ext.form.field.Text', {
            name: 'name_en',
            fieldLabel: 'დასახელება რუს.'
        });
        var titleRU = Ext.create('Ext.form.field.Text', {
            name: 'name_ru',
            fieldLabel: 'დასახელება ინგ.'
        });
        var price = Ext.create('Ext.form.field.Text', {
            name: 'price',
            fieldLabel: 'ფასი',
            allowBlank: false
        });
        var delivery_time = Ext.create('Ext.form.field.Text', {
            name: 'delivery_time',
            fieldLabel: 'მიტანის დრო',
            allowBlank: false
        });

        var form = Ext.create('Ext.form.Panel', {
            categories: config.categories,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            width: 400,
            bodyPadding: 10,
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                layout: {
                    pack: 'end'
                },
                items: [{
                    minWidth: 80,
                    text: 'Save',
                    listeners: {
                        click: onButtonClickSubmit
                    }
                }, {
                    minWidth: 80,
                    text: 'Cancel',
                    listeners: {
                        click: onButtonClickCancel
                    }
                }]
            }],
            items: [titleGE, titleEN, titleRU, price, delivery_time, {
                name: 'city_id',
                xtype: 'hidden'
            }]
        });

        that.items = [form];
        that.callParent(arguments);

        function onButtonClickSubmit() {
            if (form.isValid()) {

                var values = form.getForm().getFieldValues();

                if (!values.city_id) {
                    delete values.city_id;
                }

                Dashboard.app.api.postObject('../api/admin/cities/add', Ext.JSON.encode(values), function (result) {
                    if (result.success) {
                        that.fireEvent('added');
                    } else {
                        Ext.Msg.alert("ERROR!!!", result.message)
                    }
                });

            } else {
                Ext.Msg.alert("შეცდომა", "სათაური(GE) და ფასი სავალდებულო ველებია")
            }
        }

        function onButtonClickCancel() {
            that.close();
        }

        if (record) {
            form.getForm().setValues(record);
        }
    }
});