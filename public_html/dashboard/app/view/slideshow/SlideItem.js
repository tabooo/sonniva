Ext.define('Dashboard.view.slideshow.SlideItem', {
    extend: 'Ext.window.Window',
    layout: 'fit',
    modal: true,
    border: false,
    maximizable: true,
    constructor: function (config) {
        var that = this;

        var record = config.record;

        var imgField = Ext.create('Ext.form.field.Text', {
            flex: 1,
            //labelAlign: 'right',
            fieldLabel: 'სურათი',
            name: 'img_path',
            readOnly: true
        });

        var selectImgBtn = Ext.create('Ext.button.Button', {
            margin: '0 0 0 5px',
            iconCls: 'x-fa fa-picture-o',
            handler: showFileManagerWindow
        });

        var titleGE = Ext.create('Ext.form.field.Text', {
            name: 'title_ge',
            fieldLabel: 'სათაური ქართ.'
        });
        var titleEN = Ext.create('Ext.form.field.Text', {
            name: 'title_en',
            fieldLabel: 'სათაური რუს.'
        });
        var titleRU = Ext.create('Ext.form.field.Text', {
            name: 'title_ru',
            fieldLabel: 'სათაური ინგ.'
        });

        var form = Ext.create('Ext.form.Panel', {
            categories: config.categories,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            width: 400,
            bodyPadding: 10,
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                layout: {
                    pack: 'end'
                },
                items: [{
                    minWidth: 80,
                    text: 'Save',
                    listeners: {
                        click: onButtonClickSubmit
                    }
                }, {
                    minWidth: 80,
                    text: 'Cancel',
                    listeners: {
                        click: onButtonClickCancel
                    }
                }]
            }],
            items: [titleGE, titleEN, titleRU, {
                xtype: 'fieldcontainer',
                layout: 'hbox',
                flex: 1,
                width: '100%',
                items: [imgField, selectImgBtn]
            }, {
                name: 'post_id',
                xtype: 'hidden'
            }]
        });

        that.items = [form];
        that.callParent(arguments);

        function showFileManagerWindow() {
            var window = Ext.create('Dashboard.view.chooser.FileManagerWindow', {
                listeners: {
                    selected: function (selected) {
                        window.close();
                        var url = selected.url;
                        imgField.setValue(url);
                    }
                }
            });

            window.show();
        }

        function onButtonClickSubmit() {
            if (form.isValid()) {

                var values = form.getForm().getFieldValues();

                if (!values.post_id) {
                    delete values.post_id;
                }

                Dashboard.app.api.postObject('../api/admin/slideshow/add', Ext.JSON.encode(values), function (result) {
                    if (result.success) {
                        that.fireEvent('added');
                    } else {
                        Ext.Msg.alert("ERROR!!!", result.message)
                    }
                });

            } else {
                Ext.Msg.alert("შეცდომა", "კატეგორია და სათაური(GE) სავალდებულო ველებია")
            }
        }

        function onButtonClickCancel() {
            that.close();
        }

        if (record) {
            form.getForm().setValues(record);
        }
    }
});