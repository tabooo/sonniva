Ext.define("Dashboard.view.slideshow.Slides", {
    extend: 'Ext.container.Container',
    //anchor: "100% -1",
    margin: 10,
    title: 'სლოგანები',
    closable: true,
    layout: {
        type: "vbox",
        pack: "center"
    },
    constructor: function (config) {
        var that = this;

        var libs = 1;
        var libsLoaded = false;

        var limit = 50, start = 0;
        var lastSearchData = {
            start: start,
            limit: limit
        };

        var addBtn = Ext.create('Ext.button.Button', {
            iconCls: 'x-fa fa-plus-circle',
            text: 'სლოგანის დამატება',
            handler: function () {
                var window = Ext.create('Dashboard.view.slideshow.SlideItem', {
                    listeners: {
                        added: function (item) {
                            reLoadLibs();
                            window.close();
                        }
                    }
                }).show();
            }
        });

        var store = Ext.create('Dashboard.store.Posts', {
            pageSize: limit,
            autoLoad: false,
            proxy: {
                type: 'ajax',
                actionMethods: {
                    read: 'GET'
                },
                headers: {
                    'Content-Type': 'application/json'
                },
                jsonData: true,
                url: '../api/admin/slideshow/getAll',
                limitParam: false,
                startParam: false,
                pageParam: false,
                extraParams: lastSearchData,
                paramsAsJson: true,
                reader: {
                    type: 'json',
                    totalProperty: 'total',
                    rootProperty: 'data'
                }
            }
        });

        var pagingToolbar = Ext.create('Ext.toolbar.Paging', {
            dock: 'bottom',
            displayInfo: true,
            store: store,
            listeners: {
                beforechange: function (a, pageNumber) {
                    lastSearchData.start = (pageNumber - 1) * limit;
                }
            }
        });

        var grid = Ext.create('Ext.grid.Panel', {
            store: store,
            width: '100%',
            flex: 1,
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: [{
                    xtype: 'button',
                    text: 'განახლება',
                    iconCls: 'fa fa-refresh fa-lg',
                    handler: function () {
                        search();
                    }
                }, addBtn]
            }, pagingToolbar],
            columns: [{
                header: "სურათი",
                dataIndex: 'img_path',
                width: 150,
                renderer: function (val) {
                    if (val) {
                        return '<img src="' + val + '" width="90px">';
                    }
                    return '';
                }
            }, {
                header: "ტექსტი ქართ.",
                dataIndex: 'title_ge',
                flex: 1
            }, {
                //header : "",
                width: 50,
                xtype: 'actioncolumn',
                editor: {
                    xtype: 'button'
                },
                items: [{
                    iconCls: 'x-fa fa-trash',
                    handler: function (grid, rowIndex, colIndex) {

                        var selectedRecord = store.getAt(rowIndex);
                        if (selectedRecord.data.page_type_id == 1) {
                            return;
                        }

                        var msgBox = Ext.create('Ext.window.MessageBox', {
                            buttonText: {
                                yes: "დიახ",
                                no: "არა"
                            }
                        });

                        msgBox.confirm("დადასტურება", '<b>' + "გსურთ წაშლა?" + '</b>', function (answer) {
                            if (answer == 'yes') {
                                Dashboard.app.api.get('../api/admin/slideshow/remove/' + selectedRecord.data.post_id, function (result) {
                                    if (result.success) {
                                        store.remove(selectedRecord);
                                    } else {
                                        Ext.Msg.alert("შეტყობინება", result.message)
                                    }
                                });
                            }
                        });
                    }
                }]
            }],
            listeners: {
                itemdblclick: function (data, record, item, index, e, eOpts) {
                    var window = Ext.create('Dashboard.view.slideshow.SlideItem', {
                        record: record.data,
                        title: record.data.title_ge,
                        listeners: {
                            added: function (item) {
                                reLoadLibs();
                                window.close();
                            }
                        }
                    }).show();

                }
            }
        });

        that.items = [grid];

        that.callParent(arguments);

        function reloadLib() {
            libs--;
            if (libs == 0) {
                search();
                libsLoaded = true;
            }
        }

        function reLoadLibs() {
            if (libsLoaded) {
                search();
                return;
            }

            reloadLib();
        }

        function search() {
            lastSearchData = {
                start: start,
                limit: limit
            };
            store.proxy.extraParams = lastSearchData;
            store.loadPage(1);
        }

        reLoadLibs();
    }
});