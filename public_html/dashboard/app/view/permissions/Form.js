Ext.define('Dashboard.view.permissions.Form', {
    extend: 'Ext.form.Panel',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    bodyPadding: 15,

    defaults: {
        xtype: 'textfield',
        labelWidth: 100,
        allowBlank: false,
        labelAlign: 'right',
        msgTarget: 'side'
    },

    constructor: function (config) {
        var that = this,
            RestAPI = Ext.create('Dashboard.util.RestAPI');

        if (config.isPermission) {
            that.dockedItems = [{
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                layout: {
                    pack: 'end'
                },
                items: [{
                    minWidth: 80,
                    text: 'Save',
                    listeners: {
                        click: save
                    }
                }]
            }];
        }

        that.items = [{
            fieldLabel: 'Name',
            name: 'name'
        }, {
            fieldLabel: 'Display name',
            name: 'display_name'
        }, {
            xtype: 'textareafield',
            grow: false,
            fieldLabel: 'Description',
            name: 'description'
        }, {
            xtype: 'hiddenfield',
            name: 'id'
        }];

        that.callParent(arguments);

        function save() {
            if (that.getForm().isValid()) {
                var values = that.getForm().getFieldValues();

                that.getEl().mask("Please wait...");

                RestAPI.postObject('../admin/addPermission', values, function (result) {
                    Dashboard.util.Util.showToast(result.message);
                    if (that.getEl()) that.getEl().unmask();

                    if (result.success) {
                        that.fireEvent('addPermission');
                    }
                });
            }
        }
    }
});