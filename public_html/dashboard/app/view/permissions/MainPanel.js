Ext.define("Dashboard.view.permissions.MainPanel", {
    extend: 'Ext.panel.Panel',
    layout: 'fit',
    flex: 1,

    constructor: function (config) {
        var that = this,
            lastSelected = null;

        var gridUser = Ext.create('Dashboard.view.users.UserGrid', {
            hideDockedItems: true,
            hideRemove: true,
            withRoles: true,
            flex: 1,
            listeners: {
                selectionchange: function (model, selections, eOpts) {

                    if (selections.length && selections.length == 1) {
                        lastSelected = selections[0].data;
                        getUserPermission(selections[0].data);
                    }
                }
            }
        });

        var gridRole = Ext.create('Dashboard.view.permissions.Grid', {
            flex: 1,
            title: 'Role',
            isRole: true,
            activeColumn: true,
            listeners: {
                attachRole: function () {
                    gridUser.getStore().reload();
                }
            },
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: [{
                    xtype: 'button',
                    text: 'Add Role',
                    iconCls: 'fa fa-plus fa-lg',
                    listeners: {
                        click: showRoleWin
                    }
                }]
            }]
        });

        var gridPermission = Ext.create('Dashboard.view.permissions.Grid', {
            flex: 1,
            title: 'Permission',
            isPermission: true,
            activeColumn: true,
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: [{
                    xtype: 'button',
                    text: 'Add Permission',
                    iconCls: 'fa fa-plus fa-lg',
                    listeners: {
                        click: showPermissionWin
                    }
                }]
            }]
        });

        var permissionPanel = Ext.create('Ext.panel.Panel', {
            flex: 1,
            layout: {
                type: "hbox",
                align: "stretch"
            },
            items: [gridRole, {
                xtype: 'splitter',
                style: {
                    background: '#d8d7d6'
                }
            }, gridPermission]
        });

        var mainPanel = Ext.create('Ext.Panel', {
            flex: 1,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [gridUser, permissionPanel]
        });

        that.items = [mainPanel];
        that.callParent(arguments);

        function getUserPermission(selectedData) {
            var roleList = [], permissionList = [];
            var roles = selectedData.roles;

            for (var i = 0; i < roles.length; i++) {
                if (roles[i].perms) {
                    var perms = roles[i].perms;
                    for (var j = 0; j < perms.length; j++) {
                        permissionList.push(perms[j].id);
                    }
                }

                roleList.push(roles[i].id);
            }

            gridRole.loadDefaults();
            gridPermission.loadDefaults();

            gridRole.selectedUser(selectedData);
            gridPermission.selectedUser(selectedData);

            gridRole.checkActive(roleList);
            gridPermission.checkActive(permissionList);
        }

        function showPermissionWin() {
            Ext.create('Dashboard.view.permissions.PermissionWin', {
                title: 'Add Permission',
                flex: 1,
                listeners: {
                    addPermission: function () {
                        gridPermission.getAllPermissions();
                    }
                }
            }).show();
        }

        function showRoleWin() {
            Ext.create('Dashboard.view.permissions.RoleWin', {
                title: 'Add Role',
                flex: 1,
                listeners: {
                    addRole: function () {
                        gridRole.getAllRoles(function () {
                            if (lastSelected) getUserPermission(lastSelected);
                        });
                    }
                }
            }).show();
        }
    }
});
