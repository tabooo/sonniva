Ext.define('Dashboard.view.permissions.RoleWin', {
    extend: 'Dashboard.component.FitWindow',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    modal: true,
    height: 600,
    width: 800,

    constructor: function (cfg) {
        var that = this,
            RestAPI = Ext.create('Dashboard.util.RestAPI');

        var form = Ext.create('Dashboard.view.permissions.Form', {
            isRole: true
        });

        var permissionFrom = Ext.create('Dashboard.view.permissions.Grid', {
            flex: 1,
            title: 'Permission',
            isPermission: true,
            scrollable: true,
            activeColumn: true,
            style: {
                borderTop: '1px solid #99BCE8',
                borderRight: '1px solid #99BCE8',
                borderBottom: '1px solid #99BCE8'
            }
        });

        var permissionTo = Ext.create('Dashboard.view.permissions.Grid', {
            flex: 1,
            title: 'Permission',
            scrollable: true,
            activeColumn: true,
            style: {
                borderTop: '1px solid #99BCE8',
                borderLeft: '1px solid #99BCE8',
                borderBottom: '1px solid #99BCE8'
            }
        });

        var navBtnContainer = Ext.create('Dashboard.view.permissions.NavBtnContainer', {
            listeners: {
                addBtn: function () {
                    var record = permissionFrom.getSelectionModel().getSelection()[0];
                    addDataToStore(record);
                },
                removeBtn: function () {
                    var record = permissionTo.getSelectionModel().getSelection()[0];
                    removeDataFromStore(record);
                }
            }
        });

        var container = Ext.create('Ext.container.Container', {
            border: false,
            bodyCls: 'x-border-layout-ct',
            flex: 1,

            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [permissionFrom, navBtnContainer, permissionTo]
        });

        that.items = [form, container];


        that.dockedItems = [{
            xtype: 'toolbar',
            dock: 'bottom',
            ui: 'footer',
            layout: {
                pack: 'end'
            },
            items: [{
                minWidth: 80,
                text: 'Save',
                listeners: {
                    click: save
                }
            }]
        }];

        that.callParent(arguments);

        function addDataToStore(record) {
            if (!record.get('isAdded')) {
                var data = Ext.clone(record.data);

                permissionTo.getStore().add(data);
                record.set("isAdded", true);
            }
        }

        function removeDataFromStore(record) {
            var rec = permissionFrom.getStore().findRecord('id', record.get('id'));

            if (rec) {
                rec.set("isAdded", false);
            }

            permissionTo.getStore().remove(record);
        }

        function save() {
            if (form.getForm().isValid()) {
                var values = form.getForm().getFieldValues();

                var list = permissionTo.getStore().getRange(),
                    arr = [];

                Ext.Array.each(list, function (rec) {
                    arr.push(rec.data.id);
                });

                values.permissions = arr;

                that.getEl().mask("Please wait...");

                RestAPI.postObject('../admin/addRole', values, function (result) {
                    Dashboard.util.Util.showToast(result.message);
                    if (that.getEl()) that.getEl().unmask();

                    if (result.success) {
                        that.fireEvent('addRole');
                        that.close();
                    }
                });
            }
        }
    }
});