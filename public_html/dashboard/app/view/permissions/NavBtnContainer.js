Ext.define('Dashboard.view.permissions.NavBtnContainer', {
    extend: 'Ext.container.Container',
    margins: '0 5px 0 5px',
    layout: {
        type: 'vbox',
        pack: 'center'
    },
    buttons: ['add', 'remove'],
    buttonsText: {
        add: "Add to Selected",
        remove: "Remove from Selected"
    },

    constructor: function () {
        var me = this;

        me.items = createButtons();
        me.callParent(arguments);

        function createButtons() {
            var buttons = [];

            Ext.Array.forEach(me.buttons, function (name) {
                buttons.push({
                    xtype: 'button',
                    tooltip: me.buttonsText[name],
                    name: name,
                    handler: onNavBtnClick,
                    iconCls: (name == 'add') ? 'fa fa-chevron-right fa-lg' : 'fa fa-chevron-left fa-lg',
                    navBtn: true,
                    scope: me,
                    margin: '4 0 0 0'
                });
            });

            return buttons;
        }

        function onNavBtnClick(btn) {
            if (btn.name == 'add') {
                me.fireEvent('addBtn');
            } else if (btn.name == 'remove') {
                me.fireEvent('removeBtn');
            }
        }
    }
});