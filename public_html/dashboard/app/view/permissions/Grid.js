Ext.define('Dashboard.view.permissions.Grid', {
    extend: 'Ext.grid.Panel',
    layout: 'fit',

    constructor: function (config) {
        var that = this,
            RestAPI = Ext.create('Dashboard.util.RestAPI');

        var storeOriginalResult = [], selectedUser = null;

        var store = Ext.create('Ext.data.Store', {
            fields: ['id', 'name', 'display_name', 'description']
        });

        var columns = [{
            text: 'Name (Key)',
            dataIndex: 'name',
            flex: 1
        }, {
            text: 'Display Name',
            dataIndex: 'display_name',
            flex: 1
        }, {
            text: 'Description',
            dataIndex: 'description',
            flex: 1,
            renderer: function (value, metaData) {
                metaData.tdAttr = 'data-qtip="' + value + '"';

                return value;
            }
        }];

        if (config.activeColumn) {
            columns.unshift({
                width: 60,
                xtype: 'checkcolumn',
                text: 'Active',
                hidden: true,
                hideable: false,
                dataIndex: 'active',
                listeners: {
                    beforecheckchange: function () {
                        if (config.isPermission)
                            return false;
                    },
                    checkchange: function (me, rowIndex, checked, record) {
                        var activeRecord = record,
                            isChecked = (checked) ? 1 : 2;

                        var sendObj = {
                            userId: selectedUser.id,
                            roleId: activeRecord.data.id,
                            checked: isChecked
                        };

                        attachRole(activeRecord, sendObj);
                    }
                }
            });
        }

        columns.unshift({
            xtype: 'rownumberer',
            width: 40
        });

        that.store = store;
        that.columns = columns;
        that.callParent(arguments);

        that.on('afterrender', function () {
            setTimeout(function () {
                if (config.isRole) {
                    getAllRoles();
                } else if (config.isPermission) {
                    getAllPermissions();
                }
            }, 0);
        });

        that.checkActive = function (list) {
            that.columns[1].show();

            for (var i = 0; i < list.length; i++) {
                var index = that.getStore().findExact('id', list[i]);

                if (index == -1) {
                    return;
                }

                var record = that.getStore().getAt(index);

                if (record) {
                    record.set('active', true);
                    record.commit();
                }
            }
        };

        that.loadDefaults = function () {
            that.selectedUser(null);

            that.getStore().removeAll();
            that.getStore().loadData(Ext.clone(storeOriginalResult));
        };

        that.selectedUser = function (user) {
            selectedUser = user;
        };

        that.getAllRoles = getAllRoles;
        that.getAllPermissions = getAllPermissions;

        function getAllRoles(cb) {
            that.getView().mask("Please wait...");

            RestAPI.get('../admin/getRoles', function (result) {
                storeOriginalResult = Ext.Array.clone(result);
                that.getStore().loadData(result);
                if (that.getView()) that.getView().unmask();

                cb && cb();
            });
        }

        function getAllPermissions(cb) {
            that.getView().mask("Please wait...");

            RestAPI.get('../admin/getPermissions', function (result) {
                storeOriginalResult = Ext.Array.clone(result);
                that.getStore().loadData(result);
                if (that.getView()) that.getView().unmask();

                cb && cb();
            });
        }

        function attachRole(record, sendObj) {
            that.getView().mask("Please wait...");

            RestAPI.postObject('../admin/attachRole', sendObj, function (result) {

                if (result.success) {
                    record.commit();
                    that.fireEvent('attachRole');
                }

                Dashboard.util.Util.showToast(result.message);
                if (that.getView()) that.getView().unmask();
            });
        }
    }
});