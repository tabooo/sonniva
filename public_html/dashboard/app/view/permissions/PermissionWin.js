Ext.define('Dashboard.view.permissions.PermissionWin', {
    extend: 'Ext.window.Window',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    modal: true,
    height: 300,
    width: 800,

    constructor: function (cfg) {
        var that = this;

        var form = Ext.create('Dashboard.view.permissions.Form', {
            isPermission: true,
            listeners: {
                addPermission: function () {
                    that.fireEvent('addPermission');
                    that.close();
                }
            }
        });

        that.items = [form];
        that.callParent(arguments);
    }
});