Ext.define('Dashboard.view.installments_tbc.InstallmentsTbc', {
    extend: 'Ext.panel.Panel',
    border: false,
    flex: 1,
    title: 'TBC განვადებები',
    closable: true,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    constructor: function (config) {
        var that = this;

        var libs = 1;
        var libsLoaded = false;

        var limit = 100, start = 0;
        var lastSearchData = {
            start: start,
            limit: limit
        };

        var installmentTbcStatusStore = Ext.create('Dashboard.store.InstallmentTbcStatuses');

        var form = Ext.create('Ext.form.Panel', {
            border: false,
            buttonAlign: 'center',
            padding: 5,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            defaults: {
                defaults: {
                    xtype: 'textfield',
                    labelWidth: 110,
                    labelAlign: 'left',
                    margin: '5px 5px',
                    enableKeyEvents: true,
                    listeners: {
                        specialkey: function (f, e) {
                            if (e.getKey() == e.ENTER) {
                                search();
                            }
                        }
                    },
                    defaults: {
                        xtype: 'textfield',
                        labelWidth: 110,
                        labelAlign: 'left'
                    }
                }
            },
            items: [{
                xtype: 'container',
                layout: 'vbox',
                items: [{
                    xtype: 'combo',
                    store: installmentTbcStatusStore,
                    queryMode: 'local',
                    fieldLabel: 'სტატუსი',
                    name: 'status',
                    displayField: 'name',
                    valueField: 'id',
                    typeAhead: true,
                    selectOnFocus: true,
                    forceSelection: true,
                    editable: true
                }]
            }, {
                xtype: 'container',
                layout: 'vbox',
                items: [{
                    fieldLabel: 'განვადების ნომერი',
                    name: 'session_id'
                },]
            },],
            buttons: [{
                xtype: 'button',
                text: 'ძებნა',//Dashboard.app.Language.SEARCH,
                iconCls: 'x-fa fa-search',
                margin: '0 3px 0 3px',
                handler: function () {
                    search();
                }
            }]
        });

        var infoBtn = Ext.create('Ext.button.Button', {
            iconCls: 'x-fa fa-info-circle',
            handler: function () {
                Ext.toast({
                    html: "<span style='background-color: #f2dede; color: #090;'> ვადა </span><br>"
                        + "<span style='background-color: #fcf8e3; color: #090;'> კრიტიკული რაოდენობა </span>",
                    title: 'ინფო',//Dashboard.app.Language.INFO,
                    width: 200,
                    align: 'br',
                    closable: true
                });
            }
        });

        var viewSellBtn = Ext.create('Ext.button.Button', {
            text: 'დეტალური ნახვა',
            iconCls: 'x-fa fa-eye',
            margin: '0 3px 0 3px',
            handler: function () {
            }
        });

        var store = Ext.create('Dashboard.store.InstallmentsTBC', {
            pageSize: limit,
            autoLoad: false,
            proxy: {
                type: 'ajax',
                actionMethods: {
                    read: 'POST'
                },
                headers: {
                    'Content-Type': 'application/json'
                },
                jsonData: true,
                url: '../api/admin/installments/getTbcInstallments',
                limitParam: false,
                startParam: false,
                pageParam: false,
                extraParams: lastSearchData,
                paramsAsJson: true,
                reader: {
                    type: 'json',
                    totalProperty: 'total',
                    rootProperty: 'data'
                }
            }
        });

        store.on("load", function (st, records, successful, eOpts) {
            pagingToolbar.add(infoBtn);
        });

        var pagingToolbar = Ext.create('Ext.toolbar.Paging', {
            dock: 'bottom',
            displayInfo: true,
            store: store,
            listeners: {
                beforechange: function (a, pageNumber) {
                    lastSearchData.start = (pageNumber - 1) * limit;
                }
            }
        });

        var grid = Ext.create('Ext.grid.Panel', {
            store: store,
            flex: 1,
            viewConfig: {
                stripeRows: false,
                getRowClass: function (record) {
                    var color = "";
                    if (record.get('state') == 1) {
                        // color = "warning";
                    }
                    return color;
                }
            },
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: [viewSellBtn]
            }, pagingToolbar],
            columns: [{
                header: '#',
                dataIndex: 'sell_id',
                width: 60
            },{
                header: 'ინვოისის ნომერი',
                dataIndex: 'invoice_id',
                width: 120
            }, {
                header: 'სახელი',
                dataIndex: 'first_name',
                minWidth: 200,
                flex: 1,
                renderer: function (val, meta, rec) {
                    return rec.data.sell.first_name;
                }
            }, {
                header: 'გვარი',
                dataIndex: 'last_name',
                width: 120,
                renderer: function (val, meta, rec) {
                    return rec.data.sell.last_name;
                }
            }, {
                header: 'ელ. ფოსტა',
                dataIndex: 'email',
                width: 120,
                renderer: function (val, meta, rec) {
                    return rec.data.sell.email;
                }
            }, {
                header: 'ტელეფონი',
                dataIndex: 'phone',
                width: 120,
                renderer: function (val, meta, rec) {
                    return rec.data.sell.phone;
                }
            }, {
                header: 'ქალაქი',
                width: 90,
                dataIndex: 'city',
                renderer: function (val, meta, rec) {
                    return rec.data.sell.city;
                }
            }, {
                header: 'მისამართი',
                width: 90,
                dataIndex: 'address',
                renderer: function (val, meta, rec) {
                    return rec.data.sell.address;
                }
            }, {
                header: 'ფასი',
                dataIndex: 'whole_price',
                width: 70,
                renderer: function (val, meta, rec) {
                    return rec.data.sell.whole_price;
                }
            }, {
                header: 'სტატუსი',
                dataIndex: 'status',
                width: 120,
                renderer: function (v, b, r) {
                    var cn = '';
                    installmentTbcStatusStore.data.items.forEach(function (item) {
                        if (r.data.status == item.data.id) {
                            cn = item.data.name;
                        }
                    });
                    return cn;
                }
            }, {
                header: '',
                width: 50,
                xtype: 'actioncolumn',
                editor: {
                    xtype: 'button'
                },
                items: [{
                    iconCls: 'x-fa fa-check',
                    isDisabled: function (grid, rowIndex, colIndex, items, rec) {
                        if (rec.data.status == 1) {
                            return false;
                        }
                        return true;
                    },
                    handler: function (grid, rowIndex, colIndex) {
                        var selectedRecord = store.getAt(rowIndex);
                        confirmInstallment(selectedRecord);
                    }
                }]
            }, {
                header: '',
                width: 50,
                xtype: 'actioncolumn',
                editor: {
                    xtype: 'button'
                },
                items: [{
                    iconCls: 'x-fa fa-trash',
                    isDisabled: function (grid, rowIndex, colIndex, items, rec) {
                        if (rec.data.status == 1) {
                            return false;
                        }
                        return true;
                    },
                    handler: function (grid, rowIndex, colIndex) {
                        var selectedRecord = store.getAt(rowIndex);
                        cancelInstallment(selectedRecord);
                    }
                }]
            }, {
                header: '',
                width: 50,
                xtype: 'actioncolumn',
                editor: {
                    xtype: 'button'
                },
                items: [{
                    iconCls: 'x-fa fa-question-circle',
                    handler: function (grid, rowIndex, colIndex) {
                        var selectedRecord = store.getAt(rowIndex);
                        statusInstallment(selectedRecord);
                    }
                }]
            }],
            listeners: {
                itemdblclick: function (data, record, item, index, e, eOpts) {
                    // openProductWindow(record)
                }
            }
        });

        that.items = [form, grid];

        that.callParent(arguments);

        function cancelInstallment(installment) {
            var msgBox = Ext.create('Ext.window.MessageBox', {
                buttonText: {
                    yes: Dashboard.app.Language.YES,
                    no: Dashboard.app.Language.NO
                }
            });

            msgBox.confirm(Dashboard.app.Language.MARK_AS_REGISTERED, '<b>' + Dashboard.app.Language.MAIN_INFO + '</b>', function (answer) {
                if (answer == 'yes') {

                    that.getEl().mask();
                    Dashboard.app.api.get('../api/admin/installments/cancelTbcInstallment/' + installment.data.id, function (result) {
                        that.getEl().unmask();
                        if (result.success) {
                            search();
                        } else {
                            Ext.Msg.alert(Dashboard.app.Language.NOTICE, result.message)
                        }
                    }, function (error) {
                        that.getEl().mask();
                        Ext.Msg.alert(Dashboard.app.Language.NOTICE, result.message);
                    });
                }
            });
        }

        function confirmInstallment(installment) {
            var msgBox = Ext.create('Ext.window.MessageBox', {
                buttonText: {
                    yes: Dashboard.app.Language.YES,
                    no: Dashboard.app.Language.NO
                }
            });

            msgBox.confirm(Dashboard.app.Language.MARK_AS_REGISTERED, '<b>' + Dashboard.app.Language.MAIN_INFO + '</b>', function (answer) {
                if (answer == 'yes') {

                    that.getEl().mask();
                    Dashboard.app.api.get('../api/admin/installments/confirmTbcInstallment/' + installment.data.id, function (result) {
                        that.getEl().unmask();
                        if (result.success) {
                            search();
                        } else {
                            Ext.Msg.alert(Dashboard.app.Language.NOTICE, result.message)
                        }
                    }, function (error) {
                        that.getEl().mask();
                        Ext.Msg.alert(Dashboard.app.Language.NOTICE, result.message);
                    });
                }
            });
        }

        function statusInstallment(installment) {
            that.getEl().mask();
            Dashboard.app.api.get('../api/admin/installments/statusTbcInstallment/' + installment.data.id, function (result) {
                that.getEl().unmask();
                if (result.success) {
                    let body = JSON.parse(result.data.body);
                    Ext.Msg.alert(Dashboard.app.Language.NOTICE, body.description)
                } else {
                    Ext.Msg.alert(Dashboard.app.Language.NOTICE, result.message)
                }
            }, function (error) {
                that.getEl().mask();
                Ext.Msg.alert(Dashboard.app.Language.NOTICE, result.message);
            });
        }

        function search() {
            var formData = form.getForm().getFieldValues();
            lastSearchData = {
                status: formData.status,
                session_id: formData.session_id,
                start: start,
                limit: limit
            };
            store.proxy.extraParams = lastSearchData;
            store.loadPage(1);
        }

        function loadGrid() {
            search();
        }

        function reloadLib() {
            libs--;
            if (libs <= 0) {
                loadGrid();
                libsLoaded = true;
            }
        }

        function reloadLibs() {
            if (libsLoaded) {
                loadGrid();
                return;
            }
            reloadLib();
        }

        that.on('afterrender', function () {
            reloadLibs();
        });

    }
})
;
