Ext.define('Dashboard.view.lang.langWindow', {
    extend: 'Ext.window.Window',
    title: Dashboard.app.Language.LANGUAGE,
    autoShow: true,
    modal: true,
    // width : 400,
    // height : 220,
    bodyPadding: 10,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    constructor: function (config) {
        var that = this;

        var lang = config.lang;

        var form = Ext.create('Ext.form.Panel', {
            border: false,
            buttonAlign: 'center',
            padding: 5,
            flex: 1,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            defaults: {
                xtype: 'textfield',
                labelWidth: 90,
                width: 280,
                anchor: '100%',
                allowBlank: false
            },
            items: [{
                name: 'key',
                fieldLabel: Dashboard.app.Language.key,
                readOnly: true
            }, {
                name: 'text_ka',
                fieldLabel: Dashboard.app.Language.GEORGIAN
            }, {
                name: 'text_en',
                fieldLabel: Dashboard.app.Language.ENGLISH
            }, {
                name: 'text_ru',
                fieldLabel: Dashboard.app.Language.RUSSIAN
            }, {
                name: 'text_tr',
                fieldLabel: Dashboard.app.Language.TURKISH
            }, {
                xtype: 'hidden',
                name: 'lang_id'
            }]
        });

        var addButton = Ext.create('Ext.button.Button', {
            icon: 'icons/16x16/save.png',
            text: Dashboard.app.Language.SAVE,
            handler: function () {
                if (!form.getForm().isValid) {
                    return;
                }
                var request = form.getForm().getFieldValues();

                Dashboard.app.api.postObject('../api/admin/saveLang', request, function (resp) {
                    that.fireEvent('added', resp.added);
                });
            }
        });

        that.items = [form];

        that.buttons = [addButton];

        that.callParent(arguments);

        if (lang) {
            form.getForm().setValues(lang);
        }
    }
});