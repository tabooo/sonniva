Ext.define('Dashboard.view.lang.Langs', {
    extend: 'Ext.panel.Panel',
    border: false,
    flex: 1,
    title: Dashboard.app.Language.LANGUAGES,
    closable: true,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    constructor: function (config) {

        var that = this;

        var lastSearchData = {
            search: ''
        };

        var nameSearchField = Ext.create('Ext.form.field.Text', {
            emptyText: Dashboard.app.Language.NAME1,
            name: 'searchName',
            width: 200,
            enableKeyEvents: true,
            listeners: {
                specialkey: function (f, e) {
                    if (e.getKey() == e.ENTER) {
                        search();
                    }
                },
                keyup: function (me, e, eOpts) {
                    if (me.getValue().trim().length > 0)
                        search();
                }
            }
        });

        var searchButton = Ext.create('Ext.Button', {
            text: Dashboard.app.Language.SEARCH,
            handler: function () {
                search();
            }
        });

        var addButton = Ext.create('Ext.Button', {
            text: Dashboard.app.Language.NEW_PHRASE,
            icon: 'icons/16x16/add.png',
            handler: function () {
                var window = Ext.create('Dashboard.tabs.lang.langWindow', {
                    title: Dashboard.app.Language.NEW_PHRASE,
                    listeners: {
                        added: function (item) {
                            search();
                            window.close();
                        }
                    }
                }).show();
            }
        });

        var langsStore = Ext.create('Dashboard.store.Languages', {
            proxy: {
                type: 'ajax',
                actionMethods: {
                    read: 'POST'
                },
                url: '../api/admin/getAllLanguage',
                extraParams: lastSearchData,
                limitParam: false,
                startParam: false,
                pageParam: false,
                reader: {
                    type: 'json'
                }
            },
            autoLoad: true
        });

        var grid = Ext.create('Ext.grid.Panel', {
            store: langsStore,
            flex: 1,
            viewConfig: {
                stripeRows: false,
                getRowClass: function (record) {
                    if (record.get('blocked') == 2) {
                        return "danger";
                    }
                }
            },
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: [nameSearchField, searchButton, addButton]
            }],
            columns: [{
                text: Dashboard.app.Language.KEY,
                dataIndex: 'key',
                flex: 1,
                sortable: true
            }, {
                text: 'ქართულად',
                dataIndex: 'text_ka',
                flex: 1,
                sortable: true
            }, {
                text: 'English',
                dataIndex: 'text_en',
                flex: 1,
                sortable: true
            }, {
                text: 'Rus',
                dataIndex: 'text_ru',
                flex: 1,
                sortable: true
            }, {
                text: 'Turkey',
                dataIndex: 'text_tr',
                flex: 1,
                sortable: true
            }],
            listeners: {
                itemdblclick: function (data, record, item, index, e, eOpts) {
                    var window = Ext.create('Dashboard.view.lang.langWindow', {
                        lang: record.data,
                        title: Dashboard.app.Language.EDIT,
                        listeners: {
                            added: function (item) {
                                search(index);
                                window.close();
                            }
                        }
                    }).show();
                }
            }
        });

        that.items = grid;
        that.callParent(arguments);

        function deleteLang(lang) {
            var msgBox = Ext.create('Ext.window.MessageBox', {
                buttonText: {
                    yes: Dashboard.app.Language.YES,
                    no: Dashboard.app.Language.NO
                }
            });

            msgBox.confirm(Dashboard.app.Language.MARK_AS_REGISTERED, '<b>' + Dashboard.app.Language.ASK_DELETE + '</b>', function (answer) {
                if (answer == 'yes') {
                    if (!lang) {
                        lang = grid.getSelectionModel().getSelection()[0];
                    }
                    api.post('restapi/lang/removeLang?langId=' + lang.lang_id, function (result) {
                        search();
                    })
                }
            });
        }

        function search(index) {
            lastSearchData = {
                search: nameSearchField.getValue()
            };
            langsStore.proxy.extraParams = lastSearchData;
            langsStore.loadPage(1);

            if (index) {
                setTimeout(function () {
                    grid.getSelectionModel().select(index);
                }, 100);
            }
        }
    }
});