Ext.define("Dashboard.view.params.Params", {
    extend: 'Ext.container.Container',
    margin: 10,
    title: 'Facebook',
    closable: true,
    layout: {
        type: "vbox",
        pack: "center"
    },

    constructor: function (config) {
        var that = this;

        var store = Ext.create('Dashboard.store.Params');

        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToMoveEditor: 1,
            autoCancel: false
        });

        var grid = Ext.create('Ext.grid.Panel', {
            store: store,
            width: '100%',
            flex: 1,
            plugins: [rowEditing],
            tbar: [{
                xtype: 'button',
                text: 'განახლება',
                iconCls: 'fa fa-refresh fa-lg',
                handler: function () {
                    search();
                }
            }],
            columns: [{
                header: 'დასახელება',
                dataIndex: 'name',
                width: 200
            }, {
                header: "მნიშვნელობა",
                dataIndex: 'value',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            }]
        });

        grid.on("edit", function (editor, e, eOpts) {
            if (e.record.modified) {
                changeInfo(e.record.data);
            }
        });

        that.items = [grid];

        that.callParent(arguments);

        function changeInfo(fb) {
            Dashboard.app.api.postObject('../api/admin/params/updateParam', fb, function (result) {
                if (result.success) {
                    search();
                } else {
                    Ext.Msg.alert(Dashboard.app.Language.NOTICE, result.message)
                }
            });
        }

        function search() {
            Dashboard.app.api.post('../api/admin/params/getParams', function (result) {
                store.loadData(result);
            });

        }

        search();
    }
});