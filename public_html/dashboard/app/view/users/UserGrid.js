Ext.define("Dashboard.view.users.UserGrid", {
    extend: 'Ext.grid.Panel',
    layout: 'fit',

    constructor: function (config) {
        var that = this,
            pageSize = 50;

        var store = Ext.create('Dashboard.store.User', {
            autoLoad: false,
            pageSize: pageSize,

            proxy: {
                type: 'ajax',
                url: (config.withRoles) ? '../admin/getUsersWithRoles' : '../admin/getUsers',

                reader: {
                    type: 'json',
                    rootProperty: 'data'
                }
            },
            listeners: {
                load: function () {
                    afterLoad();
                }
            }
        });

        var columns = [{
            xtype: 'rownumberer'
        }, {
            text: "მომხმარებლის სახელი",
            flex: 1,
            dataIndex: 'name'
        }, {
            text: "ელ. ფოსტა",
            flex: 1,
            dataIndex: 'email'
        }];

        var dockedItems = [{
            dock: 'bottom',
            xtype: 'pagingtoolbar',
            store: store,
            displayInfo: true,
            displayMsg: 'Displaying films {0} - {1} of {2}',
            emptyMsg: "No films to display"
        }];

        if (!config.hideRemove) {
            columns.push({
                xtype: 'actioncolumn',
                width: 40,
                align: 'center',
                items: [{
                    iconCls: 'fa fa-minus-square',
                    tooltip: 'Delete'
                }]
            });
        }

        if (!config.hideDockedItems) {
            dockedItems.unshift({
                xtype: 'toolbar',
                dock: 'top',
                items: [{
                    xtype: 'button',
                    text: 'დამატება',
                    iconCls: 'fa fa-plus fa-lg'
                }, {
                    xtype: 'button',
                    text: 'რედაქტირება',
                    iconCls: 'fa fa-pencil-square-o fa-lg'
                }]
            });
        }

        that.store = store;
        that.columns = columns;
        that.dockedItems = dockedItems;

        that.callParent(arguments);

        that.on('afterrender', function () {
            setTimeout(loadData, 0);
        });

        function loadData() {
            that.getView().mask("Please wait...");
            that.getStore().load();
        }

        function afterLoad() {
            if (that.getView()) that.getView().unmask();
        }
    }
});