Ext.define("Dashboard.view.users.MainPanel", {
    extend: 'Ext.panel.Panel',
    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    constructor: function (config) {
        var that = this;

        var grid = Ext.create('Dashboard.view.users.UserGrid', {
            flex: 1
        });

        that.items = [grid];
        that.callParent(arguments);
    }
});
