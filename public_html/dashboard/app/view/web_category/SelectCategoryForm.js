Ext.define("Dashboard.view.web_category.SelectCategoryForm", {
    extend: 'Ext.form.Panel',
    //anchor: "100% -1",
    margin: 2,
    closable: false,
    layout: {
        type: "vbox",
        pack: "center",
        align: "center"
    },
    autoScroll: true,
    constructor: function (config) {
        var that = this;
        var categories = config.categories;

        /* var libs = 1;
         var libsLoaded = false;*/

        var categoryTreeStore = Ext.create('Dashboard.store.WebCategories');

        var tree = Ext.create('Ext.tree.Panel', {
            store: categoryTreeStore,
            rootVisible: false,
            width: '100%',
            flex: 1,
            selModel: {
                selType: 'checkboxmodel',
                mode: 'SINGLE'
            },
            columns: [{
                xtype: 'treecolumn',
                text: 'დასახელება',
                dataIndex: 'name_ge',
                flex: 1
            }],
            listeners: {
                select: function (selModel, rec) {
                    rec.cascadeBy(function (child) {
                        selModel.select(child, true, true);
                    });
                },
                deselect: function (selModel, rec) {
                    rec.cascadeBy(function (child) {
                        selModel.deselect(child, true);
                    });
                }
            }
        });

        that.items = [tree];

        that.getSelectedItems = function () {
            return tree.getSelectionModel().getSelection();
        };

        that.selectItems = function (categoriesIds) {
            var recs = [];

            Ext.each(categoryTreeStore.data.items, function (item) {
                var rec = tree.getView().getRecord(item);
                if (categoriesIds.indexOf(item.data.category_id) >= 0) {
                    recs.push(rec);
                }
            });

            tree.getSelectionModel().select(recs);
        };

        that.callParent(arguments);

        function categoriesForTreeCombo(data) {
            var items = [];

            for (var i = 0; i < data.length; i++) {
                var item = {
                    text: data[i].name_ge,
                    category_id: data[i].category_id,
                    name_ge: data[i].name_ge,
                    parent_id: data[i].parent_id,
                    expanded: true,
                    category: data[i],
                    leaf: data[i].children.length > 0 ? 'false' : 'true'
                };

                if (data[i].children.length > 0) {
                    item.children = categoriesForTreeCombo(data[i].children)
                }

                items.push(item);
            }

            return items;
        }

        /*function reloadLib() {
         libs--;
         if (libs == 0) {
         libsLoaded = true;
         search();
         }
         }

         function reLoadLibs() {
         if (libsLoaded) {
         search();
         return;
         }
         reloadLib();
         }

         function search() {
         Ext.Ajax.request({
         url: '../admin/categories',
         method: "GET",
         success: function (resultContainer) {
         var resp = JSON.parse(resultContainer.responseText);
         addToRootNode(resp);
         },
         failure: function (data) {
         Ext.Msg.alert(Language.NOTICE, data.status + ': ' + data.statusText)
         }
         })
         }*/

        function addToRootNode(resp) {
            resp = categoriesForTreeCombo(resp);
            resp = {
                text: '.',
                children: resp,
                expanded: true
            };
            categoryTreeStore.setRootNode(resp);
        }

        if (categories && categories.length) {
            addToRootNode(categories);
        }

        //reLoadLibs();
    }
});