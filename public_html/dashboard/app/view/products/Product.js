Ext.define('Dashboard.view.products.Product', {
    extend: 'Ext.window.Window',
    border: false,
    width: 800,
    height: 600,
    modal: true,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    constructor: function (config) {
        var that = this;

        var product_id = config.product_id;
        var product = {};
        var readOnly = config.readOnly;

        var mainInfoPanel = Ext.create('Dashboard.view.products.ProductMainInfo', {
            product_id: product_id,
            readOnly: readOnly,
            listeners: {
                productChanged: function (item) {
                    product = item;
                    that.fireEvent('productChanged', item)
                },
                productAdded: function (item) {
                    product_id = item.product_id;
                    product = item;
                    that.fireEvent('productAdded', item)
                },
                productLoaded: function (item) {
                    product = item;
                }
            }
        });

        var productInfoPanel = Ext.create('Dashboard.view.product.ProductInfo', {
            product_id: product_id,
            readOnly: readOnly,
            disabled: product_id ? false : true
        });

        var productResourcesPanel = Ext.create('Dashboard.view.product.ProductPhotos', {
            product_id: product_id,
            readOnly: readOnly,
            disabled: product_id ? false : true
        });

        var items = [mainInfoPanel, productInfoPanel, productResourcesPanel, /*productStoragePanel*/];

        var tabPanel = Ext.create('Ext.tab.Panel', {
            layout: 'fit',
            border: false,
            activeTab: 0,
            items: items,
            flex: 1
        });

        var recievingAddButton = Ext.create("Ext.Button", {
            xtype: "button",
            iconCls: 'x-fa fa-plus-circle',
            text: Dashboard.app.Language.RECIEVE_PRODUCT,
            hidden: readOnly,
            handler: function () {
                var productRecievingWindow = Ext.create('Dashboard.view.receive.RecievingWindow', {
                    product_id: product_id,
                    product: product,
                    listeners: {
                        recievingAdded: function (item) {
                            Ext.Ajax.request({
                                url: 'restapi/products/getStorage?product_id=' + product_id + '&firstResult=0&maxResult=100',
                                method: "POST",
                                contentType: 'application/json; charset=utf-8',
                                dataType: 'json',
                                success: function (resultContainer) {
                                    var resp = JSON.parse(resultContainer.responseText);
                                    var amount = 0;
                                    resp.forEach(function (item) {
                                        amount += item.amount;
                                    });
                                    var obj = {
                                        amount: amount,
                                        lastPrice: item.recievingOnePrice,
                                        price: item.productPrice
                                    };
                                    updateAmount(obj);
                                },
                                failure: function (data) {
                                    Ext.Msg.alert(Dashboard.app.Language.NOTICE, data.status + ': ' + data.statusText)
                                }
                            });
                            productRecievingWindow.close();
                        }
                    }
                }).show();
            }
        });

        var recievingHistoryButton = Ext.create("Ext.Button", {
            xtype: "button",
            iconCls: 'x-fa fa-history',
            hidden: readOnly,
            text: Dashboard.app.Language.RECIEVING_HISTORY,
            handler: function () {
                var productRecievingHistoryWindow = Ext.create('myjs.tabs.product.productRecievingHistoryWindow', {
                    product_id: product_id
                }).show();
            }
        });

        var btns = [];

        if (product_id) {
            btns.push(recievingHistoryButton);
            btns.push(recievingAddButton);
        }

        that.buttons = btns;

        that.items = [tabPanel];

        that.callParent(arguments);

        function updateAmount(obj) {
            //mainInfoPanel.updateAmount(obj);
            that.fireEvent('reloadProduct');
        }
    }
});
