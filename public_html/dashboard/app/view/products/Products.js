Ext.define('Dashboard.view.products.Products', {
    extend: 'Ext.panel.Panel',
    border: false,
    flex: 1,
    title: Dashboard.app.Language.PRODUCTS,
    closable: true,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    constructor: function (config) {
        var that = this;

        var libs = 1;
        var libsLoaded = false;

        var limit = 100, start = 0;
        var lastSearchData = {
            start: start,
            limit: limit
        };

        var categoriesComboStore = Ext.create('Dashboard.store.Categories');
        var searchCategoryId = null;
        var branchComboStore = Ext.create('Dashboard.store.Branches');

        var form = Ext.create('Ext.form.Panel', {
            border: false,
            buttonAlign: 'center',
            padding: 5,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            defaults: {
                defaults: {
                    xtype: 'textfield',
                    labelWidth: 110,
                    labelAlign: 'left',
                    margin: '5px 5px',
                    enableKeyEvents: true,
                    listeners: {
                        specialkey: function (f, e) {
                            if (e.getKey() == e.ENTER) {
                                search();
                            }
                        }
                    },
                    defaults: {
                        xtype: 'textfield',
                        labelWidth: 110,
                        labelAlign: 'left'
                    }
                }
            },
            items: [{
                xtype: 'container',
                layout: 'vbox',
                items: [{
                    fieldLabel: 'პროდუქტის ID',
                    name: 'product_id'
                }, {
                    xtype: 'combo',
                    store: branchComboStore,
                    queryMode: 'local',
                    fieldLabel: Dashboard.app.Language.BRANCH,
                    name: 'branch_id',
                    displayField: 'branch_name',
                    valueField: 'branch_id',
                    typeAhead: true,
                    selectOnFocus: true,
                    forceSelection: true,
                    editable: true
                }]
            }, {
                xtype: 'container',
                layout: 'vbox',
                items: [{
                    fieldLabel: Dashboard.app.Language.NAME1,
                    name: 'search_name'
                }, {
                    xtype: 'container',
                    layout: 'hbox',
                    margin: 0,
                    items: [{
                        xtype: 'textfield',
                        fieldLabel: Dashboard.app.Language.CATEGORY,
                        margin: '3px 0 3px 3px',
                        readOnly: true
                    }, {
                        xtype: 'button',
                        iconCls: 'x-fa fa-search',
                        margin: '3px 3px 3px 0',
                        handler: function () {
                            //var panel = Ext.create('myjs.tabs.categories', {
                            //    selectWindow: true,
                            //    flex: 1,
                            //    title: false,
                            //    closable: false,
                            //    listeners: {
                            //        select: function (item) {
                            //            categoryField.setValue(item.categoryName);
                            //            searchCategoryId = item.categoryId;
                            //            window.close();
                            //        }
                            //    }
                            //});
                            //var window = Ext.create('Ext.window.Window', {
                            //    border: false,
                            //    width: 800,
                            //    height: 600,
                            //    modal: true,
                            //    title: Dashboard.app.Language.CATEGORY,
                            //    layout: {
                            //        type: 'vbox',
                            //        align: 'stretch'
                            //    },
                            //    items: [panel]
                            //}).show();
                        }
                    }]
                }]
            }, {
                xtype: 'container',
                layout: 'vbox',
                items: [{
                    fieldLabel: 'შტრიხკოდი',
                    name: 'barcode'
                }, {
                    xtype: 'container',
                    layout: 'hbox',
                    items: [/*{
                        xtype: 'checkbox',
                        boxLabel: 'კრიტ. რაოდ.',
                        name: 'critical_amount',
                        margin: '0 3px 0 3px'
                    }, {
                        xtype: 'checkbox',
                        boxLabel: 'საწყობში',
                        name: 'in_storage',
                        margin: '0 3px 0 3px'
                    }*/]
                }
                ]
            }],
            buttons: [{
                xtype: 'button',
                text: 'ძებნა',//Dashboard.app.Language.SEARCH,
                iconCls: 'x-fa fa-search',
                margin: '0 3px 0 3px',
                handler: function () {
                    search();
                }
            }]
        });

        var infoBtn = Ext.create('Ext.button.Button', {
            iconCls: 'x-fa fa-info-circle',
            handler: function () {
                Ext.toast({
                    html: "<span style='background-color: #f2dede; color: #090;'> ვადა </span><br>"
                        + "<span style='background-color: #fcf8e3; color: #090;'> კრიტიკული რაოდენობა </span>",
                    title: 'ინფო',//Dashboard.app.Language.INFO,
                    width: 200,
                    align: 'br',
                    closable: true
                });
            }
        });

        var store = Ext.create('Dashboard.store.Products', {
            pageSize: limit,
            autoLoad: false,
            proxy: {
                type: 'ajax',
                actionMethods: {
                    read: 'POST'
                },
                headers: {
                    'Content-Type': 'application/json'
                },
                jsonData: true,
                url: '../api/admin/getProducts',
                limitParam: false,
                startParam: false,
                pageParam: false,
                extraParams: lastSearchData,
                paramsAsJson: true,
                reader: {
                    type: 'json',
                    totalProperty: 'total',
                    rootProperty: 'data'
                }
            }
        });

        store.on("load", function (st, records, successful, eOpts) {
            pagingToolbar.add(infoBtn);
        });

        var pagingToolbar = Ext.create('Ext.toolbar.Paging', {
            dock: 'bottom',
            displayInfo: true,
            store: store,
            listeners: {
                beforechange: function (a, pageNumber) {
                    lastSearchData.start = (pageNumber - 1) * limit;
                }
            }
        });

        var grid = Ext.create('Ext.grid.Panel', {
            store: store,
            flex: 1,
            viewConfig: {
                stripeRows: false,
                getRowClass: function (record) {
                    var color = "";
                    if (record.get('amount') < record.get('minamount')) {
                        color = "warning";
                    }
                    if (record.get('storages')) {
                        record.get('storages').forEach(function (storage) {
                            if (storage.validDate) {
                                var currentdate = new Date();
                                if ((storage.validDate - 7776000000) <= currentdate.getTime()) {
                                    color = "danger"
                                }
                            }
                        })
                    }
                    return color;
                }
            },
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: [{
                    xtype: 'button',
                    text: Dashboard.app.Language.NEW_PRODUCT,
                    iconCls: 'x-fa fa-plus-circle',
                    margin: '0 3px 0 3px',
                    handler: function () {
                        openProductWindow();
                    }
                }]
            }, pagingToolbar],
            columns: [{
                header: 'ID',
                dataIndex: 'product_id',
                width: 60
            }, {
                header: Dashboard.app.Language.NAME1,
                dataIndex: 'name',
                minWidth: 200,
                flex: 1
            }, {
                header: Dashboard.app.Language.BARCODE,
                dataIndex: 'barcode',
                width: 120
            }, {
                header: Dashboard.app.Language.CATEGORY,
                width: 120,
                dataIndex: 'category_id',
                renderer: function (v, b, r) {
                    var cn = '';
                    categoriesComboStore.data.items.forEach(function (item) {
                        if (r.data.category_id == item.data.category_id) {
                            cn = item.data.category_name;
                        }
                    });
                    return cn;
                }
            }, {
                header: Dashboard.app.Language.PRICE,
                dataIndex: 'price',
                width: 70
            }, {
                header: Dashboard.app.Language.AMOUNT,
                width: 90,
                dataIndex: 'amount'
            }, {
                header: Dashboard.app.Language.CONDITION,
                dataIndex: 'condition_id',
                width: 100,
                renderer: function (a, b, c) {
                    if (a == 1) {
                        return Dashboard.app.Language.NEW;
                    } else {
                        return Dashboard.app.Language.USED;
                    }
                }
            }, {
                header: '',
                width: 50,
                xtype: 'actioncolumn',
                editor: {
                    xtype: 'button'
                },
                items: [{
                    iconCls: 'x-fa fa-trash',
                    handler: function (grid, rowIndex, colIndex) {
                        var selectedRecord = store.getAt(rowIndex);
                        deleteProduct(selectedRecord);
                    }
                }]
            }],
            listeners: {
                itemdblclick: function (data, record, item, index, e, eOpts) {
                    openProductWindow(record)
                }
            }
        });

        grid.on('beforeitemcontextmenu', function (th, record, item, index, evt, eOpts) {
            var item;
            var items = [];
            var rows = grid.getSelectionModel().getSelection();

            if (rows.length == 1) {
                var product = grid.getSelectionModel().getSelection()[0];
                item = Ext.create('Ext.menu.Item', {
                    text: Dashboard.app.Language.EDIT,
                    iconCls: 'x-fa fa-pencil',
                    listeners: {
                        click: function (itm) {
                            openProductWindow(product);
                        }
                    }
                });
                items.push(item);

                item = Ext.create('Ext.menu.Item', {
                    text: Dashboard.app.Language.RECIEVE_PRODUCT,
                    iconCls: 'x-fa fa-plus-circle',
                    listeners: {
                        click: function (itm) {
                            var recievingWindow = Ext.create('Dashboard.view.receiving.RecievingByInvoice', {
                                product_id: product.data.product_id,
                                product: product.data,
                                title: Dashboard.app.Language.RECIEVE_PRODUCT,
                                listeners: {
                                    recievingAdded: function (item) {
                                        recievingWindow.close();
                                    }
                                }
                            }).show();
                        }
                    }
                });

                items.push(item);

                item = Ext.create('Ext.menu.Item', {
                    text: Dashboard.app.Language.RECIEVING_HISTORY,
                    iconCls: 'x-fa fa-history',
                    listeners: {
                        click: function (itm) {
                            var productRecievingWindow = Ext.create('Dashboard.view.receive.RecievingHistoryWindow', {
                                product_id: product.data.product_id,
                                title: Dashboard.app.Language.HISTORY + ' - ' + product.data.name
                            }).show();
                        }
                    }
                });
                items.push(item);

                item = Ext.create('Ext.menu.Item', {
                    text: 'ჩამოწერა',
                    iconCls: 'x-fa fa-minus-square',
                    listeners: {
                        click: function (itm) {
                            var win = Ext.create('Dashboard.view.product.productDiscardWindow', {
                                product_id: product.data.product_id,
                                product_name: product.data.name,
                                title: 'ჩამოწერა' + ' - ' + product.data.name,
                                listeners: {
                                    added: function (item) {
                                        win.close();
                                        search();
                                    }
                                }
                            }).show();
                        }
                    }
                });
                items.push(item);

                item = Ext.create('Ext.menu.Item', {
                    text: Dashboard.app.Language.DELETE,
                    iconCls: 'x-fa fa-trash',
                    listeners: {
                        click: function (itm) {
                            deleteProduct(product);
                        }
                    }
                });

                items.push(item);
            }

            var contextMenu = Ext.create('Ext.menu.Menu', {
                width: 222,
                // title : 'ოპერაციები',
                items: items
            });

            evt.stopEvent();
            contextMenu.showAt(evt.getXY());
        });

        that.items = [form, grid];

        that.callParent(arguments);

        function openProductWindow(product) {
            var productWindow = Ext.create('Dashboard.view.products.Product', {
                title: product ? ('პროდუქტი # ' + product.data.product_id + " (" + product.data.name + ")") : 'პროდუქტის დამატება',
                product_id: product ? product.data.product_id : null,
                listeners: {
                    productChanged: function (item) {
                        product.data = item;
                        product.commit();
                        // productWindow.close();
                    },
                    productAdded: function (item) {
                        store.insert(0, item);
                        productWindow.close();
                        openProductWindow(store.getAt(0));
                    },
                    reloadProduct: function () {
                        productWindow.close();
                        openProductWindow(product);
                    }
                }
            }).show();
        }

        function deleteProduct(product) {
            var msgBox = Ext.create('Ext.window.MessageBox', {
                buttonText: {
                    yes: Dashboard.app.Language.YES,
                    no: Dashboard.app.Language.NO
                }
            });

            msgBox.confirm(Dashboard.app.Language.MARK_AS_REGISTERED, '<b>' + Dashboard.app.Language.MAIN_INFO + '</b>', function (answer) {
                if (answer == 'yes') {

                    Dashboard.app.api.get('../api/admin/removeProduct/' + product.data.product_id, function (result) {
                        if (result.success) {
                            store.remove(product);
                        } else {
                            Ext.Msg.alert(Dashboard.app.Language.NOTICE, result.message)
                        }
                    });
                }
            });
        }

        if (config.categoryId >= 0) {
            searchCategoryId = config.categoryId
        }

        function search() {
            var formData = form.getForm().getFieldValues();
            lastSearchData = {
                name: formData.search_name,
                product_id: formData.product_id,
                barcode: formData.barcode,
                in_storage: formData.in_storage ? 1 : null,
                critical_amount: formData.critical_amount ? 1 : null,
                category_id: searchCategoryId,
                branch_id: formData.branch_id,
                start: start,
                limit: limit
            };
            store.proxy.extraParams = lastSearchData;
            store.loadPage(1);
        }

        function loadGrid() {
            search();
        }

        function reloadLib() {
            libs--;
            if (libs <= 0) {
                loadGrid();
                libsLoaded = true;
            }
        }

        function reloadLibs() {
            if (libsLoaded) {
                loadGrid();
                return;
            }
            reloadLib();
            //Ext.create('myjs.libs.categories').getCategories("flat", function (categories) {
            //    categoriesComboStore.loadData(categories);
            //    reloadLib();
            //});
            //
            Ext.create('Dashboard.libs.Branches').getBranches(function (result) {
                branchComboStore.loadData(result);
                form.getForm().setValues(
                    {
                        branch_id: Dashboard.statical.Config.getSessionData().branch_id
                    }
                );
                reloadLib();
            })
        }

        that.on('afterrender', function () {
            reloadLibs();
        });

    }
})
;
