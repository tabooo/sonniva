Ext.define('Dashboard.view.product.ProductInfoTypeWindow', {
    extend: 'Ext.window.Window',
    title: Dashboard.app.Language.ADD_ADDITIONAL_INFO,
    autoShow: true,
    modal: true,
    width: 400,
    bodyPadding: 10,
    constructor: function (config) {
        var that = this;

        var productInfoType = config.productInfoType;
        var product_id = config.product_id;

        var productInfoTypeStore = Ext.create('Dashboard.store.ProductInfoTypes', {
            autoLoad: true,
            proxy: {
                type: 'ajax',
                url: '../api/admin/getProductInfoTypes',
                reader: {
                    type: 'json'
                }
            }
        });

        var productInfoTypeCombo = Ext.create('Ext.form.field.ComboBox', {
            store: productInfoTypeStore,
            queryMode: 'local',
            fieldLabel: Dashboard.app.Language.INFO,
            labelWidth: 80,
            width: 250,
            name: 'product_info_type_id',
            displayField: 'product_info_type_name',
            valueField: 'product_info_type_id',
            typeAhead: true
        });

        var newProductInfoBtn = Ext.create('Ext.button.Button', {
            icon: 'icons/16x16/add.png',
            handler: function () {
                var productInfoTypeAddWindow = Ext.create('Dashboard.view.product.ProductInfoTypeAddWindow', {
                    listeners: {
                        newProductInfoTypeAdded: function (item) {
                            productInfoTypeStore.load();
                            productInfoTypeAddWindow.close();
                        }
                    }
                }).show();
            }
        });

        var valueField = Ext.create('Ext.form.field.Text', {
            labelWidth: 80,
            width: 250,
            name: 'value',
            anchor: '100%',
            fieldLabel: Dashboard.app.Language.NAME1,
            allowBlank: false
        });

        var addButton = Ext.create('Ext.button.Button', {
            text: Dashboard.app.Language.ADD,
            handler: function () {
                var request = {};
                request.product_id = product_id;
                request.product_info_type_id = productInfoTypeCombo.getValue();
                request.product_info_type_name = productInfoTypeCombo.getRawValue();
                request.value = valueField.getValue();

                Dashboard.app.api.postObject('../api/admin/addProductInfo', request, function (result) {
                    if (result.success) {
                        that.fireEvent('productInfoTypeAdded', request);
                    } else {
                        Ext.Msg.alert(Dashboard.app.Language.NOTICE, result.description)
                    }
                });

            }
        });

        that.items = [{
            xtype: 'fieldcontainer',
            layout: 'hbox',
            width: 350,
            items: [productInfoTypeCombo, newProductInfoBtn]
        }, valueField];

        that.buttons = [addButton];

        that.callParent(arguments);

        if (productInfoType) {

        }

    }
});
