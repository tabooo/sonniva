Ext.define('Dashboard.view.products.ProductMainInfo', {
    extend: 'Ext.panel.Panel',
    border: false,
    flex: 1,
    layout: 'fit',
    modal: true,
    title: Dashboard.app.Language.MAIN_INFO,
    iconCls: 'x-fa fa-home',
    padding: 5,
    constructor: function (config) {
        var that = this;

        var product_id = config.product_id;
        var product = null;

        var readOnly = config.readOnly;

        var libs = 1;
        var libsLoaded = false;

        var width = 350;
        var labelWidth = 110;

        var categoriesComboStore = Ext.create('Dashboard.store.Categories');

        var categoriessCombo = Ext.create('Ext.form.field.ComboBox', {
            store: categoriesComboStore,
            queryMode: 'local',
            fieldLabel: Dashboard.app.Language.CATEGORY,
            labelWidth: labelWidth,
            width: width,
            labelAlign: 'right',
            name: 'category_id',
            displayField: 'category_name',
            valueField: 'category_id',
            typeAhead: true,
            selectOnFocus: true,
            forceSelection: true
        });

        var unitStore = Ext.create('Dashboard.store.Units');

        var unitCombo = Ext.create('Ext.form.field.ComboBox', {
            store: unitStore,
            queryMode: 'local',
            fieldLabel: 'განზ. ერთეული',
            labelWidth: labelWidth,
            width: width,
            labelAlign: 'right',
            name: 'unit_id',
            displayField: 'unit_name',
            valueField: 'unit_id',
            typeAhead: true,
            selectOnFocus: true,
            forceSelection: true,
        });

        var amountField = Ext.create('Ext.form.field.Number', {
            fieldLabel: Dashboard.app.Language.AMOUNT,
            labelWidth: labelWidth,
            labelAlign: 'right',
            name: 'amount',
            width: width,
            readOnly: true,
            hidden: true,
        });

        var nameField = Ext.create('Ext.form.field.TextArea', {
            fieldLabel: Dashboard.app.Language.NAME1,
            labelWidth: labelWidth,
            labelAlign: 'right',
            name: 'name',
            width: width
        });

        /*var descriptionField = Ext.create('Ext.form.field.TextArea', {
            fieldLabel: Dashboard.app.Language.DESCRIPTION,
            labelWidth: labelWidth,
            labelAlign: 'right',
            name: 'description',
            width: width
        });*/

        var tinyPanelDesription = Ext.widget("tinymce_textarea", {
            xtype: 'tinymce_textarea',
            minHeight: 350,
            flex: 1,
            fieldStyle: 'font-family: Courier New; font-size: 12px;',
            style: {border: '0'},
            tinyMCEConfig: tinyCfg1,
            name: 'description',
            emptyText: 'ქართულად'
        });

        var barcodeField = Ext.create('Ext.form.field.Text', {
            fieldLabel: Dashboard.app.Language.BARCODE,
            labelWidth: labelWidth,
            labelAlign: 'right',
            name: 'barcode',
            width: width,
            hidden: true,
        });

        var minamountField = Ext.create('Ext.form.field.Number', {
            fieldLabel: Dashboard.app.Language.MIN_AMOUNT,
            labelWidth: labelWidth,
            labelAlign: 'right',
            name: 'minamount',
            width: width,
            hidden: true,
        });

        var priceField = Ext.create('Ext.form.field.Number', {
            fieldLabel: Dashboard.app.Language.PRICE,
            labelWidth: labelWidth,
            labelAlign: 'right',
            name: 'price',
            width: width,
            minValue: 0,
        });

        var selfPriceField = Ext.create('Ext.form.field.Text', {
            fieldLabel: Dashboard.app.Language.SELF_PRICE,
            labelWidth: labelWidth,
            labelAlign: 'right',
            name: 'self_price',
            width: width,
            minValue: 0,
            readOnly: true,
            hidden: true,
        });

        var lastPriceField = Ext.create('Ext.form.field.Text', {
            fieldLabel: 'ასაღები ფასი',
            labelWidth: labelWidth,
            labelAlign: 'right',
            name: 'last_price',
            width: width,
            minValue: 0,
            readOnly: true,
            hidden: true,
        });

        var conditionStore = Ext.create('Dashboard.store.Conditions');

        var conditionCombo = Ext.create('Ext.form.field.ComboBox', {
            store: conditionStore,
            queryMode: 'local',
            fieldLabel: Dashboard.app.Language.CONDITION,
            labelWidth: labelWidth,
            width: width,
            labelAlign: 'right',
            name: 'condition_id',
            displayField: 'condition_name',
            valueField: 'condition_id',
            editable: false,
            hidden: true,
        });

        conditionCombo.setValue(1);

        var form = Ext.create('Ext.form.Panel', {
            border: false,
            buttonAlign: 'center',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            defaults: {
                defaults: {
                    labelWidth: 150,
                    labelAlign: 'left',
                    anchor: '100%',
                    xtype: 'textfield',
                    enableKeyEvents: true
                }
            },
            items: [{
                xtype: 'container',
                layout: 'hbox',
                width: '100%',
                items: [
                    {
                        xtype: 'container',
                        layout: 'vbox',
                        flex: 1,
                        items: [amountField, nameField, /*descriptionField,*/]
                    },
                    {
                        xtype: 'container',
                        layout: 'vbox',
                        flex: 1,
                        items: [categoriessCombo, priceField, barcodeField, minamountField, conditionCombo, unitCombo,
                            selfPriceField, lastPriceField]
                    }]
            }, tinyPanelDesription]
        });

        var saveButton = Ext.create("Ext.Button", {
            xtype: "button",
            iconCls: 'x-fa fa-save',
            hidden: readOnly,
            text: Dashboard.app.Language.SAVE,
            handler: function () {
                var formData = form.getForm().getFieldValues();
                var requestProdact = {
                    barcode: formData.barcode,
                    category_id: formData.category_id,
                    condition_id: formData.condition_id,
                    minamount: formData.minamount,
                    name: formData.name,
                    description: modifyElement(formData.description),
                    price: formData.price,
                    product_id: product_id ? product_id : null,
                    self_price: formData.self_price,
                    last_price: formData.last_price,
                    unit_id: formData.unit_id,
                    unit_name: unitCombo.getRawValue()
                };

                Dashboard.app.api.postObject('../api/admin/addProduct', requestProdact, function (result) {
                    if (result.success) {
                        if (product_id) {
                            Ext.toast('პროდუქტი წარმატებით დარედაქტირდა');
                            that.fireEvent('productChanged', result.data)
                        } else {
                            Ext.toast('პროდუქტი წარმატებით დაემატა');
                            that.fireEvent('productAdded', result.data)
                        }
                    } else {
                        Ext.Msg.alert(Dashboard.app.Language.NOTICE, result.message)
                    }
                });
            }
        });

        that.buttons = [saveButton];

        that.items = [form];

        that.callParent(arguments);

        that.updateAmount = function (obj) {
            amountField.setValue(obj.amount);
            lastPriceField.setValue(obj.lastPrice);
            priceField.setValue(obj.price);
        };

        function loadGrid() {
            if (product_id) {
                Dashboard.app.api.get('../api/admin/getproductFullInfo/' + product_id, function (result) {
                    product = result;
                    form.getForm().setValues(result);

                    that.fireEvent('productLoaded', result);
                });
            }
        }

        function reloadLib() {
            libs--;
            if (libs <= 0) {
                loadGrid();
                libsLoaded = true;
            }
        }

        function reloadLibs() {
            if (libsLoaded) {
                loadGrid();
                return;
            }
            Dashboard.app.api.get('../api/admin/getCategoriesFlat', function (result) {
                categoriesComboStore.loadData(result);
                categoriessCombo.select(1);
                reloadLib();
            });
        }

        that.on('afterrender', function () {
            reloadLibs();
        });
    }
});
