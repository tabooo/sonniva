Ext.define('Dashboard.view.product.ProductInfo', {
    extend: 'Ext.panel.Panel',
    title: Dashboard.app.Language.ADDITIONAL_INFO,
    iconCls: 'x-fa fa-info-circle',
    autoShow: true,
    modal: true,
    flex: 1,
    layout: 'fit',
    constructor: function (config) {
        var that = this;

        var product_id = config.product_id;

        var readOnly = config.readOnly;

        var libs = 1;
        var libsLoaded = false;

        var store = Ext.create('Dashboard.store.ProductInfos');

        var productInfoGrid = Ext.create('Ext.grid.Panel', {
            store: store,
            width: '100%',
            flex: 1,
            plugins: [Ext.create('Ext.grid.plugin.RowEditing', {
                clicksToMoveEditor: 1
            })],
            columns: [{
                header: Dashboard.app.Language.NAME1,
                flex: 1,
                dataIndex: 'product_info_type_name'
            }, {
                header: Dashboard.app.Language.TEXT,
                flex: 1,
                dataIndex: 'value',
                editor: {
                    allowBlank: false
                }
            }, {
                header: '',
                width: 25,
                xtype: 'actioncolumn',
                editor: {
                    xtype: 'button'
                },
                items: [{
                    iconCls: 'x-fa fa-arrow-up',
                    isDisabled: function (grid, rowIndex, colIndex, items, rec) {
                        //if (rec.data.creatorProfileId !== curUser.user.employeeProfileId) {
                        //    return true;
                        //}
                        return false;
                    },
                    handler: function (grid, rowIndex, colIndex) {
                        var selectedRecord = store.getAt(rowIndex);
                        Dashboard.app.api.post('../api/admin/productInfo/changeOrder/' + selectedRecord.data.product_info_id + '/up', function (result) {
                            reloadLibs();
                        });
                    }
                }]
            }, {
                header: '',
                width: 25,
                xtype: 'actioncolumn',
                editor: {
                    xtype: 'button'
                },
                items: [{
                    iconCls: 'x-fa fa-arrow-down',
                    isDisabled: function (grid, rowIndex, colIndex, items, rec) {
                        //if (rec.data.creatorProfileId !== curUser.user.employeeProfileId) {
                        //    return true;
                        //}
                        return false;
                    },
                    handler: function (grid, rowIndex, colIndex) {
                        var selectedRecord = store.getAt(rowIndex);
                        Dashboard.app.api.post('../api/admin/productInfo/changeOrder/' + selectedRecord.data.product_info_id + '/down', function (result) {
                            reloadLibs();
                        });
                    }
                }]
            }, {
                header: Dashboard.app.Language.DELETE,
                width: 50,
                xtype: 'actioncolumn',
                editor: {
                    xtype: 'button'
                },
                items: [{
                    iconCls: 'x-fa fa-trash',
                    handler: function (grid, rowIndex, colIndex) {

                        var msgBox = Ext.create('Ext.window.MessageBox', {
                            buttonText: {
                                yes: Dashboard.app.Language.YES,
                                no: Dashboard.app.Language.NO
                            }
                        });

                        msgBox.confirm(Dashboard.app.Language.MARK_AS_REGISTERED, '<b>' + Dashboard.app.Language.ASK_DELETE + '</b>', function (answer) {
                            if (answer == 'yes') {

                                var selectedRecord = store.getAt(rowIndex);

                                Dashboard.app.api.get('../api/admin/removeProductInfo/' + selectedRecord.data.PRODUCT_INFO_ID, function (result) {
                                    if (result.success) {
                                        store.remove(selectedRecord);
                                    }
                                });
                            }
                        });
                    }
                }]
            }],
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: [{
                    text: Dashboard.app.Language.ADD,
                    hidden: readOnly,
                    iconCls: 'x-fa fa-plus-circle',
                    handler: function () {
                        var productInfoTypeWindow = Ext.create('Dashboard.view.product.ProductInfoTypeWindow', {
                            product_id: product_id,
                            listeners: {
                                productInfoTypeAdded: function (item) {
                                    loadGrid();
                                    productInfoTypeWindow.close();
                                }
                            }
                        }).show();
                    }
                }]
            }]
        });

        that.items = [productInfoGrid];

        that.callParent(arguments);

        function loadGrid() {
            if (product_id) {
                Dashboard.app.api.get('../api/admin/getProductInfo/' + product_id, function (result) {
                    store.loadData(result)
                });
            }
        }

        function reloadLib() {
            libs--;
            if (libs <= 0) {
                loadGrid();
                libsLoaded = true;
            }
        }

        function reloadLibs() {
            if (libsLoaded) {
                loadGrid();
                return;
            }
            reloadLib();
        }

        that.on('afterrender', function () {
            reloadLibs();
        });
    }
});
