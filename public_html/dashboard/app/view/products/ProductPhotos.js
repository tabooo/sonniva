Ext.define('Dashboard.view.product.ProductPhotos', {
    extend: 'Ext.panel.Panel',
    border: false,
    flex: 1,
    layout: 'fit',
    modal: true,
    title: 'სურათები',
    //icon:'icons/16x16/photo.png',
    iconCls: 'x-fa fa-image',
    constructor: function (config) {
        var that = this;

        var product_id = config.product_id;

        var readOnly = config.readOnly;

        var libs = 1;
        var libsLoaded = false;

        var form = Ext.create('Ext.form.Panel', {
            width: 500,
            layout: {
                type: 'hbox'
            },
            items: [{
                xtype: 'filefield',
                name: 'image[]',
                width: 400,
                fieldLabel: 'სურათი',
                labelWidth: 50,
                msgTarget: 'side',
                allowBlank: false,
                anchor: '100%',
                buttonText: 'აირჩიეთ ფაილი...'
            }, {
                xtype: 'hidden',
                name: 'product_id',
                value: product_id
            }, {
                xtype: 'button',
                text: 'ატვირთვა',
                listeners: {
                    click: function () {
                        form.submit({
                            clientValidation: true,
                            url: '../api/admin/addProductFile',
                            success: function (result) {
                                loadGrid();
                            },
                            failure: function (result) {
                                console.log(result)
                                alert('failure');
                            }
                        });
                    }
                }
            }]
        });

        var store = Ext.create('Dashboard.store.ProductPhotos');

        var productResourceGrid = Ext.create('Ext.grid.Panel', {
            store: store,
            width: '100%',
            flex: 1,
            columns: [{
                header: Dashboard.app.Language.IMAGE,
                flex: 1,
                dataIndex: 'file_name',
                renderer: function (v, a, b) {
                    return '<' + 'img src="../files/product_files/' + b.data.product_id + '/' + v + '" width="70"/>';
                }
            }, {
                header: 'წაშლა',
                width: 60,
                xtype: 'actioncolumn',
                editor: {
                    xtype: 'button'
                },
                items: [{
                    iconCls: 'x-fa fa-trash',
                    handler: function (grid, rowIndex, colIndex) {

                        var msgBox = Ext.create('Ext.window.MessageBox', {
                            buttonText: {
                                yes: Dashboard.app.Language.YES,
                                no: Dashboard.app.Language.NO
                            }
                        });

                        msgBox.confirm(Dashboard.app.Language.MARK_AS_REGISTERED, '<b>' + Dashboard.app.Language.ASK_DELETE + '</b>', function (answer) {
                            if (answer == 'yes') {
                                var selectedRecord = store.getAt(rowIndex);
                                Dashboard.app.api.get('../api/admin/removeProductFile/' + selectedRecord.data.id, function (result) {
                                    loadGrid();
                                });
                            }
                        });
                    }
                }]
            }],
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: [form]
            }]
        });

        that.items = [productResourceGrid];

        that.callParent(arguments);

        function loadGrid() {
            if (product_id) {
                Dashboard.app.api.get('../api/admin/getProductFiles/' + product_id, function (result) {
                    store.loadData(result);
                });
            }
        }

        function reloadLib() {
            libs--;
            if (libs <= 0) {
                loadGrid();
                libsLoaded = true;
            }
        }

        function reloadLibs() {
            if (libsLoaded) {
                loadGrid();
                return;
            }
            reloadLib();
        }

        that.on('afterrender', function () {
            reloadLibs();
        });
    }

});