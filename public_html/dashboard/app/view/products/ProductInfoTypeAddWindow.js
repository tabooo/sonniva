Ext.define('Dashboard.view.product.ProductInfoTypeAddWindow', {
    extend: 'Ext.window.Window',
    title: Dashboard.app.Language.ADD_NEW_ADDETIONAL_INFO_TYPE,
    autoShow: true,
    modal: true,
    width: 400,
    bodyPadding: 10,
    constructor: function (config) {
        var that = this;

        var valueField = Ext.create('Ext.form.field.Text', {
            labelWidth: 80,
            width: 250,
            name: 'value',
            anchor: '100%',
            fieldLabel: Dashboard.app.Language.NAME1,
            allowBlank: false
        });

        var addButton = Ext.create('Ext.button.Button', {
            text: Dashboard.app.Language.SAVE,
            handler: function () {
                if (!valueField.getValue()) {
                    return;
                }
                var request = {
                    product_info_type_name: valueField.getValue()
                };

                Dashboard.app.api.postObject('../api/admin/addProductInfoTypes', request, function (result) {
                    if (result.success) {
                        that.fireEvent('newProductInfoTypeAdded', result.data)
                    } else {
                        Ext.Msg.alert(Dashboard.app.Language.NOTICE, result.message)
                    }
                });
            }
        });

        that.items = [valueField];

        that.buttons = [addButton];

        that.callParent(arguments)

    }
});