Ext.define("Dashboard.view.public_users.PublicUsers", {
    extend: 'Ext.container.Container',
    //anchor: "100% -1",
    margin: 10,
    title: 'საიტის მომმხმარებლები',
    closable: true,
    layout: {
        type: "vbox",
        pack: "center"
    },
    constructor: function (config) {
        var that = this;

        var libs = 1;
        var libsLoaded = false;

        var limit = 50, start = 0;
        var lastSearchData = {
            start: start,
            limit: limit
        };

        var searchIdField = Ext.create('Ext.form.field.Text', {
            name: 'name',
            emptyText: 'სახელი',
            width: 130,
            enableKeyEvents: true,
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == e.ENTER) {
                        search();
                    }
                }
            }
        });

        var searchNameField = Ext.create('Ext.form.field.Text', {
            name: 'last_name',
            emptyText: 'გვარი',
            width: 130,
            enableKeyEvents: true,
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == e.ENTER) {
                        search();
                    }
                }
            }
        });

        var searchBtn = Ext.create('Ext.button.Button', {
            iconCls: 'x-fa fa-search',
            text: 'ძებნა',
            handler: function () {
                search();
            }
        });

        var addBtn = Ext.create('Ext.button.Button', {
            iconCls: 'x-fa fa-plus-circle',
            text: 'მომხმარებლის დამატება',
            handler: function () {
                var userPanel = Ext.create('Dashboard.view.public_users.PublicUser', {
                    listeners: {
                        added: function (item) {
                            this.close();
                            search();
                        }
                    }
                }).show();
            }
        });

        var form = Ext.create('Ext.form.Panel', {
            border: false,
            align: 'left',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [searchIdField, searchNameField],
            buttons: [searchBtn]
        });

        var store = Ext.create('Dashboard.store.PublicUser', {
            pageSize: limit,
            autoLoad: false,
            proxy: {
                type: 'ajax',
                actionMethods: {
                    read: 'POST'
                },
                headers: {
                    'Content-Type': 'application/json'
                },
                jsonData: true,
                url: '../api/admin/getPublicUsers',
                limitParam: false,
                startParam: false,
                pageParam: false,
                extraParams: lastSearchData,
                paramsAsJson: true,
                reader: {
                    type: 'json',
                    totalProperty: 'total',
                    rootProperty: 'data'
                }
            }
        });

        var pagingToolbar = Ext.create('Ext.toolbar.Paging', {
            dock: 'bottom',
            displayInfo: true,
            store: store,
            listeners: {
                beforechange: function (a, pageNumber) {
                    lastSearchData.start = (pageNumber - 1) * limit;
                }
            }
        });

        var grid = Ext.create('Ext.grid.Panel', {
            store: store,
            width: '100%',
            flex: 1,
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: [{
                    xtype: 'button',
                    text: 'განახლება',
                    iconCls: 'fa fa-refresh fa-lg',
                    handler: function () {
                        search();
                    }
                }, addBtn]
            }, pagingToolbar],
            columns: [{
                header: "ID",
                dataIndex: 'id',
                width: 90
            }, {
                header: "სახელი გვარი",
                dataIndex: 'name',
                flex: 1
            }, {
                header: "ელ. ფოსტა",
                dataIndex: 'email',
                width: 150
            }, {
                header: "ტელ. ნომერი",
                dataIndex: 'mobile_no',
                width: 150
            }, {
                //header : "",
                width: 50,
                xtype: 'actioncolumn',
                editor: {
                    xtype: 'button'
                },
                items: [{
                    iconCls: 'x-fa fa-trash',
                    handler: function (grid, rowIndex, colIndex) {

                        var selectedRecord = store.getAt(rowIndex);
                        if (selectedRecord.data.page_type_id == 1) {
                            return;
                        }

                        var msgBox = Ext.create('Ext.window.MessageBox', {
                            buttonText: {
                                yes: "დიახ",
                                no: "არა"
                            }
                        });

                        msgBox.confirm("დადასტურება", '<b>' + "გსურთ წაშლა?" + '</b>', function (answer) {
                            if (answer == 'yes') {
                                Dashboard.app.api.get('../api/admin/removePublicUsers/' + selectedRecord.data.id, function (result) {
                                    if (result.success) {
                                        store.remove(selectedRecord);
                                    } else {
                                        Ext.Msg.alert("შეტყობინება", result.message)
                                    }
                                });
                            }
                        });
                    }
                }]
            }],
            listeners: {
                itemdblclick: function (data, record, item, index, e, eOpts) {

                    var userPanel = Ext.create('Dashboard.view.public_users.PublicUser', {
                        user: record.data,
                        title: record.data.name + " " + record.data.last_name,
                        listeners: {
                            added: function (item) {
                                this.close();
                                search();
                            }
                        }
                    }).show();

                }
            }
        });

        that.items = [form, grid];

        that.callParent(arguments);

        function reloadLib() {
            libs--;
            if (libs == 0) {
                search();
                libsLoaded = true;
            }
        }

        function reLoadLibs() {
            if (libsLoaded) {
                search();
                return;
            }

            reloadLib();
        }

        function search() {
            var formData = form.getForm().getFieldValues();
            lastSearchData = {
                name: formData.name,
                last_name: formData.last_name,
                //dateFrom: startDateField.getValue() == null ? null : startDateField.getValue().getTime(),
                //dateTo: endDateField.getValue() == null ? null : endDateField.getValue().getTime(),
                start: start,
                limit: limit
            };
            store.proxy.extraParams = lastSearchData;
            store.loadPage(1);
        }

        reLoadLibs();
    }
});