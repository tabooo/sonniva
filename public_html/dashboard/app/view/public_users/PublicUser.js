Ext.define("Dashboard.view.public_users.PublicUser", {
    extend: 'Ext.Window',
    title: 'საიტის მომხარებლის დამატება',
    layout: 'fit',
    border: false,
    modal: true,
    maximizable: true,
    constructor: function (config) {
        var that = this,
            user = config.user;

        var labelWidth = 80;

        var form = Ext.create("Ext.form.Panel", {
            border: false,
            scrollable: true,
            flex: 1,
            padding: '10',
            layout: {
                type: 'vbox'
            },
            defaults: {
                width: '400',
                labelWidth: labelWidth,
                labelAlign: 'right'
            },
            defaultType: 'textfield',

            items: [{
                fieldLabel: 'სახელი',
                name: 'name'
            }, {
                fieldLabel: 'გვარი',
                name: 'last_name'
            }, {
                fieldLabel: 'ელ. ფოსტა',
                name: 'email'
            }, {
                fieldLabel: 'დაბადების თარიღი',
                name: 'birth_date'
            }, {
                fieldLabel: 'ტელეფონი',
                name: 'mobile_no'
            }, {
                fieldLabel: 'პირადი ნომერი',
                name: 'personal_no'
            }, {
                fieldLabel: 'პაროლი',
                name: 'password'
            }, {
                fieldLabel: 'აქტიური',
                name: 'is_active'
            }
            ]
        });

        that.items = [form];

        that.buttons = [{
            text: 'შენახვა',
            handler: function () {
                var request = config.pageItem ? config.pageItem : {};
                var formData = form.getForm().getFieldValues();

                request.title_ge = formData.title_ge;
                request.title_en = formData.title_en;
                request.title_ru = formData.title_ru;

                request.keyword_ge = formData.keyword_ge;
                request.keyword_en = formData.keyword_en;
                request.keyword_ru = formData.keyword_ru;

                request.google_description_ge = formData.google_description_ge;
                request.google_description_en = formData.google_description_en;
                request.google_description_ru = formData.google_description_ru;

                request.description_ge = modifyElement(formData.description_ge);
                request.description_en = modifyElement(formData.description_en);
                request.description_ru = modifyElement(formData.description_ru);
                request.disabled = formData.disabled;
                request.is_public = formData.is_public;
                request.img_path = "";


                //var imageViewStore = imageView.getStore().getRange();
                var fileManagerList = [];

                /*Ext.Array.each(imageViewStore, function (rec) {

                 if (rec.data.added) {
                 fileManagerList.push({
                 file_manager_id: rec.data.file_manager_id,
                 fileName: rec.data.record.fileName,
                 folder_id: rec.data.record.folder_id,
                 path: rec.data.record.img_path
                 });
                 }
                 });*/

                request.gallery = fileManagerList;
                request.faqs = [];

                var faqs = faqPanel.getFaqs();
                if (faqs) {
                    faqs.forEach(function (faq) {
                        request.faqs.push(faq.data)
                    })
                }

                Dashboard.app.api.postObject('../api/admin/pages/add', Ext.JSON.encode(request), function (result) {
                    if (result.success) {
                        that.setTitle('page/' + result.data[0].post_id + " - " + result.data[0].title_ge);
                        that.fireEvent("added", result.data[0]);
                    } else {
                        Ext.Msg.alert(Dashboard.app.Language.NOTICE, result.message)
                    }
                });

            }
        }];

        that.callParent(arguments);

        if (user) {
            form.getForm().setValues(user);
        }
    }
});