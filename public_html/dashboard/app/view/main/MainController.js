Ext.define('Dashboard.view.main.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.main',

    routes: {":node": "onRouteChange"},
    lastView: null,

    setCurrentView: function (x) {
        x = (x || "").toLowerCase();
        var p = this, n = p.getReferences(), v = n.mainCardPanel, r = v.getLayout(),
            u = n.navigationTreeList, m = u.getStore(),
            t = m.findNode("routeId", x) || m.findNode("view", x), o = (t && t.get("view")) || "page404",
            q = p.lastView,
            w = v.child("component[routeId=" + x + "]"), s;

        if (q && q.isWindow) {
            q.destroy()
        }
        q = r.getActiveItem();
        if (!w) {
            s = Ext.create("Dashboard.view." + o, {
                routeId: x,
                hideMode: "offsets"
            });
            //s = Ext.create({xtype: o, routeId: x, hideMode: "offsets"})
        }
        if (!s || !s.isWindow) {
            if (w) {
                if (w !== q) {
                    r.setActiveItem(w)
                }
                s = w
            } else {
                Ext.suspendLayouts();
                r.setActiveItem(v.add(s));
                Ext.resumeLayouts(true)
            }
        }
        u.setSelection(t);
        if (s.isFocusable(true)) {
            s.focus()
        }
        p.lastView = s
    },

    onNavigationTreeSelectionChange: function (e, d) {
        var f = d && (d.get("routeId") || d.get("view"));
        if (f) {
            this.redirectTo(f)
        }
        if (d.get("href")) {
            window.open(d.get("href"), d.get('hrefTarget'))
        }
    },

    onToggleNavigationSize: function () {
        var j = this, k = j.getReferences(), i = k.navigationTreeList, l = k.mainContainerWrap, h = !i.getMicro(),
            g = h ? 64 : 250;
        if (Ext.isIE9m || !Ext.os.is.Desktop) {
            Ext.suspendLayouts();
            k.senchaLogo.setWidth(g);
            i.setWidth(g);
            i.setMicro(h);
            Ext.resumeLayouts();
            l.layout.animatePolicy = l.layout.animate = null;
            l.updateLayout()
        } else {
            if (!h) {
                i.setMicro(false)
            }
            k.senchaLogo.animate({dynamic: true, to: {width: g}});
            i.width = g;
            l.updateLayout({isRoot: true});
            i.el.addCls("nav-tree-animating");
            if (h) {
                i.on({
                    afterlayoutanimation: function () {
                        i.setMicro(true);
                        i.el.removeCls("nav-tree-animating")
                    }, single: true
                })
            }
        }
    },

    onMainViewRender: function () {
        if (!window.location.hash) {
            this.redirectTo("products", true);
        } else if (window.location.hash.match(/^#elf_/)) {
            this.redirectTo("products", true);
        } else {
            this.redirectTo(window.location.hash.substring(1), true);
        }
    },

    onRouteChange: function (b) {

        this.setCurrentView(b)
    },

    onSearchRouteChange: function () {
        this.setCurrentView("searchresults")
    },

    onSwitchToModern: function () {
        Ext.Msg.confirm("Switch to Modern", "Are you sure you want to switch toolkits?", this.onSwitchToModernConfirmed, this)
    },

    onSwitchToModernConfirmed: function (d) {
        if (d === "yes") {
            var c = location.search;
            c = c.replace(/(^\?|&)classic($|&)/, "").replace(/^\?/, "");
            location.search = ("?modern&" + c).replace(/&$/, "")
        }
    },

    onEmailRouteChange: function () {
        this.setCurrentView("email")
    },

    onFileManager: function () {
        var window = Ext.create('Dashboard.view.chooser.FileManagerWindow', {
            listeners: {
                selected: function (selectedImage) {
                    imgField.setValue(selectedImage[0].data.img_path);
                    window.close();
                }
            }
        });

        window.show();
    },

    onSignout: function () {
        var RestAPI = Ext.create('Dashboard.util.RestAPI');

        localStorage.removeItem('sessionData');
        Dashboard.statical.Config.setSessionData(null);
    }

});

/*setCurrentView: function (x) {
 x = (x || "").toLowerCase();
 var p = this, n = p.getReferences(), v = n.mainCardPanel, r = v.getLayout(),
 u = n.navigationTreeList, m = u.getStore(),
 t = m.findNode("routeId", x) || m.findNode("view", x), o = (t && t.get("view")) || "page404", q = p.lastView,
 w = v.child("component[routeId=" + x + "]"), s;

 if (q && q.isWindow) {
 q.destroy()
 }
 q = r.getActiveItem();
 if (!w) {
 s = Ext.create("Dashboard.view." + o, {
 routeId: x,
 hideMode: "offsets"
 });
 }
 if (!s || !s.isWindow) {
 if (w) {
 if (w !== q) {
 r.setActiveItem(w)
 }
 s = w
 } else {
 Ext.suspendLayouts();
 r.setActiveItem(v.add(s));
 Ext.resumeLayouts(true)
 }
 }
 u.setSelection(t);
 if (s.isFocusable(true)) {
 s.focus()
 }
 p.lastView = s
 },*/