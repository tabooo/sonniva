Ext.define('Dashboard.view.main.MainContainerWrap', {
    extend: 'Ext.container.Container',
    xtype: 'maincontainerwrap',

    layout: {
        type: "hbox",
        align: "stretch",
        animate: true,
        animatePolicy: {
            x: true,
            width: true
        }
    },

    beforeLayout: function () {
        var d = this, e = Ext.Element.getViewportHeight() - 64, f = d.getComponent("navigationTreeList");
        d.minHeight = e;
        f.setStyle({"min-height": e + "px"});
        Ext.container.Container.prototype.beforeLayout.apply(this, arguments)
    }
});