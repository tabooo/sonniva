Ext.define('Dashboard.statical.Config', {
    statics: {
        sessionData: null,

        setSessionData: function (sessionData) {
            Dashboard.statical.Config.sessionData = sessionData;
        },

        getSessionData: function () {
            return Dashboard.statical.Config.sessionData;
        },

        hasRight: function (right) {
            var rights = Dashboard.statical.Config.getSessionData().permissions;

            for (var i = 0; i < rights.length; i++) {
                if (rights[i].name == right) return true;
            }

            return false;
        }
    }
});