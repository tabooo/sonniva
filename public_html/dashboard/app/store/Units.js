Ext.define('Dashboard.store.Units', {
    extend: 'Ext.data.Store',

    fields: ['unit_id', 'unit_name'],

    data: [{
        'unit_id': 1,
        'unit_name': 'ცალი'
    }, {
        'unit_id': 3,
        'unit_name': 'გრამი'
    }, {
        'unit_id': 4,
        'unit_name': 'ლიტრი'
    }, {
        'unit_id': 5,
        'unit_name': 'ტონა'
    }, {
        'unit_id': 7,
        'unit_name': 'სანტიმეტრი'
    }, {
        'unit_id': 8,
        'unit_name': 'მეტრი'
    }, {
        'unit_id': 9,
        'unit_name': 'კილომეტრი'
    }, {
        'unit_id': 10,
        'unit_name': 'კვ.სმ'
    }, {
        'unit_id': 11,
        'unit_name': 'კვ.მ'
    }, {
        'unit_id': 12,
        'unit_name': 'მ³'
    }, {
        'unit_id': 13,
        'unit_name': 'მილილიტრი'
    }, {
        'unit_id': 2,
        'unit_name': 'კგ'
    }, {
        'unit_id': 99,
        'unit_name': 'სხვა'
    }, {
        'unit_id': 14,
        'unit_name': 'შეკვრა'
    }]

});
