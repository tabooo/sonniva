Ext.define('Dashboard.store.Cities', {
    extend: 'Ext.data.Store',

    fields: ['city_id', 'name_ge', 'name_en', 'name_ru', 'price', 'delivery_time', 'state']
});
