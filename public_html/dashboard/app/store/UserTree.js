Ext.define('Dashboard.store.UserTree', {
    extend: 'Ext.data.TreeStore',

    fields: ['user_tree_id', 'user_id', 'parent_id', 'parent_path', 'user_parent_path', 'childrens', 'user'],
    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    },
    folderSort: false
});
