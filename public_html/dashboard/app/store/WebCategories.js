Ext.define('Dashboard.store.WebCategories', {
    extend: 'Ext.data.TreeStore',

    fields: [
        'news_category_id', 'name_ge', 'name_en',
        'name_ru', 'state', 'disabled', 'created_at',
        'updated_at', 'category_id', 'parent_id',
        'link', 'type', 'category', 'img_path', 'color'
    ],
    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    },
    //rootVisible: false,
    folderSort: false

});
