Ext.define('Dashboard.store.Branches', {
    extend: 'Ext.data.Store',

    fields: ['branch_id', 'branch_name', 'branch_state_id', 'car_number', 'branch_type_id']
});
