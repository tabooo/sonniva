Ext.define('Dashboard.store.ProductPhotos', {
    extend: 'Ext.data.Store',

    fields: ['id', 'product_id', 'file_name', 'is_cover']
});
