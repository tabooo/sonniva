Ext.define('Dashboard.store.ProductInfoTypes', {
	extend : 'Ext.data.Store',

	fields : [ 'product_info_type_id', 'product_info_type_name' ]
});
