Ext.define('Dashboard.store.User', {
    extend: 'Ext.data.Store',

    fields: ['id', 'name', 'email']
});