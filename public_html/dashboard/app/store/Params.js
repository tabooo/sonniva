Ext.define('Dashboard.store.Params', {
    extend: 'Ext.data.Store',

    fields: ['param_id', 'name', 'value']
});
