Ext.define('Dashboard.store.InstallmentTbcStatuses', {
    extend: 'Ext.data.Store',

    fields: ['id', 'name'],

    data: [{
        'id': 1,
        'name': 'მიმდინარე'
    }, {
        'id': 2,
        'name': 'დადასტურებული'
    }, {
        'id': 0,
        'name': 'გაუქმებული'
    }]

});
