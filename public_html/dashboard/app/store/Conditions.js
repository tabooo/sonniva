Ext.define('Dashboard.store.Conditions', {
    extend: 'Ext.data.Store',

    fields: ['condition_id', 'condition_name'],

    data: [{
        'CONDITION_ID': 1,
        'CONDITION_NAME': 'ახალი'
    }, {
        'CONDITION_ID': 2,
        'CONDITION_NAME': 'მეორადი'
    }]

});
