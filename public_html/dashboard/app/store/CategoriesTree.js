Ext.define('Dashboard.store.CategoriesTree', {
    extend: 'Ext.data.TreeStore',

    fields: ['category_id', 'category_name', 'parent_id', 'childrens', 'parents', 'insert_date', 'cancel_date', 'update_date', 'state_id'],
    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    },
    folderSort: false
});
