Ext.define('Dashboard.store.JsonAjaxProxy', {
    extend: 'Ext.data.proxy.Ajax',
    alias: 'proxy.JsonAjax',

    actionMethods: {
        create: 'POST',
        read: 'POST',
        update: 'POST',
        destroy: 'POST'
    },

    paramsAsJson: true,

    limitParam: false,
    startParam: false,
    pageParam: false
});