Ext.define('Dashboard.store.Disabled', {
    extend: 'Ext.data.Store',

    fields: [
        'disabled_id', 'disabled_name'
    ],

    data: [
        {disabled_id: 0, disabled_name: 'არა'},
        {disabled_id: 1, disabled_name: 'კი'}
    ]

});
