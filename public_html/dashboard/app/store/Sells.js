Ext.define('Dashboard.store.Sells', {
    extend: 'Ext.data.Store',

    fields: ['sell_id', 'transaction_id', 'user_id', 'whole_price', 'city_id', 'address', 'shipping_price', 'payment_type',
        'state', 'first_name', 'last_name', 'email', 'phone', 'company', 'address', 'country', 'city', 'note',]
});
