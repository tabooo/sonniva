Ext.define('Dashboard.store.PublicUser', {
    extend: 'Ext.data.Store',

    fields: ['id', 'name', 'email', 'last_name', 'birth_date', 'mobile_no', 'password', 'remember_token', 'created_at', 'updated_at', 'is_active']
});