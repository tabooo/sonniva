Ext.define('Dashboard.store.InstallmentsTBC', {
    extend: 'Ext.data.Store',

    fields: ['id', 'sell_id', 'state', 'session_id', 'status', 'create_date', 'update_date', 'sell']
});
