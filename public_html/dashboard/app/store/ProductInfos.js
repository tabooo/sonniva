Ext.define('Dashboard.store.ProductInfos', {
    extend: 'Ext.data.Store',

    fields: ['product_info_id', 'product_id', 'product_info_type_id', 'product_info_type_name', 'value', 'state_id']
});
