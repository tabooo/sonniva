Ext.define('Dashboard.store.PaymentTypes', {
    extend: 'Ext.data.Store',

    fields: ['id', 'name'],

    data: [{
        'id': 1,
        'name': 'ნაღდი'
    }, {
        'id': 2,
        'name': 'განვადება (TBC)'
    }, {
        'id': 3,
        'name': 'განვადება (Crystal)'
    }]

});
