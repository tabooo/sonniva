Ext.define('Dashboard.store.Categories', {
    extend: 'Ext.data.Store',

    fields: ['category_id', 'category_name', 'parent_id', 'childrens', 'parents', 'insert_date', 'cancel_date',
        'update_date', 'state_id', 'order']
});
