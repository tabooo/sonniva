Ext.define('Dashboard.store.Products', {
    extend: 'Ext.data.Store',

    fields: ['product_id', 'supplier_code', 'barcode', 'name', 'description', 'category_id', 'minamount', 'price', 'condiotion_id',
        'state_id', 'self_price', 'last_price', 'unit_name', 'unit_id', 'insert_date', 'cancel_date', 'update_date']
});
