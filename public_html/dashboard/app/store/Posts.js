Ext.define('Dashboard.store.Posts', {
    extend: 'Ext.data.Store',

    fields: [
        'post_id', 'title_ge', 'title_en', 'title_ru', 'description_ge', 'description_en', 'description_ru', 'img_path', 'state',
        'disabled', 'user_id', 'published_at', 'created_at', 'updated_at', 'link', 'keyword_en', 'keyword_ru', 'keyword_en',
        'google_description_ge', 'google_description_en', 'google_description_ru', 'description_ge_short', 'description_en_short',
        'description_ru_short', 'views', 'link_name', 'type', 'meta_id', 'showInGallery'
    ]

});
