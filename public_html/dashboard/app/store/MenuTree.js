Ext.define('Dashboard.store.MenuTree', {
    extend: 'Ext.data.TreeStore',

    fields: ['menu_id', 'name_ge', 'name_en', 'name_ru',
        'link', 'parent_id', 'parent_id', 'state',
        'disabled', 'editable', 'user_id',
        'created_at', 'updated_at',
        'order', 'url'],
    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
        }
    },
    //rootVisible: false,
    folderSort: false
});
