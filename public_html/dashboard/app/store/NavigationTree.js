Ext.define('Dashboard.store.NavigationTree', {
    extend: 'Ext.data.TreeStore',

    alias: 'store.dashboard',
    storeId: "NavigationTree",
    fields: [{name: "text"}],

    root: {
        expanded: true,
        children: [/*{
            text: "Dashboard",
            iconCls: "x-fa fa-desktop",
            viewType: "admindashboard",
            routeId: "dashboard",
            leaf: true
        }, */{
            text: "გაყიდვები",
            iconCls: "x-fa fa-list",
            view: "sells.Sells",
            routeId: "sells",
            leaf: true
        }, {
            text: "TBC განვადებები",
            iconCls: "x-fa fa-money",
            href: "https://installmentmerchant.tbcbank.ge/",
            hrefTarget: '_blank',
            leaf: true
        }, {
            text: "CRYSTAL განვადებები",
            iconCls: "x-fa fa-money",
            href: "https://crystalone.ge/auth",
            hrefTarget: '_blank',
            leaf: true
        }, {
            text: "CREDO განვადებები",
            iconCls: "x-fa fa-money",
            href: "http://www.shop.credo.ge",
            hrefTarget: '_blank',
            leaf: true
        }, {
            text: "პროდუქტები",
            iconCls: "x-fa fa-list",
            view: "products.Products",
            routeId: "products",
            leaf: true
        }, {
            text: "პროდუქტის კატეგორიები",
            iconCls: "x-fa fa-folder-open",
            view: "category.Categories",
            routeId: "categories",
            leaf: true
        }, {
            text: "ნავიგაცია საიტზე",
            iconCls: "x-fa fa-list",
            view: "menus.Menus",
            routeId: "menu",
            leaf: true
        }/*, {
            text: "სიახლეები",
            iconCls: "x-fa fa-newspaper-o",
            view: "news.MainPanel",
            routeId: "news",
            leaf: true
        }*/, {
            text: "გვერდები",
            iconCls: "x-fa fa-file-o",
            view: "pages.Pages",
            routeId: "pages",
            leaf: true
        }, {
            text: 'სლაიდები',
            view: "slideshow.Slides",
            iconCls: "x-fa fa fa-picture-o",
            leaf: true,
            routeId: "Slides"
        }, /*{
            text: 'ლინკები',
            view: "links.Links",
            iconCls: "x-fa fa fa-link",
            leaf: true,
            routeId: "Links"
        }, {
            text: "მომხმარებლები",
            iconCls: "x-fa fa-users",
            expanded: false,
            selectable: false,
            children: [{
                text: 'საიტის მომხმარებლები',
                view: "public_users.PublicUsers",
                iconCls: "x-fa fa fa-users",
                leaf: true,
                routeId: "PublicUsers"
            },{
                text: 'საიტის მომხმარებლები (სქემა)',
                view: "user_tree.UserTree",
                iconCls: "x-fa fa fa-tree",
                leaf: true,
                routeId: "PublicUsersTree"
            }, {
                text: "ადმინები",
                iconCls: "x-fa fa-user",
                view: "users.MainPanel",
                routeId: "users",
                leaf: true
            }, {
                text: 'უფლებები',
                view: "permissions.MainPanel",
                iconCls: "x-fa fa-exclamation-triangle",
                leaf: true,
                routeId: "permissions"
            }]
        }, */{
            text: "ბიბლიოთეკები",
            iconCls: "x-fa fa-book",
            expanded: false,
            selectable: false,
            children: [{
                text: "პარამეტრები",
                iconCls: "x-fa fa-cogs",
                view: "params.Params",
                routeId: "params",
                leaf: true
            }/*, {
                text: "ქალაქები",
                iconCls: "x-fa fa-truck",
                view: "cities.Cities",
                routeId: "cities",
                leaf: true
            }, {
                text: "ენები",
                iconCls: "x-fa fa-file-o",
                view: "lang.Langs",
                routeId: "langs",
                leaf: true
            }, {
                text: "სიახლეების კატეგორეიბი",
                iconCls: "x-fa fa-file-o",
                view: "web_category.Categories",
                routeId: "webCategories",
                leaf: true
            }*/]
        }]
    }
});


