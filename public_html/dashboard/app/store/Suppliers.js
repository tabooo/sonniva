Ext.define('Dashboard.store.Suppliers', {
    extend: 'Ext.data.Store',

    fields: ['supplier_id', 'supplier_identifier_id', 'supplier_name', 'state_id', 'debt']
});
