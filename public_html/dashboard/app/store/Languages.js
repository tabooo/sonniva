Ext.define('Dashboard.store.Languages', {
    extend: 'Ext.data.Store',

    fields: ['lang_id', 'key', 'text_ka', 'text_en', 'text_ru', 'text_tr']

});
