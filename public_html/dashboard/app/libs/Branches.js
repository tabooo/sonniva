Ext.define('Dashboard.libs.Branches', {
    getBranches: function (callback) {

        function finish() {
            var items = [];
            Ext.each(Dashboard.libs.Branches.items, function (item) {
                items.push(item);
            });
            callback(items);
        }

        if (Dashboard.libs.Branches.items) {
            finish();
        } else {
            Dashboard.app.api.get('../api/admin/getBranches', function (result) {
                Dashboard.libs.Branches.items = result;
                finish()
            });
        }
    }
});
