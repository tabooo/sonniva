Ext.define('Dashboard.util.RestAPI', {

    constructor: function (config) {
        var me = this;

        me.get = function (method) {
            var args = arguments;
            var loadCallback;

            if (typeof args[1] == 'function') {
                loadCallback = args[1];
            }

            Ext.Ajax.request({
                method: 'get',
                url: method,
                success: function (conn, response, options, eOpts) {
                    var result = Dashboard.util.Util.decodeJSONOnly(conn.responseText);

                    loadCallback(result, response);
                },

                failure: function (conn, response, options, eOpts) {
                    Dashboard.util.Util.showErrorMsg(conn.responseText);
                }
            });
        };

        me.post = function (method) {
            var params, loadCallback, parameterTypeJson;
            var args = arguments;

            if (typeof args[1] == 'function') {
                loadCallback = args[1];
                parameterTypeJson = args[2];
            } else {
                params = args[1];
                loadCallback = args[2];
                parameterTypeJson = args[3];
            }

            Ext.Ajax.request({
                method: 'post',
                url: method,
                params: (parameterTypeJson == 'jsonData') ? null : params,
                jsonData: (parameterTypeJson == 'jsonData') ? params : null,
                contentType: 'application/json; charset=utf-8',
                headers: {'Content-Type': 'application/json'},

                success: function (conn, response, options, eOpts) {
                    var result = Dashboard.util.Util.decodeJSONOnly(conn.responseText);

                    loadCallback(result, response);
                },

                failure: function (conn, response, options, eOpts) {
                    Dashboard.util.Util.showErrorMsg(conn.responseText);
                }
            });
        };

        me.postObject = function () {
            var args = Array.prototype.slice.call(arguments);

            args.push('jsonData');
            me.post.apply(me, args);
        };

        me.callParent(arguments);
    }
});