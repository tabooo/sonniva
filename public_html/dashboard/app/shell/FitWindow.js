Ext.define('Dashboard.view.shell.FitWindow', {
    extend: 'Ext.window.Window',

    constructor: function (config) {

        function computeSizes() {

            var W = window.innerWidth || document.body.offsetWidth || 0;
            var H = window.innerHeight || document.body.offsetHeight || 0;

            W -= 20 * 2;
            H -= 20 * 2;

            return [W, H];
        }

        function resize() {
            var sizes = computeSizes();
            var W = sizes[0];
            var H = sizes[1];

            that.setWidth(W);
            that.setHeight(H);
            that.center();
        }

        var that = this;

        that.callParent(arguments);

        if (that.autoShow) {
            Ext.on('resize', resize);
        }

        that.on({
            show: function () {
                Ext.on('resize', resize);
            }
        });

        resize();
    }
});