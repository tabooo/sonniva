function addItemToShoppingCart(productId, prodImg) {
    var request = {
        id: productId,
        //name: 'test',
        qty: $("#addQtyValue").val() ? $("#addQtyValue").val() : 1,
        //price: 30,
        options: {
            img: prodImg
        }

    };

    $.post($("#url").val() + '/cart/add', request, function (result) {
        getCartContent();
    })
}

function updateItemToShoppingCart(rowId, qty, reload) {
    var request = {
        rowId: rowId,
        qty: qty
    };

    $.post($("#url").val() + '/cart/updateItem', request, function (result) {
        if(reload){
            location.reload();
        }else{
            getCartContent();
        }
    })
}

function removeItemFromShoppingCart(rowId, reload) {
    $.get($("#url").val() + '/cart/remove/' + rowId, function (result) {
        if(reload){
            location.reload();
        }else{
            getCartContent();
        }
    })
}

function getCartContent() {
    $.get($("#url").val() + '/cart/getCartContent', function (result) {
        $("#cartTotalItems").html(result.count);
        $("#cartSubTotal").html(result.subtotal);

        var body = '';
        for (var key in result.cart) {
            var item = result.cart[key];
            body += '<div class="media">' +
                '<div class="media-left">' +
                ' <a href="#">' +
                '<img class="media-object img-thumbnail" src="' + item.options.img + '" width="50" alt="product">' +
                '</a></div>' +
                '  <div class="media-body">' +
                '  <a href="#" class="media-heading">' + item.name + '</a>' +
                ' <div>x' + item.qty + ' ' + item.subtotal + '</div>' +
                ' </div>' +
                ' <div class="media-right"><a href="#" data-toggle="tooltip" title="Remove" onclick="removeItemFromShoppingCart(\'' + key + '\')"><i class="fa fa-remove"></i></a></div>' +
                ' </div>';
        }
        $("#cartContent").html(body);
    })
}
