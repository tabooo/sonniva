function getProducts() {
    var url = new URL(window.location.href);
    let searchParams = new URLSearchParams(url.search);

    if (!searchParams.get('sort')) {
        window.location.search += '&sort=new'
    }
    if ($("#sortby").val()) {
        searchParams.set('sort', $("#sortby").val());
    }

    if (!searchParams.get('limit')) {
        window.location.search += '&limit=8'
    }
    if ($("#limitPerPage").val()) {
        searchParams.set('limit', $("#limitPerPage").val());
    }
    url.search = searchParams.toString();
    var new_url = url.toString();

    window.location.href = new_url;
}