function loginUser(form) {
    var username = form ? form.username.value : $("#username").val();
    var password = form ? form.password.value : $("#password").val();

    if (username == "" || password == "") {
        return;
    }

    var request = {
        email: username,
        password: password
    };

    $.post($("#url").val() + '/login', request, function (result) {
        result = JSON.parse(result);
        if (!result.success) {
            alert(result.message);
            return;
        }

        location.reload();
    })
}

function logoutUser() {
    $.get($("#url").val() + '/logout', function (result) {
        location.reload();
    })
}

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function registerUser(form) {
    var name = form.name.value;
    var last_name = form.last_name.value;
    var password = form.password.value;
    var password2 = form.password2.value;
    var email = form.email.value;
    var address = form.address.value;
    var birth_date = form.birth_date.value;
    var user_tree_id = form.user_tree_id.value;
    var city_id = form.city_id.value;
    var _token = form._token.value;

    if (name == "" || password == "") {
        alert("შეავსეთ ყველა აუცილებელი ველი!!!");
        return;
    }

    var request = {
        name: name,
        last_name: last_name,
        email: email,
        password: password,
        password2: password2,
        address: address,
        birth_date: birth_date,
        user_tree_id: user_tree_id,
        city_id: city_id,
        _token: _token
    };

    $.post($("#url").val() + '/register', request, function (result) {
        result = JSON.parse(result);
        if (!result.success) {
            alert(result.message);
            return;
        }

        location.reload();
    }).fail(function (response) {
        alert("დაფიქსირდა შეცდომა");
    })
}