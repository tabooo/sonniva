function HttpRequest(sUrl, fpCallback, method) {
	var method = method || "GET";
	
	this.request = this.createXmlHttpRequest();
	this.request.withCredentials = true;
	
	if(method === "GET") {
		this.request.open("GET", sUrl, true);
	} else {
		this.request.open("POST", sUrl);
		this.request.setRequestHeader("Content-type", "application/json; charset=utf-8");
	}
	var tempRequest = this.request;

	function request_readystatechange() {
		if (tempRequest.readyState === 4) {
			if (tempRequest.status === 200) {
				fpCallback(tempRequest.responseText);
			} else {
				console.log("An error occurred trying to contact the server.");
			}
		}
	}
	this.request.onreadystatechange = request_readystatechange;
}

HttpRequest.prototype.createXmlHttpRequest = function() {
	if (window.XMLHttpRequest) {
		var oHttp = new XMLHttpRequest();
		return oHttp;
	} else if (window.ActiveXObject) {
		var versions = [ "MSXML2.XmlHttp.6.0", "MSXML2.XmlHttp.3.0" ];

		for (var i = 0; i < versions.length; i++) {
			try {
				var oHttp = new ActiveXObject(versions[i]);
				return oHttp;
			} catch (error) {
				console.log("Your browser does not support ajax " + error);
			}
		}
	}
	return null;
};

HttpRequest.prototype.send = function(msg) {
	var msg = msg || null;
	this.request.send(msg);
};