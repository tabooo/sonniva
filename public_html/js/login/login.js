var Login = function () {
    var r = function () {
        $(".login-form").validate({
            errorElement: "span",
            errorClass: "help-block",
            focusInvalid: !1,
            rules: {username: {required: !0}, password: {required: !0}, remember: {required: !1}},
            messages: {username: {required: "Username is required."}, password: {required: "Password is required."}},
            invalidHandler: function (r, e) {
                $(".alert-danger-login").hide();
                $(".alert-danger", $(".login-form")).show()
            },
            highlight: function (r) {
                $(r).closest(".form-group").addClass("has-error")
            },
            success: function (r) {
                r.closest(".form-group").removeClass("has-error"), r.remove()
            },
            errorPlacement: function (r, e) {
                r.insertAfter(e.closest(".input-icon"))
            },
            submitHandler: function (r) {
                $.post('admin/login', {
                    "email": r[0].value,
                    "password": r[1].value,
                    "_token": r[2].value,
                }, function (data) {
                    var response = JSON.parse(data);

                    if (response.success) {
                        localStorage.setItem('sessionData', JSON.stringify(response.data));
                        var tmpUrl = window.location.href + '/dashboard';
                        tmpUrl = tmpUrl.replace('admin/', '');
                        location.href = tmpUrl;
                    } else {
                        $(".alert-danger", $(".login-form")).hide();
                        $(".alert-danger-login").show();
                    }
                }).fail(function () {
                    $(".alert-danger", $(".login-form")).hide();
                    $(".alert-danger-login").show();
                });
            }
        }), $(".login-form input").keypress(function (r) {
            return 13 == r.which ? ($(".login-form").validate().form() && $(".login-form").submit(), !1) : void 0
        }), $(".forget-form input").keypress(function (r) {
            return 13 == r.which ? ($(".forget-form").validate().form() && $(".forget-form").submit(), !1) : void 0
        }), $("#forget-password").click(function () {
            $(".login-form").hide(), $(".forget-form").show()
        }), $("#back-btn").click(function () {
            $(".login-form").show(), $(".forget-form").hide()
        })
    };
    return {
        init: function () {
            r();
        }
    }
}();
jQuery(document).ready(function () {
    Login.init()
});
