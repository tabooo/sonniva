-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 14, 2020 at 03:28 PM
-- Server version: 10.2.27-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pixlge1_gdc`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `state_id` tinyint(1) NOT NULL DEFAULT 1,
  `branch_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `state_id`, `branch_id`) VALUES
(1, 'Admin', 'mamuka@gmail.com', '$2y$10$b9Rs1OvVD/BThlNHmSGaauHgeTzWZsvj9IQJe.GH8hKudziWP0IRG', 'HbfZnDuTl3sS18NrcqNAkj15dKuHmTUeHztZlNU9V9mFazZNVs9anaNZPf00', '2017-05-04 07:45:54', '2017-05-04 07:45:54', 1, 1),
(2, 'Testa', 'admintesta@gmail.com', '$2y$10$b9Rs1OvVD/BThlNHmSGaauHgeTzWZsvj9IQJe.GH8hKudziWP0IRG', 'lySZSZtN5LGugVJ549qph3UmYFh22h3KSUKVT6Lr5KR2yDbcdTtV6b6qIoT5', '2017-05-04 07:45:54', '2017-05-04 07:45:54', 1, 1),
(3, 'Super', 'adminsuper@gmail.com', '$2y$10$b9Rs1OvVD/BThlNHmSGaauHgeTzWZsvj9IQJe.GH8hKudziWP0IRG', 'lySZSZtN5LGugVJ549qph3UmYFh22h3KSUKVT6Lr5KR2yDbcdTtV6b6qIoT5', '2017-05-04 07:45:54', '2017-05-04 07:45:54', 1, 1),
(4, 'Admin1', 'admin1@gmail.com', '$2y$10$b9Rs1OvVD/BThlNHmSGaauHgeTzWZsvj9IQJe.GH8hKudziWP0IRG', 'lySZSZtN5LGugVJ549qph3UmYFh22h3KSUKVT6Lr5KR2yDbcdTtV6b6qIoT5', '2017-05-04 07:45:54', '2017-05-04 07:45:54', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `branch_id` int(11) NOT NULL,
  `branch_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `branch_state_id` int(2) NOT NULL DEFAULT 1,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`branch_id`, `branch_name`, `branch_state_id`, `updated_at`, `created_at`) VALUES
(1, 'რუსთავი', 1, NULL, NULL),
(2, 'თბილისი', 1, NULL, NULL),
(3, 'გორი', 1, NULL, NULL),
(4, 'xinkali', 1, '2017-05-03 13:39:36', '2017-05-03 09:39:36');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `childrens` text DEFAULT NULL,
  `parents` text DEFAULT NULL,
  `cancel_date` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  `state_id` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `parent_id`, `childrens`, `parents`, `cancel_date`, `created_at`, `updated_at`, `state_id`) VALUES
(1, '– ყველა –', NULL, '74,79,80,81,82', NULL, NULL, '0000-00-00 00:00:00', '2017-05-08 11:17:19', 1),
(74, 'ტელევიზორები', 1, '76', '1', NULL, '0000-00-00 00:00:00', '2017-05-18 09:40:43', 1),
(76, 'LED ტელევიზორები', 74, NULL, '74', NULL, '0000-00-00 00:00:00', '2017-05-17 14:08:45', 1),
(79, 'ციფრული ტექნიკა', 1, '83,85', '1', NULL, '0000-00-00 00:00:00', '2017-05-17 14:06:31', 1),
(80, 'ტელეფონი/ტაბლეტი', 1, NULL, '1', NULL, '0000-00-00 00:00:00', '2017-05-17 14:07:37', 1),
(81, 'კომპიუტერული ტექნიკა', 1, NULL, '1', NULL, '0000-00-00 00:00:00', '2017-05-17 14:07:49', 1),
(82, 'საყოფაცხოვრებო ტექნიკა', 1, NULL, '1', '2017-05-02 15:24:57', '2017-05-02 09:15:13', '2017-05-17 14:08:01', 1),
(83, 'ფოტო კამერა', 79, NULL, '79', NULL, '2017-05-08 07:08:44', '2017-05-17 14:09:17', 1),
(84, 'asdadsa', 79, NULL, '79', '2017-05-08 11:17:19', '2017-05-08 07:11:44', '2017-05-08 11:17:19', 2),
(85, 'ვიდე კამერა', 79, NULL, '79', NULL, '2017-05-08 07:15:29', '2017-05-17 14:09:32', 1),
(86, 'წვრილი ტექნიკა', 1, NULL, '1', NULL, '2017-05-17 10:08:13', '2017-05-17 14:08:13', 1),
(87, 'ჩასაშენებელი ტექნიკა', 1, NULL, '1', NULL, '2017-05-17 10:08:26', '2017-05-17 14:08:26', 1),
(88, '3D ტელევიზორები', 74, NULL, '74', NULL, '2017-05-17 10:08:55', '2017-06-01 12:01:58', 1),
(89, 'მობილური ტელეფონი', 80, NULL, '80', NULL, '2017-05-17 10:10:02', '2017-05-17 14:10:03', 1),
(90, 'პლანშეტი', 80, NULL, '80', NULL, '2017-05-17 10:11:06', '2017-05-17 14:11:06', 1),
(91, 'ჭკვიანი საათი', 80, NULL, '80', NULL, '2017-05-17 10:11:17', '2017-05-17 14:11:17', 1),
(92, 'მაცივარი', 82, NULL, '82', NULL, '2017-05-17 10:11:32', '2017-05-17 14:11:32', 1),
(93, 'სარეცხი მანქანა', 82, NULL, '82', NULL, '2017-05-17 10:11:43', '2017-05-17 14:11:43', 1),
(94, 'საშრობი', 82, NULL, '82', NULL, '2017-05-17 10:11:51', '2017-05-17 14:11:52', 1),
(95, 'ჭურჭლის სარეცხი მანქანა', 82, NULL, '82', NULL, '2017-05-17 10:12:00', '2017-05-17 14:12:01', 1),
(96, 'ქურა', 82, NULL, '82', NULL, '2017-05-17 10:12:15', '2017-05-17 14:12:15', 1),
(97, 'გამწოვი', 82, NULL, '82', NULL, '2017-05-17 10:12:21', '2017-05-17 14:12:21', 1),
(98, 'ჩაიდანი', 86, NULL, '86', NULL, '2017-05-18 07:37:52', '2017-05-18 11:37:52', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `city_id` int(11) NOT NULL,
  `name_ge` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `name_en` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `name_ru` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `delivery_time` int(2) DEFAULT NULL,
  `state` int(2) DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`city_id`, `name_ge`, `name_en`, `name_ru`, `price`, `delivery_time`, `state`, `created_at`, `updated_at`) VALUES
(1, 'თბილისი', 'Tbilisi', 'Tbilisi', 5.00, 1, 1, '2017-12-25 12:45:15', '2017-12-25 17:35:39'),
(2, 'რუსთავი', 'Rustavi', 'Rustavi', 3.00, 2, 1, '2017-12-25 12:46:09', '2017-12-25 17:35:37');

-- --------------------------------------------------------

--
-- Table structure for table `langs`
--

CREATE TABLE `langs` (
  `lang_id` int(11) NOT NULL,
  `key` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `text_ka` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `text_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `text_ru` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `text_tr` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `langs`
--

INSERT INTO `langs` (`lang_id`, `key`, `text_ka`, `text_en`, `text_ru`, `text_tr`, `created_at`, `updated_at`) VALUES
(1, 'ACTION', 'მოქმედება', 'Action', 'Действие', 'Action', '0000-00-00 00:00:00', NULL),
(2, 'ACTIVE', 'აქტიური', 'Active', 'Активный', 'Active', '0000-00-00 00:00:00', NULL),
(3, 'ADD', 'დამატება', 'Add', 'Добавить', 'Add', '0000-00-00 00:00:00', NULL),
(4, 'ADDRESS', 'მისამართი', 'Address', 'Адрес', 'Address', '0000-00-00 00:00:00', NULL),
(5, 'CANCEL', 'გაუქმება', 'Cancel', 'Отменить', 'Cancel', '0000-00-00 00:00:00', NULL),
(6, 'CHANGE_PASSWORD', 'პაროლის შეცვლა', 'Change Password', 'Сменить пароль', 'Change Password', '0000-00-00 00:00:00', NULL),
(7, 'CLEAR', 'გასუფთავება', 'Clear', 'Очистить', 'Clear', '0000-00-00 00:00:00', NULL),
(8, 'CLOSE', 'დახურვა', 'Close', 'Закрыть', 'Close', '0000-00-00 00:00:00', NULL),
(9, 'DATE', 'თარიღი', 'Date', 'Дата', 'Date', '0000-00-00 00:00:00', NULL),
(10, 'DELETE', 'წაშლა', 'Delete', 'Удалить', 'Delete', '0000-00-00 00:00:00', NULL),
(11, 'EDIT', 'რედაქტირება', 'Edit', 'Редактировать', 'Edit', '0000-00-00 00:00:00', NULL),
(12, 'ERROR', 'შეცდომა', 'Error', 'Ошибка', 'Error', '0000-00-00 00:00:00', NULL),
(13, 'EXIT', 'გასვლა', 'Logout', 'Выход', 'Logout', '0000-00-00 00:00:00', NULL),
(14, 'EXPORT', 'ექსპორტი', 'Export', 'Экспорт', 'Export', '0000-00-00 00:00:00', NULL),
(15, 'FINALIZE', 'დასრულება', 'Finalize', 'Завершить', 'Finalize', '0000-00-00 00:00:00', NULL),
(16, 'FIRST_NAME', 'სახელი', 'Firstname', 'Имя', 'Firstname', '0000-00-00 00:00:00', NULL),
(17, 'FROM_WHO', 'ვისგან', 'From', 'Из', 'From', '0000-00-00 00:00:00', NULL),
(18, 'HISTORY', 'ისტორია', 'History', 'История', 'History', '0000-00-00 00:00:00', NULL),
(19, 'IDENTITY_CODE', 'საიდენტიფიკაციო კოდი', 'Identity code', 'Идентификационный код', 'Identity code', '0000-00-00 00:00:00', NULL),
(20, 'INACTIVE', 'არააქტიური', 'არააქტიური', 'Неактивный', 'არააქტიური', '0000-00-00 00:00:00', NULL),
(21, 'INFO', 'ინფო', 'Info', 'Инфо', 'Info', '0000-00-00 00:00:00', NULL),
(22, 'IP_ADDRESS', 'IP მისამართი', 'IP Address', 'IP адрес', 'IP Address', '0000-00-00 00:00:00', NULL),
(23, 'JULY', 'ივლისი', 'July', 'Июль', 'July', '0000-00-00 00:00:00', NULL),
(24, 'JUNE', 'ივნისი', 'June', 'Июнь', 'June', '0000-00-00 00:00:00', NULL),
(25, 'LANGUAGE', 'ენა', 'Language', 'Язык', 'Language', '0000-00-00 00:00:00', NULL),
(26, 'LAST_NAME', 'გვარი', 'Lastname', 'Фамилия', 'Lastname', '0000-00-00 00:00:00', NULL),
(27, 'LOGIN', 'შესვლა', 'Login', 'Войти', 'Login', '0000-00-00 00:00:00', NULL),
(28, 'MAIN_INFO', 'ძირითადი ინფორმაცია', 'Basic Information', 'Основная информация', 'Basic Information', '0000-00-00 00:00:00', NULL),
(29, 'MARCH', 'მარტი', 'March', 'Март', 'March', '0000-00-00 00:00:00', NULL),
(30, 'MAY', 'მაისი', 'May', 'Май', 'May', '0000-00-00 00:00:00', NULL),
(31, 'NEW_PASSWORD', 'ახალი პაროლი', 'New password', 'Новый пароль', 'New password', '0000-00-00 00:00:00', NULL),
(32, 'NO_RECORDS_FOUND', 'ვერ მოიძებნა ჩანაწერები', 'No records found', 'Записей не найдено', 'No records found', '0000-00-00 00:00:00', NULL),
(33, 'NOTE', 'შენიშვნა', 'Note', 'Note', 'Note', '0000-00-00 00:00:00', NULL),
(34, 'NOVEMBER', 'ნოემბერი', 'November', 'Ноябрь', 'November', '0000-00-00 00:00:00', NULL),
(35, 'OCTOBER', 'ოქტომბერი', 'October', 'Октябрь', 'October', '0000-00-00 00:00:00', NULL),
(36, 'OLD_PASSWORD', 'ძველი პაროლი', 'Old password', 'Старый пароль', 'Old password', '0000-00-00 00:00:00', NULL),
(37, 'OPTIONS', 'პარამეტრები', 'Settings', 'Опции', 'Settings', '0000-00-00 00:00:00', NULL),
(38, 'ORGANISATION', 'ორგანიზაცია', 'Organisation', 'Организация', 'Organisation', '0000-00-00 00:00:00', NULL),
(39, 'OVERDUE', 'ვადაგასული', 'Overdue', 'Истекший', 'Overdue', '0000-00-00 00:00:00', NULL),
(40, 'OWN', 'საკუთარი', 'Own', 'Собственный', 'Own', '0000-00-00 00:00:00', NULL),
(41, 'PAGE', 'გვერდი', 'Page', 'Страница', 'Page', '0000-00-00 00:00:00', NULL),
(42, 'PAGES', 'გვერდები', 'Pages', 'Страницы', 'Pages', '0000-00-00 00:00:00', NULL),
(43, 'PASSWORD', 'პაროლი', 'Password', 'Пароль', 'Password', '0000-00-00 00:00:00', NULL),
(44, 'PERSONAL', 'პირადი', 'Personal', 'Личное', 'Personal', '0000-00-00 00:00:00', NULL),
(45, 'PLEASE_WAIT', 'გთხოვთ, დაელოდოთ', 'Please Wait...', 'Пожалуйста, подождите', 'Please Wait...', '0000-00-00 00:00:00', NULL),
(46, 'PRINT', 'ბეჭდვა', 'Print', 'Печать', 'Print', '0000-00-00 00:00:00', NULL),
(47, 'PRINT_PREVIEW', 'საბეჭდად გადახედვა', 'Print Preview', 'Предварительный просмотр', 'Print Preview', '0000-00-00 00:00:00', NULL),
(48, 'REFRESH', 'განახლება', 'Refresh', 'Обновить', 'Refresh', '0000-00-00 00:00:00', NULL),
(49, 'REPEAT_NEW_PASSWORD', 'გაიმეორეთ ახალი პაროლი', 'Repeat new password', 'Повторите новый пароль', 'Repeat new password', '0000-00-00 00:00:00', NULL),
(50, 'RESULT', 'შედეგი', 'Result', 'Результат', 'Result', '0000-00-00 00:00:00', NULL),
(51, 'SAVE', 'შენახვა', 'Save', 'Сохранить', 'Save', '0000-00-00 00:00:00', NULL),
(52, 'SEARCH', 'ძებნა', 'Search', 'Поиск', 'Search', '0000-00-00 00:00:00', NULL),
(53, 'SELECT', 'არჩევა', 'Select', 'Выбирать', 'Select', '0000-00-00 00:00:00', NULL),
(54, 'SELECT_A_FILE', 'აირჩიეთ ფაილი', 'Select a file', 'Выберите файл', 'Select a file', '0000-00-00 00:00:00', NULL),
(55, 'SEND', 'გადაგზავნა', 'Send', 'Переслать', 'Send', '0000-00-00 00:00:00', NULL),
(56, 'SEPTEMBER', 'სექტემბერი', 'September', 'Сентябрь', 'September', '0000-00-00 00:00:00', NULL),
(57, 'STATES', 'სტატუსები', 'Statuses', 'Статусы', 'Statuses', '0000-00-00 00:00:00', NULL),
(58, 'STATE', 'სტატუსი', 'State', 'Статус', 'State', '0000-00-00 00:00:00', NULL),
(59, 'TIME', 'დრო', 'Time', 'Время', 'Time', '0000-00-00 00:00:00', NULL),
(60, 'TITLE', 'სათაური', 'Title', 'название', 'Title', '0000-00-00 00:00:00', NULL),
(61, 'TEXT', 'ტექსტი', 'Text', 'Текст', 'Text', '0000-00-00 00:00:00', NULL),
(62, 'TO_ARCHIVE', 'დაარქივება', 'Archive', 'Архивировать', 'Archive', '0000-00-00 00:00:00', NULL),
(63, 'TO_WHO', 'ვის', 'To', 'Кому', 'To', '0000-00-00 00:00:00', NULL),
(64, 'TOTAL', 'სულ', 'Total', 'Всего', 'Total', '0000-00-00 00:00:00', NULL),
(65, 'TYPE', 'ტიპი', 'Type', 'Вид', 'Type', '0000-00-00 00:00:00', NULL),
(66, 'USERNAME', 'მომხმარებელი', 'User', 'Пользователь', 'User', '0000-00-00 00:00:00', NULL),
(67, 'VERSION', 'ვერსია', 'Version', 'Версия', 'Version', '0000-00-00 00:00:00', NULL),
(68, 'VIEW_ALL', 'ყველას ნახვა', 'View All', 'View All', 'View All', '0000-00-00 00:00:00', NULL),
(69, 'PARCEL_NO', 'ამანათი', 'Parcel #', 'посылка', 'Parcel #', '0000-00-00 00:00:00', NULL),
(70, 'MARK_AS_REGISTERED', 'დადასტურება', 'დადასტურება', 'подтвердить', 'დადასტურება', '0000-00-00 00:00:00', NULL),
(71, 'FILENAMES', 'ფაილები', 'filenames', 'файлы', 'filenames', '0000-00-00 00:00:00', NULL),
(72, 'ADD_INGREDIENT', 'ინგრედიენტის დამატება', 'Add ingredient', 'добавить ингридиент', 'Add ingredient', '0000-00-00 00:00:00', NULL),
(73, 'ADMINISTRATOR', 'ადმინისტრატორი', 'Administrator', 'администратор', 'Administrator', '0000-00-00 00:00:00', NULL),
(74, 'AMOUNT', 'რაოდენობა', 'Amount', 'количество', 'Amount', '0000-00-00 00:00:00', NULL),
(75, 'ASK_DELETE', 'გსურთ წაშლა?', 'Do you want to delete?', 'Удалить?', 'Do you want to delete?', '0000-00-00 00:00:00', NULL),
(76, 'CASHIER', 'მოლარე', 'Cashier', 'кассир', 'Cashier', '0000-00-00 00:00:00', NULL),
(77, 'CATEGORIES', 'კატეგორიები', 'Categories', 'категории', 'Categories', '0000-00-00 00:00:00', NULL),
(78, 'CATEGORY', 'კატეგორია', 'Category', 'категория', 'Category', '0000-00-00 00:00:00', NULL),
(79, 'CHOOSE_SECTION', 'მიუთითეთ სექცია', 'Choose section', 'укажите секцию', 'Choose section', '0000-00-00 00:00:00', NULL),
(80, 'DEFAULT_COLOR', 'თავდაპირველი ფერი', 'Default color', 'начальный цвет', 'Default color', '0000-00-00 00:00:00', NULL),
(81, 'DESCRIPTION', 'აღწერა', 'Description', 'описание', 'Description', '0000-00-00 00:00:00', NULL),
(82, 'FOODS', 'კერძები', 'Foods', 'блюда', 'Foods', '0000-00-00 00:00:00', NULL),
(83, 'IMAGE', 'სურათი', 'Image', 'изображение', 'Image', '0000-00-00 00:00:00', NULL),
(84, 'INGREDIENTS', 'ინგრედიენტები', 'Ingredients', 'ингридиенты', 'Ingredients', '0000-00-00 00:00:00', NULL),
(85, 'MIN_AMOUNT', 'კრიტ. რაოდ.', 'კრიტ. რაოდ.', 'критическое количество', 'კრიტ. რაოდ.', '0000-00-00 00:00:00', NULL),
(86, 'NAME1', 'დასახელება', 'Name', 'Наименование', 'Name', '0000-00-00 00:00:00', NULL),
(87, 'NAME2', 'სახელი', 'Name', 'Имя', 'Name', '0000-00-00 00:00:00', NULL),
(88, 'NAVIGATION', 'ნავიგაცია', 'Navigation', 'навигация', 'Navigation', '0000-00-00 00:00:00', NULL),
(89, 'NEW_SELL', 'ახალი გაყიდვა', 'New sell', 'новая продажа', 'New sell', '0000-00-00 00:00:00', NULL),
(90, 'NOTICE', 'შეტყობინება', 'Notice', 'сообщение', 'Notice', '0000-00-00 00:00:00', NULL),
(91, 'PLACE', 'ადგილი', 'Place', 'место', 'Place', '0000-00-00 00:00:00', NULL),
(92, 'PRICE', 'ფასი', 'Price', 'цена', 'Price', '0000-00-00 00:00:00', NULL),
(93, 'RECIEVINGS', 'მიღებები', 'Recievings', 'закупки', 'Recievings', '0000-00-00 00:00:00', NULL),
(94, 'REPORTS', 'რეპორტები', 'Reports', 'отчеты', 'Reports', '0000-00-00 00:00:00', NULL),
(95, 'SELLS', 'გაყიდვები', 'Sells', 'продажи', 'Sells', '0000-00-00 00:00:00', NULL),
(96, 'TABLES', 'მაგიდები', 'Tables', 'столы', 'Tables', '0000-00-00 00:00:00', NULL),
(97, 'SECTIONS', 'სექციები', 'Sections', 'секции', 'Sections', '0000-00-00 00:00:00', NULL),
(98, 'PLACES', 'ადგილები', 'Places', 'места', NULL, '0000-00-00 00:00:00', NULL),
(99, 'NEW_CATEGORY', 'ახალი კატეგორია', 'New category', 'новая категория', 'New category', '0000-00-00 00:00:00', NULL),
(100, 'ADD_CATEGORY', 'კატეგორიის დამატება', 'Add category', 'добавить категорию', 'Add category', '0000-00-00 00:00:00', NULL),
(101, 'EDIT_CATEGORY', 'კატეგორიის რედაქტირება', 'Edit category', 'редактирование категории', 'Edit category', '0000-00-00 00:00:00', NULL),
(102, 'ONE_PRICE', 'ერთ. ფასი', 'One price', 'цена единицы', 'One price', '0000-00-00 00:00:00', NULL),
(103, 'WHOLEPRICE', 'ჯამი', 'Whole price', 'сумма', 'Whole price', '0000-00-00 00:00:00', NULL),
(104, 'INVOICE_NO', 'ინვოისის №', 'Invoice №', '№ Инвойса', 'Invoice №', '0000-00-00 00:00:00', NULL),
(105, 'PROVIDER', 'მომწოდებელი', 'Provider', 'поставщик', 'Provider', '0000-00-00 00:00:00', NULL),
(106, 'MANUFACTURER', 'მწარმოებელი', 'Manufacturer', 'изготовитель', 'Manufacturer', '0000-00-00 00:00:00', NULL),
(107, 'YES', 'დიახ', 'Yes', 'Да', 'Yes', '0000-00-00 00:00:00', '2017-05-02 13:21:05'),
(108, 'NO', 'არა', 'No', 'Нет', 'No', '0000-00-00 00:00:00', NULL),
(109, 'NEW_INGREDIENT', 'ახალი ინგრედიენტი', 'New ingredient', 'новый ингридиент', 'New ingredient', '0000-00-00 00:00:00', NULL),
(110, 'RECIEVE_INGREDIENT', 'ინგრედიენტის მიღება', 'Recieve ingredient', 'прием ингридиента', 'Recieve ingredient', '0000-00-00 00:00:00', NULL),
(111, 'CHOOSE_TABLE', 'მიუთითეთ მაგიდა', 'Choose table', 'укажите стол', 'Choose table', '0000-00-00 00:00:00', NULL),
(112, 'CASH', 'ნაღდი', 'Cash', 'наличные', 'Cash', '0000-00-00 00:00:00', NULL),
(113, 'TRANSFER', 'გადარიცხვა', 'Transfer', 'безналичные', 'Transfer', '0000-00-00 00:00:00', NULL),
(114, 'NEW_PLACE', 'ახალი ადგილი', 'New place', 'новое место', 'New place', '0000-00-00 00:00:00', NULL),
(115, 'ADD_PLACE', 'ადგილის დამატება', 'Add place', 'добавить место', 'Add place', '0000-00-00 00:00:00', NULL),
(116, 'ADD_INGREDIENT_ON_PRODUCT', 'პროდუქტზე ინგრედიენტის დამატება', 'Add ingredient on product', 'добавить ингридиент в продукт', 'Add ingredient on product', '0000-00-00 00:00:00', NULL),
(117, 'INGREDIENT', 'ინგრედიენტი', 'Ingredient', 'ингридиент', 'Ingredient', '0000-00-00 00:00:00', NULL),
(118, 'NEW_PRODUCT', 'ახალი პროდუქტი', 'new Product', 'Новый товар', 'new Product', '0000-00-00 00:00:00', NULL),
(119, 'ADD_PRODUCT', 'კერძის დამატება', 'Add product', 'добавить блюдо', 'Add product', '0000-00-00 00:00:00', NULL),
(120, 'ADD_SECTION', 'სექციის დამატება', 'Add section', 'добавить секцию', 'Add section', '0000-00-00 00:00:00', NULL),
(121, 'SECTION', 'სექცია', 'Section', 'секциа', 'Section', '0000-00-00 00:00:00', NULL),
(122, 'FILL_ALL_FIELDS', 'გთხოვთ მიუთითოთ ყველა აუცილებელი ველი', 'Fill all required fields', 'заполните все обязательные поля', 'Fill all required fields', '0000-00-00 00:00:00', NULL),
(123, 'ADD_TABLE', 'მაგიდის დამატება', 'Add table', 'добавить стол', 'Add table', '0000-00-00 00:00:00', NULL),
(124, 'NEW_TABLE', 'ახალი მაგიდა', 'New table', 'новый стол', 'New table', '0000-00-00 00:00:00', NULL),
(125, 'TABLE', 'მაგიდა', 'Table', 'стол', 'Table', '0000-00-00 00:00:00', NULL),
(126, 'CANCEL_ORDER', 'შეკვეთის გაუქმება', 'Cancel order', 'отмена заказа', 'Cancel order', '0000-00-00 00:00:00', NULL),
(127, 'USERS', 'მომხმარებლები', 'Users', 'пользователи', 'Users', '0000-00-00 00:00:00', NULL),
(128, 'ADD_USER', 'მომხმარებლის დამატება', 'User added', 'добавить пользователя', 'User added', '0000-00-00 00:00:00', NULL),
(129, 'RANK', 'რანკი', 'Rank', 'ранг', 'Rank', '0000-00-00 00:00:00', NULL),
(130, 'PERSONAL_NO', 'პირადი ნომერი', 'Personal No', 'личный номер', 'Personal No', '0000-00-00 00:00:00', NULL),
(131, 'PHONE', 'ტელეფონი', 'Phone', 'телефон', 'Phone', '0000-00-00 00:00:00', NULL),
(132, 'EMAIL', 'ელ. ფოსტა', 'eMail', 'эл.почта', 'eMail', '0000-00-00 00:00:00', NULL),
(133, 'PRINTER', 'პრინტერი', 'Printer', 'Принтер', 'Printer', '0000-00-00 00:00:00', NULL),
(134, 'WAITER', 'ოფიციანტი', 'Waiter', 'официант', 'Waiter', '0000-00-00 00:00:00', NULL),
(135, 'SELLITEM_REPORTS', 'კერძების რეპორტები', 'კერძების რეპორტები', 'Отчеты по блюдам', 'კერძების რეპორტები', '0000-00-00 00:00:00', NULL),
(136, 'ORDER_NUMBER', 'შეკვ. №', 'Order №', 'Заказ. №', 'Order №', '0000-00-00 00:00:00', NULL),
(137, 'SEARCH_ORDER', 'შეკვეთის ძებნა', 'Search Order', 'Поиск заказа', 'Search Order', '0000-00-00 00:00:00', NULL),
(138, 'SELF_PRICE', 'თვითღირებულება', 'Self Price', 'Себестоимость', 'Self Price', '0000-00-00 00:00:00', NULL),
(139, 'CALL', 'გამოძახება', 'CALL', 'Вызов', 'CALL', '0000-00-00 00:00:00', NULL),
(140, 'MESSAGE', 'შეტყობინება', 'Message', 'Сообщение', 'Message', '0000-00-00 00:00:00', NULL),
(141, 'CONFIRM', 'დასტური', 'Confirm', 'Подтвердить', 'Confirm', '0000-00-00 00:00:00', NULL),
(142, 'MENUS', 'მენიუები', 'Menus', 'Меню', 'Menus', '0000-00-00 00:00:00', NULL),
(143, 'ADD_MENU', 'მენიუს დამატება', 'Add Menu', 'Добавить меню', 'Add Menu', '0000-00-00 00:00:00', NULL),
(144, 'PERCENT', 'პროცენტი', 'Percent', 'Процент', 'Percent', '0000-00-00 00:00:00', NULL),
(145, 'TAKE_MONEY', 'მოწ. თანხა', 'Money', 'Поданная сумма', 'Money', '0000-00-00 00:00:00', NULL),
(146, 'RETURN_CHANGE', 'ხურდა', 'Change', 'Сдача', 'Change', '0000-00-00 00:00:00', NULL),
(147, 'MESSAGES', 'შეტყობინებები', 'Messages', 'Сообщения', 'Messages', '0000-00-00 00:00:00', NULL),
(148, 'INGREDIENTS_REPORTS', 'ინგრედიენტების რეპორტები', 'Ingredients Reports', 'Отчеты по ингридиентам', 'Ingredients Reports', '0000-00-00 00:00:00', NULL),
(149, 'OPERATIONS', 'ოპერაციები', 'Operations', 'Операции', 'Operations', '0000-00-00 00:00:00', NULL),
(150, 'RECIEVING_HISTORY', 'მიღებების ისტორია', 'Recieving History', 'История закупок', 'Recieving History', '0000-00-00 00:00:00', NULL),
(151, 'SELL_HISTORY', 'გაყიდვების ისტორია', 'Sell History', 'История продаж', 'Sell History', '0000-00-00 00:00:00', NULL),
(152, 'GUEST_COUNT', 'სტუმრების რაოდენობა', 'Guest Count', 'Кол-во гостей', 'Guest Count', '0000-00-00 00:00:00', NULL),
(153, 'VIEW_ORDER', 'შეკვეთის ნახვა', 'View Order', 'Смотреть заказ', 'View Order', '0000-00-00 00:00:00', NULL),
(154, 'ADD_IMAGE', 'სურათის დამატება', 'Add Image', 'Добавить картинку', 'Add Image', '0000-00-00 00:00:00', NULL),
(155, 'SEND_MSG', 'მიწერე მიმტანს', 'Send Message', 'Послать сообщение', 'Send Message', '0000-00-00 00:00:00', NULL),
(156, 'ADDITIONAL_INFO', 'დამატებითი ინფო', 'Additional Info', 'Доп. Инфо', 'Additional Info', '0000-00-00 00:00:00', NULL),
(157, 'ADD_ADDITIONAL_INFO', 'დამატებითი ინფოს დამატება', 'Add Additional Info', 'Добавить доп. инфо', 'Add Additional Info', '0000-00-00 00:00:00', NULL),
(158, 'ADD_NEW_ADDETIONAL_INFO_TYPE', 'ახალი დამატებითი ინფოს დამატება', 'Add new additional info type', 'Добавить новое доп. инфо', 'Add new additional info type', '0000-00-00 00:00:00', NULL),
(159, 'CHOOSE_FILE', 'აირჩიეთ ფაილი', 'Choose file', 'Выбрать файл', 'Choose file', '0000-00-00 00:00:00', NULL),
(160, 'RESOURCES', 'რესურსები', 'Resources', 'Ресурсы', 'Resources', '0000-00-00 00:00:00', NULL),
(161, 'FILE_SIZE_ERROR_30', 'ფაილის ზომა არ უნდა აღემატებოდეს 30 kb-ს', 'Max allowed file size is 30 kb', 'Размер файла не должен превышать 30 Кб', 'Max allowed file size is 30 kb', '0000-00-00 00:00:00', NULL),
(162, 'FILE_SIZE_ERROR_500', 'ფაილის ზომა არ უნდა აღემატებოდეს 500 kb-ს', 'Max allowed file size is 500 kb', 'Размер файла не должен превышать 500 Кб', 'Max allowed file size is 500 kb', '0000-00-00 00:00:00', NULL),
(163, 'CHOOSE_FILE_ERROR', 'გთხოვთ მიუთითოთ ფაილი', 'Choose file', 'Укажите файл', 'Choose file', '0000-00-00 00:00:00', NULL),
(164, 'RECIEVE_PRODUCT', 'პროდუქტის მიღება', 'Recieve product', 'Принять товар', 'Recieve product', '0000-00-00 00:00:00', NULL),
(165, 'SPLIT_AMOUNT', 'დაშლილი რაოდენობა', 'Split Amount', 'Штучное кол-во', 'Split Amount', '0000-00-00 00:00:00', NULL),
(166, 'BARCODE', 'შტრიხკოდი', 'Barcode', 'Штрихкод', 'Barcode', '0000-00-00 00:00:00', NULL),
(167, 'IN_STORAGE', 'საწყობში', 'In Storage', 'В складе', 'In Storage', '0000-00-00 00:00:00', NULL),
(168, 'RECIEVE_BY_INVOICE', 'ინვოისით მიღება', 'Recieve by Invoice', 'Прием по инвойсу', 'Recieve by Invoice', '0000-00-00 00:00:00', NULL),
(169, 'WEIGHT', 'წონა', 'Weight', 'Вес', 'Weight', '0000-00-00 00:00:00', NULL),
(170, 'WARRANTY', 'გარანტია', 'Warranty', 'Гарантия', 'Warranty', '0000-00-00 00:00:00', NULL),
(171, 'CONDITION', 'მდგომარეობა', 'Condition', 'Состояние', 'Condition', '0000-00-00 00:00:00', NULL),
(172, 'NEW', 'ახალი', 'New', 'Новый', 'New', '0000-00-00 00:00:00', NULL),
(173, 'USED', 'მეორადი', 'Used', 'Вторичный', 'Used', '0000-00-00 00:00:00', NULL),
(174, 'PRICE_WHOLESALE', 'ფასი საბითუმო', 'Price Wholesale', 'Оптовая цена', 'Price Wholesale', '0000-00-00 00:00:00', NULL),
(175, 'PRICE_VIP', 'ფასი VIP', 'Price VIP', 'Цена VIP', 'Price VIP', '0000-00-00 00:00:00', NULL),
(176, 'SPLIT', 'დაშლა', 'Split', 'Поштучно', 'Split', '0000-00-00 00:00:00', NULL),
(177, 'FILL_SPLIT_AMOUNT', 'მიუთითეთ დაშლის რაოდენობა', 'Fill Split Amount', 'Укажите поштучное кол-во', 'Fill Split Amount', '0000-00-00 00:00:00', NULL),
(178, 'INVOICE_NUMBER', 'ინვოისის ნომერი', 'Invoice Number', 'Номер инвойса', 'Invoice Number', '0000-00-00 00:00:00', NULL),
(179, 'FILL_ALL_IMPORTANT_FIELDS', 'შეავსეთ ყველა სავალდებულო ველი', 'Fill all important fields', 'Заполните все обязательные поля', 'Fill all important fields', '0000-00-00 00:00:00', NULL),
(180, 'SEARCH_PRODUCT', 'პროდუქტის ძებნა', 'Search Product', 'Поиск товара', 'Search Product', '0000-00-00 00:00:00', NULL),
(181, 'BRANCH', 'ფილიალი', 'Branch', 'Филиал', 'Branch', '0000-00-00 00:00:00', NULL),
(182, 'WHOLE', 'მთლიანი', 'Whole', 'Целый', 'Whole', '0000-00-00 00:00:00', NULL),
(183, 'PRODUCTS', 'პროდუქტები', 'Products', 'Товары', 'Products', '0000-00-00 00:00:00', NULL),
(184, 'CLIENTS', 'კლიენტები', 'Clients', 'Клиенты', 'Clients', '0000-00-00 00:00:00', NULL),
(185, 'PAYMENTS', 'გადახდები', 'Payments', 'Платежи', 'Payments', '0000-00-00 00:00:00', NULL),
(186, 'ADD_PAYMENT', 'გადახდა', 'New Payment', 'Оплатить', 'New Payment', '0000-00-00 00:00:00', NULL),
(187, 'PAYMENT_TYPE', 'გადახდის ტიპი', 'Payment Type', 'Тип оплаты', 'Payment Type', '0000-00-00 00:00:00', NULL),
(188, 'MONEY', 'თანხა', 'Money', 'Сумма', 'Money', '0000-00-00 00:00:00', NULL),
(189, 'CHANGE', 'შეცვლა', 'Change', 'Изменить', 'Change', '0000-00-00 00:00:00', NULL),
(190, 'SELLER', 'გამყიდველი', 'Seller', 'Продавец', 'Seller', '0000-00-00 00:00:00', NULL),
(191, 'SPLIT_PRICE', 'დაშლის ფასი', 'Split Price', 'Поштучнай цена', 'Split Price', '0000-00-00 00:00:00', NULL),
(192, 'DATABASE_NULL', 'ბაზის განულება', 'Delete Database', 'Обнулить базу', 'Delete Database', '0000-00-00 00:00:00', NULL),
(193, 'DATABASE_NULLED_OK', 'ბაზა განულდა წარმატებით', 'Database was nulled succesfuly', 'База обнулена успешно', 'Database was nulled succesfuly', '0000-00-00 00:00:00', NULL),
(194, 'REMOVED', 'გაუქმებული', 'Removed', 'Отенено', 'Removed', '0000-00-00 00:00:00', NULL),
(195, 'CHANGE_LANGUAGE', 'ენის შეცვლა', 'Change Language', 'Изменить язык', 'Change Language', '0000-00-00 00:00:00', NULL),
(196, 'CHANGE_THEME', 'თემის შეცვლა', 'Change Theme', 'Изменить тему', 'Change Theme', '0000-00-00 00:00:00', NULL),
(197, 'BONUS', 'ბონუსი', 'Bonus', 'Бонус', 'Bonus', '0000-00-00 00:00:00', NULL),
(198, 'NEW_CLIENT', 'ახალი კლიენტი', 'New Client', 'Новый клиент', 'New Client', '0000-00-00 00:00:00', NULL),
(199, 'CLIENT', 'კლიენტი', 'Client', 'Клиент', 'Client', '0000-00-00 00:00:00', NULL),
(200, 'CHOOSE_CLIENT', 'კლიენტის არჩევა', 'Choose Client', 'Выбрать клиента', 'Choose Client', '0000-00-00 00:00:00', NULL),
(201, 'AMOUNT_IN_STORAGE', 'რაოდ. საწყობში', 'Amount in Storage', 'Кол-во на складе', 'Amount in Storage', '0000-00-00 00:00:00', NULL),
(202, 'NEED_RECEIPT', 'საჭიროებს რეცეპტს', 'Need Receipt', 'Нужен рецепт', 'Need Receipt', '0000-00-00 00:00:00', NULL),
(203, 'EARNING', 'მოგება', 'Earning', 'Прибыль', 'Earning', '0000-00-00 00:00:00', NULL),
(204, 'SELL_PRICE', 'გასაყიდი ფასი', 'Sell Price', 'Продажная цена', 'Sell Price', '0000-00-00 00:00:00', NULL),
(205, 'VAT', 'დ.ღ.გ.', 'VAT', 'Н.Д.С.', 'VAT', '0000-00-00 00:00:00', NULL),
(206, 'INCLUDE_VAT', 'დ.ღ.გ.–ის ჩათვლით', 'Include VAT', 'С учетом НДС', 'Include VAT', '0000-00-00 00:00:00', NULL),
(207, 'WITHOUT_VAT', 'დ.ღ.გ.–ის გარეშე', 'Without VAT', 'Без НДС', 'Without VAT', '0000-00-00 00:00:00', NULL),
(208, 'CONFIRM_PRODUCT_SELL_PRICE_CHANGE', 'პროდუქტის გასაყიდი ფასი შეიცვალა. გინდათ შეიცვალოს პროდუქტის გასაყიდი ფასი?', 'Product sell price was changed. Do you want to change product sell price with new price?', 'Продажная ценв товара изменилась.', 'Product sell price was changed. Do you want to change product sell price with new price?', '0000-00-00 00:00:00', NULL),
(209, 'CALCULATE_PERCENT', 'პროცენტის გამოთვლა', 'Calculate Percent', 'Подсчет процентов', 'Calculate Percent', '0000-00-00 00:00:00', NULL),
(210, 'FROM', 'დან', 'From', 'От', 'From', '0000-00-00 00:00:00', NULL),
(211, 'BEFORE', 'მდე', 'Before', 'До', 'Before', '0000-00-00 00:00:00', NULL),
(212, 'PRODUCT', 'პროდუქტი', 'Product', 'Продукт', 'Product', '0000-00-00 00:00:00', NULL),
(213, 'SOLD_PRODUCTS', 'გაყიდული პროდუქტები', 'Sold Products', 'Продано Продукты', 'Sold Products', '0000-00-00 00:00:00', NULL),
(214, 'THIS_PRODUCT_IS_NOT_IN_STORAGE', 'მითითებული პროდუქტი არ არის საწყობში', 'This product is not in storage', 'Этот продукт не в памяти', 'This product is not in storage', '0000-00-00 00:00:00', NULL),
(215, 'CHOOSE_PRODUCT', 'აირჩიეთ პროდუქტი', 'Choose Product', 'Выберите продукт', 'Choose Product', '0000-00-00 00:00:00', NULL),
(216, 'SELECTED_NUMBER_IS_GREATER_THAN_THE_NUMBER_OF_STOCK', 'მითითებული რაოდენობა მეტია საწყობში არსებულ რაოდენობაზე', 'Set the number is greater than the number of stock', 'Установите число больше, чем число акций', 'Set the number is greater than the number of stock', '0000-00-00 00:00:00', NULL),
(217, 'SPECIFY_THE_NUMBER_OF_SALE', 'მიუთითეთ გასაყიდი რაოდენობა', 'Specify the number of sale', 'Укажите количество продажи', 'Specify the number of sale', '0000-00-00 00:00:00', NULL),
(218, 'ADD_SUBCATEGORY', 'ქვეკატეგორიის დამატება', 'Add Subcategory', 'Добавить подкатегорию', 'Add Subcategory', '0000-00-00 00:00:00', NULL),
(219, 'PARENT', 'მშობელი', 'Parent', 'Родитель', 'Parent', '0000-00-00 00:00:00', NULL),
(220, 'DISCARD_INGREDIENT', 'ინგრედიენტის ჩამოწერა', 'Discard Ingredient', 'ინგრედიენტის ჩამოწერა', 'Discard Ingredient', '0000-00-00 00:00:00', NULL),
(221, 'DISCARDED_INGREDIENTS', 'ჩამოწერილი ინრედიენტები', 'Discarded Ingredients', 'ჩამოწერილი ინრედიენტები', 'Discarded Ingredients', '0000-00-00 00:00:00', NULL),
(222, 'DISCARD_AMOUNT', 'ჩამოსაწერი რაოდენობა', 'Discard Amount', 'ჩამოსაწერი რაოდენობა', 'Discard Amount', '0000-00-00 00:00:00', NULL),
(223, 'DATABASE', 'ბაზა', 'Database', 'ბაზა', 'Database', '0000-00-00 00:00:00', NULL),
(224, 'DOWNLOAD_DATABASE_FOR_WEB', 'ბაზის ჩამოტვირთვა საიტისთვის', 'Download Database for WEB', 'ბაზის ჩამოტვირთვა საიტისთვის', 'Download Database for WEB', '0000-00-00 00:00:00', NULL),
(225, 'REMAINS', 'ნაშთები', 'Remains', 'ნაშთები', 'Remains', '0000-00-00 00:00:00', NULL),
(226, 'PRE_CHECK', 'წინასწარი ჩეკი', 'Pre Check', 'წინასწარი ჩეკი', 'Pre Check', '0000-00-00 00:00:00', NULL),
(227, 'SALE_PERCENT', 'ფასდაკ. %', 'Sale %', 'ფასდაკლება %', 'Sale %', '0000-00-00 00:00:00', NULL),
(228, 'CODE', 'კოდი', 'Code', 'Code', 'Code', '0000-00-00 00:00:00', NULL),
(229, 'INCORRECT_VALUES', 'არასწორი მონაცემები!', 'Incorrect values', 'Incorrect values', 'Incorrect values', '0000-00-00 00:00:00', NULL),
(230, 'DAY_TRADED_VOLUME', 'ღის ნავაჭრი', 'Day trading volume', 'Day trading volume', 'Day trading volume', '0000-00-00 00:00:00', NULL),
(231, 'MAIN_PAGE', 'მთავარი გვერდი', 'Main Page', 'Main Page', 'Main Page', '0000-00-00 00:00:00', NULL),
(232, 'COMMENT', 'კომენტარი', 'Comment', 'Comment', 'Comment', '0000-00-00 00:00:00', NULL),
(233, 'ORDERS', 'შეკვეთები', 'Orders', 'Orders', 'Orders', '0000-00-00 00:00:00', NULL),
(234, 'SELLING_PRICE_HAS_NOT_SET', 'გასაყიდი ფასი არ აქვს მითითებული', 'Selling price has not set', 'Selling price has not set', 'Selling price has not set', '0000-00-00 00:00:00', NULL),
(235, 'NEW_SECTION', 'ახალი სექცია', 'New Saction', 'New Saction', 'New Saction', '0000-00-00 00:00:00', NULL),
(236, 'PORTION', 'პორცია', 'Portion', 'Portion', 'Portion', '0000-00-00 00:00:00', NULL),
(237, 'ADD_TYPE', 'ტიპის დამატება', 'Add Type', 'Add Type', 'Add Type', '0000-00-00 00:00:00', NULL),
(238, 'AMOUNT_SHORT', 'რაოდ.', 'Am.', 'Am.', 'Am.', '0000-00-00 00:00:00', NULL),
(239, 'FOR_SERVICE', 'მომსახურების', 'For Service', 'For Service', 'For Service', '0000-00-00 00:00:00', NULL),
(240, 'GEL', 'ლ', 'GEL', 'GEL', 'GEL', '0000-00-00 00:00:00', NULL),
(241, 'SALE', 'ფასდაკლება', 'Sale', 'Sale', 'Sale', '0000-00-00 00:00:00', NULL),
(242, 'PLACE_OF_MAKING', 'გატანის ადგილი', 'Place Of Making', 'Place Of Making', 'Place Of Making', '0000-00-00 00:00:00', NULL),
(243, 'SUM', 'ჯამი', 'Sum', 'Sum', 'Sum', '0000-00-00 00:00:00', NULL),
(244, 'LANGUAGES', 'ენები', 'Languages', 'Languages', 'Languages', '0000-00-00 00:00:00', NULL),
(245, 'NEW_PHRASE', 'ახალი ფრაზა', 'New Phrase', 'New Phrase', 'New Phrase', '0000-00-00 00:00:00', NULL),
(246, 'TURKISH', 'თურქულად', 'Turkish', 'Turkish', 'Turkish', '0000-00-00 00:00:00', NULL),
(247, 'ENGLISH', 'ინგლისურად', 'English', 'English', 'English', '0000-00-00 00:00:00', NULL),
(248, 'RUSSIAN', 'რუსულად', 'Russian', 'Russian', 'Russian', '0000-00-00 00:00:00', NULL),
(249, 'GEORGIAN', 'ქართულად', 'Georgian', 'Georgian', 'Georgian', '0000-00-00 00:00:00', NULL),
(250, 'WAYBILLS', 'ზედნადებები', 'Waybills', 'Waybills', 'Waybills', '0000-00-00 00:00:00', NULL),
(251, 'WAYBILL_NO', 'ზედნადებების №', 'Waybill No', 'Waybill No', 'Waybill No', '0000-00-00 00:00:00', NULL),
(252, 'SELLER_NAME', 'გამყიდველი', 'Seller Name', 'Seller Name', 'Seller Name', '0000-00-00 00:00:00', NULL),
(253, 'ACTIVATE_DATE', 'აქტივაციის თარიღი', 'Activate Date', 'Activate Date', 'Activate Date', '0000-00-00 00:00:00', NULL),
(254, 'CREATE_DATE', 'შექმნის თარიღი', 'Create Date', 'Create Date', 'Create Date', '0000-00-00 00:00:00', NULL),
(255, 'CLOSE_DATE', 'დასრულების თარიღი', 'Close Date', 'Close Date', 'Close Date', '0000-00-00 00:00:00', NULL),
(256, 'DRIVER_NAME', 'მძღოლი', 'Driver Name', 'Driver Name', 'Driver Name', '0000-00-00 00:00:00', NULL),
(257, 'CAR', 'ავტო', 'Car', 'Car', 'Car', '0000-00-00 00:00:00', NULL),
(258, 'TRANS_PRICE', 'ტრანს. თანხა', 'Trans. Price', 'Trans. Price', 'Trans. Price', '0000-00-00 00:00:00', NULL),
(259, 'DELIVERY_DATE', 'მიწოდების თარიღი', 'Delivery Date', 'Delivery Date', 'Delivery Date', '0000-00-00 00:00:00', NULL),
(260, 'VIEW', 'ნახვა', 'View', 'View', 'View', '0000-00-00 00:00:00', NULL),
(261, 'CONFIRM_RECEIVE', 'მიღების დადასტურება', 'Confirm Receive', 'Confirm Receive', 'Confirm Receive', '0000-00-00 00:00:00', NULL),
(262, 'BAR_CODE', 'შტრიხკოდი', 'Barcode', 'Barcode', 'Barcode', '0000-00-00 00:00:00', NULL),
(263, 'ADD_BARCODE', 'შტრიხკოდის დამატება', 'Add Barcode', 'Add Barcode', 'Add Barcode', '0000-00-00 00:00:00', NULL),
(264, 'RECEIVE', 'მიღება', 'Receive', 'Receive', 'Receive', '0000-00-00 00:00:00', NULL),
(265, 'CAR_NO', 'მანქანის №', 'Car No', 'Car No', 'Car No', '0000-00-00 00:00:00', NULL),
(266, 'GRANT_RIGHTS', 'უფლებების მინიჭება', 'Grant Rights', 'Grant Rights', 'Grant Rights', '0000-00-00 00:00:00', NULL),
(267, 'SUPPLIERS', 'მომწოდებლები', 'Suppliers', 'Suppliers', 'Suppliers', '0000-00-00 00:00:00', NULL),
(268, 'TRADED', 'ნავაჭრი', 'Traded', 'Traded', 'Traded', '0000-00-00 00:00:00', NULL),
(269, 'RECEIPT', 'რეცეპტი', 'Receipt', 'Receipt', 'Receipt', '0000-00-00 00:00:00', NULL),
(270, 'STORAGE', 'საწყობი', 'Storage', 'Storage', 'Storage', '0000-00-00 00:00:00', NULL),
(271, 'DISCARDS', 'ჩამოწერები', 'Discards', 'Discards', 'Discards', '0000-00-00 00:00:00', NULL),
(272, 'PAYED_MONEY', 'გადახდილი თანხა', 'Payed Money', 'Payed Money', 'Payed Money', '0000-00-00 00:00:00', NULL),
(273, 'DEADLINE', 'ვადა', 'Deadline', 'Deadline', 'Deadline', '0000-00-00 00:00:00', NULL),
(275, 'UPLOAD_TO_RS', 'აიტვირთოს rs.ge-ზე', 'Upload to RS', 'Upload to RS', 'Upload to RS', '0000-00-00 00:00:00', NULL),
(276, 'CARD', 'ბარათი', 'Card', 'Card', 'Card', '0000-00-00 00:00:00', NULL),
(277, 'DEBT', 'ვალი', 'Debt', 'Debt', 'Debt', '0000-00-00 00:00:00', NULL),
(278, 'TIN', 'საიდენტიფიკაციო ნომერი', 'TIN', 'TIN', 'TIN', '0000-00-00 00:00:00', NULL),
(279, 'DRIVERS', 'მძღოლები', 'Drivers', 'Drivers', 'Drivers', '0000-00-00 00:00:00', NULL),
(280, 'DRIVER', 'მძღოლი', 'Driver', 'Driver', 'Driver', '0000-00-00 00:00:00', NULL),
(281, 'CHOOSE_DRIVER', 'აირჩიეთ მძღოლი', 'Select Driver', 'Select Driver', 'Select Driver', '0000-00-00 00:00:00', NULL),
(282, 'NEW_DRIVER', 'ახალი მძღოლი', 'New Driver', 'New Driver', 'New Driver', '0000-00-00 00:00:00', NULL),
(283, 'CARS', 'მანქანები', 'Cars', 'Cars', 'Cars', '0000-00-00 00:00:00', NULL),
(284, 'STORAGES', 'საწყობები', 'Storages', 'Storages', 'Storages', '0000-00-00 00:00:00', NULL),
(285, 'ADD_STORAGE', 'საწყობის დამატება', 'Add Storage', 'Add Storage', 'Add Storage', '0000-00-00 00:00:00', NULL),
(286, 'MOVE_PRODUCTS', 'პროდუქტების გადატანა', 'Move Products', 'Move Products', 'Move Products', '0000-00-00 00:00:00', NULL),
(287, 'TRUCK', '????????', 'Truck', 'Truck', 'Truck', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_05_02_140156_entrust_setup_tables', 2),
(4, '2017_05_05_144232_entrust_setup_tables', 3),
(5, '2017_06_01_102836_create_admins_table', 4),
(6, '2017_06_05_074505_entrust_setup_tables', 5),
(7, '2017_06_05_095027_entrust_setup_tables', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `transaction_id` varchar(32) DEFAULT NULL,
  `payment_id` varchar(12) DEFAULT NULL,
  `payment_date` varchar(19) DEFAULT NULL,
  `amount` varchar(12) DEFAULT NULL,
  `card_number` varchar(25) DEFAULT NULL,
  `card_type` varchar(25) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `reason` varchar(64) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`transaction_id`, `payment_id`, `payment_date`, `amount`, `card_number`, `card_type`, `status`, `reason`, `created_at`, `updated_at`) VALUES
('5a4f8d7c7b52f', '800518020824', '05.01.2018 18:36:51', '616600', NULL, 'CRTU!478763******4584', 'Y', '966729', '2018-01-05 14:38:51', '2018-01-05 14:38:51'),
('5a4f8dd94549c', '800518020827', '05.01.2018 18:38:25', '616600', NULL, 'CRTU!478763******4584', 'Y', '197394', '2018-01-05 14:40:27', '2018-01-05 14:40:27'),
('5a4f90b909dc2', '800518020881', '05.01.2018 18:50:40', '616600', NULL, 'CRTU!478763******4584', 'Y', '853485', '2018-01-05 14:52:55', '2018-01-05 14:52:55'),
('5a4f9161a100f', '800518020898', '05.01.2018 18:53:28', '616600', NULL, 'CRTU!478763******4584', 'Y', '968976', '2018-01-05 14:55:38', '2018-01-05 14:55:38'),
('5a4f9049cd23c', '800518020872', '05.01.2018 18:49:04', '616600', NULL, 'CRTU!478763******4584', 'Y', '526913', '2018-01-05 14:56:15', '2018-01-05 14:56:15'),
('5a560b9c912e0', '801016019186', '10.01.2018 16:48:29', '269100', NULL, 'CRTU!478763******4584', 'Y', '108993', '2018-01-10 12:50:39', '2018-01-10 12:50:39'),
('5a560c706f67f', '801016019205', '10.01.2018 16:51:56', '269100', NULL, 'CRTU!478763******4584', 'Y', '397842', '2018-01-10 12:54:00', '2018-01-10 12:54:00'),
('5a560ca4438bc', '801016019211', '10.01.2018 16:52:57', '269100', NULL, 'CRTU!478763******4584', 'Y', '430669', '2018-01-10 12:54:58', '2018-01-10 12:54:58'),
('5a560df01fa50', '801017019235', '10.01.2018 16:58:23', '269100', NULL, 'CRTU!478763******4584', 'Y', '971066', '2018-01-10 13:00:25', '2018-01-10 13:00:25'),
('5a560e14eb961', '801017019238', '10.01.2018 16:58:56', '269100', NULL, 'CRTU!478763******4584', 'Y', '234511', '2018-01-10 13:00:57', '2018-01-10 13:00:57'),
('5a560f5877682', '801017019257', '10.01.2018 17:04:22', '269100', NULL, 'CRTU!478763******4584', 'Y', '644362', '2018-01-10 13:06:26', '2018-01-10 13:06:26'),
('5a560f96dd990', '801017019260', '10.01.2018 17:05:22', '269100', NULL, 'CRTU!478763******4584', 'Y', '349498', '2018-01-10 13:07:21', '2018-01-10 13:07:21'),
('5a56104919237', '801017019275', '10.01.2018 17:08:22', '269100', NULL, 'CRTU!478763******4584', 'Y', '906794', '2018-01-10 13:10:40', '2018-01-10 13:10:40'),
('5a561150906d6', '801017019297', '10.01.2018 17:12:44', '269100', NULL, 'CRTU!478763******4584', 'Y', '711663', '2018-01-10 13:15:13', '2018-01-10 13:15:13'),
('5a56104919237', '801017019275', '10.01.2018 17:08:22', '269100', NULL, 'CRTU!478763******4584', 'Y', '906794', '2018-01-10 13:15:53', '2018-01-10 13:15:53'),
('5a561150906d6', '801017019297', '10.01.2018 17:12:44', '269100', NULL, 'CRTU!478763******4584', 'Y', '711663', '2018-01-10 13:20:23', '2018-01-10 13:20:23'),
('5a56104919237', '801017019275', '10.01.2018 17:08:22', '269100', NULL, 'CRTU!478763******4584', 'Y', '906794', '2018-01-10 13:26:01', '2018-01-10 13:26:01'),
('5a561150906d6', '801017019297', '10.01.2018 17:12:44', '269100', NULL, 'CRTU!478763******4584', 'Y', '711663', '2018-01-10 13:30:30', '2018-01-10 13:30:30'),
('5a56104919237', '801017019275', '10.01.2018 17:08:22', '269100', NULL, 'CRTU!478763******4584', 'Y', '906794', '2018-01-10 13:41:20', '2018-01-10 13:41:20'),
('5a561150906d6', '801017019297', '10.01.2018 17:12:44', '269100', NULL, 'CRTU!478763******4584', 'Y', '711663', '2018-01-10 13:45:35', '2018-01-10 13:45:35'),
('5a56104919237', '801017019275', '10.01.2018 17:08:22', '269100', NULL, 'CRTU!478763******4584', 'Y', '906794', '2018-01-10 14:01:27', '2018-01-10 14:01:27'),
('5a561150906d6', '801017019297', '10.01.2018 17:12:44', '269100', NULL, 'CRTU!478763******4584', 'Y', '711663', '2018-01-10 14:05:45', '2018-01-10 14:05:45'),
('yGx6SBGook6yDBdCCO9rVg==', '801018019540', '10.01.2018 18:09:47', '269100', NULL, 'CRTU!478763******4584', 'Y', '567766', '2018-01-10 14:12:00', '2018-01-10 14:12:00'),
('eUd4NlNCR29vazZ5REJkQ0NPOXJWZz09', '801018019554', '10.01.2018 18:12:18', '269100', NULL, 'CRTU!478763******4584', 'Y', '503590', '2018-01-10 14:14:24', '2018-01-10 14:14:24'),
('5a56104919237', '801017019275', '10.01.2018 17:08:22', '269100', NULL, 'CRTU!478763******4584', 'Y', '906794', '2018-01-10 14:26:33', '2018-01-10 14:26:33');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Test 1', 'Test 1', 'Test 1', '2017-06-05 05:55:58', '2017-06-05 05:55:58'),
(2, 'Test 2', 'Test 2', 'Test 2', '2017-06-05 05:56:04', '2017-06-05 05:56:04'),
(3, 'Test 3', 'Test 3', 'Test 3', '2017-06-05 05:56:09', '2017-06-05 05:56:09'),
(4, 'Test 4', 'Test 4', 'Test 4', '2017-06-05 05:56:14', '2017-06-05 05:56:14'),
(5, 'Test 5', 'Test 5', 'Test 5', '2017-06-05 05:56:19', '2017-06-05 05:56:19');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 2),
(2, 3),
(3, 3),
(4, 3),
(5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `supplier_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `barcode` varchar(60) CHARACTER SET utf8 DEFAULT '',
  `name` text CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `minamount` decimal(10,2) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT 0.00,
  `condition_id` int(11) DEFAULT NULL,
  `state_id` int(2) NOT NULL DEFAULT 1,
  `self_price` decimal(15,2) DEFAULT 0.00,
  `last_price` decimal(10,2) DEFAULT NULL,
  `unit_name` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `supplier_code`, `barcode`, `name`, `description`, `category_id`, `minamount`, `price`, `condition_id`, `state_id`, `self_price`, `last_price`, `unit_name`, `unit_id`, `updated_at`, `created_at`) VALUES
(1, NULL, '25', 'ზეთი \"მზიური\"', 'asd', 80, NULL, 2.00, 1, 2, 2.00, 2.00, 'ცალი', 1, '2017-05-17 10:03:29', NULL),
(2, NULL, '', 'ზალატოი', '', 80, NULL, 3.00, 1, 2, 3.00, 3.00, '', NULL, '2017-05-17 10:03:30', NULL),
(3, NULL, '', 'ქარვა', '', 80, NULL, 1.00, 1, 2, 1.00, 1.00, '', NULL, '2017-05-17 10:03:22', NULL),
(4, NULL, '', 'ბრავიტა', '', 80, NULL, 3.00, 1, 2, 3.00, 3.00, '', NULL, '2017-05-11 06:39:39', NULL),
(5, NULL, '', 'სკუმბრია', '', 82, NULL, 12.00, 1, 2, 12.00, 12.00, '', NULL, '2017-05-17 10:03:24', NULL),
(6, NULL, '', 'კალმახი', '', 81, NULL, 10.00, 1, 2, 10.00, 10.00, '', NULL, '2017-05-17 10:03:28', NULL),
(7, NULL, '', 'ლოქო', '', 81, NULL, 3.00, 1, 2, 3.00, 3.00, '', NULL, '2017-05-17 10:03:26', NULL),
(8, NULL, '', 'ბროილერის ქათამი', '', 79, NULL, 4.00, 1, 2, 4.00, 4.00, '', NULL, '2017-05-17 10:03:37', NULL),
(9, NULL, '', 'დედალი', '', 79, NULL, 4.00, 1, 2, 4.00, 4.00, '', NULL, '2017-05-17 10:03:36', NULL),
(10, NULL, NULL, 'BOSCH KDN46VW25U', 'ორკამერიანი მაცივარი/საყინულე\nNo frost მშრალი გაყინვა \nმართვა: ელექტრონული (სენსორები)', 92, NULL, 1915.00, 1, 1, 4.00, 4.00, 'ცალი', 1, '2017-05-17 10:13:06', NULL),
(12, NULL, '', 'ვიბრატორიdsdsdsdsdsds', NULL, 1, NULL, 120.00, 1, 2, 0.00, NULL, 'ცალი', 1, '2017-05-17 10:03:34', '2017-05-03 10:31:10'),
(14, NULL, '', 'ვიბრატორიwsdfssssssssssssssssssssssss', NULL, 1, NULL, 120.00, 1, 2, 0.00, NULL, 'ცალი', 1, '2017-05-17 10:03:32', '2017-05-03 10:46:57'),
(15, NULL, 'sadsasa', 'sad', 'sadsasad', 81, 1.00, 2.00, 1, 2, NULL, NULL, 'გრამი', 3, '2017-05-11 07:17:23', '2017-05-11 07:17:07'),
(16, NULL, NULL, 'HITACHI 65HZ6W69', 'LED ტელევიზორი \nზომა: 65 ინჩი (165 სმ)\nრეზოლუცია: 3840 x 2160', 76, NULL, 3475.00, 1, 1, NULL, NULL, 'ცალი', 1, '2017-05-17 10:14:38', '2017-05-17 10:09:15'),
(17, NULL, NULL, 'SAMSUNG RB37K63412C/WT/O', 'ორკამერიანი მაცივარი/საყინულე\nმუშაობის პრინციპი: NoFrost მშრალი გაყინვა \nმართვა: ელექტრონული', 92, NULL, 2688.00, 1, 1, NULL, NULL, 'ცალი', 1, '2017-05-17 10:19:22', '2017-05-17 10:19:22'),
(18, NULL, NULL, 'SAMSUNG RB37K63412A/WT/O', 'ორკამერიანი მაცივარი/საყინულე\nმუშაობის პრინციპი: NoFrost მშრალი გაყინვა \nმართვა: ელექტრონული', 1, NULL, 2730.00, 1, 1, NULL, NULL, 'ცალი', 1, '2017-05-17 10:20:06', '2017-05-17 10:20:06'),
(19, NULL, NULL, 'GORENJE K17FE', 'ჩაიდანი\nტევადობა: 1.7 ლ\nსიმძლავრე: 2200 ვატი', 98, NULL, 115.00, 1, 1, NULL, NULL, 'ცალი', 1, '2017-05-18 07:38:33', '2017-05-18 07:38:33'),
(20, NULL, NULL, 'test', 'test', 1, NULL, 43.00, 1, 2, NULL, NULL, 'ცალი', 1, '2017-05-19 03:44:53', '2017-05-19 03:17:00'),
(21, NULL, 'kima', 'kima', 'kima', 74, 1.00, 2.00, 2, 2, NULL, NULL, 'ცალი', 1, '2017-11-30 13:14:14', '2017-05-19 03:14:22');

-- --------------------------------------------------------

--
-- Table structure for table `product_files`
--

CREATE TABLE `product_files` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `original_file_name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `is_cover` tinyint(1) NOT NULL DEFAULT 0,
  `state_id` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `product_files`
--

INSERT INTO `product_files` (`id`, `product_id`, `file_name`, `original_file_name`, `is_cover`, `state_id`, `created_at`, `updated_at`) VALUES
(2, 17, '557fd1ba7f55a2b6b26e984858a1d298.jpg', '557fd1ba7f55a2b6b26e984858a1d298.jpg_original', 0, 0, '2017-05-19 08:39:48', '2017-11-30 14:05:28'),
(3, 17, 'b49ad8bdcc52a1daa4d259042637139b.jpeg', 'b49ad8bdcc52a1daa4d259042637139b.jpeg_original', 0, 0, '2017-05-19 08:42:07', '2017-11-30 14:05:30'),
(4, 17, '3570d54396ddae645882d06447300bd0.jpg', '3570d54396ddae645882d06447300bd0.jpg_original', 0, 0, '2017-05-19 08:43:37', '2017-11-30 14:05:32'),
(5, 21, 'adee272e1084a765de860889582d192c.jpg', 'adee272e1084a765de860889582d192c.jpg_original', 0, 0, '2017-11-17 08:34:51', '2017-11-30 13:14:14'),
(6, 17, '56f9b0ed690f5e1b4d00459b9d3f803b.jpg', '56f9b0ed690f5e1b4d00459b9d3f803b.jpg_original', 0, 0, '2017-11-30 14:03:47', '2017-11-30 14:05:33'),
(7, 17, '03307d6ac76cb39ab665cf0fca3214ff.jpg', '03307d6ac76cb39ab665cf0fca3214ff.jpg_original', 0, 0, '2017-11-30 14:06:51', '2017-11-30 14:23:22'),
(8, 17, 'D:\\xampp\\htdocs\\onlinemarket\\public_html/files/product_files/17/0d5aadcb84c98740f447e3c75fc5ffae.jpg', '0d5aadcb84c98740f447e3c75fc5ffae.jpg_original', 0, 0, '2017-11-30 14:13:34', '2017-11-30 14:23:23'),
(9, 17, '/files/product_files/17/cf8f05dacd8845215b0b260f1092cc99.jpg', 'cf8f05dacd8845215b0b260f1092cc99.jpg_original', 0, 0, '2017-11-30 14:17:38', '2017-11-30 14:23:25'),
(10, 17, '84b3c13701d8930a15609ffbda1f0341.jpg', '84b3c13701d8930a15609ffbda1f0341.jpg_original', 0, 0, '2017-11-30 14:26:14', '2017-11-30 14:29:13'),
(11, 17, '7239b1ac7d9ea60f3d73dafaa1d449a3.jpg', '7239b1ac7d9ea60f3d73dafaa1d449a3.jpg_original', 0, 0, '2017-11-30 14:29:28', '2017-12-14 13:48:24'),
(12, 17, '7605d2bb9f9f3c968a7fd61d39e4d6f1.jpg', '7605d2bb9f9f3c968a7fd61d39e4d6f1.jpg_original', 0, 0, '2017-11-30 14:49:08', '2017-11-30 14:49:12'),
(13, 17, '96fba45541f8539106f3b85d6df52a3d.jpg', '96fba45541f8539106f3b85d6df52a3d.jpg_original', 0, 1, '2017-12-14 13:50:15', '2017-12-14 13:50:15');

-- --------------------------------------------------------

--
-- Table structure for table `product_info`
--

CREATE TABLE `product_info` (
  `product_info_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_info_type_id` int(11) NOT NULL,
  `product_info_type_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `value` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `state_id` int(11) NOT NULL DEFAULT 1,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_info`
--

INSERT INTO `product_info` (`product_info_id`, `product_id`, `product_info_type_id`, `product_info_type_name`, `value`, `state_id`, `updated_at`, `created_at`) VALUES
(1, 1, 20, 'ტრანსმისია', 'ავტომატიკა', 2, '2017-05-03 15:21:34', '2017-05-03 11:12:38'),
(2, 1, 20, 'ტრანსმისია', 'meqanika', 1, '2017-05-11 12:03:04', '2017-05-11 08:03:04'),
(3, 1, 21, 'ტრანსმისიაssssss', 'model', 2, '2017-05-11 12:06:30', '2017-05-11 08:06:17'),
(4, 1, 23, 'xxx', 'asd', 1, '2017-05-11 12:13:12', '2017-05-11 08:13:12');

-- --------------------------------------------------------

--
-- Table structure for table `product_info_types`
--

CREATE TABLE `product_info_types` (
  `product_info_type_id` int(11) NOT NULL,
  `product_info_type_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_info_types`
--

INSERT INTO `product_info_types` (`product_info_type_id`, `product_info_type_name`, `created_at`, `updated_at`) VALUES
(1, 'ბრენდი', NULL, NULL),
(2, 'მოდელი', NULL, NULL),
(3, 'ეკრანის ზომა', NULL, NULL),
(4, 'ეკრანის ტიპი', NULL, NULL),
(5, 'ეკრანის რეზოლუცია', NULL, NULL),
(6, 'CPU', NULL, NULL),
(7, 'RAM', NULL, NULL),
(8, 'HDD', NULL, NULL),
(9, 'LAN', NULL, NULL),
(10, 'ODD', NULL, NULL),
(11, 'Display', NULL, NULL),
(12, 'Battery', NULL, NULL),
(13, 'OS', NULL, NULL),
(14, 'Weight', NULL, NULL),
(15, 'SupplierCode', NULL, NULL),
(16, 'PartNumber', NULL, NULL),
(17, 'ნოუთის ინფო', NULL, NULL),
(18, 'მონაცემები', NULL, NULL),
(20, 'ტრანსმისია', '2017-05-03 11:06:49', '2017-05-03 15:06:49'),
(21, 'ტრანსმისიაssssss', '2017-05-03 11:39:19', '2017-05-03 15:39:19'),
(22, 'ragaca', '2017-05-11 08:12:26', '2017-05-11 12:12:26'),
(23, 'xxx', '2017-05-11 08:12:57', '2017-05-11 12:12:57');

-- --------------------------------------------------------

--
-- Table structure for table `recievings`
--

CREATE TABLE `recievings` (
  `recieving_id` int(11) DEFAULT NULL,
  `recieving_whole_price` decimal(10,2) DEFAULT NULL,
  `recieving_invoic_number` tinytext DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `payed_money` decimal(10,2) DEFAULT NULL,
  `supplier_code` varchar(100) DEFAULT NULL,
  `supplier_name` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recievings`
--

INSERT INTO `recievings` (`recieving_id`, `recieving_whole_price`, `recieving_invoic_number`, `branch_id`, `user_id`, `state_id`, `payed_money`, `supplier_code`, `supplier_name`, `created_at`, `updated_at`) VALUES
(NULL, 405.00, NULL, NULL, NULL, 1, 35.00, NULL, NULL, '2017-06-20 07:59:04', '2017-06-20 11:59:04'),
(NULL, 610.00, NULL, NULL, NULL, 1, 100.00, NULL, NULL, '2017-06-20 08:03:47', '2017-06-20 12:03:47'),
(NULL, 204.00, NULL, 1, NULL, 1, 33.00, NULL, NULL, '2017-06-20 10:03:28', '2017-06-20 14:03:28'),
(NULL, 10.00, NULL, 1, NULL, 1, 10.00, NULL, NULL, '2017-06-20 10:26:56', '2017-06-20 14:26:56'),
(NULL, 10.00, NULL, 1, NULL, 1, 10.00, NULL, NULL, '2017-06-20 10:27:19', '2017-06-20 14:27:19'),
(NULL, 30.00, NULL, 1, NULL, 1, 10.00, NULL, NULL, '2017-06-20 10:32:32', '2017-06-20 14:32:32'),
(NULL, 20.00, NULL, 1, NULL, 1, 20.00, NULL, NULL, '2017-06-20 10:33:28', '2017-06-20 14:33:28'),
(NULL, 10.00, NULL, 1, NULL, 1, 1.00, NULL, NULL, '2017-06-20 10:36:25', '2017-06-20 14:36:25'),
(NULL, 10.00, NULL, 1, NULL, 1, 1.00, NULL, NULL, '2017-06-20 10:38:40', '2017-06-20 14:38:40'),
(NULL, 10.00, NULL, 1, NULL, 1, 1.00, NULL, NULL, '2017-06-20 10:39:11', '2017-06-20 14:39:11'),
(NULL, 20000.00, NULL, 1, NULL, 1, 200.00, NULL, NULL, '2017-06-20 11:09:09', '2017-06-20 15:09:09'),
(NULL, 50.00, NULL, 1, NULL, 1, 20.00, NULL, NULL, '2017-06-27 07:51:58', '2017-06-27 11:51:58'),
(NULL, 240.00, NULL, 1, NULL, 1, 12.00, NULL, NULL, '2017-06-27 07:52:49', '2017-06-27 11:52:49'),
(NULL, 50.00, NULL, 1, NULL, 1, 12.00, NULL, NULL, '2017-06-27 07:56:40', '2017-06-27 11:56:40'),
(NULL, 12.00, NULL, 1, NULL, 1, 12.00, NULL, NULL, '2017-06-27 08:34:39', '2017-06-27 12:34:39'),
(NULL, 12.00, NULL, 1, NULL, 1, 12.00, NULL, NULL, '2017-06-27 08:36:58', '2017-06-27 12:36:58'),
(NULL, 12.00, NULL, 1, NULL, 1, 12.00, NULL, NULL, '2017-06-27 08:37:14', '2017-06-27 12:37:14'),
(NULL, 12.00, NULL, 1, NULL, 1, 12.00, NULL, NULL, '2017-06-27 08:39:00', '2017-06-27 12:39:00'),
(NULL, 12.00, NULL, 1, NULL, 1, 12.00, NULL, NULL, '2017-06-27 08:46:21', '2017-06-27 12:46:21'),
(NULL, 280.00, NULL, 1, NULL, 1, 5.00, NULL, NULL, '2017-06-27 08:50:54', '2017-06-27 12:50:54'),
(NULL, 10.00, NULL, 1, NULL, 1, 1.00, NULL, NULL, '2017-06-27 08:59:48', '2017-06-27 12:59:48'),
(NULL, 10.00, NULL, 1, NULL, 1, 2.00, NULL, NULL, '2017-06-27 09:00:14', '2017-06-27 13:00:14'),
(NULL, 20.00, NULL, 1, NULL, 1, 1.00, NULL, NULL, '2017-06-27 09:01:26', '2017-06-27 13:01:26'),
(NULL, 16.00, NULL, 1, NULL, 1, 1.00, NULL, NULL, '2017-06-27 09:08:46', '2017-06-27 13:08:46'),
(NULL, 12.00, NULL, 1, NULL, 1, 12.00, NULL, NULL, '2017-06-27 09:11:17', '2017-06-27 13:11:17'),
(NULL, 28.00, NULL, 1, NULL, 1, 12.00, NULL, NULL, '2017-06-27 09:13:14', '2017-06-27 13:13:14'),
(NULL, 32.00, NULL, 1, NULL, 1, 21.00, NULL, NULL, '2017-06-27 09:16:24', '2017-06-27 13:16:24'),
(NULL, 280.00, NULL, 1, NULL, 1, 5.00, NULL, NULL, '2017-06-27 09:18:09', '2017-06-27 13:18:09'),
(NULL, 280.00, NULL, 1, NULL, 1, 5.00, NULL, NULL, '2017-06-27 09:18:29', '2017-06-27 13:18:29'),
(NULL, 4.00, NULL, 1, NULL, 1, 2.00, NULL, NULL, '2017-06-27 09:21:42', '2017-06-27 13:21:42'),
(NULL, 8.00, NULL, 1, NULL, 1, 1.00, NULL, NULL, '2017-06-27 09:21:19', '2017-06-27 13:21:19'),
(NULL, 4.00, NULL, 1, NULL, 1, 2.00, NULL, NULL, '2017-06-27 09:22:11', '2017-06-27 13:22:11'),
(NULL, 4.00, NULL, 1, NULL, 1, 2.00, NULL, NULL, '2017-06-27 09:22:37', '2017-06-27 13:22:37'),
(NULL, 12.00, NULL, 1, NULL, 1, 2.00, NULL, NULL, '2017-06-27 09:22:20', '2017-06-27 13:22:20'),
(NULL, 4.00, NULL, 1, NULL, 1, 2.00, NULL, NULL, '2017-06-27 09:23:17', '2017-06-27 13:23:17'),
(NULL, 16.00, NULL, 1, NULL, 1, 2.00, NULL, NULL, '2017-06-27 09:22:36', '2017-06-27 13:22:36'),
(NULL, 4.00, NULL, 1, NULL, 1, 2.00, NULL, NULL, '2017-06-27 09:25:46', '2017-06-27 13:25:46'),
(NULL, 4.00, NULL, 2, NULL, 1, 2.00, NULL, NULL, '2017-06-27 13:27:53', '2017-06-27 17:27:53'),
(NULL, 24.00, NULL, 1, NULL, 1, 13.00, NULL, NULL, '2017-06-27 13:37:37', '2017-06-27 17:37:37'),
(NULL, 9.00, NULL, 1, NULL, 1, 3.00, NULL, NULL, '2017-06-27 13:38:18', '2017-06-27 17:38:18'),
(NULL, 24.00, NULL, 2, NULL, 1, 2.00, NULL, NULL, '2017-06-27 13:38:42', '2017-06-27 17:38:42'),
(NULL, 125.00, NULL, 1, NULL, 1, 5.00, NULL, NULL, '2017-06-27 13:42:00', '2017-06-27 17:42:00'),
(NULL, 3350.00, NULL, 1, NULL, 1, 78.00, NULL, NULL, '2017-06-27 13:46:19', '2017-06-27 17:46:19');

-- --------------------------------------------------------

--
-- Table structure for table `recieving_products`
--

CREATE TABLE `recieving_products` (
  `recieving_product_id` int(11) DEFAULT NULL,
  `recieving_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `produvt_name` varchar(255) DEFAULT NULL,
  `amount` decimal(11,4) DEFAULT NULL,
  `one_price` decimal(10,2) DEFAULT NULL,
  `whole_price` decimal(10,2) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `valid_date` timestamp NULL DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recieving_products`
--

INSERT INTO `recieving_products` (`recieving_product_id`, `recieving_id`, `product_id`, `produvt_name`, `amount`, `one_price`, `whole_price`, `state_id`, `valid_date`, `branch_id`, `updated_at`, `created_at`) VALUES
(NULL, 1, 17, NULL, 15.0000, 3.00, 45.00, 1, NULL, NULL, '2017-06-20 07:59:05', '2017-06-20 07:59:05'),
(NULL, 1, 18, NULL, 60.0000, 6.00, 360.00, 1, NULL, NULL, '2017-06-20 07:59:06', '2017-06-20 07:59:06'),
(NULL, 1, 17, NULL, 50.0000, 5.00, 250.00, 1, NULL, NULL, '2017-06-20 08:03:47', '2017-06-20 08:03:47'),
(NULL, 1, 18, NULL, 60.0000, 6.00, 360.00, 1, NULL, NULL, '2017-06-20 08:03:47', '2017-06-20 08:03:47'),
(NULL, 1, 18, NULL, 12.0000, 2.00, 24.00, 1, NULL, 1, '2017-06-20 10:03:28', '2017-06-20 10:03:28'),
(NULL, 1, 21, NULL, 45.0000, 4.00, 180.00, 1, NULL, 1, '2017-06-20 10:03:28', '2017-06-20 10:03:28'),
(NULL, 1, 21, NULL, 5.0000, 2.00, 10.00, 1, NULL, 1, '2017-06-20 10:26:56', '2017-06-20 10:26:56'),
(NULL, 1, 21, NULL, 10.0000, 1.00, 10.00, 1, NULL, 1, '2017-06-20 10:27:19', '2017-06-20 10:27:19'),
(NULL, 1, 21, NULL, 10.0000, 3.00, 30.00, 1, NULL, 1, '2017-06-20 10:32:32', '2017-06-20 10:32:32'),
(NULL, 1, 21, NULL, 10.0000, 2.00, 20.00, 1, NULL, 1, '2017-06-20 10:33:28', '2017-06-20 10:33:28'),
(NULL, 1, 21, NULL, 5.0000, 2.00, 10.00, 1, '2015-12-06 20:00:00', 1, '2017-06-20 10:39:11', '2017-06-20 10:39:11'),
(NULL, 1, 21, NULL, 10.0000, 5.00, 50.00, 1, NULL, 1, '2017-06-27 07:51:58', '2017-06-27 07:51:58'),
(NULL, 1, 21, NULL, 20.0000, 12.00, 240.00, 1, '2015-12-07 16:00:00', 1, '2017-06-27 07:52:49', '2017-06-27 07:52:49'),
(NULL, 1, 17, NULL, 5.0000, 2.00, 10.00, 1, NULL, 1, '2017-06-27 08:59:48', '2017-06-27 08:59:48'),
(NULL, 1, 17, NULL, 5.0000, 2.00, 10.00, 1, '2017-11-30 16:00:00', 1, '2017-06-27 09:00:14', '2017-06-27 09:00:14'),
(NULL, 1, 10, NULL, 5.0000, 4.00, 20.00, 1, '2017-11-30 16:00:00', 1, '2017-06-27 09:01:26', '2017-06-27 09:01:26'),
(NULL, 1, 10, NULL, 4.0000, 4.00, 16.00, 1, NULL, 1, '2017-06-27 09:08:46', '2017-06-27 09:08:46'),
(NULL, 1, 10, NULL, 3.0000, 4.00, 12.00, 1, '2017-11-30 16:00:00', 1, '2017-06-27 09:11:17', '2017-06-27 09:11:17'),
(NULL, 1, 10, NULL, 7.0000, 4.00, 28.00, 1, '2017-11-30 16:00:00', 1, '2017-06-27 09:13:14', '2017-06-27 09:13:14'),
(NULL, 1, 10, NULL, 8.0000, 4.00, 32.00, 1, '2017-11-30 16:00:00', 1, '2017-06-27 09:16:24', '2017-06-27 09:16:24'),
(NULL, 1, 10, NULL, 2.0000, 4.00, 8.00, 1, NULL, 1, '2017-06-27 09:21:19', '2017-06-27 09:21:19'),
(NULL, 1, 10, NULL, 3.0000, 4.00, 12.00, 1, '2017-11-30 16:00:00', 1, '2017-06-27 09:22:20', '2017-06-27 09:22:20'),
(NULL, 1, 10, NULL, 4.0000, 4.00, 16.00, 1, '2017-11-30 16:00:00', 1, '2017-06-27 09:22:36', '2017-06-27 09:22:36'),
(NULL, 1, 21, NULL, 2.0000, 2.00, 4.00, 1, '2017-05-31 16:00:00', 1, '2017-06-27 09:25:46', '2017-06-27 09:25:46'),
(NULL, 1, 21, NULL, 2.0000, 2.00, 4.00, 1, '2015-05-31 20:00:00', 2, '2017-06-27 13:27:53', '2017-06-27 13:27:53'),
(NULL, 1, 21, NULL, 12.0000, 2.00, 24.00, 1, '2017-05-31 20:00:00', 1, '2017-06-27 13:37:37', '2017-06-27 13:37:37'),
(NULL, 1, 21, NULL, 3.0000, 3.00, 9.00, 1, '2017-05-31 20:00:00', 1, '2017-06-27 13:38:18', '2017-06-27 13:38:18'),
(NULL, 1, 21, NULL, 12.0000, 2.00, 24.00, 1, '2017-05-31 20:00:00', 2, '2017-06-27 13:38:42', '2017-06-27 13:38:42'),
(NULL, 1, 21, NULL, 25.0000, 5.00, 125.00, 1, NULL, 1, '2017-06-27 13:42:00', '2017-06-27 13:42:00'),
(NULL, 1, 21, NULL, 50.0000, 67.00, 3350.00, 1, NULL, 1, '2017-06-27 13:46:19', '2017-06-27 13:46:19');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Test 1', 'Test 1', 'Test 1', '2017-06-05 05:56:28', '2017-06-05 05:56:28'),
(2, 'Test 2', 'Test 2', 'Test 2', '2017-06-05 05:56:39', '2017-06-05 05:56:39'),
(3, 'Test 3', 'Test 3', 'Test 3', '2017-06-05 05:56:57', '2017-06-05 05:56:57');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 2),
(3, 3),
(4, 3);

-- --------------------------------------------------------

--
-- Table structure for table `sell`
--

CREATE TABLE `sell` (
  `sell_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `whole_price` decimal(10,2) DEFAULT NULL,
  `bonus` decimal(10,2) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `shipping_price` decimal(10,2) DEFAULT NULL,
  `transaction_id` varchar(32) DEFAULT NULL,
  `state` int(2) DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sell`
--

INSERT INTO `sell` (`sell_id`, `user_id`, `whole_price`, `bonus`, `city_id`, `address`, `shipping_price`, `transaction_id`, `state`, `created_at`, `updated_at`) VALUES
(9, NULL, 269100.00, 2691.00, NULL, NULL, NULL, '5a560b9c912e0', 1, '2018-01-10 12:50:39', '2018-01-10 12:50:39'),
(10, NULL, 269100.00, 2691.00, NULL, NULL, NULL, '5a560c706f67f', 1, '2018-01-10 12:54:00', '2018-01-10 12:54:00'),
(11, NULL, 269100.00, 2691.00, NULL, NULL, NULL, '5a560ca4438bc', 1, '2018-01-10 12:54:58', '2018-01-10 12:54:58'),
(12, NULL, 269100.00, 2691.00, NULL, NULL, NULL, '5a560df01fa50', 1, '2018-01-10 13:00:25', '2018-01-10 13:00:25'),
(13, NULL, 269100.00, 2691.00, NULL, NULL, NULL, '5a560e14eb961', 1, '2018-01-10 13:00:57', '2018-01-10 13:00:57'),
(14, NULL, 269100.00, 2691.00, NULL, NULL, NULL, '5a560f5877682', 1, '2018-01-10 13:06:26', '2018-01-10 13:06:26'),
(15, NULL, 269100.00, 2691.00, NULL, NULL, NULL, '5a560f96dd990', 1, '2018-01-10 13:07:21', '2018-01-10 13:07:21'),
(16, NULL, 269100.00, 2691.00, NULL, NULL, NULL, '5a56104919237', 1, '2018-01-10 13:10:40', '2018-01-10 13:10:40'),
(17, NULL, 269100.00, 2691.00, NULL, NULL, NULL, '5a561150906d6', 1, '2018-01-10 13:15:13', '2018-01-10 13:15:13'),
(18, NULL, 269100.00, 2691.00, NULL, NULL, NULL, '5a56104919237', 1, '2018-01-10 13:15:53', '2018-01-10 13:15:53'),
(19, NULL, 269100.00, 2691.00, NULL, NULL, NULL, '5a561150906d6', 1, '2018-01-10 13:20:23', '2018-01-10 13:20:23'),
(20, NULL, 269100.00, 2691.00, NULL, NULL, NULL, '5a56104919237', 1, '2018-01-10 13:26:01', '2018-01-10 13:26:01'),
(21, NULL, 269100.00, 2691.00, NULL, NULL, NULL, '5a561150906d6', 1, '2018-01-10 13:30:30', '2018-01-10 13:30:30'),
(22, NULL, 269100.00, 2691.00, NULL, NULL, NULL, '5a56104919237', 1, '2018-01-10 13:41:20', '2018-01-10 13:41:20'),
(23, NULL, 269100.00, 2691.00, NULL, NULL, NULL, '5a561150906d6', 1, '2018-01-10 13:45:35', '2018-01-10 13:45:35'),
(24, NULL, 269100.00, 2691.00, NULL, NULL, NULL, '5a56104919237', 1, '2018-01-10 14:01:27', '2018-01-10 14:01:27'),
(25, NULL, 269100.00, 2691.00, NULL, NULL, NULL, '5a561150906d6', 1, '2018-01-10 14:05:45', '2018-01-10 14:05:45'),
(26, NULL, 269100.00, 2691.00, NULL, NULL, NULL, 'yGx6SBGook6yDBdCCO9rVg==', 1, '2018-01-10 14:12:00', '2018-01-10 14:12:00'),
(27, NULL, 269100.00, 2691.00, NULL, NULL, NULL, 'eUd4NlNCR29vazZ5REJkQ0NPOXJWZz09', 1, '2018-01-10 14:14:24', '2018-01-10 14:14:24'),
(28, NULL, 269100.00, 2691.00, NULL, NULL, NULL, '5a56104919237', 1, '2018-01-10 14:26:33', '2018-01-10 14:26:33');

-- --------------------------------------------------------

--
-- Table structure for table `sell_items`
--

CREATE TABLE `sell_items` (
  `sell_item_id` int(11) NOT NULL,
  `sell_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_price` decimal(10,2) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `whole_price` decimal(10,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` int(2) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shoppingcart`
--

CREATE TABLE `shoppingcart` (
  `identifier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instance` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `shoppingcart`
--

INSERT INTO `shoppingcart` (`identifier`, `instance`, `content`, `created_at`, `updated_at`) VALUES
('admin@gmail.com', 'default', 'O:29:\"Illuminate\\Support\\Collection\":1:{s:8:\"\0*\0items\";a:1:{s:32:\"5bc403a550d0dac7ed010621521cf893\";O:32:\"Gloudemans\\Shoppingcart\\CartItem\":8:{s:5:\"rowId\";s:32:\"5bc403a550d0dac7ed010621521cf893\";s:2:\"id\";s:2:\"17\";s:3:\"qty\";s:1:\"1\";s:4:\"name\";s:24:\"SAMSUNG RB37K63412C/WT/O\";s:5:\"price\";d:2688;s:7:\"options\";O:39:\"Gloudemans\\Shoppingcart\\CartItemOptions\":1:{s:8:\"\0*\0items\";a:1:{s:3:\"img\";s:78:\"http://gdc.pixl.ge/files/product_files/17/96fba45541f8539106f3b85d6df52a3d.jpg\";}}s:49:\"\0Gloudemans\\Shoppingcart\\CartItem\0associatedModel\";N;s:41:\"\0Gloudemans\\Shoppingcart\\CartItem\0taxRate\";i:21;}}}', NULL, NULL),
('kakha3@gmail.com', 'default', 'O:29:\"Illuminate\\Support\\Collection\":1:{s:8:\"\0*\0items\";a:0:{}}', NULL, NULL),
('test@yahoo.com', 'default', 'O:29:\"Illuminate\\Support\\Collection\":1:{s:8:\"\0*\0items\";a:0:{}}', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `storage`
--

CREATE TABLE `storage` (
  `storage_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `amount` decimal(10,4) DEFAULT NULL,
  `place` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `valid_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `storage`
--

INSERT INTO `storage` (`storage_id`, `branch_id`, `product_id`, `amount`, `place`, `valid_date`, `created_at`, `updated_at`) VALUES
(37, 1, 21, 40.0000, NULL, '2017-05-31 20:00:00', '2017-06-27 13:37:37', '2017-06-27 13:42:00'),
(38, 2, 21, 12.0000, NULL, '2017-05-31 20:00:00', '2017-06-27 13:38:42', '2017-06-27 13:38:42'),
(39, 1, 21, 50.0000, NULL, NULL, '2017-06-27 13:46:18', '2017-06-27 13:46:18');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `birth_date` timestamp NULL DEFAULT NULL,
  `mobile_no` varchar(255) DEFAULT NULL,
  `personal_no` varchar(255) DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(555) DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `state_id` tinyint(1) NOT NULL DEFAULT 1,
  `bonus` decimal(10,2) DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `last_name`, `birth_date`, `mobile_no`, `personal_no`, `email`, `password`, `address`, `remember_token`, `created_at`, `updated_at`, `state_id`, `bonus`) VALUES
(1, 'კახა', 'თაბაგარი', '0000-00-00 00:00:00', '599 99 99 99', '123123123', 'admin@gmail.com', '$2y$10$b9Rs1OvVD/BThlNHmSGaauHgeTzWZsvj9IQJe.GH8hKudziWP0IRG', NULL, 'HSR1RjkLM5Be1ULQXCxjAJwUXCE7fcxMHvO2BwnVOea1zfvMbLw77j86uXsd', '2017-05-04 07:45:54', '2017-05-04 07:46:15', 1, 0.00),
(2, 'Admin1', '', '0000-00-00 00:00:00', NULL, NULL, 'admin1@gmail.com', '$2y$10$b9Rs1OvVD/BThlNHmSGaauHgeTzWZsvj9IQJe.GH8hKudziWP0IRG', NULL, '', '2017-05-04 07:58:39', '0000-00-00 00:00:00', 1, 0.00),
(3, 'KAKHA TABAGARI', '', '0000-00-00 00:00:00', NULL, NULL, 'tabagari89@gmail.com', '$2y$10$XVjVdez9ldUSXxTKNzinduYqNbtnX576wDfJ8B.fJp.ZRHVaOAZJq', '4 LEWIS CIR, G11403', 'HzWPeaGi1KyliNY1IU3qSDOmaXjNZLXYkiM9wK3AH2kb9TWsMQBG0uTqIKmR', '2017-06-27 09:32:28', '2017-06-27 09:32:28', 1, 0.00),
(4, 'kakha', 'TABAGARI', NULL, NULL, NULL, 'asd@gmail.com', '$2y$10$I3l6uKYTjnR5YrOa/Bl4Q.ouO2K8lqgWK7vu8N6sxGHB5jXrYiumG', '12 th m/r', NULL, '2017-11-30 12:43:16', '2017-11-30 12:43:16', 1, 0.00),
(5, 'test', 'test', NULL, NULL, NULL, 'test@yahoo.com', '$2y$10$CkEKIQ71hm5.cae83LjkVOvAd.RVMiiSpZrJAn.Aj3RB7E/Ct8FLG', NULL, 'ROkJYGqlgYDZoLPrjerAzaQlb5WoH2YpmNcMf3HbHLrx5PlyyPY7jKuksoF5', '2018-02-24 14:44:36', '2018-02-24 14:44:36', 1, 0.00),
(10, 'test', 'test', NULL, NULL, NULL, 'test2@yahoo.com', '$2y$10$XAjTfuoaxg3KS.WW.dOJOuYJP4TVbA4Lz9CLlKC0A6PyEmwr.Oj1C', NULL, NULL, '2018-02-24 14:50:41', '2018-02-24 14:50:41', 1, 0.00),
(11, 'test', 'test', NULL, NULL, NULL, 'test@gmail.com', '$2y$10$E/llgIzkssJQ6B6ORbK.dODANMoiMw1qaFw/wk5C0AdHH4xKW1MYe', NULL, NULL, '2018-02-24 14:51:01', '2018-02-24 14:51:01', 1, 0.00),
(13, 'kakha', 'katsadze', NULL, NULL, NULL, 'kakha@gmail.com', '$2y$10$ZhJNOTcZ3P1ny9sMYt5xQ.jLSgI2N1asYum6tPBM8GspSriuCFV2y', NULL, NULL, '2018-02-24 14:53:47', '2018-02-24 14:53:47', 1, 0.00),
(14, 'kakha', 'katsadze', NULL, NULL, NULL, 'kakha2@gmail.com', '$2y$10$6EJDgDypJFPfHdhVQuqKcuIhMVj3mBxKxOK8yzLz5QHW5nsy0dzD6', NULL, NULL, '2018-02-24 14:54:18', '2018-02-24 14:54:18', 1, 0.00),
(15, 'kakha', 'katsadze', NULL, NULL, NULL, 'kakha3@gmail.com', '$2y$10$O65dnNV8x26QJ/N01t7WYOWQRatlK52splx92Mej2MuT9xxqbnUcq', NULL, 'naGKfBdmTj35KCkV9tRwknhBjvWX0NyQlzzrcHR8OgHSPzkURlgWvWpe0P43', '2018-02-24 14:56:48', '2018-02-24 14:56:48', 1, 0.00),
(16, 'test', 'test', NULL, NULL, NULL, 'test2@gmail.com', '$2y$10$nTo5s1kw/eFRk8BjK7L4dOh4NQcwDlwqZHuCONTxo9agR0MfgNcXC', NULL, NULL, '2018-02-24 14:57:41', '2018-02-24 14:57:41', 1, 0.00),
(19, 'test', 'test', NULL, NULL, NULL, 'test3@gmail.com', '$2y$10$EwAuDEpbpSCMlBkPTZqzR.OPoJR7.JYmd9vfkqyq5bkXTYYwjU1Wi', NULL, NULL, '2018-02-24 14:58:45', '2018-02-24 14:58:45', 1, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `user_addresses`
--

CREATE TABLE `user_addresses` (
  `address_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `primary` int(1) DEFAULT 0,
  `state` int(2) DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_addresses`
--

INSERT INTO `user_addresses` (`address_id`, `user_id`, `city_id`, `address`, `primary`, `state`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 'qucha, 2,3', 1, 1, '2017-12-25 13:11:55', '2017-12-25 17:11:55'),
(2, 5, 1, NULL, 0, 1, '2018-02-24 14:44:36', '2018-02-24 18:44:36'),
(3, 10, 1, NULL, 0, 1, '2018-02-24 14:50:41', '2018-02-24 18:50:41'),
(4, 11, 1, NULL, 0, 1, '2018-02-24 14:51:01', '2018-02-24 18:51:01'),
(5, 13, 1, NULL, 0, 1, '2018-02-24 14:53:47', '2018-02-24 18:53:47'),
(6, 14, 1, NULL, 0, 1, '2018-02-24 14:54:18', '2018-02-24 18:54:18'),
(7, 15, 1, NULL, 0, 1, '2018-02-24 14:56:48', '2018-02-24 18:56:48'),
(8, 16, 1, NULL, 0, 1, '2018-02-24 14:57:41', '2018-02-24 18:57:41'),
(9, 19, 1, NULL, 0, 1, '2018-02-24 14:58:45', '2018-02-24 18:58:45');

-- --------------------------------------------------------

--
-- Table structure for table `user_tree`
--

CREATE TABLE `user_tree` (
  `user_tree_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `parent_path` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `user_parent_path` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `childrens` int(2) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `user_tree`
--

INSERT INTO `user_tree` (`user_tree_id`, `user_id`, `parent_id`, `parent_path`, `user_parent_path`, `childrens`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, '/1', '/1', 2, NULL, '2018-02-24 14:57:41'),
(2, 15, 1, '/1/2', '/1/15', 1, '2018-02-24 14:56:48', '2018-02-24 14:58:45'),
(3, 16, 1, '/1/3', '/1/16', 0, '2018-02-24 14:57:41', '2018-02-24 14:57:41'),
(4, 19, 2, '/1/2/4', '/1/15/19', 0, '2018-02-24 14:58:45', '2018-02-24 14:58:45');

-- --------------------------------------------------------

--
-- Table structure for table `web_categories`
--

CREATE TABLE `web_categories` (
  `category_id` int(11) NOT NULL,
  `name_ge` varchar(255) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `name_ru` varchar(255) NOT NULL,
  `state` int(11) NOT NULL DEFAULT 1,
  `disabled` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `parent_id` int(11) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  `type` int(2) DEFAULT NULL,
  `img_path` varchar(500) DEFAULT NULL,
  `color` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `web_categories`
--

INSERT INTO `web_categories` (`category_id`, `name_ge`, `name_en`, `name_ru`, `state`, `disabled`, `user_id`, `created_at`, `updated_at`, `parent_id`, `link`, `type`, `img_path`, `color`) VALUES
(1, 'კომპანიის შესახებ', 'კომპანიის შესახებ', 'კომპანიის შესახებ', 1, 0, 0, '2016-11-14 04:48:04', '2017-05-29 08:59:25', NULL, '/category/1', NULL, 'http://localhost:8082/university/public_html/files/NewFolder/ENUEpl6Sj7g.jpg', '#5b0b0b'),
(2, 'პროდუქტების შესახებ', 'პროდუქტების შესახებ', 'პროდუქტების შესახებ', 1, 0, 0, '2016-11-14 04:48:22', '2017-05-29 08:59:41', NULL, '/category/2', NULL, 'http://localhost:8082/university/public_html/files/NewFolder/ENUEpl6Sj7g.jpg', '#081765'),
(3, 'ფარმაციის ფაკულტეტი', 'ფარმაციის ფაკულტეტი', 'ფარმაციის ფაკულტეტი', 0, 0, 0, '2016-11-14 04:48:47', '2017-05-29 09:00:14', NULL, '/category/3', NULL, 'http://localhost:8082/university/public_html/files/NewFolder/ENUEpl6Sj7g.jpg', '#00FF00'),
(4, 'საზოგადოებრივი ჯანდაცვის ფაკულტეტი', 'საზოგადოებრივი ჯანდაცვის ფაკულტეტი', 'საზოგადოებრივი ჯანდაცვის ფაკულტეტი', 0, 0, 0, '2016-11-22 11:11:36', '2017-05-29 09:00:17', NULL, '/category/4', NULL, 'http://localhost:8082/university/public_html/files/NewFolder/ENUEpl6Sj7g.jpg', '#808080');

-- --------------------------------------------------------

--
-- Table structure for table `web_menu`
--

CREATE TABLE `web_menu` (
  `menu_id` int(10) UNSIGNED NOT NULL,
  `name_ge` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_ru` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `disabled` tinyint(1) NOT NULL DEFAULT 0,
  `editable` tinyint(1) NOT NULL DEFAULT 1,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `order` int(11) DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `web_menu`
--

INSERT INTO `web_menu` (`menu_id`, `name_ge`, `name_en`, `name_ru`, `link`, `parent_id`, `state`, `disabled`, `editable`, `user_id`, `created_at`, `updated_at`, `order`, `url`) VALUES
(72, 'მთავარი', 'მთავარი', 'მთავარი', '/', NULL, 0, 0, 1, NULL, '2017-05-18 05:38:18', '2017-05-18 05:39:22', 1, NULL),
(73, 'ჩვენს შესახებ', 'ჩვენს შესახებ', 'ჩვენს შესახებ', '/page/18', NULL, 1, 0, 1, NULL, '2017-05-18 05:38:35', '2017-06-08 08:12:54', 3, NULL),
(74, 'კონტაქტი', 'კონტაქტი', 'კონტაქტი', '/contact', NULL, 1, 0, 1, NULL, '2017-05-18 05:38:51', '2017-06-08 08:12:54', 4, NULL),
(75, 'სიახლეები', 'news', 'news', '/news', NULL, 1, 0, 1, NULL, '2017-05-25 06:07:49', '2017-05-25 06:07:56', 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `web_params`
--

CREATE TABLE `web_params` (
  `param_id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `value` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_params`
--

INSERT INTO `web_params` (`param_id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'address_ge', 'მისამართი: თბილისი თამარაშვილის შესახვევი 4 (გ. სვანიძის 8)', '2016-11-13 19:07:33', '0000-00-00 00:00:00'),
(2, 'address_en', 'Adress: Tamarashvili str. 4 (G. Svanidze 8), Tbilisi ', '2016-11-13 19:07:33', '0000-00-00 00:00:00'),
(3, 'address_ru', 'Adress: Tamarashvili str. 4 (G. Svanidze 8), Tbilisi ', '2016-11-13 19:07:33', '0000-00-00 00:00:00'),
(4, 'mobile_main', '+(995)322 29 34 92 ', '2016-11-13 19:08:41', '0000-00-00 00:00:00'),
(5, 'mobile', '+(995)322 29 34 92 ', '2016-11-13 19:08:41', '0000-00-00 00:00:00'),
(6, 'workTime', '09:00 - 18:01', '2016-11-13 19:09:30', '2017-05-17 09:09:25'),
(7, 'email', 'info@gdc.ge', '2016-11-14 07:42:25', '2017-05-18 07:36:15');

-- --------------------------------------------------------

--
-- Table structure for table `web_postmeta`
--

CREATE TABLE `web_postmeta` (
  `meta_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `meta_key` varchar(45) NOT NULL,
  `meta_value` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `web_postmeta`
--

INSERT INTO `web_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`, `created_at`, `updated_at`) VALUES
(1, 4, 'showInGallery', '1', '2016-12-05 09:41:20', '2016-12-05 09:41:20'),
(2, 10, 'showInGallery', '1', '2016-12-05 12:10:26', '2016-12-05 12:10:26');

-- --------------------------------------------------------

--
-- Table structure for table `web_posts`
--

CREATE TABLE `web_posts` (
  `post_id` int(10) UNSIGNED NOT NULL,
  `title_ge` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_ru` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_ge` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_en` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_ru` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `disabled` tinyint(1) NOT NULL DEFAULT 0,
  `user_id` int(11) DEFAULT NULL,
  `published_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `keyword_ge` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_description_ge` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_description_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_description_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `views` int(11) DEFAULT 0,
  `description_ge_short` text CHARACTER SET utf8 DEFAULT NULL,
  `description_en_short` text CHARACTER SET utf8 DEFAULT NULL,
  `description_ru_short` text CHARACTER SET utf8 DEFAULT NULL,
  `link_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `type` int(2) DEFAULT NULL,
  `order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `web_posts`
--

INSERT INTO `web_posts` (`post_id`, `title_ge`, `title_en`, `title_ru`, `description_ge`, `description_en`, `description_ru`, `img_path`, `state`, `disabled`, `user_id`, `published_at`, `created_at`, `updated_at`, `link`, `keyword_ge`, `keyword_en`, `keyword_ru`, `google_description_ge`, `google_description_en`, `google_description_ru`, `views`, `description_ge_short`, `description_en_short`, `description_ru_short`, `link_name`, `type`, `order`) VALUES
(1, 'welcome', 'welcome', 'welcome', '&lt;p&gt;ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.&lt;/p&gt;', 'ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.', 'ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.', '', 1, 0, 0, '0000-00-00 00:00:00', '2016-11-15 08:40:09', '2016-11-15 14:39:53', '/page/1', 'Business Consultation', 'Business Consultation', 'Business Consultation', 'Business Consultation', 'Business Consultation', 'Business Consultation', 0, NULL, NULL, '', NULL, 2, NULL),
(2, 'about', 'about', 'about', 'ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.', 'ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.', 'ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.', '', 0, 0, 0, '0000-00-00 00:00:00', '2016-11-15 14:36:57', '2017-05-29 07:29:13', '/page/2?category=1', '', '', '', '', '', '', 0, '', '', '', NULL, 2, NULL),
(3, 'about', 'about', 'about', '&lt;p&gt;ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.&lt;/p&gt;\n&lt;p&gt;&lt;a rel=&quot;prettyPhoto[pp_gal]&quot; href=&quot;../../../../files/NewFolder/ENUEpl6Sj7g.jpg&quot;&gt;&lt;img src=&quot;../../../../files/NewFolder/ENUEpl6Sj7g.jpg&quot; alt=&quot;&quot; width=&quot;303&quot; height=&quot;227&quot;&gt;&lt;/a&gt;&lt;/p&gt;', 'ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.', 'ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.', '', 0, 0, 0, '0000-00-00 00:00:00', '2016-11-24 05:09:09', '2017-05-25 05:37:50', '/page/3?category=1', '', '', '', '', '', '', 0, '', '', '', NULL, 2, NULL),
(4, 'test', '', '', '&lt;p&gt;asdas sa ds dsa dsa&lt;/p&gt;', '', '', '', 0, 0, 0, '2016-11-24 15:00:00', '2016-11-24 11:00:50', '2016-11-24 11:18:39', NULL, 'asd', '', '', 'asdsad sad sa dsa', '', '', 0, '', '', '', NULL, 1, NULL),
(5, 'test', '', '', '&lt;p&gt;asdas sa ds dsa dsa&lt;/p&gt;', '', '', '', 0, 0, 0, '2016-11-24 15:00:00', '2016-11-24 11:03:43', '2016-11-24 11:18:43', NULL, 'asd', '', '', 'asdsad sad sa dsa', '', '', 0, '', '', '', NULL, 1, NULL),
(6, 'test', '', '', '&lt;p&gt;dsa dsa dsa dsa dsa sa dsa dsa&lt;/p&gt;', '', '', '', 0, 0, 0, '2016-11-24 15:07:00', '2016-11-24 11:17:40', '2016-11-24 11:18:41', '/news/6', 'ads', '', '', 'sa dsa dsa', '', '', 0, '', '', '', NULL, 1, NULL),
(7, 'გერმანელი არქიტექტორის, მარკუს პენელის, დისკუსია: NO OLD NO CITY!', 'გერმანელი არქიტექტორის, მარკუს პენელის, დისკუსია: NO OLD NO CITY!', '', '&lt;p&gt;22 ნოემბერს, 19:00 საათზე, ილიას სახელმწიფო უნივერსიტეტის E405&amp;nbsp;(&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;„&lt;/strong&gt;&lt;strong&gt;ფორბსის&lt;/strong&gt;&lt;/a&gt;“)&amp;nbsp;აუდიტორიაში, ილიას სახელმწიფო უნივერსიტეტისა და&amp;nbsp;&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;ForbesGeorgia&lt;/strong&gt;&lt;/a&gt;&lt;strong&gt;-&lt;/strong&gt;ს ურთიერთთანამშრომლობის ფარგლებში, კომუნიკაციების და ბრენდინგის სპეციალისტი, მედიაპროდიუსერი, ზაქარია ზალიკაშვილი, წაიკითხავს საჯარო ლექციას.&amp;nbsp;&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;მომხსენებლის&lt;/strong&gt;&lt;strong&gt; შესახებ&lt;/strong&gt;&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;თბილისის ბიზნესის ინსტიტუტის დასრულების შემდეგ ზაქარია ზალიკაშვილი აშშ-ში გაემგზავრა და დაამთავრა სამხრეთ კალიფორნიის (აშშ) უნივერსიტეტის კინო- და ტელეხელოვნების ფაკულტეტი, სპეციალობით - კინორეჟისურა. პროფესიული გამოცდილება მიიღო საქართველოს სხვადასხვა კომპანიის მარკეტინგის განყოფილებაში წამყვანი სპეციალისტის და დეპარტამენტის ხელმძღვანელის პოზიციებზე. 2000 წლამდე მუშაობდა GORBI-ის სოციოლოგიური და მარკეტინგული კვლევების პროექტებზე (თამბაქოს, გამამხნევებელი სასმელების, ტელეკომუნიკაციის და სხვა ტრანსნაციონალურ კომპანიებთან. მსოფლიო ბანკთან); ამავე პერიოდში, ჯონ ჰოპკინსის უნივერსიტეტთან და სხვა საერთაშორისო ფონდებთან ერთად, აწარმოებდა სხვადასხვა ტიპის სოციალური კამპანიებს.&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;ზაქარია ზალიკაშვილი სხვადასხვა დროს ხელმძღვანელობდა სარეკლამო სააგენტოს „აუდიენცია“, რადიოს „მწვანე ტალღა“ და კომპანია „ჯეოსელის“ მარკეტინგულ კომუნიკაციებს. მან 2005 წელს დააფუძნა მარკეტინგული კონსულტაციების კომპანია „სტრომბოლი“, ხოლო 2006 წელს - კომპანია „კომუნიკატორი“. 2014 წლიდან ხელმძღვანელობს საზოგადოებასთან ურთიერთობების დეპარტამენტს ქართულ ამერიკულ ინდუსტრიულ ჯგუფში “ჯორჯიან ემერიქან ელოიზ”.&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;დრო:&lt;/strong&gt; 22 ნოემბერი, 19:00 საათი&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;ადგილმდებარეობა:&lt;/strong&gt; E405&amp;nbsp;(&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;„&lt;/strong&gt;&lt;strong&gt;ფორბსის&lt;/strong&gt;&lt;strong&gt;“&lt;/strong&gt;&lt;/a&gt;)&amp;nbsp;აუდიტორია (ჩოლოყაშვილის გამზ. #3/5), ილიას სახელმწიფო უნივერსიტეტი&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;დასწრება თავისუფალია.&lt;/p&gt;', '&lt;p&gt;22 ნოემბერს, 19:00 საათზე, ილიას სახელმწიფო უნივერსიტეტის E405&amp;nbsp;(&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;„&lt;/strong&gt;&lt;strong&gt;ფორბსის&lt;/strong&gt;&lt;/a&gt;“)&amp;nbsp;აუდიტორიაში, ილიას სახელმწიფო უნივერსიტეტისა და&amp;nbsp;&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;ForbesGeorgia&lt;/strong&gt;&lt;/a&gt;&lt;strong&gt;-&lt;/strong&gt;ს ურთიერთთანამშრომლობის ფარგლებში, კომუნიკაციების და ბრენდინგის სპეციალისტი, მედიაპროდიუსერი, ზაქარია ზალიკაშვილი, წაიკითხავს საჯარო ლექციას.&amp;nbsp;&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;მომხსენებლის&lt;/strong&gt;&lt;strong&gt; შესახებ&lt;/strong&gt;&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;თბილისის ბიზნესის ინსტიტუტის დასრულების შემდეგ ზაქარია ზალიკაშვილი აშშ-ში გაემგზავრა და დაამთავრა სამხრეთ კალიფორნიის (აშშ) უნივერსიტეტის კინო- და ტელეხელოვნების ფაკულტეტი, სპეციალობით - კინორეჟისურა. პროფესიული გამოცდილება მიიღო საქართველოს სხვადასხვა კომპანიის მარკეტინგის განყოფილებაში წამყვანი სპეციალისტის და დეპარტამენტის ხელმძღვანელის პოზიციებზე. 2000 წლამდე მუშაობდა GORBI-ის სოციოლოგიური და მარკეტინგული კვლევების პროექტებზე (თამბაქოს, გამამხნევებელი სასმელების, ტელეკომუნიკაციის და სხვა ტრანსნაციონალურ კომპანიებთან. მსოფლიო ბანკთან); ამავე პერიოდში, ჯონ ჰოპკინსის უნივერსიტეტთან და სხვა საერთაშორისო ფონდებთან ერთად, აწარმოებდა სხვადასხვა ტიპის სოციალური კამპანიებს.&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;ზაქარია ზალიკაშვილი სხვადასხვა დროს ხელმძღვანელობდა სარეკლამო სააგენტოს „აუდიენცია“, რადიოს „მწვანე ტალღა“ და კომპანია „ჯეოსელის“ მარკეტინგულ კომუნიკაციებს. მან 2005 წელს დააფუძნა მარკეტინგული კონსულტაციების კომპანია „სტრომბოლი“, ხოლო 2006 წელს - კომპანია „კომუნიკატორი“. 2014 წლიდან ხელმძღვანელობს საზოგადოებასთან ურთიერთობების დეპარტამენტს ქართულ ამერიკულ ინდუსტრიულ ჯგუფში “ჯორჯიან ემერიქან ელოიზ”.&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;დრო:&lt;/strong&gt; 22 ნოემბერი, 19:00 საათი&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;ადგილმდებარეობა:&lt;/strong&gt; E405&amp;nbsp;(&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;„&lt;/strong&gt;&lt;strong&gt;ფორბსის&lt;/strong&gt;&lt;strong&gt;“&lt;/strong&gt;&lt;/a&gt;)&amp;nbsp;აუდიტორია (ჩოლოყაშვილის გამზ. #3/5), ილიას სახელმწიფო უნივერსიტეტი&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;დასწრება თავისუფალია.&lt;/p&gt;', '', 'http://localhost:8082/onlinemarket/public/files/17/b49ad8bdcc52a1daa4d259042637139b_original.jpeg', 1, 0, 0, '2016-11-24 11:20:00', '2016-11-24 11:18:50', '2017-05-29 09:37:08', '/news/7', 'გერმანელი არქიტექტორის, მარკუს პენელის, დისკუსია: NO OLD NO CITY!', 'გერმანელი არქიტექტორის, მარკუს პენელის, დისკუსია: NO OLD NO CITY!', '', 'გერმანელი არქიტექტორის, მარკუს პენელის, დისკუსია: NO OLD NO CITY!', 'გერმანელი არქიტექტორის, მარკუს პენელის, დისკუსია: NO OLD NO CITY!', '', 0, '', '', '', NULL, 1, NULL),
(8, 'ჩემპიონატი ფუტსალში - ილიაუნი VS ღია სასწავლო უნივერსიტეტი', 'ჩემპიონატი ფუტსალში - ილიაუნი VS ღია სასწავლო უნივერსიტეტი', '', '&lt;p&gt;24 ნოემბერს, 19:00 საათზე, ილიაუნის ფუტსალის ნაკრები, საუნივერსიტეტო სპორტის ფედერაციის თასის გათამაშების ჯგუფური ეტაპის მესამე მატჩს ღია სასწავლო უნივერსიტეტის გუნდთან გამართავს. შეხვედრა ჩატარდება სპორტკომპლექსში &quot;არენა-1&quot; (უნივერსიტეტის ქ. N2).&lt;/p&gt;\n&lt;p&gt;&lt;strong&gt;დრო:&amp;nbsp;&lt;/strong&gt;24 ნოემბერი,19:00 საათი&lt;/p&gt;\n&lt;p&gt;&lt;strong&gt;ადგილმდებარეობა:&amp;nbsp;&lt;/strong&gt;სპორტკომპლექსში &quot;არენა-1&quot; (უნივერსიტეტის ქ. N2).&lt;/p&gt;', '&lt;p&gt;24 ნოემბერს, 19:00 საათზე, ილიაუნის ფუტსალის ნაკრები, საუნივერსიტეტო სპორტის ფედერაციის თასის გათამაშების ჯგუფური ეტაპის მესამე მატჩს ღია სასწავლო უნივერსიტეტის გუნდთან გამართავს. შეხვედრა ჩატარდება სპორტკომპლექსში &quot;არენა-1&quot; (უნივერსიტეტის ქ. N2).&lt;/p&gt;\n&lt;p&gt;&lt;strong&gt;დრო:&amp;nbsp;&lt;/strong&gt;24 ნოემბერი,19:00 საათი&lt;/p&gt;\n&lt;p&gt;&lt;strong&gt;ადგილმდებარეობა:&amp;nbsp;&lt;/strong&gt;სპორტკომპლექსში &quot;არენა-1&quot; (უნივერსიტეტის ქ. N2).&lt;/p&gt;', '', 'http://localhost:8082/onlinemarket/public/files/foo.jpg', 1, 0, 0, '2016-11-24 11:18:00', '2016-11-24 11:19:51', '2017-05-29 09:37:30', '/news/8', 'ჩემპიონატი ფუტსალში - ილიაუნი VS ღია სასწავლო უნივერსიტეტი', 'ჩემპიონატი ფუტსალში - ილიაუნი VS ღია სასწავლო უნივერსიტეტი', '', 'ჩემპიონატი ფუტსალში - ილიაუნი VS ღია სასწავლო უნივერსიტეტი', 'ჩემპიონატი ფუტსალში - ილიაუნი VS ღია სასწავლო უნივერსიტეტი', '', 0, '', '', '', NULL, 1, NULL),
(9, 'ილიაუნელმა კალათბურთელებმა სტუდენტური ლიგის მეორე შეხვედრაც მოიგეს', 'ილიაუნელმა კალათბურთელებმა სტუდენტური ლიგის მეორე შეხვედრაც მოიგეს', '', '&lt;p&gt;ვერის ბაღის საკალათბურთო კომპლექსში გრძელდება საქართველოს სტუდენტური ჩემპიონატი კალთბურთში. ჯგუფური ეტაპის მეორე ტურში მორიგ წარმატებას მიაღწია ილიაუნის გუნდმა, რომელმაც შავი ზღვის საერთაშორისო უნივერსიტეტის კალათბურთელები დიდი ანგარიშით - 106:54 დაამარცხა. ილიაუნის გუნდს ყველაზე მეტი, 32 ქულა ირაკლი ჯანხოთელმა მოუტანა. მე-3 ტურის მატჩს ილიაუნელები 2 დეკემბერს, 16:00 საათზე, ბათუმის საზღვაო აკადემიის კალათბურთელებთან გამართავენ. აააა&lt;/p&gt;', '&lt;p&gt;ვერის ბაღის საკალათბურთო კომპლექსში გრძელდება საქართველოს სტუდენტური ჩემპიონატი კალთბურთში. ჯგუფური ეტაპის მეორე ტურში მორიგ წარმატებას მიაღწია ილიაუნის გუნდმა, რომელმაც შავი ზღვის საერთაშორისო უნივერსიტეტის კალათბურთელები დიდი ანგარიშით - 106:54 დაამარცხა. ილიაუნის გუნდს ყველაზე მეტი, 32 ქულა ირაკლი ჯანხოთელმა მოუტანა. მე-3 ტურის მატჩს ილიაუნელები 2 დეკემბერს, 16:00 საათზე, ბათუმის საზღვაო აკადემიის კალათბურთელებთან გამართავენ.&amp;nbsp;&lt;/p&gt;', '', 'http://localhost:8082/onlinemarket/public_html/files/foo.jpg', 1, 0, 0, '2016-11-24 15:22:00', '2016-11-24 11:22:18', '2017-12-14 13:51:03', '/news/9', 'ილიაუნელმა კალათბურთელებმა სტუდენტური ლიგის მეორე შეხვედრაც მოიგეს', 'ილიაუნელმა კალათბურთელებმა სტუდენტური ლიგის მეორე შეხვედრაც მოიგეს', '', 'ილიაუნელმა კალათბურთელებმა სტუდენტური ლიგის მეორე შეხვედრაც მოიგეს', 'ილიაუნელმა კალათბურთელებმა სტუდენტური ლიგის მეორე შეხვედრაც მოიგეს', '', 0, '', '', '', NULL, 1, NULL),
(10, 'ილიაუნის „ფორბსის“ კლუბში: ზაქარია ზალიკაშვილის საჯარო ლექცია', 'ილიაუნის „ფორბსის“ კლუბში: ზაქარია ზალიკაშვილის საჯარო ლექცია', '', '&lt;p&gt;22 ნოემბერს, 19:00 საათზე, ილიას სახელმწიფო უნივერსიტეტის E405&amp;nbsp;(&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;„&lt;/strong&gt;&lt;strong&gt;ფორბსის&lt;/strong&gt;&lt;/a&gt;“)&amp;nbsp;აუდიტორიაში, ილიას სახელმწიფო უნივერსიტეტისა და&amp;nbsp;&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;ForbesGeorgia&lt;/strong&gt;&lt;/a&gt;&lt;strong&gt;-&lt;/strong&gt;ს ურთიერთთანამშრომლობის ფარგლებში, კომუნიკაციების და ბრენდინგის სპეციალისტი, მედიაპროდიუსერი, ზაქარია ზალიკაშვილი, წაიკითხავს საჯარო ლექციას.&amp;nbsp;&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;მომხსენებლის&lt;/strong&gt;&lt;strong&gt; შესახებ&lt;/strong&gt;&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;თბილისის ბიზნესის ინსტიტუტის დასრულების შემდეგ ზაქარია ზალიკაშვილი აშშ-ში გაემგზავრა და დაამთავრა სამხრეთ კალიფორნიის (აშშ) უნივერსიტეტის კინო- და ტელეხელოვნების ფაკულტეტი, სპეციალობით - კინორეჟისურა. პროფესიული გამოცდილება მიიღო საქართველოს სხვადასხვა კომპანიის მარკეტინგის განყოფილებაში წამყვანი სპეციალისტის და დეპარტამენტის ხელმძღვანელის პოზიციებზე. 2000 წლამდე მუშაობდა GORBI-ის სოციოლოგიური და მარკეტინგული კვლევების პროექტებზე (თამბაქოს, გამამხნევებელი სასმელების, ტელეკომუნიკაციის და სხვა ტრანსნაციონალურ კომპანიებთან. მსოფლიო ბანკთან); ამავე პერიოდში, ჯონ ჰოპკინსის უნივერსიტეტთან და სხვა საერთაშორისო ფონდებთან ერთად, აწარმოებდა სხვადასხვა ტიპის სოციალური კამპანიებს.&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;ზაქარია ზალიკაშვილი სხვადასხვა დროს ხელმძღვანელობდა სარეკლამო სააგენტოს „აუდიენცია“, რადიოს „მწვანე ტალღა“ და კომპანია „ჯეოსელის“ მარკეტინგულ კომუნიკაციებს. მან 2005 წელს დააფუძნა მარკეტინგული კონსულტაციების კომპანია „სტრომბოლი“, ხოლო 2006 წელს - კომპანია „კომუნიკატორი“. 2014 წლიდან ხელმძღვანელობს საზოგადოებასთან ურთიერთობების დეპარტამენტს ქართულ ამერიკულ ინდუსტრიულ ჯგუფში “ჯორჯიან ემერიქან ელოიზ”.&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;დრო:&lt;/strong&gt; 22 ნოემბერი, 19:00 საათი&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;ადგილმდებარეობა:&lt;/strong&gt; E405&amp;nbsp;(&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;„&lt;/strong&gt;&lt;strong&gt;ფორბსის&lt;/strong&gt;&lt;strong&gt;“&lt;/strong&gt;&lt;/a&gt;)&amp;nbsp;აუდიტორია (ჩოლოყაშვილის გამზ. #3/5), ილიას სახელმწიფო უნივერსიტეტი&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;დასწრება თავისუფალია.&lt;/p&gt;', '&lt;p&gt;22 ნოემბერს, 19:00 საათზე, ილიას სახელმწიფო უნივერსიტეტის E405&amp;nbsp;(&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;„&lt;/strong&gt;&lt;strong&gt;ფორბსის&lt;/strong&gt;&lt;/a&gt;“)&amp;nbsp;აუდიტორიაში, ილიას სახელმწიფო უნივერსიტეტისა და&amp;nbsp;&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;ForbesGeorgia&lt;/strong&gt;&lt;/a&gt;&lt;strong&gt;-&lt;/strong&gt;ს ურთიერთთანამშრომლობის ფარგლებში, კომუნიკაციების და ბრენდინგის სპეციალისტი, მედიაპროდიუსერი, ზაქარია ზალიკაშვილი, წაიკითხავს საჯარო ლექციას.&amp;nbsp;&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;მომხსენებლის&lt;/strong&gt;&lt;strong&gt; შესახებ&lt;/strong&gt;&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;თბილისის ბიზნესის ინსტიტუტის დასრულების შემდეგ ზაქარია ზალიკაშვილი აშშ-ში გაემგზავრა და დაამთავრა სამხრეთ კალიფორნიის (აშშ) უნივერსიტეტის კინო- და ტელეხელოვნების ფაკულტეტი, სპეციალობით - კინორეჟისურა. პროფესიული გამოცდილება მიიღო საქართველოს სხვადასხვა კომპანიის მარკეტინგის განყოფილებაში წამყვანი სპეციალისტის და დეპარტამენტის ხელმძღვანელის პოზიციებზე. 2000 წლამდე მუშაობდა GORBI-ის სოციოლოგიური და მარკეტინგული კვლევების პროექტებზე (თამბაქოს, გამამხნევებელი სასმელების, ტელეკომუნიკაციის და სხვა ტრანსნაციონალურ კომპანიებთან. მსოფლიო ბანკთან); ამავე პერიოდში, ჯონ ჰოპკინსის უნივერსიტეტთან და სხვა საერთაშორისო ფონდებთან ერთად, აწარმოებდა სხვადასხვა ტიპის სოციალური კამპანიებს.&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;ზაქარია ზალიკაშვილი სხვადასხვა დროს ხელმძღვანელობდა სარეკლამო სააგენტოს „აუდიენცია“, რადიოს „მწვანე ტალღა“ და კომპანია „ჯეოსელის“ მარკეტინგულ კომუნიკაციებს. მან 2005 წელს დააფუძნა მარკეტინგული კონსულტაციების კომპანია „სტრომბოლი“, ხოლო 2006 წელს - კომპანია „კომუნიკატორი“. 2014 წლიდან ხელმძღვანელობს საზოგადოებასთან ურთიერთობების დეპარტამენტს ქართულ ამერიკულ ინდუსტრიულ ჯგუფში “ჯორჯიან ემერიქან ელოიზ”.&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;დრო:&lt;/strong&gt; 22 ნოემბერი, 19:00 საათი&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;ადგილმდებარეობა:&lt;/strong&gt; E405&amp;nbsp;(&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;„&lt;/strong&gt;&lt;strong&gt;ფორბსის&lt;/strong&gt;&lt;strong&gt;“&lt;/strong&gt;&lt;/a&gt;)&amp;nbsp;აუდიტორია (ჩოლოყაშვილის გამზ. #3/5), ილიას სახელმწიფო უნივერსიტეტი&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;დასწრება თავისუფალია.&lt;/p&gt;', '', 'http://localhost:8082/onlinemarket/public_html/files/foo.jpg', 1, 0, 0, '2016-11-24 19:25:00', '2016-11-24 11:25:35', '2017-11-29 10:53:39', '/news/10', 'ილიაუნის „ფორბსის“ კლუბში: ზაქარია ზალიკაშვილის საჯარო ლექცია', 'ილიაუნის „ფორბსის“ კლუბში: ზაქარია ზალიკაშვილის საჯარო ლექცია', '', 'ილიაუნის „ფორბსის“ კლუბში: ზაქარია ზალიკაშვილის საჯარო ლექცია', 'ილიაუნის „ფორბსის“ კლუბში: ზაქარია ზალიკაშვილის საჯარო ლექცია', '', 0, '', '', '', NULL, 1, NULL),
(11, 'ასდ', '', '', '&lt;p&gt;ასდსადსადსა&lt;/p&gt;', '', '', '', 1, 0, 0, '0000-00-00 00:00:00', '2016-12-14 12:11:54', '2016-12-14 12:11:54', '/page/11', 'სად', '', '', 'სადსა', '', '', 0, NULL, NULL, '', NULL, NULL, NULL),
(12, 'დსა', '', '', '&lt;p&gt;სადსადსა&lt;/p&gt;', '', '', '', 0, 0, 0, '0000-00-00 00:00:00', '2016-12-14 12:13:00', '2016-12-14 12:13:07', '/page/12?category=ასდსადსა', 'დსად', '', '', 'სადსად', '', '', 0, NULL, NULL, '', NULL, 2, NULL),
(13, '1', '1', '1', NULL, NULL, NULL, 'http://localhost:8082/onlinemarket/public_html/files/brand1.png', 1, 0, 0, '0000-00-00 00:00:00', '2016-12-15 10:57:12', '2017-11-29 10:40:59', '#', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '', NULL, 5, NULL),
(14, '2', '2', '2', NULL, NULL, NULL, 'http://localhost:8082/onlinemarket/public_html/files/brand2.png', 1, 0, 0, '0000-00-00 00:00:00', '2016-12-16 07:18:33', '2017-11-29 10:41:08', '#', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '', NULL, 5, NULL),
(15, '1', '1', '1', NULL, NULL, NULL, 'http://localhost:8082/onlinemarket/public_html/files/slide-1-full.jpg', 1, 0, 0, '0000-00-00 00:00:00', '2016-12-16 09:30:04', '2017-11-29 10:40:25', 'სადსადა', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '', NULL, 6, 1),
(16, '2', '2', '2', NULL, NULL, NULL, 'http://localhost:8082/onlinemarket/public_html/files/slide-2-full.jpg', 1, 0, 0, '0000-00-00 00:00:00', '2016-12-16 09:30:24', '2017-11-29 10:40:33', 'დსა', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '', NULL, 6, 3),
(17, '3', '3', '3', NULL, NULL, NULL, 'http://localhost:8082/onlinemarket/public_html/files/slide-3-full.jpg', 1, 0, 0, '0000-00-00 00:00:00', '2016-12-16 09:30:32', '2017-11-29 10:40:40', 'სადსა', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '', NULL, 6, 2),
(18, 'ჩვენს შესახებ', 'About Us', 'About Us', '&lt;pre&gt;&lt;strong&gt;ჩვენს შესახებ&lt;/strong&gt;&lt;/pre&gt;', '&lt;p&gt;About Us&lt;/p&gt;', '&lt;p&gt;About Us&lt;/p&gt;', '', 1, 0, NULL, '0000-00-00 00:00:00', '2017-05-29 07:38:22', '2017-05-29 07:48:29', '/page/18', 'ჩვენს შესახებ', 'About Us', 'About Us', 'ჩვენს შესახებ', 'About Us', 'About Us', 0, NULL, NULL, NULL, NULL, 2, NULL),
(19, '3', '3', '3', NULL, NULL, NULL, 'http://localhost:8082/onlinemarket/public_html/files/brand3.png', 1, 0, NULL, NULL, '2017-11-29 10:43:44', '2017-11-29 10:43:44', '#', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 5, NULL),
(20, '4', '4', '4', NULL, NULL, NULL, 'http://localhost:8082/onlinemarket/public_html/files/brand4.png', 1, 0, NULL, NULL, '2017-11-29 10:43:55', '2017-11-29 10:43:55', '#', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 5, NULL),
(21, '5', '5', '5', NULL, NULL, NULL, 'http://localhost:8082/onlinemarket/public_html/files/brand5.png', 1, 0, NULL, NULL, '2017-11-29 10:44:05', '2017-11-29 10:44:05', '#', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 5, NULL),
(22, '11', '11', '11', NULL, NULL, NULL, 'http://localhost:8082/onlinemarket/public_html/files/brand1.png', 1, 0, NULL, NULL, '2017-11-29 10:44:14', '2017-11-29 10:44:14', '#', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 5, NULL),
(23, '22', '22', '22', NULL, NULL, NULL, 'http://localhost:8082/onlinemarket/public_html/files/brand2.png', 1, 0, NULL, NULL, '2017-11-29 10:44:24', '2017-11-29 10:44:24', '#', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 5, NULL),
(24, '33', '33', '33', NULL, NULL, NULL, 'http://localhost:8082/onlinemarket/public_html/files/brand3.png', 1, 0, NULL, NULL, '2017-11-29 10:44:39', '2017-11-29 10:44:39', '#', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 5, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `web_post_categories`
--

CREATE TABLE `web_post_categories` (
  `news_category_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `web_post_categories`
--

INSERT INTO `web_post_categories` (`news_category_id`, `post_id`, `category_id`, `state`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 0, '2016-11-15 08:09:38', '0000-00-00 00:00:00'),
(2, 2, 1, 1, 0, '2016-11-15 08:26:10', '0000-00-00 00:00:00'),
(3, 3, 1, 1, 0, '2016-11-15 08:26:30', '0000-00-00 00:00:00'),
(4, 4, 2, 1, 0, '2016-11-15 08:27:02', '0000-00-00 00:00:00'),
(5, 5, 2, 1, 0, '2016-11-15 08:27:22', '0000-00-00 00:00:00'),
(10, 6, 1, 1, 0, '2016-11-24 15:17:40', '0000-00-00 00:00:00'),
(16, 10, 1, 1, 0, '2016-12-01 15:43:43', '0000-00-00 00:00:00'),
(17, 9, 2, 1, 0, '2016-12-01 15:47:31', '0000-00-00 00:00:00'),
(18, 7, 1, 1, 0, '2016-12-01 15:47:40', '0000-00-00 00:00:00'),
(19, 8, 1, 1, 0, '2016-12-01 15:47:49', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `web_types`
--

CREATE TABLE `web_types` (
  `int` int(2) DEFAULT NULL,
  `text` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_types`
--

INSERT INTO `web_types` (`int`, `text`) VALUES
(1, 'news'),
(2, 'page'),
(3, 'news_category'),
(4, 'news_tags'),
(5, 'links'),
(6, 'slideshow');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`branch_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `langs`
--
ALTER TABLE `langs`
  ADD PRIMARY KEY (`lang_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `SUPPLIER_CODE_INDX` (`supplier_code`) USING BTREE,
  ADD KEY `PRODUCT_ID_INX` (`product_id`);

--
-- Indexes for table `product_files`
--
ALTER TABLE `product_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_info`
--
ALTER TABLE `product_info`
  ADD PRIMARY KEY (`product_info_id`);

--
-- Indexes for table `product_info_types`
--
ALTER TABLE `product_info_types`
  ADD PRIMARY KEY (`product_info_type_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `sell`
--
ALTER TABLE `sell`
  ADD PRIMARY KEY (`sell_id`);

--
-- Indexes for table `sell_items`
--
ALTER TABLE `sell_items`
  ADD PRIMARY KEY (`sell_item_id`);

--
-- Indexes for table `shoppingcart`
--
ALTER TABLE `shoppingcart`
  ADD PRIMARY KEY (`identifier`,`instance`);

--
-- Indexes for table `storage`
--
ALTER TABLE `storage`
  ADD PRIMARY KEY (`storage_id`,`product_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD PRIMARY KEY (`address_id`);

--
-- Indexes for table `user_tree`
--
ALTER TABLE `user_tree`
  ADD PRIMARY KEY (`user_tree_id`);

--
-- Indexes for table `web_categories`
--
ALTER TABLE `web_categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `web_menu`
--
ALTER TABLE `web_menu`
  ADD PRIMARY KEY (`menu_id`),
  ADD KEY `menu_state_index` (`state`),
  ADD KEY `menu_disabled_index` (`disabled`);

--
-- Indexes for table `web_params`
--
ALTER TABLE `web_params`
  ADD PRIMARY KEY (`param_id`);

--
-- Indexes for table `web_postmeta`
--
ALTER TABLE `web_postmeta`
  ADD PRIMARY KEY (`meta_id`);

--
-- Indexes for table `web_posts`
--
ALTER TABLE `web_posts`
  ADD PRIMARY KEY (`post_id`),
  ADD KEY `pages_state_index` (`state`),
  ADD KEY `pages_disabled_index` (`disabled`);

--
-- Indexes for table `web_post_categories`
--
ALTER TABLE `web_post_categories`
  ADD PRIMARY KEY (`news_category_id`),
  ADD KEY `news_categories_state_index` (`state`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `branch_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `langs`
--
ALTER TABLE `langs`
  MODIFY `lang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=288;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `product_files`
--
ALTER TABLE `product_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `product_info`
--
ALTER TABLE `product_info`
  MODIFY `product_info_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product_info_types`
--
ALTER TABLE `product_info_types`
  MODIFY `product_info_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sell`
--
ALTER TABLE `sell`
  MODIFY `sell_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `sell_items`
--
ALTER TABLE `sell_items`
  MODIFY `sell_item_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `storage`
--
ALTER TABLE `storage`
  MODIFY `storage_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `user_addresses`
--
ALTER TABLE `user_addresses`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user_tree`
--
ALTER TABLE `user_tree`
  MODIFY `user_tree_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `web_categories`
--
ALTER TABLE `web_categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `web_menu`
--
ALTER TABLE `web_menu`
  MODIFY `menu_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `web_params`
--
ALTER TABLE `web_params`
  MODIFY `param_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `web_postmeta`
--
ALTER TABLE `web_postmeta`
  MODIFY `meta_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `web_posts`
--
ALTER TABLE `web_posts`
  MODIFY `post_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `web_post_categories`
--
ALTER TABLE `web_post_categories`
  MODIFY `news_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
