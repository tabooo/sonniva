-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 26, 2021 at 12:57 PM
-- Server version: 10.2.24-MariaDB
-- PHP Version: 7.2.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iomaxge_pixl`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `state_id` tinyint(1) NOT NULL DEFAULT 1,
  `branch_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `state_id`, `branch_id`) VALUES
(1, 'Admin', 'admin@gmail.com', '$2y$10$b9Rs1OvVD/BThlNHmSGaauHgeTzWZsvj9IQJe.GH8hKudziWP0IRG', 'QsWAndIJoe88SZlLj6UU5x7ztnhuMk86V0Rw59jlVEwrn4CXdHAbiecljEtJ', '2017-05-04 07:45:54', '2017-05-04 07:45:54', 1, 1),
(4, 'Vasiko', 'vasiko@gmail.com', '$2y$10$b9Rs1OvVD/BThlNHmSGaauHgeTzWZsvj9IQJe.GH8hKudziWP0IRG', 'lySZSZtN5LGugVJ549qph3UmYFh22h3KSUKVT6Lr5KR2yDbcdTtV6b6qIoT5', '2017-05-04 07:45:54', '2017-05-04 07:45:54', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `branch_id` int(11) NOT NULL,
  `branch_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `branch_state_id` int(2) NOT NULL DEFAULT 1,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`branch_id`, `branch_name`, `branch_state_id`, `updated_at`, `created_at`) VALUES
(2, 'თბილისი', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `childrens` text DEFAULT NULL,
  `parents` text DEFAULT NULL,
  `cancel_date` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  `state_id` int(1) NOT NULL,
  `order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `parent_id`, `childrens`, `parents`, `cancel_date`, `created_at`, `updated_at`, `state_id`, `order`) VALUES
(1, 'ყველა კატეგორია', NULL, '25,26,33,39,44,45', NULL, NULL, '2020-03-04 08:43:04', '2021-03-02 12:09:05', 1, 1),
(2, 'ავეჯის ანჯამა', 1, NULL, '1', NULL, '2020-03-04 08:18:15', '2020-05-11 16:20:06', 2, 2),
(3, 'ავეჯის გორგოლაჭი', 1, NULL, '1', NULL, '2020-03-04 08:18:15', '2020-05-11 16:20:12', 2, 3),
(4, 'ავეჯის კარის საკეტი', 1, NULL, '1', NULL, '2020-03-04 08:19:02', '2020-05-11 16:20:15', 2, 4),
(5, 'ავეჯის ფეხი', 1, NULL, '1', NULL, '2020-03-04 08:19:02', '2020-05-11 16:20:22', 2, 5),
(6, 'ავეჯის ქოში', 1, NULL, '1', NULL, '2020-03-04 08:20:00', '2020-05-11 16:20:31', 2, 6),
(7, 'უჯრის მიმმართველი', 1, NULL, '1', NULL, '2020-03-05 07:51:01', '2020-05-11 16:20:33', 2, 7),
(8, 'ავეჯის სახელურები საკიდები', 1, NULL, '1', NULL, '2020-03-07 09:07:04', '2020-05-11 16:20:37', 2, 8),
(9, 'მოაჯირის აქსესუარები მრგვალი', 1, NULL, '1', NULL, '2020-03-11 06:02:20', '2020-05-11 16:20:39', 2, 9),
(10, 'მოაჯირის აქსესუარები კვადრატული', 1, NULL, '1', NULL, '2020-03-11 06:02:36', '2020-05-11 16:20:42', 2, 10),
(11, 'შუშის აქსესუარები', 1, NULL, '1', NULL, '2020-03-11 06:04:29', '2020-05-11 16:20:46', 2, 11),
(12, 'ხრახნი', 1, NULL, '1', NULL, '2020-03-12 04:52:02', '2020-05-11 16:20:48', 2, 12),
(13, 'მოძრავი კარის გორგოლაჭი', 1, NULL, '1', NULL, '2020-03-12 05:07:38', '2020-05-11 16:20:53', 2, 13),
(14, 'ალუმინის პროფილები', 1, NULL, '1', NULL, '2020-03-12 06:09:46', '2020-05-11 15:50:19', 2, 14),
(15, 'ალუმინის კუთხე', 14, NULL, NULL, NULL, '2020-03-12 06:12:54', '2020-05-11 15:50:19', 2, 15),
(16, 'ალუმინის მილი', 14, NULL, NULL, NULL, '2020-03-12 06:14:22', '2020-05-11 15:50:19', 2, 16),
(17, 'ალუმინის მილკვადრატი', 14, NULL, NULL, NULL, '2020-03-12 06:14:49', '2020-05-11 15:50:19', 2, 17),
(18, 'ალუმინის ზოლი', 14, NULL, NULL, NULL, '2020-03-12 06:15:25', '2020-05-11 15:50:19', 2, 18),
(19, 'ავეჯის კარის ამორტიზატორი', 1, NULL, '1', NULL, '2020-03-13 07:02:35', '2020-05-11 16:20:56', 2, 19),
(20, 'ნიკელის მილი, გადამყვანი', 1, NULL, '1', NULL, '2020-03-14 06:24:24', '2020-05-11 16:20:58', 2, 20),
(21, 'წებო, სილიკონი,ქაფი', 1, NULL, '1', NULL, '2020-03-16 05:53:08', '2020-05-11 16:21:01', 2, 21),
(22, 'თაროს დამჭერი', 1, NULL, '1', NULL, '2020-03-16 10:02:06', '2020-05-11 16:21:05', 2, 22),
(23, 'ალუმინის სახელური', 14, NULL, NULL, NULL, '2020-03-18 06:21:35', '2020-05-11 15:50:19', 2, 23),
(24, 'კუთხოვანა / ლითონი, პლასმასი', 1, NULL, '1', NULL, '2020-03-18 06:44:59', '2020-05-11 16:21:09', 2, 24),
(25, 'საწოლები', 1, '31,32', '1', NULL, '2020-03-18 10:47:00', '2021-01-15 15:27:04', 1, 33),
(26, 'სამზარეულო', 1, '28,29,38,43', '1', NULL, '2020-12-22 16:10:24', '2021-01-15 15:27:04', 1, 26),
(27, 'კომპიუტერული ტექნიკა', 25, NULL, '25', NULL, '2020-12-22 16:10:53', '2020-12-22 16:32:22', 2, 27),
(28, 'აკრილის სამზარეულო', 26, NULL, '26', NULL, '2020-12-22 16:11:06', '2020-12-24 11:57:12', 1, 28),
(29, 'ლამინატის სამზარეულო', 26, NULL, '26', NULL, '2020-12-22 16:11:14', '2020-12-24 11:58:33', 1, 29),
(30, 'სამზარეულოს რბილი კუთხე', 26, NULL, '26', NULL, '2020-12-24 12:00:57', '2020-12-24 14:40:16', 2, 30),
(31, 'ერთსაწოლიანი', 25, NULL, '25', NULL, '2020-12-24 12:07:48', '2021-01-15 13:48:25', 1, 31),
(32, 'ორსაწოლიანი', 25, NULL, '25', NULL, '2020-12-24 12:07:59', '2021-01-15 13:48:25', 1, 32),
(33, 'მაგიდები', 1, '34,36', '1', NULL, '2020-12-24 12:08:35', '2021-01-15 15:27:12', 1, 39),
(34, 'სამზარეულოს მაგიდა', 33, NULL, '33', NULL, '2020-12-24 12:08:48', '2020-12-24 12:09:02', 1, 34),
(35, 'ჟურნალის მაგიდა', 34, NULL, '34', NULL, '2020-12-24 12:10:36', '2020-12-24 12:11:04', 2, 35),
(36, 'ჟურნალის მაგიდა', 33, NULL, '33', NULL, '2020-12-24 12:11:21', '2020-12-24 12:11:21', 1, 36),
(37, 'სხვა', 33, NULL, '33', NULL, '2020-12-24 12:11:36', '2020-12-24 12:12:50', 2, 37),
(38, 'კუთხის სამზარეულო', 26, NULL, '26', NULL, '2020-12-24 12:12:31', '2020-12-24 12:12:31', 1, 38),
(39, 'სკამები', 1, '40', '1', NULL, '2020-12-24 12:14:12', '2021-01-15 15:27:12', 1, 42),
(40, 'სამზარეულოს სკამი', 39, NULL, '39', NULL, '2020-12-24 12:14:27', '2020-12-24 12:14:27', 1, 40),
(41, 'პუფი', 39, NULL, '39', NULL, '2020-12-24 12:14:52', '2020-12-24 12:15:35', 2, 41),
(42, 'პუფი', 1, NULL, '1', NULL, '2020-12-24 12:15:43', '2021-03-02 12:09:05', 2, 44),
(43, 'სამზარეულოს ყუთები', 26, NULL, '26', NULL, '2020-12-24 12:19:27', '2020-12-24 12:19:27', 1, 43),
(44, 'სამზარეულოს რბილი კუთხე', 1, NULL, '1', NULL, '2020-12-24 14:40:22', '2021-01-15 15:27:00', 1, 25),
(45, 'სამზარეულოს ნიჟარები', 1, NULL, '1', NULL, '2021-02-19 14:09:25', '2021-02-19 14:09:26', 1, 45),
(46, 'ინტერიერი და დეკორაცია', 1, NULL, '1', NULL, '2021-03-18 14:26:12', '2021-03-18 14:26:12', 1, 46);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `city_id` int(11) NOT NULL,
  `name_ge` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `name_en` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `name_ru` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `delivery_time` int(2) DEFAULT NULL,
  `state` int(2) DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`city_id`, `name_ge`, `name_en`, `name_ru`, `price`, `delivery_time`, `state`, `created_at`, `updated_at`) VALUES
(1, 'თბილისი', 'Tbilisi', 'Tbilisi', '5.00', 1, 0, '2017-12-25 12:45:15', '2020-03-11 11:57:57'),
(2, 'რუსთავი', 'Rustavi', 'Rustavi', '3.00', 2, 0, '2017-12-25 12:46:09', '2020-03-11 11:57:59'),
(3, 'თბილისი', '', '', '100.00', 0, 0, '2020-03-11 12:13:53', '2020-05-11 15:52:01'),
(4, 'რუსთავი', '', '', '150.00', 0, 0, '2020-03-14 05:24:16', '2020-05-11 15:52:04');

-- --------------------------------------------------------

--
-- Table structure for table `installments_tbc`
--

CREATE TABLE `installments_tbc` (
  `id` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `session_id` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sell_id` int(11) NOT NULL,
  `status` int(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `installments_tbc`
--

INSERT INTO `installments_tbc` (`id`, `state`, `session_id`, `created_at`, `updated_at`, `sell_id`, `status`) VALUES
(19, 1, 'a9948535-2e45-4e1c-b51d-cee6182801e8', '2021-02-16 22:58:57', '2021-02-16 22:58:57', 59, 1),
(20, 1, 'cd4e0cdd-f50b-4eb3-ab71-1a9a0ea2afa4', '2021-02-19 16:06:35', '2021-02-19 16:06:35', 60, 1),
(21, 1, 'b4fba876-c288-43cb-a6e6-d0d63e0df14a', '2021-02-22 13:31:05', '2021-02-22 13:31:05', 61, 1),
(22, 1, '8e49e743-2422-4c7b-91e6-507ce7214a80', '2021-03-12 18:19:41', '2021-03-12 18:19:41', 74, 1),
(23, 1, 'd427c9f0-ca79-4e59-ad95-7acfc7271940', '2021-03-12 18:27:21', '2021-03-12 18:27:21', 77, 1),
(24, 1, 'e1590cf7-d7ae-4496-a08e-bba783eb1608', '2021-03-12 19:20:59', '2021-03-12 19:20:59', 78, 1),
(25, 1, '960c6006-422e-49f0-8454-aa0aa667f6ee', '2021-03-12 23:46:58', '2021-03-12 23:46:58', 82, 1),
(26, 1, '3c3821a4-3c7b-467b-aab0-15f69f76bd13', '2021-03-15 21:14:09', '2021-03-15 21:14:09', 83, 1),
(27, 1, 'f72973cd-25f0-448b-8d9d-9e845a4b0625', '2021-03-15 21:50:58', '2021-03-15 21:50:58', 84, 1),
(28, 1, 'f06c1498-f831-4eb7-a2d1-b0f3d75282f9', '2021-03-23 13:14:50', '2021-03-23 13:14:50', 99, 1),
(29, 1, '5e2e6cca-096f-4807-8399-231759a6bba0', '2021-03-26 12:19:54', '2021-03-26 12:19:54', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `langs`
--

CREATE TABLE `langs` (
  `lang_id` int(11) NOT NULL,
  `key` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `text_ka` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `text_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `text_ru` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `text_tr` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `langs`
--

INSERT INTO `langs` (`lang_id`, `key`, `text_ka`, `text_en`, `text_ru`, `text_tr`, `created_at`, `updated_at`) VALUES
(1, 'ACTION', 'მოქმედება', 'Action', 'Действие', 'Action', '0000-00-00 00:00:00', NULL),
(2, 'ACTIVE', 'აქტიური', 'Active', 'Активный', 'Active', '0000-00-00 00:00:00', NULL),
(3, 'ADD', 'დამატება', 'Add', 'Добавить', 'Add', '0000-00-00 00:00:00', NULL),
(4, 'ADDRESS', 'მისამართი', 'Address', 'Адрес', 'Address', '0000-00-00 00:00:00', NULL),
(5, 'CANCEL', 'გაუქმება', 'Cancel', 'Отменить', 'Cancel', '0000-00-00 00:00:00', NULL),
(6, 'CHANGE_PASSWORD', 'პაროლის შეცვლა', 'Change Password', 'Сменить пароль', 'Change Password', '0000-00-00 00:00:00', NULL),
(7, 'CLEAR', 'გასუფთავება', 'Clear', 'Очистить', 'Clear', '0000-00-00 00:00:00', NULL),
(8, 'CLOSE', 'დახურვა', 'Close', 'Закрыть', 'Close', '0000-00-00 00:00:00', NULL),
(9, 'DATE', 'თარიღი', 'Date', 'Дата', 'Date', '0000-00-00 00:00:00', NULL),
(10, 'DELETE', 'წაშლა', 'Delete', 'Удалить', 'Delete', '0000-00-00 00:00:00', NULL),
(11, 'EDIT', 'რედაქტირება', 'Edit', 'Редактировать', 'Edit', '0000-00-00 00:00:00', NULL),
(12, 'ERROR', 'შეცდომა', 'Error', 'Ошибка', 'Error', '0000-00-00 00:00:00', NULL),
(13, 'EXIT', 'გასვლა', 'Logout', 'Выход', 'Logout', '0000-00-00 00:00:00', NULL),
(14, 'EXPORT', 'ექსპორტი', 'Export', 'Экспорт', 'Export', '0000-00-00 00:00:00', NULL),
(15, 'FINALIZE', 'დასრულება', 'Finalize', 'Завершить', 'Finalize', '0000-00-00 00:00:00', NULL),
(16, 'FIRST_NAME', 'სახელი', 'Firstname', 'Имя', 'Firstname', '0000-00-00 00:00:00', NULL),
(17, 'FROM_WHO', 'ვისგან', 'From', 'Из', 'From', '0000-00-00 00:00:00', NULL),
(18, 'HISTORY', 'ისტორია', 'History', 'История', 'History', '0000-00-00 00:00:00', NULL),
(19, 'IDENTITY_CODE', 'საიდენტიფიკაციო კოდი', 'Identity code', 'Идентификационный код', 'Identity code', '0000-00-00 00:00:00', NULL),
(20, 'INACTIVE', 'არააქტიური', 'არააქტიური', 'Неактивный', 'არააქტიური', '0000-00-00 00:00:00', NULL),
(21, 'INFO', 'ინფო', 'Info', 'Инфо', 'Info', '0000-00-00 00:00:00', NULL),
(22, 'IP_ADDRESS', 'IP მისამართი', 'IP Address', 'IP адрес', 'IP Address', '0000-00-00 00:00:00', NULL),
(23, 'JULY', 'ივლისი', 'July', 'Июль', 'July', '0000-00-00 00:00:00', NULL),
(24, 'JUNE', 'ივნისი', 'June', 'Июнь', 'June', '0000-00-00 00:00:00', NULL),
(25, 'LANGUAGE', 'ენა', 'Language', 'Язык', 'Language', '0000-00-00 00:00:00', NULL),
(26, 'LAST_NAME', 'გვარი', 'Lastname', 'Фамилия', 'Lastname', '0000-00-00 00:00:00', NULL),
(27, 'LOGIN', 'შესვლა', 'Login', 'Войти', 'Login', '0000-00-00 00:00:00', NULL),
(28, 'MAIN_INFO', 'ძირითადი ინფორმაცია', 'Basic Information', 'Основная информация', 'Basic Information', '0000-00-00 00:00:00', NULL),
(29, 'MARCH', 'მარტი', 'March', 'Март', 'March', '0000-00-00 00:00:00', NULL),
(30, 'MAY', 'მაისი', 'May', 'Май', 'May', '0000-00-00 00:00:00', NULL),
(31, 'NEW_PASSWORD', 'ახალი პაროლი', 'New password', 'Новый пароль', 'New password', '0000-00-00 00:00:00', NULL),
(32, 'NO_RECORDS_FOUND', 'ვერ მოიძებნა ჩანაწერები', 'No records found', 'Записей не найдено', 'No records found', '0000-00-00 00:00:00', NULL),
(33, 'NOTE', 'შენიშვნა', 'Note', 'Note', 'Note', '0000-00-00 00:00:00', NULL),
(34, 'NOVEMBER', 'ნოემბერი', 'November', 'Ноябрь', 'November', '0000-00-00 00:00:00', NULL),
(35, 'OCTOBER', 'ოქტომბერი', 'October', 'Октябрь', 'October', '0000-00-00 00:00:00', NULL),
(36, 'OLD_PASSWORD', 'ძველი პაროლი', 'Old password', 'Старый пароль', 'Old password', '0000-00-00 00:00:00', NULL),
(37, 'OPTIONS', 'პარამეტრები', 'Settings', 'Опции', 'Settings', '0000-00-00 00:00:00', NULL),
(38, 'ORGANISATION', 'ორგანიზაცია', 'Organisation', 'Организация', 'Organisation', '0000-00-00 00:00:00', NULL),
(39, 'OVERDUE', 'ვადაგასული', 'Overdue', 'Истекший', 'Overdue', '0000-00-00 00:00:00', NULL),
(40, 'OWN', 'საკუთარი', 'Own', 'Собственный', 'Own', '0000-00-00 00:00:00', NULL),
(41, 'PAGE', 'გვერდი', 'Page', 'Страница', 'Page', '0000-00-00 00:00:00', NULL),
(42, 'PAGES', 'გვერდები', 'Pages', 'Страницы', 'Pages', '0000-00-00 00:00:00', NULL),
(43, 'PASSWORD', 'პაროლი', 'Password', 'Пароль', 'Password', '0000-00-00 00:00:00', NULL),
(44, 'PERSONAL', 'პირადი', 'Personal', 'Личное', 'Personal', '0000-00-00 00:00:00', NULL),
(45, 'PLEASE_WAIT', 'გთხოვთ, დაელოდოთ', 'Please Wait...', 'Пожалуйста, подождите', 'Please Wait...', '0000-00-00 00:00:00', NULL),
(46, 'PRINT', 'ბეჭდვა', 'Print', 'Печать', 'Print', '0000-00-00 00:00:00', NULL),
(47, 'PRINT_PREVIEW', 'საბეჭდად გადახედვა', 'Print Preview', 'Предварительный просмотр', 'Print Preview', '0000-00-00 00:00:00', NULL),
(48, 'REFRESH', 'განახლება', 'Refresh', 'Обновить', 'Refresh', '0000-00-00 00:00:00', NULL),
(49, 'REPEAT_NEW_PASSWORD', 'გაიმეორეთ ახალი პაროლი', 'Repeat new password', 'Повторите новый пароль', 'Repeat new password', '0000-00-00 00:00:00', NULL),
(50, 'RESULT', 'შედეგი', 'Result', 'Результат', 'Result', '0000-00-00 00:00:00', NULL),
(51, 'SAVE', 'შენახვა', 'Save', 'Сохранить', 'Save', '0000-00-00 00:00:00', NULL),
(52, 'SEARCH', 'ძებნა', 'Search', 'Поиск', 'Search', '0000-00-00 00:00:00', NULL),
(53, 'SELECT', 'არჩევა', 'Select', 'Выбирать', 'Select', '0000-00-00 00:00:00', NULL),
(54, 'SELECT_A_FILE', 'აირჩიეთ ფაილი', 'Select a file', 'Выберите файл', 'Select a file', '0000-00-00 00:00:00', NULL),
(55, 'SEND', 'გადაგზავნა', 'Send', 'Переслать', 'Send', '0000-00-00 00:00:00', NULL),
(56, 'SEPTEMBER', 'სექტემბერი', 'September', 'Сентябрь', 'September', '0000-00-00 00:00:00', NULL),
(57, 'STATES', 'სტატუსები', 'Statuses', 'Статусы', 'Statuses', '0000-00-00 00:00:00', NULL),
(58, 'STATE', 'სტატუსი', 'State', 'Статус', 'State', '0000-00-00 00:00:00', NULL),
(59, 'TIME', 'დრო', 'Time', 'Время', 'Time', '0000-00-00 00:00:00', NULL),
(60, 'TITLE', 'სათაური', 'Title', 'название', 'Title', '0000-00-00 00:00:00', NULL),
(61, 'TEXT', 'ტექსტი', 'Text', 'Текст', 'Text', '0000-00-00 00:00:00', NULL),
(62, 'TO_ARCHIVE', 'დაარქივება', 'Archive', 'Архивировать', 'Archive', '0000-00-00 00:00:00', NULL),
(63, 'TO_WHO', 'ვის', 'To', 'Кому', 'To', '0000-00-00 00:00:00', NULL),
(64, 'TOTAL', 'სულ', 'Total', 'Всего', 'Total', '0000-00-00 00:00:00', NULL),
(65, 'TYPE', 'ტიპი', 'Type', 'Вид', 'Type', '0000-00-00 00:00:00', NULL),
(66, 'USERNAME', 'მომხმარებელი', 'User', 'Пользователь', 'User', '0000-00-00 00:00:00', NULL),
(67, 'VERSION', 'ვერსია', 'Version', 'Версия', 'Version', '0000-00-00 00:00:00', NULL),
(68, 'VIEW_ALL', 'ყველას ნახვა', 'View All', 'View All', 'View All', '0000-00-00 00:00:00', NULL),
(69, 'PARCEL_NO', 'ამანათი', 'Parcel #', 'посылка', 'Parcel #', '0000-00-00 00:00:00', NULL),
(70, 'MARK_AS_REGISTERED', 'დადასტურება', 'დადასტურება', 'подтвердить', 'დადასტურება', '0000-00-00 00:00:00', NULL),
(71, 'FILENAMES', 'ფაილები', 'filenames', 'файлы', 'filenames', '0000-00-00 00:00:00', NULL),
(72, 'ADD_INGREDIENT', 'ინგრედიენტის დამატება', 'Add ingredient', 'добавить ингридиент', 'Add ingredient', '0000-00-00 00:00:00', NULL),
(73, 'ADMINISTRATOR', 'ადმინისტრატორი', 'Administrator', 'администратор', 'Administrator', '0000-00-00 00:00:00', NULL),
(74, 'AMOUNT', 'რაოდენობა', 'Amount', 'количество', 'Amount', '0000-00-00 00:00:00', NULL),
(75, 'ASK_DELETE', 'გსურთ წაშლა?', 'Do you want to delete?', 'Удалить?', 'Do you want to delete?', '0000-00-00 00:00:00', NULL),
(76, 'CASHIER', 'მოლარე', 'Cashier', 'кассир', 'Cashier', '0000-00-00 00:00:00', NULL),
(77, 'CATEGORIES', 'კატეგორიები', 'Categories', 'категории', 'Categories', '0000-00-00 00:00:00', NULL),
(78, 'CATEGORY', 'კატეგორია', 'Category', 'категория', 'Category', '0000-00-00 00:00:00', NULL),
(79, 'CHOOSE_SECTION', 'მიუთითეთ სექცია', 'Choose section', 'укажите секцию', 'Choose section', '0000-00-00 00:00:00', NULL),
(80, 'DEFAULT_COLOR', 'თავდაპირველი ფერი', 'Default color', 'начальный цвет', 'Default color', '0000-00-00 00:00:00', NULL),
(81, 'DESCRIPTION', 'აღწერა', 'Description', 'описание', 'Description', '0000-00-00 00:00:00', NULL),
(82, 'FOODS', 'კერძები', 'Foods', 'блюда', 'Foods', '0000-00-00 00:00:00', NULL),
(83, 'IMAGE', 'სურათი', 'Image', 'изображение', 'Image', '0000-00-00 00:00:00', NULL),
(84, 'INGREDIENTS', 'ინგრედიენტები', 'Ingredients', 'ингридиенты', 'Ingredients', '0000-00-00 00:00:00', NULL),
(85, 'MIN_AMOUNT', 'კრიტ. რაოდ.', 'კრიტ. რაოდ.', 'критическое количество', 'კრიტ. რაოდ.', '0000-00-00 00:00:00', NULL),
(86, 'NAME1', 'დასახელება', 'Name', 'Наименование', 'Name', '0000-00-00 00:00:00', NULL),
(87, 'NAME2', 'სახელი', 'Name', 'Имя', 'Name', '0000-00-00 00:00:00', NULL),
(88, 'NAVIGATION', 'ნავიგაცია', 'Navigation', 'навигация', 'Navigation', '0000-00-00 00:00:00', NULL),
(89, 'NEW_SELL', 'ახალი გაყიდვა', 'New sell', 'новая продажа', 'New sell', '0000-00-00 00:00:00', NULL),
(90, 'NOTICE', 'შეტყობინება', 'Notice', 'сообщение', 'Notice', '0000-00-00 00:00:00', NULL),
(91, 'PLACE', 'ადგილი', 'Place', 'место', 'Place', '0000-00-00 00:00:00', NULL),
(92, 'PRICE', 'ფასი', 'Price', 'цена', 'Price', '0000-00-00 00:00:00', NULL),
(93, 'RECIEVINGS', 'მიღებები', 'Recievings', 'закупки', 'Recievings', '0000-00-00 00:00:00', NULL),
(94, 'REPORTS', 'რეპორტები', 'Reports', 'отчеты', 'Reports', '0000-00-00 00:00:00', NULL),
(95, 'SELLS', 'გაყიდვები', 'Sells', 'продажи', 'Sells', '0000-00-00 00:00:00', NULL),
(96, 'TABLES', 'მაგიდები', 'Tables', 'столы', 'Tables', '0000-00-00 00:00:00', NULL),
(97, 'SECTIONS', 'სექციები', 'Sections', 'секции', 'Sections', '0000-00-00 00:00:00', NULL),
(98, 'PLACES', 'ადგილები', 'Places', 'места', NULL, '0000-00-00 00:00:00', NULL),
(99, 'NEW_CATEGORY', 'ახალი კატეგორია', 'New category', 'новая категория', 'New category', '0000-00-00 00:00:00', NULL),
(100, 'ADD_CATEGORY', 'კატეგორიის დამატება', 'Add category', 'добавить категорию', 'Add category', '0000-00-00 00:00:00', NULL),
(101, 'EDIT_CATEGORY', 'კატეგორიის რედაქტირება', 'Edit category', 'редактирование категории', 'Edit category', '0000-00-00 00:00:00', NULL),
(102, 'ONE_PRICE', 'ერთ. ფასი', 'One price', 'цена единицы', 'One price', '0000-00-00 00:00:00', NULL),
(103, 'WHOLEPRICE', 'ჯამი', 'Whole price', 'сумма', 'Whole price', '0000-00-00 00:00:00', NULL),
(104, 'INVOICE_NO', 'ინვოისის №', 'Invoice №', '№ Инвойса', 'Invoice №', '0000-00-00 00:00:00', NULL),
(105, 'PROVIDER', 'მომწოდებელი', 'Provider', 'поставщик', 'Provider', '0000-00-00 00:00:00', NULL),
(106, 'MANUFACTURER', 'მწარმოებელი', 'Manufacturer', 'изготовитель', 'Manufacturer', '0000-00-00 00:00:00', NULL),
(107, 'YES', 'დიახ', 'Yes', 'Да', 'Yes', '0000-00-00 00:00:00', '2017-05-02 13:21:05'),
(108, 'NO', 'არა', 'No', 'Нет', 'No', '0000-00-00 00:00:00', NULL),
(109, 'NEW_INGREDIENT', 'ახალი ინგრედიენტი', 'New ingredient', 'новый ингридиент', 'New ingredient', '0000-00-00 00:00:00', NULL),
(110, 'RECIEVE_INGREDIENT', 'ინგრედიენტის მიღება', 'Recieve ingredient', 'прием ингридиента', 'Recieve ingredient', '0000-00-00 00:00:00', NULL),
(111, 'CHOOSE_TABLE', 'მიუთითეთ მაგიდა', 'Choose table', 'укажите стол', 'Choose table', '0000-00-00 00:00:00', NULL),
(112, 'CASH', 'ნაღდი', 'Cash', 'наличные', 'Cash', '0000-00-00 00:00:00', NULL),
(113, 'TRANSFER', 'გადარიცხვა', 'Transfer', 'безналичные', 'Transfer', '0000-00-00 00:00:00', NULL),
(114, 'NEW_PLACE', 'ახალი ადგილი', 'New place', 'новое место', 'New place', '0000-00-00 00:00:00', NULL),
(115, 'ADD_PLACE', 'ადგილის დამატება', 'Add place', 'добавить место', 'Add place', '0000-00-00 00:00:00', NULL),
(116, 'ADD_INGREDIENT_ON_PRODUCT', 'პროდუქტზე ინგრედიენტის დამატება', 'Add ingredient on product', 'добавить ингридиент в продукт', 'Add ingredient on product', '0000-00-00 00:00:00', NULL),
(117, 'INGREDIENT', 'ინგრედიენტი', 'Ingredient', 'ингридиент', 'Ingredient', '0000-00-00 00:00:00', NULL),
(118, 'NEW_PRODUCT', 'ახალი პროდუქტი', 'new Product', 'Новый товар', 'new Product', '0000-00-00 00:00:00', NULL),
(119, 'ADD_PRODUCT', 'კერძის დამატება', 'Add product', 'добавить блюдо', 'Add product', '0000-00-00 00:00:00', NULL),
(120, 'ADD_SECTION', 'სექციის დამატება', 'Add section', 'добавить секцию', 'Add section', '0000-00-00 00:00:00', NULL),
(121, 'SECTION', 'სექცია', 'Section', 'секциа', 'Section', '0000-00-00 00:00:00', NULL),
(122, 'FILL_ALL_FIELDS', 'გთხოვთ მიუთითოთ ყველა აუცილებელი ველი', 'Fill all required fields', 'заполните все обязательные поля', 'Fill all required fields', '0000-00-00 00:00:00', NULL),
(123, 'ADD_TABLE', 'მაგიდის დამატება', 'Add table', 'добавить стол', 'Add table', '0000-00-00 00:00:00', NULL),
(124, 'NEW_TABLE', 'ახალი მაგიდა', 'New table', 'новый стол', 'New table', '0000-00-00 00:00:00', NULL),
(125, 'TABLE', 'მაგიდა', 'Table', 'стол', 'Table', '0000-00-00 00:00:00', NULL),
(126, 'CANCEL_ORDER', 'შეკვეთის გაუქმება', 'Cancel order', 'отмена заказа', 'Cancel order', '0000-00-00 00:00:00', NULL),
(127, 'USERS', 'მომხმარებლები', 'Users', 'пользователи', 'Users', '0000-00-00 00:00:00', NULL),
(128, 'ADD_USER', 'მომხმარებლის დამატება', 'User added', 'добавить пользователя', 'User added', '0000-00-00 00:00:00', NULL),
(129, 'RANK', 'რანკი', 'Rank', 'ранг', 'Rank', '0000-00-00 00:00:00', NULL),
(130, 'PERSONAL_NO', 'პირადი ნომერი', 'Personal No', 'личный номер', 'Personal No', '0000-00-00 00:00:00', NULL),
(131, 'PHONE', 'ტელეფონი', 'Phone', 'телефон', 'Phone', '0000-00-00 00:00:00', NULL),
(132, 'EMAIL', 'ელ. ფოსტა', 'eMail', 'эл.почта', 'eMail', '0000-00-00 00:00:00', NULL),
(133, 'PRINTER', 'პრინტერი', 'Printer', 'Принтер', 'Printer', '0000-00-00 00:00:00', NULL),
(134, 'WAITER', 'ოფიციანტი', 'Waiter', 'официант', 'Waiter', '0000-00-00 00:00:00', NULL),
(135, 'SELLITEM_REPORTS', 'კერძების რეპორტები', 'კერძების რეპორტები', 'Отчеты по блюдам', 'კერძების რეპორტები', '0000-00-00 00:00:00', NULL),
(136, 'ORDER_NUMBER', 'შეკვ. №', 'Order №', 'Заказ. №', 'Order №', '0000-00-00 00:00:00', NULL),
(137, 'SEARCH_ORDER', 'შეკვეთის ძებნა', 'Search Order', 'Поиск заказа', 'Search Order', '0000-00-00 00:00:00', NULL),
(138, 'SELF_PRICE', 'თვითღირებულება', 'Self Price', 'Себестоимость', 'Self Price', '0000-00-00 00:00:00', NULL),
(139, 'CALL', 'გამოძახება', 'CALL', 'Вызов', 'CALL', '0000-00-00 00:00:00', NULL),
(140, 'MESSAGE', 'შეტყობინება', 'Message', 'Сообщение', 'Message', '0000-00-00 00:00:00', NULL),
(141, 'CONFIRM', 'დასტური', 'Confirm', 'Подтвердить', 'Confirm', '0000-00-00 00:00:00', NULL),
(142, 'MENUS', 'მენიუები', 'Menus', 'Меню', 'Menus', '0000-00-00 00:00:00', NULL),
(143, 'ADD_MENU', 'მენიუს დამატება', 'Add Menu', 'Добавить меню', 'Add Menu', '0000-00-00 00:00:00', NULL),
(144, 'PERCENT', 'პროცენტი', 'Percent', 'Процент', 'Percent', '0000-00-00 00:00:00', NULL),
(145, 'TAKE_MONEY', 'მოწ. თანხა', 'Money', 'Поданная сумма', 'Money', '0000-00-00 00:00:00', NULL),
(146, 'RETURN_CHANGE', 'ხურდა', 'Change', 'Сдача', 'Change', '0000-00-00 00:00:00', NULL),
(147, 'MESSAGES', 'შეტყობინებები', 'Messages', 'Сообщения', 'Messages', '0000-00-00 00:00:00', NULL),
(148, 'INGREDIENTS_REPORTS', 'ინგრედიენტების რეპორტები', 'Ingredients Reports', 'Отчеты по ингридиентам', 'Ingredients Reports', '0000-00-00 00:00:00', NULL),
(149, 'OPERATIONS', 'ოპერაციები', 'Operations', 'Операции', 'Operations', '0000-00-00 00:00:00', NULL),
(150, 'RECIEVING_HISTORY', 'მიღებების ისტორია', 'Recieving History', 'История закупок', 'Recieving History', '0000-00-00 00:00:00', NULL),
(151, 'SELL_HISTORY', 'გაყიდვების ისტორია', 'Sell History', 'История продаж', 'Sell History', '0000-00-00 00:00:00', NULL),
(152, 'GUEST_COUNT', 'სტუმრების რაოდენობა', 'Guest Count', 'Кол-во гостей', 'Guest Count', '0000-00-00 00:00:00', NULL),
(153, 'VIEW_ORDER', 'შეკვეთის ნახვა', 'View Order', 'Смотреть заказ', 'View Order', '0000-00-00 00:00:00', NULL),
(154, 'ADD_IMAGE', 'სურათის დამატება', 'Add Image', 'Добавить картинку', 'Add Image', '0000-00-00 00:00:00', NULL),
(155, 'SEND_MSG', 'მიწერე მიმტანს', 'Send Message', 'Послать сообщение', 'Send Message', '0000-00-00 00:00:00', NULL),
(156, 'ADDITIONAL_INFO', 'დამატებითი ინფო', 'Additional Info', 'Доп. Инфо', 'Additional Info', '0000-00-00 00:00:00', NULL),
(157, 'ADD_ADDITIONAL_INFO', 'დამატებითი ინფოს დამატება', 'Add Additional Info', 'Добавить доп. инфо', 'Add Additional Info', '0000-00-00 00:00:00', NULL),
(158, 'ADD_NEW_ADDETIONAL_INFO_TYPE', 'ახალი დამატებითი ინფოს დამატება', 'Add new additional info type', 'Добавить новое доп. инфо', 'Add new additional info type', '0000-00-00 00:00:00', NULL),
(159, 'CHOOSE_FILE', 'აირჩიეთ ფაილი', 'Choose file', 'Выбрать файл', 'Choose file', '0000-00-00 00:00:00', NULL),
(160, 'RESOURCES', 'რესურსები', 'Resources', 'Ресурсы', 'Resources', '0000-00-00 00:00:00', NULL),
(161, 'FILE_SIZE_ERROR_30', 'ფაილის ზომა არ უნდა აღემატებოდეს 30 kb-ს', 'Max allowed file size is 30 kb', 'Размер файла не должен превышать 30 Кб', 'Max allowed file size is 30 kb', '0000-00-00 00:00:00', NULL),
(162, 'FILE_SIZE_ERROR_500', 'ფაილის ზომა არ უნდა აღემატებოდეს 500 kb-ს', 'Max allowed file size is 500 kb', 'Размер файла не должен превышать 500 Кб', 'Max allowed file size is 500 kb', '0000-00-00 00:00:00', NULL),
(163, 'CHOOSE_FILE_ERROR', 'გთხოვთ მიუთითოთ ფაილი', 'Choose file', 'Укажите файл', 'Choose file', '0000-00-00 00:00:00', NULL),
(164, 'RECIEVE_PRODUCT', 'პროდუქტის მიღება', 'Recieve product', 'Принять товар', 'Recieve product', '0000-00-00 00:00:00', NULL),
(165, 'SPLIT_AMOUNT', 'დაშლილი რაოდენობა', 'Split Amount', 'Штучное кол-во', 'Split Amount', '0000-00-00 00:00:00', NULL),
(166, 'BARCODE', 'შტრიხკოდი', 'Barcode', 'Штрихкод', 'Barcode', '0000-00-00 00:00:00', NULL),
(167, 'IN_STORAGE', 'საწყობში', 'In Storage', 'В складе', 'In Storage', '0000-00-00 00:00:00', NULL),
(168, 'RECIEVE_BY_INVOICE', 'ინვოისით მიღება', 'Recieve by Invoice', 'Прием по инвойсу', 'Recieve by Invoice', '0000-00-00 00:00:00', NULL),
(169, 'WEIGHT', 'წონა', 'Weight', 'Вес', 'Weight', '0000-00-00 00:00:00', NULL),
(170, 'WARRANTY', 'გარანტია', 'Warranty', 'Гарантия', 'Warranty', '0000-00-00 00:00:00', NULL),
(171, 'CONDITION', 'მდგომარეობა', 'Condition', 'Состояние', 'Condition', '0000-00-00 00:00:00', NULL),
(172, 'NEW', 'ახალი', 'New', 'Новый', 'New', '0000-00-00 00:00:00', NULL),
(173, 'USED', 'მეორადი', 'Used', 'Вторичный', 'Used', '0000-00-00 00:00:00', NULL),
(174, 'PRICE_WHOLESALE', 'ფასი საბითუმო', 'Price Wholesale', 'Оптовая цена', 'Price Wholesale', '0000-00-00 00:00:00', NULL),
(175, 'PRICE_VIP', 'ფასი VIP', 'Price VIP', 'Цена VIP', 'Price VIP', '0000-00-00 00:00:00', NULL),
(176, 'SPLIT', 'დაშლა', 'Split', 'Поштучно', 'Split', '0000-00-00 00:00:00', NULL),
(177, 'FILL_SPLIT_AMOUNT', 'მიუთითეთ დაშლის რაოდენობა', 'Fill Split Amount', 'Укажите поштучное кол-во', 'Fill Split Amount', '0000-00-00 00:00:00', NULL),
(178, 'INVOICE_NUMBER', 'ინვოისის ნომერი', 'Invoice Number', 'Номер инвойса', 'Invoice Number', '0000-00-00 00:00:00', NULL),
(179, 'FILL_ALL_IMPORTANT_FIELDS', 'შეავსეთ ყველა სავალდებულო ველი', 'Fill all important fields', 'Заполните все обязательные поля', 'Fill all important fields', '0000-00-00 00:00:00', NULL),
(180, 'SEARCH_PRODUCT', 'პროდუქტის ძებნა', 'Search Product', 'Поиск товара', 'Search Product', '0000-00-00 00:00:00', NULL),
(181, 'BRANCH', 'ფილიალი', 'Branch', 'Филиал', 'Branch', '0000-00-00 00:00:00', NULL),
(182, 'WHOLE', 'მთლიანი', 'Whole', 'Целый', 'Whole', '0000-00-00 00:00:00', NULL),
(183, 'PRODUCTS', 'პროდუქტები', 'Products', 'Товары', 'Products', '0000-00-00 00:00:00', NULL),
(184, 'CLIENTS', 'კლიენტები', 'Clients', 'Клиенты', 'Clients', '0000-00-00 00:00:00', NULL),
(185, 'PAYMENTS', 'გადახდები', 'Payments', 'Платежи', 'Payments', '0000-00-00 00:00:00', NULL),
(186, 'ADD_PAYMENT', 'გადახდა', 'New Payment', 'Оплатить', 'New Payment', '0000-00-00 00:00:00', NULL),
(187, 'PAYMENT_TYPE', 'გადახდის ტიპი', 'Payment Type', 'Тип оплаты', 'Payment Type', '0000-00-00 00:00:00', NULL),
(188, 'MONEY', 'თანხა', 'Money', 'Сумма', 'Money', '0000-00-00 00:00:00', NULL),
(189, 'CHANGE', 'შეცვლა', 'Change', 'Изменить', 'Change', '0000-00-00 00:00:00', NULL),
(190, 'SELLER', 'გამყიდველი', 'Seller', 'Продавец', 'Seller', '0000-00-00 00:00:00', NULL),
(191, 'SPLIT_PRICE', 'დაშლის ფასი', 'Split Price', 'Поштучнай цена', 'Split Price', '0000-00-00 00:00:00', NULL),
(192, 'DATABASE_NULL', 'ბაზის განულება', 'Delete Database', 'Обнулить базу', 'Delete Database', '0000-00-00 00:00:00', NULL),
(193, 'DATABASE_NULLED_OK', 'ბაზა განულდა წარმატებით', 'Database was nulled succesfuly', 'База обнулена успешно', 'Database was nulled succesfuly', '0000-00-00 00:00:00', NULL),
(194, 'REMOVED', 'გაუქმებული', 'Removed', 'Отенено', 'Removed', '0000-00-00 00:00:00', NULL),
(195, 'CHANGE_LANGUAGE', 'ენის შეცვლა', 'Change Language', 'Изменить язык', 'Change Language', '0000-00-00 00:00:00', NULL),
(196, 'CHANGE_THEME', 'თემის შეცვლა', 'Change Theme', 'Изменить тему', 'Change Theme', '0000-00-00 00:00:00', NULL),
(197, 'BONUS', 'ბონუსი', 'Bonus', 'Бонус', 'Bonus', '0000-00-00 00:00:00', NULL),
(198, 'NEW_CLIENT', 'ახალი კლიენტი', 'New Client', 'Новый клиент', 'New Client', '0000-00-00 00:00:00', NULL),
(199, 'CLIENT', 'კლიენტი', 'Client', 'Клиент', 'Client', '0000-00-00 00:00:00', NULL),
(200, 'CHOOSE_CLIENT', 'კლიენტის არჩევა', 'Choose Client', 'Выбрать клиента', 'Choose Client', '0000-00-00 00:00:00', NULL),
(201, 'AMOUNT_IN_STORAGE', 'რაოდ. საწყობში', 'Amount in Storage', 'Кол-во на складе', 'Amount in Storage', '0000-00-00 00:00:00', NULL),
(202, 'NEED_RECEIPT', 'საჭიროებს რეცეპტს', 'Need Receipt', 'Нужен рецепт', 'Need Receipt', '0000-00-00 00:00:00', NULL),
(203, 'EARNING', 'მოგება', 'Earning', 'Прибыль', 'Earning', '0000-00-00 00:00:00', NULL),
(204, 'SELL_PRICE', 'გასაყიდი ფასი', 'Sell Price', 'Продажная цена', 'Sell Price', '0000-00-00 00:00:00', NULL),
(205, 'VAT', 'დ.ღ.გ.', 'VAT', 'Н.Д.С.', 'VAT', '0000-00-00 00:00:00', NULL),
(206, 'INCLUDE_VAT', 'დ.ღ.გ.–ის ჩათვლით', 'Include VAT', 'С учетом НДС', 'Include VAT', '0000-00-00 00:00:00', NULL),
(207, 'WITHOUT_VAT', 'დ.ღ.გ.–ის გარეშე', 'Without VAT', 'Без НДС', 'Without VAT', '0000-00-00 00:00:00', NULL),
(208, 'CONFIRM_PRODUCT_SELL_PRICE_CHANGE', 'პროდუქტის გასაყიდი ფასი შეიცვალა. გინდათ შეიცვალოს პროდუქტის გასაყიდი ფასი?', 'Product sell price was changed. Do you want to change product sell price with new price?', 'Продажная ценв товара изменилась.', 'Product sell price was changed. Do you want to change product sell price with new price?', '0000-00-00 00:00:00', NULL),
(209, 'CALCULATE_PERCENT', 'პროცენტის გამოთვლა', 'Calculate Percent', 'Подсчет процентов', 'Calculate Percent', '0000-00-00 00:00:00', NULL),
(210, 'FROM', 'დან', 'From', 'От', 'From', '0000-00-00 00:00:00', NULL),
(211, 'BEFORE', 'მდე', 'Before', 'До', 'Before', '0000-00-00 00:00:00', NULL),
(212, 'PRODUCT', 'პროდუქტი', 'Product', 'Продукт', 'Product', '0000-00-00 00:00:00', NULL),
(213, 'SOLD_PRODUCTS', 'გაყიდული პროდუქტები', 'Sold Products', 'Продано Продукты', 'Sold Products', '0000-00-00 00:00:00', NULL),
(214, 'THIS_PRODUCT_IS_NOT_IN_STORAGE', 'მითითებული პროდუქტი არ არის საწყობში', 'This product is not in storage', 'Этот продукт не в памяти', 'This product is not in storage', '0000-00-00 00:00:00', NULL),
(215, 'CHOOSE_PRODUCT', 'აირჩიეთ პროდუქტი', 'Choose Product', 'Выберите продукт', 'Choose Product', '0000-00-00 00:00:00', NULL),
(216, 'SELECTED_NUMBER_IS_GREATER_THAN_THE_NUMBER_OF_STOCK', 'მითითებული რაოდენობა მეტია საწყობში არსებულ რაოდენობაზე', 'Set the number is greater than the number of stock', 'Установите число больше, чем число акций', 'Set the number is greater than the number of stock', '0000-00-00 00:00:00', NULL),
(217, 'SPECIFY_THE_NUMBER_OF_SALE', 'მიუთითეთ გასაყიდი რაოდენობა', 'Specify the number of sale', 'Укажите количество продажи', 'Specify the number of sale', '0000-00-00 00:00:00', NULL),
(218, 'ADD_SUBCATEGORY', 'ქვეკატეგორიის დამატება', 'Add Subcategory', 'Добавить подкатегорию', 'Add Subcategory', '0000-00-00 00:00:00', NULL),
(219, 'PARENT', 'მშობელი', 'Parent', 'Родитель', 'Parent', '0000-00-00 00:00:00', NULL),
(220, 'DISCARD_INGREDIENT', 'ინგრედიენტის ჩამოწერა', 'Discard Ingredient', 'ინგრედიენტის ჩამოწერა', 'Discard Ingredient', '0000-00-00 00:00:00', NULL),
(221, 'DISCARDED_INGREDIENTS', 'ჩამოწერილი ინრედიენტები', 'Discarded Ingredients', 'ჩამოწერილი ინრედიენტები', 'Discarded Ingredients', '0000-00-00 00:00:00', NULL),
(222, 'DISCARD_AMOUNT', 'ჩამოსაწერი რაოდენობა', 'Discard Amount', 'ჩამოსაწერი რაოდენობა', 'Discard Amount', '0000-00-00 00:00:00', NULL),
(223, 'DATABASE', 'ბაზა', 'Database', 'ბაზა', 'Database', '0000-00-00 00:00:00', NULL),
(224, 'DOWNLOAD_DATABASE_FOR_WEB', 'ბაზის ჩამოტვირთვა საიტისთვის', 'Download Database for WEB', 'ბაზის ჩამოტვირთვა საიტისთვის', 'Download Database for WEB', '0000-00-00 00:00:00', NULL),
(225, 'REMAINS', 'ნაშთები', 'Remains', 'ნაშთები', 'Remains', '0000-00-00 00:00:00', NULL),
(226, 'PRE_CHECK', 'წინასწარი ჩეკი', 'Pre Check', 'წინასწარი ჩეკი', 'Pre Check', '0000-00-00 00:00:00', NULL),
(227, 'SALE_PERCENT', 'ფასდაკ. %', 'Sale %', 'ფასდაკლება %', 'Sale %', '0000-00-00 00:00:00', NULL),
(228, 'CODE', 'კოდი', 'Code', 'Code', 'Code', '0000-00-00 00:00:00', NULL),
(229, 'INCORRECT_VALUES', 'არასწორი მონაცემები!', 'Incorrect values', 'Incorrect values', 'Incorrect values', '0000-00-00 00:00:00', NULL),
(230, 'DAY_TRADED_VOLUME', 'ღის ნავაჭრი', 'Day trading volume', 'Day trading volume', 'Day trading volume', '0000-00-00 00:00:00', NULL),
(231, 'MAIN_PAGE', 'მთავარი გვერდი', 'Main Page', 'Main Page', 'Main Page', '0000-00-00 00:00:00', NULL),
(232, 'COMMENT', 'კომენტარი', 'Comment', 'Comment', 'Comment', '0000-00-00 00:00:00', NULL),
(233, 'ORDERS', 'შეკვეთები', 'Orders', 'Orders', 'Orders', '0000-00-00 00:00:00', NULL),
(234, 'SELLING_PRICE_HAS_NOT_SET', 'გასაყიდი ფასი არ აქვს მითითებული', 'Selling price has not set', 'Selling price has not set', 'Selling price has not set', '0000-00-00 00:00:00', NULL),
(235, 'NEW_SECTION', 'ახალი სექცია', 'New Saction', 'New Saction', 'New Saction', '0000-00-00 00:00:00', NULL),
(236, 'PORTION', 'პორცია', 'Portion', 'Portion', 'Portion', '0000-00-00 00:00:00', NULL),
(237, 'ADD_TYPE', 'ტიპის დამატება', 'Add Type', 'Add Type', 'Add Type', '0000-00-00 00:00:00', NULL),
(238, 'AMOUNT_SHORT', 'რაოდ.', 'Am.', 'Am.', 'Am.', '0000-00-00 00:00:00', NULL),
(239, 'FOR_SERVICE', 'მომსახურების', 'For Service', 'For Service', 'For Service', '0000-00-00 00:00:00', NULL),
(240, 'GEL', 'ლ', 'GEL', 'GEL', 'GEL', '0000-00-00 00:00:00', NULL),
(241, 'SALE', 'ფასდაკლება', 'Sale', 'Sale', 'Sale', '0000-00-00 00:00:00', NULL),
(242, 'PLACE_OF_MAKING', 'გატანის ადგილი', 'Place Of Making', 'Place Of Making', 'Place Of Making', '0000-00-00 00:00:00', NULL),
(243, 'SUM', 'ჯამი', 'Sum', 'Sum', 'Sum', '0000-00-00 00:00:00', NULL),
(244, 'LANGUAGES', 'ენები', 'Languages', 'Languages', 'Languages', '0000-00-00 00:00:00', NULL),
(245, 'NEW_PHRASE', 'ახალი ფრაზა', 'New Phrase', 'New Phrase', 'New Phrase', '0000-00-00 00:00:00', NULL),
(246, 'TURKISH', 'თურქულად', 'Turkish', 'Turkish', 'Turkish', '0000-00-00 00:00:00', NULL),
(247, 'ENGLISH', 'ინგლისურად', 'English', 'English', 'English', '0000-00-00 00:00:00', NULL),
(248, 'RUSSIAN', 'რუსულად', 'Russian', 'Russian', 'Russian', '0000-00-00 00:00:00', NULL),
(249, 'GEORGIAN', 'ქართულად', 'Georgian', 'Georgian', 'Georgian', '0000-00-00 00:00:00', NULL),
(250, 'WAYBILLS', 'ზედნადებები', 'Waybills', 'Waybills', 'Waybills', '0000-00-00 00:00:00', NULL),
(251, 'WAYBILL_NO', 'ზედნადებების №', 'Waybill No', 'Waybill No', 'Waybill No', '0000-00-00 00:00:00', NULL),
(252, 'SELLER_NAME', 'გამყიდველი', 'Seller Name', 'Seller Name', 'Seller Name', '0000-00-00 00:00:00', NULL),
(253, 'ACTIVATE_DATE', 'აქტივაციის თარიღი', 'Activate Date', 'Activate Date', 'Activate Date', '0000-00-00 00:00:00', NULL),
(254, 'CREATE_DATE', 'შექმნის თარიღი', 'Create Date', 'Create Date', 'Create Date', '0000-00-00 00:00:00', NULL),
(255, 'CLOSE_DATE', 'დასრულების თარიღი', 'Close Date', 'Close Date', 'Close Date', '0000-00-00 00:00:00', NULL),
(256, 'DRIVER_NAME', 'მძღოლი', 'Driver Name', 'Driver Name', 'Driver Name', '0000-00-00 00:00:00', NULL),
(257, 'CAR', 'ავტო', 'Car', 'Car', 'Car', '0000-00-00 00:00:00', NULL),
(258, 'TRANS_PRICE', 'ტრანს. თანხა', 'Trans. Price', 'Trans. Price', 'Trans. Price', '0000-00-00 00:00:00', NULL),
(259, 'DELIVERY_DATE', 'მიწოდების თარიღი', 'Delivery Date', 'Delivery Date', 'Delivery Date', '0000-00-00 00:00:00', NULL),
(260, 'VIEW', 'ნახვა', 'View', 'View', 'View', '0000-00-00 00:00:00', NULL),
(261, 'CONFIRM_RECEIVE', 'მიღების დადასტურება', 'Confirm Receive', 'Confirm Receive', 'Confirm Receive', '0000-00-00 00:00:00', NULL),
(262, 'BAR_CODE', 'შტრიხკოდი', 'Barcode', 'Barcode', 'Barcode', '0000-00-00 00:00:00', NULL),
(263, 'ADD_BARCODE', 'შტრიხკოდის დამატება', 'Add Barcode', 'Add Barcode', 'Add Barcode', '0000-00-00 00:00:00', NULL),
(264, 'RECEIVE', 'მიღება', 'Receive', 'Receive', 'Receive', '0000-00-00 00:00:00', NULL),
(265, 'CAR_NO', 'მანქანის №', 'Car No', 'Car No', 'Car No', '0000-00-00 00:00:00', NULL),
(266, 'GRANT_RIGHTS', 'უფლებების მინიჭება', 'Grant Rights', 'Grant Rights', 'Grant Rights', '0000-00-00 00:00:00', NULL),
(267, 'SUPPLIERS', 'მომწოდებლები', 'Suppliers', 'Suppliers', 'Suppliers', '0000-00-00 00:00:00', NULL),
(268, 'TRADED', 'ნავაჭრი', 'Traded', 'Traded', 'Traded', '0000-00-00 00:00:00', NULL),
(269, 'RECEIPT', 'რეცეპტი', 'Receipt', 'Receipt', 'Receipt', '0000-00-00 00:00:00', NULL),
(270, 'STORAGE', 'საწყობი', 'Storage', 'Storage', 'Storage', '0000-00-00 00:00:00', NULL),
(271, 'DISCARDS', 'ჩამოწერები', 'Discards', 'Discards', 'Discards', '0000-00-00 00:00:00', NULL),
(272, 'PAYED_MONEY', 'გადახდილი თანხა', 'Payed Money', 'Payed Money', 'Payed Money', '0000-00-00 00:00:00', NULL),
(273, 'DEADLINE', 'ვადა', 'Deadline', 'Deadline', 'Deadline', '0000-00-00 00:00:00', NULL),
(275, 'UPLOAD_TO_RS', 'აიტვირთოს rs.ge-ზე', 'Upload to RS', 'Upload to RS', 'Upload to RS', '0000-00-00 00:00:00', NULL),
(276, 'CARD', 'ბარათი', 'Card', 'Card', 'Card', '0000-00-00 00:00:00', NULL),
(277, 'DEBT', 'ვალი', 'Debt', 'Debt', 'Debt', '0000-00-00 00:00:00', NULL),
(278, 'TIN', 'საიდენტიფიკაციო ნომერი', 'TIN', 'TIN', 'TIN', '0000-00-00 00:00:00', NULL),
(279, 'DRIVERS', 'მძღოლები', 'Drivers', 'Drivers', 'Drivers', '0000-00-00 00:00:00', NULL),
(280, 'DRIVER', 'მძღოლი', 'Driver', 'Driver', 'Driver', '0000-00-00 00:00:00', NULL),
(281, 'CHOOSE_DRIVER', 'აირჩიეთ მძღოლი', 'Select Driver', 'Select Driver', 'Select Driver', '0000-00-00 00:00:00', NULL),
(282, 'NEW_DRIVER', 'ახალი მძღოლი', 'New Driver', 'New Driver', 'New Driver', '0000-00-00 00:00:00', NULL),
(283, 'CARS', 'მანქანები', 'Cars', 'Cars', 'Cars', '0000-00-00 00:00:00', NULL),
(284, 'STORAGES', 'საწყობები', 'Storages', 'Storages', 'Storages', '0000-00-00 00:00:00', NULL),
(285, 'ADD_STORAGE', 'საწყობის დამატება', 'Add Storage', 'Add Storage', 'Add Storage', '0000-00-00 00:00:00', NULL),
(286, 'MOVE_PRODUCTS', 'პროდუქტების გადატანა', 'Move Products', 'Move Products', 'Move Products', '0000-00-00 00:00:00', NULL),
(287, 'TRUCK', '????????', 'Truck', 'Truck', 'Truck', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_05_02_140156_entrust_setup_tables', 2),
(4, '2017_05_05_144232_entrust_setup_tables', 3),
(5, '2017_06_01_102836_create_admins_table', 4),
(6, '2017_06_05_074505_entrust_setup_tables', 5),
(7, '2017_06_05_095027_entrust_setup_tables', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `transaction_id` varchar(32) DEFAULT NULL,
  `payment_id` varchar(12) DEFAULT NULL,
  `payment_date` varchar(19) DEFAULT NULL,
  `amount` varchar(12) DEFAULT NULL,
  `card_number` varchar(25) DEFAULT NULL,
  `card_type` varchar(25) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `reason` varchar(64) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`transaction_id`, `payment_id`, `payment_date`, `amount`, `card_number`, `card_type`, `status`, `reason`, `created_at`, `updated_at`) VALUES
('5a4f8d7c7b52f', '800518020824', '05.01.2018 18:36:51', '616600', NULL, 'CRTU!478763******4584', 'Y', '966729', '2018-01-05 14:38:51', '2018-01-05 14:38:51'),
('5a4f8dd94549c', '800518020827', '05.01.2018 18:38:25', '616600', NULL, 'CRTU!478763******4584', 'Y', '197394', '2018-01-05 14:40:27', '2018-01-05 14:40:27'),
('5a4f90b909dc2', '800518020881', '05.01.2018 18:50:40', '616600', NULL, 'CRTU!478763******4584', 'Y', '853485', '2018-01-05 14:52:55', '2018-01-05 14:52:55'),
('5a4f9161a100f', '800518020898', '05.01.2018 18:53:28', '616600', NULL, 'CRTU!478763******4584', 'Y', '968976', '2018-01-05 14:55:38', '2018-01-05 14:55:38'),
('5a4f9049cd23c', '800518020872', '05.01.2018 18:49:04', '616600', NULL, 'CRTU!478763******4584', 'Y', '526913', '2018-01-05 14:56:15', '2018-01-05 14:56:15'),
('5a560b9c912e0', '801016019186', '10.01.2018 16:48:29', '269100', NULL, 'CRTU!478763******4584', 'Y', '108993', '2018-01-10 12:50:39', '2018-01-10 12:50:39'),
('5a560c706f67f', '801016019205', '10.01.2018 16:51:56', '269100', NULL, 'CRTU!478763******4584', 'Y', '397842', '2018-01-10 12:54:00', '2018-01-10 12:54:00'),
('5a560ca4438bc', '801016019211', '10.01.2018 16:52:57', '269100', NULL, 'CRTU!478763******4584', 'Y', '430669', '2018-01-10 12:54:58', '2018-01-10 12:54:58'),
('5a560df01fa50', '801017019235', '10.01.2018 16:58:23', '269100', NULL, 'CRTU!478763******4584', 'Y', '971066', '2018-01-10 13:00:25', '2018-01-10 13:00:25'),
('5a560e14eb961', '801017019238', '10.01.2018 16:58:56', '269100', NULL, 'CRTU!478763******4584', 'Y', '234511', '2018-01-10 13:00:57', '2018-01-10 13:00:57'),
('5a560f5877682', '801017019257', '10.01.2018 17:04:22', '269100', NULL, 'CRTU!478763******4584', 'Y', '644362', '2018-01-10 13:06:26', '2018-01-10 13:06:26'),
('5a560f96dd990', '801017019260', '10.01.2018 17:05:22', '269100', NULL, 'CRTU!478763******4584', 'Y', '349498', '2018-01-10 13:07:21', '2018-01-10 13:07:21'),
('5a56104919237', '801017019275', '10.01.2018 17:08:22', '269100', NULL, 'CRTU!478763******4584', 'Y', '906794', '2018-01-10 13:10:40', '2018-01-10 13:10:40'),
('5a561150906d6', '801017019297', '10.01.2018 17:12:44', '269100', NULL, 'CRTU!478763******4584', 'Y', '711663', '2018-01-10 13:15:13', '2018-01-10 13:15:13'),
('5a56104919237', '801017019275', '10.01.2018 17:08:22', '269100', NULL, 'CRTU!478763******4584', 'Y', '906794', '2018-01-10 13:15:53', '2018-01-10 13:15:53'),
('5a561150906d6', '801017019297', '10.01.2018 17:12:44', '269100', NULL, 'CRTU!478763******4584', 'Y', '711663', '2018-01-10 13:20:23', '2018-01-10 13:20:23'),
('5a56104919237', '801017019275', '10.01.2018 17:08:22', '269100', NULL, 'CRTU!478763******4584', 'Y', '906794', '2018-01-10 13:26:01', '2018-01-10 13:26:01'),
('5a561150906d6', '801017019297', '10.01.2018 17:12:44', '269100', NULL, 'CRTU!478763******4584', 'Y', '711663', '2018-01-10 13:30:30', '2018-01-10 13:30:30'),
('5a56104919237', '801017019275', '10.01.2018 17:08:22', '269100', NULL, 'CRTU!478763******4584', 'Y', '906794', '2018-01-10 13:41:20', '2018-01-10 13:41:20'),
('5a561150906d6', '801017019297', '10.01.2018 17:12:44', '269100', NULL, 'CRTU!478763******4584', 'Y', '711663', '2018-01-10 13:45:35', '2018-01-10 13:45:35'),
('5a56104919237', '801017019275', '10.01.2018 17:08:22', '269100', NULL, 'CRTU!478763******4584', 'Y', '906794', '2018-01-10 14:01:27', '2018-01-10 14:01:27'),
('5a561150906d6', '801017019297', '10.01.2018 17:12:44', '269100', NULL, 'CRTU!478763******4584', 'Y', '711663', '2018-01-10 14:05:45', '2018-01-10 14:05:45'),
('yGx6SBGook6yDBdCCO9rVg==', '801018019540', '10.01.2018 18:09:47', '269100', NULL, 'CRTU!478763******4584', 'Y', '567766', '2018-01-10 14:12:00', '2018-01-10 14:12:00'),
('eUd4NlNCR29vazZ5REJkQ0NPOXJWZz09', '801018019554', '10.01.2018 18:12:18', '269100', NULL, 'CRTU!478763******4584', 'Y', '503590', '2018-01-10 14:14:24', '2018-01-10 14:14:24'),
('5a56104919237', '801017019275', '10.01.2018 17:08:22', '269100', NULL, 'CRTU!478763******4584', 'Y', '906794', '2018-01-10 14:26:33', '2018-01-10 14:26:33');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Test 1', 'Test 1', 'Test 1', '2017-06-05 05:55:58', '2017-06-05 05:55:58'),
(2, 'Test 2', 'Test 2', 'Test 2', '2017-06-05 05:56:04', '2017-06-05 05:56:04'),
(3, 'Test 3', 'Test 3', 'Test 3', '2017-06-05 05:56:09', '2017-06-05 05:56:09'),
(4, 'Test 4', 'Test 4', 'Test 4', '2017-06-05 05:56:14', '2017-06-05 05:56:14'),
(5, 'Test 5', 'Test 5', 'Test 5', '2017-06-05 05:56:19', '2017-06-05 05:56:19');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 2),
(2, 3),
(3, 3),
(4, 3),
(5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `supplier_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `barcode` varchar(60) CHARACTER SET utf8 DEFAULT '',
  `name` text CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `minamount` decimal(10,2) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT 0.00,
  `condition_id` int(11) DEFAULT NULL,
  `state_id` int(2) NOT NULL DEFAULT 1,
  `self_price` decimal(15,2) DEFAULT 0.00,
  `last_price` decimal(10,2) DEFAULT NULL,
  `unit_name` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `supplier_code`, `barcode`, `name`, `description`, `category_id`, `minamount`, `price`, `condition_id`, `state_id`, `self_price`, `last_price`, `unit_name`, `unit_id`, `updated_at`, `created_at`) VALUES
(1, NULL, NULL, 'ავეჯის ანჯამა - 3D / რბილი დახურვით H=0 / 01 - 00500', NULL, 1, NULL, '2.20', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:12:47', '2020-03-05 13:15:47'),
(2, NULL, NULL, 'ავეჯის ანჯამა - 3D / რბილი დახურვით H=17 / 02 - 00500', 'ავეჯის ანჯამა - 3D / რბილი დახურვით H=17 / 02 - 00500', 2, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:12:46', '2020-03-05 13:15:47'),
(3, NULL, NULL, 'ავეჯის ანჯამა - 3D / რბილი დახურვით H=8 / 03 - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:12:45', '2020-03-05 13:15:47'),
(4, NULL, NULL, 'ავეჯის ანჯამა - ბრტყელი მინიატურული ანჯამა / 04 - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:12:44', '2020-03-05 13:15:47'),
(5, NULL, NULL, 'ავეჯის ანჯამა - დაძველების ეფექტით - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:12:41', '2020-03-05 13:15:47'),
(6, NULL, NULL, 'ავეჯის ანჯამა - ზამბარის გარეშე H=0 / 05 - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:12:40', '2020-03-05 13:15:47'),
(7, NULL, NULL, 'ავეჯის ანჯამა - ზამბარის გარეშე H=17 / 07 - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:12:39', '2020-03-05 13:15:47'),
(8, NULL, NULL, 'ავეჯის ანჯამა - ზამბარის გარეშე H=8 / 06 - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:12:38', '2020-03-05 13:15:47'),
(9, NULL, NULL, 'ავეჯის ანჯამა - მაგიდისთვის (ოქროსფერი) / 2801008 / 08 - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:12:37', '2020-03-05 13:15:47'),
(10, NULL, NULL, 'ავეჯის ანჯამა - მინიატურული ანჯამა / 2802006 / 09 - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:12:36', '2020-03-05 13:15:47'),
(11, NULL, NULL, 'ავეჯის ანჯამა - ორმაგი კარის ანჯამა - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:12:35', '2020-03-05 13:15:47'),
(12, NULL, NULL, 'ავეჯის ანჯამა - რბილი დახურვით 165* / 10 - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:12:34', '2020-03-05 13:15:47'),
(13, NULL, NULL, 'ავეჯის ანჯამა - რბილი დახურვით 45* / 11 - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:12:33', '2020-03-05 13:15:47'),
(14, NULL, NULL, 'ავეჯის ანჯამა - რბილი დახურვით 45* / SAMET - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:12:32', '2020-03-05 13:15:47'),
(15, NULL, NULL, 'ავეჯის ანჯამა - რბილი დახურვით 90* / 12 - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:12:31', '2020-03-05 13:15:47'),
(16, NULL, NULL, 'ავეჯის ანჯამა - რბილი დახურვით 90* / SAMET - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:12:30', '2020-03-05 13:15:47'),
(17, NULL, NULL, 'ავეჯის ანჯამა - რბილი დახურვით H=0 / 13 - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:12:29', '2020-03-05 13:15:47'),
(18, NULL, NULL, 'ავეჯის ანჯამა - რბილი დახურვით H=0 / SAMET / 39 - 00500', NULL, 1, NULL, '3.80', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:12:28', '2020-03-05 13:15:47'),
(19, NULL, NULL, 'ავეჯის ანჯამა - რბილი დახურვით H=17 / 15 - 00500', NULL, 2, NULL, '1.10', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:53', '2020-03-05 13:15:47'),
(20, NULL, NULL, 'ავეჯის ანჯამა - რბილი დახურვით H=17 / SAMET / 41 - 00500', NULL, 2, NULL, '2.80', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:52', '2020-03-05 13:15:47'),
(21, NULL, NULL, 'ავეჯის ანჯამა - რბილი დახურვით H=8 / 14 - 00500', NULL, 2, NULL, '1.10', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:50', '2020-03-05 13:15:47'),
(22, NULL, NULL, 'ავეჯის ანჯამა - რბილი დახურვით H=8 / SAMET / 40 - 00500', NULL, 2, NULL, '2.70', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:49', '2020-03-05 13:15:47'),
(23, NULL, NULL, 'ავეჯის ანჯამა - რეგულირების გარეშე / 16 - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:48', '2020-03-05 13:15:47'),
(24, NULL, NULL, 'ავეჯის ანჯამა - რევერსული თვითგაღებით H=0 / 17 - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:47', '2020-03-05 13:15:47'),
(25, NULL, NULL, 'ავეჯის ანჯამა - რევერსული თვითგაღებით H=17 / 19 - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:47', '2020-03-05 13:15:47'),
(26, NULL, NULL, 'ავეჯის ანჯამა - რევერსული თვითგაღებით H=8 / 18 - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:46', '2020-03-05 13:15:47'),
(27, NULL, NULL, 'ავეჯის ანჯამა - როიალის ანჯამა / 20 - 00500', NULL, 2, NULL, '2.80', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:45', '2020-03-05 13:15:47'),
(28, NULL, NULL, 'ავეჯის ანჯამა - სტანდარტული -30 / SAMET / 37 - 00500', 'ავეჯის ანჯამა - სტანდარტული -30 / SAMET / 37 - 00500', 2, NULL, '4.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:45', '2020-03-05 13:15:47'),
(29, NULL, NULL, 'ავეჯის ანჯამა - სტანდარტული -45 / SAMET / 38 - 00500', NULL, 2, NULL, '4.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:44', '2020-03-05 13:15:47'),
(30, NULL, NULL, 'ავეჯის ანჯამა - სტანდარტული 135* / 21 - 00500', NULL, 2, NULL, '2.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:43', '2020-03-05 13:15:47'),
(31, NULL, NULL, 'ავეჯის ანჯამა - სტანდარტული 165* / 22 - 00500', NULL, 2, NULL, '3.50', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:43', '2020-03-05 13:15:47'),
(32, NULL, NULL, 'ავეჯის ანჯამა - სტანდარტული 45* / 23 - 00500', NULL, 2, NULL, '2.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:42', '2020-03-05 13:15:47'),
(33, NULL, NULL, 'ავეჯის ანჯამა - სტანდარტული 45* / SAMET - 00500', NULL, 2, NULL, '2.50', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:41', '2020-03-05 13:15:47'),
(34, NULL, NULL, 'ავეჯის ანჯამა - სტანდარტული 90* / 24 - 00500', NULL, 2, NULL, '2.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:40', '2020-03-05 13:15:47'),
(35, NULL, NULL, 'ავეჯის ანჯამა - სტანდარტული 90* / SAMET - 00500', NULL, 2, NULL, '3.50', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:39', '2020-03-05 13:15:47'),
(36, NULL, NULL, 'ავეჯის ანჯამა - სტანდარტული H=0 / 25 - 00500', NULL, 2, NULL, '0.50', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:38', '2020-03-05 13:15:47'),
(37, NULL, NULL, 'ავეჯის ანჯამა - სტანდარტული H=0 / SAMET / 32 - 00500', NULL, 2, NULL, '0.80', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:37', '2020-03-05 13:15:47'),
(38, NULL, NULL, 'ავეჯის ანჯამა - სტანდარტული H=17 / 27 - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:37', '2020-03-05 13:15:47'),
(39, NULL, NULL, 'ავეჯის ანჯამა - სტანდარტული H=17 / SAMET / 34 - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:36', '2020-03-05 13:15:47'),
(40, NULL, NULL, 'ავეჯის ანჯამა - სტანდარტული H=8 / 26 - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:35', '2020-03-05 13:15:47'),
(41, NULL, NULL, 'ავეჯის ანჯამა - სტანდარტული H=8 / SAMET / 33 - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:35', '2020-03-05 13:15:47'),
(42, NULL, NULL, 'ავეჯის ანჯამა - შუშის კარების ანჯამის ხუფი - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:34', '2020-03-05 13:15:47'),
(43, NULL, NULL, 'ავეჯის ანჯამა - შუშის კარისთვის H=0 / 28 - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:33', '2020-03-05 13:15:47'),
(44, NULL, NULL, 'ავეჯის ანჯამა - შუშის კარისთვის H=0 / SAMET - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:33', '2020-03-05 13:15:47'),
(45, NULL, NULL, 'ავეჯის ანჯამა - შუშის კარისთვის H=17 / 30 - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:32', '2020-03-05 13:15:47'),
(46, NULL, NULL, 'ავეჯის ანჯამა - შუშის კარისთვის H=8 / 29 - 00500', NULL, 1, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:32', '2020-03-05 13:15:47'),
(47, NULL, NULL, 'ავეჯის ანჯამა - შუშის უბრალო ანჯამა - შავი / 31 - 00500', NULL, 1, NULL, '1.20', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:31', '2020-03-05 13:15:47'),
(48, NULL, NULL, 'ავეჯის ანჯამა - ძირის ქვეშსაგები პლასტმასი / 17.5 მმ / SAMET / 45 - 00500', NULL, 2, NULL, '0.60', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:30', '2020-03-05 13:15:47'),
(49, NULL, NULL, 'ავეჯის ანჯამა - ძირის ქვეშსაგები პლასტმასი / 7.5 მმ / SAMET / 44 - 00500', NULL, 2, NULL, '0.60', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:29', '2020-03-05 13:15:47'),
(50, NULL, NULL, 'ავეჯის გორგოლაჭი - ზოლიანი მუხრუჭით Ø 60 / 1901012A / 01 - 00505', NULL, 3, NULL, '2.80', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:28', '2020-03-05 13:15:47'),
(51, NULL, NULL, 'ავეჯის გორგოლაჭი - ზოლიანი უბრალო Ø 60 / 1901012 / 02 - 00505', NULL, 3, NULL, '2.80', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:28', '2020-03-05 13:15:47'),
(52, NULL, NULL, 'ავეჯის გორგოლაჭი - მრგვალი საფარით Ø 50 / 1901030 / 03 - 00505', NULL, 3, NULL, '1.65', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:27', '2020-03-05 13:15:47'),
(53, NULL, NULL, 'ავეჯის გორგოლაჭი - ნაცრისფერი რეზინით მუხრუჭით Ø 50 / 1901020A / 04 - 00505', NULL, 3, NULL, '4.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:26', '2020-03-05 13:15:47'),
(54, NULL, NULL, 'ავეჯის გორგოლაჭი - ნაცრისფერი რეზინით მუხრუჭით Ø 75 / 1901021 / 06 - 00505', NULL, 3, NULL, '6.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:25', '2020-03-05 13:15:47'),
(55, NULL, NULL, 'ავეჯის გორგოლაჭი - ნაცრისფერი რეზინით უბრალო Ø 50 / 1901020 - 00505', NULL, 3, NULL, '3.50', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:24', '2020-03-05 13:15:47'),
(56, NULL, NULL, 'ავეჯის გორგოლაჭი - ნაცრისფერი რეზინით უბრალო Ø 75 / 1901020A / 05 - 00505', NULL, 3, NULL, '4.50', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:23', '2020-03-05 13:15:47'),
(57, NULL, NULL, 'ავეჯის გორგოლაჭი - ნაცრისფერი რეზინით უბრალო Ø 75 / 1901021 / 07 - 00505', NULL, 3, NULL, '4.50', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:22', '2020-03-05 13:15:47'),
(58, NULL, NULL, 'ავეჯის გორგოლაჭი - პირდაპირი Ø 25 მმ / 1901007 / 08 - 00505', NULL, 3, NULL, '0.30', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:22', '2020-03-05 13:15:47'),
(59, NULL, NULL, 'ავეჯის გორგოლაჭი - პირდაპირი Ø 30 მმ / 1901007 / 09 - 00505', NULL, 3, NULL, '0.35', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 11:53:03', '2020-03-05 13:15:47'),
(60, NULL, NULL, 'ავეჯის გორგოლაჭი - საოფისე სკამის Ø 50 მმ / 1901005 / 10 - 00505', NULL, 3, NULL, '0.90', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 11:52:56', '2020-03-05 13:15:47'),
(61, NULL, NULL, 'ავეჯის გორგოლაჭი - სილიკონის Ø 50 მმ / 1901057 / 11 - 00505', 'ავეჯის გორგოლაჭი - სილიკონის Ø 50 მმ / 1901057 / 11 - 00505', 3, NULL, '3.40', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:18:21', '2020-03-05 13:15:47'),
(62, NULL, NULL, 'ავეჯის გორგოლაჭი - სილიკონის Ø 75 მმ / 1901057 / 12 - 00505', NULL, 3, NULL, '6.50', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:55', '2020-03-05 13:15:47'),
(63, NULL, NULL, 'ავეჯის გორგოლაჭი - სუპერ დიდი მუხრუჭით Ø 100 მმ / 1901049 / 13 - 00505', 'ავეჯის გორგოლაჭი - სუპერ დიდი მუხრუჭით Ø 100 მმ / 1901049 / 13 - 00505', 3, NULL, '17.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:54', '2020-03-05 13:15:47'),
(64, NULL, NULL, 'ავეჯის გორგოლაჭი - სუპერ დიდი უბრალო Ø 100 მმ / 1901049 / 14 - 00505', 'ავეჯის გორგოლაჭი - სუპერ დიდი უბრალო Ø 100 მმ / 1901049 / 14 - 00505', 3, NULL, '14.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:54', '2020-03-05 13:15:47'),
(65, NULL, NULL, 'ავეჯის გორგოლაჭი - უბრალო ( შავი ) Ø 50 მმ / 1901001 / 15 - 00505', 'ავეჯის გორგოლაჭი - უბრალო ( შავი ) Ø 50 მმ / 1901001 / 15 - 00505', 3, NULL, '0.60', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:53', '2020-03-05 13:15:47'),
(66, NULL, NULL, 'ავეჯის გორგოლაჭი - უბრალო მუხრუჭით ( შავი ) Ø 50 მმ / 1901001 / 16 - 00505', 'ავეჯის გორგოლაჭი - უბრალო მუხრუჭით ( შავი ) Ø 50 მმ / 1901001 / 16 - 00505', 3, NULL, '0.65', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:52', '2020-03-05 13:15:47'),
(67, NULL, NULL, 'ავეჯის კარის საკეტი - ერთი ლამინატის უბრალო საკეტი / 0701001 / 01 - 00510', NULL, 3, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:52', '2020-03-05 13:15:47'),
(68, NULL, NULL, 'ავეჯის კარის საკეტი - ერთი შუშის კარის საკეტი / 0702006 / 05 - 00510', NULL, 3, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:51', '2020-03-05 13:15:47'),
(69, NULL, NULL, 'ავეჯის კარის საკეტი - მოძრავი კარის საკეტი / 02 - 00510', NULL, 3, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:51', '2020-03-05 13:15:47'),
(70, NULL, NULL, 'ავეჯის კარის საკეტი - ორი ლამინატის საკეტი / 0702002 / 07 - 00510', NULL, 3, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:49', '2020-03-05 13:15:47'),
(71, NULL, NULL, 'ავეჯის კარის საკეტი - ორი შუშის კარის საკეტი / 0702005 / 06 - 00510', NULL, 3, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:48', '2020-03-05 13:15:47'),
(72, NULL, NULL, 'ავეჯის კარის საკეტი - საკეტი 09 / 0702015 / 03 - 00510', NULL, 4, NULL, '2.40', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:47', '2020-03-05 13:15:47'),
(73, NULL, NULL, 'ავეჯის კარის საკეტი - სამი უჯრის ცენტრალური საკეტი / 0702008 / 04 - 00510', NULL, 4, NULL, '4.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:47', '2020-03-05 13:15:47'),
(74, NULL, NULL, 'ავეჯის ფეხი - ალუმინის ინოქსირებული 40 x 40 x 100 მმ / 01 - 00515', NULL, 5, NULL, '2.80', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:46', '2020-03-05 13:15:47'),
(75, NULL, NULL, 'ავეჯის ფეხი - ალუმინის ინოქსირებული 40 x 40 x 50 მმ  / 01 - 00515', NULL, 5, NULL, '2.40', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:46', '2020-03-05 13:15:47'),
(76, NULL, NULL, 'ავეჯის ფეხი - ალუმინის ინოქსირებული 40 x 40 x 80 მმ  / 01 - 00515', NULL, 5, NULL, '2.60', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:45', '2020-03-05 13:15:47'),
(77, NULL, NULL, 'ავეჯის ფეხი - ბარის დახრილი ფეხი / 24 - 00515', 'ავეჯის ფეხი - ბარის დახრილი ფეხი / 24 - 00515', 5, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:44', '2020-03-05 13:15:47'),
(78, NULL, NULL, 'ავეჯის ფეხი - გადასაბმელი ფეხი 42 x 10 მმ / 02 - 00515', NULL, 5, NULL, '4.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:43', '2020-03-05 13:15:47'),
(79, NULL, NULL, 'ავეჯის ფეხი - გადასაბმელი ფეხი 42 x 12 მმ / 02 - 00515', NULL, 5, NULL, '4.50', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:41', '2020-03-05 13:15:47'),
(80, NULL, NULL, 'ავეჯის ფეხი - გადასაბმელი ფეხი 42 x 15 მმ / 02 - 00515', NULL, 5, NULL, '4.70', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:40', '2020-03-05 13:15:47'),
(81, NULL, NULL, 'ავეჯის ფეხი - გადასაბმელი ფეხი 42 x 20 მმ / 02 - 00515', NULL, 5, NULL, '4.90', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:40', '2020-03-05 13:15:47'),
(82, NULL, NULL, 'ავეჯის ფეხი - გადასაბმელი ფეხი 42 x 25 მმ / 02 - 00515', NULL, 5, NULL, '5.10', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:39', '2020-03-05 13:15:47'),
(83, NULL, NULL, 'ავეჯის ფეხი - გადასაბმელი ფეხი 42 x 30 მმ / 02 - 00515', NULL, 5, NULL, '5.30', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:38', '2020-03-05 13:15:47'),
(84, NULL, NULL, 'ავეჯის ფეხი - გადასაბმელი ფეხი 42 x 35 მმ / 02 - 00515', NULL, 5, NULL, '5.80', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:38', '2020-03-05 13:15:47'),
(85, NULL, NULL, 'ავეჯის ფეხი - გადასაბმელი ფეხი 42 x 40 მმ / 02 - 00515', NULL, 5, NULL, '6.30', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:36', '2020-03-05 13:15:47'),
(86, NULL, NULL, 'ავეჯის ფეხი - დახრილი ელიფსის ფორმის / კაკალი - 00515', 'ავეჯის ფეხი - დახრილი ელიფსის ფორმის / კაკალი - 00515', 5, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:36', '2020-03-05 13:15:47'),
(87, NULL, NULL, 'ავეჯის ფეხი - ელიფსის ფორმის 100 მმ / 25 - 00515', 'ავეჯის ფეხი - ელიფსის ფორმის 100 მმ / 25 - 00515', 5, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:35', '2020-03-05 13:15:47'),
(88, NULL, NULL, 'ავეჯის ფეხი - ელიფსის ფორმის 80 მმ / 25 - 00515', 'ავეჯის ფეხი - ელიფსის ფორმის 80 მმ / 25 - 00515', 5, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:35', '2020-03-05 13:15:47'),
(89, NULL, NULL, 'ავეჯის ფეხი - ინოქსირებული ლითონის 50 x 100 მმ / 03 - 00515', 'ავეჯის ფეხი - ინოქსირებული ლითონის 50 x 100 მმ / 03 - 00515', 5, NULL, '1.90', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:34', '2020-03-05 13:15:47'),
(90, NULL, NULL, 'ავეჯის ფეხი - ინოქსირებული ლითონის 50 x 60 მმ / 03 - 00515', 'ავეჯის ფეხი - ინოქსირებული ლითონის 50 x 60 მმ / 03 - 00515', 5, NULL, '1.30', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:33', '2020-03-05 13:15:47'),
(91, NULL, NULL, 'ავეჯის ფეხი - ინოქსირებული ლითონის 50 x 80 მმ / 03 - 00515', 'ავეჯის ფეხი - ინოქსირებული ლითონის 50 x 80 მმ / 03 - 00515', 5, NULL, '1.60', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:32', '2020-03-05 13:15:47'),
(92, NULL, NULL, 'ავეჯის ფეხი - ინოქსირებული ლითონის 60 x 710 მმ / 10 - 00515', 'ავეჯის ფეხი - ინოქსირებული ლითონის 60 x 710 მმ / 10 - 00515', 5, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:31', '2020-03-05 13:15:47'),
(93, NULL, NULL, 'ავეჯის ფეხი - კარადის (რეგულირებადი) / ლითონის / 23 - 00515', NULL, 5, NULL, '3.20', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:31', '2020-03-05 13:15:47'),
(94, NULL, NULL, 'ავეჯის ფეხი - კასრის ფორმის / თეთრი / 50 მმ - 00515', 'ავეჯის ფეხი - კასრის ფორმის / თეთრი / 50 მმ - 00515', 5, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:30', '2020-03-05 13:15:47'),
(95, NULL, NULL, 'ავეჯის ფეხი - ლამინატისთვის / ნატურალი 100 მმ / 22 - 00515', 'ავეჯის ფეხი - ლამინატისთვის / ნატურალი 100 მმ / 22 - 00515', 5, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:29', '2020-03-05 13:15:47'),
(96, NULL, NULL, 'ავეჯის ფეხი - მაგიდის ფეხი მრგვალი ძირით 60 x 710 მმ / 16 - 00515', NULL, 5, NULL, '60.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:28', '2020-03-05 13:15:47'),
(97, NULL, NULL, 'ავეჯის ფეხი - მაგიდის ფეხი ოთხკუთხედი ძირით 45 x 45 CM + 60 x 60 MM + 30 x 30CM +710 მმ / ქრომირებული - 00515', NULL, 4, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-03-19 10:01:20', '2020-03-05 13:15:47'),
(98, NULL, NULL, 'ავეჯის ფეხი - მაგიდის ფეხი ოთხკუთხედი ძირით 45 x 45 CM + 60 x 60 MM + 30 x 30CM +710 მმ / შავი - 00515', NULL, 4, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-03-19 10:01:40', '2020-03-05 13:15:47'),
(99, NULL, NULL, 'ავეჯის ფეხი - მაგიდის ფეხი ოთხკუთხედი ძირით 60 x 60 x 710 მმ / 15 - 00515', 'ავეჯის ფეხი - მაგიდის ფეხი ოთხკუთხედი ძირით 60 x 60 x 710 მმ / 15 - 00515', 5, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:27', '2020-03-05 13:15:47'),
(100, NULL, NULL, 'ავეჯის ფეხი - მაღალი ელიფსის ფორმის / კაკალი - 00515', 'ავეჯის ფეხი - მაღალი ელიფსის ფორმის / კაკალი - 00515', 5, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:27', '2020-03-05 13:15:47'),
(101, NULL, NULL, 'ავეჯის ფეხი - პლასტმასი / დეკორატიული / 21 - 00515', NULL, 5, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:26', '2020-03-05 13:15:47'),
(102, NULL, NULL, 'ავეჯის ფეხი - პლასტმასის 100 მმ / 06 - 00515', NULL, 5, NULL, '1.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:25', '2020-03-05 13:15:47'),
(103, NULL, NULL, 'ავეჯის ფეხი - პლასტმასის 50 მმ / 06 - 00515', NULL, 5, NULL, '0.90', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:24', '2020-03-05 13:15:47'),
(104, NULL, NULL, 'ავეჯის ფეხი - პლასტმასის 80 მმ / 06 - 00515', NULL, 5, NULL, '0.95', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:24', '2020-03-05 13:15:47'),
(105, NULL, NULL, 'ავეჯის ფეხი - პლასტმასის ელიფსის ფორმის / შავი / 13 - 00515', 'ავეჯის ფეხი - პლასტმასის ელიფსის ფორმის / შავი / 13 - 00515', 5, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:23', '2020-03-05 13:15:47'),
(106, NULL, NULL, 'ავეჯის ფეხი - პლასტმასის ოთხკუთხედი 100 მმ / 20 - 00515', NULL, 5, NULL, '1.50', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:22', '2020-03-05 13:15:47'),
(107, NULL, NULL, 'ავეჯის ფეხი - პლასტმასის პატარა ფეხი რეზბით / 14 - 00515', NULL, 5, NULL, '0.20', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:21', '2020-03-05 13:15:47'),
(108, NULL, NULL, 'ავეჯის ფეხი - პლასტმასის რეგულირებადი / თეთრი 100 x 150 მმ / 09 - 00515', NULL, 5, NULL, '0.40', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:21', '2020-03-05 13:15:47'),
(109, NULL, NULL, 'ავეჯის ფეხი - პლასტმასის რეგულირებადი / შავი 100 x 150 მმ / 09 - 00515', NULL, 5, NULL, '0.40', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:20', '2020-03-05 13:15:47'),
(110, NULL, NULL, 'ავეჯის ფეხი - საწოლის ფეხი 42 x 23 / ქრომირებული / 19 - 00515', NULL, 5, NULL, '4.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:19', '2020-03-05 13:15:47'),
(111, NULL, NULL, 'ავეჯის ფეხი - ქრომირებული ლითონის 42 x 100 მმ / 07 - 00515', NULL, 5, NULL, '3.70', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:18', '2020-03-05 13:15:47'),
(112, NULL, NULL, 'ავეჯის ფეხი - ქრომირებული ლითონის 50 x 100 მმ / 04 - 00515', NULL, 5, NULL, '1.90', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:18', '2020-03-05 13:15:47'),
(113, NULL, NULL, 'ავეჯის ფეხი - ქრომირებული ლითონის 50 x 120 მმ / 04 - 00515', NULL, 5, NULL, '1.80', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:17', '2020-03-05 13:15:47'),
(114, NULL, NULL, 'ავეჯის ფეხი - ქრომირებული ლითონის 50 x 150 მმ / 04 - 00515', NULL, 5, NULL, '2.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:16', '2020-03-05 13:15:47'),
(115, NULL, NULL, 'ავეჯის ფეხი - ქრომირებული ლითონის 50 x 200 მმ / 04 - 00515', NULL, 5, NULL, '2.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:15', '2020-03-05 13:15:47'),
(116, NULL, NULL, 'ავეჯის ფეხი - ქრომირებული ლითონის 50 x 250 მმ / 04 - 00515', NULL, 5, NULL, '2.50', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:14', '2020-03-05 13:15:47'),
(117, NULL, NULL, 'ავეჯის ფეხი - ქრომირებული ლითონის 50 x 60 მმ / 04 - 00515', NULL, 5, NULL, '1.30', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:13', '2020-03-05 13:15:47'),
(118, NULL, NULL, 'ავეჯის ფეხი - ქრომირებული ლითონის 50 x 80 მმ / 04 - 00515', NULL, 5, NULL, '1.80', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:12', '2020-03-05 13:15:47'),
(119, NULL, NULL, 'ავეჯის ფეხი - ქრომირებული ლითონის 51 x 100 მმ / 11 - 00515', NULL, 5, NULL, '3.80', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:11', '2020-03-05 13:15:47'),
(120, NULL, NULL, 'ავეჯის ფეხი - ქრომირებული ლითონის 60 x 100 მმ / 05 - 00515', NULL, 5, NULL, '2.70', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:11', '2020-03-05 13:15:47'),
(121, NULL, NULL, 'ავეჯის ფეხი - ქრომირებული ლითონის 60 x 1000 მმ / 12 - 00515', NULL, 5, NULL, '15.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:10', '2020-03-05 13:15:47'),
(122, NULL, NULL, 'ავეჯის ფეხი - ქრომირებული ლითონის 60 x 1100 მმ  / 12 - 00515', NULL, 5, NULL, '22.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:17:08', '2020-03-05 13:15:47'),
(123, NULL, NULL, 'ავეჯის ფეხი - ქრომირებული ლითონის 60 x 120 მმ / 05 - 00515', NULL, 5, NULL, '3.20', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:09:58', '2020-03-05 13:15:47'),
(124, NULL, NULL, 'ავეჯის ფეხი - ქრომირებული ლითონის 60 x 150 მმ / 05 - 00515', NULL, 5, NULL, '3.80', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:09:57', '2020-03-05 13:15:47'),
(125, NULL, NULL, 'ავეჯის ფეხი - ქრომირებული ლითონის 60 x 200 მმ / 05 - 00515', NULL, 5, NULL, '4.40', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:09:56', '2020-03-05 13:15:47'),
(126, NULL, NULL, 'ავეჯის ფეხი - ქრომირებული ლითონის 60 x 400 მმ / 05 - 00515', NULL, 5, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:09:54', '2020-03-05 13:15:47'),
(127, NULL, NULL, 'ავეჯის ფეხი - ქრომირებული ლითონის 60 x 60 მმ / 05 - 00515', NULL, 5, NULL, '2.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:09:53', '2020-03-05 13:15:47'),
(128, NULL, NULL, 'ავეჯის ფეხი - ქრომირებული ლითონის 60 x 710 მმ / 10 - 00515', NULL, 5, NULL, '7.40', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:09:53', '2020-03-05 13:15:47'),
(129, NULL, NULL, 'ავეჯის ფეხი - ქრომირებული ლითონის 60 x 710 მმ / შუშის სამაგრი ლაზერით / 17 - 00515', NULL, 4, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-03-16 11:05:42', '2020-03-05 13:15:47'),
(130, NULL, NULL, 'ავეჯის ფეხი - ქრომირებული ლითონის 60 x 80 მმ / 05 - 00515', NULL, 5, NULL, '2.30', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:09:52', '2020-03-05 13:15:47'),
(131, NULL, NULL, 'ავეჯის ფეხი - ქრომირებული ლითონის 60 x 800 მმ / შუშის სამაგრი ლაზერით / 17 - 00515', NULL, 4, NULL, '0.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-03-16 11:06:28', '2020-03-05 13:15:47'),
(132, NULL, NULL, 'ავეჯის ფეხი - ქრომირებული ლითონის 60 x 900 მმ / 12 - 00515', NULL, 5, NULL, '13.50', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:09:44', '2020-03-05 13:15:47'),
(133, NULL, NULL, 'ავეჯის ფეხი - ქრომირებული ლითონის 76 x 710 მმ / 18 - 00515', NULL, 5, NULL, '7.40', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:09:43', '2020-03-05 13:15:47'),
(134, NULL, NULL, 'ავეჯის ფეხი - ქრომირებული ლითონის დიდი ძირით 50 x 60 მმ / 08 - 00515', NULL, 5, NULL, '1.30', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:09:42', '2020-03-05 13:15:47'),
(135, NULL, NULL, 'ავეჯის ფეხი - ქრომირებული ლითონის დიდი ძირით 50 x 80 მმ / 08 - 00515', NULL, 5, NULL, '1.50', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:09:41', '2020-03-05 13:15:47'),
(136, NULL, NULL, 'ავეჯის ფეხი - ყვითელი ლითონის 60 x 710 მმ  / 10 - 00515', NULL, 5, NULL, '7.40', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:09:36', '2020-03-05 13:15:47'),
(137, NULL, NULL, 'ავეჯის ფეხი - შავი ლითონის 60 x 710 მმ  / 10 - 00515', NULL, 5, NULL, '11.00', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:09:34', '2020-03-05 13:15:47'),
(138, NULL, NULL, 'ავეჯის ქოში - დომინოს მსგავსი / ბეჟი / 01 - 00520', NULL, 6, NULL, '0.02', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:09:33', '2020-03-05 13:15:47'),
(139, NULL, NULL, 'ავეჯის ქოში - დომინოს მსგავსი / ვენგე / 01 - 00520', NULL, 6, NULL, '0.02', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:09:32', '2020-03-05 13:15:47'),
(140, NULL, NULL, 'ავეჯის ქოში - დომინოს მსგავსი / ვერცხლისფერი / 01 - 00520', NULL, 6, NULL, '0.02', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:09:31', '2020-03-05 13:15:47'),
(141, NULL, NULL, 'ავეჯის ქოში - დომინოს მსგავსი / თეთრი / 01 - 00520', NULL, 6, NULL, '0.02', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:09:30', '2020-03-05 13:15:47'),
(142, NULL, NULL, 'ავეჯის ქოში - დომინოს მსგავსი / შავი / 01 - 00520', NULL, 6, NULL, '0.02', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:09:29', '2020-03-05 13:15:47'),
(143, NULL, NULL, 'ავეჯის ქოში - ლურსმნიანი მრგვალი / ბეჟი / 02 - 00520', NULL, 6, NULL, '0.02', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:09:28', '2020-03-05 13:15:47'),
(144, NULL, NULL, 'ავეჯის ქოში - ლურსმნიანი მრგვალი / ვენგე / 02 - 00520', NULL, 6, NULL, '0.02', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:09:27', '2020-03-05 13:15:47'),
(145, NULL, NULL, 'ავეჯის ქოში - ლურსმნიანი მრგვალი / ვერცხლისფერი / 02 - 00520', NULL, 6, NULL, '0.02', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:09:26', '2020-03-05 13:15:47'),
(146, NULL, NULL, 'ავეჯის ქოში - ლურსმნიანი მრგვალი / თეთრი / 02 - 00520', NULL, 6, NULL, '0.02', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:09:25', '2020-03-05 13:15:47'),
(147, NULL, NULL, 'ავეჯის ქოში - ლურსმნიანი მრგვალი / შავი / 02 - 00520', NULL, 6, NULL, '0.02', 1, 2, '0.00', '0.00', 'ცალი', 1, '2020-05-11 12:09:24', '2020-03-05 13:15:47'),
(148, NULL, NULL, 'უჯრის მიმმართველი - ტელესკოპური 42 x 250 მმ / 07', NULL, 7, '0.00', '2.85', 1, 2, NULL, NULL, 'ცალი', 1, '2020-03-05 08:39:26', '2020-03-05 07:53:31'),
(149, NULL, NULL, 'უჯრის მიმმართველი	250 მმ / თეთრი / 01', NULL, 7, NULL, '1.45', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:55', '2020-03-05 08:06:17'),
(150, NULL, NULL, 'უჯრის მიმმართველი	300 მმ / თეთრი / 01', NULL, 7, NULL, '1.60', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:54', '2020-03-05 08:07:10'),
(151, NULL, NULL, 'უჯრის მიმმართველი	350 მმ / თეთრი / 01', NULL, 7, NULL, '1.90', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:53', '2020-03-05 08:08:09'),
(152, NULL, NULL, 'უჯრის მიმმართველი	400 მმ / თეთრი / 01', NULL, 7, NULL, '2.10', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:52', '2020-03-05 08:22:56'),
(153, NULL, NULL, 'უჯრის მიმმართველი	450 მმ / თეთრი / 01', NULL, 7, NULL, '2.25', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:50', '2020-03-05 08:23:38'),
(154, NULL, NULL, 'უჯრის მიმმართველი	500 მმ / თეთრი / 01', NULL, 7, NULL, '2.40', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:49', '2020-03-05 08:24:19'),
(155, NULL, NULL, 'უჯრის მიმმართველი	ტელესკოპური 42 x 250 მმ / 07', NULL, 7, NULL, '2.85', 1, 2, NULL, NULL, 'ცალი', 1, '2020-03-05 08:42:53', '2020-03-05 08:38:22'),
(156, NULL, NULL, 'უჯრის მიმმართველი	ტელესკოპური 42 x 250 მმ / 07', NULL, 7, NULL, '2.85', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:49', '2020-03-05 08:40:20'),
(157, NULL, NULL, 'უჯრის მიმმართველი	ტელესკოპური 42 x 300 მმ / 07', NULL, 7, NULL, '3.30', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:44', '2020-03-05 08:41:05'),
(158, NULL, NULL, 'უჯრის მიმმართველი	ტელესკოპური 42 x 350 მმ / 07', NULL, 7, NULL, '3.80', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:31', '2020-03-05 08:41:44'),
(159, NULL, NULL, 'უჯრის მიმმართველი	ტელესკოპური 42 x 400 მმ / 07', NULL, 7, NULL, '4.10', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:29', '2020-03-05 08:43:29'),
(160, NULL, NULL, 'უჯრის მიმმართველი	ტელესკოპური 42 x 450 მმ / 07', NULL, 7, NULL, '4.65', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:28', '2020-03-05 08:44:07'),
(161, NULL, NULL, 'უჯრის მიმმართველი	ტელესკოპური 42 x 500 მმ / 07', NULL, 7, NULL, '5.30', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:27', '2020-03-05 08:44:51'),
(162, NULL, NULL, 'უჯრის მიმმართველი	ტელესკოპური 42 x 550 მმ / 07', NULL, 7, NULL, '6.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:25', '2020-03-05 08:45:26'),
(163, NULL, NULL, 'უჯრის მიმმართველი	ტელესკოპური 42 x 600 მმ / 07', NULL, 7, NULL, '7.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:25', '2020-03-05 08:46:09'),
(164, NULL, NULL, 'უჯრის მიმმართველი	ტელესკოპური 42 x 700 მმ / 07', NULL, 7, NULL, '10.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:23', '2020-03-05 08:46:48'),
(165, NULL, NULL, 'უჯრის მიმმართველი	ტელესკოპური რბილი დახურვით 45 x 250 მმ / 08', NULL, 7, NULL, '9.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:23', '2020-03-05 09:05:19'),
(166, NULL, NULL, 'უჯრის მიმმართველი	ტელესკოპური რბილი დახურვით 45 x 300 მმ / 08', NULL, 7, NULL, '9.90', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:22', '2020-03-05 09:06:15'),
(167, NULL, NULL, 'უჯრის მიმმართველი	ტელესკოპური რბილი დახურვით 45 x 350 მმ / 08', NULL, 7, NULL, '10.20', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:21', '2020-03-05 09:07:27'),
(168, NULL, NULL, 'უჯრის მიმმართველი	ტელესკოპური რბილი დახურვით 45 x 400 მმ / 08', NULL, 7, NULL, '11.20', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:20', '2020-03-05 09:08:15'),
(169, NULL, NULL, 'უჯრის მიმმართველი	ტელესკოპური რბილი დახურვით 45 x 450 მმ / 08', NULL, 7, NULL, '12.70', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:19', '2020-03-05 09:08:55'),
(170, NULL, NULL, 'უჯრის მიმმართველი	ტელესკოპური რბილი დახურვით 45 x 500 მმ / 08', NULL, 7, NULL, '13.20', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:19', '2020-03-05 09:09:28'),
(171, NULL, NULL, 'უჯრის მიმმართველი	ტელესკოპური რბილი დახურვით 45 x 550 მმ / 08', NULL, 7, NULL, '15.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:17', '2020-03-05 09:10:13'),
(172, NULL, NULL, 'უჯრის მიმმართველი	ტელესკოპური რბილი დახურვით 45 x 600 მმ / 08', NULL, 7, NULL, '17.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:16', '2020-03-05 09:10:47'),
(173, NULL, NULL, 'უჯრის მიმმართველი	ტელესკოპური რბილი დახურვით 45 x 700 მმ / 08', NULL, 7, NULL, '19.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:15', '2020-03-05 09:11:20'),
(174, NULL, NULL, 'უჯრის მიმმართველი	ტელესკოპური რბილი დახურვით 46 x 350 მმ / SAMET', NULL, 7, NULL, '13.70', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:14', '2020-03-05 11:35:46'),
(175, NULL, NULL, 'უჯრის მიმმართველი	ტელესკოპური რბილი დახურვით 46 x 400 მმ / SAMET', NULL, 7, NULL, '14.20', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:13', '2020-03-05 11:37:44'),
(176, NULL, NULL, 'უჯრის მიმმართველი	ტელესკოპური რბილი დახურვით 46 x 450 მმ / SAMET', NULL, 7, NULL, '14.60', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:13', '2020-03-05 11:39:01'),
(177, NULL, NULL, 'უჯრის მიმმართველი	ტელესკოპური რბილი დახურვით 46 x 500 მმ / SAMET', NULL, 7, NULL, '15.70', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:12', '2020-03-05 11:39:44'),
(178, NULL, NULL, '205', 'ბეჟი, შავი, თეთრი, ვენგე, კაკალი, ნაცრისფერი, ვერცხლისფერი', 8, NULL, '0.60', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:00', '2020-03-07 09:09:59'),
(179, NULL, NULL, '1044', '096 მმ / ინოქსირებული\n128 მმ /  ინოქსირებული\n160 მმ / ინოქსირებული', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:54', '2020-03-07 09:29:41'),
(180, NULL, NULL, '1049', '128 მმ / თეთრი\n128 მმ / ღია შავი\n128 მმ / შავი\n128 მმ / წითელი\n160 მმ / თეთრი\n160 მმ / ღია შავი\n160 მმ / შავი\n160 მმ / წითელი\n192 მმ / თეთრი\n192 მმ / ღია შავი\n192 მმ / შავი\n224 მმ / თეთრი\n224 მმ / ღია შავი\n224 მმ / შავი', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:53', '2020-03-07 09:31:21'),
(181, NULL, NULL, '1022', '096 მმ / ქრომირებული ბოლოებით\n128 მმ / ქრომირებული ბოლოებით\n160 მმ / ქრომირებული ბოლოებით\n192 მმ / ქრომირებული ბოლოებით\n224 მმ / ქრომირებული ბოლოებით', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:18:09', '2020-03-07 09:32:59'),
(182, NULL, NULL, '1064', '128 მმ / ინოქსირებული\n128 მმ / ქრომირებული\n160 მმ / ინოქსირებული\n160 მმ / ქრომირებული', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:46', '2020-03-07 09:34:26'),
(183, NULL, NULL, '1063', '128 მმ / ინოქსირებული\n128 მმ / ქრომირებული\n160 მმ / ინოქსირებული\n160 მმ / ქრომირებული\n224 მმ / ინოქსირებული\n224 მმ / ქრომირებული', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:47', '2020-03-07 09:39:01'),
(184, NULL, NULL, '10120', '096 მმ / ქრომირებული', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:02', '2020-03-07 09:40:37'),
(185, NULL, NULL, '10123', '160 მმ / ქრომირებული', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:18:17', '2020-03-07 09:41:46'),
(186, NULL, NULL, '1033', '128 მმ / თეთრი\n128 მმ / ინოქსირებული\n128 მმ / შავი\n160 მმ / ინოქსირებული\n192 მმ / ინოქსირებული', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:55', '2020-03-07 09:43:39'),
(187, NULL, NULL, '1086', '096 მმ / ინოქსირებული\n128 მმ / ინოქსირებული\n160 მმ / ინოქსირებული\n192 მმ / ინოქსირებული\n224 მმ / ინოქსირებული', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:41', '2020-03-07 09:45:09'),
(188, NULL, NULL, '1055', '128 მმ / ინოქსირებული\n160 მმ / ინოქსირებული\n160 მმ / ქრომირებული\n192 მმ / ინოქსირებული\n192 მმ / ქრომირებული\n224 მმ / ინოქსირებული\n224 მმ / ქრომირებული\n256 მმ / ინოქსირებული\n288 მმ / ინოქსირებული\n320 მმ / ინოქსირებული', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:52', '2020-03-07 09:46:17'),
(189, NULL, NULL, '1060', '128 მმ / შავი\n128 მმ / ინოქსირებული\n128 მმ / ქრომირებული\n160 მმ / ინოქსირებული\n160 მმ / ქრომირებული\n160 მმ / შავი\n192 მმ / ინოქსირებული\n192 მმ / ქრომირებული\n192 მმ / შავი\n224 მმ / ინოქსირებული\n224 მმ / ქრომირებული\n224 მმ / შავი', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:51', '2020-03-07 09:48:02'),
(190, NULL, NULL, '1088', '160 მმ / ოქროსფერი\n160 მმ / ქრომირებული\n224 მმ / ოქროსფერი', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:40', '2020-03-07 09:57:28'),
(191, NULL, NULL, '1074', '160 მმ / ინოქსირებული\n192 მმ / ინოქსირებული\n224 მმ / ინოქსირებული', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:42', '2020-03-07 09:58:40'),
(192, NULL, NULL, '1066', '128 მმ / ქრომირებული\n160 მმ / ქრომირებული\n192 მმ / ქრომირებული\n224 მმ / ქრომირებული', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:44', '2020-03-07 09:59:41'),
(193, NULL, NULL, '1091', '128 მმ / ინოქსირებული\n160 მმ / ინოქსირებული\n160 მმ / ქრომირებული\n192 მმ / ინოქსირებული\n192 მმ / ქრომირებული\n224 მმ / ინოქსირებული\n224 მმ / ქრომირებული', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:36', '2020-03-07 10:00:52'),
(194, NULL, NULL, '00670-მრგვალი ჩასასმელი სახელური', 'ქრომირებული / პლასტმასი', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:10:24', '2020-03-07 10:09:28'),
(195, NULL, NULL, '10128', 'ოვალური ჩასასმელი სახელური', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:18:09', '2020-03-07 10:10:32'),
(196, NULL, NULL, '1090', '128 - 160 მმ / ინოქსირებული\n128 - 160 მმ / ქრომირებული', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:38', '2020-03-11 04:26:46'),
(197, NULL, NULL, '1065', '096 მმ / ოქროსფერი\n096 მმ / ქრომირებული\n128 მმ / ოქროსფერი\n128 მმ / ქრომირებული', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:45', '2020-03-11 04:27:40'),
(198, NULL, NULL, '806', '160 მმ / ინოქსირებული\n192 მმ / ინოქსირებული', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:45', '2020-03-11 04:28:44'),
(199, NULL, NULL, '10117', '128 მმ / ინოქსირებული\n160 მმ / ინოქსირებული\n192 მმ / ინოქსირებული', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:05', '2020-03-11 04:29:34'),
(200, NULL, NULL, '1089', '128 - 160 მმ / ინოქსირებული\n128 - 160 მმ / ქრომირებული', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:39', '2020-03-11 04:30:30'),
(201, NULL, NULL, '10118', '128 მმ / ინოქსირებული\n160 მმ / ინოქსირებული\n192 მმ / ინოქსირებული', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:04', '2020-03-11 04:31:32'),
(202, NULL, NULL, '10124', '128 მმ / ქრომირებული\n160 მმ / ქრომირებული\n192 მმ / ქრომირებული', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:18:16', '2020-03-11 04:32:29'),
(203, NULL, NULL, '1005', '096 მმ / ინოქსირებული\n096 მმ /ქრომირებული', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:10:13', '2020-03-11 04:50:45'),
(204, NULL, NULL, '10100', '128 მმ / ოქროსფერი\n160 მმ / ოქროსფერი', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:10:12', '2020-03-11 04:51:48'),
(205, NULL, NULL, '10102', '128 მმ / ოქროსფერი', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:10:10', '2020-03-11 04:53:11'),
(206, NULL, NULL, '10101', '128 მმ / ოქროსფერი', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:10:11', '2020-03-11 04:55:28'),
(207, NULL, NULL, '10103', '128 მმ / ოქროსფერი', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:16', '2020-03-11 04:56:33'),
(208, NULL, NULL, '10115', '096 მმ / ოქროსფერი\n128 მმ / ოქროსფერი', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:07', '2020-03-11 04:57:23'),
(209, NULL, NULL, '10104', '064 მმ / ოქროსფერი\n096 მმ / ოქროსფერი', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:15', '2020-03-11 04:58:20'),
(210, NULL, NULL, '10119', '128 მმ / ოქროსფერი', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:03', '2020-03-11 05:01:40'),
(211, NULL, NULL, '10114', '128 მმ / ოქროსფერი', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:08', '2020-03-11 05:02:39'),
(212, NULL, NULL, '1092', 'ოქროსფერი', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:34', '2020-03-11 05:10:03'),
(213, NULL, NULL, '1093', 'ოქროსფერი', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:33', '2020-03-11 05:10:57'),
(214, NULL, NULL, '1099', 'ოქროსფერი', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:26', '2020-03-11 05:11:55'),
(215, NULL, NULL, '1094', 'ოქროსფერი', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:32', '2020-03-11 05:12:41'),
(216, NULL, NULL, '1096', 'ოქროსფერი', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:30', '2020-03-11 05:13:29'),
(217, NULL, NULL, '1095', 'ოქროსფერი', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:31', '2020-03-11 05:14:21'),
(218, NULL, NULL, '1098', 'ოქროსფერი', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:27', '2020-03-11 05:16:28'),
(219, NULL, NULL, '1097', 'ოქროსფერი', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:28', '2020-03-11 05:17:36'),
(220, NULL, NULL, '10116', 'მრგვალი / ოქროსფერი', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:06', '2020-03-11 05:18:48'),
(221, NULL, NULL, '10125', 'ოქროსფერი', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:18:15', '2020-03-11 05:19:48'),
(222, NULL, NULL, '10105', 'ქრომირებული', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:14', '2020-03-11 05:20:35'),
(223, NULL, NULL, '10106', 'ქრომირებული', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:13', '2020-03-11 05:21:30'),
(224, NULL, NULL, '10107', 'ქრომირებული', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:12', '2020-03-11 05:23:18'),
(225, NULL, NULL, '10108', 'ქრომირებული', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:11', '2020-03-11 05:24:08'),
(226, NULL, NULL, '10109', 'ოქროსფერი', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:10', '2020-03-11 05:25:14'),
(227, NULL, NULL, '10126', '128 მმ / ოქროსფერი\n160 მმ / ოქროსფერი\n160 მმ / ქრომირებული\n160 მმ / შავი', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:18:11', '2020-03-11 05:26:55'),
(228, NULL, NULL, '10121', 'დაძველების ეფექტით / დიდი\nდაძველების ეფექტით / პატარა', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:01', '2020-03-11 05:28:08'),
(229, NULL, NULL, '1001', 'ქრომირებული / დიდი\nქრომირებული / პატარა', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:10:14', '2020-03-11 05:29:34'),
(230, NULL, NULL, '10122', 'დაძველების ეფექტთ / დიდი\nდაძველების ეფექტთ / პატარა\nშავი / დიდი\nშავი / პატარა', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:00', '2020-03-11 05:30:28'),
(231, NULL, NULL, '1062', 'ქრომირებული', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:48', '2020-03-11 05:52:08'),
(232, NULL, NULL, '1058', 'ქრომირებული / დიდი\nქრომირებული / პატარა', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:52', '2020-03-11 05:53:04'),
(233, NULL, NULL, '1040', 'ვერცხლისფერი\nთეთრი\nშავი', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:54', '2020-03-11 05:53:52'),
(234, NULL, NULL, '1061', 'პლასტმასი\nქრომირებული', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:49', '2020-03-11 05:54:59'),
(235, NULL, NULL, '10113', 'ხის ფაქტურით / პატარა', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:09', '2020-03-11 05:56:03'),
(236, NULL, NULL, '1039', 'ვერცხლისფერი / პატარა\nქრომირებული / დიდი\nქრომირებული / პატარა', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:55', '2020-03-11 05:56:55'),
(237, NULL, NULL, '1031', 'ოქროსფერი / დიდი\nოქროსფერი / პატარა\nქრომირებული / პატარა', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:56', '2020-03-11 05:57:42'),
(238, NULL, NULL, '1031', 'ოქროსფერი / დიდი\nოქროსფერი / პატარა\nქრომირებული / პატარა', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:57', '2020-03-11 05:57:47'),
(239, NULL, NULL, '1057', 'ქრომირებული / დიდი\nდაძველების ეფექტით / პატარა\nინოქსირებული / პატარა\nქრომირებული / პატარა', 8, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:53', '2020-03-11 05:59:30'),
(240, NULL, NULL, 'EKS 10-34', 'Ø 16 მილის კედელზე სამაგრი რეგულირებადი კუთხე (EKS 10-34) / ინოქსირებული', 9, NULL, '11.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:38', '2020-03-11 06:06:53'),
(241, NULL, NULL, 'EKS 15-01', 'Ø 16 მილის 90* უძრავი კუთხე (EKS 15-01) / ინოქსირებული', 9, NULL, '1.70', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:14', '2020-03-11 06:08:21'),
(242, NULL, NULL, 'EKS 10-27', 'Ø 16 მილის ფლიანეცი შიდა საშურუპით (EKS 10-27) / ინოქსირებული', 9, NULL, '2.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:39', '2020-03-11 06:09:32'),
(243, NULL, NULL, 'EKS 15-02', 'Ø 16 მილის 45* უძრავი კუთხე (EKS 15-02) / ინოქსირებული', 9, NULL, '1.70', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:13', '2020-03-11 06:12:24'),
(244, NULL, NULL, 'EKS 10-28', 'Ø 16 მილის ფლიანეცი გარე საშურუპით (EKS 10-28) / ინოქსირებული', 9, NULL, '3.10', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:38', '2020-03-11 06:13:23'),
(245, NULL, NULL, 'EKS 15-11', 'Ø 16 მილის 90* რეგულირებადი კუთხე (EKS 15-11) / ინოქსირებული', 9, NULL, '6.60', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:11', '2020-03-11 06:14:10'),
(246, NULL, NULL, 'EKS 16-01', 'Ø 16 მილის გადასაბმელი (EKS 16-01) / ინოქსირებული', 9, NULL, '0.65', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:09', '2020-03-11 06:49:59'),
(247, NULL, NULL, 'EKS 18-02', 'Ø 16 მილის ორმაგი ოვალის ადაპტორი (EKS 18-02) / ინოქსირებული', 9, NULL, '3.40', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:02', '2020-03-11 06:50:58'),
(248, NULL, NULL, 'EKS 17-01', 'Ø 16 მილის საცობი (EKS 17-01) / ინოქსირებული', 9, NULL, '0.85', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:07', '2020-03-11 06:52:25'),
(249, NULL, NULL, 'EKS 18-05', 'Ø 16 მილის ოვალი ადაპტორი (EKS 18-05) / ინოქსირებული', 9, NULL, '2.25', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:01', '2020-03-11 06:53:14'),
(250, NULL, NULL, 'EKS 18-01', 'Ø 16 მილის ჩასასმელი ადაპტორი (EKS 18-01) / ინოქსირებული', 9, NULL, '1.80', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:03', '2020-03-11 06:56:16'),
(251, NULL, NULL, 'EKS 18-08', 'Ø 16 მილის ორმაგი X ადაპტორი (EKS 18-08) / ნატურალი', 9, NULL, '2.95', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:00', '2020-03-11 06:57:40'),
(252, NULL, NULL, 'EKS 17-08', 'Ø 50 მილის საცობი (EKS 17-08) / ინოქსირებული', 9, NULL, '2.75', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:05', '2020-03-11 07:19:23'),
(253, NULL, NULL, 'GS 14-01', 'Ø 50 ზოლიანი მილის საცობი (GS 14-01) / ინოქსირებული', 9, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:49', '2020-03-11 07:20:27'),
(254, NULL, NULL, 'EKS 10-15', 'Ø 30 მილის ფლიანეცი გარე საშურუპით (EKS 10-15) / ნატურალი', 9, NULL, '4.10', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:41', '2020-03-11 07:22:03'),
(255, NULL, NULL, 'EKS 14-03', '40 x 50 უძრავი გადამყვანი (EKS 14-03) / ინოქსირებული', 1, NULL, '3.20', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:35', '2020-03-11 07:22:53'),
(256, NULL, NULL, 'EKS 17-04', 'Ø 30 მილის საცობი (EKS 17-04) / ნატურალი', 9, NULL, '2.15', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:07', '2020-03-11 07:23:51'),
(257, NULL, NULL, 'EKS 13-08', '40 x 50 მოძრავი გადამყვანი (EKS 13-08) / ინოქსირებული', 9, NULL, '3.60', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:36', '2020-03-11 07:24:46'),
(258, NULL, NULL, 'EKS 10-05', 'ცალღარიანი მილის ძირის ფლიანეცი (EKS 10-05) / ინოქსირებული', 9, NULL, '1.70', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:44', '2020-03-11 07:26:44'),
(259, NULL, NULL, 'EKS 10-13', 'Ø 40 მილის ფლიანეცი გარე საშურუპით (EKS 10-13) / ინოქსირებული', 9, NULL, '5.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:41', '2020-03-11 07:27:28'),
(260, NULL, NULL, 'EKS 10-07', 'ცალღარიანი მილის ძირის ფლიანეცი 45* (EKS 10-07) / ინოქსირებული', 9, NULL, '3.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:43', '2020-03-11 07:28:15'),
(261, NULL, NULL, 'EKS 17-06', 'Ø 40 მილის საცობი (EKS 17-06) / ინოქსირებული', 9, NULL, '2.65', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:06', '2020-03-11 07:29:11'),
(262, NULL, NULL, 'EKS 10-12', 'Ø 40 მილის ფლიანეცი შიდა საშურუპით (EKS 10-12) / ინოქსირებული', 9, NULL, '1.70', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:43', '2020-03-11 07:30:20'),
(263, NULL, NULL, 'EKS 17-07', 'Ø 40 ღარიანი მილის საცობი (EKS 17-07) / ინოქსირებული', 9, NULL, '2.60', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:05', '2020-03-11 07:31:02'),
(264, NULL, NULL, 'EKS 19-06', 'Ø 40 კედლის ანკერი (EKS 19-06) / ინოქსირებული', 9, NULL, '8.80', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:18:57', '2020-03-11 07:33:15'),
(265, NULL, NULL, 'EKS 21-10', 'Ø 40 მილზე შუშის ორწერტილიანი სამაგრი (EKS 21-10) / ინოქსირებული', 9, NULL, '13.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:51', '2020-03-11 07:34:07'),
(266, NULL, NULL, 'EKS 21-07', 'Ø 40 მილზე შუშის დამჭერი (EKS 21-07) / ინოქსირებული', 9, NULL, '4.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:53', '2020-03-11 07:34:50'),
(267, NULL, NULL, 'EKS 21-09', 'Ø 40 მილზე შუშის ერთწერტილიანი სამაგრი (EKS 21-09) / ინოქსირებული', 9, NULL, '9.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:52', '2020-03-11 07:36:10'),
(268, NULL, NULL, 'EKS 19-04', 'Ø 40 მილის კედლის ფლიანეცი (EKS 19-04) / ინოქსირებული', 9, NULL, '4.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:12:22', '2020-03-11 07:37:02'),
(269, NULL, NULL, 'EKS 13-07', '50 x 50 მოძრავი გადამყვანი (EKS 13-07) / ინოქსირებული', 9, NULL, '7.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:37', '2020-03-11 07:37:49'),
(270, NULL, NULL, 'EKS 14-06', '50 x 50 უძრავი გადამყვანი (EKS 14-06) / ინოქსირებული', 9, NULL, '5.80', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:14', '2020-03-11 08:08:16'),
(271, NULL, NULL, 'EKS 19-13-1', 'Ø 50 მილის კედელზე სამაგრი მუხლი (EKS 19-13-1) ინოქსირებული', 9, NULL, '16.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:18:55', '2020-03-11 08:09:17'),
(272, NULL, NULL, 'EKS 19-03-1', 'Ø 50 მილის კედელზე სამაგრი მილი გადამყვანის თავით (EKS 19-03-1) / ინოქსირებული', 9, NULL, '3.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:18:59', '2020-03-11 08:10:09'),
(273, NULL, NULL, '(EKS 10-16', 'Ø 50 მილის ფლიანეცი შიდა საშურუპით (EKS 10-16) / ინოქსირებული', 9, NULL, '2.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:10:27', '2020-03-11 08:11:03');
INSERT INTO `products` (`product_id`, `supplier_code`, `barcode`, `name`, `description`, `category_id`, `minamount`, `price`, `condition_id`, `state_id`, `self_price`, `last_price`, `unit_name`, `unit_id`, `updated_at`, `created_at`) VALUES
(274, NULL, NULL, 'EKS 19-03', 'Ø 50 მილის კედელზე სამაგრი თათი (EKS 19-03) / ინოქსირებული', 9, NULL, '3.20', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:18:59', '2020-03-11 08:12:17'),
(275, NULL, NULL, 'EKS 10-19', 'Ø 50 მილის ფლიანეცი გარე საშურუპით (EKS 10-19) / ინოქსირებული', 9, NULL, '5.70', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:39', '2020-03-11 08:13:10'),
(276, NULL, NULL, 'EKS 15-09', 'Ø 50 მილის 90* უძრავი კუთხე (EKS 15-09) / ინოქსირებული', 9, NULL, '5.90', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:12', '2020-03-11 08:14:03'),
(277, NULL, NULL, 'EKS 15-15', 'Ø 50 მილის 90* რეგულირებადი კუთხე სოკოს ძირით (EKS 15-15) / ინოქსირებული', 9, NULL, '17.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:10', '2020-03-11 08:15:42'),
(278, NULL, NULL, 'EKS 15-10', 'Ø 50 მილის 45* უძრავი კუთხე (EKS 15-10) / ინოქსირებული', 9, NULL, '5.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:12', '2020-03-11 08:16:41'),
(279, NULL, NULL, 'EKS 16-03', 'Ø 50 მილის გადასაბმელი (EKS 16-03) / ინოქსირებული', 9, NULL, '1.65', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:08', '2020-03-11 08:17:26'),
(280, NULL, NULL, 'EKS 15-14', 'Ø 50 მილის 90* რეგულირებადი კუთხე (EKS 15-14) / ინოქსირებული', 9, NULL, '15.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:10', '2020-03-11 09:01:39'),
(281, NULL, NULL, 'EKS 17-08', 'Ø 50 მილის საცობი (EKS 17-08) / ინოქსირებული', 9, NULL, '2.75', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:04', '2020-03-11 09:03:24'),
(282, NULL, NULL, 'EKS 19-05', 'Ø 50 მილის კედლის ფლიანეცი (EKS 19-05) / ინოქსირებული', 9, NULL, '4.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:18:58', '2020-03-11 09:04:33'),
(283, NULL, NULL, 'EKS 10-35', 'Ø 50 მილის კედელზე სამაგრი რეგულირებადი კუთხე (EKS 10-35) / ინოქსირებული', 9, NULL, '13.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:37', '2020-03-11 09:05:23'),
(284, NULL, NULL, 'EKS 19-11', 'Ø 50 მილის შუშაზე სამაგრი გადამყვანის ძირით (EKS 19-11) / ინოქსირებული', 9, NULL, '14.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:18:57', '2020-03-11 09:06:16'),
(285, NULL, NULL, 'EKS 19-15', 'Ø 50 მილის ანჯამა (EKS 19-15) / ინოქსირებული', 9, NULL, '10.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:18:55', '2020-03-11 09:07:05'),
(286, NULL, NULL, 'EKS 19-12', 'მინის კედელზე სამაგრი მილი (EKS 19-12) / ინოქსირებული', 9, NULL, '12.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:18:56', '2020-03-11 09:07:55'),
(287, NULL, NULL, 'EKS 19-16', 'Ø 50 მილის ჩამკეტი (EKS 19-16) / ინოქსირებული', 9, NULL, '15.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:18:54', '2020-03-11 09:08:54'),
(288, NULL, NULL, 'EKS 20-11', 'Ø 40 მილის ხეზე სამაგრი ანკერი (EKS 20-11) / ინოქსირებული', 9, NULL, '18.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:56', '2020-03-11 09:09:54'),
(289, NULL, NULL, 'EKS 20-07', 'ანკერი 22 სმ (EKS 20-07) / მასიური', 9, NULL, '2.60', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:18:53', '2020-03-11 09:10:47'),
(290, NULL, NULL, 'EKS 21-06', 'მინის მართკუთხა პატარა სამაგრი (EKS 21-06)', 9, NULL, '0.45', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:54', '2020-03-11 09:12:08'),
(291, NULL, NULL, 'EKS 22-13', 'გრანიტის წებო (EKS 22-13)', 9, NULL, '11.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:50', '2020-03-11 09:13:49'),
(292, NULL, NULL, 'SQ 12-02', '14 X 14 მილის 45* უძრავი კუთხე (SQ 12-02) / ინოქსირებული', 10, NULL, '3.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:42', '2020-03-11 09:26:49'),
(293, NULL, NULL, 'SQ 12-04-1', '14 X 14 მილის 90*  ვერტიკალურად რეგულირებადი კუთხე (SQ 12-04-1) / ინოქსირებული', 10, NULL, '7.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:41', '2020-03-11 09:28:03'),
(294, NULL, NULL, 'SQ 12-01', '14 X 14 მილის 90* უძრავი კუთხე (SQ 12-01) / ინოქსირებული', 10, NULL, '3.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:44', '2020-03-11 09:29:02'),
(295, NULL, NULL, 'SQ 13-04', '14 X 14 მილის გადასაბმელი (SQ 13-04) / ინოქსირებული', 1, NULL, '1.60', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:35', '2020-03-11 09:30:13'),
(296, NULL, NULL, 'SQ 12-03-1', '14 X 14 მილის 90*  წრიულად რეგულირებადი კუთხე (SQ 12-03-1) / ინოქსირებული', 10, NULL, '5.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:41', '2020-03-11 09:31:55'),
(297, NULL, NULL, 'SQ 14-01', '14 X 14 მილის საცობი (SQ 14-01) / ინოქსირებული', 10, NULL, '1.35', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:31', '2020-03-11 09:47:37'),
(298, NULL, NULL, 'SQ 15-02', '14 X 14 მილის ჩასასმელი ადაპტორი 40 X 40 მილზე (SQ 15-02) / ინოქსირებული', 10, NULL, '2.25', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:28', '2020-03-11 09:48:47'),
(299, NULL, NULL, 'SQ 11-07', '40 X 40 მილის უძრავი სოკო (SQ 11-07) / ინოქსირებული', 10, NULL, '3.30', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:45', '2020-03-11 09:49:30'),
(300, NULL, NULL, 'SQ 17-02', '14 X 14 მილზე შუშის სამაგრი (SQ 17-02) / ინოქსირებული', 10, NULL, '4.10', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:24', '2020-03-11 09:50:14'),
(301, NULL, NULL, 'SQ 10-01', '40 X 40 მილის ძირის ფლიანეცი (SQ 10-01) / ინოქსირებული', 10, NULL, '2.10', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:48', '2020-03-11 09:52:00'),
(302, NULL, NULL, 'SQ 11-04', '40 X 40 მილის მოძრავი სოკო (SQ 11-04) / ინოქსირებული', 10, NULL, '5.30', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:46', '2020-03-11 09:52:49'),
(303, NULL, NULL, 'SQ 10-01-1', '40 X 40 მილის ფლიანეცი გარე საშურუპით (SQ 10-01-1) / ინოქსირებული', 10, NULL, '9.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:47', '2020-03-11 09:53:43'),
(304, NULL, NULL, '(SQ 14-03', '40 X 40 მილის საცობი (SQ 14-03) / ინოქსირებული', 10, NULL, '2.35', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:10:25', '2020-03-11 09:58:21'),
(305, NULL, NULL, 'SQ 17-01', '40 X 40 მილიზე შუშის დამჭერი (SQ 17-01) / ინოქსირებული', 10, NULL, '5.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:25', '2020-03-11 09:59:20'),
(306, NULL, NULL, 'SQ 13-02', '25 X 60 სახელურის გადასაბმელი (SQ 13-02) / ინოქსირებული', 10, NULL, '1.90', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:36', '2020-03-11 10:00:12'),
(307, NULL, NULL, 'SQ 16-01-1', '40 X 40 მილის კედელზე სამაგრი ანკერი (SQ 16-01-1) / ინოქსირებული', 10, NULL, '11.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:27', '2020-03-11 10:01:03'),
(308, NULL, NULL, 'SQ 13-07', '25 X 60 სახელურის ვიწრო გადასაბმელი (SQ 13-07) / ინოქსირებული', 10, NULL, '2.80', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:33', '2020-03-11 10:01:47'),
(309, NULL, NULL, 'SQ 13-07', '25 X 60 სახელურის ვიწრო გადასაბმელი (SQ 13-07) / ინოქსირებული', 10, NULL, '2.80', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:34', '2020-03-11 10:03:23'),
(310, NULL, NULL, 'SQ 12-05', '25 X 60 სახელურის ვერტიკალურად რეგულირებადი კუთხე (SQ 12-05) / ინოქსირებული', 10, NULL, '15.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:40', '2020-03-11 10:04:26'),
(311, NULL, NULL, 'SQ 10-05', '25 X 60 სახელურის კედელზე სამაგრი ფლიანეცი  (SQ 10-05)/ ინოქსირებული', 10, NULL, '4.45', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:47', '2020-03-11 10:06:57'),
(312, NULL, NULL, 'SQ 12-05-1', '25 X 60 სახელურის ვერტიკალურად რეგულირებადი მასიური კუთხე (SQ 12-05-1) / ინოქსირებული', 10, NULL, '17.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:39', '2020-03-11 10:07:51'),
(313, NULL, NULL, 'SQ12-31-2', '25 X 60 სახელურის 90* უძრავი კუთხე სოკოს ძირით (SQ12-31-2) / ინოქსირებული', 10, NULL, '17.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:12:09', '2020-03-11 10:10:11'),
(314, NULL, NULL, 'SQ12-14', '25 X 60 სახელურის ჰორიზონტალურად რეგულირებადი კუთხე (SQ12-14) / ინოქსირებული', 10, NULL, '27.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:22', '2020-03-11 10:11:45'),
(315, NULL, NULL, 'SQ12-15', '25 X 60 სახელურის მასიური მარჯვენა რეგულირებადი კუთხე (SQ12-15) / ინოქსირებული', 10, NULL, '40.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:12:13', '2020-03-11 10:23:14'),
(316, NULL, NULL, 'SQ12-14-3', '25 X 60 სახელურის ორმხრივად რეგულირებადი კუთხე (SQ12-14-3) / ინოქსირებული', 10, NULL, '45.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:22', '2020-03-11 10:24:09'),
(317, NULL, NULL, 'SQ12-16', '25 X 60 სახელურის მასიური მარცხენა რეგულირებადი კუთხე (SQ12-16) / ინოქსირებული', 10, NULL, '40.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:12:12', '2020-03-11 10:25:04'),
(318, NULL, NULL, 'SQ12-17', '25 X 60 სახელურის მასიური მოხვევის რეგულირებადი კუთხე სოკოს ძირით (SQ12-17) / ინოქსირებული', 10, NULL, '15.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:12:10', '2020-03-11 10:25:53'),
(319, NULL, NULL, 'SQ12-06', '25 X 60 სახელურის მასიური მოხვევის რეგულირებადი კუთხე (SQ12-06) / ინოქსირებული', 10, NULL, '18.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:24', '2020-03-11 10:26:47'),
(320, NULL, NULL, 'SQ 12-31', '25 X 60 სახელურის 90* შემაერთებელი (SQ 12-31) / ინოქსირებული', 10, NULL, '12.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:38', '2020-03-11 10:27:39'),
(321, NULL, NULL, 'SQ12-16-1', '25 X 60 სახელურის მასიური რეგულირებადი კუთხე (SQ12-16-1) / ინოქსირებული', 10, NULL, '45.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:12:11', '2020-03-11 10:32:03'),
(322, NULL, NULL, 'SQ 14-14', 'შუშის პროფილის საცობი (SQ 14-14) / ინოქსირებული', 10, NULL, '3.60', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:29', '2020-03-11 10:34:38'),
(323, NULL, NULL, 'SQ 14-02', '20 X 27 მილის საცობი (SQ 14-02) / ინოქსირებული', 10, NULL, '2.15', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:30', '2020-03-11 10:35:42'),
(324, NULL, NULL, 'SQ 16-04', '40 X 40 მილის ხეზე სამაგრი ანკერი (SQ 16-04) / ინოქსირებული', 10, NULL, '20.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:26', '2020-03-11 10:36:43'),
(325, NULL, NULL, 'SQ 12-37', '20 X 27 მილის რეგულირებადი კუთხე (SQ 12-37) / ინოქსირებული', 10, NULL, '15.90', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:15:37', '2020-03-11 10:37:56'),
(326, NULL, NULL, 'TM - C250', 'TM - C250 შუშის ანჯამა / კედელი-შუშა', 11, NULL, '25.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:12:04', '2020-03-11 10:45:01'),
(327, NULL, NULL, 'TM - C251', 'TM - C251 შუშის ანჯამა 180*', 11, NULL, '25.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:12:03', '2020-03-11 10:46:02'),
(328, NULL, NULL, 'TM - C252', 'TM - C252 შუშის ანჯამა 135*', 11, NULL, '30.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:12:02', '2020-03-11 10:46:42'),
(329, NULL, NULL, 'TM - C253 შ', 'TM - C253 შუშის ანჯამა 90*', 11, NULL, '35.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:45', '2020-03-11 10:47:31'),
(330, NULL, NULL, 'TM - C254', 'TM - C254 შუშის ანჯამა / კედელი-შუშა / ორმხრივი გაღებით', 11, NULL, '25.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:44', '2020-03-11 10:48:12'),
(331, NULL, NULL, 'TM - C483', 'TM - C483 კედლის და მინის შემაერთებელი 90*', 11, NULL, '13.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:40', '2020-03-11 10:49:07'),
(332, NULL, NULL, 'TM - C484', 'TM - C484 კედლის და მინის შემაერთებელი 45*', 11, NULL, '13.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:39', '2020-03-11 11:05:14'),
(333, NULL, NULL, 'TM - C476', 'TM - C476 მინის შემაერთებელი 90*', 11, NULL, '12.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:42', '2020-03-11 11:06:06'),
(334, NULL, NULL, 'TM - C475', 'TM - C475 მინის შემაერთებელი 180*', 11, NULL, '15.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:43', '2020-03-11 11:08:07'),
(335, NULL, NULL, 'TM - C477', 'TM - C477 მინის შემაერთებელი 135*', 11, NULL, '15.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:41', '2020-03-11 11:09:02'),
(336, NULL, NULL, 'TM - i1278', 'TM - i1278 მინის კარის ანჯამა', 11, NULL, '16.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:21', '2020-03-11 11:09:59'),
(337, NULL, NULL, 'TM-H1277', 'TM-H1277 მინის კარის ანჯამა', 11, NULL, '7.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:13:48', '2020-03-11 11:10:54'),
(338, NULL, NULL, 'TM-H1283', 'TM-H1283 მინის კარის ანჯამა', 11, NULL, '5.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:13:47', '2020-03-11 11:13:00'),
(339, NULL, NULL, 'TM - C688', 'TM - C688 / შუშის დამჭერი ოვალური თავით  / ინოქსირებული /6 - 8 მმ / ქრომირებული', 11, NULL, '8.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:38', '2020-03-11 11:15:13'),
(340, NULL, NULL, 'TM - C711 Ø25 მილის დამჭერი / ქრომირებული', 'TM - C711', 11, NULL, '12.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:34', '2020-03-11 11:17:11'),
(341, NULL, NULL, 'TM - C714', 'TM - C714 Ø25 მილის სარეგულირო გადამყვანი / ქრომირებული', 11, NULL, '20.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:33', '2020-03-11 11:18:16'),
(342, NULL, NULL, 'TM - C710 Ø25', 'TM - C710 Ø25 მილის ფლიანეცი შიდა საშურუფით / ქრომირებული', 11, NULL, '9.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:35', '2020-03-11 11:19:08'),
(343, NULL, NULL, 'TM - C722', 'TM - C722 Ø25 მილის დამჭერი რეგულირებადი ძირით / ქრომირებული', 11, NULL, '23.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:32', '2020-03-11 11:20:08'),
(344, NULL, NULL, 'TM - J1647', 'TM - J1647 მინის ფლიანეცი Ø 45 მმ', 11, NULL, '2.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:21', '2020-03-11 11:22:38'),
(345, NULL, NULL, 'TM - J1647A', 'TM - J1647A მინის ფლიანეცი Ø 65 მმ', 11, NULL, '5.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:20', '2020-03-11 11:23:14'),
(346, NULL, NULL, 'TM-858', 'TM-858 მინის ქრომირებული სახელური', 11, NULL, '9.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:00', '2020-03-11 11:24:22'),
(347, NULL, NULL, 'TM - E883', 'TM - E883  მინის ქრომირებული სახელური', 11, NULL, '25.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:22', '2020-03-11 11:27:55'),
(348, NULL, NULL, 'TM - E873', 'TM - E873  მინის ქრომირებული სახელური', 11, NULL, '8.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:23', '2020-03-11 11:28:45'),
(349, NULL, NULL, 'TM-893', 'TM-893 მოძრავი კარების საკეტი', 11, NULL, '80.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:13:59', '2020-03-11 11:30:09'),
(350, NULL, NULL, 'TM-894', 'TM-894 მოძრავი კარების საკეტი', 11, NULL, '70.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:13:57', '2020-03-11 11:31:17'),
(351, NULL, NULL, 'TM - E854', 'TM - E854  მინის ქრომირებული სახელური', 11, NULL, '20.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:24', '2020-03-11 11:32:19'),
(352, NULL, NULL, 'TM - E836', 'TM - E836  მინის ქრომირებული სახელური', 11, NULL, '30.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:25', '2020-03-11 11:33:35'),
(353, NULL, NULL, 'TM - E810', 'TM - E810  მინის ქრომირებული სახელური', 11, NULL, '80.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:29', '2020-03-11 11:34:17'),
(354, NULL, NULL, 'TM - E812', 'TM - E812  მინის ქრომირებული სახელური', 11, NULL, '80.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:28', '2020-03-11 11:35:13'),
(355, NULL, NULL, 'TM - E806', 'TM - E806  მინის ქრომირებული სახელური', 11, NULL, '70.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:30', '2020-03-11 11:35:56'),
(356, NULL, NULL, 'TM - E823', 'TM - E823  მინის ქრომირებული სახელური', 11, NULL, '60.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:27', '2020-03-11 11:36:41'),
(357, NULL, NULL, 'TM - E833', 'TM - E833  მინის ქრომირებული სახელური', 11, NULL, '50.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:26', '2020-03-11 12:16:32'),
(358, NULL, NULL, 'TM - E809', 'TM - E809  მინის ქრომირებული სახელური', 11, NULL, '60.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:29', '2020-03-11 12:17:26'),
(359, NULL, NULL, 'TM - K001', 'TM - K001 მინის პლასტმასის კანტი 8 მმ 10მმ', 11, NULL, '23.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:17', '2020-03-11 12:18:41'),
(360, NULL, NULL, 'TM - K002', 'TM - K002 მინის პლასტმასის კანტი 8 მმ 10მმ', 11, NULL, '25.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:15', '2020-03-11 12:19:57'),
(361, NULL, NULL, 'TM - K005', 'TM - K005 მინის პლასტმასის კანტი 8 მმ 10მმ', 11, NULL, '23.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:15', '2020-03-11 12:21:08'),
(362, NULL, NULL, 'TM - K013', 'TM - K013 მინის პლასტმასის კანტი 8 მმ 10მმ', 11, NULL, '7.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:14', '2020-03-11 12:22:31'),
(363, NULL, NULL, 'TM - K015', 'TM - K015 მინის პლასტმასის კანტი 8 მმ', 11, NULL, '7.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:13', '2020-03-12 04:20:55'),
(364, NULL, NULL, 'TM - K015A', 'TM - K015A მინის პლასტმასის კანტი 10 მმ', 11, NULL, '7.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:12', '2020-03-12 04:21:33'),
(365, NULL, NULL, 'TM - K016', 'TM - K016 მინის პლასტმასის კანტი 8 მმ', 11, NULL, '7.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:11', '2020-03-12 04:22:13'),
(366, NULL, NULL, 'TM - K016A', 'TM - K016A მინის პლასტმასის კანტი 10 მმ', 11, NULL, '7.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:10', '2020-03-12 04:22:44'),
(367, NULL, NULL, 'TM - K019', 'TM - K019 მინის პლასტმასის კანტი 8 მმ', 11, NULL, '4.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:08', '2020-03-12 04:23:39'),
(368, NULL, NULL, 'TM - K019A', 'TM - K019A მინის პლასტმასის კანტი 10 მმ', 11, NULL, '4.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:07', '2020-03-12 04:24:25'),
(369, NULL, NULL, 'TM - K029', 'TM - K029 მინის პლასტმასის კანტი 8 მმ', 11, NULL, '7.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:06', '2020-03-12 04:25:13'),
(370, NULL, NULL, 'TM - K031', 'TM - K031 მინის პლასტმასის კანტი 8 მმ', 11, NULL, '7.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:05', '2020-03-12 04:25:57'),
(371, NULL, NULL, 'TM - K031A', 'TM - K031A მინის პლასტმასის კანტი 10 მმ', 11, NULL, '7.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:03', '2020-03-12 04:26:26'),
(372, NULL, NULL, 'TM - K032', 'TM - K032 მინის პლასტმასის კანტი 8 მმ', 11, NULL, '7.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:02', '2020-03-12 04:27:11'),
(373, NULL, NULL, 'TM - K032A', 'TM - K032A მინის პლასტმასის კანტი 10 მმ', 11, NULL, '7.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:01', '2020-03-12 04:27:38'),
(374, NULL, NULL, 'TM - A02 დაკიდებული კარის სისტემა', 'TM - A02 დაკიდებული კარის სისტემა', 11, NULL, '160.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:12:08', '2020-03-12 04:28:33'),
(375, NULL, NULL, 'TM - A12 დაკიდებული კარის სისტემა', 'TM - A12 დაკიდებული კარის სისტემა', 11, NULL, '270.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:12:06', '2020-03-12 04:29:55'),
(376, NULL, NULL, 'TM - A13 დაკიდებული კარის სისტემა', 'TM - A13 დაკიდებული კარის სისტემა', 11, NULL, '230.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:12:05', '2020-03-12 04:30:33'),
(377, NULL, NULL, 'TM - D783', 'TM - D783 შუშის დამჭერი მილი 600 - 800', 11, NULL, '45.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:31', '2020-03-12 04:31:27'),
(378, NULL, NULL, 'TM-A01-3', 'TM-A01-3 მილკვადრატის შემაერთებელი კუთხე', 11, NULL, '18.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:13:53', '2020-03-12 04:32:42'),
(379, NULL, NULL, 'TM-A02-4', 'TM-A02-4 მილკვადრატის შემაერთებელი კუთხე', 11, NULL, '15.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:13:52', '2020-03-12 04:33:51'),
(380, NULL, NULL, 'TM - A05-4', 'TM - A05-4 მილკვადრატის კედელზე სამაგრი დეტალი', 11, NULL, '13.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:12:07', '2020-03-12 04:35:15'),
(381, NULL, NULL, 'TM - C769', 'TM - C769 შუშის დამჭერი დეტალი მილკვადრატისთვის', 11, NULL, '23.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:32', '2020-03-12 04:36:45'),
(382, NULL, NULL, 'TM - J1655', 'TM - J1655 მინის წებოს საშრობი ლაზერი', 11, NULL, '60.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:18', '2020-03-12 04:37:37'),
(383, NULL, NULL, 'TM - J1652', 'TM - J1652 მინის წებო 50 მგ', 11, NULL, '45.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:19', '2020-03-12 04:45:10'),
(384, NULL, NULL, 'მილკვადრატი Ø 10 x 30 / 6000 მმ', 'მილკვადრატი Ø 10 x 30 / 6000 მმ', 11, NULL, '30.00', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 11:52:36', '2020-03-12 04:46:47'),
(385, NULL, NULL, 'მილკვადრატი Ø 15 x 40 / 6000 მმ', 'მილკვადრატი Ø 15 x 40 / 6000 მმ', 11, NULL, '35.00', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 11:52:35', '2020-03-12 04:48:08'),
(386, NULL, NULL, 'მილი Ø 25 / 6000 მმ', 'მილი Ø 25 / 6000 მმ', 11, NULL, '37.00', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 11:52:37', '2020-03-12 04:49:03'),
(387, NULL, NULL, 'TM - J1808', 'TM - J1808 საპირფარეშოს კაბინის კომპლექტაცია', 11, NULL, '60.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:14:17', '2020-03-12 04:50:03'),
(388, NULL, NULL, '3.5 x 16 მმ / 1000', '3.5 x 16 მმ / 1000', 12, NULL, '9.70', 1, 2, NULL, NULL, 'შეკვრა', 14, '2020-05-11 12:10:57', '2020-03-12 04:53:12'),
(389, NULL, NULL, '3.5 x 18 მმ / 1000', '3.5 x 18 მმ / 1000', 12, NULL, '7.50', 1, 2, NULL, NULL, 'შეკვრა', 14, '2020-05-11 12:10:55', '2020-03-12 04:54:06'),
(390, NULL, NULL, '3.5 x 30 მმ / 1000', '3.5 x 30 მმ / 1000', 12, NULL, '10.50', 1, 2, NULL, NULL, 'შეკვრა', 14, '2020-05-11 12:10:54', '2020-03-12 04:55:07'),
(391, NULL, NULL, '3.5 x 50 მმ / 500', '3.5 x 50 მმ / 500', 12, NULL, '7.80', 1, 2, NULL, NULL, 'შეკვრა', 14, '2020-05-11 12:10:53', '2020-03-12 04:55:47'),
(392, NULL, NULL, '4.0 x 18 მმ / 1000', '4.0 x 18 მმ / 1000', 12, NULL, '9.00', 1, 2, NULL, NULL, 'შეკვრა', 14, '2020-05-11 12:10:48', '2020-03-12 04:56:19'),
(393, NULL, NULL, '4.0 x 20 მმ / 1000', '4.0 x 20 მმ / 1000', 12, NULL, '10.00', 1, 2, NULL, NULL, 'შეკვრა', 14, '2020-05-11 12:10:47', '2020-03-12 04:56:54'),
(394, NULL, NULL, '4.0 x 25 მმ / 1000', '4.0 x 25 მმ / 1000', 12, NULL, '11.00', 1, 2, NULL, NULL, 'შეკვრა', 14, '2020-05-11 12:10:45', '2020-03-12 04:59:15'),
(395, NULL, NULL, '4.0 x 30 მმ / 1000', '4.0 x 30 მმ / 1000', 12, NULL, '13.00', 1, 2, NULL, NULL, 'შეკვრა', 14, '2020-05-11 12:10:44', '2020-03-12 04:59:48'),
(396, NULL, NULL, '4.0 x 40 მმ / 500', '4.0 x 40 მმ / 500', 12, NULL, '10.00', 1, 2, NULL, NULL, 'შეკვრა', 14, '2020-05-11 12:10:43', '2020-03-12 05:00:12'),
(397, NULL, NULL, '4.0 x 50 მმ / 500', '4.0 x 50 მმ / 500', 12, NULL, '11.00', 1, 2, NULL, NULL, 'შეკვრა', 14, '2020-05-11 12:10:40', '2020-03-12 05:00:40'),
(398, NULL, NULL, '6.0 x 80 მმ / 100', '6.0 x 80 მმ / 100', 12, NULL, '8.00', 1, 2, NULL, NULL, 'შეკვრა', 14, '2020-05-11 12:10:36', '2020-03-12 05:01:29'),
(399, NULL, NULL, '6.3 x 50 მმ / EURO', '6.3 x 50 მმ / EURO', 12, NULL, '0.10', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:10:36', '2020-03-12 05:02:16'),
(400, NULL, NULL, '4.0 x 45 მმ / 02', 'სამტვრევი შურუპი 4.0 x 45 მმ / 02', 12, NULL, '0.05', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:10:41', '2020-03-12 05:03:53'),
(401, NULL, NULL, '4.0 x 22 მმ / 03', 'სახელურის შურუპი 4.0 x 22 მმ / 03', 12, NULL, '0.05', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:10:46', '2020-03-12 05:04:48'),
(402, NULL, NULL, '4.0 x 45 მმ / 02', 'სამტვრევი შურუპი 4.0 x 45 მმ / 02', 12, NULL, '0.05', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:10:42', '2020-03-12 05:05:25'),
(403, NULL, NULL, '80 კგ / HMS', 'სტანდარტული / 80 კგ / HMS', 13, NULL, '8.00', 1, 2, NULL, NULL, 'შეკვრა', 14, '2020-05-11 12:19:47', '2020-03-12 05:09:37'),
(404, NULL, NULL, '100 კგ / 02', 'სტანდარტული / 100 კგ / 02', 13, NULL, '11.20', 1, 2, NULL, NULL, 'შეკვრა', 14, '2020-05-11 12:10:20', '2020-03-12 05:10:28'),
(405, NULL, NULL, '75 კგ / SAMET / 09', '75 კგ / SAMET / 09', 13, NULL, '13.00', 1, 2, NULL, NULL, 'შეკვრა', 14, '2020-05-11 12:19:48', '2020-03-12 05:11:46'),
(406, NULL, NULL, 'ზედა - ქვედა განსხვავებული / 03', 'ზედა - ქვედა განსხვავებული / 03', 13, NULL, '12.40', 1, 2, NULL, NULL, 'შეკვრა', 14, '2020-05-11 11:52:45', '2020-03-12 05:12:53'),
(407, NULL, NULL, 'დაკიდებული კარისთვის / 05', 'დაკიდებული კარისთვის / 05', 13, NULL, '14.00', 1, 2, NULL, NULL, 'შეკვრა', 14, '2020-05-11 11:52:50', '2020-03-12 05:13:45'),
(408, NULL, NULL, 'გადმოსაკიდი კარისთვის / 04', 'გადმოსაკიდი კარისთვის / 04', 13, NULL, '26.00', 1, 2, NULL, NULL, 'შეკვრა', 14, '2020-05-11 12:02:47', '2020-03-12 05:14:27'),
(409, NULL, NULL, 'უხილავი მექანიზმი / 10', 'უხილავი მექანიზმი / 10', 13, NULL, '120.00', 1, 2, NULL, NULL, 'შეკვრა', 14, '2020-05-11 11:48:56', '2020-03-12 05:15:43'),
(410, NULL, NULL, 'Ø 10 მმ / ნატურალი', 'Ø 10 მმ / ნატურალი', 16, NULL, '1.50', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:13:46', '2020-03-12 06:17:03'),
(411, NULL, NULL, 'Ø 16 მმ / ინოქსირებული / 04', 'Ø 16 მმ / ინოქსირებული / 04', 16, NULL, '2.80', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:13:38', '2020-03-12 06:18:41'),
(412, NULL, NULL, 'Ø 16 მმ / ნატურალი / 04', 'Ø 16 მმ / ნატურალი / 04', 16, NULL, '2.70', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:13:37', '2020-03-12 06:19:16'),
(413, NULL, NULL, 'Ø 25 მმ / ინოქსირებული', 'Ø 25 მმ / ინოქსირებული', 16, NULL, '3.80', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:13:29', '2020-03-12 06:19:58'),
(414, NULL, NULL, 'Ø 30 მმ / ნატურალი', 'Ø 30 მმ / ნატურალი', 16, NULL, '0.00', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:13:26', '2020-03-12 06:20:46'),
(415, NULL, NULL, 'Ø 40 მმ / ინოქსირებული', 'Ø 40 მმ / ინოქსირებული', 16, NULL, '8.00', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:12:54', '2020-03-12 06:21:19'),
(416, NULL, NULL, 'Ø 40 მმ / ნატურალი', 'Ø 40 მმ / ნატურალი', 16, NULL, '8.00', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:12:53', '2020-03-12 06:21:48'),
(417, NULL, NULL, 'Ø 50 მმ / ინოქსირებული / 03', 'Ø 50 მმ / ინოქსირებული / 03', 16, NULL, '9.50', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:12:49', '2020-03-12 06:22:28'),
(418, NULL, NULL, 'Ø 50 მმ / ნატურალი / 03', 'Ø 50 მმ / ნატურალი / 03', 16, NULL, '9.00', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:12:48', '2020-03-12 07:22:58'),
(419, NULL, NULL, 'Ø 40 მმ ე', 'Ø 40 მმ ერთი ზოლით / ინოქსირებული / 01', 16, NULL, '14.80', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:12:51', '2020-03-12 07:28:54'),
(420, NULL, NULL, 'Ø 40 მმ', 'Ø 40 მმ ერთი ზოლით / ნატურალი / 01', 16, NULL, '13.00', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:12:55', '2020-03-12 07:29:39'),
(421, NULL, NULL, 'Ø 40 მმ', 'Ø 40 მმ ორი ზოლით / ინოქსირებული / 02', 16, NULL, '13.50', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:12:56', '2020-03-12 07:30:37'),
(422, NULL, NULL, 'Ø 40 მმ', 'Ø 40 მმ ორი ზოლით / ნატურალი / 02', 16, NULL, '13.00', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:12:58', '2020-03-12 07:31:08'),
(423, NULL, NULL, 'Ø 50 მმ', 'Ø 50 მმ / საგულე ზოლით / ინოქსირებული / 09', 16, NULL, '18.00', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:12:50', '2020-03-12 07:32:04'),
(424, NULL, NULL, '10 x 10 მმ', '10 x 10 მმ / ნატურალი', 15, NULL, '1.10', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:10:23', '2020-03-12 07:33:37'),
(425, NULL, NULL, '10 x 25 მმ', '10 x 25 მმ / ნატურალი', 15, NULL, '2.70', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:10:21', '2020-03-12 07:34:08'),
(426, NULL, NULL, '15 x 15 მმ', '15 x 15 მმ / წაკვეთილი ბოლოებით / ნატურალი', 15, NULL, '1.80', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:11:21', '2020-03-12 07:34:42'),
(427, NULL, NULL, '20 x 20 მმ', '20 x 20 მმ / ინოქსირებული', 15, NULL, '2.40', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:11:13', '2020-03-12 07:35:46'),
(428, NULL, NULL, '20 x 25 მმ', '20 x 25 მმ / ნატურალი', 15, NULL, '2.90', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:11:12', '2020-03-12 07:36:32'),
(429, NULL, NULL, '20 x 40 მმ', '20 x 40 მმ / ნატურალი', 15, NULL, '3.40', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:11:10', '2020-03-12 07:37:21'),
(430, NULL, NULL, '20 x 50 მმ', '20 x 50 მმ / ნატურალი', 15, NULL, '4.00', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:11:09', '2020-03-12 08:31:07'),
(431, NULL, NULL, '20 x 60 მმ', '20 x 60 მმ / ნატურალი', 15, NULL, '4.60', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:11:06', '2020-03-12 08:31:47'),
(432, NULL, NULL, '20 x 80 მმ', '20 x 80 მმ / ნატურალი', 15, NULL, '6.50', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:11:05', '2020-03-12 08:32:19'),
(433, NULL, NULL, '20 x 100 მმ', '20 x 100 მმ / ნატურალი', 15, NULL, '13.50', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:11:17', '2020-03-12 08:33:00'),
(434, NULL, NULL, '20 x 120 მმ', '20 x 120 მმ / ნატურალი', 15, NULL, '10.00', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:11:16', '2020-03-12 08:57:45'),
(435, NULL, NULL, '25 x 25 მმ', '25 x 25 მმ / ნატურალი', 15, NULL, '3.00', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:10:58', '2020-03-12 08:58:36'),
(436, NULL, NULL, '30 x 10 მმ', '30 x 10 მმ / ნატურალი', 15, NULL, '2.60', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:10:52', '2020-03-12 08:59:25'),
(437, NULL, NULL, '30 x 50 მმ ~', '30 x 50 მმ / ნატურალი', 15, NULL, '6.50', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:10:50', '2020-03-12 09:00:12'),
(438, NULL, NULL, '40 x 40 მმ', '40 x 40 მმ / წაკვეთილი ბოლოებით / ნატურალი', 15, NULL, '5.50', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:10:38', '2020-03-12 09:00:50'),
(439, NULL, NULL, '10 x 20 მმ', '10 x 20 მმ / ნატურალი', 17, NULL, '3.50', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:10:22', '2020-03-12 09:03:59'),
(440, NULL, NULL, '20 x 20 მმ', '20 x 20 მმ / ნატურალი', 17, NULL, '3.50', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:11:14', '2020-03-12 11:33:33'),
(441, NULL, NULL, '20 x 40 მმ', '20 x 40 მმ / ნატურალი', 17, NULL, '4.50', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:11:11', '2020-03-12 11:35:21'),
(442, NULL, NULL, '20 x 60 მმ', '20 x 60 მმ / ნატურალი', 17, NULL, '9.50', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:11:07', '2020-03-12 11:36:07'),
(443, NULL, NULL, '20 x 80 მმ / ნატურალი', '20 x 80 მმ / ნატურალი', 17, NULL, '13.00', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:11:04', '2020-03-12 11:36:53'),
(444, NULL, NULL, '20 x 100 მმ', '20 x 100 მმ / ნატურალი', 17, NULL, '12.00', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:11:19', '2020-03-12 11:38:27'),
(445, NULL, NULL, '30 x 30 მმ', '30 x 30 მმ / ნატურალი', 17, NULL, '5.00', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:10:51', '2020-03-12 11:39:17'),
(446, NULL, NULL, '40 x 40 მმ', '40 x 40 მმ / ნატურალი', 17, NULL, '6.50', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:10:39', '2020-03-12 11:40:17'),
(447, NULL, NULL, '75 x 32 მმ', '75 x 32 მმ / ნატურალი', 17, NULL, '12.50', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:19:49', '2020-03-12 11:41:03'),
(448, NULL, NULL, '20 მმ', '20 მმ / ნატურალი', 18, NULL, '2.90', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:11:02', '2020-03-13 04:58:17'),
(449, NULL, NULL, '30 მმ', '30 მმ / ნატურალი', 18, NULL, '3.20', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:10:49', '2020-03-13 04:59:17'),
(450, NULL, NULL, '40 მმ', '40 მმ / ნატურალი', 18, NULL, '3.50', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:10:37', '2020-03-13 04:59:54'),
(451, NULL, NULL, '75 მმ', '75 მმ / ნატურალი', 18, NULL, '7.00', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:19:47', '2020-03-13 05:00:27'),
(452, NULL, NULL, '100 მმ', '100 მმ / ნატურალი', 18, NULL, '11.00', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:10:17', '2020-03-13 05:01:03'),
(453, NULL, NULL, '200 მმ /', '200 მმ / ნატურალი', 18, NULL, '20.00', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:11:01', '2020-03-13 05:01:41'),
(454, NULL, NULL, '60 ნიუტონი / თეთრი / SAMET / 04', '60 ნიუტონი / თეთრი / SAMET / 04', 19, NULL, '6.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:10:33', '2020-03-13 07:03:58'),
(455, NULL, NULL, '80 ნიუტონი / თეთრი / SAMET / 04', '80 ნიუტონი / თეთრი / SAMET / 04', 19, NULL, '6.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:46', '2020-03-13 10:21:40'),
(456, NULL, NULL, '100 ნიუტონი / თეთრი / SAMET / 04', '100 ნიუტონი / თეთრი / SAMET / 04', 19, NULL, '6.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:10:15', '2020-03-13 10:22:10'),
(457, NULL, NULL, '120 ნიუტონი / თეთრი / SAMET / 04', '120 ნიუტონი / თეთრი / SAMET / 04', 19, NULL, '6.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:24', '2020-03-13 10:22:48'),
(458, NULL, NULL, '60 ნიუტონი / 01', '60 ნიუტონი / 01', 19, NULL, '1.80', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:10:35', '2020-03-14 04:28:04'),
(459, NULL, NULL, '80 ნიუტონი / 01', '80 ნიუტონი / 01', 19, NULL, '1.80', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:46', '2020-03-14 04:29:07'),
(460, NULL, NULL, '100 ნიუტონი / 01', '100 ნიუტონი / 01', 19, NULL, '1.80', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:10:16', '2020-03-14 04:29:33'),
(461, NULL, NULL, '120 ნიუტონი / 01', '120 ნიუტონი / 01', 19, NULL, '1.80', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:25', '2020-03-14 04:40:29'),
(462, NULL, NULL, '60 ნიუტონი / თეთრი / SONNIVA / 05', '60 ნიუტონი / თეთრი / SONNIVA / 05', 19, NULL, '2.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:19:50', '2020-03-14 05:17:38'),
(463, NULL, NULL, '80 ნიუტონი / თეთრი / SONNIVA / 05', '80 ნიუტონი / თეთრი / SONNIVA / 05', 19, NULL, '2.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:12:20', '2020-03-14 05:18:09'),
(464, NULL, NULL, '100 ნიუტონი / თეთრი / SONNIVA / 05', '100 ნიუტონი / თეთრი / SONNIVA / 05', 19, NULL, '2.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:10:15', '2020-03-14 05:18:36'),
(465, NULL, NULL, '120 ნიუტონი / თეთრი / SONNIVA / 05', '120 ნიუტონი / თეთრი / SONNIVA / 05', 19, NULL, '2.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:11:23', '2020-03-14 05:19:01'),
(466, NULL, NULL, 'რბილი დახურვით / 60 ნიუტონი / 06', 'რბილი დახურვით / 60 ნიუტონი / 06', 19, NULL, '3.40', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:24', '2020-03-14 05:20:08'),
(467, NULL, NULL, 'რბილი დახურვით / 80 ნიუტონი / 06', 'რბილი დახურვით / 80 ნიუტონი / 06', 19, NULL, '3.40', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:23', '2020-03-14 05:20:41'),
(468, NULL, NULL, 'რბილი დახურვით / 100 ნიუტონი / 06', 'რბილი დახურვით / 100 ნიუტონი / 06', 19, NULL, '3.40', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:27', '2020-03-14 05:21:10'),
(469, NULL, NULL, 'რბილი დახურვით / 120 ნიუტონი / 06', 'რბილი დახურვით / 120 ნიუტონი / 06', 19, NULL, '3.40', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:25', '2020-03-14 05:21:36'),
(470, NULL, NULL, 'ნიკელის მილი,Ø 25 მმ / 04', 'ნიკელის მილი,Ø 25 მმ / 04', 20, NULL, '5.30', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:38', '2020-03-14 06:27:47'),
(471, NULL, NULL, 'ნიკელის მილი Ø 15 x 30 მმ / 01', 'ნიკელის მილი Ø 15 x 30 მმ / 01', 20, NULL, '6.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:42', '2020-03-14 06:29:04'),
(472, NULL, NULL, 'ნიკელის მილი Ø 25 x 25 მმ / 03', 'ნიკელის მილი Ø 25 x 25 მმ / 03', 20, NULL, '16.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:40', '2020-03-14 06:29:47'),
(473, NULL, NULL, 'ნიკელის მილი Ø 16 მმ / 02', 'ნიკელის მილი Ø 16 მმ / 02', 20, NULL, '5.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:41', '2020-03-14 06:30:53'),
(474, NULL, NULL, 'ნიკელის მილი Ø 50 მმ / 05', 'ნიკელის მილი Ø 50 მმ / 05', 20, NULL, '19.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:39', '2020-03-14 06:31:59'),
(475, NULL, NULL, 'Ø 25 მმ უბრალო ფლიანეცი', 'Ø 25 მმ უბრალო ფლიანეცი / 01A01043 / 19', 20, NULL, '0.30', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:13:28', '2020-03-14 06:33:44'),
(476, NULL, NULL, 'Ø 16 - ფეხიანი დახურული საკიდი / 08', 'Ø 16 - ფეხიანი დახურული საკიდი / 08', 20, NULL, '1.20', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:13:39', '2020-03-14 06:34:42'),
(477, NULL, NULL, 'Ø 16 - ფეხიანი ღია საკიდი / 07', 'Ø 16 - ფეხიანი ღია საკიდი / 07', 20, NULL, '1.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:13:38', '2020-03-14 06:35:35'),
(478, NULL, NULL, 'Ø 16 მმ მილის ფლიანეცი მაღალი ყელით / 35', 'Ø 16 მმ მილის ფლიანეცი მაღალი ყელით / 35', 20, NULL, '0.90', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:13:36', '2020-03-14 06:36:42'),
(479, NULL, NULL, 'Ø 16 მმ პერპენდიკულარი გადამყვანი კვადრატული ძირით / 1202027 / 09', 'Ø 16 მმ პერპენდიკულარი გადამყვანი კვადრატული ძირით / 1202027 / 09', 20, NULL, '2.20', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:13:34', '2020-03-14 06:43:54'),
(480, NULL, NULL, 'Ø 16 მმ პერპენდიკულარი', 'Ø 16 მმ პერპენდიკულარი გადამყვანი მრგვალი ძირით / 01A02065 / 09', 20, NULL, '2.40', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:13:35', '2020-03-14 07:33:38'),
(481, NULL, NULL, 'Ø 16 - მილის დეკორატიული საცობი / 1202024 / 34', 'Ø 16 - მილის დეკორატიული საცობი / 1202024 / 34', 20, NULL, '1.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:13:40', '2020-03-14 07:34:41'),
(482, NULL, NULL, 'Ø 15 x 30 მმ', 'Ø 15 x 30 მმ ოვალური მილის ჩამოსადები / 01A02020 / 05', 20, NULL, '0.25', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:13:42', '2020-03-14 07:54:20'),
(483, NULL, NULL, 'Ø 15 x 30 მმ ოვალური მილის ჩამოსადები (მაღალი) / 01A02025 / 06', 'Ø 15 x 30 მმ ოვალური მილის ჩამოსადები (მაღალი) / 01A02025 / 06', 20, NULL, '0.60', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:13:41', '2020-03-14 07:55:26'),
(484, NULL, NULL, 'Ø 15 x 30 - ფეხიანი დახურული საკიდი / 04', 'Ø 15 x 30 - ფეხიანი დახურული საკიდი / 04', 20, NULL, '1.20', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:13:43', '2020-03-14 07:56:58'),
(485, NULL, NULL, 'Ø 15 x 30 - ფეხიანი ღია საკიდი / 03', 'Ø 15 x 30 - ფეხიანი ღია საკიდი / 03', 20, NULL, '1.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:13:43', '2020-03-14 07:58:06'),
(486, NULL, NULL, 'Ø 16 მმ უბრალო ფლიანეცი / 01A01043 / 33', 'Ø 16 მმ უბრალო ფლიანეცი / 01A01043 / 33', 20, NULL, '0.25', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:13:33', '2020-03-14 08:32:49'),
(487, NULL, NULL, 'Ø 25 მმ ფლიანეცი მაღალი ყელით / 01A02060 / 20', 'Ø 25 მმ ფლიანეცი მაღალი ყელით / 01A02060 / 20', 20, NULL, '2.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:13:27', '2020-03-14 08:37:01'),
(488, NULL, NULL, 'Ø 25 - ფეხიანი დახურული საკიდი / 01', 'Ø 25 - ფეხიანი დახურული საკიდი / 01', 20, NULL, '1.40', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:13:32', '2020-03-14 08:41:29'),
(489, NULL, NULL, 'Ø 25 - ფეხიანი ღია საკიდი / 02', 'Ø 25 - ფეხიანი ღია საკიდი / 02', 20, NULL, '1.40', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:13:31', '2020-03-14 08:42:53'),
(490, NULL, NULL, 'Ø 25 მმ  მილის ჩამოსადები ორი საშურუფით / 36', 'Ø 25 მმ  მილის ჩამოსადები ორი საშურუფით / 36', 20, NULL, '0.20', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:13:30', '2020-03-14 08:44:16'),
(491, NULL, NULL, 'ტელესკოპური 43 x 250 მმ / SAMET / 09', 'ტელესკოპური 43 x 250 მმ / SAMET / 09', 7, NULL, '8.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:04', '2020-03-16 04:39:03'),
(492, NULL, NULL, 'ტელესკოპური 43 x 300 მმ / SAMET / 09', 'ტელესკოპური 43 x 300 მმ / SAMET / 09', 7, NULL, '9.10', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:03', '2020-03-16 04:40:05'),
(493, NULL, NULL, 'ტელესკოპური 43 x 350 მმ / SAMET / 09', 'ტელესკოპური 43 x 350 მმ / SAMET / 09', 7, NULL, '9.70', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:01', '2020-03-16 04:40:41'),
(494, NULL, NULL, 'ტელესკოპური 43 x 400 მმ / SAMET / 09', 'ტელესკოპური 43 x 400 მმ / SAMET / 09', 7, NULL, '10.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:00', '2020-03-16 04:41:10'),
(495, NULL, NULL, 'ტელესკოპური 43 x 450 მმ / SAMET / 09', 'ტელესკოპური 43 x 450 მმ / SAMET / 09', 1, NULL, '11.30', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:59', '2020-03-16 04:41:33'),
(496, NULL, NULL, 'ტელესკოპური 43 x 500 მმ / SAMET / 09', 'ტელესკოპური 43 x 500 მმ / SAMET / 09', 7, NULL, '12.40', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:58', '2020-03-16 04:43:50'),
(497, NULL, NULL, 'ტელესკოპური 43 x 700 მმ / SAMET / 09', 'ტელესკოპური 43 x 700 მმ / SAMET / 09', 7, NULL, '20.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:56', '2020-03-16 04:44:25'),
(498, NULL, NULL, 'ტელესკოპური 35 x 300 მმ / 06', 'ტელესკოპური 35 x 300 მმ / 06', 7, NULL, '2.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:11', '2020-03-16 05:48:57'),
(499, NULL, NULL, 'ტელესკოპური 35 x 350 მმ / 06', 'ტელესკოპური 35 x 350 მმ / 06', 1, NULL, '2.90', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:09', '2020-03-16 05:49:23'),
(500, NULL, NULL, 'ტელესკოპური 35 x 400 მმ / 06', 'ტელესკოპური 35 x 400 მმ / 06', 7, NULL, '3.30', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:06', '2020-03-16 05:49:50'),
(501, NULL, NULL, 'ტელესკოპური 35 x 450 მმ / 06', 'ტელესკოპური 35 x 450 მმ / 06', 7, NULL, '3.70', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:05', '2020-03-16 05:50:26'),
(502, NULL, NULL, 'ორკომპონენტიანი 400 x 100 გრ / APEL', 'ორკომპონენტიანი 400 x 100 გრ / APEL', 21, NULL, '7.95', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:32', '2020-03-16 06:14:34'),
(503, NULL, NULL, 'ორკომპონენტიანი 400 x 100 გრ / 01', 'ორკომპონენტიანი 400 x 100 გრ / 01', 21, NULL, '6.80', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:33', '2020-03-16 06:15:11'),
(504, NULL, NULL, 'ხის ქაფი წებო / APEL / 05', 'ხის ქაფი წებო / APEL / 05', 21, NULL, '11.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-08 13:17:42', '2020-03-16 06:16:08'),
(505, NULL, NULL, '100 გრ / 04', '100 გრ / 04', 21, NULL, '4.30', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:10:20', '2020-03-16 06:27:08'),
(506, NULL, NULL, 'სარკის სილიკონი MAGPOW / 300ml', 'სარკის სილიკონი MAGPOW / 300ml', 21, NULL, '7.90', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:20', '2020-03-16 07:11:21'),
(507, NULL, NULL, 'მჟავა სილიკონი 100% MAGPOW', 'მჟავა სილიკონი 100% MAGPOW', 21, NULL, '8.90', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:45', '2020-03-16 07:32:02'),
(508, NULL, NULL, 'ბიზონი / საკონტაქტო წებო 1000 მ/ლ / 03', 'ბიზონი / საკონტაქტო წებო 1000 მ/ლ / 03', 21, NULL, '15.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:02:40', '2020-03-16 07:33:52'),
(509, NULL, NULL, 'სილიკონი / აკვარიუმის / SELSIL / 300 ML', 'სილიკონი / აკვარიუმის / SELSIL / 300 ML', 21, NULL, '10.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:18', '2020-03-16 08:25:51'),
(510, NULL, NULL, 'სილიკონი / სარკის / SELSIL / 310 ML', 'სილიკონი / სარკის / SELSIL / 310 ML', 21, NULL, '11.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:16', '2020-03-16 08:45:53'),
(511, NULL, NULL, 'სილიკონი / უნივერსალი / SELSIL / 280 ML', 'სილიკონი / უნივერსალი / SELSIL / 280 ML', 21, NULL, '7.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:15', '2020-03-16 08:46:28'),
(512, NULL, NULL, 'სილიკონის  წებო  280 მლ - გამჭვირვალე   (AKFIX)', 'სილიკონის  წებო  280 მლ - გამჭვირვალე   (AKFIX)  SA041', 21, NULL, '6.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:13', '2020-03-16 08:47:10'),
(513, NULL, NULL, 'სილიკონის წებო   310ML - სარკის   (AKFIX)', 'სილიკონის წებო   310ML - სარკის   (AKFIX) SA081', 21, NULL, '9.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:11', '2020-03-16 08:48:02'),
(514, NULL, NULL, 'სილიკონის  წებო  310 მლ -აკვარიუმის    (AKFIX)', 'სილიკონის  წებო  310 მლ -აკვარიუმის    (AKFIX) SA080', 21, NULL, '8.70', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:12', '2020-03-16 08:48:34'),
(515, NULL, NULL, 'ლითონის ღერძი', 'ლითონის ღერძით / თეთრი / შავი / ყავისფერი / კრემისფერი', 22, NULL, '0.02', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:52:39', '2020-03-16 10:03:03'),
(516, NULL, NULL, 'თაროს დამჭერი', 'ლურსმნიანი / თეთრი / შავი / გამჭვირვალე / კრემისფერი / ვერცხლისფერი', 22, NULL, '0.01', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:52:44', '2020-03-16 10:05:15'),
(517, NULL, NULL, 'პოლიურეთანის  ქაფი  (თოფზე მომუშავე)  750', 'FA001', 21, NULL, '11.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:29', '2020-03-18 05:50:04'),
(518, NULL, NULL, 'პოლიურეთანის  ქაფი   850 GR  (AKFIX)', 'FA011', 21, NULL, '9.80', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:31', '2020-03-18 05:51:23'),
(519, NULL, NULL, 'პოლიურეთანის  ქაფი ზამთრის  850 GR  (AKFIX)', 'FA011W', 21, NULL, '12.50', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:28', '2020-03-18 05:54:31'),
(520, NULL, NULL, 'საგულე პროფილის მსგავსი', 'საგულე პროფილის მსგავსი / ინოქსირებული, ნატურალი, ოქროსფერი, შავი', 23, NULL, '0.00', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 11:49:22', '2020-03-18 06:27:15'),
(521, NULL, NULL, 'ბათუმის სახელური', 'ბათუმის სახელური / ინოქსირებული, ინოქსირებული ეკო, ნატურალი, შავი, თეთრი, ოქროსფერი', 23, NULL, '0.00', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 12:09:16', '2020-03-18 06:29:56'),
(522, NULL, NULL, 'მსხვილბურთულიანი', 'მსხვილბურთულიანი / ინოქსირებული / 03', 23, NULL, '0.00', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 11:49:43', '2020-03-18 06:32:26'),
(523, NULL, NULL, 'წვრილბურთულიანი', 'წვრილბურთულიანი / ინოქსირებული, ნატურალი', 23, NULL, '0.00', 1, 2, NULL, NULL, 'მეტრი', 8, '2020-05-11 11:47:55', '2020-03-18 06:38:42'),
(524, NULL, NULL, 'კუთხის შემკვრელი პეპელა', 'გამჭირვალე / პლასტმასი / 01', 24, NULL, '0.30', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:52:41', '2020-03-18 07:02:15'),
(525, NULL, NULL, 'გამჭირვალე / პლასტმასი / 01', 'ლითონი / 02', 24, NULL, '0.60', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:02:07', '2020-03-18 07:04:29'),
(526, NULL, NULL, 'სამზარეულოს საკიდი / თეთრი / 14', 'სამზარეულოს საკიდი / თეთრი. ყავისფერი, შავი', 24, NULL, '0.30', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:21', '2020-03-18 07:05:33'),
(527, NULL, NULL, 'ბრტყელტუჩა მეტალის, რეზინის სახელურით 160MM.6\"', 'TOL1-10000 ბრტყელტუჩა მეტალის, რეზინის სახელურით 160MM.6\"', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:02:26', '2020-03-18 10:54:45'),
(528, NULL, NULL, 'ბრტყელტუჩა მკვნეტარა მეტალის რეზინის სახელურით 160MM,6\"', 'TOL4-10003 ბრტყელტუჩა მკვნეტარა მეტალის რეზინის სახელურით 160MM,6\"', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:02:16', '2020-03-18 10:57:46'),
(529, NULL, NULL, 'ბრტყელტუჩა გრძელცხვირა მეტალის რეზინის სახელურით 160MM,6\"', 'TOL5-10006 ბრტყელტუჩა გრძელცხვირა მეტალის რეზინის სახელურით 160MM,6\"', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:02:37', '2020-03-18 11:00:58'),
(530, NULL, NULL, 'ბრტყელტუჩა მეტალის, რეზინის სახელურით 180MM,7\"', 'TOL2-10001 ბრტყელტუჩა მეტალის, რეზინის სახელურით 180MM,7\"', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:02:32', '2020-03-18 11:05:48'),
(531, NULL, NULL, 'ბრტყელტუჩა მეტალის რეზინის სახელურით 200MM,8\"', 'TOL3-10002 ბრტყელტუჩა მეტალის რეზინის სახელურით 200MM,8\"', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:02:28', '2020-03-18 11:06:28'),
(532, NULL, NULL, 'ბრტყელტუჩა გრძელცხვირა მეტალის რეზინის სახელურით მოხრილი თავით 160MM,6\"', 'TOL6-10008 ბრტყელტუჩა გრძელცხვირა მეტალის რეზინის სახელურით მოხრილი თავით 160MM,6\"', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:02:42', '2020-03-18 11:07:37'),
(533, NULL, NULL, 'ბრტყელტუჩა წყლის ტუმბო მეტალის რეზინის სახელურით 250MM,10\"', 'TOL7-10014 ბრტყელტუჩა წყლის ტუმბო მეტალის რეზინის სახელურით 250MM,10\"', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:02:24', '2020-03-18 11:08:38'),
(534, NULL, NULL, 'ბრტყელტუჩა მეტალის რეზინის სახელურით 160mm,6\"', 'TOL383-10015 ბრტყელტუჩა მეტალის რეზინის სახელურით 160mm,6\"', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:02:34', '2020-03-18 11:10:17'),
(535, NULL, NULL, 'ბრტყელტუჩა მეტალის რეზინის სახელურით 180mm,7\"', 'TOL384-10016  ბრტყელტუჩა მეტალის რეზინის სახელურით 180mm,7\"', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:02:30', '2020-03-18 11:10:49'),
(536, NULL, NULL, 'ბრტყელტუჩა სადურგლო მრგვალი ბოლოებით 180mm,7\"', 'TOL271-10044 ბრტყელტუჩა სადურგლო მრგვალი ბოლოებით 180mm,7\"', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:02:19', '2020-03-18 11:18:10'),
(537, NULL, NULL, 'ბრტყელტუჩა ჩამკეტით ოთკუთხა თავით 280მმ.11\'', 'TOL272-10056 ბრტყელტუჩა ჩამკეტით ოთკუთხა თავით 280მმ.11\'', 25, NULL, '0.00', 1, 2, NULL, NULL, 'გრამი', 3, '2020-05-11 12:02:22', '2020-03-18 11:19:24'),
(538, NULL, NULL, 'ფიქსატორი მეტალის', '10112	75mm, 3″	/\n10113	100mm, 4″	/\n10114	150mm, 5″	/\n10115	200mm, 8″	/', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:10', '2020-03-18 11:26:20'),
(539, NULL, NULL, 'ფიქსატორი', '50×150 mm	4.5×14.5 mm\n50×250 mm	4.5×14.5 mm\n120×300 mm	6.5×28.5 mm	\n120×500 mm	6.5×28.5 mm	\n120×800 mm	6.5×28.5 mm	\n120×1000 mm	6.5×28.5 mm', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:11', '2020-03-18 11:48:56'),
(540, NULL, NULL, 'ფიქსატორი ზამბარით', '100mm, 4″    /\n150mm, 6″	/\n200mm, 9″	/', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:10', '2020-03-18 11:50:14'),
(541, NULL, NULL, 'ფიქსატორი პლასტმასის', NULL, 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:09', '2020-03-18 11:51:28'),
(542, NULL, NULL, 'ქანჩის გასაღები რეგულირებადი 165MM', 'TOL1104-15280 ქანჩის გასაღები რეგულირებადი 165MM', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:01', '2020-03-18 12:50:22'),
(543, NULL, NULL, 'ქანჩის გასაღები ორმხრივი მეტალის  12*13MM', 'TOL17-15054 ქანჩის გასაღები ორმხრივი მეტალის  12*13MM', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:03', '2020-03-18 12:52:15'),
(544, NULL, NULL, 'ქანჩის გასაღები მეტალის,კომბინირებული 10მმ', 'TOL155-15018 ქანჩის გასაღები მეტალის,კომბინირებული 10მმ', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:04', '2020-03-18 12:54:23'),
(545, NULL, NULL, 'ქანჩის გასაღები მეტალის კომბინირებული 13MM', 'TOL260-15021 ქანჩის გასაღები მეტალის კომბინირებული 13MM', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:05', '2020-03-18 12:55:17'),
(546, NULL, NULL, 'ქანჩის გასაღები მეტალის 10mm', 'TOL392-15206 ქანჩის გასაღები მეტალის 10mm', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:07', '2020-03-18 12:57:59'),
(547, NULL, NULL, 'ქანჩის გასაღები მეტალის 13mm', 'TOL395-15209 ქანჩის გასაღები მეტალის 13mm', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:05', '2020-03-18 12:58:33'),
(548, NULL, NULL, 'ქანჩის გასაღები მეტალის 10mm', 'TOL1470-15236 ქანჩის გასაღები მეტალის 10mm', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:08', '2020-03-18 12:59:39'),
(549, NULL, NULL, 'ქანჩის გასაღები მეტალის 13mm', 'TOL1471-15239 ქანჩის გასაღები მეტალის 13mm', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:06', '2020-03-18 13:00:17'),
(550, NULL, NULL, 'ქანჩის გასაღები რეგულირების ფუნქციით 1/4\"', 'TOL279-15118 ქანჩის გასაღები რეგულირების ფუნქციით 1/4\"', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:00', '2020-03-19 07:55:24'),
(551, NULL, NULL, 'ქანჩის გასაღები სათადარიგო პირებით', 'TOL1101-15150 ქანჩის გასაღები სათადარიგო პირებით 1/4 -14ც-იანი', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:48:00', '2020-03-19 07:56:45'),
(552, NULL, NULL, 'ქანჩის გასაღები სათადარიგო პირებით 3/8 -12ც-იანი', 'TOL1102-15151 ქანჩის გასაღები სათადარიგო პირებით 3/8 -12ც-იანი', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:47:57', '2020-03-19 07:58:09'),
(553, NULL, NULL, 'ქანჩის გასაღები სათადარიგო პირებით 1/2 -12ც-იანი', 'TOL1103-15152 ქანჩის გასაღები სათადარიგო პირებით 1/2 -12ც-იანი', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:47:58', '2020-03-19 07:58:37');
INSERT INTO `products` (`product_id`, `supplier_code`, `barcode`, `name`, `description`, `category_id`, `minamount`, `price`, `condition_id`, `state_id`, `self_price`, `last_price`, `unit_name`, `unit_id`, `updated_at`, `created_at`) VALUES
(554, NULL, NULL, 'ქანჩის გასაღების ნაკრები სათადარიგო პირებით 46ც.-იანი', 'TOL34-15138 ქანჩის გასაღების ნაკრები სათადარიგო პირებით 46ც.-იანი', 25, NULL, '0.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:47:56', '2020-03-19 08:00:11'),
(555, NULL, NULL, 'ტელესკოპური 35 x 350 მმ / 06', 'ტელესკოპური 35 x 350 მმ / 06', 7, NULL, '2.65', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:07', '2020-03-19 09:17:09'),
(556, NULL, NULL, 'გამჭირვალე', 'გამჭირვალე / BCT01 / 16', 22, NULL, '0.20', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:02:11', '2020-03-19 09:21:33'),
(557, NULL, NULL, '23 x 30 x 30 / 03', '23 x 30 x 30 / 03', 24, NULL, '0.20', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:10:59', '2020-03-19 09:22:22'),
(558, NULL, NULL, 'საწოლის შემკვრელი / 13', 'საწოლის შემკვრელი / 13', 25, NULL, '0.45', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:18', '2020-03-19 09:23:24'),
(559, NULL, NULL, 'ანკერი დუბელთ / 01', 'ანკერი დუბელთ / 01', 22, NULL, '3.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 12:09:22', '2020-03-19 09:39:48'),
(560, NULL, NULL, 'დასაკეცი თარო 400 მმ / 03', 'დასაკეცი თარო 400 მმ / 03', 22, NULL, '15.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:52:48', '2020-03-19 09:41:24'),
(561, NULL, NULL, 'თაროს დამჭერი,ვიზუალი / ქრომირებული / 04', 'ვიზუალი / ქრომირებული / 04', 22, NULL, '0.25', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:52:43', '2020-03-19 09:49:28'),
(562, NULL, NULL, 'ოვალური', 'ოვალური / OVALCAM / 2602191 / 07', 22, NULL, '3.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:36', '2020-03-19 09:50:40'),
(563, NULL, NULL, 'ერთი ლამინატის უბრალო საკეტი / 0701001 / 01', 'ერთი ლამინატის უბრალო საკეტი / 0701001 / 01', 4, NULL, '1.10', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:52:47', '2020-03-19 09:56:00'),
(564, NULL, NULL, 'ერთი შუშის კარის საკეტი / 0702006 / 05', 'ერთი შუშის კარის საკეტი / 0702006 / 05', 4, NULL, '3.00', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:52:46', '2020-03-19 09:57:10'),
(565, NULL, NULL, 'ორი შუშის კარის საკეტი / 0702005 / 06', 'ორი შუშის კარის საკეტი / 0702005 / 06', 4, NULL, '3.20', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:34', '2020-03-19 09:57:45'),
(566, NULL, NULL, 'მოძრავი კარის საკეტი / 02', 'მოძრავი კარის საკეტი / 02', 4, NULL, '2.80', 1, 2, NULL, NULL, 'ცალი', 1, '2020-05-11 11:49:47', '2020-03-19 10:00:21'),
(567, NULL, NULL, 'სამზარეულოს რბილი კუთხე', NULL, 44, NULL, '489.00', 1, 2, NULL, NULL, 'ცალი', 1, '2021-01-05 15:52:08', '2020-12-22 22:36:45'),
(568, NULL, NULL, 'სამზარეულოს რბილი კუთხე', 'კუთხის ზომა 120 X 160 სმ', 44, NULL, '490.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-01-13 12:14:43', '2021-01-05 15:52:49'),
(569, NULL, NULL, 'სამზარეულოს რბილი კუთხე', 'კუთხის ზომა 120 X 160 სმ', 44, NULL, '490.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-01-13 12:19:47', '2021-01-13 12:19:47'),
(570, NULL, NULL, 'სამზარეულოს რბილი კუთხე', 'კუთხის ზომა 120 X 160 სმ', 44, NULL, '490.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-01-13 12:20:42', '2021-01-13 12:20:42'),
(571, NULL, NULL, 'სამზარეულოს რბილი კუთხე', 'კუთხის ზომა 120 X 160 სმ', 44, NULL, '490.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-01-13 12:21:30', '2021-01-13 12:21:30'),
(572, NULL, NULL, 'სამზარეულოს რბილი კუთხე', 'კუთხის ზომა 120 X 160 სმ', 44, NULL, '370.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-01-13 12:23:00', '2021-01-13 12:23:00'),
(573, NULL, NULL, 'სამზარეულოს რბილი კუთხე', 'კუთხის ზომა 120 X 160 სმ', 44, NULL, '370.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-01-13 12:23:56', '2021-01-13 12:23:56'),
(574, NULL, NULL, 'სამზარეულოს რბილი კუთხე', 'კუთხის ზომა 120 X 160 სმ', 44, NULL, '370.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-01-13 12:24:44', '2021-01-13 12:24:44'),
(575, NULL, NULL, 'სამზარეულოს რბილი კუთხე', 'კუთხის ზომა 120 X 160 სმ', 44, NULL, '370.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-01-13 12:25:11', '2021-01-13 12:25:11'),
(576, NULL, NULL, 'სამზარეულოს რბილი კუთხის ნაწილი', 'ზომა 120 სმ', 44, NULL, '150.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-01-13 12:49:19', '2021-01-13 12:27:37'),
(577, NULL, NULL, 'სამზარეულოს რბილი კუთხის ნაწილი', 'ზომა 120 სმ', 44, NULL, '90.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-01-13 12:49:42', '2021-01-13 12:28:42'),
(578, NULL, NULL, 'სამზარეულოს რბილი კუთხის ნაწილი', 'ზომა 120 სმ', 44, NULL, '90.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-01-13 12:49:36', '2021-01-13 12:29:23'),
(579, NULL, NULL, 'სამზარეულოს მაგიდა', '65 X 100 ( 65X 170 )', 34, NULL, '120.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-01-13 12:51:26', '2021-01-13 12:51:26'),
(580, NULL, NULL, 'სამზარეულოს მაგიდა', '65 X 100 ( 65X 170 )', 34, NULL, '120.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-01-13 12:52:02', '2021-01-13 12:52:02'),
(581, NULL, NULL, 'სამზარეულოს მაგიდა', '65 X 100 ( 65X 170 )', 34, NULL, '120.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-01-13 12:52:25', '2021-01-13 12:52:25'),
(582, NULL, NULL, 'სამზარეულოს სკამი', NULL, 40, NULL, '25.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-01-15 16:41:19', '2021-01-13 12:53:09'),
(583, NULL, NULL, 'სამზარეულოს სკამი', NULL, 40, NULL, '25.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-01-13 12:53:27', '2021-01-13 12:53:27'),
(584, NULL, NULL, 'სამზარეულოს სკამი', NULL, 40, NULL, '25.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-01-13 12:53:45', '2021-01-13 12:53:45'),
(585, NULL, NULL, 'სამზარეულოს სკამი', NULL, 40, NULL, '25.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-01-13 12:54:09', '2021-01-13 12:54:09'),
(586, NULL, NULL, 'სამზარეულო', NULL, 29, NULL, '950.00', 1, 2, NULL, NULL, 'ცალი', 1, '2021-03-03 11:54:48', '2021-01-13 12:55:43'),
(587, NULL, NULL, 'სამზარეულო', NULL, 28, NULL, '1100.00', 1, 2, NULL, NULL, 'ცალი', 1, '2021-03-03 11:54:51', '2021-01-13 12:57:23'),
(588, NULL, NULL, 'სამზარეულო', NULL, 29, NULL, '1600.00', 1, 2, NULL, NULL, 'ცალი', 1, '2021-03-03 11:54:54', '2021-01-13 12:57:55'),
(589, NULL, NULL, 'საწოლი + მატრასი', 'იომაქსი გთავაზობთ თურქული ლამინატისაგან დამზადებულ სერიულ საწოლებს ფერების არჩევანით...', 25, NULL, '320.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-01-15 14:10:46', '2021-01-13 13:00:36'),
(590, NULL, NULL, 'სამზარეულოს დასადგამი ნიჟარა', 'ზომა: 80 x 60 სმ \nსიღრმე: 16 სმ', 45, NULL, '100.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-02-19 14:14:42', '2021-02-19 14:14:42'),
(591, NULL, NULL, 'MDF-ის სამზარეულო 4m2', 'MDF-ის სამზარეულო 4m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე 2.60 მეტრი', 28, NULL, '1100.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:33:24', '2021-03-02 11:26:24'),
(592, NULL, NULL, 'MDF-ის სამზარეულო 4m2', 'MDF-ის სამზარეულო 4m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე 2.60 მეტრი', 28, NULL, '1100.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:33:36', '2021-03-02 11:30:53'),
(593, NULL, NULL, 'MDF-ის სამზარეულო 4m2', 'MDF-ის სამზარეულო 4m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე 2.60 მეტრი', 28, NULL, '1100.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:33:40', '2021-03-02 11:32:00'),
(594, NULL, NULL, 'MDF-ის სამზარეულო 4m2', 'MDF-ის სამზარეულო 4m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე 2.60 მეტრი', 28, NULL, '1100.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:33:46', '2021-03-02 11:32:24'),
(595, NULL, NULL, 'MDF-ის სამზარეულო 4m2', 'MDF-ის სამზარეულო 4m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე 2.60 მეტრი', 28, NULL, '1100.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:51:10', '2021-03-02 11:34:15'),
(596, NULL, NULL, 'MDF-ის სამზარეულო 4m2', 'MDF-ის სამზარეულო 4m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე 2.60 მეტრი', 28, NULL, '1100.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:51:19', '2021-03-02 11:34:40'),
(597, NULL, NULL, 'MDF-ის სამზარეულო 4m2', 'MDF-ის სამზარეულო 4m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე 2.60 მეტრი', 28, NULL, '1100.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:51:24', '2021-03-02 11:35:14'),
(598, NULL, NULL, 'MDF-ის სამზარეულო 4m2', 'MDF-ის სამზარეულო 4m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე 2.60 მეტრი', 28, NULL, '1100.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:51:29', '2021-03-02 11:35:38'),
(599, NULL, NULL, 'MDF-ის სამზარეულო 4m2', 'MDF-ის სამზარეულო 4m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე 2.60 მეტრი', 28, NULL, '1100.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:51:33', '2021-03-02 11:36:19'),
(600, NULL, NULL, 'MDF-ის სამზარეულო 4m2', 'MDF-ის სამზარეულო 4m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე 2.60 მეტრი', 28, NULL, '1100.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:51:38', '2021-03-02 11:36:45'),
(601, NULL, NULL, 'MDF-ის სამზარეულო 4m2', 'MDF-ის სამზარეულო 4m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე 2.60 მეტრი', 28, NULL, '1100.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:51:42', '2021-03-02 11:37:09'),
(602, NULL, NULL, 'MDF-ის სამზარეულო 4m2', 'MDF-ის სამზარეულო 4m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე 2.60 მეტრი', 28, NULL, '1100.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:51:46', '2021-03-02 11:38:59'),
(603, NULL, NULL, 'MDF-ის სამზარეულო 4m2', 'MDF-ის სამზარეულო 4m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე 2.60 მეტრი', 28, NULL, '1100.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:51:51', '2021-03-02 11:39:25'),
(604, NULL, NULL, 'MDF-ის სამზარეულო 4m2', 'MDF-ის სამზარეულო 4m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე 2.60 მეტრი', 28, NULL, '1100.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:51:56', '2021-03-02 11:39:48'),
(605, NULL, NULL, 'MDF-ის სამზარეულო 4m2', 'MDF-ის სამზარეულო 4m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე 2.60 მეტრი', 28, NULL, '1100.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:52:02', '2021-03-02 11:40:17'),
(606, NULL, NULL, 'MDF-ის სამზარეულო 4m2', 'MDF-ის სამზარეულო 4m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე 2.60 მეტრი', 28, NULL, '1100.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:52:09', '2021-03-02 11:40:40'),
(607, NULL, NULL, 'MDF-ის კუთხის სამზარეულო 7.8m2', 'MDF-ის სამზარეულო 7.8m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე  4.30 მეტრი', 38, NULL, '1550.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-18 18:31:26', '2021-03-02 11:48:30'),
(608, NULL, NULL, 'MDF-ის კუთხის სამზარეულო 7.8m2', 'MDF-ის სამზარეულო 7.8m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე  4.30 მეტრი', 38, NULL, '1550.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:53:08', '2021-03-02 11:53:08'),
(609, NULL, NULL, 'MDF-ის კუთხის სამზარეულო 7.8m2', 'MDF-ის სამზარეულო 7.8m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე  4.30 მეტრი', 38, NULL, '1550.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:53:40', '2021-03-02 11:53:40'),
(610, NULL, NULL, 'MDF-ის კუთხის სამზარეულო 7.8m2', 'MDF-ის სამზარეულო 7.8m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე  4.30 მეტრი', 38, NULL, '1550.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:54:05', '2021-03-02 11:54:05'),
(611, NULL, NULL, 'MDF-ის კუთხის სამზარეულო 7.8m2', 'MDF-ის სამზარეულო 7.8m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე  4.30 მეტრი', 38, NULL, '1550.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:54:29', '2021-03-02 11:54:29'),
(612, NULL, NULL, 'MDF-ის კუთხის სამზარეულო 7.8m2', 'MDF-ის სამზარეულო 7.8m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე  4.30 მეტრი', 38, NULL, '1550.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:54:53', '2021-03-02 11:54:53'),
(613, NULL, NULL, 'MDF-ის კუთხის სამზარეულო 7.8m2', 'MDF-ის სამზარეულო 7.8m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე  4.30 მეტრი', 38, NULL, '1550.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:55:17', '2021-03-02 11:55:17'),
(614, NULL, NULL, 'MDF-ის კუთხის სამზარეულო 7.8m2', 'MDF-ის სამზარეულო 7.8m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე  4.30 მეტრი', 38, NULL, '1550.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:55:41', '2021-03-02 11:55:41'),
(615, NULL, NULL, 'MDF-ის კუთხის სამზარეულო 7.8m2', 'MDF-ის სამზარეულო 7.8m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე  4.30 მეტრი', 1, NULL, '1550.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:56:22', '2021-03-02 11:56:22'),
(616, NULL, NULL, 'MDF-ის კუთხის სამზარეულო 7.8m2', 'MDF-ის სამზარეულო 7.8m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე  4.30 მეტრი', 38, NULL, '1550.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:56:51', '2021-03-02 11:56:51'),
(617, NULL, NULL, 'MDF-ის კუთხის სამზარეულო 7.8m2', 'MDF-ის სამზარეულო 7.8m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე  4.30 მეტრი', 38, NULL, '1550.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:57:21', '2021-03-02 11:57:21'),
(618, NULL, NULL, 'MDF-ის კუთხის სამზარეულო 7.8m2', 'MDF-ის სამზარეულო 7.8m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე  4.30 მეტრი', 38, NULL, '1550.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 11:58:33', '2021-03-02 11:58:33'),
(619, NULL, NULL, 'MDF-ის კუთხის სამზარეულო 7.8m2', 'MDF-ის სამზარეულო 7.8m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე  4.30 მეტრი', 38, NULL, '1550.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 12:02:37', '2021-03-02 12:02:37'),
(620, NULL, NULL, 'MDF-ის კუთხის სამზარეულო 7.8m2', 'MDF-ის სამზარეულო 7.8m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე  4.30 მეტრი', 38, NULL, '1550.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 12:02:59', '2021-03-02 12:02:59'),
(621, NULL, NULL, 'MDF-ის კუთხის სამზარეულო 7.8m2', 'MDF-ის სამზარეულო 7.8m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე  4.30 მეტრი', 38, NULL, '1550.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 12:03:27', '2021-03-02 12:03:27'),
(622, NULL, NULL, 'MDF-ის კუთხის სამზარეულო 7.8m2', 'MDF-ის სამზარეულო 7.8m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე  4.30 მეტრი', 38, NULL, '1550.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 12:04:09', '2021-03-02 12:04:09'),
(623, NULL, NULL, 'MDF-ის კუთხის სამზარეულო 7.8m2', 'MDF-ის სამზარეულო 7.8m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე  4.30 მეტრი', 38, NULL, '1550.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 12:04:38', '2021-03-02 12:04:38'),
(624, NULL, NULL, 'MDF-ის კუთხის სამზარეულო 7.8m2', 'MDF-ის სამზარეულო 7.8m2. ზედა ყუთების სიმაღლე 70 სმ, სიღრმე 30 სმ. ქვედა ყუთების სიმაღლე 85 სმ, სიღრმე 55 სმ. სამზარეულოს სიგრძე  4.30 მეტრი', 38, NULL, '1550.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-02 12:05:38', '2021-03-02 12:05:38'),
(625, NULL, NULL, 'სამზარეულო 260 სმ + ნიჟარა', 'სამზარეულო 260 სმ + ნიჟარა', 29, NULL, '1250.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-15 13:17:56', '2021-03-12 18:25:43'),
(626, NULL, NULL, 'დეკორატიული ტიხარი', NULL, 46, NULL, '80.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-18 14:26:36', '2021-03-18 14:26:36'),
(627, NULL, NULL, 'ლამინატის სამზარეულო 190 სმ', 'ლამინატის სამზარეულო 190 სმ', 29, NULL, '600.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-24 11:46:48', '2021-03-24 11:46:48'),
(628, NULL, NULL, 'აკრილის სამზარეულო 355 სმ', 'აკრილის სამზარეულო 355 სმ', 28, NULL, '1875.00', 1, 1, NULL, NULL, 'ცალი', 1, '2021-03-24 12:44:32', '2021-03-24 12:44:32');

-- --------------------------------------------------------

--
-- Table structure for table `product_files`
--

CREATE TABLE `product_files` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `original_file_name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `is_cover` tinyint(1) NOT NULL DEFAULT 0,
  `state_id` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `product_files`
--

INSERT INTO `product_files` (`id`, `product_id`, `file_name`, `original_file_name`, `is_cover`, `state_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'f7368c91ba7178e8966e8759f469a732.png', 'f7368c91ba7178e8966e8759f469a732.png_original', 0, 0, '2020-03-04 09:43:32', '2020-05-11 12:12:47'),
(2, 66, 'e4c544f981a767e79478e6afcf70e5f8.png', 'e4c544f981a767e79478e6afcf70e5f8.png_original', 0, 0, '2020-03-04 10:45:25', '2020-05-11 12:17:52'),
(3, 65, 'e5f56f97e47b41366ae40c05bc0d5c4c.png', 'e5f56f97e47b41366ae40c05bc0d5c4c.png_original', 0, 0, '2020-03-04 10:47:28', '2020-05-11 12:17:53'),
(4, 64, '40be285914086f6347e5dad477a5af50.png', '40be285914086f6347e5dad477a5af50.png_original', 0, 0, '2020-03-04 10:48:12', '2020-05-11 12:17:54'),
(5, 63, '77e79a4e1761e8ffcdd448cc7f575e21.png', '77e79a4e1761e8ffcdd448cc7f575e21.png_original', 0, 0, '2020-03-04 10:49:22', '2020-05-11 12:17:54'),
(6, 62, '156950eaf432d219be8aa6f50c0d34e8.png', '156950eaf432d219be8aa6f50c0d34e8.png_original', 0, 0, '2020-03-04 10:51:16', '2020-05-11 12:17:55'),
(7, 61, '3ef94aa58bd58da3bfd1d8edb22f9e0f.png', '3ef94aa58bd58da3bfd1d8edb22f9e0f.png_original', 0, 0, '2020-03-04 10:51:46', '2020-05-11 12:18:21'),
(8, 60, '2934ccf8d8db2a36f9932133b578cb8a.png', '2934ccf8d8db2a36f9932133b578cb8a.png_original', 0, 0, '2020-03-04 10:52:18', '2020-05-11 11:52:56'),
(9, 59, 'ed3b272b5d836824e39acf73e7adb874.png', 'ed3b272b5d836824e39acf73e7adb874.png_original', 0, 0, '2020-03-04 11:19:10', '2020-05-11 11:53:03'),
(10, 57, '37eb73b755eb8386fec7ee0bce436ddf.png', '37eb73b755eb8386fec7ee0bce436ddf.png_original', 0, 0, '2020-03-04 11:20:12', '2020-03-04 11:21:21'),
(11, 58, '2cc51d01640df86dba85dc9d81a516c3.png', '2cc51d01640df86dba85dc9d81a516c3.png_original', 0, 0, '2020-03-04 11:20:49', '2020-05-11 12:18:22'),
(12, 57, '08aeb1ef42b8df9fd1f0af8476f4a126.png', '08aeb1ef42b8df9fd1f0af8476f4a126.png_original', 0, 0, '2020-03-04 11:22:07', '2020-05-11 12:18:22'),
(13, 47, '726c2fddcd9fcf70f12f1f38aa4a4c6e.png', '726c2fddcd9fcf70f12f1f38aa4a4c6e.png_original', 0, 0, '2020-03-04 11:23:51', '2020-05-11 12:18:31'),
(14, 37, '63021b3351451740bae49a7ac791d2ce.png', '63021b3351451740bae49a7ac791d2ce.png_original', 0, 0, '2020-03-04 11:25:20', '2020-03-05 07:08:42'),
(15, 36, '129bed7200238e4ad898e60ec1a80064.png', '129bed7200238e4ad898e60ec1a80064.png_original', 0, 0, '2020-03-04 11:25:58', '2020-05-11 12:18:38'),
(16, 18, '2e1c94cac5352b478f2b55aae765f326.png', '2e1c94cac5352b478f2b55aae765f326.png_original', 0, 0, '2020-03-04 11:46:42', '2020-05-11 12:12:28'),
(17, 147, 'ce069ae89f4a80e84378e680e8c0771b.jpg', 'ce069ae89f4a80e84378e680e8c0771b.jpg_original', 0, 0, '2020-03-04 12:36:21', '2020-05-11 12:09:24'),
(18, 146, '65f4156960f1b02854f2ad263f69ac78.jpg', '65f4156960f1b02854f2ad263f69ac78.jpg_original', 0, 0, '2020-03-04 12:40:59', '2020-05-11 12:09:25'),
(19, 145, 'f7447ee9dd0fdb3a71eba38c739b5820.jpg', 'f7447ee9dd0fdb3a71eba38c739b5820.jpg_original', 0, 0, '2020-03-04 12:54:12', '2020-05-11 12:09:26'),
(20, 144, '95067ff1d6408ee6920802a775168fec.jpg', '95067ff1d6408ee6920802a775168fec.jpg_original', 0, 0, '2020-03-04 12:54:36', '2020-05-11 12:09:27'),
(21, 143, '3402d6b8a47a4014242d5edad86819ed.jpg', '3402d6b8a47a4014242d5edad86819ed.jpg_original', 0, 0, '2020-03-04 12:54:55', '2020-05-11 12:09:28'),
(22, 142, 'dd4b1d9ea144803c1b006fea50de8ac0.jpeg', 'dd4b1d9ea144803c1b006fea50de8ac0.jpeg_original', 0, 0, '2020-03-05 04:55:01', '2020-05-11 12:09:29'),
(23, 141, 'd6d2cdc4d9cf89f039694517f33f6260.jpeg', 'd6d2cdc4d9cf89f039694517f33f6260.jpeg_original', 0, 0, '2020-03-05 04:55:19', '2020-05-11 12:09:30'),
(24, 139, '14f87af0d6e23ad016b3682d3e0f0c7c.jpeg', '14f87af0d6e23ad016b3682d3e0f0c7c.jpeg_original', 0, 0, '2020-03-05 04:56:00', '2020-05-11 12:09:32'),
(25, 137, '163ea7f90c170f8aae0d0d556a0aef7f.jpg', '163ea7f90c170f8aae0d0d556a0aef7f.jpg_original', 0, 0, '2020-03-05 05:01:54', '2020-05-11 12:09:34'),
(26, 136, '6f9ba4c8bec9ee9aa820b05d182166e6.png', '6f9ba4c8bec9ee9aa820b05d182166e6.png_original', 0, 0, '2020-03-05 05:04:23', '2020-05-11 12:09:36'),
(27, 133, 'd682ddabc3d90e0e7b64f35e2b0ebac7.jpg', 'd682ddabc3d90e0e7b64f35e2b0ebac7.jpg_original', 0, 0, '2020-03-05 05:04:55', '2020-05-11 12:09:43'),
(28, 135, 'aa7a795ddee1e5f449463c8e5b4b944a.jpg', 'aa7a795ddee1e5f449463c8e5b4b944a.jpg_original', 0, 0, '2020-03-05 05:07:17', '2020-03-05 05:07:56'),
(29, 135, 'b4bc1e318b71708d97b87da920b0c418.jpg', 'b4bc1e318b71708d97b87da920b0c418.jpg_original', 0, 0, '2020-03-05 05:12:37', '2020-05-11 12:09:41'),
(30, 134, 'ab8203145ecadb63ee6dc9177f39ff92.jpg', 'ab8203145ecadb63ee6dc9177f39ff92.jpg_original', 0, 0, '2020-03-05 05:13:08', '2020-05-11 12:09:42'),
(31, 132, '075ff545351b83a9a7a0e2fa66a58dec.jpg', '075ff545351b83a9a7a0e2fa66a58dec.jpg_original', 0, 0, '2020-03-05 05:13:36', '2020-05-11 12:09:44'),
(32, 131, '84b9fd42e9084a0d6c932eeb829e7840.jpg', '84b9fd42e9084a0d6c932eeb829e7840.jpg_original', 0, 0, '2020-03-05 06:45:37', '2020-03-16 11:06:28'),
(33, 130, 'ab23e9e93e902dd76b5311e49d638c7e.jpg', 'ab23e9e93e902dd76b5311e49d638c7e.jpg_original', 0, 0, '2020-03-05 06:47:55', '2020-05-11 12:09:52'),
(34, 129, '0746fa5021a7bd4e9a29027d3c860cdd.jpg', '0746fa5021a7bd4e9a29027d3c860cdd.jpg_original', 0, 0, '2020-03-05 06:52:49', '2020-03-16 11:05:42'),
(35, 128, '006c269d4ed897cec5e241d466585066.jpg', '006c269d4ed897cec5e241d466585066.jpg_original', 0, 0, '2020-03-05 06:53:20', '2020-05-11 12:09:53'),
(36, 49, '6724532aa0e1786c2ddb656195a59559.png', '6724532aa0e1786c2ddb656195a59559.png_original', 0, 0, '2020-03-05 06:54:47', '2020-05-11 12:18:29'),
(37, 48, '91279f17feb980ce936d16d4c46e683b.png', '91279f17feb980ce936d16d4c46e683b.png_original', 0, 0, '2020-03-05 06:55:34', '2020-05-11 12:18:30'),
(38, 37, 'ed664d61deb6d798c958063893c50c09.jpg', 'ed664d61deb6d798c958063893c50c09.jpg_original', 0, 0, '2020-03-05 07:08:51', '2020-05-11 12:18:37'),
(39, 35, '99cc0bd262bec3593cb40c5fe747611b.jpg', '99cc0bd262bec3593cb40c5fe747611b.jpg_original', 0, 0, '2020-03-05 07:09:48', '2020-05-11 12:18:39'),
(40, 34, '48584238cdfa84a25d2a57f5c92fbc83.jpg', '48584238cdfa84a25d2a57f5c92fbc83.jpg_original', 0, 0, '2020-03-05 07:10:53', '2020-05-11 12:18:40'),
(41, 33, '7249e6f1a1dc9626340274cb49f1558a.jpg', '7249e6f1a1dc9626340274cb49f1558a.jpg_original', 0, 0, '2020-03-05 07:11:42', '2020-05-11 12:18:41'),
(42, 32, '26aa2e35e80575dc65973585d76c2443.jpg', '26aa2e35e80575dc65973585d76c2443.jpg_original', 0, 0, '2020-03-05 07:12:50', '2020-05-11 12:18:42'),
(43, 31, '76bcd73e0ffbb6f8cfec336a4782d20c.jpg', '76bcd73e0ffbb6f8cfec336a4782d20c.jpg_original', 0, 0, '2020-03-05 07:14:40', '2020-05-11 12:18:43'),
(44, 30, 'fe32ee6684cf36e7374292664596d92e.jpg', 'fe32ee6684cf36e7374292664596d92e.jpg_original', 0, 0, '2020-03-05 07:15:51', '2020-05-11 12:18:43'),
(45, 27, '819f3a21ca03490bb00970956a3f0b7a.jpg', '819f3a21ca03490bb00970956a3f0b7a.jpg_original', 0, 0, '2020-03-05 07:40:51', '2020-05-11 12:18:45'),
(46, 22, 'a38e8643c6086d5a51d6466ac2043458.jpg', 'a38e8643c6086d5a51d6466ac2043458.jpg_original', 0, 0, '2020-03-05 07:43:38', '2020-05-11 12:18:49'),
(47, 20, 'a6f07f34443544a2cb97ebc8188fcf2f.jpg', 'a6f07f34443544a2cb97ebc8188fcf2f.jpg_original', 0, 0, '2020-03-05 07:44:27', '2020-05-11 12:18:52'),
(48, 21, 'cdd9a7aaacc85ec3d6f1d506f78903c9.jpg', 'cdd9a7aaacc85ec3d6f1d506f78903c9.jpg_original', 0, 0, '2020-03-05 07:45:51', '2020-05-11 12:18:51'),
(49, 19, 'ba408a7a13ad4929f0d2557ffffc9d98.jpg', 'ba408a7a13ad4929f0d2557ffffc9d98.jpg_original', 0, 0, '2020-03-05 07:46:29', '2020-05-11 12:18:53'),
(50, 148, 'ec9e7d9ea2e7ec74737895bac71af22d.jpg', 'ec9e7d9ea2e7ec74737895bac71af22d.jpg_original', 0, 0, '2020-03-05 07:54:28', '2020-03-05 08:39:26'),
(51, 149, '9c4399d162675425fc87eabd504cfca4.jpg', '9c4399d162675425fc87eabd504cfca4.jpg_original', 0, 0, '2020-03-05 08:06:26', '2020-05-11 11:48:55'),
(52, 150, '36197438a3b28b272bb1294848822e85.jpg', '36197438a3b28b272bb1294848822e85.jpg_original', 0, 0, '2020-03-05 08:07:18', '2020-05-11 11:48:54'),
(53, 152, '256733506edd50da5e567f8b930d921f.jpg', '256733506edd50da5e567f8b930d921f.jpg_original', 0, 0, '2020-03-05 08:23:05', '2020-05-11 11:48:52'),
(54, 153, 'fa9dd12b6cf2bad52a244c777890e228.jpg', 'fa9dd12b6cf2bad52a244c777890e228.jpg_original', 0, 0, '2020-03-05 08:23:46', '2020-05-11 11:48:50'),
(55, 154, 'd649ffe68371389cf7d9c995fc457671.jpg', 'd649ffe68371389cf7d9c995fc457671.jpg_original', 0, 0, '2020-03-05 08:24:27', '2020-05-11 11:48:50'),
(56, 151, '6d98f8ab4952837a78673cd429d6383e.jpg', '6d98f8ab4952837a78673cd429d6383e.jpg_original', 0, 0, '2020-03-05 08:25:06', '2020-05-11 11:48:53'),
(57, 156, '4f286f1bc80d18c979549107931eea65.jpg', '4f286f1bc80d18c979549107931eea65.jpg_original', 0, 0, '2020-03-05 08:40:33', '2020-05-11 11:48:49'),
(58, 157, '9c560f04f6932b076b21246489785b15.jpg', '9c560f04f6932b076b21246489785b15.jpg_original', 0, 0, '2020-03-05 08:41:13', '2020-05-11 11:48:44'),
(59, 158, '57a42426080f7100031247426ab9f522.jpg', '57a42426080f7100031247426ab9f522.jpg_original', 0, 0, '2020-03-05 08:41:52', '2020-05-11 11:48:31'),
(60, 159, '1c94843525f34e2454412dd28b8ab3c7.jpg', '1c94843525f34e2454412dd28b8ab3c7.jpg_original', 0, 0, '2020-03-05 08:43:36', '2020-05-11 11:48:29'),
(61, 160, '9655fab5030e262551b04e4d0556aa3b.jpg', '9655fab5030e262551b04e4d0556aa3b.jpg_original', 0, 0, '2020-03-05 08:44:16', '2020-05-11 11:48:28'),
(62, 161, '6f53f9388297114f1d3b027e128f5e8c.jpg', '6f53f9388297114f1d3b027e128f5e8c.jpg_original', 0, 0, '2020-03-05 08:44:59', '2020-05-11 11:48:27'),
(63, 163, '4dc91f19c8065191860a9c1dc12e812d.jpg', '4dc91f19c8065191860a9c1dc12e812d.jpg_original', 0, 0, '2020-03-05 08:46:19', '2020-05-11 11:48:25'),
(64, 164, '439afc0ce2308f4326b553b713907ec9.jpg', '439afc0ce2308f4326b553b713907ec9.jpg_original', 0, 0, '2020-03-05 08:46:56', '2020-05-11 11:48:23'),
(65, 162, 'e27dec51db4280b0b5aec21aabe083d0.jpg', 'e27dec51db4280b0b5aec21aabe083d0.jpg_original', 0, 0, '2020-03-05 08:47:37', '2020-05-11 11:48:26'),
(66, 165, '0de3d1b8615901b6b484365b9a1df494.jpg', '0de3d1b8615901b6b484365b9a1df494.jpg_original', 0, 0, '2020-03-05 09:05:30', '2020-05-11 11:48:23'),
(67, 166, '6be5903e9f42538293ff4e0f3a4e7209.jpg', '6be5903e9f42538293ff4e0f3a4e7209.jpg_original', 0, 0, '2020-03-05 09:06:26', '2020-05-11 11:48:22'),
(68, 167, 'e381ad1392a12985d41ed576ef38fc85.jpg', 'e381ad1392a12985d41ed576ef38fc85.jpg_original', 0, 0, '2020-03-05 09:07:39', '2020-05-11 11:48:21'),
(69, 168, '151f3ab4e4d350b9f038785db0795029.jpg', '151f3ab4e4d350b9f038785db0795029.jpg_original', 0, 0, '2020-03-05 09:08:24', '2020-05-11 11:48:20'),
(70, 169, '48afbff6901dda91b4cf78e4407e5986.jpg', '48afbff6901dda91b4cf78e4407e5986.jpg_original', 0, 0, '2020-03-05 09:09:05', '2020-05-11 11:48:20'),
(71, 170, '4bdaf8893797f8bc0780ec00be968224.jpg', '4bdaf8893797f8bc0780ec00be968224.jpg_original', 0, 0, '2020-03-05 09:09:36', '2020-05-11 11:48:19'),
(72, 171, '0b08c625c5ef961c424a9181adf127ab.jpg', '0b08c625c5ef961c424a9181adf127ab.jpg_original', 0, 0, '2020-03-05 09:10:20', '2020-05-11 11:48:17'),
(73, 172, '9cb4f9d031a87254162f832b894b7e69.jpg', '9cb4f9d031a87254162f832b894b7e69.jpg_original', 0, 0, '2020-03-05 09:10:53', '2020-05-11 11:48:16'),
(74, 173, 'bbe0a26becda187e3386a6aae9579d2f.jpg', 'bbe0a26becda187e3386a6aae9579d2f.jpg_original', 0, 0, '2020-03-05 09:11:28', '2020-05-11 11:48:15'),
(75, 174, '9d3b0ea18386546f4fd24f1e07cd27b7.jpg', '9d3b0ea18386546f4fd24f1e07cd27b7.jpg_original', 0, 0, '2020-03-05 11:35:56', '2020-05-11 11:48:14'),
(76, 175, '158d51f15bebd20adb8eb755b030049c.jpg', '158d51f15bebd20adb8eb755b030049c.jpg_original', 0, 0, '2020-03-05 11:38:06', '2020-05-11 11:48:13'),
(77, 176, '1b1557ce3967bd4578d30cc0d068e982.jpg', '1b1557ce3967bd4578d30cc0d068e982.jpg_original', 0, 0, '2020-03-05 11:39:09', '2020-05-11 11:48:13'),
(78, 177, 'a88ad50a1c7302c9eb7c7a5011d3acce.jpg', 'a88ad50a1c7302c9eb7c7a5011d3acce.jpg_original', 0, 0, '2020-03-05 11:39:52', '2020-05-11 11:48:12'),
(79, 72, 'aa568f41849bfa962b975bc218e5728e.jpg', 'aa568f41849bfa962b975bc218e5728e.jpg_original', 0, 0, '2020-03-05 11:44:57', '2020-05-11 12:17:47'),
(80, 73, 'a6473f70241357bf03df6a6311013ddc.jpg', 'a6473f70241357bf03df6a6311013ddc.jpg_original', 0, 0, '2020-03-05 11:45:44', '2020-05-11 12:17:47'),
(81, 74, '1efd21ca2fa18415a5a916547da7bd10.jpg', '1efd21ca2fa18415a5a916547da7bd10.jpg_original', 0, 0, '2020-03-05 11:47:09', '2020-05-11 12:17:46'),
(82, 75, '467fc38bd897f8d40509ebfa534d19ad.jpg', '467fc38bd897f8d40509ebfa534d19ad.jpg_original', 0, 0, '2020-03-05 11:47:35', '2020-05-11 12:17:46'),
(83, 76, '59a7f5d89b5c71bc2017ec6cf5131279.jpg', '59a7f5d89b5c71bc2017ec6cf5131279.jpg_original', 0, 0, '2020-03-05 11:48:07', '2020-05-11 12:17:45'),
(84, 78, '7d888c097980a8e94b877ecbfd8495d8.jpg', '7d888c097980a8e94b877ecbfd8495d8.jpg_original', 0, 0, '2020-03-05 11:49:33', '2020-05-11 12:17:43'),
(85, 89, '91a6912f5fa5c5df36b2dc59cb2d6687.jpg', '91a6912f5fa5c5df36b2dc59cb2d6687.jpg_original', 0, 0, '2020-03-05 11:50:47', '2020-05-11 12:17:34'),
(86, 90, '2279f776bc3940b504de2de464f50c30.jpg', '2279f776bc3940b504de2de464f50c30.jpg_original', 0, 0, '2020-03-05 11:51:11', '2020-05-11 12:17:33'),
(87, 91, 'f2971c0236638c28055efbd15721ea0a.jpg', 'f2971c0236638c28055efbd15721ea0a.jpg_original', 0, 0, '2020-03-05 11:51:33', '2020-05-11 12:17:32'),
(88, 93, '4e8301cb739845ab6bfc16a64340e538.jpg', '4e8301cb739845ab6bfc16a64340e538.jpg_original', 0, 0, '2020-03-05 12:56:23', '2020-05-11 12:17:31'),
(89, 96, 'c6ee99a3122d9ad1ebfb1d650dc0a3cf.jpg', 'c6ee99a3122d9ad1ebfb1d650dc0a3cf.jpg_original', 0, 0, '2020-03-05 12:58:11', '2020-05-11 12:17:28'),
(90, 127, 'bd4e18066b77bf60abf732c20b9ac20c.jpg', 'bd4e18066b77bf60abf732c20b9ac20c.jpg_original', 0, 0, '2020-03-07 05:33:15', '2020-05-11 12:09:53'),
(91, 124, '24c873e2fa2ba94d3efafdca640b4a6c.jpg', '24c873e2fa2ba94d3efafdca640b4a6c.jpg_original', 0, 0, '2020-03-07 05:38:48', '2020-05-11 12:09:57'),
(92, 125, 'a7a5a95550930d2e25a110643b24cc4c.jpg', 'a7a5a95550930d2e25a110643b24cc4c.jpg_original', 0, 0, '2020-03-07 05:39:15', '2020-05-11 12:09:56'),
(93, 123, '834a975e98043fb458e0d2eaf69c46d4.jpg', '834a975e98043fb458e0d2eaf69c46d4.jpg_original', 0, 0, '2020-03-07 05:39:41', '2020-05-11 12:09:58'),
(94, 122, 'b4f5da3fe4e440a4d10f481e6bbc03d8.jpg', 'b4f5da3fe4e440a4d10f481e6bbc03d8.jpg_original', 0, 0, '2020-03-07 05:41:16', '2020-05-11 12:17:08'),
(95, 121, '0d583e9346cd2bf071858395a20fd5af.jpg', '0d583e9346cd2bf071858395a20fd5af.jpg_original', 0, 0, '2020-03-07 05:41:36', '2020-05-11 12:17:10'),
(96, 120, 'd5c30e78c4d4c14c2dd95b9f1e3d88c1.jpg', 'd5c30e78c4d4c14c2dd95b9f1e3d88c1.jpg_original', 0, 0, '2020-03-07 05:42:57', '2020-05-11 12:17:11'),
(97, 119, '25fec895fa5d5a1562afdb89e7faab47.jpg', '25fec895fa5d5a1562afdb89e7faab47.jpg_original', 0, 0, '2020-03-07 05:43:21', '2020-05-11 12:17:11'),
(98, 118, '0ab74e123aa1a2bd2f94a48d372340e5.jpg', '0ab74e123aa1a2bd2f94a48d372340e5.jpg_original', 0, 0, '2020-03-07 05:44:44', '2020-05-11 12:17:12'),
(99, 117, 'a96d806000ae57f2e3ecc289c63a8001.jpg', 'a96d806000ae57f2e3ecc289c63a8001.jpg_original', 0, 0, '2020-03-07 05:45:06', '2020-05-11 12:17:13'),
(100, 116, 'ffaa6d14e0cb34bf2fe720132dcd8b4f.jpg', 'ffaa6d14e0cb34bf2fe720132dcd8b4f.jpg_original', 0, 0, '2020-03-07 05:47:17', '2020-05-11 12:17:14'),
(101, 115, '76ba9e96c6f1fc147f204f6741cae360.jpg', '76ba9e96c6f1fc147f204f6741cae360.jpg_original', 0, 0, '2020-03-07 05:47:42', '2020-05-11 12:17:15'),
(102, 114, '360955ef9006122c571c77027774d2e7.jpg', '360955ef9006122c571c77027774d2e7.jpg_original', 0, 0, '2020-03-07 05:48:07', '2020-05-11 12:17:16'),
(103, 112, '74bba4883dda8e599993acdd81b4df7a.jpg', '74bba4883dda8e599993acdd81b4df7a.jpg_original', 0, 0, '2020-03-07 05:51:15', '2020-05-11 12:17:18'),
(104, 111, '77a4a69b753c87e9e289975ae5d4b0a4.jpg', '77a4a69b753c87e9e289975ae5d4b0a4.jpg_original', 0, 0, '2020-03-07 05:52:04', '2020-05-11 12:17:18'),
(105, 110, '0a25466d3d0115bdc650856200a63ccb.jpg', '0a25466d3d0115bdc650856200a63ccb.jpg_original', 0, 0, '2020-03-07 05:52:33', '2020-05-11 12:17:19'),
(106, 109, '7c79d88006669db0ee36de44cf58ccff.jpg', '7c79d88006669db0ee36de44cf58ccff.jpg_original', 0, 0, '2020-03-07 06:02:10', '2020-05-11 12:17:20'),
(107, 108, 'a3867fc93be12dd7f070e2dba7c94ffd.jpg', 'a3867fc93be12dd7f070e2dba7c94ffd.jpg_original', 0, 0, '2020-03-07 06:06:12', '2020-05-11 12:17:21'),
(108, 107, 'abf8a267ed746fc3518b12b3a5357989.jpg', 'abf8a267ed746fc3518b12b3a5357989.jpg_original', 0, 0, '2020-03-07 06:07:40', '2020-05-11 12:17:21'),
(109, 106, '4e0c9a29d9d15c9c19b06f2439d4669e.jpg', '4e0c9a29d9d15c9c19b06f2439d4669e.jpg_original', 0, 0, '2020-03-07 06:08:46', '2020-05-11 12:17:22'),
(110, 104, '5bbb6f6906da48115db1eccffc8593ba.jpg', '5bbb6f6906da48115db1eccffc8593ba.jpg_original', 0, 0, '2020-03-07 06:11:01', '2020-05-11 12:17:24'),
(111, 103, '037d5fa2ee2c817f84fb22cb56a42c7f.jpg', '037d5fa2ee2c817f84fb22cb56a42c7f.jpg_original', 0, 0, '2020-03-07 06:11:41', '2020-05-11 12:17:24'),
(112, 102, '2994befbc262678daa6a395cd09edaf4.jpg', '2994befbc262678daa6a395cd09edaf4.jpg_original', 0, 0, '2020-03-07 06:11:58', '2020-05-11 12:17:25'),
(113, 101, '4d3d76c9494a2e1ed4aa38a70309a643.jpg', '4d3d76c9494a2e1ed4aa38a70309a643.jpg_original', 0, 0, '2020-03-07 06:13:16', '2020-05-11 12:17:26'),
(114, 79, '890b6d029d28df89e4cbb55c5920e379.jpg', '890b6d029d28df89e4cbb55c5920e379.jpg_original', 0, 0, '2020-03-07 06:15:26', '2020-05-11 12:17:41'),
(115, 80, '083f0902676cfb1b4ee452df1236193f.jpg', '083f0902676cfb1b4ee452df1236193f.jpg_original', 0, 0, '2020-03-07 06:15:49', '2020-05-11 12:17:40'),
(116, 81, '98065fa617db40792d8d9a244a821462.jpg', '98065fa617db40792d8d9a244a821462.jpg_original', 0, 0, '2020-03-07 06:16:52', '2020-05-11 12:17:40'),
(117, 82, '5aca9be51b404bffccaa90716e8d29e3.jpg', '5aca9be51b404bffccaa90716e8d29e3.jpg_original', 0, 0, '2020-03-07 06:17:13', '2020-05-11 12:17:39'),
(118, 83, 'f8ae54b461dbe559227ba1ff719b4e87.jpg', 'f8ae54b461dbe559227ba1ff719b4e87.jpg_original', 0, 0, '2020-03-07 06:17:34', '2020-05-11 12:17:39'),
(119, 84, 'f29f49ddddc0ca9ad07ae0dee891cb52.jpg', 'f29f49ddddc0ca9ad07ae0dee891cb52.jpg_original', 0, 0, '2020-03-07 06:17:54', '2020-05-11 12:17:38'),
(120, 85, '4003ac00360e656ad514a77f9bc54616.jpg', '4003ac00360e656ad514a77f9bc54616.jpg_original', 0, 0, '2020-03-07 06:18:15', '2020-05-11 12:17:36'),
(121, 50, '10ee1890a482de2787b3d79c3ec73c25.jpg', '10ee1890a482de2787b3d79c3ec73c25.jpg_original', 0, 0, '2020-03-07 06:20:22', '2020-05-11 12:18:28'),
(122, 51, '5fe6ae17fcca8efda3757cf4e8c55cad.jpg', '5fe6ae17fcca8efda3757cf4e8c55cad.jpg_original', 0, 0, '2020-03-07 06:21:09', '2020-05-11 12:18:28'),
(123, 52, 'df63b375dd67794961ac45532e591189.jpg', 'df63b375dd67794961ac45532e591189.jpg_original', 0, 0, '2020-03-07 06:21:55', '2020-05-11 12:18:27'),
(124, 54, '4a269f595aa972d82e997cbdad508958.jpg', '4a269f595aa972d82e997cbdad508958.jpg_original', 0, 0, '2020-03-07 06:22:54', '2020-05-11 12:18:25'),
(125, 56, '26f697583cdadab504d450348db5c2e8.jpg', '26f697583cdadab504d450348db5c2e8.jpg_original', 0, 0, '2020-03-07 06:23:46', '2020-05-11 12:18:23'),
(126, 55, 'c36ccd4b91a0f05aac39ef6d5ead5e33.jpg', 'c36ccd4b91a0f05aac39ef6d5ead5e33.jpg_original', 0, 0, '2020-03-07 06:24:44', '2020-05-11 12:18:24'),
(127, 53, '5e8b43c251383f39787f74a53a8a9df0.jpg', '5e8b43c251383f39787f74a53a8a9df0.jpg_original', 0, 0, '2020-03-07 06:25:18', '2020-05-11 12:18:26'),
(128, 178, '0582a05c23954a9c1bf88f2b5055bea9.jpg', '0582a05c23954a9c1bf88f2b5055bea9.jpg_original', 0, 0, '2020-03-07 09:10:12', '2020-05-11 12:11:00'),
(129, 179, '78f906d78b6bb3bb47133ea038f1ce90.jpg', '78f906d78b6bb3bb47133ea038f1ce90.jpg_original', 0, 0, '2020-03-07 09:29:48', '2020-05-11 12:19:54'),
(130, 180, '6871f51bb7c7f9cb96a5c8818b9eea6e.jpg', '6871f51bb7c7f9cb96a5c8818b9eea6e.jpg_original', 0, 0, '2020-03-07 09:31:27', '2020-05-11 12:19:53'),
(131, 181, '4ec0dbf2e1af827fdf69cc348355fec2.jpg', '4ec0dbf2e1af827fdf69cc348355fec2.jpg_original', 0, 0, '2020-03-07 09:33:17', '2020-05-11 12:18:09'),
(132, 181, '8da58f88f3f3e018494bcdab93cfc799.jpg', '8da58f88f3f3e018494bcdab93cfc799.jpg_original', 0, 0, '2020-03-07 09:33:19', '2020-03-07 09:33:22'),
(133, 182, 'd8807ce2096021b7743769a8403a39e1.jpg', 'd8807ce2096021b7743769a8403a39e1.jpg_original', 0, 0, '2020-03-07 09:34:38', '2020-05-11 12:11:46'),
(134, 183, '50323e35475660a10073b0ada374e9ab.jpg', '50323e35475660a10073b0ada374e9ab.jpg_original', 0, 0, '2020-03-07 09:39:08', '2020-05-11 12:11:47'),
(135, 184, '2c2efce28c1ce5328c83f439a0c62bce.jpg', '2c2efce28c1ce5328c83f439a0c62bce.jpg_original', 0, 0, '2020-03-07 09:40:46', '2020-05-11 12:15:02'),
(136, 185, '880fc0266c65785817ffe9e1198fc652.jpg', '880fc0266c65785817ffe9e1198fc652.jpg_original', 0, 0, '2020-03-07 09:41:55', '2020-05-11 12:18:17'),
(137, 186, 'f82d3273b85bc3edef99b3a988dc0202.jpg', 'f82d3273b85bc3edef99b3a988dc0202.jpg_original', 0, 0, '2020-03-07 09:43:52', '2020-05-11 12:19:56'),
(138, 187, '8650d771ef61aeeebfdd0bd0b969f81b.jpg', '8650d771ef61aeeebfdd0bd0b969f81b.jpg_original', 0, 0, '2020-03-07 09:45:20', '2020-05-11 12:11:41'),
(139, 188, '58404c5559426e7cf0b1a9cd44dd6102.jpg', '58404c5559426e7cf0b1a9cd44dd6102.jpg_original', 0, 0, '2020-03-07 09:46:26', '2020-05-11 12:19:52'),
(140, 189, '173580bca7fe4807e94b1273a173b450.jpg', '173580bca7fe4807e94b1273a173b450.jpg_original', 0, 0, '2020-03-07 09:48:11', '2020-05-11 12:11:51'),
(141, 190, 'e1855dd8d4de9afccff4d80b1b0d75bb.jpg', 'e1855dd8d4de9afccff4d80b1b0d75bb.jpg_original', 0, 0, '2020-03-07 09:57:36', '2020-05-11 12:11:40'),
(142, 191, 'e8fc1d6e78933ba792e16bcf43b22fe8.jpg', 'e8fc1d6e78933ba792e16bcf43b22fe8.jpg_original', 0, 0, '2020-03-07 09:58:48', '2020-05-11 12:11:42'),
(143, 192, '424be0b8cb3bce19b00758ac1a3dd81c.jpg', '424be0b8cb3bce19b00758ac1a3dd81c.jpg_original', 0, 0, '2020-03-07 09:59:51', '2020-05-11 12:11:44'),
(144, 193, 'ebdabe8918e9f6103ad25227582bfefb.jpg', 'ebdabe8918e9f6103ad25227582bfefb.jpg_original', 0, 0, '2020-03-07 10:01:00', '2020-05-11 12:11:36'),
(145, 194, 'bc84766eb7b307ac514d63fe53a06627.jpg', 'bc84766eb7b307ac514d63fe53a06627.jpg_original', 0, 0, '2020-03-07 10:09:36', '2020-05-11 12:10:24'),
(146, 195, 'ebda63b181ad16531d47289dbcf6cca7.jpg', 'ebda63b181ad16531d47289dbcf6cca7.jpg_original', 0, 0, '2020-03-07 10:10:43', '2020-05-11 12:18:10'),
(147, 196, '4966fc76fd171780f294e5a3bcb73dea.jpg', '4966fc76fd171780f294e5a3bcb73dea.jpg_original', 0, 0, '2020-03-11 04:26:55', '2020-05-11 12:11:38'),
(148, 197, 'e1946bbb00063b8db906d3248d182e6a.jpg', 'e1946bbb00063b8db906d3248d182e6a.jpg_original', 0, 0, '2020-03-11 04:27:48', '2020-05-11 12:11:45'),
(149, 199, '59499ba78cb72e9b3ff330c448e43aae.jpg', '59499ba78cb72e9b3ff330c448e43aae.jpg_original', 0, 0, '2020-03-11 04:29:42', '2020-05-11 12:15:05'),
(150, 201, '8c031de2c08a0fadfc1324a24b56a272.jpg', '8c031de2c08a0fadfc1324a24b56a272.jpg_original', 0, 0, '2020-03-11 04:31:39', '2020-05-11 12:15:04'),
(151, 202, 'f9df162a9dce875f16a3bc5d1f969582.jpg', 'f9df162a9dce875f16a3bc5d1f969582.jpg_original', 0, 0, '2020-03-11 04:32:36', '2020-05-11 12:18:16'),
(152, 203, '10b6a0a15a26c76f3a6d7377f6838dd7.jpg', '10b6a0a15a26c76f3a6d7377f6838dd7.jpg_original', 0, 0, '2020-03-11 04:50:52', '2020-05-11 12:10:13'),
(153, 204, 'ff01305051153e255f5ad16f4f0e9ba0.jpg', 'ff01305051153e255f5ad16f4f0e9ba0.jpg_original', 0, 0, '2020-03-11 04:51:59', '2020-05-11 12:10:12'),
(154, 205, '3580faaadf40c49305a1268efdd6b858.jpg', '3580faaadf40c49305a1268efdd6b858.jpg_original', 0, 0, '2020-03-11 04:54:37', '2020-05-11 12:10:10'),
(155, 206, '9886477069e14c1845c14bf6216e532b.jpg', '9886477069e14c1845c14bf6216e532b.jpg_original', 0, 0, '2020-03-11 04:55:34', '2020-05-11 12:10:11'),
(156, 207, '6365fe3690c00dfabbeebe6b747a62e8.jpg', '6365fe3690c00dfabbeebe6b747a62e8.jpg_original', 0, 0, '2020-03-11 04:56:41', '2020-05-11 12:15:16'),
(157, 208, '371445a378cd7eae8ac727555869af36.jpg', '371445a378cd7eae8ac727555869af36.jpg_original', 0, 0, '2020-03-11 04:57:31', '2020-05-11 12:15:07'),
(158, 209, 'fcdff83bd1c9f894ff641069317e6d73.jpg', 'fcdff83bd1c9f894ff641069317e6d73.jpg_original', 0, 0, '2020-03-11 04:58:29', '2020-05-11 12:15:15'),
(159, 210, '24c261f5c024efb8cb8f0397b1344417.jpg', '24c261f5c024efb8cb8f0397b1344417.jpg_original', 0, 0, '2020-03-11 05:01:50', '2020-05-11 12:15:03'),
(160, 211, 'cbc96cca290891fad44d41993903b85a.jpg', 'cbc96cca290891fad44d41993903b85a.jpg_original', 0, 0, '2020-03-11 05:02:47', '2020-05-11 12:15:08'),
(161, 212, '3672c5eb09feea97cd1793b88556cf06.jpg', '3672c5eb09feea97cd1793b88556cf06.jpg_original', 0, 0, '2020-03-11 05:10:13', '2020-05-11 12:11:34'),
(162, 213, 'f93e5c0e06dbaae5b1a96dc2433cf0da.jpg', 'f93e5c0e06dbaae5b1a96dc2433cf0da.jpg_original', 0, 0, '2020-03-11 05:11:06', '2020-05-11 12:11:33'),
(163, 214, '04b4ca1f5a58234d4399500d4ec9e9dd.jpg', '04b4ca1f5a58234d4399500d4ec9e9dd.jpg_original', 0, 0, '2020-03-11 05:12:03', '2020-05-11 12:11:26'),
(164, 215, '5d2a94262e34e1c42ccf7eb612c80608.jpg', '5d2a94262e34e1c42ccf7eb612c80608.jpg_original', 0, 0, '2020-03-11 05:12:48', '2020-05-11 12:11:32'),
(165, 216, '3be41558251ecc9bd674aeaec9cef67a.jpg', '3be41558251ecc9bd674aeaec9cef67a.jpg_original', 0, 0, '2020-03-11 05:13:35', '2020-05-11 12:11:30'),
(166, 217, '1428ee704c2539a473ae3bbadbef3237.jpg', '1428ee704c2539a473ae3bbadbef3237.jpg_original', 0, 0, '2020-03-11 05:15:43', '2020-05-11 12:11:31'),
(167, 218, '2d0de5b7ed2bd93513dd55492e3f1d1a.jpg', '2d0de5b7ed2bd93513dd55492e3f1d1a.jpg_original', 0, 0, '2020-03-11 05:16:37', '2020-05-11 12:11:28'),
(168, 219, 'ed435e1aac730b6ffa402700f4cded55.jpg', 'ed435e1aac730b6ffa402700f4cded55.jpg_original', 0, 0, '2020-03-11 05:17:44', '2020-05-11 12:11:28'),
(169, 220, 'c8659591b933421f374c639978cf5b0b.jpg', 'c8659591b933421f374c639978cf5b0b.jpg_original', 0, 0, '2020-03-11 05:18:56', '2020-05-11 12:15:06'),
(170, 221, '207dcc49fff49b31c95626032e7059b7.jpg', '207dcc49fff49b31c95626032e7059b7.jpg_original', 0, 0, '2020-03-11 05:19:55', '2020-05-11 12:18:15'),
(171, 222, 'dee668c5487c6b9c28e38787d58761ea.jpg', 'dee668c5487c6b9c28e38787d58761ea.jpg_original', 0, 0, '2020-03-11 05:20:49', '2020-05-11 12:15:14'),
(172, 223, 'd6063cec8297e80baae539362858023d.jpg', 'd6063cec8297e80baae539362858023d.jpg_original', 0, 0, '2020-03-11 05:21:39', '2020-05-11 12:15:13'),
(173, 224, '55680bd73e4deddb07a38c663616c36d.jpg', '55680bd73e4deddb07a38c663616c36d.jpg_original', 0, 0, '2020-03-11 05:23:25', '2020-05-11 12:15:12'),
(174, 225, '7c7e0e8bb2884d8260dac1718e784427.jpg', '7c7e0e8bb2884d8260dac1718e784427.jpg_original', 0, 0, '2020-03-11 05:24:18', '2020-05-11 12:15:11'),
(175, 226, '5c457e3e000d75776e8d08c386fcaddd.jpg', '5c457e3e000d75776e8d08c386fcaddd.jpg_original', 0, 0, '2020-03-11 05:25:23', '2020-05-11 12:15:10'),
(176, 227, '960601c78b6474d06dbf894c20c9ed24.jpg', '960601c78b6474d06dbf894c20c9ed24.jpg_original', 0, 0, '2020-03-11 05:27:12', '2020-05-11 12:18:11'),
(177, 228, '1e1a3d7858743fc4c8307283ed830b73.jpg', '1e1a3d7858743fc4c8307283ed830b73.jpg_original', 0, 0, '2020-03-11 05:28:17', '2020-05-11 12:15:01'),
(178, 229, 'fb0c27e65b233708d6dbd5f1d2f16e36.jpg', 'fb0c27e65b233708d6dbd5f1d2f16e36.jpg_original', 0, 0, '2020-03-11 05:29:41', '2020-05-11 12:10:14'),
(179, 230, 'ca0cd4e09ddf1f0f13d8c8a28026d928.jpg', 'ca0cd4e09ddf1f0f13d8c8a28026d928.jpg_original', 0, 0, '2020-03-11 05:30:37', '2020-05-11 12:15:00'),
(180, 231, '30f2f0e3eaadf5560bd203cc07697913.jpg', '30f2f0e3eaadf5560bd203cc07697913.jpg_original', 0, 0, '2020-03-11 05:52:16', '2020-05-11 12:11:48'),
(181, 232, '6e244c78dcfc2cedab7208aa1d8583ae.jpg', '6e244c78dcfc2cedab7208aa1d8583ae.jpg_original', 0, 0, '2020-03-11 05:53:12', '2020-05-11 12:11:52'),
(182, 233, 'b45bece484f8a011bc22c86d534ef973.jpg', 'b45bece484f8a011bc22c86d534ef973.jpg_original', 0, 0, '2020-03-11 05:53:58', '2020-05-11 12:19:54'),
(183, 234, 'a88a2aefa21435b9e2a9f9097db9abb9.jpg', 'a88a2aefa21435b9e2a9f9097db9abb9.jpg_original', 0, 0, '2020-03-11 05:55:09', '2020-05-11 12:11:49'),
(184, 235, '026f7a9382e5837b8977537992bc958e.jpg', '026f7a9382e5837b8977537992bc958e.jpg_original', 0, 0, '2020-03-11 05:56:14', '2020-05-11 12:15:09'),
(185, 236, 'f589352b2c8febab8da9b5bffaca4204.jpg', 'f589352b2c8febab8da9b5bffaca4204.jpg_original', 0, 0, '2020-03-11 05:57:01', '2020-05-11 12:19:55'),
(186, 238, 'd1a9a17ae3b47d6302e2a66f7dee2e7a.jpg', 'd1a9a17ae3b47d6302e2a66f7dee2e7a.jpg_original', 0, 0, '2020-03-11 05:57:53', '2020-05-11 12:19:57'),
(187, 239, 'e642be36adf72487aa0b9b12c64937da.jpg', 'e642be36adf72487aa0b9b12c64937da.jpg_original', 0, 0, '2020-03-11 05:59:37', '2020-05-11 12:11:53'),
(188, 240, '23064ac9b990dac46e7f96310d9b67a9.jpg', '23064ac9b990dac46e7f96310d9b67a9.jpg_original', 0, 0, '2020-03-11 06:07:01', '2020-05-11 12:19:38'),
(189, 241, 'c17afa4d1e28a2d085dbb2f27c95a2b4.jpg', 'c17afa4d1e28a2d085dbb2f27c95a2b4.jpg_original', 0, 0, '2020-03-11 06:08:27', '2020-05-11 12:19:14'),
(190, 242, '3a5b0d4e310c6a54deff35272de076ea.jpg', '3a5b0d4e310c6a54deff35272de076ea.jpg_original', 0, 0, '2020-03-11 06:09:38', '2020-05-11 12:19:39'),
(191, 243, '17e3c057330ca39906f865c844b45f6c.jpg', '17e3c057330ca39906f865c844b45f6c.jpg_original', 0, 0, '2020-03-11 06:12:33', '2020-05-11 12:19:13'),
(192, 244, 'eee4d651b16d78b0d6ed2bd7472c28d8.jpg', 'eee4d651b16d78b0d6ed2bd7472c28d8.jpg_original', 0, 0, '2020-03-11 06:13:30', '2020-05-11 12:19:38'),
(193, 245, '297222928890d203ca4ad137cb242f60.jpg', '297222928890d203ca4ad137cb242f60.jpg_original', 0, 0, '2020-03-11 06:14:16', '2020-05-11 12:19:11'),
(194, 246, 'dc665aba4927550e356607d7af5cc359.jpg', 'dc665aba4927550e356607d7af5cc359.jpg_original', 0, 0, '2020-03-11 06:50:10', '2020-05-11 12:19:09'),
(195, 247, '2a426ccfbfe599bce83e88e2da138927.jpg', '2a426ccfbfe599bce83e88e2da138927.jpg_original', 0, 0, '2020-03-11 06:51:14', '2020-05-11 12:19:03'),
(196, 248, '1158d0e70420d4188ada89672ce37034.jpg', '1158d0e70420d4188ada89672ce37034.jpg_original', 0, 0, '2020-03-11 06:52:32', '2020-05-11 12:19:07'),
(197, 249, '0b9359a8045e2664757d8b2d3b7f0f3f.jpg', '0b9359a8045e2664757d8b2d3b7f0f3f.jpg_original', 0, 0, '2020-03-11 06:54:25', '2020-05-11 12:19:01'),
(198, 250, 'e0b7ef2156358edd79969c45219735ad.jpg', 'e0b7ef2156358edd79969c45219735ad.jpg_original', 0, 0, '2020-03-11 06:56:31', '2020-05-11 12:19:03'),
(199, 251, '9a1dfe9b039044b7d33a6f475a544bd2.jpg', '9a1dfe9b039044b7d33a6f475a544bd2.jpg_original', 0, 0, '2020-03-11 06:57:54', '2020-05-11 12:19:00'),
(200, 252, 'cc1807ee1e9f0de20ce9848056ab2e70.jpg', 'cc1807ee1e9f0de20ce9848056ab2e70.jpg_original', 0, 0, '2020-03-11 07:19:34', '2020-05-11 12:19:05'),
(201, 253, 'd416b5dc015e052033922948c61cbf83.jpg', 'd416b5dc015e052033922948c61cbf83.jpg_original', 0, 0, '2020-03-11 07:20:37', '2020-05-11 12:15:49'),
(202, 254, '03fb01dae21fb92c5c2cf4bda0dc7192.jpg', '03fb01dae21fb92c5c2cf4bda0dc7192.jpg_original', 0, 0, '2020-03-11 07:22:10', '2020-05-11 12:19:41'),
(203, 255, '0fbde374e25fd6985175b098c765af75.jpg', '0fbde374e25fd6985175b098c765af75.jpg_original', 0, 0, '2020-03-11 07:22:59', '2020-05-11 12:19:35'),
(204, 256, 'fb5fc607c4251caaac3183409e04ca25.jpg', 'fb5fc607c4251caaac3183409e04ca25.jpg_original', 0, 0, '2020-03-11 07:23:59', '2020-05-11 12:19:07'),
(205, 257, '169800b709eafdded248419fa0e7cb3e.jpg', '169800b709eafdded248419fa0e7cb3e.jpg_original', 0, 0, '2020-03-11 07:24:52', '2020-05-11 12:19:36'),
(206, 258, 'a477b982ac7d6a92d231d1b26246debb.jpg', 'a477b982ac7d6a92d231d1b26246debb.jpg_original', 0, 0, '2020-03-11 07:26:50', '2020-05-11 12:19:44'),
(207, 259, '902dd9c4f21c36336e08e3f1e82fcc36.jpg', '902dd9c4f21c36336e08e3f1e82fcc36.jpg_original', 0, 0, '2020-03-11 07:27:35', '2020-05-11 12:19:41'),
(208, 260, '191b32ca50067ce97fd9b6d267ba18bc.jpg', '191b32ca50067ce97fd9b6d267ba18bc.jpg_original', 0, 0, '2020-03-11 07:28:27', '2020-05-11 12:19:43'),
(209, 261, '66b69d0438bfdd2a19fc21e68ac6dd3e.jpg', '66b69d0438bfdd2a19fc21e68ac6dd3e.jpg_original', 0, 0, '2020-03-11 07:29:38', '2020-05-11 12:19:06'),
(210, 262, 'ece3b33275ea52f3f5c2b9b67bd38dc1.jpg', 'ece3b33275ea52f3f5c2b9b67bd38dc1.jpg_original', 0, 0, '2020-03-11 07:30:25', '2020-05-11 12:19:43'),
(211, 263, '2defeee7e0b99248b199cb8b9b96c565.jpg', '2defeee7e0b99248b199cb8b9b96c565.jpg_original', 0, 0, '2020-03-11 07:31:12', '2020-05-11 12:19:05'),
(212, 264, '83e58902b09d4e47b5f9e30a910e5743.jpg', '83e58902b09d4e47b5f9e30a910e5743.jpg_original', 0, 0, '2020-03-11 07:33:26', '2020-05-11 12:18:57'),
(213, 265, 'e84b94830737ff925d34877221e1f5b9.jpg', 'e84b94830737ff925d34877221e1f5b9.jpg_original', 0, 0, '2020-03-11 07:34:13', '2020-05-11 12:15:51'),
(214, 266, '8b0503e09e99c6f7e96d05136d4f9b59.jpg', '8b0503e09e99c6f7e96d05136d4f9b59.jpg_original', 0, 0, '2020-03-11 07:35:23', '2020-05-11 12:15:53'),
(215, 267, '28ccd483e3ac1a916fbdc9283cdfd574.jpg', '28ccd483e3ac1a916fbdc9283cdfd574.jpg_original', 0, 0, '2020-03-11 07:36:17', '2020-05-11 12:15:52'),
(216, 268, '16595d9509c655eac2fb81af8403c96f.jpg', '16595d9509c655eac2fb81af8403c96f.jpg_original', 0, 0, '2020-03-11 07:37:12', '2020-05-11 12:12:22'),
(217, 269, '83b25786369ca3e1eda8540d4e0d14b7.jpg', '83b25786369ca3e1eda8540d4e0d14b7.jpg_original', 0, 0, '2020-03-11 07:38:01', '2020-05-11 12:19:37'),
(218, 270, '580829eb99ac149a4dbdaca0a5da7e74.jpg', '580829eb99ac149a4dbdaca0a5da7e74.jpg_original', 0, 0, '2020-03-11 08:08:25', '2020-05-11 12:19:14'),
(219, 271, '200ca217f0ee25b86101917354b368d7.jpg', '200ca217f0ee25b86101917354b368d7.jpg_original', 0, 0, '2020-03-11 08:09:26', '2020-05-11 12:18:55'),
(220, 272, '4fdb5ce4fce3bffb603b3f55b5d08073.jpg', '4fdb5ce4fce3bffb603b3f55b5d08073.jpg_original', 0, 0, '2020-03-11 08:10:17', '2020-05-11 12:18:59'),
(221, 273, 'a2dbc487b27b4c65db7d793df106407e.jpg', 'a2dbc487b27b4c65db7d793df106407e.jpg_original', 0, 0, '2020-03-11 08:11:10', '2020-05-11 12:10:27'),
(222, 274, '8757b49f936b95347b4a080c81822b5b.jpg', '8757b49f936b95347b4a080c81822b5b.jpg_original', 0, 0, '2020-03-11 08:12:25', '2020-05-11 12:18:59'),
(223, 275, '5f3381927d376876f2634dc5f6954dd5.jpg', '5f3381927d376876f2634dc5f6954dd5.jpg_original', 0, 0, '2020-03-11 08:13:15', '2020-05-11 12:19:39'),
(224, 276, '41ec110bd1ba156bc63aa74a42277597.jpg', '41ec110bd1ba156bc63aa74a42277597.jpg_original', 0, 0, '2020-03-11 08:14:08', '2020-05-11 12:19:12'),
(225, 277, 'de890e49e0cd94c145541eaad8728479.jpg', 'de890e49e0cd94c145541eaad8728479.jpg_original', 0, 0, '2020-03-11 08:15:59', '2020-05-11 12:19:10'),
(226, 278, '4bbe3fcbe6149c59c414c2b63e8f0ad1.jpg', '4bbe3fcbe6149c59c414c2b63e8f0ad1.jpg_original', 0, 0, '2020-03-11 08:16:46', '2020-05-11 12:19:12'),
(227, 279, 'f60c939ba55c456b3928fa0ee02baae2.jpg', 'f60c939ba55c456b3928fa0ee02baae2.jpg_original', 0, 0, '2020-03-11 08:17:33', '2020-05-11 12:19:08'),
(228, 280, '58074e55163970e8e3e55c23bb63aabc.jpg', '58074e55163970e8e3e55c23bb63aabc.jpg_original', 0, 0, '2020-03-11 09:01:56', '2020-05-11 12:19:10'),
(229, 281, 'c2f14246418aa70841ed9b51605ec80c.jpg', 'c2f14246418aa70841ed9b51605ec80c.jpg_original', 0, 0, '2020-03-11 09:03:30', '2020-05-11 12:19:04'),
(230, 282, '75297fa3a99badaba6b0d86422bfbe5c.jpg', '75297fa3a99badaba6b0d86422bfbe5c.jpg_original', 0, 0, '2020-03-11 09:04:42', '2020-05-11 12:18:58'),
(231, 283, '88ee278df6921a3cf4ccbcf7f2cd64f8.jpg', '88ee278df6921a3cf4ccbcf7f2cd64f8.jpg_original', 0, 0, '2020-03-11 09:05:30', '2020-05-11 12:19:37'),
(232, 284, '83459703cd6570626d6ab06eac1b706d.jpg', '83459703cd6570626d6ab06eac1b706d.jpg_original', 0, 0, '2020-03-11 09:06:23', '2020-05-11 12:18:57'),
(233, 285, 'c4b569c7cb61045d9c7dd76b5165c5c1.jpg', 'c4b569c7cb61045d9c7dd76b5165c5c1.jpg_original', 0, 0, '2020-03-11 09:07:13', '2020-05-11 12:18:55'),
(234, 286, '4c5ef2864ca3eb3114dfa06b9926c710.jpg', '4c5ef2864ca3eb3114dfa06b9926c710.jpg_original', 0, 0, '2020-03-11 09:08:02', '2020-05-11 12:18:56'),
(235, 287, 'e79e53e101f71078aa3a1de2985c964b.jpg', 'e79e53e101f71078aa3a1de2985c964b.jpg_original', 0, 0, '2020-03-11 09:09:08', '2020-05-11 12:18:54'),
(236, 288, '49701a13cff09f1022b66906c317c5b1.jpg', '49701a13cff09f1022b66906c317c5b1.jpg_original', 0, 0, '2020-03-11 09:10:01', '2020-05-11 12:15:57'),
(237, 289, '9a6df73e8c865a5754c3da916678b25a.jpg', '9a6df73e8c865a5754c3da916678b25a.jpg_original', 0, 0, '2020-03-11 09:10:55', '2020-05-11 12:18:53'),
(238, 290, '01d8e68183927b8f506b213cde6dbb4e.jpg', '01d8e68183927b8f506b213cde6dbb4e.jpg_original', 0, 0, '2020-03-11 09:12:29', '2020-05-11 12:15:54'),
(239, 291, '48e432e63f8b1e88e55d44940af806a0.jpg', '48e432e63f8b1e88e55d44940af806a0.jpg_original', 0, 0, '2020-03-11 09:13:59', '2020-05-11 12:15:50'),
(240, 292, '0dd3de8e48cb63e10e4b4e71f08ced22.jpg', '0dd3de8e48cb63e10e4b4e71f08ced22.jpg_original', 0, 0, '2020-03-11 09:26:54', '2020-05-11 12:15:42'),
(241, 293, '0cc682d56654856076e98c6b405c6999.jpg', '0cc682d56654856076e98c6b405c6999.jpg_original', 0, 0, '2020-03-11 09:28:09', '2020-05-11 12:15:41'),
(242, 294, '393160d9a505517e333f96bf200d8407.jpg', '393160d9a505517e333f96bf200d8407.jpg_original', 0, 0, '2020-03-11 09:29:09', '2020-05-11 12:15:44'),
(243, 295, '5b96e7ad6c49f10bc7b7a96a3c97361a.jpg', '5b96e7ad6c49f10bc7b7a96a3c97361a.jpg_original', 0, 0, '2020-03-11 09:30:18', '2020-05-11 12:15:35'),
(244, 296, '868d6a1082e2b9acec465a048699c24a.jpg', '868d6a1082e2b9acec465a048699c24a.jpg_original', 0, 0, '2020-03-11 09:32:02', '2020-05-11 12:15:41'),
(245, 297, 'e68e241a9bcb55632596c5db41269f97.jpg', 'e68e241a9bcb55632596c5db41269f97.jpg_original', 0, 0, '2020-03-11 09:47:46', '2020-05-11 12:15:31'),
(246, 298, 'e6ad5d0b80b5efcf5337c62ebd6bf965.jpg', 'e6ad5d0b80b5efcf5337c62ebd6bf965.jpg_original', 0, 0, '2020-03-11 09:48:52', '2020-05-11 12:15:28'),
(247, 299, 'ce84d72dc98c38436ad1b72b34d7f7c2.jpg', 'ce84d72dc98c38436ad1b72b34d7f7c2.jpg_original', 0, 0, '2020-03-11 09:49:36', '2020-05-11 12:15:45'),
(248, 300, '88e0ad2acef9370eab571b94ab284119.jpg', '88e0ad2acef9370eab571b94ab284119.jpg_original', 0, 0, '2020-03-11 09:50:25', '2020-05-11 12:15:24'),
(249, 301, '74705cc6b845d1d89b70a42363bd1a39.jpg', '74705cc6b845d1d89b70a42363bd1a39.jpg_original', 0, 0, '2020-03-11 09:52:05', '2020-05-11 12:15:48'),
(250, 302, '79f72123d33d1bd4d0368dde0b1e4233.jpg', '79f72123d33d1bd4d0368dde0b1e4233.jpg_original', 0, 0, '2020-03-11 09:52:56', '2020-05-11 12:15:46'),
(251, 303, '62dd90ea168c5e1244e11164e73e5f3e.jpg', '62dd90ea168c5e1244e11164e73e5f3e.jpg_original', 0, 0, '2020-03-11 09:53:50', '2020-05-11 12:15:47'),
(252, 304, '966eaab003dfff8ee34ea4186c4b16b4.jpg', '966eaab003dfff8ee34ea4186c4b16b4.jpg_original', 0, 0, '2020-03-11 09:58:27', '2020-05-11 12:10:25'),
(253, 305, '2da974fa5de7407197b94f305dd76bcf.jpg', '2da974fa5de7407197b94f305dd76bcf.jpg_original', 0, 0, '2020-03-11 09:59:28', '2020-05-11 12:15:25'),
(254, 306, '71cd8ef2bb313d0a92f997c886112bd9.jpg', '71cd8ef2bb313d0a92f997c886112bd9.jpg_original', 0, 0, '2020-03-11 10:00:18', '2020-05-11 12:15:36'),
(255, 307, '34a3acc1a8bfe1b463858343f201cf2c.jpg', '34a3acc1a8bfe1b463858343f201cf2c.jpg_original', 0, 0, '2020-03-11 10:01:10', '2020-05-11 12:15:27'),
(256, 308, '747e43af68c15ffb6685981a52aef4e2.jpg', '747e43af68c15ffb6685981a52aef4e2.jpg_original', 0, 0, '2020-03-11 10:01:55', '2020-05-11 12:15:33'),
(257, 309, '3b3d8b5e2d570bb9fa0e3914127253c1.jpg', '3b3d8b5e2d570bb9fa0e3914127253c1.jpg_original', 0, 0, '2020-03-11 10:03:42', '2020-05-11 12:15:34'),
(258, 310, 'ca4772d95f3c97f6f2d82da08adc39f1.jpg', 'ca4772d95f3c97f6f2d82da08adc39f1.jpg_original', 0, 0, '2020-03-11 10:04:35', '2020-05-11 12:15:40'),
(259, 311, '5d69dcc46ca772d75776fe9a505c085c.jpg', '5d69dcc46ca772d75776fe9a505c085c.jpg_original', 0, 0, '2020-03-11 10:07:04', '2020-05-11 12:15:47'),
(260, 312, 'd5345f79ee2d3c0b5e47afe9aee6e555.jpg', 'd5345f79ee2d3c0b5e47afe9aee6e555.jpg_original', 0, 0, '2020-03-11 10:09:24', '2020-05-11 12:15:39'),
(261, 313, 'c452983f07b6322498654e2eada467d6.jpg', 'c452983f07b6322498654e2eada467d6.jpg_original', 0, 0, '2020-03-11 10:10:21', '2020-05-11 12:12:09'),
(262, 314, '138f5d4385c109e7740cba3bd901f990.jpg', '138f5d4385c109e7740cba3bd901f990.jpg_original', 0, 0, '2020-03-11 10:12:04', '2020-05-11 12:15:22'),
(263, 315, '5dcce18457d9e988c0a37e1f2f7d7acc.jpg', '5dcce18457d9e988c0a37e1f2f7d7acc.jpg_original', 0, 0, '2020-03-11 10:23:21', '2020-05-11 12:12:13'),
(264, 316, 'f702e87fabe0950b4317b4e783cd47cf.jpg', 'f702e87fabe0950b4317b4e783cd47cf.jpg_original', 0, 0, '2020-03-11 10:24:20', '2020-05-11 12:15:22'),
(265, 317, '9bd618f5b03e10e06f51ec2c3582afea.jpg', '9bd618f5b03e10e06f51ec2c3582afea.jpg_original', 0, 0, '2020-03-11 10:25:11', '2020-05-11 12:12:12'),
(266, 318, '42bcce5fd9e0f68c31eabfb6425c4f85.jpg', '42bcce5fd9e0f68c31eabfb6425c4f85.jpg_original', 0, 0, '2020-03-11 10:26:02', '2020-05-11 12:12:10'),
(267, 319, 'c5b60fcdcaadc66a118a914f5ae591f5.jpg', 'c5b60fcdcaadc66a118a914f5ae591f5.jpg_original', 0, 0, '2020-03-11 10:26:55', '2020-05-11 12:15:24'),
(268, 320, '10623d2fb8c606af5e3ada0ebf5378ca.jpg', '10623d2fb8c606af5e3ada0ebf5378ca.jpg_original', 0, 0, '2020-03-11 10:27:48', '2020-05-11 12:15:38'),
(269, 321, '32d325835eabb81a51bb321a25176c1e.jpg', '32d325835eabb81a51bb321a25176c1e.jpg_original', 0, 0, '2020-03-11 10:32:36', '2020-05-11 12:12:11'),
(270, 322, 'a80191cb17494df5736272793bdfe471.jpg', 'a80191cb17494df5736272793bdfe471.jpg_original', 0, 0, '2020-03-11 10:34:50', '2020-05-11 12:15:29'),
(271, 323, '811abf6dd2be87fa97a540db3f1e3476.jpg', '811abf6dd2be87fa97a540db3f1e3476.jpg_original', 0, 0, '2020-03-11 10:35:52', '2020-05-11 12:15:30'),
(272, 324, 'ce95e8b94d90d748ffa8e03885f2d1de.jpg', 'ce95e8b94d90d748ffa8e03885f2d1de.jpg_original', 0, 0, '2020-03-11 10:36:51', '2020-05-11 12:15:26'),
(273, 325, '9f62470b4195e72bba1a4a9594b3d028.jpg', '9f62470b4195e72bba1a4a9594b3d028.jpg_original', 0, 0, '2020-03-11 10:38:06', '2020-05-11 12:15:37'),
(274, 326, 'e26bc2a48abb35afd45f263bf290f1c1.jpg', 'e26bc2a48abb35afd45f263bf290f1c1.jpg_original', 0, 0, '2020-03-11 10:45:07', '2020-05-11 12:12:04'),
(275, 327, '6b97d92f2cdde569d8ec9a40bf84967c.jpg', '6b97d92f2cdde569d8ec9a40bf84967c.jpg_original', 0, 0, '2020-03-11 10:46:07', '2020-05-11 12:12:03'),
(276, 328, '16a536a75382ccb2bcbf252356811f78.jpg', '16a536a75382ccb2bcbf252356811f78.jpg_original', 0, 0, '2020-03-11 10:46:48', '2020-05-11 12:12:02'),
(277, 329, 'ab007ae45ec11157830c97fd70d7c4e4.jpg', 'ab007ae45ec11157830c97fd70d7c4e4.jpg_original', 0, 0, '2020-03-11 10:47:36', '2020-05-11 12:14:45'),
(278, 330, '98b34dcca1fe3595ca02f6bda2395437.jpg', '98b34dcca1fe3595ca02f6bda2395437.jpg_original', 0, 0, '2020-03-11 10:48:18', '2020-05-11 12:14:44'),
(279, 331, '58dc2092008bde822afb270a222768fa.jpg', '58dc2092008bde822afb270a222768fa.jpg_original', 0, 0, '2020-03-11 10:49:12', '2020-05-11 12:14:40'),
(280, 332, '4c058b8050fc25d4319b8832f120d31b.jpg', '4c058b8050fc25d4319b8832f120d31b.jpg_original', 0, 0, '2020-03-11 11:05:21', '2020-05-11 12:14:39'),
(281, 333, '1826618692844e08fc5c0514147e86f1.jpg', '1826618692844e08fc5c0514147e86f1.jpg_original', 0, 0, '2020-03-11 11:06:16', '2020-05-11 12:14:42'),
(282, 334, '088b3373a1524dc1191dcd857cbab82b.jpg', '088b3373a1524dc1191dcd857cbab82b.jpg_original', 0, 0, '2020-03-11 11:08:14', '2020-05-11 12:14:43'),
(283, 335, '18511bbb675d3835958c22f787c30c72.jpg', '18511bbb675d3835958c22f787c30c72.jpg_original', 0, 0, '2020-03-11 11:09:08', '2020-05-11 12:14:41'),
(284, 336, 'b660e0d568cf8bf625268242ac4d6896.jpg', 'b660e0d568cf8bf625268242ac4d6896.jpg_original', 0, 0, '2020-03-11 11:10:12', '2020-05-11 12:14:21'),
(285, 337, 'dd4a090962391cabe7d0c303fd53870c.jpg', 'dd4a090962391cabe7d0c303fd53870c.jpg_original', 0, 0, '2020-03-11 11:11:00', '2020-05-11 12:13:48'),
(286, 338, '78b7fd8b77cbbdacd7d6e86d02805f3b.jpg', '78b7fd8b77cbbdacd7d6e86d02805f3b.jpg_original', 0, 0, '2020-03-11 11:13:08', '2020-05-11 12:13:47'),
(287, 339, '445a4cbee6336d8e8fc3b2bf7a1a379f.jpg', '445a4cbee6336d8e8fc3b2bf7a1a379f.jpg_original', 0, 0, '2020-03-11 11:15:19', '2020-05-11 12:14:38'),
(288, 340, 'fab860c2c988c7aa4fe903640c1e4e00.jpg', 'fab860c2c988c7aa4fe903640c1e4e00.jpg_original', 0, 0, '2020-03-11 11:17:19', '2020-05-11 12:14:34'),
(289, 341, 'f26c22ad5be66fa5f682cb477b73ef77.jpg', 'f26c22ad5be66fa5f682cb477b73ef77.jpg_original', 0, 0, '2020-03-11 11:18:24', '2020-05-11 12:14:33'),
(290, 342, 'b335fb4bffd8870a1c62fc338db10e22.jpg', 'b335fb4bffd8870a1c62fc338db10e22.jpg_original', 0, 0, '2020-03-11 11:19:15', '2020-05-11 12:14:35'),
(291, 343, '08ac076c5b9472914ad9a1a7e213cf32.jpg', '08ac076c5b9472914ad9a1a7e213cf32.jpg_original', 0, 0, '2020-03-11 11:20:24', '2020-05-11 12:14:32'),
(292, 344, '8b901a4952c71e60eedb45f785bd9b33.jpg', '8b901a4952c71e60eedb45f785bd9b33.jpg_original', 0, 0, '2020-03-11 11:22:45', '2020-05-11 12:14:21'),
(293, 345, 'b1e0caed37334bf084427090a3d39bf9.jpg', 'b1e0caed37334bf084427090a3d39bf9.jpg_original', 0, 0, '2020-03-11 11:23:35', '2020-05-11 12:14:20'),
(294, 346, 'd69b7a2fa98551069976bf10e192692f.jpg', 'd69b7a2fa98551069976bf10e192692f.jpg_original', 0, 0, '2020-03-11 11:24:30', '2020-05-11 12:14:00'),
(295, 347, '918c24d1739d430f05f8379d2c365648.jpg', '918c24d1739d430f05f8379d2c365648.jpg_original', 0, 0, '2020-03-11 11:28:07', '2020-05-11 12:14:22'),
(296, 348, '1a9b0e59a7ef9ca5ffbd9098cb522499.jpg', '1a9b0e59a7ef9ca5ffbd9098cb522499.jpg_original', 0, 0, '2020-03-11 11:29:05', '2020-05-11 12:14:23'),
(297, 349, '5ce38034685e1ade3917c3024490aa21.jpg', '5ce38034685e1ade3917c3024490aa21.jpg_original', 0, 0, '2020-03-11 11:30:18', '2020-05-11 12:13:59'),
(298, 350, '7090a60f1be2cb0366146e9dadd39f31.jpg', '7090a60f1be2cb0366146e9dadd39f31.jpg_original', 0, 0, '2020-03-11 11:31:26', '2020-05-11 12:13:57'),
(299, 351, '5f8e7f01b65aabe5bc5122a3d8239121.jpg', '5f8e7f01b65aabe5bc5122a3d8239121.jpg_original', 0, 0, '2020-03-11 11:32:36', '2020-03-11 11:32:47'),
(300, 351, 'a787fe51557b7b2417ed4cdf0a88f881.jpg', 'a787fe51557b7b2417ed4cdf0a88f881.jpg_original', 0, 0, '2020-03-11 11:32:55', '2020-05-11 12:14:24'),
(301, 352, 'ee36290ae73606d62d30a172d410cf10.jpg', 'ee36290ae73606d62d30a172d410cf10.jpg_original', 0, 0, '2020-03-11 11:33:40', '2020-05-11 12:14:25'),
(302, 353, '93369f22cfc769f6c9d7ffeb509fa332.jpg', '93369f22cfc769f6c9d7ffeb509fa332.jpg_original', 0, 0, '2020-03-11 11:34:24', '2020-05-11 12:14:29'),
(303, 354, '40585d93af9e0ba178f2958af52e9dc6.jpg', '40585d93af9e0ba178f2958af52e9dc6.jpg_original', 0, 0, '2020-03-11 11:35:19', '2020-05-11 12:14:28'),
(304, 355, 'fd9c2e55ab3121c3bbec64c77bf0378d.jpg', 'fd9c2e55ab3121c3bbec64c77bf0378d.jpg_original', 0, 0, '2020-03-11 11:36:03', '2020-05-11 12:14:30'),
(305, 356, 'fc875c1a1d6086bf97ff7de5fef1d685.jpg', 'fc875c1a1d6086bf97ff7de5fef1d685.jpg_original', 0, 0, '2020-03-11 11:36:47', '2020-05-11 12:14:27'),
(306, 357, 'cf351adc329d761849c703e22a4a7809.jpg', 'cf351adc329d761849c703e22a4a7809.jpg_original', 0, 0, '2020-03-11 12:16:40', '2020-05-11 12:14:26'),
(307, 358, '01d76f5fbbec24cabe98c4c83b665515.jpg', '01d76f5fbbec24cabe98c4c83b665515.jpg_original', 0, 0, '2020-03-11 12:17:35', '2020-05-11 12:14:30'),
(308, 359, 'd9f2bb8accdbceef7077df4279f8215b.jpg', 'd9f2bb8accdbceef7077df4279f8215b.jpg_original', 0, 0, '2020-03-11 12:18:53', '2020-05-11 12:14:17'),
(309, 360, '8dfee3d7eea43d56e6de57fea8644170.jpg', '8dfee3d7eea43d56e6de57fea8644170.jpg_original', 0, 0, '2020-03-11 12:20:06', '2020-05-11 12:14:15'),
(310, 362, '6d14b58135dd65071dbfa57244861671.jpg', '6d14b58135dd65071dbfa57244861671.jpg_original', 0, 0, '2020-03-11 12:22:38', '2020-05-11 12:14:14'),
(311, 363, '45ed1e3ba965d11979479e9eb3d4ca0c.jpg', '45ed1e3ba965d11979479e9eb3d4ca0c.jpg_original', 0, 0, '2020-03-12 04:21:01', '2020-05-11 12:14:13'),
(312, 364, '8da4ffdbdd123abf16b6cdc8473d9f5e.jpg', '8da4ffdbdd123abf16b6cdc8473d9f5e.jpg_original', 0, 0, '2020-03-12 04:21:38', '2020-05-11 12:14:12'),
(313, 365, '977d308f1a326e73733185706c36cbfb.jpg', '977d308f1a326e73733185706c36cbfb.jpg_original', 0, 0, '2020-03-12 04:22:21', '2020-05-11 12:14:11'),
(314, 366, '224519dfea80350a353cdc204612250f.jpg', '224519dfea80350a353cdc204612250f.jpg_original', 0, 0, '2020-03-12 04:22:51', '2020-05-11 12:14:10'),
(315, 367, '1c934e23d1b5613b7419d3bff86fcb90.jpg', '1c934e23d1b5613b7419d3bff86fcb90.jpg_original', 0, 0, '2020-03-12 04:23:45', '2020-05-11 12:14:08'),
(316, 368, 'eb2a5f7d6300ca5918334d90e9b0ef72.jpg', 'eb2a5f7d6300ca5918334d90e9b0ef72.jpg_original', 0, 0, '2020-03-12 04:24:31', '2020-05-11 12:14:07'),
(317, 369, '8855ceedc9eb6e8e7bca9c480dfa9a43.jpg', '8855ceedc9eb6e8e7bca9c480dfa9a43.jpg_original', 0, 0, '2020-03-12 04:25:19', '2020-05-11 12:14:06'),
(318, 370, '01ee1699c63e9833078958ad36753888.jpg', '01ee1699c63e9833078958ad36753888.jpg_original', 0, 0, '2020-03-12 04:26:04', '2020-05-11 12:14:05'),
(319, 371, 'b998a89b2e4f68d261265600a02caad9.jpg', 'b998a89b2e4f68d261265600a02caad9.jpg_original', 0, 0, '2020-03-12 04:26:32', '2020-05-11 12:14:04'),
(320, 372, '29f3274fc59d1a7244c4ede336aa4f42.jpg', '29f3274fc59d1a7244c4ede336aa4f42.jpg_original', 0, 0, '2020-03-12 04:27:17', '2020-05-11 12:14:02'),
(321, 373, '22da96eb74484b26863aa8e30c8a36a1.jpg', '22da96eb74484b26863aa8e30c8a36a1.jpg_original', 0, 0, '2020-03-12 04:27:44', '2020-05-11 12:14:01'),
(322, 374, '37cc84dffe70193ca366b0a03d68b049.jpg', '37cc84dffe70193ca366b0a03d68b049.jpg_original', 0, 0, '2020-03-12 04:28:57', '2020-05-11 12:12:08'),
(323, 375, 'c9c039f1cd31d748cef3972cf6262f7b.jpg', 'c9c039f1cd31d748cef3972cf6262f7b.jpg_original', 0, 0, '2020-03-12 04:30:01', '2020-05-11 12:12:06'),
(324, 376, '5322a4ed8176024c1768bd7ae680a214.jpg', '5322a4ed8176024c1768bd7ae680a214.jpg_original', 0, 0, '2020-03-12 04:30:39', '2020-05-11 12:12:05'),
(325, 377, '4b224d9a2e6d58b8225bbaf2c06d4fea.jpg', '4b224d9a2e6d58b8225bbaf2c06d4fea.jpg_original', 0, 0, '2020-03-12 04:31:34', '2020-05-11 12:14:31'),
(326, 378, '45a21c3c4ca13c2e77ab3ed7bdfe6f5a.jpg', '45a21c3c4ca13c2e77ab3ed7bdfe6f5a.jpg_original', 0, 0, '2020-03-12 04:32:48', '2020-05-11 12:13:53'),
(327, 379, '88b4d3a9bc3f4d13d0d452848f026e5b.jpg', '88b4d3a9bc3f4d13d0d452848f026e5b.jpg_original', 0, 0, '2020-03-12 04:33:58', '2020-05-11 12:13:52'),
(328, 380, '934cec20ea38929e479f84f407f3e932.jpg', '934cec20ea38929e479f84f407f3e932.jpg_original', 0, 0, '2020-03-12 04:35:23', '2020-05-11 12:12:07'),
(329, 381, '95bf1f27cbf38fe1467ceb394f66ea3f.jpg', '95bf1f27cbf38fe1467ceb394f66ea3f.jpg_original', 0, 0, '2020-03-12 04:37:00', '2020-05-11 12:14:32'),
(330, 382, '0c55fac7a150c8730cf894f66fbafd8a.jpg', '0c55fac7a150c8730cf894f66fbafd8a.jpg_original', 0, 0, '2020-03-12 04:37:47', '2020-05-11 12:14:18'),
(331, 383, '476b98e1a77e781e5c9a928a8bc89fe7.jpg', '476b98e1a77e781e5c9a928a8bc89fe7.jpg_original', 0, 0, '2020-03-12 04:45:19', '2020-05-11 12:14:19');
INSERT INTO `product_files` (`id`, `product_id`, `file_name`, `original_file_name`, `is_cover`, `state_id`, `created_at`, `updated_at`) VALUES
(332, 384, 'a5bbbd87eb0036c79495c2b5341e3165.jpg', 'a5bbbd87eb0036c79495c2b5341e3165.jpg_original', 0, 0, '2020-03-12 04:47:24', '2020-05-11 11:52:36'),
(333, 385, 'fd31e94597c4daf2ba8fc4603ab428ff.jpg', 'fd31e94597c4daf2ba8fc4603ab428ff.jpg_original', 0, 0, '2020-03-12 04:48:17', '2020-05-11 11:52:35'),
(334, 386, '30660ccae347da46c18ff3e78f79c60a.jpg', '30660ccae347da46c18ff3e78f79c60a.jpg_original', 0, 0, '2020-03-12 04:49:09', '2020-05-11 11:52:37'),
(335, 387, 'e25385371a4c24b8dc1cf4bbf8554d29.jpg', 'e25385371a4c24b8dc1cf4bbf8554d29.jpg_original', 0, 0, '2020-03-12 04:50:23', '2020-05-11 12:14:18'),
(336, 388, 'a735202ef922b59778286a3f9ec81cb3.jpg', 'a735202ef922b59778286a3f9ec81cb3.jpg_original', 0, 0, '2020-03-12 04:53:28', '2020-05-11 12:10:57'),
(337, 389, '2a0a00d2614cc180815e06e51383adb0.jpg', '2a0a00d2614cc180815e06e51383adb0.jpg_original', 0, 0, '2020-03-12 04:54:14', '2020-05-11 12:10:55'),
(338, 390, '1797bc0a40539569712a76ed30be3543.jpg', '1797bc0a40539569712a76ed30be3543.jpg_original', 0, 0, '2020-03-12 04:55:13', '2020-05-11 12:10:54'),
(339, 391, '0c51bc8749b3a779acb53b14ae1e60f7.jpg', '0c51bc8749b3a779acb53b14ae1e60f7.jpg_original', 0, 0, '2020-03-12 04:55:53', '2020-05-11 12:10:53'),
(340, 392, 'bd5caa1a79b88163657923d2866f8732.jpg', 'bd5caa1a79b88163657923d2866f8732.jpg_original', 0, 0, '2020-03-12 04:56:30', '2020-05-11 12:10:48'),
(341, 393, 'c3d6b56d549c3cfea4f5b970631241d3.jpg', 'c3d6b56d549c3cfea4f5b970631241d3.jpg_original', 0, 0, '2020-03-12 04:57:00', '2020-05-11 12:10:47'),
(342, 394, '447fe69eb9010ac340f7812f1582c9b0.jpg', '447fe69eb9010ac340f7812f1582c9b0.jpg_original', 0, 0, '2020-03-12 04:59:21', '2020-05-11 12:10:45'),
(343, 395, '04e14be1295519f086a9ed97ac3a354a.jpg', '04e14be1295519f086a9ed97ac3a354a.jpg_original', 0, 0, '2020-03-12 04:59:53', '2020-05-11 12:10:44'),
(344, 396, 'd3d89ff8c35b0000cfa03f278c4eb427.jpg', 'd3d89ff8c35b0000cfa03f278c4eb427.jpg_original', 0, 0, '2020-03-12 05:00:17', '2020-05-11 12:10:43'),
(345, 397, 'd0039987b2e092a97e0fbdd7eabac3fa.jpg', 'd0039987b2e092a97e0fbdd7eabac3fa.jpg_original', 0, 0, '2020-03-12 05:00:46', '2020-05-11 12:10:40'),
(346, 398, 'ce0d3782d80163802dea8a6ddf0d282b.jpg', 'ce0d3782d80163802dea8a6ddf0d282b.jpg_original', 0, 0, '2020-03-12 05:01:34', '2020-05-11 12:10:36'),
(347, 399, '7b1edb4a4aa765efb713b4ee1ba05822.jpg', '7b1edb4a4aa765efb713b4ee1ba05822.jpg_original', 0, 0, '2020-03-12 05:02:23', '2020-05-11 12:10:36'),
(348, 400, '59f5dbcd3f206f4977a691626b2c5008.jpg', '59f5dbcd3f206f4977a691626b2c5008.jpg_original', 0, 0, '2020-03-12 05:04:00', '2020-05-11 12:10:41'),
(349, 401, '191cb0744af8a9befdcdc53e23b0c1d7.jpg', '191cb0744af8a9befdcdc53e23b0c1d7.jpg_original', 0, 0, '2020-03-12 05:04:54', '2020-05-11 12:10:46'),
(350, 402, 'af2a13a815306561881f6bb38ee2a18c.jpg', 'af2a13a815306561881f6bb38ee2a18c.jpg_original', 0, 0, '2020-03-12 05:05:30', '2020-05-11 12:10:42'),
(351, 403, '8ca62cffb53934e01dbc08a54efec39d.jpg', '8ca62cffb53934e01dbc08a54efec39d.jpg_original', 0, 0, '2020-03-12 05:09:43', '2020-05-11 12:19:47'),
(352, 404, '96a0ef24954ca052abf3f9cf8d28cebc.jpg', '96a0ef24954ca052abf3f9cf8d28cebc.jpg_original', 0, 0, '2020-03-12 05:10:35', '2020-05-11 12:10:20'),
(353, 405, 'edc79f6e8094750a6be06112d8eb15f4.jpg', 'edc79f6e8094750a6be06112d8eb15f4.jpg_original', 0, 0, '2020-03-12 05:11:53', '2020-05-11 12:19:48'),
(354, 406, '6b0342449f9d21c4f99bffb3cb52960f.jpg', '6b0342449f9d21c4f99bffb3cb52960f.jpg_original', 0, 0, '2020-03-12 05:12:59', '2020-05-11 11:52:45'),
(355, 407, 'cfe5bad34d065746e8376af765bd117a.jpg', 'cfe5bad34d065746e8376af765bd117a.jpg_original', 0, 0, '2020-03-12 05:13:51', '2020-05-11 11:52:50'),
(356, 408, 'e1b0ea73b895a6b07d33754e7bcb48d9.jpg', 'e1b0ea73b895a6b07d33754e7bcb48d9.jpg_original', 0, 0, '2020-03-12 05:14:35', '2020-05-11 12:02:47'),
(357, 409, 'd0e38333739421012831f2a54cab5db9.jpg', 'd0e38333739421012831f2a54cab5db9.jpg_original', 0, 0, '2020-03-12 05:15:50', '2020-05-11 11:48:56'),
(358, 410, 'bb1efe87eebe1eb9ab2658a44127bd12.jpg', 'bb1efe87eebe1eb9ab2658a44127bd12.jpg_original', 0, 0, '2020-03-12 06:17:12', '2020-05-11 12:13:46'),
(359, 411, '70910642a9f6393fd165349797d54dad.jpg', '70910642a9f6393fd165349797d54dad.jpg_original', 0, 0, '2020-03-12 06:18:47', '2020-05-11 12:13:38'),
(360, 412, 'd272d198ac82e5fc2c9a5705d99f80c2.jpg', 'd272d198ac82e5fc2c9a5705d99f80c2.jpg_original', 0, 0, '2020-03-12 06:19:23', '2020-05-11 12:13:37'),
(361, 413, '5522a61afb797b99a91b22e581c232ec.jpg', '5522a61afb797b99a91b22e581c232ec.jpg_original', 0, 0, '2020-03-12 06:20:07', '2020-05-11 12:13:29'),
(362, 414, '765a5d5cbf9be10406e6b3979c97bfcf.jpg', '765a5d5cbf9be10406e6b3979c97bfcf.jpg_original', 0, 0, '2020-03-12 06:20:52', '2020-05-11 12:13:26'),
(363, 415, 'b99179837d2154ddc321c9cad84ca540.jpg', 'b99179837d2154ddc321c9cad84ca540.jpg_original', 0, 0, '2020-03-12 06:21:26', '2020-05-11 12:12:54'),
(364, 416, 'e4ac5a60b4e5b47b0537140103c28f0a.jpg', 'e4ac5a60b4e5b47b0537140103c28f0a.jpg_original', 0, 0, '2020-03-12 06:21:54', '2020-05-11 12:12:53'),
(365, 417, 'd533c8b2107d71a03420e1bd81377adb.jpg', 'd533c8b2107d71a03420e1bd81377adb.jpg_original', 0, 0, '2020-03-12 06:22:48', '2020-05-11 12:12:49'),
(366, 418, '1ce3cf2bbabd57e92c85e3a10e76d1b3.jpg', '1ce3cf2bbabd57e92c85e3a10e76d1b3.jpg_original', 0, 0, '2020-03-12 07:23:06', '2020-05-11 12:12:48'),
(367, 419, '74a692e959f272b44761dcfbe0cb2f8c.jpg', '74a692e959f272b44761dcfbe0cb2f8c.jpg_original', 0, 0, '2020-03-12 07:29:03', '2020-05-11 12:12:51'),
(368, 420, '33e34a7d1a923a6128b588cb16fce648.jpg', '33e34a7d1a923a6128b588cb16fce648.jpg_original', 0, 0, '2020-03-12 07:29:48', '2020-05-11 12:12:55'),
(369, 421, '6d4b27945625d1df64d90b3c12d5ff02.jpg', '6d4b27945625d1df64d90b3c12d5ff02.jpg_original', 0, 0, '2020-03-12 07:30:43', '2020-05-11 12:12:56'),
(370, 422, '1bd3dca0d204e860d5fe51c023c5eb79.jpg', '1bd3dca0d204e860d5fe51c023c5eb79.jpg_original', 0, 0, '2020-03-12 07:31:13', '2020-05-11 12:12:58'),
(371, 423, '7119b105a8a2ad50cd4bfe294b7957ef.jpg', '7119b105a8a2ad50cd4bfe294b7957ef.jpg_original', 0, 0, '2020-03-12 07:32:10', '2020-05-11 12:12:50'),
(372, 424, '6e2bc1b3d08b6fed9d08ec02ea578d81.jpg', '6e2bc1b3d08b6fed9d08ec02ea578d81.jpg_original', 0, 0, '2020-03-12 07:33:42', '2020-05-11 12:10:23'),
(373, 425, '2f73c6a08e560611b1e5016f933a7cdd.jpg', '2f73c6a08e560611b1e5016f933a7cdd.jpg_original', 0, 0, '2020-03-12 07:34:12', '2020-05-11 12:10:21'),
(374, 426, 'b122659a52ded7ff94a10704758e8c56.jpg', 'b122659a52ded7ff94a10704758e8c56.jpg_original', 0, 0, '2020-03-12 07:34:48', '2020-05-11 12:11:21'),
(375, 427, 'e42bf10df3df6fabdf71181c186bda0d.jpg', 'e42bf10df3df6fabdf71181c186bda0d.jpg_original', 0, 0, '2020-03-12 07:35:53', '2020-05-11 12:11:13'),
(376, 428, 'db418070bc43262634fa24884fddb9d9.jpg', 'db418070bc43262634fa24884fddb9d9.jpg_original', 0, 0, '2020-03-12 07:36:37', '2020-05-11 12:11:12'),
(377, 429, 'f27b7e38edd6575de79368d56ac62e39.jpg', 'f27b7e38edd6575de79368d56ac62e39.jpg_original', 0, 0, '2020-03-12 07:37:25', '2020-05-11 12:11:10'),
(378, 430, '11577d47578c1f1872b0f88c16653a7d.jpg', '11577d47578c1f1872b0f88c16653a7d.jpg_original', 0, 0, '2020-03-12 08:31:14', '2020-05-11 12:11:09'),
(379, 431, '90b9235e7145c86dd1bd16b210720f2f.jpg', '90b9235e7145c86dd1bd16b210720f2f.jpg_original', 0, 0, '2020-03-12 08:31:51', '2020-05-11 12:11:06'),
(380, 432, 'ecd8b44ecd4dacd6b8d67f9de236a862.jpg', 'ecd8b44ecd4dacd6b8d67f9de236a862.jpg_original', 0, 0, '2020-03-12 08:32:24', '2020-05-11 12:11:05'),
(381, 433, '4cabaac898c5ea8fa460ab8c0e8c0e81.jpg', '4cabaac898c5ea8fa460ab8c0e8c0e81.jpg_original', 0, 0, '2020-03-12 08:33:04', '2020-05-11 12:11:17'),
(382, 434, '452c1136e025000f4819f6cb6962bfbe.jpg', '452c1136e025000f4819f6cb6962bfbe.jpg_original', 0, 0, '2020-03-12 08:57:54', '2020-05-11 12:11:16'),
(383, 435, 'a7cdc129b6564ae13f4bd44036f9fb92.jpg', 'a7cdc129b6564ae13f4bd44036f9fb92.jpg_original', 0, 0, '2020-03-12 08:58:43', '2020-05-11 12:10:58'),
(384, 436, 'b83fddc6ad63cacb7ce681cefb2991bf.jpg', 'b83fddc6ad63cacb7ce681cefb2991bf.jpg_original', 0, 0, '2020-03-12 08:59:29', '2020-05-11 12:10:52'),
(385, 437, '0fe1ff08834fae87ffc2f7ec6f90da30.jpg', '0fe1ff08834fae87ffc2f7ec6f90da30.jpg_original', 0, 0, '2020-03-12 09:00:18', '2020-05-11 12:10:50'),
(386, 438, '8fe1bb1e49e05c30f7d72c1ffd67c6a0.jpg', '8fe1bb1e49e05c30f7d72c1ffd67c6a0.jpg_original', 0, 0, '2020-03-12 09:00:55', '2020-05-11 12:10:38'),
(387, 439, '0122bcd60bd751a1bb66281850970ec9.jpg', '0122bcd60bd751a1bb66281850970ec9.jpg_original', 0, 0, '2020-03-12 09:04:19', '2020-05-11 12:10:22'),
(388, 440, '421fdafb4b78e6239678ceceba881a53.jpg', '421fdafb4b78e6239678ceceba881a53.jpg_original', 0, 0, '2020-03-12 11:33:42', '2020-05-11 12:11:14'),
(389, 441, 'e370f8b5fa89e26e3a3d9175190b576f.jpg', 'e370f8b5fa89e26e3a3d9175190b576f.jpg_original', 0, 0, '2020-03-12 11:35:28', '2020-05-11 12:11:11'),
(390, 442, 'bf677656c82cb2d06f1d99b46d6f099d.jpg', 'bf677656c82cb2d06f1d99b46d6f099d.jpg_original', 0, 0, '2020-03-12 11:36:14', '2020-05-11 12:11:07'),
(391, 443, 'be93daef6461c42d1b6879f2a7e6d294.jpg', 'be93daef6461c42d1b6879f2a7e6d294.jpg_original', 0, 0, '2020-03-12 11:37:01', '2020-05-11 12:11:04'),
(392, 444, '63e1a5adfd6550e16dcf2e697e8e616e.jpg', '63e1a5adfd6550e16dcf2e697e8e616e.jpg_original', 0, 0, '2020-03-12 11:38:36', '2020-05-11 12:11:19'),
(393, 445, 'be85ab4c183a8d0186b95324970c7c5d.jpg', 'be85ab4c183a8d0186b95324970c7c5d.jpg_original', 0, 0, '2020-03-12 11:39:23', '2020-05-11 12:10:51'),
(394, 446, '6c8df9b96e6d29b31a7f87eda776feb5.jpg', '6c8df9b96e6d29b31a7f87eda776feb5.jpg_original', 0, 0, '2020-03-12 11:40:25', '2020-05-11 12:10:39'),
(395, 447, 'b6cff42036b14233d9d1145b3f4f73f6.jpg', 'b6cff42036b14233d9d1145b3f4f73f6.jpg_original', 0, 0, '2020-03-12 11:41:09', '2020-05-11 12:19:49'),
(396, 448, 'fe24b3c4b1f8348d50ebf7cc19806f59.jpg', 'fe24b3c4b1f8348d50ebf7cc19806f59.jpg_original', 0, 0, '2020-03-13 04:58:30', '2020-05-11 12:11:02'),
(397, 449, 'e02aa18d6c65c9b2697942830b6bd1ba.jpg', 'e02aa18d6c65c9b2697942830b6bd1ba.jpg_original', 0, 0, '2020-03-13 04:59:28', '2020-05-11 12:10:49'),
(398, 450, 'ee932343abf00df405ba63c06cd739ba.jpg', 'ee932343abf00df405ba63c06cd739ba.jpg_original', 0, 0, '2020-03-13 05:00:02', '2020-05-11 12:10:38'),
(399, 451, '1cccbe5718376fe2eab62fd26a5b9c19.jpg', '1cccbe5718376fe2eab62fd26a5b9c19.jpg_original', 0, 0, '2020-03-13 05:00:36', '2020-05-11 12:19:47'),
(400, 452, '11db9c81de61e04e2d9ad5421e7945ee.jpg', '11db9c81de61e04e2d9ad5421e7945ee.jpg_original', 0, 0, '2020-03-13 05:01:09', '2020-05-11 12:10:17'),
(401, 453, 'af9a7854d8731f0083765b701cad0bb0.jpg', 'af9a7854d8731f0083765b701cad0bb0.jpg_original', 0, 0, '2020-03-13 05:01:49', '2020-05-11 12:11:01'),
(402, 454, '3f848187620b6654f2feb4fe8944902e.jpg', '3f848187620b6654f2feb4fe8944902e.jpg_original', 0, 0, '2020-03-13 07:04:18', '2020-05-11 12:10:33'),
(403, 455, '1889a27107912a06b8bb1191c4a854ce.jpg', '1889a27107912a06b8bb1191c4a854ce.jpg_original', 0, 0, '2020-03-13 10:21:52', '2020-05-11 12:19:46'),
(404, 456, 'a3d9db824c57658425b45fa07e01795f.jpg', 'a3d9db824c57658425b45fa07e01795f.jpg_original', 0, 0, '2020-03-13 10:22:19', '2020-05-11 12:10:16'),
(405, 457, 'af2e91bd2ac6d0b3e7ebb147c01e47bc.jpg', 'af2e91bd2ac6d0b3e7ebb147c01e47bc.jpg_original', 0, 0, '2020-03-13 10:22:59', '2020-05-11 12:11:24'),
(406, 458, '34565ce909997d276996cafd7303c2a1.jpg', '34565ce909997d276996cafd7303c2a1.jpg_original', 0, 0, '2020-03-14 04:28:39', '2020-05-11 12:10:35'),
(407, 459, '92904a4b2f910ac0fea59437f03aa531.jpg', '92904a4b2f910ac0fea59437f03aa531.jpg_original', 0, 0, '2020-03-14 04:29:13', '2020-05-11 12:19:46'),
(408, 460, '4d6d33c14d709919bf77affb36ac958d.jpg', '4d6d33c14d709919bf77affb36ac958d.jpg_original', 0, 0, '2020-03-14 04:29:38', '2020-05-11 12:10:16'),
(409, 461, '478de319e2dfbc5abf60bb86d5b42743.jpg', '478de319e2dfbc5abf60bb86d5b42743.jpg_original', 0, 0, '2020-03-14 04:40:37', '2020-05-11 12:11:25'),
(410, 462, '1a61585e6417e9d6b0c270a93f785149.jpg', '1a61585e6417e9d6b0c270a93f785149.jpg_original', 0, 0, '2020-03-14 05:17:51', '2020-05-11 12:19:50'),
(411, 463, '038a56cad4e2e74cdf7fde41a463e167.jpg', '038a56cad4e2e74cdf7fde41a463e167.jpg_original', 0, 0, '2020-03-14 05:18:15', '2020-05-11 12:12:20'),
(412, 464, '7d910206a33312d7d97008cd414c7ddd.jpg', '7d910206a33312d7d97008cd414c7ddd.jpg_original', 0, 0, '2020-03-14 05:18:42', '2020-05-11 12:10:15'),
(413, 465, '9c29dee06c059c8c1c9c64a96d262822.jpg', '9c29dee06c059c8c1c9c64a96d262822.jpg_original', 0, 0, '2020-03-14 05:19:07', '2020-05-11 12:11:23'),
(414, 466, 'f1eb68beba6ccf7d7af505be7c2a9b2c.jpg', 'f1eb68beba6ccf7d7af505be7c2a9b2c.jpg_original', 0, 0, '2020-03-14 05:20:17', '2020-05-11 11:49:25'),
(415, 467, '5c763d4f4bca77e56bef877331b37e1f.jpg', '5c763d4f4bca77e56bef877331b37e1f.jpg_original', 0, 0, '2020-03-14 05:20:48', '2020-05-11 11:49:23'),
(416, 468, 'b0cdcab22aa21e0d72e72835807342aa.jpg', 'b0cdcab22aa21e0d72e72835807342aa.jpg_original', 0, 0, '2020-03-14 05:21:17', '2020-05-11 11:49:27'),
(417, 469, 'b4881957a37490251d03df684bcd0ef8.jpg', 'b4881957a37490251d03df684bcd0ef8.jpg_original', 0, 0, '2020-03-14 05:21:44', '2020-05-11 11:49:25'),
(418, 470, 'a26457bc3d34f1cf6bb227b8e7502610.jpg', 'a26457bc3d34f1cf6bb227b8e7502610.jpg_original', 0, 0, '2020-03-14 06:28:31', '2020-05-11 11:49:38'),
(419, 471, '0dfb0c506f483c335ed462b4f41700a1.jpg', '0dfb0c506f483c335ed462b4f41700a1.jpg_original', 0, 0, '2020-03-14 06:29:13', '2020-05-11 11:49:42'),
(420, 472, 'cf628fae106e5756f0f589f53dfc5858.jpg', 'cf628fae106e5756f0f589f53dfc5858.jpg_original', 0, 0, '2020-03-14 06:29:55', '2020-05-11 11:49:40'),
(421, 473, 'b892b2e1e939714bcf036e5604152873.jpg', 'b892b2e1e939714bcf036e5604152873.jpg_original', 0, 0, '2020-03-14 06:31:00', '2020-05-11 11:49:41'),
(422, 474, '97474c424a547e0d2ab95dce38d9ce74.jpg', '97474c424a547e0d2ab95dce38d9ce74.jpg_original', 0, 0, '2020-03-14 06:32:04', '2020-05-11 11:49:39'),
(423, 475, '740fb20101863e91cc269a4132be2a1a.jpg', '740fb20101863e91cc269a4132be2a1a.jpg_original', 0, 0, '2020-03-14 06:33:50', '2020-05-11 12:13:28'),
(424, 476, '9b1364ea0f33e65a2e631fcb38685967.jpg', '9b1364ea0f33e65a2e631fcb38685967.jpg_original', 0, 0, '2020-03-14 06:34:53', '2020-05-11 12:13:39'),
(425, 477, 'e299129753f1b9b50e945d93fc35437c.jpg', 'e299129753f1b9b50e945d93fc35437c.jpg_original', 0, 0, '2020-03-14 06:35:41', '2020-05-11 12:13:38'),
(426, 478, '4575df339f04eebd3df9bc7dc5fb4574.jpg', '4575df339f04eebd3df9bc7dc5fb4574.jpg_original', 0, 0, '2020-03-14 06:38:36', '2020-05-11 12:13:36'),
(427, 479, '634a5f708dfdccfff468aa9fe770fdd6.jpg', '634a5f708dfdccfff468aa9fe770fdd6.jpg_original', 0, 0, '2020-03-14 06:44:02', '2020-05-11 12:13:34'),
(428, 480, 'd59f4faf03973238f1780ba5b61a2e30.jpg', 'd59f4faf03973238f1780ba5b61a2e30.jpg_original', 0, 0, '2020-03-14 07:33:54', '2020-05-11 12:13:35'),
(429, 481, '68c0bade8656fdd530ede38b4395c213.jpg', '68c0bade8656fdd530ede38b4395c213.jpg_original', 0, 0, '2020-03-14 07:34:48', '2020-05-11 12:13:40'),
(430, 482, '5e7a15d0e8085c057ff2e15d4a5e533a.jpg', '5e7a15d0e8085c057ff2e15d4a5e533a.jpg_original', 0, 0, '2020-03-14 07:54:28', '2020-05-11 12:13:42'),
(431, 483, '48dfe6dc2c650bf6eb3daf5180c9e89d.jpg', '48dfe6dc2c650bf6eb3daf5180c9e89d.jpg_original', 0, 0, '2020-03-14 07:55:34', '2020-05-11 12:13:41'),
(432, 484, '87039f4ace8da180e224504e0a2baab2.jpg', '87039f4ace8da180e224504e0a2baab2.jpg_original', 0, 0, '2020-03-14 07:57:04', '2020-05-11 12:13:43'),
(433, 485, '2d1afb53889f09b994781b4d43be8293.jpg', '2d1afb53889f09b994781b4d43be8293.jpg_original', 0, 0, '2020-03-14 07:58:12', '2020-05-11 12:13:43'),
(434, 486, '0917d32ad3f0a4520b6dee27908ac802.jpg', '0917d32ad3f0a4520b6dee27908ac802.jpg_original', 0, 0, '2020-03-14 08:32:58', '2020-05-11 12:13:33'),
(435, 487, '723aca40217ce0c48587db764cb63e21.jpg', '723aca40217ce0c48587db764cb63e21.jpg_original', 0, 0, '2020-03-14 08:37:45', '2020-05-11 12:13:27'),
(436, 488, '7e1ca9faa588780b693ffd07de95f1af.jpg', '7e1ca9faa588780b693ffd07de95f1af.jpg_original', 0, 0, '2020-03-14 08:41:42', '2020-05-11 12:13:32'),
(437, 489, 'c6b30407c3af6a7b3929e997eb13a6c7.jpg', 'c6b30407c3af6a7b3929e997eb13a6c7.jpg_original', 0, 0, '2020-03-14 08:43:07', '2020-05-11 12:13:31'),
(438, 490, '41dd9d493baed522c8f36e3943ce3f64.jpg', '41dd9d493baed522c8f36e3943ce3f64.jpg_original', 0, 0, '2020-03-14 08:44:23', '2020-05-11 12:13:30'),
(439, 491, '314d8a3ce9806cf7f6ec31303101484c.jpg', '314d8a3ce9806cf7f6ec31303101484c.jpg_original', 0, 0, '2020-03-16 04:39:12', '2020-05-11 11:49:04'),
(440, 492, '0433f150daa004c80bd2465f6ca87338.jpg', '0433f150daa004c80bd2465f6ca87338.jpg_original', 0, 0, '2020-03-16 04:40:11', '2020-05-11 11:49:03'),
(441, 493, '81e1d8d6cb22511f30addcd637ba4fa6.jpg', '81e1d8d6cb22511f30addcd637ba4fa6.jpg_original', 0, 0, '2020-03-16 04:40:47', '2020-05-11 11:49:01'),
(442, 494, 'b3f2c54ffbdea9bc53c5e1463ee2f006.jpg', 'b3f2c54ffbdea9bc53c5e1463ee2f006.jpg_original', 0, 0, '2020-03-16 04:41:16', '2020-05-11 11:49:01'),
(443, 495, '6606873f2df5f01d5cd124c374de3643.jpg', '6606873f2df5f01d5cd124c374de3643.jpg_original', 0, 0, '2020-03-16 04:41:39', '2020-05-11 11:48:59'),
(444, 496, '5607f065de60e45dd6a52e128c15b3ea.jpg', '5607f065de60e45dd6a52e128c15b3ea.jpg_original', 0, 0, '2020-03-16 04:44:05', '2020-05-11 11:48:58'),
(445, 497, 'b42935e0921750cb9d145837ecbe39a0.jpg', 'b42935e0921750cb9d145837ecbe39a0.jpg_original', 0, 0, '2020-03-16 04:44:30', '2020-05-11 11:48:56'),
(446, 498, 'c6e598dafea53efd0cb76a9f45d95836.jpg', 'c6e598dafea53efd0cb76a9f45d95836.jpg_original', 0, 0, '2020-03-16 05:49:03', '2020-05-11 11:49:11'),
(447, 499, '37ec4f2a72fe70fd85bab630a994e872.jpg', '37ec4f2a72fe70fd85bab630a994e872.jpg_original', 0, 0, '2020-03-16 05:49:30', '2020-05-11 11:49:10'),
(448, 500, 'f292550f7e1e2d56f8f634b7a5971b15.jpg', 'f292550f7e1e2d56f8f634b7a5971b15.jpg_original', 0, 0, '2020-03-16 05:49:56', '2020-05-11 11:49:06'),
(449, 501, '14626f5ded1951cc3001e68eacfd16f9.jpg', '14626f5ded1951cc3001e68eacfd16f9.jpg_original', 0, 0, '2020-03-16 05:50:38', '2020-05-11 11:49:05'),
(450, 502, '274a4e12a31458c68de1e87d0ce21826.jpg', '274a4e12a31458c68de1e87d0ce21826.jpg_original', 0, 0, '2020-03-16 06:14:42', '2020-05-11 11:49:32'),
(451, 503, '6318608be316d22625b53659c2b0ff8d.png', '6318608be316d22625b53659c2b0ff8d.png_original', 0, 0, '2020-03-16 06:15:21', '2020-05-11 11:49:33'),
(452, 504, '98a7185b0d8a864006b3f956a5d26de9.jpg', '98a7185b0d8a864006b3f956a5d26de9.jpg_original', 0, 0, '2020-03-16 06:16:14', '2020-05-08 13:17:42'),
(453, 505, 'abad9735aeb51edb8e770113733baab2.png', 'abad9735aeb51edb8e770113733baab2.png_original', 0, 0, '2020-03-16 06:27:24', '2020-05-11 12:10:21'),
(454, 506, '467513a9344bbdef0ba9134ccb4b6e2e.jpg', '467513a9344bbdef0ba9134ccb4b6e2e.jpg_original', 0, 0, '2020-03-16 07:11:30', '2020-05-11 11:49:20'),
(455, 507, '54d8ff0f2f40e938db264576af8291dd.jpg', '54d8ff0f2f40e938db264576af8291dd.jpg_original', 0, 0, '2020-03-16 07:32:09', '2020-05-11 11:49:45'),
(456, 508, 'b56128b0d860561c32dff51a279b17b5.jpg', 'b56128b0d860561c32dff51a279b17b5.jpg_original', 0, 0, '2020-03-16 07:34:01', '2020-05-11 12:02:41'),
(457, 509, '612e82f27e8426104b923f3f332473cb.png', '612e82f27e8426104b923f3f332473cb.png_original', 0, 0, '2020-03-16 08:26:10', '2020-05-11 11:49:18'),
(458, 510, 'ec784cb86acef0a88f9107611dd79d53.png', 'ec784cb86acef0a88f9107611dd79d53.png_original', 0, 0, '2020-03-16 08:45:57', '2020-05-11 11:49:16'),
(459, 511, '8c0e4f63b0a7b642592405591df0e1c4.png', '8c0e4f63b0a7b642592405591df0e1c4.png_original', 0, 0, '2020-03-16 08:46:35', '2020-05-11 11:49:15'),
(460, 512, '780fb24d5d8ebf2b150a0384f914b04d.png', '780fb24d5d8ebf2b150a0384f914b04d.png_original', 0, 0, '2020-03-16 08:47:31', '2020-05-11 11:49:14'),
(461, 513, '411ed128b4830e4cffd23aace74ec1ba.png', '411ed128b4830e4cffd23aace74ec1ba.png_original', 0, 0, '2020-03-16 08:48:14', '2020-05-11 11:49:12'),
(462, 514, '0306d86a0304c0104809b4f3e67cedf6.png', '0306d86a0304c0104809b4f3e67cedf6.png_original', 0, 0, '2020-03-16 08:48:39', '2020-05-11 11:49:12'),
(463, 515, 'cc0c25906436f67c336ad49654b28248.jpg', 'cc0c25906436f67c336ad49654b28248.jpg_original', 0, 0, '2020-03-16 10:03:13', '2020-05-11 11:52:39'),
(464, 516, '5fe4436cd468950e21a0d095ac6e0228.jpg', '5fe4436cd468950e21a0d095ac6e0228.jpg_original', 0, 0, '2020-03-16 10:05:26', '2020-05-11 11:52:44'),
(465, 29, '13385444e0b34339e59a6dc1df709543.jpg', '13385444e0b34339e59a6dc1df709543.jpg_original', 0, 0, '2020-03-16 10:55:29', '2020-05-11 12:18:44'),
(466, 28, '8b9f5f4d6d175c9a9fdf8b3834a465a8.jpg', '8b9f5f4d6d175c9a9fdf8b3834a465a8.jpg_original', 0, 0, '2020-03-16 10:55:43', '2020-05-11 12:18:45'),
(467, 2, '069d686738f8609096e5c704f5925d58.jpg', '069d686738f8609096e5c704f5925d58.jpg_original', 0, 0, '2020-03-18 05:21:45', '2020-05-11 12:12:46'),
(468, 517, '74e979354f4d9c676c9f899e38314e9a.png', '74e979354f4d9c676c9f899e38314e9a.png_original', 0, 0, '2020-03-18 05:50:12', '2020-05-11 11:49:29'),
(469, 518, '863642a98cdbe5e067b2651dba8531c7.png', '863642a98cdbe5e067b2651dba8531c7.png_original', 0, 0, '2020-03-18 05:51:31', '2020-05-11 11:49:31'),
(470, 519, '3ec335cb6604143357608c1e6c9b69a0.png', '3ec335cb6604143357608c1e6c9b69a0.png_original', 0, 0, '2020-03-18 05:54:39', '2020-05-11 11:49:28'),
(471, 520, 'bae89f6680f1f3b77c9782901f2012cd.jpg', 'bae89f6680f1f3b77c9782901f2012cd.jpg_original', 0, 0, '2020-03-18 06:27:22', '2020-05-11 11:49:22'),
(472, 521, '7be43d3689a227f37d30f93375fce782.jpg', '7be43d3689a227f37d30f93375fce782.jpg_original', 0, 0, '2020-03-18 06:30:03', '2020-05-11 12:09:16'),
(473, 522, '845252a1236feb761a5e437cdce6a977.jpg', '845252a1236feb761a5e437cdce6a977.jpg_original', 0, 0, '2020-03-18 06:32:33', '2020-05-11 11:49:43'),
(474, 523, 'b9b8aa757b350130f27afb43803a6ee0.jpg', 'b9b8aa757b350130f27afb43803a6ee0.jpg_original', 0, 0, '2020-03-18 06:38:49', '2020-05-11 11:47:55'),
(475, 524, '16a873dd154c8bdb97154d272e8261ac.jpg', '16a873dd154c8bdb97154d272e8261ac.jpg_original', 0, 0, '2020-03-18 07:02:26', '2020-05-11 11:52:41'),
(476, 524, '6af3a2a398832d5544bacd5ea01a9d0a.jpg', '6af3a2a398832d5544bacd5ea01a9d0a.jpg_original', 0, 0, '2020-03-18 07:02:32', '2020-05-11 11:52:41'),
(477, 524, 'dce5b5835ce51d25f9237f29cffd1193.jpg', 'dce5b5835ce51d25f9237f29cffd1193.jpg_original', 0, 0, '2020-03-18 07:02:38', '2020-05-11 11:52:41'),
(478, 525, '05fabe67acd0d483e284be8b95fa9169.jpg', '05fabe67acd0d483e284be8b95fa9169.jpg_original', 0, 0, '2020-03-18 07:04:35', '2020-05-11 12:02:07'),
(479, 525, '1e1a74dfbcc2464f3c4aad7ff55cbb57.jpg', '1e1a74dfbcc2464f3c4aad7ff55cbb57.jpg_original', 0, 0, '2020-03-18 07:04:41', '2020-05-11 12:02:07'),
(480, 525, '51e9569f978532bdd00807085a0f219b.jpg', '51e9569f978532bdd00807085a0f219b.jpg_original', 0, 0, '2020-03-18 07:04:48', '2020-05-11 12:02:07'),
(481, 526, '628aa9058c5c3a858cb516ae66c495a6.jpg', '628aa9058c5c3a858cb516ae66c495a6.jpg_original', 0, 0, '2020-03-18 07:05:50', '2020-05-11 11:49:21'),
(482, 526, '1ca74170f7c5d98d75e9c98bcb35cd05.jpg', '1ca74170f7c5d98d75e9c98bcb35cd05.jpg_original', 0, 0, '2020-03-18 07:05:58', '2020-05-11 11:49:21'),
(483, 527, 'cac49450d9be50cf53999b0e62ab9d5f.jpg', 'cac49450d9be50cf53999b0e62ab9d5f.jpg_original', 0, 0, '2020-03-18 10:54:52', '2020-05-11 12:02:26'),
(484, 528, '66974e7cc0d3d319019190523e219209.jpg', '66974e7cc0d3d319019190523e219209.jpg_original', 0, 0, '2020-03-18 10:57:59', '2020-05-11 12:02:16'),
(485, 529, '2ae104197eff59f45316f4cd800312bb.jpg', '2ae104197eff59f45316f4cd800312bb.jpg_original', 0, 0, '2020-03-18 11:01:27', '2020-05-11 12:02:37'),
(486, 530, 'aebb5ba0cb66f5098d2980730f3926af.jpg', 'aebb5ba0cb66f5098d2980730f3926af.jpg_original', 0, 0, '2020-03-18 11:05:59', '2020-05-11 12:02:32'),
(487, 531, '42608396264b42063dd7fc93dd58ac75.jpg', '42608396264b42063dd7fc93dd58ac75.jpg_original', 0, 0, '2020-03-18 11:06:41', '2020-05-11 12:02:28'),
(488, 532, '55c00c16b3ddc577230c6e877d4af61b.jpg', '55c00c16b3ddc577230c6e877d4af61b.jpg_original', 0, 0, '2020-03-18 11:07:47', '2020-05-11 12:02:42'),
(489, 533, '08a3b2468d8ca03df456916d22a0c88e.jpg', '08a3b2468d8ca03df456916d22a0c88e.jpg_original', 0, 0, '2020-03-18 11:09:07', '2020-05-11 12:02:24'),
(490, 534, 'b0d2588485fd94bb59824a7c5f307397.jpg', 'b0d2588485fd94bb59824a7c5f307397.jpg_original', 0, 0, '2020-03-18 11:10:23', '2020-05-11 12:02:34'),
(491, 535, 'f0c25256f794aecd94935cc5667c057d.jpg', 'f0c25256f794aecd94935cc5667c057d.jpg_original', 0, 0, '2020-03-18 11:10:56', '2020-05-11 12:02:30'),
(492, 536, 'eb97e84224d0c43cb69056647ec4ad67.jpg', 'eb97e84224d0c43cb69056647ec4ad67.jpg_original', 0, 0, '2020-03-18 11:18:18', '2020-05-11 12:02:19'),
(493, 537, '2ae73c1961fdecfee6a92b850b24ff8b.jpg', '2ae73c1961fdecfee6a92b850b24ff8b.jpg_original', 0, 0, '2020-03-18 11:19:31', '2020-05-11 12:02:22'),
(494, 538, '7eec8d8d56ed56c5aa8db7e5d9d14468.jpg', '7eec8d8d56ed56c5aa8db7e5d9d14468.jpg_original', 0, 0, '2020-03-18 11:26:25', '2020-05-11 11:48:10'),
(495, 539, 'ec5f04930d09f224cf9a670395bbc64d.jpg', 'ec5f04930d09f224cf9a670395bbc64d.jpg_original', 0, 0, '2020-03-18 11:49:05', '2020-05-11 11:48:11'),
(496, 540, '8f6659b72911341f7661d5637c6cd462.jpg', '8f6659b72911341f7661d5637c6cd462.jpg_original', 0, 0, '2020-03-18 11:50:19', '2020-05-11 11:48:10'),
(497, 541, 'a1b86c82590847c109b9a7974ad6dd37.jpg', 'a1b86c82590847c109b9a7974ad6dd37.jpg_original', 0, 0, '2020-03-18 11:51:34', '2020-05-11 11:48:09'),
(498, 542, 'eb47319c4409942f8a576cfb3fac98e6.jpg', 'eb47319c4409942f8a576cfb3fac98e6.jpg_original', 0, 0, '2020-03-18 12:50:33', '2020-05-11 11:48:02'),
(499, 543, 'e1762c46251b867a9c81a9c894f1618e.jpg', 'e1762c46251b867a9c81a9c894f1618e.jpg_original', 0, 0, '2020-03-18 12:52:23', '2020-05-11 11:48:03'),
(500, 544, '5a98d416fe49ddb63c56f262f9afe5da.jpg', '5a98d416fe49ddb63c56f262f9afe5da.jpg_original', 0, 0, '2020-03-18 12:54:31', '2020-05-11 11:48:04'),
(501, 545, '28258ed8ceefbdd1297ccca1c8b06b09.jpg', '28258ed8ceefbdd1297ccca1c8b06b09.jpg_original', 0, 0, '2020-03-18 12:55:23', '2020-05-11 11:48:05'),
(502, 546, 'c4d1a1d78fc688f154d83d4845e90a5b.jpg', 'c4d1a1d78fc688f154d83d4845e90a5b.jpg_original', 0, 0, '2020-03-18 12:58:05', '2020-05-11 11:48:07'),
(503, 547, '43fdeea5fbb440a4901fd69d2de9cadb.jpg', '43fdeea5fbb440a4901fd69d2de9cadb.jpg_original', 0, 0, '2020-03-18 12:58:40', '2020-05-11 11:48:05'),
(504, 548, '9f2e21872fb95d67b9f3038409b1b32c.jpg', '9f2e21872fb95d67b9f3038409b1b32c.jpg_original', 0, 0, '2020-03-18 12:59:45', '2020-05-11 11:48:08'),
(505, 549, 'eaf1d4d5467e27f1e6ff0aacc9f94a1b.jpg', 'eaf1d4d5467e27f1e6ff0aacc9f94a1b.jpg_original', 0, 0, '2020-03-18 13:00:24', '2020-05-11 11:48:06'),
(506, 550, 'f173d28ab2a5de6406bcf950ac104d42.jpg', 'f173d28ab2a5de6406bcf950ac104d42.jpg_original', 0, 0, '2020-03-19 07:55:32', '2020-05-11 11:48:00'),
(507, 551, 'd4c695d580a2a023a1e51a9c02f58688.jpg', 'd4c695d580a2a023a1e51a9c02f58688.jpg_original', 0, 0, '2020-03-19 07:56:53', '2020-05-11 11:48:00'),
(508, 552, 'f255515cfb2b40b35d0875a14d7fe22c.jpg', 'f255515cfb2b40b35d0875a14d7fe22c.jpg_original', 0, 0, '2020-03-19 07:58:15', '2020-05-11 11:47:58'),
(509, 553, '92e6fb23ada04e4689eac565afa117ae.jpg', '92e6fb23ada04e4689eac565afa117ae.jpg_original', 0, 0, '2020-03-19 07:58:42', '2020-05-11 11:47:59'),
(510, 554, '6b71823d2487c00c155af4edfae5e950.jpg', '6b71823d2487c00c155af4edfae5e950.jpg_original', 0, 0, '2020-03-19 08:00:17', '2020-05-11 11:47:57'),
(511, 555, 'a9be7d0206c8ed0fd4ba8413bc352884.jpg', 'a9be7d0206c8ed0fd4ba8413bc352884.jpg_original', 0, 0, '2020-03-19 09:17:18', '2020-05-11 11:49:07'),
(512, 556, '2a0c92a2e5d54a7c16e6d3a56faf23ac.jpg', '2a0c92a2e5d54a7c16e6d3a56faf23ac.jpg_original', 0, 0, '2020-03-19 09:21:41', '2020-05-11 12:02:11'),
(513, 556, '1bc41f88eba65888f34adf547bd24fd9.jpg', '1bc41f88eba65888f34adf547bd24fd9.jpg_original', 0, 0, '2020-03-19 09:21:46', '2020-05-11 12:02:11'),
(514, 557, 'a4203779f8cefdbbea36b8931e49f886.jpg', 'a4203779f8cefdbbea36b8931e49f886.jpg_original', 0, 0, '2020-03-19 09:22:32', '2020-05-11 12:10:59'),
(515, 558, '942b340c1bfbfce44885ab6fb5ee52a4.jpg', '942b340c1bfbfce44885ab6fb5ee52a4.jpg_original', 0, 0, '2020-03-19 09:23:32', '2020-05-11 11:49:18'),
(516, 559, 'ebc22e7cbcbd7abb4bf8efca578397ef.jpg', 'ebc22e7cbcbd7abb4bf8efca578397ef.jpg_original', 0, 0, '2020-03-19 09:39:57', '2020-05-11 12:09:22'),
(517, 560, '182f7d5334f01028911b599377ff1e0d.jpg', '182f7d5334f01028911b599377ff1e0d.jpg_original', 0, 0, '2020-03-19 09:44:58', '2020-03-19 09:45:47'),
(518, 560, 'bc5e4e42258e27efd87b4e9351832d30.jpg', 'bc5e4e42258e27efd87b4e9351832d30.jpg_original', 0, 0, '2020-03-19 09:46:06', '2020-05-11 11:52:48'),
(519, 561, '8ab801f221391fb0ccbf58cb53c1ab51.jpg', '8ab801f221391fb0ccbf58cb53c1ab51.jpg_original', 0, 0, '2020-03-19 09:49:34', '2020-05-11 11:52:43'),
(520, 562, 'a3c8a5fcbd2b5ad0c0c5bdf1f1502350.jpg', 'a3c8a5fcbd2b5ad0c0c5bdf1f1502350.jpg_original', 0, 0, '2020-03-19 09:50:48', '2020-05-11 11:49:36'),
(521, 563, '0bb88e607d75776033d7e4260160e6c8.jpg', '0bb88e607d75776033d7e4260160e6c8.jpg_original', 0, 0, '2020-03-19 09:56:38', '2020-05-11 11:52:47'),
(522, 564, 'a057fc3a9de2dfcbe3b01eea03017eb8.jpg', 'a057fc3a9de2dfcbe3b01eea03017eb8.jpg_original', 0, 0, '2020-03-19 09:57:17', '2020-05-11 11:52:46'),
(523, 565, '84100efea5b465f513d8d40d3219cdac.jpg', '84100efea5b465f513d8d40d3219cdac.jpg_original', 0, 0, '2020-03-19 09:58:18', '2020-05-11 11:49:34'),
(524, 566, '1eaa363c36701ccb03562f0dcf24fa55.jpg', '1eaa363c36701ccb03562f0dcf24fa55.jpg_original', 0, 0, '2020-03-19 10:00:28', '2020-05-11 11:49:47'),
(525, 567, 'b9d9551d132b48165512640da1c94bdb.jpg', 'b9d9551d132b48165512640da1c94bdb.jpg_original', 0, 0, '2020-12-22 22:37:48', '2020-12-24 11:51:32'),
(526, 567, 'c308277baf3e4e58e624c358b1cf2b23.png', 'c308277baf3e4e58e624c358b1cf2b23.png_original', 0, 0, '2021-01-05 15:33:18', '2021-01-05 15:34:10'),
(527, 567, '08267297a16d4ea956cbe92dfa0448f3.jpg', '08267297a16d4ea956cbe92dfa0448f3.jpg_original', 0, 0, '2021-01-05 15:34:57', '2021-01-05 15:52:08'),
(528, 567, 'd8309c0f7de857f59759917b5223411e.jpg', 'd8309c0f7de857f59759917b5223411e.jpg_original', 0, 0, '2021-01-05 15:35:10', '2021-01-05 15:35:27'),
(529, 568, 'a488f5dc91bc3d8933f777bcaad4e8a2.jpg', 'a488f5dc91bc3d8933f777bcaad4e8a2.jpg_original', 0, 0, '2021-01-05 15:53:47', '2021-01-13 12:13:57'),
(530, 568, '459a0662221510f23d76376f309ce09f.jpg', '459a0662221510f23d76376f309ce09f.jpg_original', 0, 0, '2021-01-05 15:54:51', '2021-01-05 16:01:31'),
(531, 568, '5ed8f37f0c32cce24992ef012433e2c0.jpg', '5ed8f37f0c32cce24992ef012433e2c0.jpg_original', 0, 0, '2021-01-13 12:14:13', '2021-01-13 14:05:51'),
(532, 569, 'a9d5c27411fd92988442cc55b581488f.jpg', 'a9d5c27411fd92988442cc55b581488f.jpg_original', 0, 0, '2021-01-13 12:20:13', '2021-01-13 14:05:27'),
(533, 570, '31cb7294e2b7fa778c4ad6923835aa8c.jpg', '31cb7294e2b7fa778c4ad6923835aa8c.jpg_original', 0, 0, '2021-01-13 12:20:53', '2021-01-13 14:05:09'),
(534, 571, 'b9ee8ba9db496b187990205b41deb62c.jpg', 'b9ee8ba9db496b187990205b41deb62c.jpg_original', 0, 0, '2021-01-13 12:21:43', '2021-01-13 14:04:52'),
(535, 572, '365b2535bb37fa91ab6bf1f8b41f813b.jpg', '365b2535bb37fa91ab6bf1f8b41f813b.jpg_original', 0, 0, '2021-01-13 12:23:15', '2021-01-13 14:04:35'),
(536, 573, '2f383e940182f59bf59f7aabc5ae4071.jpg', '2f383e940182f59bf59f7aabc5ae4071.jpg_original', 0, 0, '2021-01-13 12:24:06', '2021-01-13 14:04:19'),
(537, 574, '20b76733c3f40edd1281363c726ccea7.jpg', '20b76733c3f40edd1281363c726ccea7.jpg_original', 0, 0, '2021-01-13 12:24:53', '2021-01-13 14:03:54'),
(538, 575, '73487e3b9156ef844553d51f9688d9d3.jpg', '73487e3b9156ef844553d51f9688d9d3.jpg_original', 0, 0, '2021-01-13 12:25:24', '2021-01-13 14:03:37'),
(539, 576, '81f99ae826c3145a527a433fbc864fa2.jpg', '81f99ae826c3145a527a433fbc864fa2.jpg_original', 0, 0, '2021-01-13 12:27:52', '2021-01-13 14:02:56'),
(540, 577, 'ff70607660988e3d2edef9505be4bb02.jpg', 'ff70607660988e3d2edef9505be4bb02.jpg_original', 0, 0, '2021-01-13 12:28:55', '2021-01-13 14:02:41'),
(541, 578, '324bc103f742f2711162738b0498aa06.jpg', '324bc103f742f2711162738b0498aa06.jpg_original', 0, 0, '2021-01-13 12:29:36', '2021-01-13 14:02:23'),
(542, 579, '8b878b2109b78064cd1cab0ecadd9d1d.jpg', '8b878b2109b78064cd1cab0ecadd9d1d.jpg_original', 0, 0, '2021-01-13 12:51:34', '2021-01-13 14:01:51'),
(543, 580, '20a9c419dd5296e24dc700ed7289e12d.jpg', '20a9c419dd5296e24dc700ed7289e12d.jpg_original', 0, 0, '2021-01-13 12:52:10', '2021-01-13 14:01:30'),
(544, 581, '3d78bfea6beb164b089006b2b04fdea5.jpg', '3d78bfea6beb164b089006b2b04fdea5.jpg_original', 0, 0, '2021-01-13 12:52:34', '2021-01-13 14:01:05'),
(545, 582, 'dee9474f3d4d4d54ce983b79ed7d6573.jpg', 'dee9474f3d4d4d54ce983b79ed7d6573.jpg_original', 0, 0, '2021-01-13 12:53:16', '2021-01-13 13:57:40'),
(546, 583, 'e56f17c1da93ca83c933e9958259c62c.jpg', 'e56f17c1da93ca83c933e9958259c62c.jpg_original', 0, 0, '2021-01-13 12:53:34', '2021-01-13 13:57:10'),
(547, 584, '6b6feb20e3bb188fe791a4295de65107.jpg', '6b6feb20e3bb188fe791a4295de65107.jpg_original', 0, 0, '2021-01-13 12:53:53', '2021-01-13 13:56:49'),
(548, 585, '3d8f21c0db764aeaa9f6fc639351cdd9.jpg', '3d8f21c0db764aeaa9f6fc639351cdd9.jpg_original', 0, 0, '2021-01-13 12:54:15', '2021-01-13 13:56:04'),
(549, 586, '5de481f774dee9dc66a9be35e38478e8.jpg', '5de481f774dee9dc66a9be35e38478e8.jpg_original', 0, 0, '2021-01-13 12:56:56', '2021-01-13 14:06:49'),
(550, 587, '6f6fbb5d640eb67cd2f3d7a60f978250.jpg', '6f6fbb5d640eb67cd2f3d7a60f978250.jpg_original', 0, 0, '2021-01-13 12:57:34', '2021-01-13 14:06:32'),
(551, 588, '861e17602c4ffa6407f02287f5db1c6f.jpg', '861e17602c4ffa6407f02287f5db1c6f.jpg_original', 0, 0, '2021-01-13 12:58:01', '2021-01-13 13:40:28'),
(552, 589, '7a7a0403f82172f9763931188f3494ad.jpg', '7a7a0403f82172f9763931188f3494ad.jpg_original', 0, 0, '2021-01-13 13:00:52', '2021-01-13 13:37:41'),
(553, 589, '22809e6ea414346cc265fed8e670698d.jpg', '22809e6ea414346cc265fed8e670698d.jpg_original', 0, 0, '2021-01-13 13:37:38', '2021-01-13 13:39:57'),
(554, 589, 'c81c7d8da1df7d03d8c5c4c7236eb75f.jpg', 'c81c7d8da1df7d03d8c5c4c7236eb75f.jpg_original', 0, 0, '2021-01-13 13:40:09', '2021-01-13 14:12:12'),
(555, 588, 'b6fa37b9e8e65cd2f971696c5a03096b.jpg', 'b6fa37b9e8e65cd2f971696c5a03096b.jpg_original', 0, 0, '2021-01-13 13:40:39', '2021-01-15 13:25:14'),
(556, 585, '20df9267a1fa183c6db642d11eb302fe.jpg', '20df9267a1fa183c6db642d11eb302fe.jpg_original', 0, 1, '2021-01-13 13:56:13', '2021-01-13 13:56:13'),
(557, 584, 'f28ee7323822b863d71cde623257afb2.jpg', 'f28ee7323822b863d71cde623257afb2.jpg_original', 0, 1, '2021-01-13 13:57:00', '2021-01-13 13:57:00'),
(558, 583, 'b8d3232a4cc5349b2486f60c0bdea965.jpg', 'b8d3232a4cc5349b2486f60c0bdea965.jpg_original', 0, 1, '2021-01-13 13:57:20', '2021-01-13 13:57:20'),
(559, 582, 'd9f354548f486af16624f2b1dca729d0.jpg', 'd9f354548f486af16624f2b1dca729d0.jpg_original', 0, 1, '2021-01-13 13:57:48', '2021-01-13 13:57:48'),
(560, 581, '706356c86e90f2c2888ecce5e41b411b.jpg', '706356c86e90f2c2888ecce5e41b411b.jpg_original', 0, 1, '2021-01-13 14:01:22', '2021-01-13 14:01:22'),
(561, 580, 'dd5bfc098a56dbeb058c40e0a65faac4.jpg', 'dd5bfc098a56dbeb058c40e0a65faac4.jpg_original', 0, 1, '2021-01-13 14:01:44', '2021-01-13 14:01:44'),
(562, 579, '6bf8df266e0f2e89e5619763de64884d.jpg', '6bf8df266e0f2e89e5619763de64884d.jpg_original', 0, 1, '2021-01-13 14:01:58', '2021-01-13 14:01:58'),
(563, 578, 'f844e341efb437fe8eb56bb33ca24e7d.jpg', 'f844e341efb437fe8eb56bb33ca24e7d.jpg_original', 0, 1, '2021-01-13 14:02:18', '2021-01-13 14:02:18'),
(564, 577, 'fc30330e4c1b44127eea55ef65c54ca1.jpg', 'fc30330e4c1b44127eea55ef65c54ca1.jpg_original', 0, 1, '2021-01-13 14:02:36', '2021-01-13 14:02:36'),
(565, 576, '0afb3e0f07d3a58c7197b4d6dab307d2.jpg', '0afb3e0f07d3a58c7197b4d6dab307d2.jpg_original', 0, 1, '2021-01-13 14:02:53', '2021-01-13 14:02:53'),
(566, 575, 'b12bc83e20e7463ceed531793b7586c1.jpg', 'b12bc83e20e7463ceed531793b7586c1.jpg_original', 0, 1, '2021-01-13 14:03:30', '2021-01-13 14:03:30'),
(567, 574, '41a45cd544693dbc201ed65ab257ccd0.jpg', '41a45cd544693dbc201ed65ab257ccd0.jpg_original', 0, 1, '2021-01-13 14:03:49', '2021-01-13 14:03:49'),
(568, 573, '467b77e7adc0fec9ec4bc3d5ce3a8462.jpg', '467b77e7adc0fec9ec4bc3d5ce3a8462.jpg_original', 0, 1, '2021-01-13 14:04:15', '2021-01-13 14:04:15'),
(569, 572, '1010ed4bd824ff8800b208f6dcae8e0d.jpg', '1010ed4bd824ff8800b208f6dcae8e0d.jpg_original', 0, 1, '2021-01-13 14:04:32', '2021-01-13 14:04:32'),
(570, 571, 'a8cebc60a0c8337e9cbcfa9c401baee3.jpg', 'a8cebc60a0c8337e9cbcfa9c401baee3.jpg_original', 0, 1, '2021-01-13 14:04:48', '2021-01-13 14:04:48'),
(571, 570, '4ac9d15f8b2b7bf9f9c29730d1271d8d.jpg', '4ac9d15f8b2b7bf9f9c29730d1271d8d.jpg_original', 0, 1, '2021-01-13 14:05:03', '2021-01-13 14:05:03'),
(572, 569, '054d01622a8b413480a45e464f4b41cf.jpg', '054d01622a8b413480a45e464f4b41cf.jpg_original', 0, 1, '2021-01-13 14:05:22', '2021-01-13 14:05:22'),
(573, 568, '587288549e2f43ed9b99e7ea47733208.jpg', '587288549e2f43ed9b99e7ea47733208.jpg_original', 0, 1, '2021-01-13 14:05:48', '2021-01-13 14:05:48'),
(574, 587, 'b7d7c3e667e98d4db3f9bfeb3dee26eb.jpg', 'b7d7c3e667e98d4db3f9bfeb3dee26eb.jpg_original', 0, 0, '2021-01-13 14:06:29', '2021-01-15 13:24:59'),
(575, 586, '47261ae8093f9a0d19aa91806a016711.jpg', '47261ae8093f9a0d19aa91806a016711.jpg_original', 0, 0, '2021-01-13 14:06:46', '2021-01-15 13:24:34'),
(576, 589, '42b99e44d7dfdebeeb1cb5416fb6570e.jpg', '42b99e44d7dfdebeeb1cb5416fb6570e.jpg_original', 0, 1, '2021-01-13 14:12:21', '2021-01-13 14:12:21'),
(577, 589, 'd4a9df6ae6a307e19f6495d0494259e3.jpg', 'd4a9df6ae6a307e19f6495d0494259e3.jpg_original', 0, 1, '2021-01-13 14:12:27', '2021-01-13 14:12:27'),
(578, 586, 'ada4726a5a2e983fc524f7508fbb62b3.jpg', 'ada4726a5a2e983fc524f7508fbb62b3.jpg_original', 0, 0, '2021-01-15 13:24:47', '2021-03-03 11:54:48'),
(579, 587, '12b538afac5bf5337eb22cd68fc10f31.jpg', '12b538afac5bf5337eb22cd68fc10f31.jpg_original', 0, 0, '2021-01-15 13:25:08', '2021-03-03 11:54:51'),
(580, 588, 'f1db25008d7cd297cb5c08047a3d2167.jpg', 'f1db25008d7cd297cb5c08047a3d2167.jpg_original', 0, 0, '2021-01-15 13:25:20', '2021-03-03 11:54:54'),
(581, 590, '1c3f53b47058a31fa683d53263e6a52d.png', '1c3f53b47058a31fa683d53263e6a52d.png_original', 0, 1, '2021-02-19 14:14:54', '2021-02-19 14:14:54'),
(582, 591, 'b15a751e836b9c092087caf6f211cbca.jpg', 'b15a751e836b9c092087caf6f211cbca.jpg_original', 0, 1, '2021-03-02 11:27:13', '2021-03-02 11:27:13'),
(583, 592, 'fbaa977c93b4f4d5bb62e1c1b6e52d9c.jpg', 'fbaa977c93b4f4d5bb62e1c1b6e52d9c.jpg_original', 0, 1, '2021-03-02 11:31:03', '2021-03-02 11:31:03'),
(584, 593, 'e240746971f6b611c4751bfdb1e57d36.jpg', 'e240746971f6b611c4751bfdb1e57d36.jpg_original', 0, 1, '2021-03-02 11:32:06', '2021-03-02 11:32:06'),
(585, 594, 'cf7acc5fb45f5d3934ec250d05441bad.jpg', 'cf7acc5fb45f5d3934ec250d05441bad.jpg_original', 0, 1, '2021-03-02 11:32:36', '2021-03-02 11:32:36'),
(586, 595, '992f58931d130dde59f9b57731a038a3.jpg', '992f58931d130dde59f9b57731a038a3.jpg_original', 0, 1, '2021-03-02 11:34:22', '2021-03-02 11:34:22'),
(587, 596, 'db094de4b756a82d0b1cc37f90a04adb.jpg', 'db094de4b756a82d0b1cc37f90a04adb.jpg_original', 0, 1, '2021-03-02 11:34:49', '2021-03-02 11:34:49'),
(588, 597, '5a1b5b50f0ea77ff475904b07bebd6c7.jpg', '5a1b5b50f0ea77ff475904b07bebd6c7.jpg_original', 0, 1, '2021-03-02 11:35:19', '2021-03-02 11:35:19'),
(589, 598, 'd38a11cdebdf1fe8263d8ce5400bcd4e.jpg', 'd38a11cdebdf1fe8263d8ce5400bcd4e.jpg_original', 0, 1, '2021-03-02 11:35:46', '2021-03-02 11:35:46'),
(590, 599, '7cc355fcb0576617dbca109e1dbca937.jpg', '7cc355fcb0576617dbca109e1dbca937.jpg_original', 0, 1, '2021-03-02 11:36:26', '2021-03-02 11:36:26'),
(591, 600, 'aab33745298f5d2d7aa365d5bc12ffcb.jpg', 'aab33745298f5d2d7aa365d5bc12ffcb.jpg_original', 0, 0, '2021-03-02 11:36:51', '2021-03-02 11:38:26'),
(592, 601, '9351abcb88f115d14d3ea0bccf961aff.jpg', '9351abcb88f115d14d3ea0bccf961aff.jpg_original', 0, 1, '2021-03-02 11:37:35', '2021-03-02 11:37:35'),
(593, 601, '7dea1824163798e1999a084f9093e572.jpg', '7dea1824163798e1999a084f9093e572.jpg_original', 0, 0, '2021-03-02 11:37:41', '2021-03-02 11:37:45'),
(594, 600, '43ffa1cf0f45cabc52ec1c02345e28a1.jpg', '43ffa1cf0f45cabc52ec1c02345e28a1.jpg_original', 0, 1, '2021-03-02 11:38:32', '2021-03-02 11:38:32'),
(595, 602, 'b36b2e1d4c2e8d59cb422ba45bf3ae33.jpg', 'b36b2e1d4c2e8d59cb422ba45bf3ae33.jpg_original', 0, 1, '2021-03-02 11:39:04', '2021-03-02 11:39:04'),
(596, 603, '7bbd14b04331b7b1150545dfaca2e8a6.jpg', '7bbd14b04331b7b1150545dfaca2e8a6.jpg_original', 0, 1, '2021-03-02 11:39:31', '2021-03-02 11:39:31'),
(597, 604, '34b5e355a0dc9c89b99e0710c2e546ee.jpg', '34b5e355a0dc9c89b99e0710c2e546ee.jpg_original', 0, 1, '2021-03-02 11:39:55', '2021-03-02 11:39:55'),
(598, 605, 'b56b4c33e2af6bd49ea5694741c18954.jpg', 'b56b4c33e2af6bd49ea5694741c18954.jpg_original', 0, 1, '2021-03-02 11:40:24', '2021-03-02 11:40:24'),
(599, 606, '257a3300d3a22e9e683ca3c28c5fa977.jpg', '257a3300d3a22e9e683ca3c28c5fa977.jpg_original', 0, 1, '2021-03-02 11:40:48', '2021-03-02 11:40:48'),
(600, 607, '3be0f58d72bc6ff291e527150a6546cc.jpg', '3be0f58d72bc6ff291e527150a6546cc.jpg_original', 0, 1, '2021-03-02 11:48:39', '2021-03-02 11:48:39'),
(601, 608, '18e8723c891bf0356975228b7aefaa51.jpg', '18e8723c891bf0356975228b7aefaa51.jpg_original', 0, 0, '2021-03-02 11:53:16', '2021-03-02 11:59:42'),
(602, 609, 'dde4edf9a8d9c1d92165486e1cc5af5e.jpg', 'dde4edf9a8d9c1d92165486e1cc5af5e.jpg_original', 0, 0, '2021-03-02 11:53:46', '2021-03-02 11:59:54'),
(603, 610, '42c656a91bb12b8cad70a1f236b888f1.jpg', '42c656a91bb12b8cad70a1f236b888f1.jpg_original', 0, 0, '2021-03-02 11:54:10', '2021-03-02 12:00:06'),
(604, 611, 'a351421b4e48cb6358f5d453e89f5703.jpg', 'a351421b4e48cb6358f5d453e89f5703.jpg_original', 0, 0, '2021-03-02 11:54:38', '2021-03-02 12:00:19'),
(605, 612, 'd73c18919ab0012e32673bed4a9f3d45.jpg', 'd73c18919ab0012e32673bed4a9f3d45.jpg_original', 0, 0, '2021-03-02 11:55:00', '2021-03-02 12:00:55'),
(606, 613, 'e5dcbf86dee8bd612f4acd23997a8196.jpg', 'e5dcbf86dee8bd612f4acd23997a8196.jpg_original', 0, 0, '2021-03-02 11:55:24', '2021-03-02 12:01:12'),
(607, 614, '4a584a0d05ed682ba545643fb4c1d82f.jpg', '4a584a0d05ed682ba545643fb4c1d82f.jpg_original', 0, 0, '2021-03-02 11:55:46', '2021-03-02 12:01:22'),
(608, 615, '5679544ab8cf636398a17b57e16bb417.jpg', '5679544ab8cf636398a17b57e16bb417.jpg_original', 0, 0, '2021-03-02 11:56:29', '2021-03-02 12:01:33'),
(609, 616, '7063b04a58b6a4f607895d7fdf2deb2e.jpg', '7063b04a58b6a4f607895d7fdf2deb2e.jpg_original', 0, 0, '2021-03-02 11:56:58', '2021-03-02 12:01:44'),
(610, 617, 'b4e794f50fe6dd2558efef2c594de734.jpg', 'b4e794f50fe6dd2558efef2c594de734.jpg_original', 0, 0, '2021-03-02 11:57:27', '2021-03-02 12:01:55'),
(611, 618, 'a1e4569e7b2d33087b3b08e63ca6c72f.jpg', 'a1e4569e7b2d33087b3b08e63ca6c72f.jpg_original', 0, 0, '2021-03-02 11:58:46', '2021-03-02 12:02:03'),
(612, 608, '953d1df4076588e307da48798d0f6059.jpg', '953d1df4076588e307da48798d0f6059.jpg_original', 0, 1, '2021-03-02 11:59:48', '2021-03-02 11:59:48'),
(613, 609, '2d0c4b0a970548d5da6cf1e9788e7973.jpg', '2d0c4b0a970548d5da6cf1e9788e7973.jpg_original', 0, 1, '2021-03-02 12:00:00', '2021-03-02 12:00:00'),
(614, 610, 'bf8f864caddbdec87fd87e713e40231a.jpg', 'bf8f864caddbdec87fd87e713e40231a.jpg_original', 0, 1, '2021-03-02 12:00:14', '2021-03-02 12:00:14'),
(615, 611, '0359f6910a87fb7d8c35194d834c51ab.jpg', '0359f6910a87fb7d8c35194d834c51ab.jpg_original', 0, 1, '2021-03-02 12:00:31', '2021-03-02 12:00:31'),
(616, 612, '07b225900cc7b12c2ef5e0d8edeba2cf.jpg', '07b225900cc7b12c2ef5e0d8edeba2cf.jpg_original', 0, 1, '2021-03-02 12:01:07', '2021-03-02 12:01:07'),
(617, 613, '4083756a1a66ac9d3ff3c4f37241c2d0.jpg', '4083756a1a66ac9d3ff3c4f37241c2d0.jpg_original', 0, 1, '2021-03-02 12:01:17', '2021-03-02 12:01:17'),
(618, 614, '6af74c8b4362f86f2ac2b3f0f9b85eeb.jpg', '6af74c8b4362f86f2ac2b3f0f9b85eeb.jpg_original', 0, 1, '2021-03-02 12:01:28', '2021-03-02 12:01:28'),
(619, 615, '5999ada240b4b42a02d516b422f98fd7.jpg', '5999ada240b4b42a02d516b422f98fd7.jpg_original', 0, 1, '2021-03-02 12:01:38', '2021-03-02 12:01:38'),
(620, 616, '92aa295c9443e1e907fe99fa548a87f2.jpg', '92aa295c9443e1e907fe99fa548a87f2.jpg_original', 0, 1, '2021-03-02 12:01:50', '2021-03-02 12:01:50'),
(621, 617, 'b71dc055d8d44d16515c11abf85a78cf.jpg', 'b71dc055d8d44d16515c11abf85a78cf.jpg_original', 0, 1, '2021-03-02 12:01:59', '2021-03-02 12:01:59'),
(622, 618, '55e813fe47d48cce156690a56bc28919.jpg', '55e813fe47d48cce156690a56bc28919.jpg_original', 0, 1, '2021-03-02 12:02:11', '2021-03-02 12:02:11'),
(623, 619, 'd6597cdc11fcba68c6a02e283891e076.jpg', 'd6597cdc11fcba68c6a02e283891e076.jpg_original', 0, 1, '2021-03-02 12:02:42', '2021-03-02 12:02:42'),
(624, 620, 'ef6f14641a6d04756166378d940c09d5.jpg', 'ef6f14641a6d04756166378d940c09d5.jpg_original', 0, 1, '2021-03-02 12:03:04', '2021-03-02 12:03:04'),
(625, 621, 'd464948d3efa9de4e607b0047b9191c6.jpg', 'd464948d3efa9de4e607b0047b9191c6.jpg_original', 0, 1, '2021-03-02 12:03:40', '2021-03-02 12:03:40'),
(626, 622, 'd53143dd2d02458b629b34f3afc7ac70.jpg', 'd53143dd2d02458b629b34f3afc7ac70.jpg_original', 0, 0, '2021-03-02 12:04:16', '2021-03-02 12:05:09'),
(627, 623, '63096fd4f6b6187828a365617d4d905c.jpg', '63096fd4f6b6187828a365617d4d905c.jpg_original', 0, 0, '2021-03-02 12:04:50', '2021-03-02 12:05:18'),
(628, 622, '6af3198fbc663d2a7f9046a0c71ba89d.jpg', '6af3198fbc663d2a7f9046a0c71ba89d.jpg_original', 0, 1, '2021-03-02 12:05:13', '2021-03-02 12:05:13'),
(629, 623, 'eda2f8c1a524ca6d1aadabeb79a2c13a.jpg', 'eda2f8c1a524ca6d1aadabeb79a2c13a.jpg_original', 0, 1, '2021-03-02 12:05:22', '2021-03-02 12:05:22'),
(630, 624, '76b026e9709b1338cd85bdd821259286.jpg', '76b026e9709b1338cd85bdd821259286.jpg_original', 0, 1, '2021-03-02 12:05:55', '2021-03-02 12:05:55'),
(631, 625, '6ed50cc489b00f4b35e8b77203c3bc59.jpg', '6ed50cc489b00f4b35e8b77203c3bc59.jpg_original', 0, 1, '2021-03-12 18:26:13', '2021-03-12 18:26:13'),
(632, 625, 'a2885ef27c2d14a8b9aeee615c4a8a54.jpg', 'a2885ef27c2d14a8b9aeee615c4a8a54.jpg_original', 0, 1, '2021-03-12 18:26:26', '2021-03-12 18:26:26'),
(633, 626, 'f9f81f648f03c51e9e1012d036d5968c.jpg', 'f9f81f648f03c51e9e1012d036d5968c.jpg_original', 0, 1, '2021-03-18 14:27:00', '2021-03-18 14:27:00'),
(634, 627, '821dd4af581c1902f51651681e4772c9.jpg', '821dd4af581c1902f51651681e4772c9.jpg_original', 0, 1, '2021-03-24 11:46:58', '2021-03-24 11:46:58'),
(635, 628, '94fb2d1cd75d8c6ad6d375272626768f.jpg', '94fb2d1cd75d8c6ad6d375272626768f.jpg_original', 0, 1, '2021-03-24 12:44:44', '2021-03-24 12:44:44');

-- --------------------------------------------------------

--
-- Table structure for table `product_info`
--

CREATE TABLE `product_info` (
  `product_info_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_info_type_id` int(11) NOT NULL,
  `product_info_type_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `value` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `state_id` int(11) NOT NULL DEFAULT 1,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_info`
--

INSERT INTO `product_info` (`product_info_id`, `product_id`, `product_info_type_id`, `product_info_type_name`, `value`, `state_id`, `updated_at`, `created_at`, `order`) VALUES
(1, 1, 1, 'კოდი', '500', 1, NULL, NULL, 1),
(2, 2, 1, 'კოდი', '500', 1, NULL, NULL, 2),
(3, 3, 1, 'კოდი', '500', 1, NULL, NULL, 3),
(4, 4, 1, 'კოდი', '500', 1, NULL, NULL, 4),
(5, 5, 1, 'კოდი', '500', 1, NULL, NULL, 5),
(6, 6, 1, 'კოდი', '500', 1, NULL, NULL, 6),
(7, 7, 1, 'კოდი', '500', 1, NULL, NULL, 7),
(8, 8, 1, 'კოდი', '500', 1, NULL, NULL, 8),
(9, 9, 1, 'კოდი', '500', 1, NULL, NULL, 9),
(10, 10, 1, 'კოდი', '500', 1, NULL, NULL, 10),
(11, 11, 1, 'კოდი', '500', 1, NULL, NULL, 11),
(12, 12, 1, 'კოდი', '500', 1, NULL, NULL, 12),
(13, 13, 1, 'კოდი', '500', 1, NULL, NULL, 13),
(14, 14, 1, 'კოდი', '500', 1, NULL, NULL, 14),
(15, 15, 1, 'კოდი', '500', 1, NULL, NULL, 15),
(16, 16, 1, 'კოდი', '500', 1, NULL, NULL, 16),
(17, 17, 1, 'კოდი', '500', 1, NULL, NULL, 17),
(18, 18, 1, 'კოდი', '500', 1, NULL, NULL, 18),
(19, 19, 1, 'კოდი', '500', 1, NULL, NULL, 19),
(20, 20, 1, 'კოდი', '500', 1, NULL, NULL, 20),
(21, 21, 1, 'კოდი', '500', 1, NULL, NULL, 21),
(22, 22, 1, 'კოდი', '500', 1, NULL, NULL, 22),
(23, 23, 1, 'კოდი', '500', 1, NULL, NULL, 23),
(24, 24, 1, 'კოდი', '500', 1, NULL, NULL, 24),
(25, 25, 1, 'კოდი', '500', 1, NULL, NULL, 25),
(26, 26, 1, 'კოდი', '500', 1, NULL, NULL, 26),
(27, 27, 1, 'კოდი', '500', 1, NULL, NULL, 27),
(28, 28, 1, 'კოდი', '500', 1, NULL, NULL, 28),
(29, 29, 1, 'კოდი', '500', 1, NULL, NULL, 29),
(30, 30, 1, 'კოდი', '500', 1, NULL, NULL, 30),
(31, 31, 1, 'კოდი', '500', 1, NULL, NULL, 31),
(32, 32, 1, 'კოდი', '500', 1, NULL, NULL, 32),
(33, 33, 1, 'კოდი', '500', 1, NULL, NULL, 33),
(34, 34, 1, 'კოდი', '500', 1, NULL, NULL, 34),
(35, 35, 1, 'კოდი', '500', 1, NULL, NULL, 35),
(36, 36, 1, 'კოდი', '500', 1, NULL, NULL, 36),
(37, 37, 1, 'კოდი', '500', 1, NULL, NULL, 37),
(38, 38, 1, 'კოდი', '500', 1, NULL, NULL, 38),
(39, 39, 1, 'კოდი', '500', 1, NULL, NULL, 39),
(40, 40, 1, 'კოდი', '500', 1, NULL, NULL, 40),
(41, 41, 1, 'კოდი', '500', 1, NULL, NULL, 41),
(42, 42, 1, 'კოდი', '500', 1, NULL, NULL, 42),
(43, 43, 1, 'კოდი', '500', 1, NULL, NULL, 43),
(44, 44, 1, 'კოდი', '500', 1, NULL, NULL, 44),
(45, 45, 1, 'კოდი', '500', 1, NULL, NULL, 45),
(46, 46, 1, 'კოდი', '500', 1, NULL, NULL, 46),
(47, 47, 1, 'კოდი', '500', 1, NULL, NULL, 47),
(48, 48, 1, 'კოდი', '500', 1, NULL, NULL, 48),
(49, 49, 1, 'კოდი', '500', 1, NULL, NULL, 49),
(50, 50, 1, 'კოდი', '505', 1, NULL, NULL, 50),
(51, 51, 1, 'კოდი', '505', 1, NULL, NULL, 51),
(52, 52, 1, 'კოდი', '505', 1, NULL, NULL, 52),
(53, 53, 1, 'კოდი', '505', 1, NULL, NULL, 53),
(54, 54, 1, 'კოდი', '505', 1, NULL, NULL, 54),
(55, 55, 1, 'კოდი', '505', 1, NULL, NULL, 55),
(56, 56, 1, 'კოდი', '505', 1, NULL, NULL, 56),
(57, 57, 1, 'კოდი', '505', 1, NULL, NULL, 57),
(58, 58, 1, 'კოდი', '505', 1, NULL, NULL, 58),
(59, 59, 1, 'კოდი', '505', 1, NULL, NULL, 59),
(60, 60, 1, 'კოდი', '505', 1, NULL, NULL, 60),
(61, 61, 1, 'კოდი', '505', 1, NULL, NULL, 61),
(62, 62, 1, 'კოდი', '505', 1, NULL, NULL, 62),
(63, 63, 1, 'კოდი', '505', 1, NULL, NULL, 63),
(64, 64, 1, 'კოდი', '505', 1, NULL, NULL, 64),
(65, 65, 1, 'კოდი', '505', 1, NULL, NULL, 65),
(66, 66, 1, 'კოდი', '505', 1, NULL, NULL, 66),
(67, 67, 1, 'კოდი', '510', 1, NULL, NULL, 67),
(68, 68, 1, 'კოდი', '510', 1, NULL, NULL, 68),
(69, 69, 1, 'კოდი', '510', 1, NULL, NULL, 69),
(70, 70, 1, 'კოდი', '510', 1, NULL, NULL, 70),
(71, 71, 1, 'კოდი', '510', 1, NULL, NULL, 71),
(72, 72, 1, 'კოდი', '510', 1, NULL, NULL, 72),
(73, 73, 1, 'კოდი', '510', 1, NULL, NULL, 73),
(74, 74, 1, 'კოდი', '515', 1, NULL, NULL, 74),
(75, 75, 1, 'კოდი', '515', 1, NULL, NULL, 75),
(76, 76, 1, 'კოდი', '515', 1, NULL, NULL, 76),
(77, 77, 1, 'კოდი', '515', 1, NULL, NULL, 77),
(78, 78, 1, 'კოდი', '515', 1, NULL, NULL, 78),
(79, 79, 1, 'კოდი', '515', 1, NULL, NULL, 79),
(80, 80, 1, 'კოდი', '515', 1, NULL, NULL, 80),
(81, 81, 1, 'კოდი', '515', 1, NULL, NULL, 81),
(82, 82, 1, 'კოდი', '515', 1, NULL, NULL, 82),
(83, 83, 1, 'კოდი', '515', 1, NULL, NULL, 83),
(84, 84, 1, 'კოდი', '515', 1, NULL, NULL, 84),
(85, 85, 1, 'კოდი', '515', 1, NULL, NULL, 85),
(86, 86, 1, 'კოდი', '515', 1, NULL, NULL, 86),
(87, 87, 1, 'კოდი', '515', 1, NULL, NULL, 87),
(88, 88, 1, 'კოდი', '515', 1, NULL, NULL, 88),
(89, 89, 1, 'კოდი', '515', 1, NULL, NULL, 89),
(90, 90, 1, 'კოდი', '515', 1, NULL, NULL, 90),
(91, 91, 1, 'კოდი', '515', 1, NULL, NULL, 91),
(92, 92, 1, 'კოდი', '515', 1, NULL, NULL, 92),
(93, 93, 1, 'კოდი', '515', 1, NULL, NULL, 93),
(94, 94, 1, 'კოდი', '515', 1, NULL, NULL, 94),
(95, 95, 1, 'კოდი', '515', 1, NULL, NULL, 95),
(96, 96, 1, 'კოდი', '515', 1, NULL, NULL, 96),
(97, 97, 1, 'კოდი', '515', 1, NULL, NULL, 97),
(98, 98, 1, 'კოდი', '515', 1, NULL, NULL, 98),
(99, 99, 1, 'კოდი', '515', 1, NULL, NULL, 99),
(100, 100, 1, 'კოდი', '515', 1, NULL, NULL, 100),
(101, 101, 1, 'კოდი', '515', 1, NULL, NULL, 101),
(102, 102, 1, 'კოდი', '515', 1, NULL, NULL, 102),
(103, 103, 1, 'კოდი', '515', 1, NULL, NULL, 103),
(104, 104, 1, 'კოდი', '515', 1, NULL, NULL, 104),
(105, 105, 1, 'კოდი', '515', 1, NULL, NULL, 105),
(106, 106, 1, 'კოდი', '515', 1, NULL, NULL, 106),
(107, 107, 1, 'კოდი', '515', 1, NULL, NULL, 107),
(108, 108, 1, 'კოდი', '515', 1, NULL, NULL, 108),
(109, 109, 1, 'კოდი', '515', 1, NULL, NULL, 109),
(110, 110, 1, 'კოდი', '515', 1, NULL, NULL, 110),
(111, 111, 1, 'კოდი', '515', 1, NULL, NULL, 111),
(112, 112, 1, 'კოდი', '515', 1, NULL, NULL, 112),
(113, 113, 1, 'კოდი', '515', 1, NULL, NULL, 113),
(114, 114, 1, 'კოდი', '515', 1, NULL, NULL, 114),
(115, 115, 1, 'კოდი', '515', 1, NULL, NULL, 115),
(116, 116, 1, 'კოდი', '515', 1, NULL, NULL, 116),
(117, 117, 1, 'კოდი', '515', 1, NULL, NULL, 117),
(118, 118, 1, 'კოდი', '515', 1, NULL, NULL, 118),
(119, 119, 1, 'კოდი', '515', 1, NULL, NULL, 119),
(120, 120, 1, 'კოდი', '515', 1, NULL, NULL, 120),
(121, 121, 1, 'კოდი', '515', 1, NULL, NULL, 121),
(122, 122, 1, 'კოდი', '515', 1, NULL, NULL, 122),
(123, 123, 1, 'კოდი', '515', 1, NULL, NULL, 123),
(124, 124, 1, 'კოდი', '515', 1, NULL, NULL, 124),
(125, 125, 1, 'კოდი', '515', 1, NULL, NULL, 125),
(126, 126, 1, 'კოდი', '515', 1, NULL, NULL, 126),
(127, 127, 1, 'კოდი', '515', 1, NULL, NULL, 127),
(128, 128, 1, 'კოდი', '515', 1, NULL, NULL, 128),
(129, 129, 1, 'კოდი', '515', 1, NULL, NULL, 129),
(130, 130, 1, 'კოდი', '515', 1, NULL, NULL, 130),
(131, 131, 1, 'კოდი', '515', 1, NULL, NULL, 131),
(132, 132, 1, 'კოდი', '515', 1, NULL, NULL, 132),
(133, 133, 1, 'კოდი', '515', 1, NULL, NULL, 133),
(134, 134, 1, 'კოდი', '515', 1, NULL, NULL, 134),
(135, 135, 1, 'კოდი', '515', 1, NULL, NULL, 135),
(136, 136, 1, 'კოდი', '515', 1, NULL, NULL, 136),
(137, 137, 1, 'კოდი', '515', 1, NULL, NULL, 137),
(138, 138, 1, 'კოდი', '520', 1, NULL, NULL, 138),
(139, 139, 1, 'კოდი', '520', 1, NULL, NULL, 139),
(140, 140, 1, 'კოდი', '520', 1, NULL, NULL, 140),
(141, 141, 1, 'კოდი', '520', 1, NULL, NULL, 141),
(142, 142, 1, 'კოდი', '520', 1, NULL, NULL, 142),
(143, 143, 1, 'კოდი', '520', 1, NULL, NULL, 143),
(144, 144, 1, 'კოდი', '520', 1, NULL, NULL, 144),
(145, 145, 1, 'კოდი', '520', 1, NULL, NULL, 145),
(146, 146, 1, 'კოდი', '520', 1, NULL, NULL, 146),
(147, 147, 1, 'კოდი', '520', 1, NULL, NULL, 147),
(148, 567, 1, 'კოდი', '123', 1, '2020-12-23 21:47:34', '2020-12-23 21:47:34', 148),
(149, 589, 1, 'კოდი', '90190', 1, '2021-01-13 16:04:05', '2021-01-13 16:04:05', 149),
(150, 589, 4, 'ზომა', '90 x 190 სმ', 1, '2021-01-19 20:59:12', '2021-01-14 11:59:43', 152),
(151, 589, 6, 'მასალა', 'ლამინატი', 1, '2021-01-19 20:59:12', '2021-01-15 13:34:30', 151),
(152, 589, 7, 'სათავსო', 'აქვს', 1, '2021-01-19 20:59:02', '2021-01-15 13:39:31', 153),
(153, 589, 3, 'ბრენდი', 'io-Max', 1, '2021-01-19 20:59:06', '2021-01-15 13:39:57', 150),
(154, 582, 1, 'კოდი', 'DST', 1, '2021-01-15 16:34:00', '2021-01-15 16:34:00', 154),
(155, 582, 4, 'ზომა', '45X35X35', 1, '2021-01-15 16:35:21', '2021-01-15 16:35:21', 155),
(156, 582, 6, 'მასალა', 'ლამინატი', 1, '2021-01-15 16:35:36', '2021-01-15 16:35:36', 156),
(157, 582, 3, 'ბრენდი', 'io-Max', 1, '2021-01-15 16:36:09', '2021-01-15 16:36:09', 157),
(158, 583, 1, 'კოდი', 'DST', 1, '2021-01-15 16:39:16', '2021-01-15 16:39:16', 158),
(159, 583, 4, 'ზომა', '45 X 35 X 35', 1, '2021-01-15 16:39:34', '2021-01-15 16:39:34', 159),
(160, 583, 6, 'მასალა', 'ლამინატი', 1, '2021-01-15 16:39:48', '2021-01-15 16:39:48', 160),
(161, 583, 3, 'ბრენდი', 'io-Max', 1, '2021-01-15 16:39:56', '2021-01-15 16:39:56', 161),
(162, 584, 1, 'კოდი', 'DST', 1, '2021-01-15 16:41:53', '2021-01-15 16:41:53', 162),
(163, 584, 4, 'ზომა', '45 X 35 X 35', 1, '2021-01-15 16:42:04', '2021-01-15 16:42:04', 163),
(164, 584, 6, 'მასალა', 'ლამინატი', 1, '2021-01-15 16:42:12', '2021-01-15 16:42:12', 164),
(165, 584, 3, 'ბრენდი', 'io-Max', 1, '2021-01-15 16:42:28', '2021-01-15 16:42:28', 165),
(166, 585, 1, 'კოდი', 'DST', 1, '2021-01-15 16:43:00', '2021-01-15 16:43:00', 166),
(167, 585, 4, 'ზომა', '45X35X35', 1, '2021-01-15 16:43:13', '2021-01-15 16:43:13', 167),
(168, 585, 6, 'მასალა', 'ლამინატი', 1, '2021-01-15 16:43:21', '2021-01-15 16:43:21', 168),
(169, 585, 3, 'ბრენდი', 'io-Max', 1, '2021-01-15 16:43:28', '2021-01-15 16:43:28', 169);

-- --------------------------------------------------------

--
-- Table structure for table `product_info_types`
--

CREATE TABLE `product_info_types` (
  `product_info_type_id` int(11) NOT NULL,
  `product_info_type_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_info_types`
--

INSERT INTO `product_info_types` (`product_info_type_id`, `product_info_type_name`, `created_at`, `updated_at`) VALUES
(1, 'კოდი', NULL, NULL),
(2, 'TOLSEN', '2020-03-18 10:37:11', '2020-03-18 14:37:11'),
(3, 'ბრენდი', '2020-03-18 10:38:21', '2020-03-18 14:38:21'),
(4, 'ზომა', '2021-01-14 11:59:26', '2021-01-14 11:59:26'),
(5, 'ლამინატი', '2021-01-15 13:34:01', '2021-01-15 13:34:01'),
(6, 'მასალა', '2021-01-15 13:34:23', '2021-01-15 13:34:23'),
(7, 'სათავსო', '2021-01-15 13:39:17', '2021-01-15 13:39:17'),
(8, 'სიმაღლე', '2021-01-15 16:32:54', '2021-01-15 16:32:54');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Test 1', 'Test 1', 'Test 1', '2017-06-05 05:56:28', '2017-06-05 05:56:28'),
(2, 'Test 2', 'Test 2', 'Test 2', '2017-06-05 05:56:39', '2017-06-05 05:56:39'),
(3, 'Test 3', 'Test 3', 'Test 3', '2017-06-05 05:56:57', '2017-06-05 05:56:57');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(4, 3);

-- --------------------------------------------------------

--
-- Table structure for table `sell`
--

CREATE TABLE `sell` (
  `sell_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `whole_price` decimal(10,2) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `shipping_price` decimal(10,2) DEFAULT NULL,
  `payment_type` int(2) NOT NULL,
  `transaction_id` varchar(32) DEFAULT NULL,
  `state` int(2) DEFAULT 1,
  `note` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `first_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `last_name` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `phone` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `company` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `country` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `city` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `invoice_id` varchar(50) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sell`
--

INSERT INTO `sell` (`sell_id`, `user_id`, `whole_price`, `city_id`, `address`, `shipping_price`, `payment_type`, `transaction_id`, `state`, `note`, `created_at`, `updated_at`, `first_name`, `last_name`, `email`, `phone`, `company`, `country`, `city`, `invoice_id`) VALUES
(59, NULL, '490.00', NULL, 'იუშემავიჩის ქუჩა14', NULL, 2, 'a9948535-2e45-4e1c-b51d-cee61828', 1, 'სათავსო ხომ აქვს', '2021-02-16 22:58:57', '2021-02-16 22:58:57', 'ლელა', 'შეყრილაძე', 'Lela.sheyriladze@mail.ru', '555678487', 'P.s.p.farma', '1', 'თბილისი', '1613501936'),
(60, NULL, '1200.00', NULL, 'თბილისი, გაბრიელ სალოსის გამზირი 45, ბ3', NULL, 2, 'cd4e0cdd-f50b-4eb3-ab71-1a9a0ea2', 1, '', '2021-02-19 16:06:35', '2021-02-19 16:06:35', 'ნინო', 'შუბითიძე', 'ninoshubitidze@gmail.com', '557761569', '', '1', 'თბილისი', '1613736393'),
(61, NULL, '490.00', NULL, '', NULL, 2, 'b4fba876-c288-43cb-a6e6-d0d63e0d', 1, '', '2021-02-22 13:31:05', '2021-02-22 13:31:05', '', '', '', '', '', '1', '', '1613986264'),
(62, NULL, '1550.00', NULL, '', NULL, 3, NULL, 1, '', '2021-03-02 21:29:31', '2021-03-02 21:29:31', 'sad', 'sadsa', '', '', '', '1', '', '1614706171'),
(63, NULL, '1550.00', NULL, '', NULL, 3, NULL, 1, '', '2021-03-02 21:38:21', '2021-03-02 21:38:21', '', '', '', '', '', '1', '', '1614706701'),
(64, NULL, '1550.00', NULL, '', NULL, 3, NULL, 1, '', '2021-03-02 21:41:15', '2021-03-02 21:41:15', '', '', '', '', '', '1', '', '1614706875'),
(65, NULL, '1550.00', NULL, '', NULL, 3, NULL, 1, '', '2021-03-02 22:06:20', '2021-03-02 22:06:20', '', '', '', '', '', '1', '', '1614708380'),
(66, NULL, '640.00', NULL, 'ფერისცვალების ქ.9', NULL, 3, NULL, 1, '', '2021-03-09 16:23:47', '2021-03-09 16:23:47', 'ნინო', 'არველაძე', 'ninoarveladze@yahoo.com', '55428858', '', '1', 'თბილისი', '1615292627'),
(67, NULL, '1550.00', NULL, 'asdasdas', NULL, 3, NULL, 1, '', '2021-03-09 16:27:00', '2021-03-09 16:27:00', 'dfsd', 'fsdfsd', 'fsdfsd', 'fsdfsd', '', '1', 'sdfsdf', '1615292820'),
(68, NULL, '1550.00', NULL, 'sadasdsa', NULL, 3, NULL, 1, '', '2021-03-09 17:19:01', '2021-03-09 17:19:01', 'asd', 'asdasdsad', 'dasdsad', 'sadas', '', '1', 'asdasdsa', '1615295941'),
(69, NULL, '320.00', NULL, 'ფერისცვალების ქ.9', NULL, 3, NULL, 1, '', '2021-03-09 17:20:25', '2021-03-09 17:20:25', 'ნინო', 'არველაძე', 'ninoarveladze@yahoo.com', '55428858', '', '1', 'თბილისი', '1615296025'),
(70, NULL, '1080.00', NULL, 'წალკა, სოფ. საყდრიონი', NULL, 2, NULL, 1, '', '2021-03-12 18:08:47', '2021-03-12 18:08:47', 'თემური', 'სურმანიძე', 't.surmanidze@gmail.com', '568202761', '', '1', 'წალკა', '1615558126'),
(71, NULL, '1080.00', NULL, 'წალკა, საყდრიონი', NULL, 2, NULL, 1, '', '2021-03-12 18:10:29', '2021-03-12 18:10:29', 'თემური', 'სურმანიძე', 't.surmanidze@gmail.com', '568202761', '', '1', 'წალკა', '1615558228'),
(72, NULL, '1080.00', NULL, 'წალკა, საყდრიონი', NULL, 2, NULL, 1, '', '2021-03-12 18:12:30', '2021-03-12 18:12:30', 'თემური', 'სურმანიძე', 't.surmanidze@gmail.com', '568202761', '', '1', 'წალკა', '1615558349'),
(73, NULL, '1080.00', NULL, 'წალკა, საყდრიონი', NULL, 2, NULL, 1, '', '2021-03-12 18:16:30', '2021-03-12 18:16:30', 'თემური', 'სურმანიძე', 't.surmanidze@gmail.com', '568202761', '', '1', 'წალკა', '1615558590'),
(74, NULL, '490.00', NULL, 'თბილისი, გაბრიელ სალოსის გამზირი 45, ბ3', NULL, 2, '8e49e743-2422-4c7b-91e6-507ce721', 1, '', '2021-03-12 18:19:41', '2021-03-12 18:19:41', 'ნინო', 'შუბითიძე', 'ninoshubitidze@gmail.com', '555555555', '', '1', 'თბილისი', '1615558780'),
(75, NULL, '1080.00', NULL, 'წალკა, საყდრიონი', NULL, 2, NULL, 1, '', '2021-03-12 18:20:59', '2021-03-12 18:20:59', 'თემური', 'სურმანიძე', 't.surmanidze@gmail.com', '568202761', '', '1', 'წალკა', '1615558859'),
(76, NULL, '1080.00', NULL, 'წალკა, სოფ. საყდრიონი', NULL, 2, NULL, 1, '', '2021-03-12 18:24:43', '2021-03-12 18:24:43', 'თემური', 'სურმანიძე', 't.surmanidze@gmail.com', '568202761', '', '1', 'წალკა', '1615559082'),
(77, NULL, '1080.00', NULL, 'წალკა, სოფ. საყდრიონი', NULL, 2, 'd427c9f0-ca79-4e59-ad95-7acfc727', 1, '', '2021-03-12 18:27:21', '2021-03-12 18:27:21', 'თემური', 'სურმანიძე', 't.surmanidze@gmail.com', '568202761', '', '1', 'წალკა', '1615559240'),
(78, NULL, '2630.00', NULL, 'test', NULL, 2, 'e1590cf7-d7ae-4496-a08e-bba783eb', 1, 'test', '2021-03-12 19:20:59', '2021-03-12 19:20:59', 'test', 'test', 'test', 'test', '', '1', 'test', '1615562458'),
(79, NULL, '5260.00', NULL, 'ასდსად', NULL, 2, NULL, 1, '', '2021-03-12 20:44:11', '2021-03-12 20:44:11', 'ასდ', 'ასდსა', 'სადსა', 'დსადას', 'ასდსა', '1', 'სადსადსა', '1615567450'),
(80, NULL, '2160.00', NULL, 'სადსადსა', NULL, 2, NULL, 1, '', '2021-03-12 20:44:42', '2021-03-12 20:44:42', 'ასდსა', 'დსადსა', 'სადსა', 'დსად', '', '1', 'სადსადსა', '1615567481'),
(81, NULL, '2160.00', NULL, 'sadsadsa', NULL, 2, NULL, 1, 'asdsadsa', '2021-03-12 20:47:12', '2021-03-12 20:47:12', 'sad', 'sadsa', 'sadsa', 'dsad', '', '1', 'sadsad', '1615567631'),
(82, NULL, '4320.00', NULL, 'dsadsa', NULL, 2, '960c6006-422e-49f0-8454-aa0aa667', 1, 'dsadsa', '2021-03-12 23:46:58', '2021-03-12 23:46:58', 'asd', 'sadsad', 'dsad', 'sadsa', '', '1', 'asdsa', '1615578417'),
(83, NULL, '1960.00', NULL, 'Goris raioni sopeli ateni', NULL, 2, '3c3821a4-3c7b-467b-aab0-15f69f76', 1, '', '2021-03-15 21:14:09', '2021-03-15 21:14:09', 'ნინო', 'ბოჟაძე', 'Nino.bojadze1990@mail.ru', '551105544', '', '1', 'Gori', '1615828448'),
(84, NULL, '1470.00', NULL, 'Goris raioni sopeli ateni', NULL, 2, 'f72973cd-25f0-448b-8d9d-9e845a4b', 1, '', '2021-03-15 21:50:58', '2021-03-15 21:50:58', 'ნინო', 'ბოჟაძე', 'Nino.bojadze1990@mail.ru', '551105544', '', '1', 'Gori', '1615830657'),
(85, NULL, '80.00', NULL, 'asdsad', NULL, 4, NULL, 1, 'asdsa', '2021-03-18 16:29:55', '2021-03-18 16:29:55', 'sad', 'sadsadasd', 'asdas', 'asdsad', '', '1', 'asdas', '1616070595'),
(86, NULL, '1250.00', NULL, 'asdasdsa', NULL, 4, NULL, 1, '', '2021-03-18 16:31:20', '2021-03-18 16:31:20', 'asdas', 'dasdsa', 'sadas', 'dasd', '', '1', 'asdsadas', '1616070680'),
(87, NULL, '1250.00', NULL, 'asdasdsa', NULL, 4, NULL, 1, '', '2021-03-18 16:32:33', '2021-03-18 16:32:33', 'asd', 'asdsadsad', 'asdasd', 'adas', '', '1', 'asdasd', '1616070753'),
(88, NULL, '1250.00', NULL, 'asdasdas', NULL, 4, NULL, 1, '', '2021-03-18 16:34:34', '2021-03-18 16:34:34', 'asds', 'adsadsa', 'asadsad', 'dad', '', '1', 'asdadas', '1616070874'),
(89, NULL, '1250.00', NULL, 'asdasdsa', NULL, 4, NULL, 1, '', '2021-03-18 16:35:28', '2021-03-18 16:35:28', 'asds', 'adsadsa', 'sadas', 'sad', '', '1', 'dasdsa', '1616070928'),
(90, NULL, '3180.00', NULL, 'სდფსდფსდ', NULL, 4, NULL, 1, '', '2021-03-18 16:37:08', '2021-03-18 16:37:08', 'სდფდს', 'ფსდფსდ', 'ფსდფდს', 'ფსდფსდ', '', '1', 'სდფსდფსდ', '1616071028'),
(91, NULL, '1100.00', NULL, 'თბილისი, გაბრიელ სალოსის გამზირი 45, ბ3', NULL, 4, NULL, 1, '', '2021-03-18 16:52:20', '2021-03-18 16:52:20', 'ნინო', 'შუბითიძე', 'ninoshubitidze@gmail.com', '555555555', '', '1', 'თბილისი', '1616071940'),
(92, NULL, '320.00', NULL, 'asdasdas', NULL, 4, NULL, 1, '', '2021-03-18 16:56:17', '2021-03-18 16:56:17', 'asds', 'adsaas', 'dsada', 'dsadsa', '', '1', 'das', '1616072177'),
(93, NULL, '1100.00', NULL, 'თბილისი, გაბრიელ სალოსის გამზირი 45, ბ3', NULL, 4, NULL, 1, '', '2021-03-18 17:01:34', '2021-03-18 17:01:34', 'ნინო', 'შუბითიძე', 'ninoshubitidze@gmail.com', '557722013', '', '1', 'თბილისი', '1616072494'),
(94, NULL, '1100.00', NULL, 'sdfsdfsd', NULL, 4, NULL, 1, '', '2021-03-18 17:01:42', '2021-03-18 17:01:42', 'dfdsfdsf', 'sdfdsfd', 'sdfsd', 'fsdf', '', '1', 'sdfsdfsd', '1616072502'),
(95, NULL, '1100.00', NULL, 'sadsada', NULL, 4, NULL, 1, '', '2021-03-18 17:04:33', '2021-03-18 17:04:33', 'asdsad', 'adsadsadsa', 'adsa', 'dsads', '', '1', 'dsadsasa', '1616072673'),
(96, NULL, '490.00', NULL, 'asdsad', NULL, 4, NULL, 1, '', '2021-03-18 17:05:15', '2021-03-18 17:05:15', 'asdsad', 'sadsasda', 'das', 'dsadas', '', '1', 'asdasdas', '1616072715'),
(97, NULL, '1100.00', NULL, 'თბილისი, გაბრიელ სალოსის გამზირი 45, ბ3', NULL, 3, NULL, 1, '', '2021-03-23 13:13:18', '2021-03-23 13:13:18', 'ნინო', 'შუბითიძე', 'ninoshubitidze@gmail.com', '5451', '', '1', 'თბილისი', '1616490798'),
(98, NULL, '490.00', NULL, 'თბილისი, გაბრიელ სალოსის გამზირი 45, ბ3', NULL, 4, NULL, 1, '', '2021-03-23 13:14:14', '2021-03-23 13:14:14', 'ნინო', 'შუბითიძე', 'ninoshubitidze@gmail.com', '5451', '', '1', 'თბილისი', '1616490854'),
(99, NULL, '490.00', NULL, 'თბილისი, გაბრიელ სალოსის გამზირი 45, ბ3', NULL, 2, 'f06c1498-f831-4eb7-a2d1-b0f3d752', 1, '', '2021-03-23 13:14:50', '2021-03-23 13:14:50', 'ნინო', 'შუბითიძე', 'ninoshubitidze@gmail.com', '5451', '', '1', 'თბილისი', '1616490889'),
(100, NULL, '600.00', NULL, 'თბილისი, გაბრიელ სალოსის გამზირი 45, ბ3', NULL, 3, NULL, 1, '', '2021-03-24 11:47:52', '2021-03-24 11:47:52', 'ნინო', 'შუბითიძე', 'ninoshubitidze@gmail.com', '5451', '', '1', 'თბილისი', '1616572072'),
(101, NULL, '5625.00', NULL, 'Sagarejo kostavas 20', NULL, 4, NULL, 1, '', '2021-03-24 12:53:06', '2021-03-24 12:53:06', 'Tamazi', 'Simonishvili', 'Tsikarishvili1995@gmail.com', '571246047', '', '1', 'Sagarejo', '1616575986'),
(102, NULL, '1875.00', NULL, 'თბილისი, გაბრიელ სალოსის გამზირი 45, ბ3', NULL, 4, NULL, 1, '', '2021-03-24 12:53:09', '2021-03-24 12:53:09', 'ნინო', 'შუბითიძე', 'ninoshubitidze@gmail.com', '5451', '', '1', 'თბილისი', '1616575989'),
(103, NULL, '1875.00', NULL, 'თბილისი, გაბრიელ სალოსის გამზირი 45, ბ3', NULL, 4, NULL, 1, '', '2021-03-24 13:00:55', '2021-03-24 13:00:55', 'ნინო', 'შუბითიძე', 'ninoshubitidze@gmail.com', '5451', '', '1', 'თბილისი', '1616576455'),
(104, NULL, '1875.00', NULL, 'Sagarejo kostavas 20 bina 30', NULL, 4, NULL, 1, '', '2021-03-24 13:00:56', '2021-03-24 13:00:56', 'Tamazi', 'Simonishvili', 'Tsikarishvili1995@gmail.com', '571246047', '', '1', 'Sagarejo', '1616576456'),
(105, NULL, '1875.00', NULL, 'Kostvas 20', NULL, 4, NULL, 1, '', '2021-03-24 13:35:23', '2021-03-24 13:35:23', 'Tamazi', 'Simonishvli', 'Tsikarishvili1995@gmail.com', '571246047', '', '1', 'Sagarjo', '1616578523'),
(106, NULL, '1875.00', NULL, 'საგარეჯო, კოსტავას 20, ბ30', NULL, 4, NULL, 1, '', '2021-03-24 14:07:21', '2021-03-24 14:07:21', 'თამაზი', 'სიმონიშვილი', 'vasilorjonikidze@yahoo.com', '571246047', '', '1', 'საგარეჯო', '1616580441'),
(107, NULL, '1875.00', NULL, 'საგარეჯო, კოსტავას 20, ბ30', NULL, 4, NULL, 1, '', '2021-03-24 16:28:30', '2021-03-24 16:28:30', 'თამაზი', 'სიმონიშვილი', 'vasilorjonikidze@yahoo.com', '571246047', '', '1', 'საგარეჯო', '1616588910'),
(108, NULL, '1875.00', NULL, 'საგარეჯო, კოსტავას 20', NULL, 4, NULL, 1, '', '2021-03-25 11:35:43', '2021-03-25 11:35:43', 'თამაზი', 'სიმონიშვილი', 'vasilorjonikidze@yahoo.com', '571246047', '', '1', 'საგარეჯო', '1616657743'),
(109, NULL, '1875.00', NULL, 'test', NULL, 1, NULL, 1, 'test', '2021-03-26 12:19:35', '2021-03-26 12:19:35', 'test', 'test', 'test', 'test', '', '1', 'test', '1616746775');

-- --------------------------------------------------------

--
-- Table structure for table `sell_items`
--

CREATE TABLE `sell_items` (
  `sell_item_id` int(11) NOT NULL,
  `sell_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_price` decimal(10,2) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `whole_price` decimal(10,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` int(2) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sell_items`
--

INSERT INTO `sell_items` (`sell_item_id`, `sell_id`, `product_id`, `product_price`, `amount`, `whole_price`, `created_at`, `updated_at`, `state`) VALUES
(27, 59, 568, '490.00', '1.00', '490.00', '2021-02-16 22:58:57', '2021-02-16 22:58:57', 1),
(28, 60, 590, '100.00', '1.00', '100.00', '2021-02-19 16:06:35', '2021-02-19 16:06:35', 1),
(29, 60, 587, '1100.00', '1.00', '1100.00', '2021-02-19 16:06:35', '2021-02-19 16:06:35', 1),
(30, 61, 569, '490.00', '1.00', '490.00', '2021-02-22 13:31:05', '2021-02-22 13:31:05', 1),
(31, 62, 624, '1550.00', '1.00', '1550.00', '2021-03-02 21:29:31', '2021-03-02 21:29:31', 1),
(32, 63, 624, '1550.00', '1.00', '1550.00', '2021-03-02 21:38:21', '2021-03-02 21:38:21', 1),
(33, 64, 624, '1550.00', '1.00', '1550.00', '2021-03-02 21:41:15', '2021-03-02 21:41:15', 1),
(34, 65, 624, '1550.00', '1.00', '1550.00', '2021-03-02 22:06:20', '2021-03-02 22:06:20', 1),
(35, 66, 589, '320.00', '2.00', '640.00', '2021-03-09 16:23:47', '2021-03-09 16:23:47', 1),
(36, 67, 624, '1550.00', '1.00', '1550.00', '2021-03-09 16:27:00', '2021-03-09 16:27:00', 1),
(37, 68, 624, '1550.00', '1.00', '1550.00', '2021-03-09 17:19:01', '2021-03-09 17:19:01', 1),
(38, 69, 589, '320.00', '1.00', '320.00', '2021-03-09 17:20:25', '2021-03-09 17:20:25', 1),
(39, 70, 590, '100.00', '1.00', '100.00', '2021-03-12 18:08:47', '2021-03-12 18:08:47', 1),
(40, 70, 568, '490.00', '2.00', '980.00', '2021-03-12 18:08:47', '2021-03-12 18:08:47', 1),
(41, 71, 568, '490.00', '2.00', '980.00', '2021-03-12 18:10:29', '2021-03-12 18:10:29', 1),
(42, 71, 590, '100.00', '1.00', '100.00', '2021-03-12 18:10:29', '2021-03-12 18:10:29', 1),
(43, 72, 568, '490.00', '2.00', '980.00', '2021-03-12 18:12:30', '2021-03-12 18:12:30', 1),
(44, 72, 590, '100.00', '1.00', '100.00', '2021-03-12 18:12:30', '2021-03-12 18:12:30', 1),
(45, 73, 570, '490.00', '2.00', '980.00', '2021-03-12 18:16:30', '2021-03-12 18:16:30', 1),
(46, 73, 590, '100.00', '1.00', '100.00', '2021-03-12 18:16:30', '2021-03-12 18:16:30', 1),
(47, 74, 568, '490.00', '1.00', '490.00', '2021-03-12 18:19:41', '2021-03-12 18:19:41', 1),
(48, 75, 568, '490.00', '2.00', '980.00', '2021-03-12 18:20:59', '2021-03-12 18:20:59', 1),
(49, 75, 590, '100.00', '1.00', '100.00', '2021-03-12 18:20:59', '2021-03-12 18:20:59', 1),
(50, 76, 580, '120.00', '9.00', '1080.00', '2021-03-12 18:24:43', '2021-03-12 18:24:43', 1),
(51, 77, 625, '1080.00', '1.00', '1080.00', '2021-03-12 18:27:21', '2021-03-12 18:27:21', 1),
(52, 78, 625, '1080.00', '1.00', '1080.00', '2021-03-12 19:20:59', '2021-03-12 19:20:59', 1),
(53, 78, 624, '1550.00', '1.00', '1550.00', '2021-03-12 19:20:59', '2021-03-12 19:20:59', 1),
(54, 79, 625, '1080.00', '2.00', '2160.00', '2021-03-12 20:44:11', '2021-03-12 20:44:11', 1),
(55, 79, 624, '1550.00', '1.00', '1550.00', '2021-03-12 20:44:11', '2021-03-12 20:44:11', 1),
(56, 79, 623, '1550.00', '1.00', '1550.00', '2021-03-12 20:44:11', '2021-03-12 20:44:11', 1),
(57, 80, 625, '1080.00', '2.00', '2160.00', '2021-03-12 20:44:42', '2021-03-12 20:44:42', 1),
(58, 81, 625, '1080.00', '2.00', '2160.00', '2021-03-12 20:47:12', '2021-03-12 20:47:12', 1),
(59, 82, 625, '1080.00', '4.00', '4320.00', '2021-03-12 23:46:58', '2021-03-12 23:46:58', 1),
(60, 83, 568, '490.00', '4.00', '1960.00', '2021-03-15 21:14:09', '2021-03-15 21:14:09', 1),
(61, 84, 569, '490.00', '3.00', '1470.00', '2021-03-15 21:50:58', '2021-03-15 21:50:58', 1),
(62, 85, 626, '80.00', '1.00', '80.00', '2021-03-18 16:29:55', '2021-03-18 16:29:55', 1),
(63, 86, 625, '1250.00', '1.00', '1250.00', '2021-03-18 16:31:20', '2021-03-18 16:31:20', 1),
(64, 87, 625, '1250.00', '1.00', '1250.00', '2021-03-18 16:32:33', '2021-03-18 16:32:33', 1),
(65, 88, 625, '1250.00', '1.00', '1250.00', '2021-03-18 16:34:34', '2021-03-18 16:34:34', 1),
(66, 89, 625, '1250.00', '1.00', '1250.00', '2021-03-18 16:35:28', '2021-03-18 16:35:28', 1),
(67, 90, 624, '1550.00', '2.00', '3100.00', '2021-03-18 16:37:08', '2021-03-18 16:37:08', 1),
(68, 90, 626, '80.00', '1.00', '80.00', '2021-03-18 16:37:08', '2021-03-18 16:37:08', 1),
(69, 91, 591, '1100.00', '1.00', '1100.00', '2021-03-18 16:52:20', '2021-03-18 16:52:20', 1),
(70, 92, 589, '320.00', '1.00', '320.00', '2021-03-18 16:56:17', '2021-03-18 16:56:17', 1),
(71, 93, 591, '1100.00', '1.00', '1100.00', '2021-03-18 17:01:34', '2021-03-18 17:01:34', 1),
(72, 94, 591, '1100.00', '1.00', '1100.00', '2021-03-18 17:01:42', '2021-03-18 17:01:42', 1),
(73, 95, 591, '1100.00', '1.00', '1100.00', '2021-03-18 17:04:33', '2021-03-18 17:04:33', 1),
(74, 96, 568, '490.00', '1.00', '490.00', '2021-03-18 17:05:15', '2021-03-18 17:05:15', 1),
(75, 97, 591, '1100.00', '1.00', '1100.00', '2021-03-23 13:13:18', '2021-03-23 13:13:18', 1),
(76, 98, 568, '490.00', '1.00', '490.00', '2021-03-23 13:14:14', '2021-03-23 13:14:14', 1),
(77, 99, 569, '490.00', '1.00', '490.00', '2021-03-23 13:14:50', '2021-03-23 13:14:50', 1),
(78, 100, 627, '600.00', '1.00', '600.00', '2021-03-24 11:47:52', '2021-03-24 11:47:52', 1),
(79, 101, 628, '1875.00', '3.00', '5625.00', '2021-03-24 12:53:06', '2021-03-24 12:53:06', 1),
(80, 102, 628, '1875.00', '1.00', '1875.00', '2021-03-24 12:53:09', '2021-03-24 12:53:09', 1),
(81, 103, 628, '1875.00', '1.00', '1875.00', '2021-03-24 13:00:55', '2021-03-24 13:00:55', 1),
(82, 104, 628, '1875.00', '1.00', '1875.00', '2021-03-24 13:00:56', '2021-03-24 13:00:56', 1),
(83, 105, 628, '1875.00', '1.00', '1875.00', '2021-03-24 13:35:23', '2021-03-24 13:35:23', 1),
(84, 106, 628, '1875.00', '1.00', '1875.00', '2021-03-24 14:07:21', '2021-03-24 14:07:21', 1),
(85, 107, 628, '1875.00', '1.00', '1875.00', '2021-03-24 16:28:30', '2021-03-24 16:28:30', 1),
(86, 108, 628, '1875.00', '1.00', '1875.00', '2021-03-25 11:35:43', '2021-03-25 11:35:43', 1),
(87, 109, 628, '1875.00', '1.00', '1875.00', '2021-03-26 12:19:35', '2021-03-26 12:19:35', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shoppingcart`
--

CREATE TABLE `shoppingcart` (
  `identifier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instance` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `shoppingcart`
--

INSERT INTO `shoppingcart` (`identifier`, `instance`, `content`, `created_at`, `updated_at`) VALUES
('admin@gmail.com', 'default', 'O:29:\"Illuminate\\Support\\Collection\":1:{s:8:\"\0*\0items\";a:1:{s:32:\"5bc403a550d0dac7ed010621521cf893\";O:32:\"Gloudemans\\Shoppingcart\\CartItem\":8:{s:5:\"rowId\";s:32:\"5bc403a550d0dac7ed010621521cf893\";s:2:\"id\";s:2:\"17\";s:3:\"qty\";s:1:\"1\";s:4:\"name\";s:24:\"SAMSUNG RB37K63412C/WT/O\";s:5:\"price\";d:2688;s:7:\"options\";O:39:\"Gloudemans\\Shoppingcart\\CartItemOptions\":1:{s:8:\"\0*\0items\";a:1:{s:3:\"img\";s:78:\"http://gdc.pixl.ge/files/product_files/17/96fba45541f8539106f3b85d6df52a3d.jpg\";}}s:49:\"\0Gloudemans\\Shoppingcart\\CartItem\0associatedModel\";N;s:41:\"\0Gloudemans\\Shoppingcart\\CartItem\0taxRate\";i:21;}}}', NULL, NULL),
('g_schilakadse@hotmail.com', 'default', 'O:29:\"Illuminate\\Support\\Collection\":1:{s:8:\"\0*\0items\";a:1:{s:32:\"d922024ca896e31663b25437cbe0e18b\";O:32:\"Gloudemans\\Shoppingcart\\CartItem\":8:{s:5:\"rowId\";s:32:\"d922024ca896e31663b25437cbe0e18b\";s:2:\"id\";s:2:\"74\";s:3:\"qty\";s:1:\"4\";s:4:\"name\";s:129:\"ავეჯის ფეხი - ალუმინის ინოქსირებული 40 x 40 x 100 მმ / 01 - 00515\";s:5:\"price\";d:2.7999999999999998;s:7:\"options\";O:39:\"Gloudemans\\Shoppingcart\\CartItemOptions\":1:{s:8:\"\0*\0items\";a:1:{s:3:\"img\";s:81:\"http://www.sonniva.ge/files/product_files/74/1efd21ca2fa18415a5a916547da7bd10.jpg\";}}s:49:\"\0Gloudemans\\Shoppingcart\\CartItem\0associatedModel\";N;s:41:\"\0Gloudemans\\Shoppingcart\\CartItem\0taxRate\";i:21;}}}', NULL, NULL),
('kakha3@gmail.com', 'default', 'O:29:\"Illuminate\\Support\\Collection\":1:{s:8:\"\0*\0items\";a:0:{}}', NULL, NULL),
('test@yahoo.com', 'default', 'O:29:\"Illuminate\\Support\\Collection\":1:{s:8:\"\0*\0items\";a:0:{}}', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `birth_date` timestamp NULL DEFAULT NULL,
  `mobile_no` varchar(255) DEFAULT NULL,
  `personal_no` varchar(255) DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(555) DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `state_id` tinyint(1) NOT NULL DEFAULT 1,
  `bonus` decimal(10,2) DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `last_name`, `birth_date`, `mobile_no`, `personal_no`, `email`, `password`, `address`, `remember_token`, `created_at`, `updated_at`, `state_id`, `bonus`) VALUES
(1, 'კახა', 'თაბაგარი', '0000-00-00 00:00:00', '599 99 99 99', '123123123', 'admin@gmail.com', '$2y$10$b9Rs1OvVD/BThlNHmSGaauHgeTzWZsvj9IQJe.GH8hKudziWP0IRG', NULL, '', '2017-05-04 07:45:54', '2017-05-04 07:46:15', 1, '0.00'),
(2, 'Admin1', '', '0000-00-00 00:00:00', NULL, NULL, 'admin1@gmail.com', '$2y$10$b9Rs1OvVD/BThlNHmSGaauHgeTzWZsvj9IQJe.GH8hKudziWP0IRG', NULL, '', '2017-05-04 07:58:39', '0000-00-00 00:00:00', 1, '0.00'),
(3, 'KAKHA TABAGARI', '', '0000-00-00 00:00:00', NULL, NULL, 'tabagari89@gmail.com', '$2y$10$XVjVdez9ldUSXxTKNzinduYqNbtnX576wDfJ8B.fJp.ZRHVaOAZJq', '4 LEWIS CIR, G11403', 'HzWPeaGi1KyliNY1IU3qSDOmaXjNZLXYkiM9wK3AH2kb9TWsMQBG0uTqIKmR', '2017-06-27 09:32:28', '2017-06-27 09:32:28', 1, '0.00'),
(4, 'kakha', 'TABAGARI', NULL, NULL, NULL, 'asd@gmail.com', '$2y$10$I3l6uKYTjnR5YrOa/Bl4Q.ouO2K8lqgWK7vu8N6sxGHB5jXrYiumG', '12 th m/r', NULL, '2017-11-30 12:43:16', '2017-11-30 12:43:16', 1, '0.00'),
(5, 'test', 'test', NULL, NULL, NULL, 'test@yahoo.com', '$2y$10$CkEKIQ71hm5.cae83LjkVOvAd.RVMiiSpZrJAn.Aj3RB7E/Ct8FLG', NULL, 'ROkJYGqlgYDZoLPrjerAzaQlb5WoH2YpmNcMf3HbHLrx5PlyyPY7jKuksoF5', '2018-02-24 14:44:36', '2018-02-24 14:44:36', 1, '0.00'),
(10, 'test', 'test', NULL, NULL, NULL, 'test2@yahoo.com', '$2y$10$XAjTfuoaxg3KS.WW.dOJOuYJP4TVbA4Lz9CLlKC0A6PyEmwr.Oj1C', NULL, NULL, '2018-02-24 14:50:41', '2018-02-24 14:50:41', 1, '0.00'),
(11, 'test', 'test', NULL, NULL, NULL, 'test@gmail.com', '$2y$10$E/llgIzkssJQ6B6ORbK.dODANMoiMw1qaFw/wk5C0AdHH4xKW1MYe', NULL, NULL, '2018-02-24 14:51:01', '2018-02-24 14:51:01', 1, '0.00'),
(13, 'kakha', 'katsadze', NULL, NULL, NULL, 'kakha@gmail.com', '$2y$10$ZhJNOTcZ3P1ny9sMYt5xQ.jLSgI2N1asYum6tPBM8GspSriuCFV2y', NULL, NULL, '2018-02-24 14:53:47', '2018-02-24 14:53:47', 1, '0.00'),
(14, 'kakha', 'katsadze', NULL, NULL, NULL, 'kakha2@gmail.com', '$2y$10$6EJDgDypJFPfHdhVQuqKcuIhMVj3mBxKxOK8yzLz5QHW5nsy0dzD6', NULL, NULL, '2018-02-24 14:54:18', '2018-02-24 14:54:18', 1, '0.00'),
(15, 'kakha', 'katsadze', NULL, NULL, NULL, 'kakha3@gmail.com', '$2y$10$O65dnNV8x26QJ/N01t7WYOWQRatlK52splx92Mej2MuT9xxqbnUcq', NULL, 'naGKfBdmTj35KCkV9tRwknhBjvWX0NyQlzzrcHR8OgHSPzkURlgWvWpe0P43', '2018-02-24 14:56:48', '2018-02-24 14:56:48', 1, '0.00'),
(16, 'test', 'test', NULL, NULL, NULL, 'test2@gmail.com', '$2y$10$nTo5s1kw/eFRk8BjK7L4dOh4NQcwDlwqZHuCONTxo9agR0MfgNcXC', NULL, NULL, '2018-02-24 14:57:41', '2018-02-24 14:57:41', 1, '0.00'),
(19, 'test', 'test', NULL, NULL, NULL, 'test3@gmail.com', '$2y$10$EwAuDEpbpSCMlBkPTZqzR.OPoJR7.JYmd9vfkqyq5bkXTYYwjU1Wi', NULL, NULL, '2018-02-24 14:58:45', '2018-02-24 14:58:45', 1, '0.00'),
(20, 'დავით', 'ბათუმაშვილი', NULL, NULL, NULL, 'davitbatumashvili@gmail.com', '$2y$10$mipeXCd8e6FLlwMDNLQ2ZeO/fPOEX2mn6ch1J76bPMzTNO2N3gZAu', 'თეთნულდის #34. ჩიხი', NULL, '2020-03-13 12:41:59', '2020-03-13 12:41:59', 1, '0.00'),
(27, 'ჯონი', 'დევრისაშვილი', NULL, NULL, NULL, 'jobyde@mail.ru', '$2y$10$9o.bPl3lBLTcgCuIHP7WQuM/HDMSL77JxORTM/jO38F6WqxejmiUq', NULL, NULL, '2020-03-16 11:18:47', '2020-03-16 11:18:47', 1, '0.00'),
(28, 'ჯონი', 'დევრისაშვილი', NULL, NULL, NULL, 'jonyde@mail.ru', '$2y$10$MRSn88F5muwuVnUcyi9HVeWK0V7iZDvCsDJBW9Ioi3InI5Eil3lyK', NULL, NULL, '2020-03-16 11:19:55', '2020-03-16 11:19:55', 1, '0.00'),
(31, 'დავით', 'ცქიტიშვილი', NULL, NULL, NULL, 'ckito@gmail.com', '$2y$10$XD1ahMgSdlq2YbEVH3oIsewcNRWH.3frlwetXs.QfyZ.3S9bUPW.6', NULL, NULL, '2020-03-16 12:17:56', '2020-03-16 12:17:56', 1, '0.00'),
(32, 'გიორგი', 'შილაკაძე', NULL, NULL, NULL, 'g_schilakadse@hotmail.com', '$2y$10$pXNZfGwEWUV/0xztxHyJ/eh4sPLCkhOc.u5ka8JuvS8Y9snVtZ3ju', NULL, 'EG6cDTrpmRLYDhPRCpbiqYWPBHnId78uru2LWy6zpO76gILsIb8M7H0YfL4v', '2020-04-28 17:58:05', '2020-04-28 17:58:05', 1, '0.00'),
(36, 'ლევან', 'კემულარია', NULL, NULL, NULL, 'levankemularia@gmail.com', '$2y$10$mJgAd3toGfiT3e84HWD4m.gHnzJroBKflZ0C/EaRvt19UeYeqtpbC', 'ყაზბეგის 3 ბინა 2', NULL, '2020-05-05 08:01:48', '2020-05-05 08:01:48', 1, '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `user_addresses`
--

CREATE TABLE `user_addresses` (
  `address_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `primary` int(1) DEFAULT 0,
  `state` int(2) DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_addresses`
--

INSERT INTO `user_addresses` (`address_id`, `user_id`, `city_id`, `address`, `primary`, `state`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 'qucha, 2,3', 1, 1, '2017-12-25 13:11:55', '2017-12-25 17:11:55'),
(2, 5, 1, NULL, 0, 1, '2018-02-24 14:44:36', '2018-02-24 18:44:36'),
(3, 10, 1, NULL, 0, 1, '2018-02-24 14:50:41', '2018-02-24 18:50:41'),
(4, 11, 1, NULL, 0, 1, '2018-02-24 14:51:01', '2018-02-24 18:51:01'),
(5, 13, 1, NULL, 0, 1, '2018-02-24 14:53:47', '2018-02-24 18:53:47'),
(6, 14, 1, NULL, 0, 1, '2018-02-24 14:54:18', '2018-02-24 18:54:18'),
(7, 15, 1, NULL, 0, 1, '2018-02-24 14:56:48', '2018-02-24 18:56:48'),
(8, 16, 1, NULL, 0, 1, '2018-02-24 14:57:41', '2018-02-24 18:57:41'),
(9, 19, 1, NULL, 0, 1, '2018-02-24 14:58:45', '2018-02-24 18:58:45'),
(10, 20, 3, 'თეთნულდის #34. ჩიხი', 0, 1, '2020-03-13 12:41:59', '2020-03-13 16:41:59'),
(11, 27, 3, NULL, 0, 1, '2020-03-16 11:18:47', '2020-03-16 15:18:47'),
(12, 28, 3, NULL, 0, 1, '2020-03-16 11:19:55', '2020-03-16 15:19:55'),
(13, 31, 3, NULL, 0, 1, '2020-03-16 12:17:56', '2020-03-16 16:17:56'),
(14, 32, 3, NULL, 0, 1, '2020-04-28 17:58:05', '2020-04-28 21:58:05'),
(15, 36, 3, 'ყაზბეგის 3 ბინა 2', 0, 1, '2020-05-05 08:01:48', '2020-05-05 12:01:48');

-- --------------------------------------------------------

--
-- Table structure for table `user_tree`
--

CREATE TABLE `user_tree` (
  `user_tree_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `parent_path` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `user_parent_path` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `childrens` int(2) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `user_tree`
--

INSERT INTO `user_tree` (`user_tree_id`, `user_id`, `parent_id`, `parent_path`, `user_parent_path`, `childrens`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, '/1', '/1', 2, NULL, '2018-02-24 14:57:41'),
(2, 15, 1, '/1/2', '/1/15', 1, '2018-02-24 14:56:48', '2018-02-24 14:58:45'),
(3, 16, 1, '/1/3', '/1/16', 0, '2018-02-24 14:57:41', '2018-02-24 14:57:41'),
(4, 19, 2, '/1/2/4', '/1/15/19', 0, '2018-02-24 14:58:45', '2018-02-24 14:58:45');

-- --------------------------------------------------------

--
-- Table structure for table `web_categories`
--

CREATE TABLE `web_categories` (
  `category_id` int(11) NOT NULL,
  `name_ge` varchar(255) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `name_ru` varchar(255) NOT NULL,
  `state` int(11) NOT NULL DEFAULT 1,
  `disabled` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `parent_id` int(11) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  `type` int(2) DEFAULT NULL,
  `img_path` varchar(500) DEFAULT NULL,
  `color` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `web_categories`
--

INSERT INTO `web_categories` (`category_id`, `name_ge`, `name_en`, `name_ru`, `state`, `disabled`, `user_id`, `created_at`, `updated_at`, `parent_id`, `link`, `type`, `img_path`, `color`) VALUES
(1, 'კომპანიის შესახებ', 'კომპანიის შესახებ', 'კომპანიის შესახებ', 1, 0, 0, '2016-11-14 04:48:04', '2017-05-29 08:59:25', NULL, '/category/1', NULL, 'http://localhost:8082/university/public_html/files/NewFolder/ENUEpl6Sj7g.jpg', '#5b0b0b'),
(2, 'პროდუქტების შესახებ', 'პროდუქტების შესახებ', 'პროდუქტების შესახებ', 1, 0, 0, '2016-11-14 04:48:22', '2017-05-29 08:59:41', NULL, '/category/2', NULL, 'http://localhost:8082/university/public_html/files/NewFolder/ENUEpl6Sj7g.jpg', '#081765'),
(3, 'ფარმაციის ფაკულტეტი', 'ფარმაციის ფაკულტეტი', 'ფარმაციის ფაკულტეტი', 0, 0, 0, '2016-11-14 04:48:47', '2017-05-29 09:00:14', NULL, '/category/3', NULL, 'http://localhost:8082/university/public_html/files/NewFolder/ENUEpl6Sj7g.jpg', '#00FF00'),
(4, 'საზოგადოებრივი ჯანდაცვის ფაკულტეტი', 'საზოგადოებრივი ჯანდაცვის ფაკულტეტი', 'საზოგადოებრივი ჯანდაცვის ფაკულტეტი', 0, 0, 0, '2016-11-22 11:11:36', '2017-05-29 09:00:17', NULL, '/category/4', NULL, 'http://localhost:8082/university/public_html/files/NewFolder/ENUEpl6Sj7g.jpg', '#808080');

-- --------------------------------------------------------

--
-- Table structure for table `web_menu`
--

CREATE TABLE `web_menu` (
  `menu_id` int(10) UNSIGNED NOT NULL,
  `name_ge` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_ru` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `disabled` tinyint(1) NOT NULL DEFAULT 0,
  `editable` tinyint(1) NOT NULL DEFAULT 1,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `order` int(11) DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `web_menu`
--

INSERT INTO `web_menu` (`menu_id`, `name_ge`, `name_en`, `name_ru`, `link`, `parent_id`, `state`, `disabled`, `editable`, `user_id`, `created_at`, `updated_at`, `order`, `url`) VALUES
(72, 'მთავარი', 'მთავარი', 'მთავარი', '/', NULL, 0, 0, 1, NULL, '2017-05-18 05:38:18', '2020-12-24 12:33:14', 4, NULL),
(73, 'ფასის კალკულატორი', 'ფასის კალკულატორი', 'ფასის კალკულატორი', '/price-calculator', NULL, 1, 0, 1, NULL, '2017-05-18 05:38:35', '2021-01-16 23:39:02', 3, NULL),
(74, 'კონტაქტი', 'კონტაქტი', 'კონტაქტი', '/contact', NULL, 1, 0, 1, NULL, '2017-05-18 05:38:51', '2021-01-15 13:22:23', 5, NULL),
(75, 'მთავარი გვერდი', 'home', 'home', '/', NULL, 1, 0, 1, NULL, '2017-05-25 06:07:49', '2020-12-24 12:33:17', 1, NULL),
(76, 'განვადების პირობები', 'განვადების პირობები', 'განვადების პირობები', '/page/31', NULL, 1, 0, 1, NULL, '2020-12-24 12:31:52', '2020-12-24 12:33:17', 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `web_params`
--

CREATE TABLE `web_params` (
  `param_id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `value` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_params`
--

INSERT INTO `web_params` (`param_id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'address_ge', 'თბილისი, ცაიშის ქუჩა 38/42', '2016-11-13 19:07:33', '2021-01-13 14:30:15'),
(2, 'address_en', '.', '2016-11-13 19:07:33', '2020-05-11 11:51:38'),
(3, 'address_ru', '.', '2016-11-13 19:07:33', '2020-05-11 11:51:41'),
(4, 'mobile_main', '+ 995 591 320 835', '2016-11-13 19:08:41', '2021-01-13 14:30:48'),
(5, 'mobile', '.', '2016-11-13 19:08:41', '2020-05-11 11:51:48'),
(6, 'workTime', '.', '2016-11-13 19:09:30', '2020-05-11 11:51:50'),
(7, 'email', 'iomakha99@gmail.com', '2016-11-14 07:42:25', '2021-01-13 13:23:09');

-- --------------------------------------------------------

--
-- Table structure for table `web_postmeta`
--

CREATE TABLE `web_postmeta` (
  `meta_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `meta_key` varchar(45) NOT NULL,
  `meta_value` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `web_postmeta`
--

INSERT INTO `web_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`, `created_at`, `updated_at`) VALUES
(1, 4, 'showInGallery', '1', '2016-12-05 09:41:20', '2016-12-05 09:41:20');

-- --------------------------------------------------------

--
-- Table structure for table `web_posts`
--

CREATE TABLE `web_posts` (
  `post_id` int(10) UNSIGNED NOT NULL,
  `title_ge` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_ru` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_ge` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_en` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_ru` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `disabled` tinyint(1) NOT NULL DEFAULT 0,
  `user_id` int(11) DEFAULT NULL,
  `published_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `keyword_ge` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_description_ge` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_description_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_description_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `views` int(11) DEFAULT 0,
  `description_ge_short` text CHARACTER SET utf8 DEFAULT NULL,
  `description_en_short` text CHARACTER SET utf8 DEFAULT NULL,
  `description_ru_short` text CHARACTER SET utf8 DEFAULT NULL,
  `link_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `type` int(2) DEFAULT NULL,
  `order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `web_posts`
--

INSERT INTO `web_posts` (`post_id`, `title_ge`, `title_en`, `title_ru`, `description_ge`, `description_en`, `description_ru`, `img_path`, `state`, `disabled`, `user_id`, `published_at`, `created_at`, `updated_at`, `link`, `keyword_ge`, `keyword_en`, `keyword_ru`, `google_description_ge`, `google_description_en`, `google_description_ru`, `views`, `description_ge_short`, `description_en_short`, `description_ru_short`, `link_name`, `type`, `order`) VALUES
(1, 'welcome', 'welcome', 'welcome', '&lt;p&gt;ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.&lt;/p&gt;', 'ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.', 'ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.', '', 0, 0, 0, '0000-00-00 00:00:00', '2016-11-15 08:40:09', '2020-03-04 09:50:17', '/page/1', 'Business Consultation', 'Business Consultation', 'Business Consultation', 'Business Consultation', 'Business Consultation', 'Business Consultation', 0, NULL, NULL, '', NULL, 2, NULL),
(2, 'about', 'about', 'about', 'ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.', 'ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.', 'ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.', '', 0, 0, 0, '0000-00-00 00:00:00', '2016-11-15 14:36:57', '2017-05-29 07:29:13', '/page/2?category=1', '', '', '', '', '', '', 0, '', '', '', NULL, 2, NULL),
(3, 'about', 'about', 'about', '&lt;p&gt;ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.&lt;/p&gt;\n&lt;p&gt;&lt;a rel=&quot;prettyPhoto[pp_gal]&quot; href=&quot;../../../../files/NewFolder/ENUEpl6Sj7g.jpg&quot;&gt;&lt;img src=&quot;../../../../files/NewFolder/ENUEpl6Sj7g.jpg&quot; alt=&quot;&quot; width=&quot;303&quot; height=&quot;227&quot;&gt;&lt;/a&gt;&lt;/p&gt;', 'ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.', 'ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.', '', 0, 0, 0, '0000-00-00 00:00:00', '2016-11-24 05:09:09', '2017-05-25 05:37:50', '/page/3?category=1', '', '', '', '', '', '', 0, '', '', '', NULL, 2, NULL),
(4, 'test', '', '', '&lt;p&gt;asdas sa ds dsa dsa&lt;/p&gt;', '', '', '', 0, 0, 0, '2016-11-24 15:00:00', '2016-11-24 11:00:50', '2016-11-24 11:18:39', NULL, 'asd', '', '', 'asdsad sad sa dsa', '', '', 0, '', '', '', NULL, 1, NULL),
(5, 'test', '', '', '&lt;p&gt;asdas sa ds dsa dsa&lt;/p&gt;', '', '', '', 0, 0, 0, '2016-11-24 15:00:00', '2016-11-24 11:03:43', '2016-11-24 11:18:43', NULL, 'asd', '', '', 'asdsad sad sa dsa', '', '', 0, '', '', '', NULL, 1, NULL),
(6, 'test', '', '', '&lt;p&gt;dsa dsa dsa dsa dsa sa dsa dsa&lt;/p&gt;', '', '', '', 0, 0, 0, '2016-11-24 15:07:00', '2016-11-24 11:17:40', '2016-11-24 11:18:41', '/news/6', 'ads', '', '', 'sa dsa dsa', '', '', 0, '', '', '', NULL, 1, NULL),
(7, 'გერმანელი არქიტექტორის, მარკუს პენელის, დისკუსია: NO OLD NO CITY!', 'გერმანელი არქიტექტორის, მარკუს პენელის, დისკუსია: NO OLD NO CITY!', '', '&lt;p&gt;22 ნოემბერს, 19:00 საათზე, ილიას სახელმწიფო უნივერსიტეტის E405&amp;nbsp;(&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;„&lt;/strong&gt;&lt;strong&gt;ფორბსის&lt;/strong&gt;&lt;/a&gt;“)&amp;nbsp;აუდიტორიაში, ილიას სახელმწიფო უნივერსიტეტისა და&amp;nbsp;&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;ForbesGeorgia&lt;/strong&gt;&lt;/a&gt;&lt;strong&gt;-&lt;/strong&gt;ს ურთიერთთანამშრომლობის ფარგლებში, კომუნიკაციების და ბრენდინგის სპეციალისტი, მედიაპროდიუსერი, ზაქარია ზალიკაშვილი, წაიკითხავს საჯარო ლექციას.&amp;nbsp;&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;მომხსენებლის&lt;/strong&gt;&lt;strong&gt; შესახებ&lt;/strong&gt;&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;თბილისის ბიზნესის ინსტიტუტის დასრულების შემდეგ ზაქარია ზალიკაშვილი აშშ-ში გაემგზავრა და დაამთავრა სამხრეთ კალიფორნიის (აშშ) უნივერსიტეტის კინო- და ტელეხელოვნების ფაკულტეტი, სპეციალობით - კინორეჟისურა. პროფესიული გამოცდილება მიიღო საქართველოს სხვადასხვა კომპანიის მარკეტინგის განყოფილებაში წამყვანი სპეციალისტის და დეპარტამენტის ხელმძღვანელის პოზიციებზე. 2000 წლამდე მუშაობდა GORBI-ის სოციოლოგიური და მარკეტინგული კვლევების პროექტებზე (თამბაქოს, გამამხნევებელი სასმელების, ტელეკომუნიკაციის და სხვა ტრანსნაციონალურ კომპანიებთან. მსოფლიო ბანკთან); ამავე პერიოდში, ჯონ ჰოპკინსის უნივერსიტეტთან და სხვა საერთაშორისო ფონდებთან ერთად, აწარმოებდა სხვადასხვა ტიპის სოციალური კამპანიებს.&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;ზაქარია ზალიკაშვილი სხვადასხვა დროს ხელმძღვანელობდა სარეკლამო სააგენტოს „აუდიენცია“, რადიოს „მწვანე ტალღა“ და კომპანია „ჯეოსელის“ მარკეტინგულ კომუნიკაციებს. მან 2005 წელს დააფუძნა მარკეტინგული კონსულტაციების კომპანია „სტრომბოლი“, ხოლო 2006 წელს - კომპანია „კომუნიკატორი“. 2014 წლიდან ხელმძღვანელობს საზოგადოებასთან ურთიერთობების დეპარტამენტს ქართულ ამერიკულ ინდუსტრიულ ჯგუფში “ჯორჯიან ემერიქან ელოიზ”.&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;დრო:&lt;/strong&gt; 22 ნოემბერი, 19:00 საათი&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;ადგილმდებარეობა:&lt;/strong&gt; E405&amp;nbsp;(&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;„&lt;/strong&gt;&lt;strong&gt;ფორბსის&lt;/strong&gt;&lt;strong&gt;“&lt;/strong&gt;&lt;/a&gt;)&amp;nbsp;აუდიტორია (ჩოლოყაშვილის გამზ. #3/5), ილიას სახელმწიფო უნივერსიტეტი&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;დასწრება თავისუფალია.&lt;/p&gt;', '&lt;p&gt;22 ნოემბერს, 19:00 საათზე, ილიას სახელმწიფო უნივერსიტეტის E405&amp;nbsp;(&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;„&lt;/strong&gt;&lt;strong&gt;ფორბსის&lt;/strong&gt;&lt;/a&gt;“)&amp;nbsp;აუდიტორიაში, ილიას სახელმწიფო უნივერსიტეტისა და&amp;nbsp;&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;ForbesGeorgia&lt;/strong&gt;&lt;/a&gt;&lt;strong&gt;-&lt;/strong&gt;ს ურთიერთთანამშრომლობის ფარგლებში, კომუნიკაციების და ბრენდინგის სპეციალისტი, მედიაპროდიუსერი, ზაქარია ზალიკაშვილი, წაიკითხავს საჯარო ლექციას.&amp;nbsp;&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;მომხსენებლის&lt;/strong&gt;&lt;strong&gt; შესახებ&lt;/strong&gt;&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;თბილისის ბიზნესის ინსტიტუტის დასრულების შემდეგ ზაქარია ზალიკაშვილი აშშ-ში გაემგზავრა და დაამთავრა სამხრეთ კალიფორნიის (აშშ) უნივერსიტეტის კინო- და ტელეხელოვნების ფაკულტეტი, სპეციალობით - კინორეჟისურა. პროფესიული გამოცდილება მიიღო საქართველოს სხვადასხვა კომპანიის მარკეტინგის განყოფილებაში წამყვანი სპეციალისტის და დეპარტამენტის ხელმძღვანელის პოზიციებზე. 2000 წლამდე მუშაობდა GORBI-ის სოციოლოგიური და მარკეტინგული კვლევების პროექტებზე (თამბაქოს, გამამხნევებელი სასმელების, ტელეკომუნიკაციის და სხვა ტრანსნაციონალურ კომპანიებთან. მსოფლიო ბანკთან); ამავე პერიოდში, ჯონ ჰოპკინსის უნივერსიტეტთან და სხვა საერთაშორისო ფონდებთან ერთად, აწარმოებდა სხვადასხვა ტიპის სოციალური კამპანიებს.&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;ზაქარია ზალიკაშვილი სხვადასხვა დროს ხელმძღვანელობდა სარეკლამო სააგენტოს „აუდიენცია“, რადიოს „მწვანე ტალღა“ და კომპანია „ჯეოსელის“ მარკეტინგულ კომუნიკაციებს. მან 2005 წელს დააფუძნა მარკეტინგული კონსულტაციების კომპანია „სტრომბოლი“, ხოლო 2006 წელს - კომპანია „კომუნიკატორი“. 2014 წლიდან ხელმძღვანელობს საზოგადოებასთან ურთიერთობების დეპარტამენტს ქართულ ამერიკულ ინდუსტრიულ ჯგუფში “ჯორჯიან ემერიქან ელოიზ”.&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;დრო:&lt;/strong&gt; 22 ნოემბერი, 19:00 საათი&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;ადგილმდებარეობა:&lt;/strong&gt; E405&amp;nbsp;(&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;„&lt;/strong&gt;&lt;strong&gt;ფორბსის&lt;/strong&gt;&lt;strong&gt;“&lt;/strong&gt;&lt;/a&gt;)&amp;nbsp;აუდიტორია (ჩოლოყაშვილის გამზ. #3/5), ილიას სახელმწიფო უნივერსიტეტი&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;დასწრება თავისუფალია.&lt;/p&gt;', '', 'http://localhost:8082/onlinemarket/public/files/17/b49ad8bdcc52a1daa4d259042637139b_original.jpeg', 0, 0, 0, '2016-11-24 11:20:00', '2016-11-24 11:18:50', '2020-03-11 06:16:09', '/news/7', 'გერმანელი არქიტექტორის, მარკუს პენელის, დისკუსია: NO OLD NO CITY!', 'გერმანელი არქიტექტორის, მარკუს პენელის, დისკუსია: NO OLD NO CITY!', '', 'გერმანელი არქიტექტორის, მარკუს პენელის, დისკუსია: NO OLD NO CITY!', 'გერმანელი არქიტექტორის, მარკუს პენელის, დისკუსია: NO OLD NO CITY!', '', 0, '', '', '', NULL, 1, NULL),
(8, 'ჩემპიონატი ფუტსალში - ილიაუნი VS ღია სასწავლო უნივერსიტეტი', 'ჩემპიონატი ფუტსალში - ილიაუნი VS ღია სასწავლო უნივერსიტეტი', '', '&lt;p&gt;24 ნოემბერს, 19:00 საათზე, ილიაუნის ფუტსალის ნაკრები, საუნივერსიტეტო სპორტის ფედერაციის თასის გათამაშების ჯგუფური ეტაპის მესამე მატჩს ღია სასწავლო უნივერსიტეტის გუნდთან გამართავს. შეხვედრა ჩატარდება სპორტკომპლექსში &quot;არენა-1&quot; (უნივერსიტეტის ქ. N2).&lt;/p&gt;\n&lt;p&gt;&lt;strong&gt;დრო:&amp;nbsp;&lt;/strong&gt;24 ნოემბერი,19:00 საათი&lt;/p&gt;\n&lt;p&gt;&lt;strong&gt;ადგილმდებარეობა:&amp;nbsp;&lt;/strong&gt;სპორტკომპლექსში &quot;არენა-1&quot; (უნივერსიტეტის ქ. N2).&lt;/p&gt;', '&lt;p&gt;24 ნოემბერს, 19:00 საათზე, ილიაუნის ფუტსალის ნაკრები, საუნივერსიტეტო სპორტის ფედერაციის თასის გათამაშების ჯგუფური ეტაპის მესამე მატჩს ღია სასწავლო უნივერსიტეტის გუნდთან გამართავს. შეხვედრა ჩატარდება სპორტკომპლექსში &quot;არენა-1&quot; (უნივერსიტეტის ქ. N2).&lt;/p&gt;\n&lt;p&gt;&lt;strong&gt;დრო:&amp;nbsp;&lt;/strong&gt;24 ნოემბერი,19:00 საათი&lt;/p&gt;\n&lt;p&gt;&lt;strong&gt;ადგილმდებარეობა:&amp;nbsp;&lt;/strong&gt;სპორტკომპლექსში &quot;არენა-1&quot; (უნივერსიტეტის ქ. N2).&lt;/p&gt;', '', 'http://localhost:8082/onlinemarket/public/files/foo.jpg', 0, 0, 0, '2016-11-24 11:18:00', '2016-11-24 11:19:51', '2020-03-04 10:10:02', '/news/8', 'ჩემპიონატი ფუტსალში - ილიაუნი VS ღია სასწავლო უნივერსიტეტი', 'ჩემპიონატი ფუტსალში - ილიაუნი VS ღია სასწავლო უნივერსიტეტი', '', 'ჩემპიონატი ფუტსალში - ილიაუნი VS ღია სასწავლო უნივერსიტეტი', 'ჩემპიონატი ფუტსალში - ილიაუნი VS ღია სასწავლო უნივერსიტეტი', '', 0, '', '', '', NULL, 1, NULL),
(9, 'ილიაუნელმა კალათბურთელებმა სტუდენტური ლიგის მეორე შეხვედრაც მოიგეს', 'ილიაუნელმა კალათბურთელებმა სტუდენტური ლიგის მეორე შეხვედრაც მოიგეს', '', '&lt;p&gt;ვერის ბაღის საკალათბურთო კომპლექსში გრძელდება საქართველოს სტუდენტური ჩემპიონატი კალთბურთში. ჯგუფური ეტაპის მეორე ტურში მორიგ წარმატებას მიაღწია ილიაუნის გუნდმა, რომელმაც შავი ზღვის საერთაშორისო უნივერსიტეტის კალათბურთელები დიდი ანგარიშით - 106:54 დაამარცხა. ილიაუნის გუნდს ყველაზე მეტი, 32 ქულა ირაკლი ჯანხოთელმა მოუტანა. მე-3 ტურის მატჩს ილიაუნელები 2 დეკემბერს, 16:00 საათზე, ბათუმის საზღვაო აკადემიის კალათბურთელებთან გამართავენ. აააა&lt;/p&gt;', '&lt;p&gt;ვერის ბაღის საკალათბურთო კომპლექსში გრძელდება საქართველოს სტუდენტური ჩემპიონატი კალთბურთში. ჯგუფური ეტაპის მეორე ტურში მორიგ წარმატებას მიაღწია ილიაუნის გუნდმა, რომელმაც შავი ზღვის საერთაშორისო უნივერსიტეტის კალათბურთელები დიდი ანგარიშით - 106:54 დაამარცხა. ილიაუნის გუნდს ყველაზე მეტი, 32 ქულა ირაკლი ჯანხოთელმა მოუტანა. მე-3 ტურის მატჩს ილიაუნელები 2 დეკემბერს, 16:00 საათზე, ბათუმის საზღვაო აკადემიის კალათბურთელებთან გამართავენ.&amp;nbsp;&lt;/p&gt;', '', 'http://localhost:8082/onlinemarket/public_html/files/foo.jpg', 0, 0, 0, '2016-11-24 15:22:00', '2016-11-24 11:22:18', '2020-03-11 06:23:09', '/news/9', 'ილიაუნელმა კალათბურთელებმა სტუდენტური ლიგის მეორე შეხვედრაც მოიგეს', 'ილიაუნელმა კალათბურთელებმა სტუდენტური ლიგის მეორე შეხვედრაც მოიგეს', '', 'ილიაუნელმა კალათბურთელებმა სტუდენტური ლიგის მეორე შეხვედრაც მოიგეს', 'ილიაუნელმა კალათბურთელებმა სტუდენტური ლიგის მეორე შეხვედრაც მოიგეს', '', 0, '', '', '', NULL, 1, NULL),
(10, 'საყოფაცხოვრებო ტექნიკა', 'ილიაუნის „ფორბსის“ კლუბში: ზაქარია ზალიკაშვილის საჯარო ლექცია', '', '&lt;ol&gt;\n&lt;li&gt;&lt;span style=&quot;font-size: 14pt;&quot;&gt;&lt;strong&gt;A6SF26DDT / ჩასაშენებელი ღუმელი&lt;/strong&gt;&lt;/span&gt;&lt;/li&gt;\n&lt;li&gt;&lt;span style=&quot;font-size: 14pt;&quot;&gt;&lt;strong&gt;A6SF26DT / ჩასაშენებელი ღუმელი&lt;/strong&gt;&lt;/span&gt;&lt;/li&gt;\n&lt;li&gt;&lt;span style=&quot;font-size: 14pt;&quot;&gt;&lt;strong&gt;A6SF2MT / ჩასაშენებელი ღუმელი&lt;/strong&gt;&lt;/span&gt;&lt;/li&gt;\n&lt;li&gt;&lt;span style=&quot;font-size: 14pt;&quot;&gt;&lt;strong&gt;DA6 - 830SIYAH / აირის გამწოვი&lt;/strong&gt;&lt;/span&gt;&lt;/li&gt;\n&lt;li&gt;&lt;span style=&quot;font-size: 14pt;&quot;&gt;&lt;strong&gt;DP6XINOX / აირის გამწოვი&lt;/strong&gt;&lt;/span&gt;&lt;/li&gt;\n&lt;li&gt;&lt;span style=&quot;font-size: 14pt;&quot;&gt;&lt;strong&gt;LX40TAHDF / ჩასაშენებელი გაზქურა&lt;/strong&gt;&lt;/span&gt;&lt;/li&gt;\n&lt;li&gt;&lt;span style=&quot;font-size: 14pt;&quot;&gt;&lt;strong&gt;LX410INOX / ჩასაშენებელი გაზქურა&lt;/strong&gt;&lt;/span&gt;&lt;/li&gt;\n&lt;li&gt;&lt;span style=&quot;font-size: 14pt;&quot;&gt;&lt;strong&gt;LX412INOX / ჩასაშენებელი გაზქურა&lt;/strong&gt;&lt;/span&gt;&lt;/li&gt;\n&lt;li&gt;&lt;span style=&quot;font-size: 14pt;&quot;&gt;&lt;strong&gt;LX710 / აირის გამწოვი / შავი&lt;/strong&gt;&lt;/span&gt;&lt;/li&gt;\n&lt;li&gt;&lt;span style=&quot;font-size: 14pt;&quot;&gt;&lt;strong&gt;LX710INOX / აირის გამწოვი&lt;/strong&gt;&lt;/span&gt;&lt;/li&gt;\n&lt;li&gt;&lt;span style=&quot;font-size: 14pt;&quot;&gt;&lt;strong&gt;LX735 / აირის გამწოვი&lt;/strong&gt;&lt;/span&gt;&lt;/li&gt;\n&lt;li&gt;&lt;span style=&quot;font-size: 14pt;&quot;&gt;&lt;strong&gt;LX755 / აირის გამწოვი&lt;/strong&gt;&lt;/span&gt;&lt;/li&gt;\n&lt;li&gt;&lt;span style=&quot;font-size: 14pt;&quot;&gt;&lt;strong&gt;ჩასაშენებელი გამწოვი / 01&lt;/strong&gt;&lt;/span&gt;&lt;/li&gt;\n&lt;/ol&gt;', '&lt;p&gt;22 ნოემბერს, 19:00 საათზე, ილიას სახელმწიფო უნივერსიტეტის E405&amp;nbsp;(&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;„&lt;/strong&gt;&lt;strong&gt;ფორბსის&lt;/strong&gt;&lt;/a&gt;“)&amp;nbsp;აუდიტორიაში, ილიას სახელმწიფო უნივერსიტეტისა და&amp;nbsp;&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;ForbesGeorgia&lt;/strong&gt;&lt;/a&gt;&lt;strong&gt;-&lt;/strong&gt;ს ურთიერთთანამშრომლობის ფარგლებში, კომუნიკაციების და ბრენდინგის სპეციალისტი, მედიაპროდიუსერი, ზაქარია ზალიკაშვილი, წაიკითხავს საჯარო ლექციას.&amp;nbsp;&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;მომხსენებლის&lt;/strong&gt;&lt;strong&gt; შესახებ&lt;/strong&gt;&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;თბილისის ბიზნესის ინსტიტუტის დასრულების შემდეგ ზაქარია ზალიკაშვილი აშშ-ში გაემგზავრა და დაამთავრა სამხრეთ კალიფორნიის (აშშ) უნივერსიტეტის კინო- და ტელეხელოვნების ფაკულტეტი, სპეციალობით - კინორეჟისურა. პროფესიული გამოცდილება მიიღო საქართველოს სხვადასხვა კომპანიის მარკეტინგის განყოფილებაში წამყვანი სპეციალისტის და დეპარტამენტის ხელმძღვანელის პოზიციებზე. 2000 წლამდე მუშაობდა GORBI-ის სოციოლოგიური და მარკეტინგული კვლევების პროექტებზე (თამბაქოს, გამამხნევებელი სასმელების, ტელეკომუნიკაციის და სხვა ტრანსნაციონალურ კომპანიებთან. მსოფლიო ბანკთან); ამავე პერიოდში, ჯონ ჰოპკინსის უნივერსიტეტთან და სხვა საერთაშორისო ფონდებთან ერთად, აწარმოებდა სხვადასხვა ტიპის სოციალური კამპანიებს.&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;ზაქარია ზალიკაშვილი სხვადასხვა დროს ხელმძღვანელობდა სარეკლამო სააგენტოს „აუდიენცია“, რადიოს „მწვანე ტალღა“ და კომპანია „ჯეოსელის“ მარკეტინგულ კომუნიკაციებს. მან 2005 წელს დააფუძნა მარკეტინგული კონსულტაციების კომპანია „სტრომბოლი“, ხოლო 2006 წელს - კომპანია „კომუნიკატორი“. 2014 წლიდან ხელმძღვანელობს საზოგადოებასთან ურთიერთობების დეპარტამენტს ქართულ ამერიკულ ინდუსტრიულ ჯგუფში “ჯორჯიან ემერიქან ელოიზ”.&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;დრო:&lt;/strong&gt; 22 ნოემბერი, 19:00 საათი&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;ადგილმდებარეობა:&lt;/strong&gt; E405&amp;nbsp;(&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;„&lt;/strong&gt;&lt;strong&gt;ფორბსის&lt;/strong&gt;&lt;strong&gt;“&lt;/strong&gt;&lt;/a&gt;)&amp;nbsp;აუდიტორია (ჩოლოყაშვილის გამზ. #3/5), ილიას სახელმწიფო უნივერსიტეტი&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;დასწრება თავისუფალია.&lt;/p&gt;', '', 'http://sonniva.ge/files/unnamed%20%281%29.png', 0, 0, 0, '2020-03-11 06:00:00', '2016-11-24 11:25:35', '2020-05-11 11:50:46', '/news/10', 'საყოფაცხოვრებო ტექნიკა', 'ილიაუნის „ფორბსის“ კლუბში: ზაქარია ზალიკაშვილის საჯარო ლექცია', '', 'საყოფაცხოვრებო ტექნიკა', 'ილიაუნის „ფორბსის“ კლუბში: ზაქარია ზალიკაშვილის საჯარო ლექცია', '', 0, '', '', '', NULL, 1, NULL),
(11, 'ასდ', '', '', '&lt;p&gt;ასდსადსადსა&lt;/p&gt;', '', '', '', 1, 0, 0, '0000-00-00 00:00:00', '2016-12-14 12:11:54', '2016-12-14 12:11:54', '/page/11', 'სად', '', '', 'სადსა', '', '', 0, NULL, NULL, '', NULL, NULL, NULL),
(12, 'დსა', '', '', '&lt;p&gt;სადსადსა&lt;/p&gt;', '', '', '', 0, 0, 0, '0000-00-00 00:00:00', '2016-12-14 12:13:00', '2016-12-14 12:13:07', '/page/12?category=ასდსადსა', 'დსად', '', '', 'სადსად', '', '', 0, NULL, NULL, '', NULL, 2, NULL),
(13, '1', '1', '1', NULL, NULL, NULL, 'http://localhost:8082/onlinemarket/public_html/files/brand1.png', 0, 0, 0, '0000-00-00 00:00:00', '2016-12-15 10:57:12', '2020-03-04 11:43:53', '#', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '', NULL, 5, NULL),
(14, '2', '2', '2', NULL, NULL, NULL, 'http://localhost:8082/onlinemarket/public_html/files/brand2.png', 0, 0, 0, '0000-00-00 00:00:00', '2016-12-16 07:18:33', '2020-03-05 04:03:03', '#', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '', NULL, 5, NULL),
(15, '1', '1', '1', NULL, NULL, NULL, 'http://localhost:8082/onlinemarket/public_html/files/slide-1-full.jpg', 0, 0, 0, '0000-00-00 00:00:00', '2016-12-16 09:30:04', '2020-03-04 11:37:40', 'სადსადა', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '', NULL, 6, 1),
(16, '2', '2', '2', NULL, NULL, NULL, 'http://localhost:8082/onlinemarket/public_html/files/slide-2-full.jpg', 0, 0, 0, '0000-00-00 00:00:00', '2016-12-16 09:30:24', '2020-03-04 11:37:43', 'დსა', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '', NULL, 6, 3),
(17, '3', '3', '3', NULL, NULL, NULL, 'http://localhost:8082/onlinemarket/public_html/files/slide-3-full.jpg', 0, 0, 0, '0000-00-00 00:00:00', '2016-12-16 09:30:32', '2020-03-04 11:37:48', 'სადსა', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '', NULL, 6, 2),
(18, 'ფასის კალკულატორი', 'About Us', 'About Us', '', '&lt;p&gt;About Us&lt;/p&gt;', '&lt;p&gt;About Us&lt;/p&gt;', '', 1, 0, NULL, '0000-00-00 00:00:00', '2017-05-29 07:38:22', '2020-12-24 12:30:48', '/page/18', 'ფასის კალკულატორი', 'About Us', 'About Us', 'ფასის კალკულატორი', 'About Us', 'About Us', 0, NULL, NULL, NULL, NULL, 2, NULL),
(19, '3', '3', '3', NULL, NULL, NULL, 'http://localhost:8082/onlinemarket/public_html/files/brand3.png', 0, 0, NULL, NULL, '2017-11-29 10:43:44', '2020-03-05 04:02:55', '#', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 5, NULL),
(20, '4', '4', '4', NULL, NULL, NULL, 'http://localhost:8082/onlinemarket/public_html/files/brand4.png', 0, 0, NULL, NULL, '2017-11-29 10:43:55', '2020-03-05 04:03:06', '#', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 5, NULL),
(21, '5', '5', '5', NULL, NULL, NULL, 'http://localhost:8082/onlinemarket/public_html/files/brand5.png', 0, 0, NULL, NULL, '2017-11-29 10:44:05', '2020-03-05 04:02:51', '#', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 5, NULL),
(22, '11', '11', '11', NULL, NULL, NULL, 'http://localhost:8082/onlinemarket/public_html/files/brand1.png', 0, 0, NULL, NULL, '2017-11-29 10:44:14', '2020-03-05 04:02:42', '#', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 5, NULL),
(23, '22', '22', '22', NULL, NULL, NULL, 'http://localhost:8082/onlinemarket/public_html/files/brand2.png', 0, 0, NULL, NULL, '2017-11-29 10:44:24', '2020-03-05 04:02:58', '#', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 5, NULL),
(24, '33', '33', '33', NULL, NULL, NULL, 'http://localhost:8082/onlinemarket/public_html/files/brand3.png', 0, 0, NULL, NULL, '2017-11-29 10:44:39', '2020-03-05 04:03:00', '#', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 5, NULL),
(25, '1', '1', '1', NULL, NULL, NULL, 'http://sonniva.ge/files/thumb/1518778124ee27%20copy%201.jpg', 0, 0, NULL, NULL, '2020-03-04 11:43:00', '2020-05-11 11:50:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, NULL),
(26, '2', '2', '2', NULL, NULL, NULL, 'http://sonniva.ge/files/mobile/14881936650dd8.jpg', 0, 0, NULL, NULL, '2020-03-04 11:52:25', '2020-05-11 11:50:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, NULL),
(27, '3', '3', '3', NULL, NULL, NULL, 'http://sonniva.ge/files/mobile/142559713750cc.jpg', 0, 0, NULL, NULL, '2020-03-04 11:52:50', '2020-05-11 11:50:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, NULL),
(28, '4', '4', '4', NULL, NULL, NULL, 'http://sonniva.ge/files/mobile/144895920667cc.jpg', 0, 0, NULL, NULL, '2020-03-04 11:53:17', '2020-05-11 11:51:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, NULL),
(29, '5', '5', '5', NULL, NULL, NULL, 'http://sonniva.ge/files/mobile/157408516033a7.jpg', 0, 0, NULL, NULL, '2020-03-04 11:53:40', '2020-05-11 11:51:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, NULL),
(30, 'Lift-Up Door Systems', '', '', '&lt;p&gt;Soft and silent movement features of D-Lite Lift provide maximum comfort in lift-up cabinet doors. Cabinet doors can be stopped at a desired level by gradually opening feature. D- Lite Lift is designed and produced with the innovative perspective of Samet to cover minimum space in cabinet. Four standard colour options of D-Lite Lift complies with innovative furniture designs.&lt;/p&gt;', '', '', 'http://sonniva.ge/files/basic-html/1554295163d068.jpg', 0, 0, NULL, '2020-03-13 06:57:00', '2020-03-13 06:59:20', '2020-05-11 11:50:49', '/post/30', '', '', '', '', '', '', 0, '', '', '', NULL, 1, NULL),
(31, 'განვადების პირობები', NULL, NULL, '&lt;p&gt;თიბისი ბანკის ონლაინ განვადების შემთხვევაში ჩვენი მხრიდან თქვენ სარგებლობთ 0%-იანი განვადებით...&lt;/p&gt;', '', '', '', 1, 0, NULL, NULL, '2020-12-24 12:31:29', '2021-02-24 12:08:06', '/page/31', 'განვადების პირობები', '', '', 'განვადების პირობები', '', '', 0, NULL, NULL, NULL, NULL, 2, NULL),
(32, 'ტესტ', 'ტესტ', 'ტესტ', NULL, NULL, NULL, 'https://iomax.ge/files/unnamed.png', 0, 0, NULL, NULL, '2021-01-13 14:02:39', '2021-01-13 14:22:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, NULL),
(33, '', '', '', NULL, NULL, NULL, 'https://iomax.ge/files/489.jpg', 0, 0, NULL, NULL, '2021-01-13 14:21:54', '2021-01-15 14:54:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, NULL),
(34, '', '', '', NULL, NULL, NULL, 'https://iomax.ge/files/unnamed.png', 0, 0, NULL, NULL, '2021-01-13 14:35:04', '2021-01-15 14:54:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, NULL),
(35, '', '', '', NULL, NULL, NULL, 'https://iomax.ge/files/GEO_8879.jpg', 0, 0, NULL, NULL, '2021-01-15 14:13:03', '2021-01-15 14:16:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, NULL),
(36, '', '', '', NULL, NULL, NULL, 'https://iomax.ge/files/1111.jpg', 0, 0, NULL, NULL, '2021-01-15 14:16:53', '2021-01-15 14:33:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, NULL),
(37, '', '', '', NULL, NULL, NULL, 'https://iomax.ge/files/Untitled-1.jpg', 0, 0, NULL, NULL, '2021-01-15 14:33:45', '2021-01-15 14:35:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, NULL),
(38, '', '', '', NULL, NULL, NULL, 'https://iomax.ge/files/Untitled-1.jpg', 0, 0, NULL, NULL, '2021-01-15 14:35:40', '2021-01-15 14:38:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, NULL),
(39, '', '', '', NULL, NULL, NULL, 'https://iomax.ge/files/Untitled-1.jpg', 0, 0, NULL, NULL, '2021-01-15 14:38:48', '2021-01-15 14:40:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, NULL),
(40, '', '', '', NULL, NULL, NULL, 'https://iomax.ge/files/Untitled-1.jpg', 0, 0, NULL, NULL, '2021-01-15 14:40:26', '2021-01-15 15:08:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, NULL),
(41, '', '', '', NULL, NULL, NULL, 'https://iomax.ge/files/Untitled-2.jpg', 0, 0, NULL, NULL, '2021-01-15 14:55:16', '2021-01-15 15:04:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, NULL),
(42, '', '', '', NULL, NULL, NULL, 'https://iomax.ge/files/Untitled-2.jpg', 0, 0, NULL, NULL, '2021-01-15 15:05:02', '2021-01-15 15:08:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, NULL),
(43, '', '', '', NULL, NULL, NULL, 'https://iomax.ge/files/Untitled-2.jpg', 1, 0, NULL, NULL, '2021-01-15 15:08:07', '2021-01-15 15:08:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, NULL),
(44, '', '', '', NULL, NULL, NULL, 'https://iomax.ge/files/Untitled-1.jpg', 0, 0, NULL, NULL, '2021-01-15 15:08:12', '2021-01-15 18:24:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, NULL),
(45, '', '', '', NULL, NULL, NULL, 'https://iomax.ge/files/Untitled-3.jpg', 0, 0, NULL, NULL, '2021-01-15 18:25:12', '2021-01-15 18:40:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, NULL),
(46, '', '', '', NULL, NULL, NULL, 'https://iomax.ge/files/Untitled-3.jpg', 0, 0, NULL, NULL, '2021-01-15 18:40:43', '2021-01-15 18:41:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, NULL),
(47, '', '', '', NULL, NULL, NULL, 'https://iomax.ge/files/Untitled-3.jpg', 0, 0, NULL, NULL, '2021-01-15 18:42:02', '2021-01-15 18:42:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, NULL),
(48, '', '', '', NULL, NULL, NULL, 'https://iomax.ge/files/Untitled-3.jpg', 1, 0, NULL, NULL, '2021-01-15 18:43:09', '2021-01-15 18:43:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, NULL),
(49, '', '', '', NULL, NULL, NULL, 'https://iomax.ge/files/BLACK_RED.jpg', 0, 0, NULL, NULL, '2021-03-01 17:38:02', '2021-03-01 17:39:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, NULL),
(50, '', '', '', NULL, NULL, NULL, 'https://iomax.ge/files/Untitled-1.jpg', 0, 0, NULL, NULL, '2021-03-01 17:39:56', '2021-03-01 17:42:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, NULL),
(51, '', '', '', NULL, NULL, NULL, 'https://iomax.ge/files/BLACK_RED.jpg', 0, 0, NULL, NULL, '2021-03-01 17:41:54', '2021-03-01 17:42:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `web_post_categories`
--

CREATE TABLE `web_post_categories` (
  `news_category_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `web_post_categories`
--

INSERT INTO `web_post_categories` (`news_category_id`, `post_id`, `category_id`, `state`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 0, '2016-11-15 08:09:38', '0000-00-00 00:00:00'),
(2, 2, 1, 1, 0, '2016-11-15 08:26:10', '0000-00-00 00:00:00'),
(3, 3, 1, 1, 0, '2016-11-15 08:26:30', '0000-00-00 00:00:00'),
(4, 4, 2, 1, 0, '2016-11-15 08:27:02', '0000-00-00 00:00:00'),
(5, 5, 2, 1, 0, '2016-11-15 08:27:22', '0000-00-00 00:00:00'),
(10, 6, 1, 1, 0, '2016-11-24 15:17:40', '0000-00-00 00:00:00'),
(17, 9, 2, 1, 0, '2016-12-01 15:47:31', '0000-00-00 00:00:00'),
(18, 7, 1, 1, 0, '2016-12-01 15:47:40', '0000-00-00 00:00:00'),
(19, 8, 1, 1, 0, '2016-12-01 15:47:49', '0000-00-00 00:00:00'),
(20, 10, 2, 1, 0, '2020-03-11 06:22:30', '0000-00-00 00:00:00'),
(21, 30, 2, 1, 0, '2020-03-13 06:59:20', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `web_types`
--

CREATE TABLE `web_types` (
  `int` int(2) DEFAULT NULL,
  `text` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_types`
--

INSERT INTO `web_types` (`int`, `text`) VALUES
(1, 'news'),
(2, 'page'),
(3, 'news_category'),
(4, 'news_tags'),
(5, 'links'),
(6, 'slideshow');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `installments_tbc`
--
ALTER TABLE `installments_tbc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `langs`
--
ALTER TABLE `langs`
  ADD PRIMARY KEY (`lang_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `SUPPLIER_CODE_INDX` (`supplier_code`) USING BTREE,
  ADD KEY `PRODUCT_ID_INX` (`product_id`);

--
-- Indexes for table `product_files`
--
ALTER TABLE `product_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_info`
--
ALTER TABLE `product_info`
  ADD PRIMARY KEY (`product_info_id`);

--
-- Indexes for table `product_info_types`
--
ALTER TABLE `product_info_types`
  ADD PRIMARY KEY (`product_info_type_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `sell`
--
ALTER TABLE `sell`
  ADD PRIMARY KEY (`sell_id`);

--
-- Indexes for table `sell_items`
--
ALTER TABLE `sell_items`
  ADD PRIMARY KEY (`sell_item_id`);

--
-- Indexes for table `shoppingcart`
--
ALTER TABLE `shoppingcart`
  ADD PRIMARY KEY (`identifier`,`instance`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD PRIMARY KEY (`address_id`);

--
-- Indexes for table `user_tree`
--
ALTER TABLE `user_tree`
  ADD PRIMARY KEY (`user_tree_id`);

--
-- Indexes for table `web_categories`
--
ALTER TABLE `web_categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `web_menu`
--
ALTER TABLE `web_menu`
  ADD PRIMARY KEY (`menu_id`),
  ADD KEY `menu_state_index` (`state`),
  ADD KEY `menu_disabled_index` (`disabled`);

--
-- Indexes for table `web_params`
--
ALTER TABLE `web_params`
  ADD PRIMARY KEY (`param_id`);

--
-- Indexes for table `web_postmeta`
--
ALTER TABLE `web_postmeta`
  ADD PRIMARY KEY (`meta_id`);

--
-- Indexes for table `web_posts`
--
ALTER TABLE `web_posts`
  ADD PRIMARY KEY (`post_id`),
  ADD KEY `pages_state_index` (`state`),
  ADD KEY `pages_disabled_index` (`disabled`);

--
-- Indexes for table `web_post_categories`
--
ALTER TABLE `web_post_categories`
  ADD PRIMARY KEY (`news_category_id`),
  ADD KEY `news_categories_state_index` (`state`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `installments_tbc`
--
ALTER TABLE `installments_tbc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `langs`
--
ALTER TABLE `langs`
  MODIFY `lang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=288;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=629;

--
-- AUTO_INCREMENT for table `product_files`
--
ALTER TABLE `product_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=636;

--
-- AUTO_INCREMENT for table `product_info`
--
ALTER TABLE `product_info`
  MODIFY `product_info_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;

--
-- AUTO_INCREMENT for table `product_info_types`
--
ALTER TABLE `product_info_types`
  MODIFY `product_info_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sell`
--
ALTER TABLE `sell`
  MODIFY `sell_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT for table `sell_items`
--
ALTER TABLE `sell_items`
  MODIFY `sell_item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `user_addresses`
--
ALTER TABLE `user_addresses`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `user_tree`
--
ALTER TABLE `user_tree`
  MODIFY `user_tree_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `web_categories`
--
ALTER TABLE `web_categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `web_menu`
--
ALTER TABLE `web_menu`
  MODIFY `menu_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `web_params`
--
ALTER TABLE `web_params`
  MODIFY `param_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `web_postmeta`
--
ALTER TABLE `web_postmeta`
  MODIFY `meta_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_posts`
--
ALTER TABLE `web_posts`
  MODIFY `post_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `web_post_categories`
--
ALTER TABLE `web_post_categories`
  MODIFY `news_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
