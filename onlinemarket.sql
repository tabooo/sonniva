/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : onlinemarket

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-02-23 17:27:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admins
-- ----------------------------
DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `state_id` tinyint(1) NOT NULL DEFAULT '1',
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admins
-- ----------------------------
INSERT INTO `admins` VALUES ('1', 'Admin', 'admin@gmail.com', '$2y$10$b9Rs1OvVD/BThlNHmSGaauHgeTzWZsvj9IQJe.GH8hKudziWP0IRG', 'HbfZnDuTl3sS18NrcqNAkj15dKuHmTUeHztZlNU9V9mFazZNVs9anaNZPf00', '2017-05-04 11:45:54', '2017-05-04 11:45:54', '1', '1');
INSERT INTO `admins` VALUES ('2', 'Testa', 'admintesta@gmail.com', '$2y$10$b9Rs1OvVD/BThlNHmSGaauHgeTzWZsvj9IQJe.GH8hKudziWP0IRG', 'lySZSZtN5LGugVJ549qph3UmYFh22h3KSUKVT6Lr5KR2yDbcdTtV6b6qIoT5', '2017-05-04 11:45:54', '2017-05-04 11:45:54', '1', '1');
INSERT INTO `admins` VALUES ('3', 'Super', 'adminsuper@gmail.com', '$2y$10$b9Rs1OvVD/BThlNHmSGaauHgeTzWZsvj9IQJe.GH8hKudziWP0IRG', 'lySZSZtN5LGugVJ549qph3UmYFh22h3KSUKVT6Lr5KR2yDbcdTtV6b6qIoT5', '2017-05-04 11:45:54', '2017-05-04 11:45:54', '1', '1');
INSERT INTO `admins` VALUES ('4', 'Admin1', 'admin1@gmail.com', '$2y$10$b9Rs1OvVD/BThlNHmSGaauHgeTzWZsvj9IQJe.GH8hKudziWP0IRG', 'lySZSZtN5LGugVJ549qph3UmYFh22h3KSUKVT6Lr5KR2yDbcdTtV6b6qIoT5', '2017-05-04 11:45:54', '2017-05-04 11:45:54', '1', '1');

-- ----------------------------
-- Table structure for branches
-- ----------------------------
DROP TABLE IF EXISTS `branches`;
CREATE TABLE `branches` (
  `branch_id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `branch_state_id` int(2) NOT NULL DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of branches
-- ----------------------------
INSERT INTO `branches` VALUES ('1', 'რუსთავი', '1', null, null);
INSERT INTO `branches` VALUES ('2', 'თბილისი', '1', null, null);
INSERT INTO `branches` VALUES ('3', 'გორი', '1', null, null);
INSERT INTO `branches` VALUES ('4', 'xinkali', '1', '2017-05-03 13:39:36', '2017-05-03 13:39:36');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `childrens` text,
  `parents` text,
  `cancel_date` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `state_id` int(1) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', '– ყველა –', null, '74,79,80,81,82', null, null, '0000-00-00 00:00:00', '2017-05-08 11:17:19', '1');
INSERT INTO `category` VALUES ('74', 'ტელევიზორები', '1', '76', '1', null, '0000-00-00 00:00:00', '2017-05-18 09:40:43', '1');
INSERT INTO `category` VALUES ('76', 'LED ტელევიზორები', '74', null, '74', null, '0000-00-00 00:00:00', '2017-05-17 14:08:45', '1');
INSERT INTO `category` VALUES ('79', 'ციფრული ტექნიკა', '1', '83,85', '1', null, '0000-00-00 00:00:00', '2017-05-17 14:06:31', '1');
INSERT INTO `category` VALUES ('80', 'ტელეფონი/ტაბლეტი', '1', null, '1', null, '0000-00-00 00:00:00', '2017-05-17 14:07:37', '1');
INSERT INTO `category` VALUES ('81', 'კომპიუტერული ტექნიკა', '1', null, '1', null, '0000-00-00 00:00:00', '2017-05-17 14:07:49', '1');
INSERT INTO `category` VALUES ('82', 'საყოფაცხოვრებო ტექნიკა', '1', null, '1', '2017-05-02 15:24:57', '2017-05-02 13:15:13', '2017-05-17 14:08:01', '1');
INSERT INTO `category` VALUES ('83', 'ფოტო კამერა', '79', null, '79', null, '2017-05-08 11:08:44', '2017-05-17 14:09:17', '1');
INSERT INTO `category` VALUES ('84', 'asdadsa', '79', null, '79', '2017-05-08 11:17:19', '2017-05-08 11:11:44', '2017-05-08 11:17:19', '2');
INSERT INTO `category` VALUES ('85', 'ვიდე კამერა', '79', null, '79', null, '2017-05-08 11:15:29', '2017-05-17 14:09:32', '1');
INSERT INTO `category` VALUES ('86', 'წვრილი ტექნიკა', '1', null, '1', null, '2017-05-17 14:08:13', '2017-05-17 14:08:13', '1');
INSERT INTO `category` VALUES ('87', 'ჩასაშენებელი ტექნიკა', '1', null, '1', null, '2017-05-17 14:08:26', '2017-05-17 14:08:26', '1');
INSERT INTO `category` VALUES ('88', '3D ტელევიზორები', '74', null, '74', null, '2017-05-17 14:08:55', '2017-06-01 12:01:58', '1');
INSERT INTO `category` VALUES ('89', 'მობილური ტელეფონი', '80', null, '80', null, '2017-05-17 14:10:02', '2017-05-17 14:10:03', '1');
INSERT INTO `category` VALUES ('90', 'პლანშეტი', '80', null, '80', null, '2017-05-17 14:11:06', '2017-05-17 14:11:06', '1');
INSERT INTO `category` VALUES ('91', 'ჭკვიანი საათი', '80', null, '80', null, '2017-05-17 14:11:17', '2017-05-17 14:11:17', '1');
INSERT INTO `category` VALUES ('92', 'მაცივარი', '82', null, '82', null, '2017-05-17 14:11:32', '2017-05-17 14:11:32', '1');
INSERT INTO `category` VALUES ('93', 'სარეცხი მანქანა', '82', null, '82', null, '2017-05-17 14:11:43', '2017-05-17 14:11:43', '1');
INSERT INTO `category` VALUES ('94', 'საშრობი', '82', null, '82', null, '2017-05-17 14:11:51', '2017-05-17 14:11:52', '1');
INSERT INTO `category` VALUES ('95', 'ჭურჭლის სარეცხი მანქანა', '82', null, '82', null, '2017-05-17 14:12:00', '2017-05-17 14:12:01', '1');
INSERT INTO `category` VALUES ('96', 'ქურა', '82', null, '82', null, '2017-05-17 14:12:15', '2017-05-17 14:12:15', '1');
INSERT INTO `category` VALUES ('97', 'გამწოვი', '82', null, '82', null, '2017-05-17 14:12:21', '2017-05-17 14:12:21', '1');
INSERT INTO `category` VALUES ('98', 'ჩაიდანი', '86', null, '86', null, '2017-05-18 11:37:52', '2017-05-18 11:37:52', '1');

-- ----------------------------
-- Table structure for cities
-- ----------------------------
DROP TABLE IF EXISTS `cities`;
CREATE TABLE `cities` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `name_ge` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `name_en` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `name_ru` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `delivery_time` int(2) DEFAULT NULL,
  `state` int(2) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cities
-- ----------------------------
INSERT INTO `cities` VALUES ('1', 'თბილისი', 'Tbilisi', 'Tbilisi', '5.00', '1', '1', '2017-12-25 16:45:15', '2017-12-25 17:35:39');
INSERT INTO `cities` VALUES ('2', 'რუსთავი', 'Rustavi', 'Rustavi', '3.00', '2', '1', '2017-12-25 16:46:09', '2017-12-25 17:35:37');

-- ----------------------------
-- Table structure for langs
-- ----------------------------
DROP TABLE IF EXISTS `langs`;
CREATE TABLE `langs` (
  `lang_id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `text_ka` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `text_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `text_ru` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `text_tr` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`lang_id`)
) ENGINE=InnoDB AUTO_INCREMENT=288 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of langs
-- ----------------------------
INSERT INTO `langs` VALUES ('1', 'ACTION', 'მოქმედება', 'Action', 'Действие', 'Action', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('2', 'ACTIVE', 'აქტიური', 'Active', 'Активный', 'Active', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('3', 'ADD', 'დამატება', 'Add', 'Добавить', 'Add', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('4', 'ADDRESS', 'მისამართი', 'Address', 'Адрес', 'Address', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('5', 'CANCEL', 'გაუქმება', 'Cancel', 'Отменить', 'Cancel', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('6', 'CHANGE_PASSWORD', 'პაროლის შეცვლა', 'Change Password', 'Сменить пароль', 'Change Password', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('7', 'CLEAR', 'გასუფთავება', 'Clear', 'Очистить', 'Clear', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('8', 'CLOSE', 'დახურვა', 'Close', 'Закрыть', 'Close', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('9', 'DATE', 'თარიღი', 'Date', 'Дата', 'Date', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('10', 'DELETE', 'წაშლა', 'Delete', 'Удалить', 'Delete', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('11', 'EDIT', 'რედაქტირება', 'Edit', 'Редактировать', 'Edit', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('12', 'ERROR', 'შეცდომა', 'Error', 'Ошибка', 'Error', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('13', 'EXIT', 'გასვლა', 'Logout', 'Выход', 'Logout', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('14', 'EXPORT', 'ექსპორტი', 'Export', 'Экспорт', 'Export', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('15', 'FINALIZE', 'დასრულება', 'Finalize', 'Завершить', 'Finalize', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('16', 'FIRST_NAME', 'სახელი', 'Firstname', 'Имя', 'Firstname', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('17', 'FROM_WHO', 'ვისგან', 'From', 'Из', 'From', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('18', 'HISTORY', 'ისტორია', 'History', 'История', 'History', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('19', 'IDENTITY_CODE', 'საიდენტიფიკაციო კოდი', 'Identity code', 'Идентификационный код', 'Identity code', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('20', 'INACTIVE', 'არააქტიური', 'არააქტიური', 'Неактивный', 'არააქტიური', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('21', 'INFO', 'ინფო', 'Info', 'Инфо', 'Info', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('22', 'IP_ADDRESS', 'IP მისამართი', 'IP Address', 'IP адрес', 'IP Address', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('23', 'JULY', 'ივლისი', 'July', 'Июль', 'July', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('24', 'JUNE', 'ივნისი', 'June', 'Июнь', 'June', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('25', 'LANGUAGE', 'ენა', 'Language', 'Язык', 'Language', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('26', 'LAST_NAME', 'გვარი', 'Lastname', 'Фамилия', 'Lastname', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('27', 'LOGIN', 'შესვლა', 'Login', 'Войти', 'Login', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('28', 'MAIN_INFO', 'ძირითადი ინფორმაცია', 'Basic Information', 'Основная информация', 'Basic Information', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('29', 'MARCH', 'მარტი', 'March', 'Март', 'March', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('30', 'MAY', 'მაისი', 'May', 'Май', 'May', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('31', 'NEW_PASSWORD', 'ახალი პაროლი', 'New password', 'Новый пароль', 'New password', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('32', 'NO_RECORDS_FOUND', 'ვერ მოიძებნა ჩანაწერები', 'No records found', 'Записей не найдено', 'No records found', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('33', 'NOTE', 'შენიშვნა', 'Note', 'Note', 'Note', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('34', 'NOVEMBER', 'ნოემბერი', 'November', 'Ноябрь', 'November', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('35', 'OCTOBER', 'ოქტომბერი', 'October', 'Октябрь', 'October', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('36', 'OLD_PASSWORD', 'ძველი პაროლი', 'Old password', 'Старый пароль', 'Old password', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('37', 'OPTIONS', 'პარამეტრები', 'Settings', 'Опции', 'Settings', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('38', 'ORGANISATION', 'ორგანიზაცია', 'Organisation', 'Организация', 'Organisation', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('39', 'OVERDUE', 'ვადაგასული', 'Overdue', 'Истекший', 'Overdue', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('40', 'OWN', 'საკუთარი', 'Own', 'Собственный', 'Own', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('41', 'PAGE', 'გვერდი', 'Page', 'Страница', 'Page', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('42', 'PAGES', 'გვერდები', 'Pages', 'Страницы', 'Pages', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('43', 'PASSWORD', 'პაროლი', 'Password', 'Пароль', 'Password', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('44', 'PERSONAL', 'პირადი', 'Personal', 'Личное', 'Personal', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('45', 'PLEASE_WAIT', 'გთხოვთ, დაელოდოთ', 'Please Wait...', 'Пожалуйста, подождите', 'Please Wait...', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('46', 'PRINT', 'ბეჭდვა', 'Print', 'Печать', 'Print', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('47', 'PRINT_PREVIEW', 'საბეჭდად გადახედვა', 'Print Preview', 'Предварительный просмотр', 'Print Preview', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('48', 'REFRESH', 'განახლება', 'Refresh', 'Обновить', 'Refresh', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('49', 'REPEAT_NEW_PASSWORD', 'გაიმეორეთ ახალი პაროლი', 'Repeat new password', 'Повторите новый пароль', 'Repeat new password', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('50', 'RESULT', 'შედეგი', 'Result', 'Результат', 'Result', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('51', 'SAVE', 'შენახვა', 'Save', 'Сохранить', 'Save', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('52', 'SEARCH', 'ძებნა', 'Search', 'Поиск', 'Search', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('53', 'SELECT', 'არჩევა', 'Select', 'Выбирать', 'Select', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('54', 'SELECT_A_FILE', 'აირჩიეთ ფაილი', 'Select a file', 'Выберите файл', 'Select a file', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('55', 'SEND', 'გადაგზავნა', 'Send', 'Переслать', 'Send', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('56', 'SEPTEMBER', 'სექტემბერი', 'September', 'Сентябрь', 'September', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('57', 'STATES', 'სტატუსები', 'Statuses', 'Статусы', 'Statuses', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('58', 'STATE', 'სტატუსი', 'State', 'Статус', 'State', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('59', 'TIME', 'დრო', 'Time', 'Время', 'Time', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('60', 'TITLE', 'სათაური', 'Title', 'название', 'Title', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('61', 'TEXT', 'ტექსტი', 'Text', 'Текст', 'Text', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('62', 'TO_ARCHIVE', 'დაარქივება', 'Archive', 'Архивировать', 'Archive', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('63', 'TO_WHO', 'ვის', 'To', 'Кому', 'To', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('64', 'TOTAL', 'სულ', 'Total', 'Всего', 'Total', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('65', 'TYPE', 'ტიპი', 'Type', 'Вид', 'Type', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('66', 'USERNAME', 'მომხმარებელი', 'User', 'Пользователь', 'User', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('67', 'VERSION', 'ვერსია', 'Version', 'Версия', 'Version', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('68', 'VIEW_ALL', 'ყველას ნახვა', 'View All', 'View All', 'View All', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('69', 'PARCEL_NO', 'ამანათი', 'Parcel #', 'посылка', 'Parcel #', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('70', 'MARK_AS_REGISTERED', 'დადასტურება', 'დადასტურება', 'подтвердить', 'დადასტურება', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('71', 'FILENAMES', 'ფაილები', 'filenames', 'файлы', 'filenames', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('72', 'ADD_INGREDIENT', 'ინგრედიენტის დამატება', 'Add ingredient', 'добавить ингридиент', 'Add ingredient', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('73', 'ADMINISTRATOR', 'ადმინისტრატორი', 'Administrator', 'администратор', 'Administrator', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('74', 'AMOUNT', 'რაოდენობა', 'Amount', 'количество', 'Amount', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('75', 'ASK_DELETE', 'გსურთ წაშლა?', 'Do you want to delete?', 'Удалить?', 'Do you want to delete?', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('76', 'CASHIER', 'მოლარე', 'Cashier', 'кассир', 'Cashier', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('77', 'CATEGORIES', 'კატეგორიები', 'Categories', 'категории', 'Categories', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('78', 'CATEGORY', 'კატეგორია', 'Category', 'категория', 'Category', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('79', 'CHOOSE_SECTION', 'მიუთითეთ სექცია', 'Choose section', 'укажите секцию', 'Choose section', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('80', 'DEFAULT_COLOR', 'თავდაპირველი ფერი', 'Default color', 'начальный цвет', 'Default color', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('81', 'DESCRIPTION', 'აღწერა', 'Description', 'описание', 'Description', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('82', 'FOODS', 'კერძები', 'Foods', 'блюда', 'Foods', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('83', 'IMAGE', 'სურათი', 'Image', 'изображение', 'Image', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('84', 'INGREDIENTS', 'ინგრედიენტები', 'Ingredients', 'ингридиенты', 'Ingredients', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('85', 'MIN_AMOUNT', 'კრიტ. რაოდ.', 'კრიტ. რაოდ.', 'критическое количество', 'კრიტ. რაოდ.', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('86', 'NAME1', 'დასახელება', 'Name', 'Наименование', 'Name', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('87', 'NAME2', 'სახელი', 'Name', 'Имя', 'Name', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('88', 'NAVIGATION', 'ნავიგაცია', 'Navigation', 'навигация', 'Navigation', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('89', 'NEW_SELL', 'ახალი გაყიდვა', 'New sell', 'новая продажа', 'New sell', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('90', 'NOTICE', 'შეტყობინება', 'Notice', 'сообщение', 'Notice', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('91', 'PLACE', 'ადგილი', 'Place', 'место', 'Place', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('92', 'PRICE', 'ფასი', 'Price', 'цена', 'Price', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('93', 'RECIEVINGS', 'მიღებები', 'Recievings', 'закупки', 'Recievings', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('94', 'REPORTS', 'რეპორტები', 'Reports', 'отчеты', 'Reports', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('95', 'SELLS', 'გაყიდვები', 'Sells', 'продажи', 'Sells', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('96', 'TABLES', 'მაგიდები', 'Tables', 'столы', 'Tables', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('97', 'SECTIONS', 'სექციები', 'Sections', 'секции', 'Sections', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('98', 'PLACES', 'ადგილები', 'Places', 'места', null, '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('99', 'NEW_CATEGORY', 'ახალი კატეგორია', 'New category', 'новая категория', 'New category', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('100', 'ADD_CATEGORY', 'კატეგორიის დამატება', 'Add category', 'добавить категорию', 'Add category', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('101', 'EDIT_CATEGORY', 'კატეგორიის რედაქტირება', 'Edit category', 'редактирование категории', 'Edit category', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('102', 'ONE_PRICE', 'ერთ. ფასი', 'One price', 'цена единицы', 'One price', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('103', 'WHOLEPRICE', 'ჯამი', 'Whole price', 'сумма', 'Whole price', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('104', 'INVOICE_NO', 'ინვოისის №', 'Invoice №', '№ Инвойса', 'Invoice №', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('105', 'PROVIDER', 'მომწოდებელი', 'Provider', 'поставщик', 'Provider', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('106', 'MANUFACTURER', 'მწარმოებელი', 'Manufacturer', 'изготовитель', 'Manufacturer', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('107', 'YES', 'დიახ', 'Yes', 'Да', 'Yes', '0000-00-00 00:00:00', '2017-05-02 13:21:05');
INSERT INTO `langs` VALUES ('108', 'NO', 'არა', 'No', 'Нет', 'No', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('109', 'NEW_INGREDIENT', 'ახალი ინგრედიენტი', 'New ingredient', 'новый ингридиент', 'New ingredient', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('110', 'RECIEVE_INGREDIENT', 'ინგრედიენტის მიღება', 'Recieve ingredient', 'прием ингридиента', 'Recieve ingredient', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('111', 'CHOOSE_TABLE', 'მიუთითეთ მაგიდა', 'Choose table', 'укажите стол', 'Choose table', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('112', 'CASH', 'ნაღდი', 'Cash', 'наличные', 'Cash', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('113', 'TRANSFER', 'გადარიცხვა', 'Transfer', 'безналичные', 'Transfer', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('114', 'NEW_PLACE', 'ახალი ადგილი', 'New place', 'новое место', 'New place', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('115', 'ADD_PLACE', 'ადგილის დამატება', 'Add place', 'добавить место', 'Add place', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('116', 'ADD_INGREDIENT_ON_PRODUCT', 'პროდუქტზე ინგრედიენტის დამატება', 'Add ingredient on product', 'добавить ингридиент в продукт', 'Add ingredient on product', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('117', 'INGREDIENT', 'ინგრედიენტი', 'Ingredient', 'ингридиент', 'Ingredient', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('118', 'NEW_PRODUCT', 'ახალი პროდუქტი', 'new Product', 'Новый товар', 'new Product', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('119', 'ADD_PRODUCT', 'კერძის დამატება', 'Add product', 'добавить блюдо', 'Add product', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('120', 'ADD_SECTION', 'სექციის დამატება', 'Add section', 'добавить секцию', 'Add section', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('121', 'SECTION', 'სექცია', 'Section', 'секциа', 'Section', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('122', 'FILL_ALL_FIELDS', 'გთხოვთ მიუთითოთ ყველა აუცილებელი ველი', 'Fill all required fields', 'заполните все обязательные поля', 'Fill all required fields', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('123', 'ADD_TABLE', 'მაგიდის დამატება', 'Add table', 'добавить стол', 'Add table', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('124', 'NEW_TABLE', 'ახალი მაგიდა', 'New table', 'новый стол', 'New table', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('125', 'TABLE', 'მაგიდა', 'Table', 'стол', 'Table', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('126', 'CANCEL_ORDER', 'შეკვეთის გაუქმება', 'Cancel order', 'отмена заказа', 'Cancel order', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('127', 'USERS', 'მომხმარებლები', 'Users', 'пользователи', 'Users', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('128', 'ADD_USER', 'მომხმარებლის დამატება', 'User added', 'добавить пользователя', 'User added', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('129', 'RANK', 'რანკი', 'Rank', 'ранг', 'Rank', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('130', 'PERSONAL_NO', 'პირადი ნომერი', 'Personal No', 'личный номер', 'Personal No', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('131', 'PHONE', 'ტელეფონი', 'Phone', 'телефон', 'Phone', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('132', 'EMAIL', 'ელ. ფოსტა', 'eMail', 'эл.почта', 'eMail', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('133', 'PRINTER', 'პრინტერი', 'Printer', 'Принтер', 'Printer', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('134', 'WAITER', 'ოფიციანტი', 'Waiter', 'официант', 'Waiter', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('135', 'SELLITEM_REPORTS', 'კერძების რეპორტები', 'კერძების რეპორტები', 'Отчеты по блюдам', 'კერძების რეპორტები', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('136', 'ORDER_NUMBER', 'შეკვ. №', 'Order №', 'Заказ. №', 'Order №', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('137', 'SEARCH_ORDER', 'შეკვეთის ძებნა', 'Search Order', 'Поиск заказа', 'Search Order', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('138', 'SELF_PRICE', 'თვითღირებულება', 'Self Price', 'Себестоимость', 'Self Price', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('139', 'CALL', 'გამოძახება', 'CALL', 'Вызов', 'CALL', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('140', 'MESSAGE', 'შეტყობინება', 'Message', 'Сообщение', 'Message', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('141', 'CONFIRM', 'დასტური', 'Confirm', 'Подтвердить', 'Confirm', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('142', 'MENUS', 'მენიუები', 'Menus', 'Меню', 'Menus', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('143', 'ADD_MENU', 'მენიუს დამატება', 'Add Menu', 'Добавить меню', 'Add Menu', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('144', 'PERCENT', 'პროცენტი', 'Percent', 'Процент', 'Percent', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('145', 'TAKE_MONEY', 'მოწ. თანხა', 'Money', 'Поданная сумма', 'Money', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('146', 'RETURN_CHANGE', 'ხურდა', 'Change', 'Сдача', 'Change', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('147', 'MESSAGES', 'შეტყობინებები', 'Messages', 'Сообщения', 'Messages', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('148', 'INGREDIENTS_REPORTS', 'ინგრედიენტების რეპორტები', 'Ingredients Reports', 'Отчеты по ингридиентам', 'Ingredients Reports', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('149', 'OPERATIONS', 'ოპერაციები', 'Operations', 'Операции', 'Operations', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('150', 'RECIEVING_HISTORY', 'მიღებების ისტორია', 'Recieving History', 'История закупок', 'Recieving History', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('151', 'SELL_HISTORY', 'გაყიდვების ისტორია', 'Sell History', 'История продаж', 'Sell History', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('152', 'GUEST_COUNT', 'სტუმრების რაოდენობა', 'Guest Count', 'Кол-во гостей', 'Guest Count', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('153', 'VIEW_ORDER', 'შეკვეთის ნახვა', 'View Order', 'Смотреть заказ', 'View Order', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('154', 'ADD_IMAGE', 'სურათის დამატება', 'Add Image', 'Добавить картинку', 'Add Image', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('155', 'SEND_MSG', 'მიწერე მიმტანს', 'Send Message', 'Послать сообщение', 'Send Message', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('156', 'ADDITIONAL_INFO', 'დამატებითი ინფო', 'Additional Info', 'Доп. Инфо', 'Additional Info', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('157', 'ADD_ADDITIONAL_INFO', 'დამატებითი ინფოს დამატება', 'Add Additional Info', 'Добавить доп. инфо', 'Add Additional Info', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('158', 'ADD_NEW_ADDETIONAL_INFO_TYPE', 'ახალი დამატებითი ინფოს დამატება', 'Add new additional info type', 'Добавить новое доп. инфо', 'Add new additional info type', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('159', 'CHOOSE_FILE', 'აირჩიეთ ფაილი', 'Choose file', 'Выбрать файл', 'Choose file', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('160', 'RESOURCES', 'რესურსები', 'Resources', 'Ресурсы', 'Resources', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('161', 'FILE_SIZE_ERROR_30', 'ფაილის ზომა არ უნდა აღემატებოდეს 30 kb-ს', 'Max allowed file size is 30 kb', 'Размер файла не должен превышать 30 Кб', 'Max allowed file size is 30 kb', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('162', 'FILE_SIZE_ERROR_500', 'ფაილის ზომა არ უნდა აღემატებოდეს 500 kb-ს', 'Max allowed file size is 500 kb', 'Размер файла не должен превышать 500 Кб', 'Max allowed file size is 500 kb', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('163', 'CHOOSE_FILE_ERROR', 'გთხოვთ მიუთითოთ ფაილი', 'Choose file', 'Укажите файл', 'Choose file', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('164', 'RECIEVE_PRODUCT', 'პროდუქტის მიღება', 'Recieve product', 'Принять товар', 'Recieve product', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('165', 'SPLIT_AMOUNT', 'დაშლილი რაოდენობა', 'Split Amount', 'Штучное кол-во', 'Split Amount', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('166', 'BARCODE', 'შტრიხკოდი', 'Barcode', 'Штрихкод', 'Barcode', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('167', 'IN_STORAGE', 'საწყობში', 'In Storage', 'В складе', 'In Storage', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('168', 'RECIEVE_BY_INVOICE', 'ინვოისით მიღება', 'Recieve by Invoice', 'Прием по инвойсу', 'Recieve by Invoice', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('169', 'WEIGHT', 'წონა', 'Weight', 'Вес', 'Weight', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('170', 'WARRANTY', 'გარანტია', 'Warranty', 'Гарантия', 'Warranty', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('171', 'CONDITION', 'მდგომარეობა', 'Condition', 'Состояние', 'Condition', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('172', 'NEW', 'ახალი', 'New', 'Новый', 'New', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('173', 'USED', 'მეორადი', 'Used', 'Вторичный', 'Used', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('174', 'PRICE_WHOLESALE', 'ფასი საბითუმო', 'Price Wholesale', 'Оптовая цена', 'Price Wholesale', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('175', 'PRICE_VIP', 'ფასი VIP', 'Price VIP', 'Цена VIP', 'Price VIP', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('176', 'SPLIT', 'დაშლა', 'Split', 'Поштучно', 'Split', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('177', 'FILL_SPLIT_AMOUNT', 'მიუთითეთ დაშლის რაოდენობა', 'Fill Split Amount', 'Укажите поштучное кол-во', 'Fill Split Amount', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('178', 'INVOICE_NUMBER', 'ინვოისის ნომერი', 'Invoice Number', 'Номер инвойса', 'Invoice Number', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('179', 'FILL_ALL_IMPORTANT_FIELDS', 'შეავსეთ ყველა სავალდებულო ველი', 'Fill all important fields', 'Заполните все обязательные поля', 'Fill all important fields', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('180', 'SEARCH_PRODUCT', 'პროდუქტის ძებნა', 'Search Product', 'Поиск товара', 'Search Product', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('181', 'BRANCH', 'ფილიალი', 'Branch', 'Филиал', 'Branch', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('182', 'WHOLE', 'მთლიანი', 'Whole', 'Целый', 'Whole', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('183', 'PRODUCTS', 'პროდუქტები', 'Products', 'Товары', 'Products', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('184', 'CLIENTS', 'კლიენტები', 'Clients', 'Клиенты', 'Clients', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('185', 'PAYMENTS', 'გადახდები', 'Payments', 'Платежи', 'Payments', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('186', 'ADD_PAYMENT', 'გადახდა', 'New Payment', 'Оплатить', 'New Payment', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('187', 'PAYMENT_TYPE', 'გადახდის ტიპი', 'Payment Type', 'Тип оплаты', 'Payment Type', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('188', 'MONEY', 'თანხა', 'Money', 'Сумма', 'Money', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('189', 'CHANGE', 'შეცვლა', 'Change', 'Изменить', 'Change', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('190', 'SELLER', 'გამყიდველი', 'Seller', 'Продавец', 'Seller', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('191', 'SPLIT_PRICE', 'დაშლის ფასი', 'Split Price', 'Поштучнай цена', 'Split Price', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('192', 'DATABASE_NULL', 'ბაზის განულება', 'Delete Database', 'Обнулить базу', 'Delete Database', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('193', 'DATABASE_NULLED_OK', 'ბაზა განულდა წარმატებით', 'Database was nulled succesfuly', 'База обнулена успешно', 'Database was nulled succesfuly', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('194', 'REMOVED', 'გაუქმებული', 'Removed', 'Отенено', 'Removed', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('195', 'CHANGE_LANGUAGE', 'ენის შეცვლა', 'Change Language', 'Изменить язык', 'Change Language', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('196', 'CHANGE_THEME', 'თემის შეცვლა', 'Change Theme', 'Изменить тему', 'Change Theme', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('197', 'BONUS', 'ბონუსი', 'Bonus', 'Бонус', 'Bonus', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('198', 'NEW_CLIENT', 'ახალი კლიენტი', 'New Client', 'Новый клиент', 'New Client', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('199', 'CLIENT', 'კლიენტი', 'Client', 'Клиент', 'Client', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('200', 'CHOOSE_CLIENT', 'კლიენტის არჩევა', 'Choose Client', 'Выбрать клиента', 'Choose Client', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('201', 'AMOUNT_IN_STORAGE', 'რაოდ. საწყობში', 'Amount in Storage', 'Кол-во на складе', 'Amount in Storage', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('202', 'NEED_RECEIPT', 'საჭიროებს რეცეპტს', 'Need Receipt', 'Нужен рецепт', 'Need Receipt', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('203', 'EARNING', 'მოგება', 'Earning', 'Прибыль', 'Earning', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('204', 'SELL_PRICE', 'გასაყიდი ფასი', 'Sell Price', 'Продажная цена', 'Sell Price', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('205', 'VAT', 'დ.ღ.გ.', 'VAT', 'Н.Д.С.', 'VAT', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('206', 'INCLUDE_VAT', 'დ.ღ.გ.–ის ჩათვლით', 'Include VAT', 'С учетом НДС', 'Include VAT', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('207', 'WITHOUT_VAT', 'დ.ღ.გ.–ის გარეშე', 'Without VAT', 'Без НДС', 'Without VAT', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('208', 'CONFIRM_PRODUCT_SELL_PRICE_CHANGE', 'პროდუქტის გასაყიდი ფასი შეიცვალა. გინდათ შეიცვალოს პროდუქტის გასაყიდი ფასი?', 'Product sell price was changed. Do you want to change product sell price with new price?', 'Продажная ценв товара изменилась.', 'Product sell price was changed. Do you want to change product sell price with new price?', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('209', 'CALCULATE_PERCENT', 'პროცენტის გამოთვლა', 'Calculate Percent', 'Подсчет процентов', 'Calculate Percent', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('210', 'FROM', 'დან', 'From', 'От', 'From', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('211', 'BEFORE', 'მდე', 'Before', 'До', 'Before', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('212', 'PRODUCT', 'პროდუქტი', 'Product', 'Продукт', 'Product', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('213', 'SOLD_PRODUCTS', 'გაყიდული პროდუქტები', 'Sold Products', 'Продано Продукты', 'Sold Products', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('214', 'THIS_PRODUCT_IS_NOT_IN_STORAGE', 'მითითებული პროდუქტი არ არის საწყობში', 'This product is not in storage', 'Этот продукт не в памяти', 'This product is not in storage', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('215', 'CHOOSE_PRODUCT', 'აირჩიეთ პროდუქტი', 'Choose Product', 'Выберите продукт', 'Choose Product', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('216', 'SELECTED_NUMBER_IS_GREATER_THAN_THE_NUMBER_OF_STOCK', 'მითითებული რაოდენობა მეტია საწყობში არსებულ რაოდენობაზე', 'Set the number is greater than the number of stock', 'Установите число больше, чем число акций', 'Set the number is greater than the number of stock', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('217', 'SPECIFY_THE_NUMBER_OF_SALE', 'მიუთითეთ გასაყიდი რაოდენობა', 'Specify the number of sale', 'Укажите количество продажи', 'Specify the number of sale', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('218', 'ADD_SUBCATEGORY', 'ქვეკატეგორიის დამატება', 'Add Subcategory', 'Добавить подкатегорию', 'Add Subcategory', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('219', 'PARENT', 'მშობელი', 'Parent', 'Родитель', 'Parent', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('220', 'DISCARD_INGREDIENT', 'ინგრედიენტის ჩამოწერა', 'Discard Ingredient', 'ინგრედიენტის ჩამოწერა', 'Discard Ingredient', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('221', 'DISCARDED_INGREDIENTS', 'ჩამოწერილი ინრედიენტები', 'Discarded Ingredients', 'ჩამოწერილი ინრედიენტები', 'Discarded Ingredients', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('222', 'DISCARD_AMOUNT', 'ჩამოსაწერი რაოდენობა', 'Discard Amount', 'ჩამოსაწერი რაოდენობა', 'Discard Amount', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('223', 'DATABASE', 'ბაზა', 'Database', 'ბაზა', 'Database', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('224', 'DOWNLOAD_DATABASE_FOR_WEB', 'ბაზის ჩამოტვირთვა საიტისთვის', 'Download Database for WEB', 'ბაზის ჩამოტვირთვა საიტისთვის', 'Download Database for WEB', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('225', 'REMAINS', 'ნაშთები', 'Remains', 'ნაშთები', 'Remains', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('226', 'PRE_CHECK', 'წინასწარი ჩეკი', 'Pre Check', 'წინასწარი ჩეკი', 'Pre Check', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('227', 'SALE_PERCENT', 'ფასდაკ. %', 'Sale %', 'ფასდაკლება %', 'Sale %', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('228', 'CODE', 'კოდი', 'Code', 'Code', 'Code', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('229', 'INCORRECT_VALUES', 'არასწორი მონაცემები!', 'Incorrect values', 'Incorrect values', 'Incorrect values', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('230', 'DAY_TRADED_VOLUME', 'ღის ნავაჭრი', 'Day trading volume', 'Day trading volume', 'Day trading volume', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('231', 'MAIN_PAGE', 'მთავარი გვერდი', 'Main Page', 'Main Page', 'Main Page', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('232', 'COMMENT', 'კომენტარი', 'Comment', 'Comment', 'Comment', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('233', 'ORDERS', 'შეკვეთები', 'Orders', 'Orders', 'Orders', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('234', 'SELLING_PRICE_HAS_NOT_SET', 'გასაყიდი ფასი არ აქვს მითითებული', 'Selling price has not set', 'Selling price has not set', 'Selling price has not set', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('235', 'NEW_SECTION', 'ახალი სექცია', 'New Saction', 'New Saction', 'New Saction', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('236', 'PORTION', 'პორცია', 'Portion', 'Portion', 'Portion', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('237', 'ADD_TYPE', 'ტიპის დამატება', 'Add Type', 'Add Type', 'Add Type', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('238', 'AMOUNT_SHORT', 'რაოდ.', 'Am.', 'Am.', 'Am.', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('239', 'FOR_SERVICE', 'მომსახურების', 'For Service', 'For Service', 'For Service', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('240', 'GEL', 'ლ', 'GEL', 'GEL', 'GEL', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('241', 'SALE', 'ფასდაკლება', 'Sale', 'Sale', 'Sale', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('242', 'PLACE_OF_MAKING', 'გატანის ადგილი', 'Place Of Making', 'Place Of Making', 'Place Of Making', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('243', 'SUM', 'ჯამი', 'Sum', 'Sum', 'Sum', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('244', 'LANGUAGES', 'ენები', 'Languages', 'Languages', 'Languages', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('245', 'NEW_PHRASE', 'ახალი ფრაზა', 'New Phrase', 'New Phrase', 'New Phrase', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('246', 'TURKISH', 'თურქულად', 'Turkish', 'Turkish', 'Turkish', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('247', 'ENGLISH', 'ინგლისურად', 'English', 'English', 'English', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('248', 'RUSSIAN', 'რუსულად', 'Russian', 'Russian', 'Russian', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('249', 'GEORGIAN', 'ქართულად', 'Georgian', 'Georgian', 'Georgian', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('250', 'WAYBILLS', 'ზედნადებები', 'Waybills', 'Waybills', 'Waybills', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('251', 'WAYBILL_NO', 'ზედნადებების №', 'Waybill No', 'Waybill No', 'Waybill No', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('252', 'SELLER_NAME', 'გამყიდველი', 'Seller Name', 'Seller Name', 'Seller Name', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('253', 'ACTIVATE_DATE', 'აქტივაციის თარიღი', 'Activate Date', 'Activate Date', 'Activate Date', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('254', 'CREATE_DATE', 'შექმნის თარიღი', 'Create Date', 'Create Date', 'Create Date', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('255', 'CLOSE_DATE', 'დასრულების თარიღი', 'Close Date', 'Close Date', 'Close Date', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('256', 'DRIVER_NAME', 'მძღოლი', 'Driver Name', 'Driver Name', 'Driver Name', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('257', 'CAR', 'ავტო', 'Car', 'Car', 'Car', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('258', 'TRANS_PRICE', 'ტრანს. თანხა', 'Trans. Price', 'Trans. Price', 'Trans. Price', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('259', 'DELIVERY_DATE', 'მიწოდების თარიღი', 'Delivery Date', 'Delivery Date', 'Delivery Date', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('260', 'VIEW', 'ნახვა', 'View', 'View', 'View', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('261', 'CONFIRM_RECEIVE', 'მიღების დადასტურება', 'Confirm Receive', 'Confirm Receive', 'Confirm Receive', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('262', 'BAR_CODE', 'შტრიხკოდი', 'Barcode', 'Barcode', 'Barcode', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('263', 'ADD_BARCODE', 'შტრიხკოდის დამატება', 'Add Barcode', 'Add Barcode', 'Add Barcode', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('264', 'RECEIVE', 'მიღება', 'Receive', 'Receive', 'Receive', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('265', 'CAR_NO', 'მანქანის №', 'Car No', 'Car No', 'Car No', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('266', 'GRANT_RIGHTS', 'უფლებების მინიჭება', 'Grant Rights', 'Grant Rights', 'Grant Rights', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('267', 'SUPPLIERS', 'მომწოდებლები', 'Suppliers', 'Suppliers', 'Suppliers', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('268', 'TRADED', 'ნავაჭრი', 'Traded', 'Traded', 'Traded', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('269', 'RECEIPT', 'რეცეპტი', 'Receipt', 'Receipt', 'Receipt', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('270', 'STORAGE', 'საწყობი', 'Storage', 'Storage', 'Storage', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('271', 'DISCARDS', 'ჩამოწერები', 'Discards', 'Discards', 'Discards', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('272', 'PAYED_MONEY', 'გადახდილი თანხა', 'Payed Money', 'Payed Money', 'Payed Money', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('273', 'DEADLINE', 'ვადა', 'Deadline', 'Deadline', 'Deadline', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('275', 'UPLOAD_TO_RS', 'აიტვირთოს rs.ge-ზე', 'Upload to RS', 'Upload to RS', 'Upload to RS', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('276', 'CARD', 'ბარათი', 'Card', 'Card', 'Card', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('277', 'DEBT', 'ვალი', 'Debt', 'Debt', 'Debt', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('278', 'TIN', 'საიდენტიფიკაციო ნომერი', 'TIN', 'TIN', 'TIN', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('279', 'DRIVERS', 'მძღოლები', 'Drivers', 'Drivers', 'Drivers', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('280', 'DRIVER', 'მძღოლი', 'Driver', 'Driver', 'Driver', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('281', 'CHOOSE_DRIVER', 'აირჩიეთ მძღოლი', 'Select Driver', 'Select Driver', 'Select Driver', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('282', 'NEW_DRIVER', 'ახალი მძღოლი', 'New Driver', 'New Driver', 'New Driver', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('283', 'CARS', 'მანქანები', 'Cars', 'Cars', 'Cars', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('284', 'STORAGES', 'საწყობები', 'Storages', 'Storages', 'Storages', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('285', 'ADD_STORAGE', 'საწყობის დამატება', 'Add Storage', 'Add Storage', 'Add Storage', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('286', 'MOVE_PRODUCTS', 'პროდუქტების გადატანა', 'Move Products', 'Move Products', 'Move Products', '0000-00-00 00:00:00', null);
INSERT INTO `langs` VALUES ('287', 'TRUCK', '????????', 'Truck', 'Truck', 'Truck', '0000-00-00 00:00:00', null);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('3', '2017_05_02_140156_entrust_setup_tables', '2');
INSERT INTO `migrations` VALUES ('4', '2017_05_05_144232_entrust_setup_tables', '3');
INSERT INTO `migrations` VALUES ('5', '2017_06_01_102836_create_admins_table', '4');
INSERT INTO `migrations` VALUES ('6', '2017_06_05_074505_entrust_setup_tables', '5');
INSERT INTO `migrations` VALUES ('7', '2017_06_05_095027_entrust_setup_tables', '6');
INSERT INTO `migrations` VALUES ('8', '2017_12_06_122924_create_shoppingcart_table', '7');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for payments
-- ----------------------------
DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments` (
  `transaction_id` varchar(32) DEFAULT NULL,
  `payment_id` varchar(12) DEFAULT NULL,
  `payment_date` varchar(19) DEFAULT NULL,
  `amount` varchar(12) DEFAULT NULL,
  `card_number` varchar(25) DEFAULT NULL,
  `card_type` varchar(25) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `reason` varchar(64) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of payments
-- ----------------------------

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES ('1', 'Test 1', 'Test 1', 'Test 1', '2017-06-05 09:55:58', '2017-06-05 09:55:58');
INSERT INTO `permissions` VALUES ('2', 'Test 2', 'Test 2', 'Test 2', '2017-06-05 09:56:04', '2017-06-05 09:56:04');
INSERT INTO `permissions` VALUES ('3', 'Test 3', 'Test 3', 'Test 3', '2017-06-05 09:56:09', '2017-06-05 09:56:09');
INSERT INTO `permissions` VALUES ('4', 'Test 4', 'Test 4', 'Test 4', '2017-06-05 09:56:14', '2017-06-05 09:56:14');
INSERT INTO `permissions` VALUES ('5', 'Test 5', 'Test 5', 'Test 5', '2017-06-05 09:56:19', '2017-06-05 09:56:19');

-- ----------------------------
-- Table structure for permission_role
-- ----------------------------
DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of permission_role
-- ----------------------------
INSERT INTO `permission_role` VALUES ('1', '1');
INSERT INTO `permission_role` VALUES ('1', '2');
INSERT INTO `permission_role` VALUES ('1', '3');
INSERT INTO `permission_role` VALUES ('2', '2');
INSERT INTO `permission_role` VALUES ('2', '3');
INSERT INTO `permission_role` VALUES ('3', '3');
INSERT INTO `permission_role` VALUES ('4', '3');
INSERT INTO `permission_role` VALUES ('5', '3');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `barcode` varchar(60) CHARACTER SET utf8 DEFAULT '',
  `name` text CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8,
  `category_id` int(11) NOT NULL,
  `minamount` decimal(10,2) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT '0.00',
  `condition_id` int(11) DEFAULT NULL,
  `state_id` int(2) NOT NULL DEFAULT '1',
  `self_price` decimal(15,2) DEFAULT '0.00',
  `last_price` decimal(10,2) DEFAULT NULL,
  `unit_name` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`product_id`),
  KEY `SUPPLIER_CODE_INDX` (`supplier_code`) USING BTREE,
  KEY `PRODUCT_ID_INX` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('1', null, '25', 'ზეთი \"მზიური\"', 'asd', '80', null, '2.00', '1', '2', '2.00', '2.00', 'ცალი', '1', '2017-05-17 14:03:29', null);
INSERT INTO `products` VALUES ('2', null, '', 'ზალატოი', '', '80', null, '3.00', '1', '2', '3.00', '3.00', '', null, '2017-05-17 14:03:30', null);
INSERT INTO `products` VALUES ('3', null, '', 'ქარვა', '', '80', null, '1.00', '1', '2', '1.00', '1.00', '', null, '2017-05-17 14:03:22', null);
INSERT INTO `products` VALUES ('4', null, '', 'ბრავიტა', '', '80', null, '3.00', '1', '2', '3.00', '3.00', '', null, '2017-05-11 10:39:39', null);
INSERT INTO `products` VALUES ('5', null, '', 'სკუმბრია', '', '82', null, '12.00', '1', '2', '12.00', '12.00', '', null, '2017-05-17 14:03:24', null);
INSERT INTO `products` VALUES ('6', null, '', 'კალმახი', '', '81', null, '10.00', '1', '2', '10.00', '10.00', '', null, '2017-05-17 14:03:28', null);
INSERT INTO `products` VALUES ('7', null, '', 'ლოქო', '', '81', null, '3.00', '1', '2', '3.00', '3.00', '', null, '2017-05-17 14:03:26', null);
INSERT INTO `products` VALUES ('8', null, '', 'ბროილერის ქათამი', '', '79', null, '4.00', '1', '2', '4.00', '4.00', '', null, '2017-05-17 14:03:37', null);
INSERT INTO `products` VALUES ('9', null, '', 'დედალი', '', '79', null, '4.00', '1', '2', '4.00', '4.00', '', null, '2017-05-17 14:03:36', null);
INSERT INTO `products` VALUES ('10', null, null, 'BOSCH KDN46VW25U', 'ორკამერიანი მაცივარი/საყინულე\nNo frost მშრალი გაყინვა \nმართვა: ელექტრონული (სენსორები)', '92', null, '1915.00', '1', '1', '4.00', '4.00', 'ცალი', '1', '2017-05-17 14:13:06', null);
INSERT INTO `products` VALUES ('12', null, '', 'ვიბრატორიdsdsdsdsdsds', null, '1', null, '120.00', '1', '2', '0.00', null, 'ცალი', '1', '2017-05-17 14:03:34', '2017-05-03 14:31:10');
INSERT INTO `products` VALUES ('14', null, '', 'ვიბრატორიwsdfssssssssssssssssssssssss', null, '1', null, '120.00', '1', '2', '0.00', null, 'ცალი', '1', '2017-05-17 14:03:32', '2017-05-03 14:46:57');
INSERT INTO `products` VALUES ('15', null, 'sadsasa', 'sad', 'sadsasad', '81', '1.00', '2.00', '1', '2', null, null, 'გრამი', '3', '2017-05-11 11:17:23', '2017-05-11 11:17:07');
INSERT INTO `products` VALUES ('16', null, null, 'HITACHI 65HZ6W69', 'LED ტელევიზორი \nზომა: 65 ინჩი (165 სმ)\nრეზოლუცია: 3840 x 2160', '76', null, '3475.00', '1', '1', null, null, 'ცალი', '1', '2017-05-17 14:14:38', '2017-05-17 14:09:15');
INSERT INTO `products` VALUES ('17', null, null, 'SAMSUNG RB37K63412C/WT/O', 'ორკამერიანი მაცივარი/საყინულე\nმუშაობის პრინციპი: NoFrost მშრალი გაყინვა \nმართვა: ელექტრონული', '92', null, '2688.00', '1', '1', null, null, 'ცალი', '1', '2017-05-17 14:19:22', '2017-05-17 14:19:22');
INSERT INTO `products` VALUES ('18', null, null, 'SAMSUNG RB37K63412A/WT/O', 'ორკამერიანი მაცივარი/საყინულე\nმუშაობის პრინციპი: NoFrost მშრალი გაყინვა \nმართვა: ელექტრონული', '1', null, '2730.00', '1', '1', null, null, 'ცალი', '1', '2017-05-17 14:20:06', '2017-05-17 14:20:06');
INSERT INTO `products` VALUES ('19', null, null, 'GORENJE K17FE', 'ჩაიდანი\nტევადობა: 1.7 ლ\nსიმძლავრე: 2200 ვატი', '98', null, '115.00', '1', '1', null, null, 'ცალი', '1', '2017-05-18 11:38:33', '2017-05-18 11:38:33');
INSERT INTO `products` VALUES ('20', null, null, 'test', 'test', '1', null, '43.00', '1', '2', null, null, 'ცალი', '1', '2017-05-19 07:44:53', '2017-05-19 07:17:00');
INSERT INTO `products` VALUES ('21', null, 'kima', 'kima', 'kima', '74', '1.00', '2.00', '2', '2', null, null, 'ცალი', '1', '2017-11-30 17:14:14', '2017-05-19 07:14:22');

-- ----------------------------
-- Table structure for product_files
-- ----------------------------
DROP TABLE IF EXISTS `product_files`;
CREATE TABLE `product_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `original_file_name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `is_cover` tinyint(1) NOT NULL DEFAULT '0',
  `state_id` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of product_files
-- ----------------------------
INSERT INTO `product_files` VALUES ('2', '17', '557fd1ba7f55a2b6b26e984858a1d298.jpg', '557fd1ba7f55a2b6b26e984858a1d298.jpg_original', '0', '0', '2017-05-19 12:39:48', '2017-11-30 18:05:28');
INSERT INTO `product_files` VALUES ('3', '17', 'b49ad8bdcc52a1daa4d259042637139b.jpeg', 'b49ad8bdcc52a1daa4d259042637139b.jpeg_original', '0', '0', '2017-05-19 12:42:07', '2017-11-30 18:05:30');
INSERT INTO `product_files` VALUES ('4', '17', '3570d54396ddae645882d06447300bd0.jpg', '3570d54396ddae645882d06447300bd0.jpg_original', '0', '0', '2017-05-19 12:43:37', '2017-11-30 18:05:32');
INSERT INTO `product_files` VALUES ('5', '21', 'adee272e1084a765de860889582d192c.jpg', 'adee272e1084a765de860889582d192c.jpg_original', '0', '0', '2017-11-17 12:34:51', '2017-11-30 17:14:14');
INSERT INTO `product_files` VALUES ('6', '17', '56f9b0ed690f5e1b4d00459b9d3f803b.jpg', '56f9b0ed690f5e1b4d00459b9d3f803b.jpg_original', '0', '0', '2017-11-30 18:03:47', '2017-11-30 18:05:33');
INSERT INTO `product_files` VALUES ('7', '17', '03307d6ac76cb39ab665cf0fca3214ff.jpg', '03307d6ac76cb39ab665cf0fca3214ff.jpg_original', '0', '0', '2017-11-30 18:06:51', '2017-11-30 18:23:22');
INSERT INTO `product_files` VALUES ('8', '17', 'D:\\xampp\\htdocs\\onlinemarket\\public_html/files/product_files/17/0d5aadcb84c98740f447e3c75fc5ffae.jpg', '0d5aadcb84c98740f447e3c75fc5ffae.jpg_original', '0', '0', '2017-11-30 18:13:34', '2017-11-30 18:23:23');
INSERT INTO `product_files` VALUES ('9', '17', '/files/product_files/17/cf8f05dacd8845215b0b260f1092cc99.jpg', 'cf8f05dacd8845215b0b260f1092cc99.jpg_original', '0', '0', '2017-11-30 18:17:38', '2017-11-30 18:23:25');
INSERT INTO `product_files` VALUES ('10', '17', '84b3c13701d8930a15609ffbda1f0341.jpg', '84b3c13701d8930a15609ffbda1f0341.jpg_original', '0', '0', '2017-11-30 18:26:14', '2017-11-30 18:29:13');
INSERT INTO `product_files` VALUES ('11', '17', '7239b1ac7d9ea60f3d73dafaa1d449a3.jpg', '7239b1ac7d9ea60f3d73dafaa1d449a3.jpg_original', '0', '1', '2017-11-30 18:29:28', '2017-11-30 18:29:28');

-- ----------------------------
-- Table structure for product_info
-- ----------------------------
DROP TABLE IF EXISTS `product_info`;
CREATE TABLE `product_info` (
  `product_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `product_info_type_id` int(11) NOT NULL,
  `product_info_type_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `value` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `state_id` int(11) NOT NULL DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`product_info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product_info
-- ----------------------------
INSERT INTO `product_info` VALUES ('1', '1', '20', 'ტრანსმისია', 'ავტომატიკა', '2', '2017-05-03 15:21:34', '2017-05-03 15:12:38');
INSERT INTO `product_info` VALUES ('2', '1', '20', 'ტრანსმისია', 'meqanika', '1', '2017-05-11 12:03:04', '2017-05-11 12:03:04');
INSERT INTO `product_info` VALUES ('3', '1', '21', 'ტრანსმისიაssssss', 'model', '2', '2017-05-11 12:06:30', '2017-05-11 12:06:17');
INSERT INTO `product_info` VALUES ('4', '1', '23', 'xxx', 'asd', '1', '2017-05-11 12:13:12', '2017-05-11 12:13:12');

-- ----------------------------
-- Table structure for product_info_types
-- ----------------------------
DROP TABLE IF EXISTS `product_info_types`;
CREATE TABLE `product_info_types` (
  `product_info_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_info_type_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`product_info_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product_info_types
-- ----------------------------
INSERT INTO `product_info_types` VALUES ('1', 'ბრენდი', null, null);
INSERT INTO `product_info_types` VALUES ('2', 'მოდელი', null, null);
INSERT INTO `product_info_types` VALUES ('3', 'ეკრანის ზომა', null, null);
INSERT INTO `product_info_types` VALUES ('4', 'ეკრანის ტიპი', null, null);
INSERT INTO `product_info_types` VALUES ('5', 'ეკრანის რეზოლუცია', null, null);
INSERT INTO `product_info_types` VALUES ('6', 'CPU', null, null);
INSERT INTO `product_info_types` VALUES ('7', 'RAM', null, null);
INSERT INTO `product_info_types` VALUES ('8', 'HDD', null, null);
INSERT INTO `product_info_types` VALUES ('9', 'LAN', null, null);
INSERT INTO `product_info_types` VALUES ('10', 'ODD', null, null);
INSERT INTO `product_info_types` VALUES ('11', 'Display', null, null);
INSERT INTO `product_info_types` VALUES ('12', 'Battery', null, null);
INSERT INTO `product_info_types` VALUES ('13', 'OS', null, null);
INSERT INTO `product_info_types` VALUES ('14', 'Weight', null, null);
INSERT INTO `product_info_types` VALUES ('15', 'SupplierCode', null, null);
INSERT INTO `product_info_types` VALUES ('16', 'PartNumber', null, null);
INSERT INTO `product_info_types` VALUES ('17', 'ნოუთის ინფო', null, null);
INSERT INTO `product_info_types` VALUES ('18', 'მონაცემები', null, null);
INSERT INTO `product_info_types` VALUES ('20', 'ტრანსმისია', '2017-05-03 15:06:49', '2017-05-03 15:06:49');
INSERT INTO `product_info_types` VALUES ('21', 'ტრანსმისიაssssss', '2017-05-03 15:39:19', '2017-05-03 15:39:19');
INSERT INTO `product_info_types` VALUES ('22', 'ragaca', '2017-05-11 12:12:26', '2017-05-11 12:12:26');
INSERT INTO `product_info_types` VALUES ('23', 'xxx', '2017-05-11 12:12:57', '2017-05-11 12:12:57');

-- ----------------------------
-- Table structure for recievings
-- ----------------------------
DROP TABLE IF EXISTS `recievings`;
CREATE TABLE `recievings` (
  `recieving_id` int(11) DEFAULT NULL,
  `recieving_whole_price` decimal(10,2) DEFAULT NULL,
  `recieving_invoic_number` tinytext,
  `branch_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `payed_money` decimal(10,2) DEFAULT NULL,
  `supplier_code` varchar(100) DEFAULT NULL,
  `supplier_name` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of recievings
-- ----------------------------
INSERT INTO `recievings` VALUES (null, '405.00', null, null, null, '1', '35.00', null, null, '2017-06-20 11:59:04', '2017-06-20 11:59:04');
INSERT INTO `recievings` VALUES (null, '610.00', null, null, null, '1', '100.00', null, null, '2017-06-20 12:03:47', '2017-06-20 12:03:47');
INSERT INTO `recievings` VALUES (null, '204.00', null, '1', null, '1', '33.00', null, null, '2017-06-20 14:03:28', '2017-06-20 14:03:28');
INSERT INTO `recievings` VALUES (null, '10.00', null, '1', null, '1', '10.00', null, null, '2017-06-20 14:26:56', '2017-06-20 14:26:56');
INSERT INTO `recievings` VALUES (null, '10.00', null, '1', null, '1', '10.00', null, null, '2017-06-20 14:27:19', '2017-06-20 14:27:19');
INSERT INTO `recievings` VALUES (null, '30.00', null, '1', null, '1', '10.00', null, null, '2017-06-20 14:32:32', '2017-06-20 14:32:32');
INSERT INTO `recievings` VALUES (null, '20.00', null, '1', null, '1', '20.00', null, null, '2017-06-20 14:33:28', '2017-06-20 14:33:28');
INSERT INTO `recievings` VALUES (null, '10.00', null, '1', null, '1', '1.00', null, null, '2017-06-20 14:36:25', '2017-06-20 14:36:25');
INSERT INTO `recievings` VALUES (null, '10.00', null, '1', null, '1', '1.00', null, null, '2017-06-20 14:38:40', '2017-06-20 14:38:40');
INSERT INTO `recievings` VALUES (null, '10.00', null, '1', null, '1', '1.00', null, null, '2017-06-20 14:39:11', '2017-06-20 14:39:11');
INSERT INTO `recievings` VALUES (null, '20000.00', null, '1', null, '1', '200.00', null, null, '2017-06-20 15:09:09', '2017-06-20 15:09:09');
INSERT INTO `recievings` VALUES (null, '50.00', null, '1', null, '1', '20.00', null, null, '2017-06-27 11:51:58', '2017-06-27 11:51:58');
INSERT INTO `recievings` VALUES (null, '240.00', null, '1', null, '1', '12.00', null, null, '2017-06-27 11:52:49', '2017-06-27 11:52:49');
INSERT INTO `recievings` VALUES (null, '50.00', null, '1', null, '1', '12.00', null, null, '2017-06-27 11:56:40', '2017-06-27 11:56:40');
INSERT INTO `recievings` VALUES (null, '12.00', null, '1', null, '1', '12.00', null, null, '2017-06-27 12:34:39', '2017-06-27 12:34:39');
INSERT INTO `recievings` VALUES (null, '12.00', null, '1', null, '1', '12.00', null, null, '2017-06-27 12:36:58', '2017-06-27 12:36:58');
INSERT INTO `recievings` VALUES (null, '12.00', null, '1', null, '1', '12.00', null, null, '2017-06-27 12:37:14', '2017-06-27 12:37:14');
INSERT INTO `recievings` VALUES (null, '12.00', null, '1', null, '1', '12.00', null, null, '2017-06-27 12:39:00', '2017-06-27 12:39:00');
INSERT INTO `recievings` VALUES (null, '12.00', null, '1', null, '1', '12.00', null, null, '2017-06-27 12:46:21', '2017-06-27 12:46:21');
INSERT INTO `recievings` VALUES (null, '280.00', null, '1', null, '1', '5.00', null, null, '2017-06-27 12:50:54', '2017-06-27 12:50:54');
INSERT INTO `recievings` VALUES (null, '10.00', null, '1', null, '1', '1.00', null, null, '2017-06-27 12:59:48', '2017-06-27 12:59:48');
INSERT INTO `recievings` VALUES (null, '10.00', null, '1', null, '1', '2.00', null, null, '2017-06-27 13:00:14', '2017-06-27 13:00:14');
INSERT INTO `recievings` VALUES (null, '20.00', null, '1', null, '1', '1.00', null, null, '2017-06-27 13:01:26', '2017-06-27 13:01:26');
INSERT INTO `recievings` VALUES (null, '16.00', null, '1', null, '1', '1.00', null, null, '2017-06-27 13:08:46', '2017-06-27 13:08:46');
INSERT INTO `recievings` VALUES (null, '12.00', null, '1', null, '1', '12.00', null, null, '2017-06-27 13:11:17', '2017-06-27 13:11:17');
INSERT INTO `recievings` VALUES (null, '28.00', null, '1', null, '1', '12.00', null, null, '2017-06-27 13:13:14', '2017-06-27 13:13:14');
INSERT INTO `recievings` VALUES (null, '32.00', null, '1', null, '1', '21.00', null, null, '2017-06-27 13:16:24', '2017-06-27 13:16:24');
INSERT INTO `recievings` VALUES (null, '280.00', null, '1', null, '1', '5.00', null, null, '2017-06-27 13:18:09', '2017-06-27 13:18:09');
INSERT INTO `recievings` VALUES (null, '280.00', null, '1', null, '1', '5.00', null, null, '2017-06-27 13:18:29', '2017-06-27 13:18:29');
INSERT INTO `recievings` VALUES (null, '4.00', null, '1', null, '1', '2.00', null, null, '2017-06-27 13:21:42', '2017-06-27 13:21:42');
INSERT INTO `recievings` VALUES (null, '8.00', null, '1', null, '1', '1.00', null, null, '2017-06-27 13:21:19', '2017-06-27 13:21:19');
INSERT INTO `recievings` VALUES (null, '4.00', null, '1', null, '1', '2.00', null, null, '2017-06-27 13:22:11', '2017-06-27 13:22:11');
INSERT INTO `recievings` VALUES (null, '4.00', null, '1', null, '1', '2.00', null, null, '2017-06-27 13:22:37', '2017-06-27 13:22:37');
INSERT INTO `recievings` VALUES (null, '12.00', null, '1', null, '1', '2.00', null, null, '2017-06-27 13:22:20', '2017-06-27 13:22:20');
INSERT INTO `recievings` VALUES (null, '4.00', null, '1', null, '1', '2.00', null, null, '2017-06-27 13:23:17', '2017-06-27 13:23:17');
INSERT INTO `recievings` VALUES (null, '16.00', null, '1', null, '1', '2.00', null, null, '2017-06-27 13:22:36', '2017-06-27 13:22:36');
INSERT INTO `recievings` VALUES (null, '4.00', null, '1', null, '1', '2.00', null, null, '2017-06-27 13:25:46', '2017-06-27 13:25:46');
INSERT INTO `recievings` VALUES (null, '4.00', null, '2', null, '1', '2.00', null, null, '2017-06-27 17:27:53', '2017-06-27 17:27:53');
INSERT INTO `recievings` VALUES (null, '24.00', null, '1', null, '1', '13.00', null, null, '2017-06-27 17:37:37', '2017-06-27 17:37:37');
INSERT INTO `recievings` VALUES (null, '9.00', null, '1', null, '1', '3.00', null, null, '2017-06-27 17:38:18', '2017-06-27 17:38:18');
INSERT INTO `recievings` VALUES (null, '24.00', null, '2', null, '1', '2.00', null, null, '2017-06-27 17:38:42', '2017-06-27 17:38:42');
INSERT INTO `recievings` VALUES (null, '125.00', null, '1', null, '1', '5.00', null, null, '2017-06-27 17:42:00', '2017-06-27 17:42:00');
INSERT INTO `recievings` VALUES (null, '3350.00', null, '1', null, '1', '78.00', null, null, '2017-06-27 17:46:19', '2017-06-27 17:46:19');

-- ----------------------------
-- Table structure for recieving_products
-- ----------------------------
DROP TABLE IF EXISTS `recieving_products`;
CREATE TABLE `recieving_products` (
  `recieving_product_id` int(11) DEFAULT NULL,
  `recieving_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `produvt_name` varchar(255) DEFAULT NULL,
  `amount` decimal(11,4) DEFAULT NULL,
  `one_price` decimal(10,2) DEFAULT NULL,
  `whole_price` decimal(10,2) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `valid_date` timestamp NULL DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of recieving_products
-- ----------------------------
INSERT INTO `recieving_products` VALUES (null, '1', '17', null, '15.0000', '3.00', '45.00', '1', null, null, '2017-06-20 11:59:05', '2017-06-20 11:59:05');
INSERT INTO `recieving_products` VALUES (null, '1', '18', null, '60.0000', '6.00', '360.00', '1', null, null, '2017-06-20 11:59:06', '2017-06-20 11:59:06');
INSERT INTO `recieving_products` VALUES (null, '1', '17', null, '50.0000', '5.00', '250.00', '1', null, null, '2017-06-20 12:03:47', '2017-06-20 12:03:47');
INSERT INTO `recieving_products` VALUES (null, '1', '18', null, '60.0000', '6.00', '360.00', '1', null, null, '2017-06-20 12:03:47', '2017-06-20 12:03:47');
INSERT INTO `recieving_products` VALUES (null, '1', '18', null, '12.0000', '2.00', '24.00', '1', null, '1', '2017-06-20 14:03:28', '2017-06-20 14:03:28');
INSERT INTO `recieving_products` VALUES (null, '1', '21', null, '45.0000', '4.00', '180.00', '1', null, '1', '2017-06-20 14:03:28', '2017-06-20 14:03:28');
INSERT INTO `recieving_products` VALUES (null, '1', '21', null, '5.0000', '2.00', '10.00', '1', null, '1', '2017-06-20 14:26:56', '2017-06-20 14:26:56');
INSERT INTO `recieving_products` VALUES (null, '1', '21', null, '10.0000', '1.00', '10.00', '1', null, '1', '2017-06-20 14:27:19', '2017-06-20 14:27:19');
INSERT INTO `recieving_products` VALUES (null, '1', '21', null, '10.0000', '3.00', '30.00', '1', null, '1', '2017-06-20 14:32:32', '2017-06-20 14:32:32');
INSERT INTO `recieving_products` VALUES (null, '1', '21', null, '10.0000', '2.00', '20.00', '1', null, '1', '2017-06-20 14:33:28', '2017-06-20 14:33:28');
INSERT INTO `recieving_products` VALUES (null, '1', '21', null, '5.0000', '2.00', '10.00', '1', '2015-12-07 00:00:00', '1', '2017-06-20 14:39:11', '2017-06-20 14:39:11');
INSERT INTO `recieving_products` VALUES (null, '1', '21', null, '10.0000', '5.00', '50.00', '1', null, '1', '2017-06-27 11:51:58', '2017-06-27 11:51:58');
INSERT INTO `recieving_products` VALUES (null, '1', '21', null, '20.0000', '12.00', '240.00', '1', '2015-12-07 20:00:00', '1', '2017-06-27 11:52:49', '2017-06-27 11:52:49');
INSERT INTO `recieving_products` VALUES (null, '1', '17', null, '5.0000', '2.00', '10.00', '1', null, '1', '2017-06-27 12:59:48', '2017-06-27 12:59:48');
INSERT INTO `recieving_products` VALUES (null, '1', '17', null, '5.0000', '2.00', '10.00', '1', '2017-11-30 20:00:00', '1', '2017-06-27 13:00:14', '2017-06-27 13:00:14');
INSERT INTO `recieving_products` VALUES (null, '1', '10', null, '5.0000', '4.00', '20.00', '1', '2017-11-30 20:00:00', '1', '2017-06-27 13:01:26', '2017-06-27 13:01:26');
INSERT INTO `recieving_products` VALUES (null, '1', '10', null, '4.0000', '4.00', '16.00', '1', null, '1', '2017-06-27 13:08:46', '2017-06-27 13:08:46');
INSERT INTO `recieving_products` VALUES (null, '1', '10', null, '3.0000', '4.00', '12.00', '1', '2017-11-30 20:00:00', '1', '2017-06-27 13:11:17', '2017-06-27 13:11:17');
INSERT INTO `recieving_products` VALUES (null, '1', '10', null, '7.0000', '4.00', '28.00', '1', '2017-11-30 20:00:00', '1', '2017-06-27 13:13:14', '2017-06-27 13:13:14');
INSERT INTO `recieving_products` VALUES (null, '1', '10', null, '8.0000', '4.00', '32.00', '1', '2017-11-30 20:00:00', '1', '2017-06-27 13:16:24', '2017-06-27 13:16:24');
INSERT INTO `recieving_products` VALUES (null, '1', '10', null, '2.0000', '4.00', '8.00', '1', null, '1', '2017-06-27 13:21:19', '2017-06-27 13:21:19');
INSERT INTO `recieving_products` VALUES (null, '1', '10', null, '3.0000', '4.00', '12.00', '1', '2017-11-30 20:00:00', '1', '2017-06-27 13:22:20', '2017-06-27 13:22:20');
INSERT INTO `recieving_products` VALUES (null, '1', '10', null, '4.0000', '4.00', '16.00', '1', '2017-11-30 20:00:00', '1', '2017-06-27 13:22:36', '2017-06-27 13:22:36');
INSERT INTO `recieving_products` VALUES (null, '1', '21', null, '2.0000', '2.00', '4.00', '1', '2017-05-31 20:00:00', '1', '2017-06-27 13:25:46', '2017-06-27 13:25:46');
INSERT INTO `recieving_products` VALUES (null, '1', '21', null, '2.0000', '2.00', '4.00', '1', '2015-06-01 00:00:00', '2', '2017-06-27 17:27:53', '2017-06-27 17:27:53');
INSERT INTO `recieving_products` VALUES (null, '1', '21', null, '12.0000', '2.00', '24.00', '1', '2017-06-01 00:00:00', '1', '2017-06-27 17:37:37', '2017-06-27 17:37:37');
INSERT INTO `recieving_products` VALUES (null, '1', '21', null, '3.0000', '3.00', '9.00', '1', '2017-06-01 00:00:00', '1', '2017-06-27 17:38:18', '2017-06-27 17:38:18');
INSERT INTO `recieving_products` VALUES (null, '1', '21', null, '12.0000', '2.00', '24.00', '1', '2017-06-01 00:00:00', '2', '2017-06-27 17:38:42', '2017-06-27 17:38:42');
INSERT INTO `recieving_products` VALUES (null, '1', '21', null, '25.0000', '5.00', '125.00', '1', null, '1', '2017-06-27 17:42:00', '2017-06-27 17:42:00');
INSERT INTO `recieving_products` VALUES (null, '1', '21', null, '50.0000', '67.00', '3350.00', '1', null, '1', '2017-06-27 17:46:19', '2017-06-27 17:46:19');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'Test 1', 'Test 1', 'Test 1', '2017-06-05 09:56:28', '2017-06-05 09:56:28');
INSERT INTO `roles` VALUES ('2', 'Test 2', 'Test 2', 'Test 2', '2017-06-05 09:56:39', '2017-06-05 09:56:39');
INSERT INTO `roles` VALUES ('3', 'Test 3', 'Test 3', 'Test 3', '2017-06-05 09:56:57', '2017-06-05 09:56:57');

-- ----------------------------
-- Table structure for role_user
-- ----------------------------
DROP TABLE IF EXISTS `role_user`;
CREATE TABLE `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of role_user
-- ----------------------------
INSERT INTO `role_user` VALUES ('1', '1');
INSERT INTO `role_user` VALUES ('1', '2');
INSERT INTO `role_user` VALUES ('1', '3');
INSERT INTO `role_user` VALUES ('2', '2');
INSERT INTO `role_user` VALUES ('3', '3');
INSERT INTO `role_user` VALUES ('4', '3');

-- ----------------------------
-- Table structure for sell
-- ----------------------------
DROP TABLE IF EXISTS `sell`;
CREATE TABLE `sell` (
  `sell_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `whole_price` decimal(10,2) DEFAULT NULL,
  `bonus` decimal(10,2) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `shipping_price` decimal(10,2) DEFAULT NULL,
  `transaction_id` varchar(32) DEFAULT NULL,
  `state` int(2) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`sell_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sell
-- ----------------------------

-- ----------------------------
-- Table structure for sell_items
-- ----------------------------
DROP TABLE IF EXISTS `sell_items`;
CREATE TABLE `sell_items` (
  `sell_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `sell_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_price` decimal(10,2) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `whole_price` decimal(10,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` int(2) DEFAULT '1',
  PRIMARY KEY (`sell_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sell_items
-- ----------------------------
INSERT INTO `sell_items` VALUES ('1', '1', '17', '2688.00', '1.00', '2688.00', '2018-01-10 17:01:06', '2018-01-10 17:01:06', '1');
INSERT INTO `sell_items` VALUES ('2', '1', '17', '2688.00', '1.00', '2688.00', '2018-01-10 17:01:19', '2018-01-10 17:01:19', '1');

-- ----------------------------
-- Table structure for shoppingcart
-- ----------------------------
DROP TABLE IF EXISTS `shoppingcart`;
CREATE TABLE `shoppingcart` (
  `identifier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instance` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`identifier`,`instance`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of shoppingcart
-- ----------------------------
INSERT INTO `shoppingcart` VALUES ('admin@gmail.com', 'default', 'O:29:\"Illuminate\\Support\\Collection\":1:{s:8:\"\0*\0items\";a:1:{s:32:\"9ee3b12717cfe42641bc142d8d0e059e\";O:32:\"Gloudemans\\Shoppingcart\\CartItem\":8:{s:5:\"rowId\";s:32:\"9ee3b12717cfe42641bc142d8d0e059e\";s:2:\"id\";s:2:\"17\";s:3:\"qty\";i:2;s:4:\"name\";s:24:\"SAMSUNG RB37K63412C/WT/O\";s:5:\"price\";d:2688;s:7:\"options\";O:39:\"Gloudemans\\Shoppingcart\\CartItemOptions\":1:{s:8:\"\0*\0items\";a:1:{s:3:\"img\";s:106:\"http://localhost:8082/onlinemarket/public_html/files/product_files/17/7239b1ac7d9ea60f3d73dafaa1d449a3.jpg\";}}s:49:\"\0Gloudemans\\Shoppingcart\\CartItem\0associatedModel\";N;s:41:\"\0Gloudemans\\Shoppingcart\\CartItem\0taxRate\";i:21;}}}', null, null);
INSERT INTO `shoppingcart` VALUES ('bbb@gmail.com', 'default', 'O:29:\"Illuminate\\Support\\Collection\":1:{s:8:\"\0*\0items\";a:0:{}}', null, null);
INSERT INTO `shoppingcart` VALUES ('test@gmail.com', 'default', 'O:29:\"Illuminate\\Support\\Collection\":1:{s:8:\"\0*\0items\";a:2:{s:32:\"9ee3b12717cfe42641bc142d8d0e059e\";O:32:\"Gloudemans\\Shoppingcart\\CartItem\":8:{s:5:\"rowId\";s:32:\"9ee3b12717cfe42641bc142d8d0e059e\";s:2:\"id\";s:2:\"17\";s:3:\"qty\";i:2;s:4:\"name\";s:24:\"SAMSUNG RB37K63412C/WT/O\";s:5:\"price\";d:2688;s:7:\"options\";O:39:\"Gloudemans\\Shoppingcart\\CartItemOptions\":1:{s:8:\"\0*\0items\";a:1:{s:3:\"img\";s:106:\"http://localhost:8082/onlinemarket/public_html/files/product_files/17/7239b1ac7d9ea60f3d73dafaa1d449a3.jpg\";}}s:49:\"\0Gloudemans\\Shoppingcart\\CartItem\0associatedModel\";N;s:41:\"\0Gloudemans\\Shoppingcart\\CartItem\0taxRate\";i:21;}s:32:\"f03d94cd97a73c3b81dbb3f36c8b1f5f\";O:32:\"Gloudemans\\Shoppingcart\\CartItem\":8:{s:5:\"rowId\";s:32:\"f03d94cd97a73c3b81dbb3f36c8b1f5f\";s:2:\"id\";s:2:\"16\";s:3:\"qty\";s:1:\"1\";s:4:\"name\";s:16:\"HITACHI 65HZ6W69\";s:5:\"price\";d:3475;s:7:\"options\";O:39:\"Gloudemans\\Shoppingcart\\CartItemOptions\":1:{s:8:\"\0*\0items\";a:1:{s:3:\"img\";s:77:\"http://localhost:8082/onlinemarket/public_html/assets/images/demo/noimage.jpg\";}}s:49:\"\0Gloudemans\\Shoppingcart\\CartItem\0associatedModel\";N;s:41:\"\0Gloudemans\\Shoppingcart\\CartItem\0taxRate\";i:21;}}}', null, null);

-- ----------------------------
-- Table structure for storage
-- ----------------------------
DROP TABLE IF EXISTS `storage`;
CREATE TABLE `storage` (
  `storage_id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `amount` decimal(10,4) DEFAULT NULL,
  `place` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `valid_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`storage_id`,`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of storage
-- ----------------------------
INSERT INTO `storage` VALUES ('37', '1', '21', '40.0000', null, '2017-06-01 00:00:00', '2017-06-27 17:37:37', '2017-06-27 17:42:00');
INSERT INTO `storage` VALUES ('38', '2', '21', '12.0000', null, '2017-06-01 00:00:00', '2017-06-27 17:38:42', '2017-06-27 17:38:42');
INSERT INTO `storage` VALUES ('39', '1', '21', '50.0000', null, null, '2017-06-27 17:46:18', '2017-06-27 17:46:18');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `birth_date` timestamp NULL DEFAULT NULL,
  `mobile_no` varchar(255) DEFAULT NULL,
  `personal_no` varchar(255) DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(555) DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `state_id` tinyint(1) NOT NULL DEFAULT '1',
  `bonus` decimal(10,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'კახა', 'თაბაგარი', '0000-00-00 00:00:00', '599 99 99 99', '123123123', 'admin@gmail.com', '$2y$10$b9Rs1OvVD/BThlNHmSGaauHgeTzWZsvj9IQJe.GH8hKudziWP0IRG', null, 'AkQqsy0d13L07WJwnOk2pA0xoq9CpRbtHqZFIIMLnefqoBFKkT5xmZQ5p9oi', '2017-05-04 11:45:54', '2017-05-04 11:46:15', '1', null);
INSERT INTO `users` VALUES ('2', 'Admin1', '', '0000-00-00 00:00:00', null, null, 'admin1@gmail.com', '$2y$10$b9Rs1OvVD/BThlNHmSGaauHgeTzWZsvj9IQJe.GH8hKudziWP0IRG', null, '', '2017-05-04 11:58:39', '0000-00-00 00:00:00', '1', null);
INSERT INTO `users` VALUES ('3', 'KAKHA TABAGARI', '', '0000-00-00 00:00:00', null, null, 'tabagari89@gmail.com', '$2y$10$XVjVdez9ldUSXxTKNzinduYqNbtnX576wDfJ8B.fJp.ZRHVaOAZJq', '4 LEWIS CIR, G11403', 'HzWPeaGi1KyliNY1IU3qSDOmaXjNZLXYkiM9wK3AH2kb9TWsMQBG0uTqIKmR', '2017-06-27 13:32:28', '2017-06-27 13:32:28', '1', null);
INSERT INTO `users` VALUES ('4', 'kakha', 'TABAGARI', null, null, null, 'asd@gmail.com', '$2y$10$b9Rs1OvVD/BThlNHmSGaauHgeTzWZsvj9IQJe.GH8hKudziWP0IRG', '12 th m/r', null, '2017-11-30 16:43:16', '2017-11-30 16:43:16', '1', null);
INSERT INTO `users` VALUES ('5', 'test', 'test', null, null, null, 'test@gmail.com', '$2y$10$b9Rs1OvVD/BThlNHmSGaauHgeTzWZsvj9IQJe.GH8hKudziWP0IRG', 'asdsa', 'GTs22bmlupXPgVL8hrIGg18hyTREPeoQxtJmJn4OQhiHM4i7amzVSQehT4i6', '2017-12-14 12:16:48', '2017-12-14 12:16:48', '1', null);
INSERT INTO `users` VALUES ('6', 'ააა', 'ააა', null, null, null, 'aaa@gmail.com', '$2y$10$EOodT9x0.O7q55aTu2/SFO2pc12zbwu4EHKjkTnXoHopuJB8jSe2a', null, null, '2017-12-14 12:34:06', '2017-12-14 12:34:06', '1', '0.00');
INSERT INTO `users` VALUES ('11', 'bbbb', 'bbbb', null, null, null, 'bbb@gmail.com', '$2y$10$WuYdboG9eGxllXMnWyOQ0.6nMJQS0eZRqPOJpdUQ5WDDb0QxISY3a', 'qucha, 2,3', null, '2017-12-25 17:11:55', '2017-12-25 17:11:55', '1', '0.00');

-- ----------------------------
-- Table structure for user_addresses
-- ----------------------------
DROP TABLE IF EXISTS `user_addresses`;
CREATE TABLE `user_addresses` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `primary` int(1) DEFAULT '0',
  `state` int(2) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`address_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_addresses
-- ----------------------------
INSERT INTO `user_addresses` VALUES ('1', '1', '2', 'qucha, 2,3', '1', '1', '2017-12-25 17:11:55', '2017-12-25 17:11:55');

-- ----------------------------
-- Table structure for user_tree
-- ----------------------------
DROP TABLE IF EXISTS `user_tree`;
CREATE TABLE `user_tree` (
  `user_tree_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `parent_path` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `user_parent_path` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `childrens` int(2) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_tree_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_tree
-- ----------------------------
INSERT INTO `user_tree` VALUES ('6', '1', null, '/6', '/1', '2', null, null);
INSERT INTO `user_tree` VALUES ('9', '2', '6', '/6/9', '/1/2', '2', '2018-02-22 12:32:10', '2018-02-22 13:08:30');
INSERT INTO `user_tree` VALUES ('10', '11', '6', '/6/10', '/1/11', '2', '2018-02-22 12:35:26', '2018-02-22 13:08:40');
INSERT INTO `user_tree` VALUES ('14', '5', '9', '/6/9/14', '/1/2/5', '1', '2018-02-22 13:08:18', '2018-02-22 13:08:49');
INSERT INTO `user_tree` VALUES ('15', '5', '9', '/6/9/15', '/1/2/5', '0', '2018-02-22 13:08:30', '2018-02-22 13:08:30');
INSERT INTO `user_tree` VALUES ('16', '5', '10', '/6/10/16', '/1/11/5', '0', '2018-02-22 13:08:35', '2018-02-22 13:08:35');
INSERT INTO `user_tree` VALUES ('17', '5', '10', '/6/10/17', '/1/11/5', '0', '2018-02-22 13:08:40', '2018-02-22 13:08:40');
INSERT INTO `user_tree` VALUES ('18', '5', '14', '/6/9/14/18', '/1/2/5/5', '0', '2018-02-22 13:08:49', '2018-02-22 13:08:49');

-- ----------------------------
-- Table structure for web_categories
-- ----------------------------
DROP TABLE IF EXISTS `web_categories`;
CREATE TABLE `web_categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name_ge` varchar(255) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `name_ru` varchar(255) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '1',
  `disabled` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `parent_id` int(11) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  `type` int(2) DEFAULT NULL,
  `img_path` varchar(500) DEFAULT NULL,
  `color` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_categories
-- ----------------------------
INSERT INTO `web_categories` VALUES ('1', 'კომპანიის შესახებ', 'კომპანიის შესახებ', 'კომპანიის შესახებ', '1', '0', '0', '2016-11-14 08:48:04', '2017-05-29 12:59:25', null, '/category/1', null, 'http://localhost:8082/university/public_html/files/NewFolder/ENUEpl6Sj7g.jpg', '#5b0b0b');
INSERT INTO `web_categories` VALUES ('2', 'პროდუქტების შესახებ', 'პროდუქტების შესახებ', 'პროდუქტების შესახებ', '1', '0', '0', '2016-11-14 08:48:22', '2017-05-29 12:59:41', null, '/category/2', null, 'http://localhost:8082/university/public_html/files/NewFolder/ENUEpl6Sj7g.jpg', '#081765');
INSERT INTO `web_categories` VALUES ('3', 'ფარმაციის ფაკულტეტი', 'ფარმაციის ფაკულტეტი', 'ფარმაციის ფაკულტეტი', '0', '0', '0', '2016-11-14 08:48:47', '2017-05-29 13:00:14', null, '/category/3', null, 'http://localhost:8082/university/public_html/files/NewFolder/ENUEpl6Sj7g.jpg', '#00FF00');
INSERT INTO `web_categories` VALUES ('4', 'საზოგადოებრივი ჯანდაცვის ფაკულტეტი', 'საზოგადოებრივი ჯანდაცვის ფაკულტეტი', 'საზოგადოებრივი ჯანდაცვის ფაკულტეტი', '0', '0', '0', '2016-11-22 15:11:36', '2017-05-29 13:00:17', null, '/category/4', null, 'http://localhost:8082/university/public_html/files/NewFolder/ENUEpl6Sj7g.jpg', '#808080');

-- ----------------------------
-- Table structure for web_menu
-- ----------------------------
DROP TABLE IF EXISTS `web_menu`;
CREATE TABLE `web_menu` (
  `menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name_ge` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_ru` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  `editable` tinyint(1) NOT NULL DEFAULT '1',
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `order` int(11) DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`menu_id`),
  KEY `menu_state_index` (`state`),
  KEY `menu_disabled_index` (`disabled`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of web_menu
-- ----------------------------
INSERT INTO `web_menu` VALUES ('72', 'მთავარი', 'მთავარი', 'მთავარი', '/', null, '0', '0', '1', null, '2017-05-18 09:38:18', '2017-05-18 09:39:22', '1', null);
INSERT INTO `web_menu` VALUES ('73', 'ჩვენს შესახებ', 'ჩვენს შესახებ', 'ჩვენს შესახებ', '/page/18', null, '1', '0', '1', null, '2017-05-18 09:38:35', '2017-06-08 12:12:54', '3', null);
INSERT INTO `web_menu` VALUES ('74', 'კონტაქტი', 'კონტაქტი', 'კონტაქტი', '/contact', null, '1', '0', '1', null, '2017-05-18 09:38:51', '2017-06-08 12:12:54', '4', null);
INSERT INTO `web_menu` VALUES ('75', 'სიახლეები', 'news', 'news', '/news', null, '1', '0', '1', null, '2017-05-25 10:07:49', '2017-05-25 10:07:56', '2', null);

-- ----------------------------
-- Table structure for web_params
-- ----------------------------
DROP TABLE IF EXISTS `web_params`;
CREATE TABLE `web_params` (
  `param_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `value` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`param_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of web_params
-- ----------------------------
INSERT INTO `web_params` VALUES ('1', 'address_ge', 'მისამართი: თბილისი თამარაშვილის შესახვევი 4 (გ. სვანიძის 8)', '2016-11-13 23:07:33', '0000-00-00 00:00:00');
INSERT INTO `web_params` VALUES ('2', 'address_en', 'Adress: Tamarashvili str. 4 (G. Svanidze 8), Tbilisi ', '2016-11-13 23:07:33', '0000-00-00 00:00:00');
INSERT INTO `web_params` VALUES ('3', 'address_ru', 'Adress: Tamarashvili str. 4 (G. Svanidze 8), Tbilisi ', '2016-11-13 23:07:33', '0000-00-00 00:00:00');
INSERT INTO `web_params` VALUES ('4', 'mobile_main', '+(995)322 29 34 92 ', '2016-11-13 23:08:41', '0000-00-00 00:00:00');
INSERT INTO `web_params` VALUES ('5', 'mobile', '+(995)322 29 34 92 ', '2016-11-13 23:08:41', '0000-00-00 00:00:00');
INSERT INTO `web_params` VALUES ('6', 'workTime', '09:00 - 18:01', '2016-11-13 23:09:30', '2017-05-17 13:09:25');
INSERT INTO `web_params` VALUES ('7', 'email', 'info@gdc.ge', '2016-11-14 11:42:25', '2017-05-18 11:36:15');

-- ----------------------------
-- Table structure for web_postmeta
-- ----------------------------
DROP TABLE IF EXISTS `web_postmeta`;
CREATE TABLE `web_postmeta` (
  `meta_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `meta_key` varchar(45) NOT NULL,
  `meta_value` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`meta_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_postmeta
-- ----------------------------
INSERT INTO `web_postmeta` VALUES ('1', '4', 'showInGallery', '1', '2016-12-05 13:41:20', '2016-12-05 13:41:20');
INSERT INTO `web_postmeta` VALUES ('2', '10', 'showInGallery', '1', '2016-12-05 16:10:26', '2016-12-05 16:10:26');

-- ----------------------------
-- Table structure for web_posts
-- ----------------------------
DROP TABLE IF EXISTS `web_posts`;
CREATE TABLE `web_posts` (
  `post_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_ge` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_ru` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description_ge` longtext COLLATE utf8_unicode_ci,
  `description_en` longtext COLLATE utf8_unicode_ci,
  `description_ru` longtext COLLATE utf8_unicode_ci,
  `img_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `published_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `keyword_ge` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_description_ge` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_description_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_description_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `description_ge_short` text CHARACTER SET utf8,
  `description_en_short` text CHARACTER SET utf8,
  `description_ru_short` text CHARACTER SET utf8,
  `link_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `type` int(2) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`post_id`),
  KEY `pages_state_index` (`state`),
  KEY `pages_disabled_index` (`disabled`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of web_posts
-- ----------------------------
INSERT INTO `web_posts` VALUES ('1', 'welcome', 'welcome', 'welcome', '&lt;p&gt;ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.&lt;/p&gt;', 'ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.', 'ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.', '', '1', '0', '0', '0000-00-00 00:00:00', '2016-11-15 12:40:09', '2016-11-15 18:39:53', '/page/1', 'Business Consultation', 'Business Consultation', 'Business Consultation', 'Business Consultation', 'Business Consultation', 'Business Consultation', '0', null, null, '', null, '2', null);
INSERT INTO `web_posts` VALUES ('2', 'about', 'about', 'about', 'ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.', 'ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.', 'ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.', '', '0', '0', '0', '0000-00-00 00:00:00', '2016-11-15 18:36:57', '2017-05-29 11:29:13', '/page/2?category=1', '', '', '', '', '', '', '0', '', '', '', null, '2', null);
INSERT INTO `web_posts` VALUES ('3', 'about', 'about', 'about', '&lt;p&gt;ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.&lt;/p&gt;\n&lt;p&gt;&lt;a rel=&quot;prettyPhoto[pp_gal]&quot; href=&quot;../../../../files/NewFolder/ENUEpl6Sj7g.jpg&quot;&gt;&lt;img src=&quot;../../../../files/NewFolder/ENUEpl6Sj7g.jpg&quot; alt=&quot;&quot; width=&quot;303&quot; height=&quot;227&quot;&gt;&lt;/a&gt;&lt;/p&gt;', 'ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.', 'ცნობილი ფაქტია, რომ გვერდის წაკითხვად შიგთავსს შეუძლია მკითხველის ყურადღება მიიზიდოს და დიზაინის აღქმაში ხელი შეუშალოს. Lorem Ipsum-ის გამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე ერთიდაიგივე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი წაითხვა კი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა და ვებგვერდი იყენებს Lorem Ipsum-ს, როგორც დროებით ტექსტს წყობის შესავსებად; Lorem Ipsum-ის მოძებნისას კი საძიებო სისტემები ბევრ დაუსრულებელ გვერდს გვიჩვენებენ. წლების მანძილზე ამ ტექსტის უამრავი ვერსია გამოჩნდა, ზოგი შემთხვევით დაშვებული შეცდომის გამო, ზოგი კი — განზრახ, ხუმრობით.', '', '0', '0', '0', '0000-00-00 00:00:00', '2016-11-24 09:09:09', '2017-05-25 09:37:50', '/page/3?category=1', '', '', '', '', '', '', '0', '', '', '', null, '2', null);
INSERT INTO `web_posts` VALUES ('4', 'test', '', '', '&lt;p&gt;asdas sa ds dsa dsa&lt;/p&gt;', '', '', '', '0', '0', '0', '2016-11-24 19:00:00', '2016-11-24 15:00:50', '2016-11-24 15:18:39', null, 'asd', '', '', 'asdsad sad sa dsa', '', '', '0', '', '', '', null, '1', null);
INSERT INTO `web_posts` VALUES ('5', 'test', '', '', '&lt;p&gt;asdas sa ds dsa dsa&lt;/p&gt;', '', '', '', '0', '0', '0', '2016-11-24 19:00:00', '2016-11-24 15:03:43', '2016-11-24 15:18:43', null, 'asd', '', '', 'asdsad sad sa dsa', '', '', '0', '', '', '', null, '1', null);
INSERT INTO `web_posts` VALUES ('6', 'test', '', '', '&lt;p&gt;dsa dsa dsa dsa dsa sa dsa dsa&lt;/p&gt;', '', '', '', '0', '0', '0', '2016-11-24 19:07:00', '2016-11-24 15:17:40', '2016-11-24 15:18:41', '/news/6', 'ads', '', '', 'sa dsa dsa', '', '', '0', '', '', '', null, '1', null);
INSERT INTO `web_posts` VALUES ('7', 'გერმანელი არქიტექტორის, მარკუს პენელის, დისკუსია: NO OLD NO CITY!', 'გერმანელი არქიტექტორის, მარკუს პენელის, დისკუსია: NO OLD NO CITY!', '', '&lt;p&gt;22 ნოემბერს, 19:00 საათზე, ილიას სახელმწიფო უნივერსიტეტის E405&amp;nbsp;(&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;„&lt;/strong&gt;&lt;strong&gt;ფორბსის&lt;/strong&gt;&lt;/a&gt;“)&amp;nbsp;აუდიტორიაში, ილიას სახელმწიფო უნივერსიტეტისა და&amp;nbsp;&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;ForbesGeorgia&lt;/strong&gt;&lt;/a&gt;&lt;strong&gt;-&lt;/strong&gt;ს ურთიერთთანამშრომლობის ფარგლებში, კომუნიკაციების და ბრენდინგის სპეციალისტი, მედიაპროდიუსერი, ზაქარია ზალიკაშვილი, წაიკითხავს საჯარო ლექციას.&amp;nbsp;&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;მომხსენებლის&lt;/strong&gt;&lt;strong&gt; შესახებ&lt;/strong&gt;&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;თბილისის ბიზნესის ინსტიტუტის დასრულების შემდეგ ზაქარია ზალიკაშვილი აშშ-ში გაემგზავრა და დაამთავრა სამხრეთ კალიფორნიის (აშშ) უნივერსიტეტის კინო- და ტელეხელოვნების ფაკულტეტი, სპეციალობით - კინორეჟისურა. პროფესიული გამოცდილება მიიღო საქართველოს სხვადასხვა კომპანიის მარკეტინგის განყოფილებაში წამყვანი სპეციალისტის და დეპარტამენტის ხელმძღვანელის პოზიციებზე. 2000 წლამდე მუშაობდა GORBI-ის სოციოლოგიური და მარკეტინგული კვლევების პროექტებზე (თამბაქოს, გამამხნევებელი სასმელების, ტელეკომუნიკაციის და სხვა ტრანსნაციონალურ კომპანიებთან. მსოფლიო ბანკთან); ამავე პერიოდში, ჯონ ჰოპკინსის უნივერსიტეტთან და სხვა საერთაშორისო ფონდებთან ერთად, აწარმოებდა სხვადასხვა ტიპის სოციალური კამპანიებს.&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;ზაქარია ზალიკაშვილი სხვადასხვა დროს ხელმძღვანელობდა სარეკლამო სააგენტოს „აუდიენცია“, რადიოს „მწვანე ტალღა“ და კომპანია „ჯეოსელის“ მარკეტინგულ კომუნიკაციებს. მან 2005 წელს დააფუძნა მარკეტინგული კონსულტაციების კომპანია „სტრომბოლი“, ხოლო 2006 წელს - კომპანია „კომუნიკატორი“. 2014 წლიდან ხელმძღვანელობს საზოგადოებასთან ურთიერთობების დეპარტამენტს ქართულ ამერიკულ ინდუსტრიულ ჯგუფში “ჯორჯიან ემერიქან ელოიზ”.&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;დრო:&lt;/strong&gt; 22 ნოემბერი, 19:00 საათი&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;ადგილმდებარეობა:&lt;/strong&gt; E405&amp;nbsp;(&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;„&lt;/strong&gt;&lt;strong&gt;ფორბსის&lt;/strong&gt;&lt;strong&gt;“&lt;/strong&gt;&lt;/a&gt;)&amp;nbsp;აუდიტორია (ჩოლოყაშვილის გამზ. #3/5), ილიას სახელმწიფო უნივერსიტეტი&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;დასწრება თავისუფალია.&lt;/p&gt;', '&lt;p&gt;22 ნოემბერს, 19:00 საათზე, ილიას სახელმწიფო უნივერსიტეტის E405&amp;nbsp;(&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;„&lt;/strong&gt;&lt;strong&gt;ფორბსის&lt;/strong&gt;&lt;/a&gt;“)&amp;nbsp;აუდიტორიაში, ილიას სახელმწიფო უნივერსიტეტისა და&amp;nbsp;&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;ForbesGeorgia&lt;/strong&gt;&lt;/a&gt;&lt;strong&gt;-&lt;/strong&gt;ს ურთიერთთანამშრომლობის ფარგლებში, კომუნიკაციების და ბრენდინგის სპეციალისტი, მედიაპროდიუსერი, ზაქარია ზალიკაშვილი, წაიკითხავს საჯარო ლექციას.&amp;nbsp;&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;მომხსენებლის&lt;/strong&gt;&lt;strong&gt; შესახებ&lt;/strong&gt;&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;თბილისის ბიზნესის ინსტიტუტის დასრულების შემდეგ ზაქარია ზალიკაშვილი აშშ-ში გაემგზავრა და დაამთავრა სამხრეთ კალიფორნიის (აშშ) უნივერსიტეტის კინო- და ტელეხელოვნების ფაკულტეტი, სპეციალობით - კინორეჟისურა. პროფესიული გამოცდილება მიიღო საქართველოს სხვადასხვა კომპანიის მარკეტინგის განყოფილებაში წამყვანი სპეციალისტის და დეპარტამენტის ხელმძღვანელის პოზიციებზე. 2000 წლამდე მუშაობდა GORBI-ის სოციოლოგიური და მარკეტინგული კვლევების პროექტებზე (თამბაქოს, გამამხნევებელი სასმელების, ტელეკომუნიკაციის და სხვა ტრანსნაციონალურ კომპანიებთან. მსოფლიო ბანკთან); ამავე პერიოდში, ჯონ ჰოპკინსის უნივერსიტეტთან და სხვა საერთაშორისო ფონდებთან ერთად, აწარმოებდა სხვადასხვა ტიპის სოციალური კამპანიებს.&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;ზაქარია ზალიკაშვილი სხვადასხვა დროს ხელმძღვანელობდა სარეკლამო სააგენტოს „აუდიენცია“, რადიოს „მწვანე ტალღა“ და კომპანია „ჯეოსელის“ მარკეტინგულ კომუნიკაციებს. მან 2005 წელს დააფუძნა მარკეტინგული კონსულტაციების კომპანია „სტრომბოლი“, ხოლო 2006 წელს - კომპანია „კომუნიკატორი“. 2014 წლიდან ხელმძღვანელობს საზოგადოებასთან ურთიერთობების დეპარტამენტს ქართულ ამერიკულ ინდუსტრიულ ჯგუფში “ჯორჯიან ემერიქან ელოიზ”.&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;დრო:&lt;/strong&gt; 22 ნოემბერი, 19:00 საათი&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;ადგილმდებარეობა:&lt;/strong&gt; E405&amp;nbsp;(&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;„&lt;/strong&gt;&lt;strong&gt;ფორბსის&lt;/strong&gt;&lt;strong&gt;“&lt;/strong&gt;&lt;/a&gt;)&amp;nbsp;აუდიტორია (ჩოლოყაშვილის გამზ. #3/5), ილიას სახელმწიფო უნივერსიტეტი&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;დასწრება თავისუფალია.&lt;/p&gt;', '', 'http://localhost:8082/onlinemarket/public/files/17/b49ad8bdcc52a1daa4d259042637139b_original.jpeg', '1', '0', '0', '2016-11-24 15:20:00', '2016-11-24 15:18:50', '2017-05-29 13:37:08', '/news/7', 'გერმანელი არქიტექტორის, მარკუს პენელის, დისკუსია: NO OLD NO CITY!', 'გერმანელი არქიტექტორის, მარკუს პენელის, დისკუსია: NO OLD NO CITY!', '', 'გერმანელი არქიტექტორის, მარკუს პენელის, დისკუსია: NO OLD NO CITY!', 'გერმანელი არქიტექტორის, მარკუს პენელის, დისკუსია: NO OLD NO CITY!', '', '0', '', '', '', null, '1', null);
INSERT INTO `web_posts` VALUES ('8', 'ჩემპიონატი ფუტსალში - ილიაუნი VS ღია სასწავლო უნივერსიტეტი', 'ჩემპიონატი ფუტსალში - ილიაუნი VS ღია სასწავლო უნივერსიტეტი', '', '&lt;p&gt;24 ნოემბერს, 19:00 საათზე, ილიაუნის ფუტსალის ნაკრები, საუნივერსიტეტო სპორტის ფედერაციის თასის გათამაშების ჯგუფური ეტაპის მესამე მატჩს ღია სასწავლო უნივერსიტეტის გუნდთან გამართავს. შეხვედრა ჩატარდება სპორტკომპლექსში &quot;არენა-1&quot; (უნივერსიტეტის ქ. N2).&lt;/p&gt;\n&lt;p&gt;&lt;strong&gt;დრო:&amp;nbsp;&lt;/strong&gt;24 ნოემბერი,19:00 საათი&lt;/p&gt;\n&lt;p&gt;&lt;strong&gt;ადგილმდებარეობა:&amp;nbsp;&lt;/strong&gt;სპორტკომპლექსში &quot;არენა-1&quot; (უნივერსიტეტის ქ. N2).&lt;/p&gt;', '&lt;p&gt;24 ნოემბერს, 19:00 საათზე, ილიაუნის ფუტსალის ნაკრები, საუნივერსიტეტო სპორტის ფედერაციის თასის გათამაშების ჯგუფური ეტაპის მესამე მატჩს ღია სასწავლო უნივერსიტეტის გუნდთან გამართავს. შეხვედრა ჩატარდება სპორტკომპლექსში &quot;არენა-1&quot; (უნივერსიტეტის ქ. N2).&lt;/p&gt;\n&lt;p&gt;&lt;strong&gt;დრო:&amp;nbsp;&lt;/strong&gt;24 ნოემბერი,19:00 საათი&lt;/p&gt;\n&lt;p&gt;&lt;strong&gt;ადგილმდებარეობა:&amp;nbsp;&lt;/strong&gt;სპორტკომპლექსში &quot;არენა-1&quot; (უნივერსიტეტის ქ. N2).&lt;/p&gt;', '', 'http://localhost:8082/onlinemarket/public/files/foo.jpg', '1', '0', '0', '2016-11-24 15:18:00', '2016-11-24 15:19:51', '2017-05-29 13:37:30', '/news/8', 'ჩემპიონატი ფუტსალში - ილიაუნი VS ღია სასწავლო უნივერსიტეტი', 'ჩემპიონატი ფუტსალში - ილიაუნი VS ღია სასწავლო უნივერსიტეტი', '', 'ჩემპიონატი ფუტსალში - ილიაუნი VS ღია სასწავლო უნივერსიტეტი', 'ჩემპიონატი ფუტსალში - ილიაუნი VS ღია სასწავლო უნივერსიტეტი', '', '0', '', '', '', null, '1', null);
INSERT INTO `web_posts` VALUES ('9', 'ილიაუნელმა კალათბურთელებმა სტუდენტური ლიგის მეორე შეხვედრაც მოიგეს', 'ილიაუნელმა კალათბურთელებმა სტუდენტური ლიგის მეორე შეხვედრაც მოიგეს', '', '&lt;p&gt;ვერის ბაღის საკალათბურთო კომპლექსში გრძელდება საქართველოს სტუდენტური ჩემპიონატი კალთბურთში. ჯგუფური ეტაპის მეორე ტურში მორიგ წარმატებას მიაღწია ილიაუნის გუნდმა, რომელმაც შავი ზღვის საერთაშორისო უნივერსიტეტის კალათბურთელები დიდი ანგარიშით - 106:54 დაამარცხა. ილიაუნის გუნდს ყველაზე მეტი, 32 ქულა ირაკლი ჯანხოთელმა მოუტანა. მე-3 ტურის მატჩს ილიაუნელები 2 დეკემბერს, 16:00 საათზე, ბათუმის საზღვაო აკადემიის კალათბურთელებთან გამართავენ.&amp;nbsp;&lt;/p&gt;', '&lt;p&gt;ვერის ბაღის საკალათბურთო კომპლექსში გრძელდება საქართველოს სტუდენტური ჩემპიონატი კალთბურთში. ჯგუფური ეტაპის მეორე ტურში მორიგ წარმატებას მიაღწია ილიაუნის გუნდმა, რომელმაც შავი ზღვის საერთაშორისო უნივერსიტეტის კალათბურთელები დიდი ანგარიშით - 106:54 დაამარცხა. ილიაუნის გუნდს ყველაზე მეტი, 32 ქულა ირაკლი ჯანხოთელმა მოუტანა. მე-3 ტურის მატჩს ილიაუნელები 2 დეკემბერს, 16:00 საათზე, ბათუმის საზღვაო აკადემიის კალათბურთელებთან გამართავენ.&amp;nbsp;&lt;/p&gt;', '', 'http://localhost:8082/onlinemarket/public_html/files/foo.jpg', '1', '0', '0', '2016-11-24 19:22:00', '2016-11-24 15:22:18', '2017-11-29 14:54:11', '/news/9', 'ილიაუნელმა კალათბურთელებმა სტუდენტური ლიგის მეორე შეხვედრაც მოიგეს', 'ილიაუნელმა კალათბურთელებმა სტუდენტური ლიგის მეორე შეხვედრაც მოიგეს', '', 'ილიაუნელმა კალათბურთელებმა სტუდენტური ლიგის მეორე შეხვედრაც მოიგეს', 'ილიაუნელმა კალათბურთელებმა სტუდენტური ლიგის მეორე შეხვედრაც მოიგეს', '', '0', '', '', '', null, '1', null);
INSERT INTO `web_posts` VALUES ('10', 'ილიაუნის „ფორბსის“ კლუბში: ზაქარია ზალიკაშვილის საჯარო ლექცია', 'ილიაუნის „ფორბსის“ კლუბში: ზაქარია ზალიკაშვილის საჯარო ლექცია', '', '&lt;p&gt;22 ნოემბერს, 19:00 საათზე, ილიას სახელმწიფო უნივერსიტეტის E405&amp;nbsp;(&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;„&lt;/strong&gt;&lt;strong&gt;ფორბსის&lt;/strong&gt;&lt;/a&gt;“)&amp;nbsp;აუდიტორიაში, ილიას სახელმწიფო უნივერსიტეტისა და&amp;nbsp;&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;ForbesGeorgia&lt;/strong&gt;&lt;/a&gt;&lt;strong&gt;-&lt;/strong&gt;ს ურთიერთთანამშრომლობის ფარგლებში, კომუნიკაციების და ბრენდინგის სპეციალისტი, მედიაპროდიუსერი, ზაქარია ზალიკაშვილი, წაიკითხავს საჯარო ლექციას.&amp;nbsp;&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;მომხსენებლის&lt;/strong&gt;&lt;strong&gt; შესახებ&lt;/strong&gt;&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;თბილისის ბიზნესის ინსტიტუტის დასრულების შემდეგ ზაქარია ზალიკაშვილი აშშ-ში გაემგზავრა და დაამთავრა სამხრეთ კალიფორნიის (აშშ) უნივერსიტეტის კინო- და ტელეხელოვნების ფაკულტეტი, სპეციალობით - კინორეჟისურა. პროფესიული გამოცდილება მიიღო საქართველოს სხვადასხვა კომპანიის მარკეტინგის განყოფილებაში წამყვანი სპეციალისტის და დეპარტამენტის ხელმძღვანელის პოზიციებზე. 2000 წლამდე მუშაობდა GORBI-ის სოციოლოგიური და მარკეტინგული კვლევების პროექტებზე (თამბაქოს, გამამხნევებელი სასმელების, ტელეკომუნიკაციის და სხვა ტრანსნაციონალურ კომპანიებთან. მსოფლიო ბანკთან); ამავე პერიოდში, ჯონ ჰოპკინსის უნივერსიტეტთან და სხვა საერთაშორისო ფონდებთან ერთად, აწარმოებდა სხვადასხვა ტიპის სოციალური კამპანიებს.&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;ზაქარია ზალიკაშვილი სხვადასხვა დროს ხელმძღვანელობდა სარეკლამო სააგენტოს „აუდიენცია“, რადიოს „მწვანე ტალღა“ და კომპანია „ჯეოსელის“ მარკეტინგულ კომუნიკაციებს. მან 2005 წელს დააფუძნა მარკეტინგული კონსულტაციების კომპანია „სტრომბოლი“, ხოლო 2006 წელს - კომპანია „კომუნიკატორი“. 2014 წლიდან ხელმძღვანელობს საზოგადოებასთან ურთიერთობების დეპარტამენტს ქართულ ამერიკულ ინდუსტრიულ ჯგუფში “ჯორჯიან ემერიქან ელოიზ”.&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;დრო:&lt;/strong&gt; 22 ნოემბერი, 19:00 საათი&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;ადგილმდებარეობა:&lt;/strong&gt; E405&amp;nbsp;(&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;„&lt;/strong&gt;&lt;strong&gt;ფორბსის&lt;/strong&gt;&lt;strong&gt;“&lt;/strong&gt;&lt;/a&gt;)&amp;nbsp;აუდიტორია (ჩოლოყაშვილის გამზ. #3/5), ილიას სახელმწიფო უნივერსიტეტი&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;დასწრება თავისუფალია.&lt;/p&gt;', '&lt;p&gt;22 ნოემბერს, 19:00 საათზე, ილიას სახელმწიფო უნივერსიტეტის E405&amp;nbsp;(&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;„&lt;/strong&gt;&lt;strong&gt;ფორბსის&lt;/strong&gt;&lt;/a&gt;“)&amp;nbsp;აუდიტორიაში, ილიას სახელმწიფო უნივერსიტეტისა და&amp;nbsp;&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;ForbesGeorgia&lt;/strong&gt;&lt;/a&gt;&lt;strong&gt;-&lt;/strong&gt;ს ურთიერთთანამშრომლობის ფარგლებში, კომუნიკაციების და ბრენდინგის სპეციალისტი, მედიაპროდიუსერი, ზაქარია ზალიკაშვილი, წაიკითხავს საჯარო ლექციას.&amp;nbsp;&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;მომხსენებლის&lt;/strong&gt;&lt;strong&gt; შესახებ&lt;/strong&gt;&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;თბილისის ბიზნესის ინსტიტუტის დასრულების შემდეგ ზაქარია ზალიკაშვილი აშშ-ში გაემგზავრა და დაამთავრა სამხრეთ კალიფორნიის (აშშ) უნივერსიტეტის კინო- და ტელეხელოვნების ფაკულტეტი, სპეციალობით - კინორეჟისურა. პროფესიული გამოცდილება მიიღო საქართველოს სხვადასხვა კომპანიის მარკეტინგის განყოფილებაში წამყვანი სპეციალისტის და დეპარტამენტის ხელმძღვანელის პოზიციებზე. 2000 წლამდე მუშაობდა GORBI-ის სოციოლოგიური და მარკეტინგული კვლევების პროექტებზე (თამბაქოს, გამამხნევებელი სასმელების, ტელეკომუნიკაციის და სხვა ტრანსნაციონალურ კომპანიებთან. მსოფლიო ბანკთან); ამავე პერიოდში, ჯონ ჰოპკინსის უნივერსიტეტთან და სხვა საერთაშორისო ფონდებთან ერთად, აწარმოებდა სხვადასხვა ტიპის სოციალური კამპანიებს.&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;ზაქარია ზალიკაშვილი სხვადასხვა დროს ხელმძღვანელობდა სარეკლამო სააგენტოს „აუდიენცია“, რადიოს „მწვანე ტალღა“ და კომპანია „ჯეოსელის“ მარკეტინგულ კომუნიკაციებს. მან 2005 წელს დააფუძნა მარკეტინგული კონსულტაციების კომპანია „სტრომბოლი“, ხოლო 2006 წელს - კომპანია „კომუნიკატორი“. 2014 წლიდან ხელმძღვანელობს საზოგადოებასთან ურთიერთობების დეპარტამენტს ქართულ ამერიკულ ინდუსტრიულ ჯგუფში “ჯორჯიან ემერიქან ელოიზ”.&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;დრო:&lt;/strong&gt; 22 ნოემბერი, 19:00 საათი&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;&lt;strong&gt;ადგილმდებარეობა:&lt;/strong&gt; E405&amp;nbsp;(&lt;a href=&quot;http://forbes.ge/&quot; target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot;&gt;&lt;strong&gt;„&lt;/strong&gt;&lt;strong&gt;ფორბსის&lt;/strong&gt;&lt;strong&gt;“&lt;/strong&gt;&lt;/a&gt;)&amp;nbsp;აუდიტორია (ჩოლოყაშვილის გამზ. #3/5), ილიას სახელმწიფო უნივერსიტეტი&lt;/p&gt;\n&lt;p class=&quot;Body&quot;&gt;დასწრება თავისუფალია.&lt;/p&gt;', '', 'http://localhost:8082/onlinemarket/public_html/files/foo.jpg', '1', '0', '0', '2016-11-24 23:25:00', '2016-11-24 15:25:35', '2017-11-29 14:53:39', '/news/10', 'ილიაუნის „ფორბსის“ კლუბში: ზაქარია ზალიკაშვილის საჯარო ლექცია', 'ილიაუნის „ფორბსის“ კლუბში: ზაქარია ზალიკაშვილის საჯარო ლექცია', '', 'ილიაუნის „ფორბსის“ კლუბში: ზაქარია ზალიკაშვილის საჯარო ლექცია', 'ილიაუნის „ფორბსის“ კლუბში: ზაქარია ზალიკაშვილის საჯარო ლექცია', '', '0', '', '', '', null, '1', null);
INSERT INTO `web_posts` VALUES ('11', 'ასდ', '', '', '&lt;p&gt;ასდსადსადსა&lt;/p&gt;', '', '', '', '1', '0', '0', '0000-00-00 00:00:00', '2016-12-14 16:11:54', '2016-12-14 16:11:54', '/page/11', 'სად', '', '', 'სადსა', '', '', '0', null, null, '', null, null, null);
INSERT INTO `web_posts` VALUES ('12', 'დსა', '', '', '&lt;p&gt;სადსადსა&lt;/p&gt;', '', '', '', '0', '0', '0', '0000-00-00 00:00:00', '2016-12-14 16:13:00', '2016-12-14 16:13:07', '/page/12?category=ასდსადსა', 'დსად', '', '', 'სადსად', '', '', '0', null, null, '', null, '2', null);
INSERT INTO `web_posts` VALUES ('13', '1', '1', '1', null, null, null, 'http://localhost:8082/onlinemarket/public_html/files/brand1.png', '1', '0', '0', '0000-00-00 00:00:00', '2016-12-15 14:57:12', '2017-11-29 14:40:59', '#', null, null, null, null, null, null, '0', null, null, '', null, '5', null);
INSERT INTO `web_posts` VALUES ('14', '2', '2', '2', null, null, null, 'http://localhost:8082/onlinemarket/public_html/files/brand2.png', '1', '0', '0', '0000-00-00 00:00:00', '2016-12-16 11:18:33', '2017-11-29 14:41:08', '#', null, null, null, null, null, null, '0', null, null, '', null, '5', null);
INSERT INTO `web_posts` VALUES ('15', '1', '1', '1', null, null, null, 'http://localhost:8082/onlinemarket/public_html/files/slide-1-full.jpg', '1', '0', '0', '0000-00-00 00:00:00', '2016-12-16 13:30:04', '2017-11-29 14:40:25', 'სადსადა', null, null, null, null, null, null, '0', null, null, '', null, '6', '1');
INSERT INTO `web_posts` VALUES ('16', '2', '2', '2', null, null, null, 'http://localhost:8082/onlinemarket/public_html/files/slide-2-full.jpg', '1', '0', '0', '0000-00-00 00:00:00', '2016-12-16 13:30:24', '2017-11-29 14:40:33', 'დსა', null, null, null, null, null, null, '0', null, null, '', null, '6', '3');
INSERT INTO `web_posts` VALUES ('17', '3', '3', '3', null, null, null, 'http://localhost:8082/onlinemarket/public_html/files/slide-3-full.jpg', '1', '0', '0', '0000-00-00 00:00:00', '2016-12-16 13:30:32', '2017-11-29 14:40:40', 'სადსა', null, null, null, null, null, null, '0', null, null, '', null, '6', '2');
INSERT INTO `web_posts` VALUES ('18', 'ჩვენს შესახებ', 'About Us', 'About Us', '&lt;pre&gt;&lt;strong&gt;ჩვენს შესახებ&lt;/strong&gt;&lt;/pre&gt;', '&lt;p&gt;About Us&lt;/p&gt;', '&lt;p&gt;About Us&lt;/p&gt;', '', '1', '0', null, '0000-00-00 00:00:00', '2017-05-29 11:38:22', '2017-05-29 11:48:29', '/page/18', 'ჩვენს შესახებ', 'About Us', 'About Us', 'ჩვენს შესახებ', 'About Us', 'About Us', '0', null, null, null, null, '2', null);
INSERT INTO `web_posts` VALUES ('19', '3', '3', '3', null, null, null, 'http://localhost:8082/onlinemarket/public_html/files/brand3.png', '1', '0', null, null, '2017-11-29 14:43:44', '2017-11-29 14:43:44', '#', null, null, null, null, null, null, '0', null, null, null, null, '5', null);
INSERT INTO `web_posts` VALUES ('20', '4', '4', '4', null, null, null, 'http://localhost:8082/onlinemarket/public_html/files/brand4.png', '1', '0', null, null, '2017-11-29 14:43:55', '2017-11-29 14:43:55', '#', null, null, null, null, null, null, '0', null, null, null, null, '5', null);
INSERT INTO `web_posts` VALUES ('21', '5', '5', '5', null, null, null, 'http://localhost:8082/onlinemarket/public_html/files/brand5.png', '1', '0', null, null, '2017-11-29 14:44:05', '2017-11-29 14:44:05', '#', null, null, null, null, null, null, '0', null, null, null, null, '5', null);
INSERT INTO `web_posts` VALUES ('22', '11', '11', '11', null, null, null, 'http://localhost:8082/onlinemarket/public_html/files/brand1.png', '1', '0', null, null, '2017-11-29 14:44:14', '2017-11-29 14:44:14', '#', null, null, null, null, null, null, '0', null, null, null, null, '5', null);
INSERT INTO `web_posts` VALUES ('23', '22', '22', '22', null, null, null, 'http://localhost:8082/onlinemarket/public_html/files/brand2.png', '1', '0', null, null, '2017-11-29 14:44:24', '2017-11-29 14:44:24', '#', null, null, null, null, null, null, '0', null, null, null, null, '5', null);
INSERT INTO `web_posts` VALUES ('24', '33', '33', '33', null, null, null, 'http://localhost:8082/onlinemarket/public_html/files/brand3.png', '1', '0', null, null, '2017-11-29 14:44:39', '2017-11-29 14:44:39', '#', null, null, null, null, null, null, '0', null, null, null, null, '5', null);

-- ----------------------------
-- Table structure for web_post_categories
-- ----------------------------
DROP TABLE IF EXISTS `web_post_categories`;
CREATE TABLE `web_post_categories` (
  `news_category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`news_category_id`),
  KEY `news_categories_state_index` (`state`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of web_post_categories
-- ----------------------------
INSERT INTO `web_post_categories` VALUES ('1', '1', '1', '1', '0', '2016-11-15 12:09:38', '0000-00-00 00:00:00');
INSERT INTO `web_post_categories` VALUES ('2', '2', '1', '1', '0', '2016-11-15 12:26:10', '0000-00-00 00:00:00');
INSERT INTO `web_post_categories` VALUES ('3', '3', '1', '1', '0', '2016-11-15 12:26:30', '0000-00-00 00:00:00');
INSERT INTO `web_post_categories` VALUES ('4', '4', '2', '1', '0', '2016-11-15 12:27:02', '0000-00-00 00:00:00');
INSERT INTO `web_post_categories` VALUES ('5', '5', '2', '1', '0', '2016-11-15 12:27:22', '0000-00-00 00:00:00');
INSERT INTO `web_post_categories` VALUES ('10', '6', '1', '1', '0', '2016-11-24 19:17:40', '0000-00-00 00:00:00');
INSERT INTO `web_post_categories` VALUES ('16', '10', '1', '1', '0', '2016-12-01 19:43:43', '0000-00-00 00:00:00');
INSERT INTO `web_post_categories` VALUES ('17', '9', '2', '1', '0', '2016-12-01 19:47:31', '0000-00-00 00:00:00');
INSERT INTO `web_post_categories` VALUES ('18', '7', '1', '1', '0', '2016-12-01 19:47:40', '0000-00-00 00:00:00');
INSERT INTO `web_post_categories` VALUES ('19', '8', '1', '1', '0', '2016-12-01 19:47:49', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for web_types
-- ----------------------------
DROP TABLE IF EXISTS `web_types`;
CREATE TABLE `web_types` (
  `int` int(2) DEFAULT NULL,
  `text` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of web_types
-- ----------------------------
INSERT INTO `web_types` VALUES ('1', 'news');
INSERT INTO `web_types` VALUES ('2', 'page');
INSERT INTO `web_types` VALUES ('3', 'news_category');
INSERT INTO `web_types` VALUES ('4', 'news_tags');
INSERT INTO `web_types` VALUES ('5', 'links');
INSERT INTO `web_types` VALUES ('6', 'slideshow');
